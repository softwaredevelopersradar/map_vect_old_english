﻿namespace GrozaMap
{
    partial class FormAirPlane
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bShow = new System.Windows.Forms.Button();
            this.bClean = new System.Windows.Forms.Button();
            this.tbLat = new System.Windows.Forms.TextBox();
            this.tbLong = new System.Windows.Forms.TextBox();
            this.lLat = new System.Windows.Forms.Label();
            this.lLong = new System.Windows.Forms.Label();
            this.tbAngle = new System.Windows.Forms.TextBox();
            this.lAngle = new System.Windows.Forms.Label();
            this.tbNum = new System.Windows.Forms.TextBox();
            this.lNum = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bShow
            // 
            this.bShow.Location = new System.Drawing.Point(22, 24);
            this.bShow.Name = "bShow";
            this.bShow.Size = new System.Drawing.Size(77, 23);
            this.bShow.TabIndex = 115;
            this.bShow.Text = "Отобразить";
            this.bShow.UseVisualStyleBackColor = true;
            this.bShow.Click += new System.EventHandler(this.bShow_Click);
            // 
            // bClean
            // 
            this.bClean.Location = new System.Drawing.Point(106, 24);
            this.bClean.Name = "bClean";
            this.bClean.Size = new System.Drawing.Size(77, 23);
            this.bClean.TabIndex = 116;
            this.bClean.Text = "Очистить";
            this.bClean.UseVisualStyleBackColor = true;
            this.bClean.Click += new System.EventHandler(this.bClean_Click);
            // 
            // tbLat
            // 
            this.tbLat.Location = new System.Drawing.Point(100, 78);
            this.tbLat.MaxLength = 7;
            this.tbLat.Name = "tbLat";
            this.tbLat.Size = new System.Drawing.Size(86, 20);
            this.tbLat.TabIndex = 117;
            this.tbLat.TextChanged += new System.EventHandler(this.tbLat_TextChanged);
            // 
            // tbLong
            // 
            this.tbLong.Location = new System.Drawing.Point(99, 104);
            this.tbLong.MaxLength = 7;
            this.tbLong.Name = "tbLong";
            this.tbLong.Size = new System.Drawing.Size(86, 20);
            this.tbLong.TabIndex = 118;
            // 
            // lLat
            // 
            this.lLat.AutoSize = true;
            this.lLat.Location = new System.Drawing.Point(19, 81);
            this.lLat.Name = "lLat";
            this.lLat.Size = new System.Drawing.Size(74, 13);
            this.lLat.TabIndex = 119;
            this.lLat.Text = "Широта, град";
            // 
            // lLong
            // 
            this.lLong.AutoSize = true;
            this.lLong.Location = new System.Drawing.Point(20, 107);
            this.lLong.Name = "lLong";
            this.lLong.Size = new System.Drawing.Size(79, 13);
            this.lLong.TabIndex = 120;
            this.lLong.Text = "Долгота, град";
            // 
            // tbAngle
            // 
            this.tbAngle.Location = new System.Drawing.Point(100, 52);
            this.tbAngle.MaxLength = 7;
            this.tbAngle.Name = "tbAngle";
            this.tbAngle.Size = new System.Drawing.Size(86, 20);
            this.tbAngle.TabIndex = 121;
            // 
            // lAngle
            // 
            this.lAngle.AutoSize = true;
            this.lAngle.Location = new System.Drawing.Point(19, 55);
            this.lAngle.Name = "lAngle";
            this.lAngle.Size = new System.Drawing.Size(61, 13);
            this.lAngle.TabIndex = 122;
            this.lAngle.Text = "Угол, град";
            // 
            // tbNum
            // 
            this.tbNum.Location = new System.Drawing.Point(100, 130);
            this.tbNum.MaxLength = 7;
            this.tbNum.Name = "tbNum";
            this.tbNum.Size = new System.Drawing.Size(86, 20);
            this.tbNum.TabIndex = 123;
            // 
            // lNum
            // 
            this.lNum.AutoSize = true;
            this.lNum.Location = new System.Drawing.Point(20, 133);
            this.lNum.Name = "lNum";
            this.lNum.Size = new System.Drawing.Size(41, 13);
            this.lNum.TabIndex = 124;
            this.lNum.Text = "Номер";
            // 
            // FormAirPlane
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(195, 155);
            this.Controls.Add(this.lNum);
            this.Controls.Add(this.tbNum);
            this.Controls.Add(this.lAngle);
            this.Controls.Add(this.tbAngle);
            this.Controls.Add(this.lLong);
            this.Controls.Add(this.lLat);
            this.Controls.Add(this.tbLong);
            this.Controls.Add(this.tbLat);
            this.Controls.Add(this.bClean);
            this.Controls.Add(this.bShow);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormAirPlane";
            this.Text = "Самолеты";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAirPlane_FormClosing);
            this.Load += new System.EventHandler(this.FormAirPlane_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bShow;
        private System.Windows.Forms.Button bClean;
        public System.Windows.Forms.TextBox tbLat;
        public System.Windows.Forms.TextBox tbLong;
        private System.Windows.Forms.Label lLat;
        private System.Windows.Forms.Label lLong;
        public System.Windows.Forms.TextBox tbAngle;
        private System.Windows.Forms.Label lAngle;
        public System.Windows.Forms.TextBox tbNum;
        private System.Windows.Forms.Label lNum;
    }
}