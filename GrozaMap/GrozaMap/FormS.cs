﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace GrozaMap
{
    public partial class FormS : Form
    {

        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        //private MapForm objMapForm6;
        private AxaxcMapScreen axaxcMapScreen;

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        public double dchislo1;
        public long ichislo1;
        public double X_Coordl1;
        public double Y_Coordl1;
        public double X_Coordl2;
        public double Y_Coordl2;
        public double S_XY;
        public double DXX;
        public double DYY;
        // ......................................................................

        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные


        // Конструктор *********************************************************** 

        public FormS(ref AxaxcMapScreen axaxcMapScreen1)
        {
            InitializeComponent();

            axaxcMapScreen = axaxcMapScreen1;

            dchislo1 = 0;
            ichislo1 = 0;
            X_Coordl1=0;
            Y_Coordl1=0;
            X_Coordl2 = 0;
            Y_Coordl2 = 0;
            S_XY=0;
            DXX=0;
            DYY=0;

            // ......................................................................

        } // Конструктор
        // ***********************************************************  Конструктор

        // *****************************************************************************************
        // Обработчик кнопки Button1: Выбрать начальное положение
        // *****************************************************************************************

        private void button1_Click(object sender, EventArgs e)
        {
            // ......................................................................
            GlobalVarLn.fl_S1_stat = 1;
            GlobalVarLn.fl_S2_stat = 0;
            // ......................................................................
            // Отображение координат карты, полученных кликом мыши на карте
            // !!! MapX1,MapY1 -> реальные координаты на местности карты в м (Plane)

            // m->km
            X_Coordl1 = GlobalVarLn.MapX1 / 1000;
            Y_Coordl1 = GlobalVarLn.MapY1 / 1000;

            ichislo1 = (long)(X_Coordl1 * 10000000);
            dchislo1 = ((double)ichislo1) / 10000000;
            textBox1.Text = Convert.ToString(dchislo1);   // X, карта

            ichislo1 = (long)(Y_Coordl1 * 10000000);
            dchislo1 = ((double)ichislo1) / 10000000;
            textBox2.Text = Convert.ToString(dchislo1);   // Y, карта
            // ......................................................................
            GlobalVarLn.X_Coordl1_stat = GlobalVarLn.MapX1;
            GlobalVarLn.Y_Coordl1_stat = GlobalVarLn.MapY1;
            // ......................................................................
            f_Map_Rect_XY(
                          GlobalVarLn.MapX1,
                          GlobalVarLn.MapY1
                         );
            // ......................................................................

        } // Button1
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button2: Выбрать конечное положение
        // *****************************************************************************************

        private void button2_Click(object sender, EventArgs e)
        {
            // ......................................................................
            GlobalVarLn.fl_S2_stat = 1;
            // ......................................................................
            // Отображение координат карты, полученных кликом мыши на карте
            // !!! MapX1,MapY1 -> реальные координаты на местности карты в м (Plane)

            // m->km
            X_Coordl2 = GlobalVarLn.MapX1 / 1000;
            Y_Coordl2 = GlobalVarLn.MapY1 / 1000;

            ichislo1 = (long)(X_Coordl2 * 10000000);
            dchislo1 = ((double)ichislo1) / 10000000;
            textBox3.Text = Convert.ToString(dchislo1);   // X, карта

            ichislo1 = (long)(Y_Coordl2 * 10000000);
            dchislo1 = ((double)ichislo1) / 10000000;
            textBox4.Text = Convert.ToString(dchislo1);   // Y, карта
            // ......................................................................
            GlobalVarLn.X_Coordl2_stat = GlobalVarLn.MapX1;
            GlobalVarLn.Y_Coordl2_stat = GlobalVarLn.MapY1;
            // ......................................................................
            f_Map_Rect_XY(
                          GlobalVarLn.MapX1,
                          GlobalVarLn.MapY1
                         );
            // ......................................................................
            f_Map_Line_XY(

                          X_Coordl1*1000,
                          Y_Coordl1*1000,
                          X_Coordl2*1000,
                          Y_Coordl2*1000

                         );
            // ......................................................................

            //km
            DXX = X_Coordl2 - X_Coordl1;
            DYY = Y_Coordl2 - Y_Coordl1;

            S_XY = Math.Sqrt(DXX * DXX + DYY * DYY);
            // ......................................................................
            //// m->km
            //X_Coordl2 = S_XY / 1000;
            X_Coordl2 = S_XY;

            ichislo1 = (long)(X_Coordl2 * 10000000);
            dchislo1 = ((double)ichislo1) / 10000000;
            textBox5.Text = Convert.ToString(dchislo1);   // S, карта
            // ......................................................................


        } // Button2
        // *************************************************************************************


        // FUNCTIONS ********************************************************************************

        // ******************************************************************************************
        // Нарисовать метку по координатам (красный прямоугольник)
        //
        // Входные параметры:
        // X - X, m
        // Y - Y, m
        // ******************************************************************************************
        public void f_Map_Rect_XY(

                                  double X,
                                  double Y
                                  )
        {

            // -------------------------------------------------------------------------------------
            Graphics graph = axaxcMapScreen.CreateGraphics();

            Pen penRed2 = new Pen(Color.Red, 2);
            Brush brushRed2 = new SolidBrush(Color.Red);

            // -------------------------------------------------------------------------------------

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {
                graph.FillRectangle(brushRed2, (int)X - axaxcMapScreen.MapLeft, (int)Y - axaxcMapScreen.MapTop, 7, 7);

                // Надпись
                //TitleObject(typeStation, lat, lon);
            }

            // -------------------------------------------------------------------------------------

            //objMapForm6.graph.Dispose();


        } // P/P f_Map_Rect_XY

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            ;
        } 
        // *************************************************************************************

        // ******************************************************************************************
        // Нарисовать линию по координатам
        //
        // Входные параметры:
        // X1,X2 - X, m
        // Y1,Y2 - Y, m
        // ******************************************************************************************
        public void f_Map_Line_XY(
                                  double X1,
                                  double Y1,
                                  double X2,
                                  double Y2

                                 )
        {

            // -------------------------------------------------------------------------------------
            Graphics graph = axaxcMapScreen.CreateGraphics();

            Pen penRed2 = new Pen(Color.Red, 2);
            Brush brushRed2 = new SolidBrush(Color.Red);

            // -------------------------------------------------------------------------------------

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X1, ref Y1);
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X2, ref Y2);

            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                Point point1 = new Point((int)X1 - axaxcMapScreen.MapLeft, (int)Y1 - axaxcMapScreen.MapTop);
                Point point2 = new Point((int)X2 - axaxcMapScreen.MapLeft, (int)Y2 - axaxcMapScreen.MapTop);

                graph.DrawLine(penRed2, point1, point2);
            }

            // -------------------------------------------------------------------------------------

        }

        private void FormS_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

        } // P/P f_Map_Line_XY

        // ********************************************************************************* FUNCTIONS


    } // Class
} // namespace
