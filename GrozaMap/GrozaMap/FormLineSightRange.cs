﻿using System;
using System.Collections.Generic;
using System.Drawing;
using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;
using System.Globalization;
using System.ComponentModel;
using System.Reflection;
using System.Linq;


namespace GrozaMap
{
    public partial class FormLineSightRange : Form
    {
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        private AxaxcMapScreen axaxcMapScreen;



        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private double dchislo;
        private long ichislo;
        //private double LAMBDA;
        // .....................................................................
        // Координаты центра ЗПВ

        // Координаты центра ЗПВ на местности в м
        //private double XSP_comm;
        //private double YSP_comm;

/*
        // DATUM
        private double dXdat_comm;
        private double dYdat_comm;
        private double dZdat_comm;

        private double dLat_comm;
        private double dLong_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_comm;
        private double LongKrG_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_comm;
        private double LongKrR_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_comm;
        private int Lat_Min_comm;
        private double Lat_Sec_comm;
        private int Long_Grad_comm;
        private int Long_Min_comm;
        private double Long_Sec_comm;
        // Гаусс-крюгер(СК42) м
        private double XSP42_comm;
        private double YSP42_comm;
*/
        //777*
        private double OwnHeight_comm;
        private double OwnHeight2_comm;

        public static FormLineSightRange Instance { get; private set; }

        // Высота средства подавления
        private double HeightAntennOwn_comm;
        //777*
        private double HeightTotalOwn_comm;
        private double HeightTotalOwn2_comm;

        // объект подавления
        private int i_HeightOpponent_comm;
        private int i_HeightOwnObject_comm;
        //777*
        private double HeightOpponent_comm;
        private double HeightOpponent2_comm;

        // Высота антенны противника
        private int iOpponAnten_comm;

        //777*
        private int iMiddleHeight_comm;
        private int iMiddleHeight2_comm;

        // ДПВ
        //777*
        private int iDSR;
        private int iDSR2;

        //private Point[] tpPointDSR;
        //private Point[] tpPointPictDSR;

        // ......................................................................
        // ZOSP
/*
        private double F_ZOSP;
        private double P_ZOSP;
        private double K_ZOSP;
        private int iKP_ZOSP;
        private double dKP_ZOSP;
        private double VC_ZOSP;
        private double ipw_ZOSP;
 */ 
        // ......................................................................

        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 
        ComponentResourceManager resources = new ComponentResourceManager(typeof(FormLineSightRange));
        private int NumberOfLanguage;


        public FormLineSightRange(ref AxaxcMapScreen axaxcMapScreen1)
        {
            Instance = this;

            InitializeComponent();

           

            axaxcMapScreen = axaxcMapScreen1;

            dchislo = 0;
            ichislo = 0;
            //LAMBDA = 300000;

            // .....................................................................
            // Координаты центра ЗПВ

            // Координаты центра ЗПВ на местности в м
            //XSP_comm = 0;
            //YSP_comm = 0;

/*
            // DATUM
            // ГОСТ 51794_2008
            dXdat_comm = 25;
            dYdat_comm = -141;
            dZdat_comm = -80;

            dLat_comm = 0;
            dLong_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_comm = 0;
            LongKrG_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_comm = 0;
            LongKrR_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_comm = 0;
            Lat_Min_comm = 0;
            Lat_Sec_comm = 0;
            Long_Grad_comm = 0;
            Long_Min_comm = 0;
            Long_Sec_comm = 0;
            // Гаусс-крюгер(СК42) м
            XSP42_comm = 0;
            YSP42_comm = 0;
*/
            //777*
            OwnHeight_comm = 0;
            OwnHeight2_comm = 0;

            i_HeightOwnObject_comm = 0;
            HeightAntennOwn_comm = 0; // антенна
            //777*
            HeightTotalOwn_comm = 0;
            HeightTotalOwn2_comm = 0;

            // объект подавления
            i_HeightOpponent_comm = 0;
            //777*
            HeightOpponent_comm = 0;
            HeightOpponent2_comm = 0;

            // Высота антенны противника
            iOpponAnten_comm = 0;

            //777*
            iMiddleHeight_comm = 0;
            iMiddleHeight2_comm = 0;

            // ДПВ
            //777*
            iDSR = 0;
            iDSR2 = 0;
            // ......................................................................
            // ZOSP
/*
            F_ZOSP = 0;
            P_ZOSP = 0;
            K_ZOSP = 0;
            iKP_ZOSP = 0;
            dKP_ZOSP = 0;
            VC_ZOSP = 300000000;
            ipw_ZOSP = 0;
            // ......................................................................
*/

        } // Конструктор
        // ***********************************************************  Конструктор


        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void FormLineSightRange_Load(object sender, EventArgs e)
        {
            string strExePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string PathFolder1 = Path.GetDirectoryName(strExePath);
            string strPathLatest = PathFolder1 + "\\INI\\Common.ini";
            foreach (string line in File.ReadLines(strPathLatest))
            {
                if (line.Contains("Language "))
                {
                    string LanguageAsNumber = line.Split(new string[] { "=" }, StringSplitOptions.None).Last();
                    NumberOfLanguage = Int32.Parse(LanguageAsNumber);
                    break;
                }
            }
            LanguageChooser();
            // ----------------------------------------------------------------------
            gbOwnRect.Visible = true;
            gbOwnRect.Location = new Point(6, 9);

            gbOwnRect42.Visible = false;
            gbOwnRad.Visible = false;
            gbOwnDegMin.Visible = false;
            gbOwnDegMinSec.Visible = false;

            cbChooseSC.SelectedIndex = 0;
            cbCenterLSR.SelectedIndex = 0;
            // ----------------------------------------------------------------------
            // Переменные

            GlobalVarLn.fl_LineSightRange = 0;
            GlobalVarLn.flCoordZPV = 0; // =1-> Выбрали центр ЗПВ
            //777*
            GlobalVarLn.flCoordParaZPV = 0; // =1-> Выбрали центр ЗПВ
            // ----------------------------------------------------------------------
            //777*
            if (GlobalVarLn.listPointDSR.Count != 0)
                GlobalVarLn.listPointDSR.Clear();
            if (GlobalVarLn.listPointDSR1.Count != 0)
                GlobalVarLn.listPointDSR1.Clear();
            if (GlobalVarLn.listPointDSR2.Count != 0)
                GlobalVarLn.listPointDSR2.Clear();

            // ---------------------------------------------------------------------

        } // Загрузка формы
        // ************************************************************************
        // ************************************************************************
        // Очистка
        // ************************************************************************
        private void bClear_Click_1(object sender, EventArgs e)
        {

            ClassMap.ClearZPV();

        } // Clear
        // ************************************************************************

        // ************************************************************************
        // Выбор Center ZPV
        // ************************************************************************
 
        private void button1_Click_1(object sender, EventArgs e)
        {

            double xtmp_ed, ytmp_ed, htmp_ed;
            double xtmp1_ed, ytmp1_ed;
            int ind1 = 0;
            int ind2 = 0;

            xtmp_ed = 0;
            ytmp_ed = 0;
            htmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;
            // ......................................................................
            ClassMap objClassMap1_ed = new ClassMap();
            ClassMap objClassMap2_ed = new ClassMap();
            ClassMap objClassMap3_ed = new ClassMap();
            // ......................................................................

            ind1=GlobalVarLn.ObjFormLineSightRangeG.cbChooseSC.SelectedIndex;
            ind2=GlobalVarLn.ObjFormLineSightRangeG.cbCenterLSR.SelectedIndex;

            ClassMap.ClearZPV();

            GlobalVarLn.ObjFormLineSightRangeG.cbChooseSC.SelectedIndex = ind1;
            GlobalVarLn.ObjFormLineSightRangeG.cbCenterLSR.SelectedIndex = ind2;

            // Убрать с карты
            //GlobalVarLn.axMapScreenGlobal.Repaint();
            // ----------------------------------------------------------------------
            // Enter Center ZPV

            switch (cbCenterLSR.SelectedIndex)
            {
                case 0: // Мышь на карте

                    // !!! реальные координаты на местности карты в м (Plane)
                    GlobalVarLn.XCenter_ZPV = GlobalVarLn.MapX1;
                    GlobalVarLn.YCenter_ZPV = GlobalVarLn.MapY1;

                    if ((GlobalVarLn.XCenter_ZPV == 0) || (GlobalVarLn.YCenter_ZPV == 0))
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("Не выбран центр ЗПВ");
                        }
                        else if (GlobalVarLn.fl_Azb == 1)
                        {
                            //Azb???
                            MessageBox.Show("zonanın merkezi seçilmeyib");
                        }
                        else 
                        {
                            MessageBox.Show("Center of LOS is not selected");
                        }

                        return;
                    }

                break;

                case 1: // Выбор из списка СП: АСП

                //if (System.IO.File.Exists(Application.StartupPath + "\\Settings.ini"))
                //{
                //    GlobalVarLn.XCenter_ZPV = (double)iniRW.get_X_ASP();
                //    GlobalVarLn.YCenter_ZPV = (double)iniRW.get_Y_ASP();
                //}
                //else
                //{
                //    MessageBox.Show("Невозможно открыть INI файл");
                //    return;
                //}
                //777
                GlobalVarLn.XCenter_ZPV = GlobalVarLn.XCenter_Sost;
                GlobalVarLn.YCenter_ZPV = GlobalVarLn.YCenter_Sost;

                if ((GlobalVarLn.XCenter_ZPV == 0) || (GlobalVarLn.YCenter_ZPV == 0))
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Нет координат АСП");
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                        {
                        //Azb???
                        MessageBox.Show("No mənşəyi (MS)");
                    }
                    else 
                     {
                      MessageBox.Show("No station coordinates");
                     }

                     return;
                }

                break;

                case 2: // Выбор из списка СП: АСПсопр.

                //if (System.IO.File.Exists(Application.StartupPath + "\\Settings.ini"))
                //{
                //    GlobalVarLn.XCenter_ZPV = (double)iniRW.get_X_ASPS();
                //    GlobalVarLn.YCenter_ZPV = (double)iniRW.get_Y_ASPS();
                //}
                //else
                //{
                //    MessageBox.Show("Невозможно открыть INI файл");
                //    return;
                //}

                //777
                GlobalVarLn.XCenter_ZPV = GlobalVarLn.XPoint1_Sost;
                GlobalVarLn.YCenter_ZPV = GlobalVarLn.YPoint1_Sost;

                if ((GlobalVarLn.XCenter_ZPV == 0) || (GlobalVarLn.YCenter_ZPV == 0))
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Нет координат АСП сопряженной");
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                    {
                        //Azb???
                        MessageBox.Show("No mənşəyi (Beraber işleyen manee stansiyası)");
                    }
                    else
                    {
                     MessageBox.Show("No JSmated coordinates");
                    }

                   return;
                }

                break;

                case 3: // Выбор из списка СП: PU

                //if (System.IO.File.Exists(Application.StartupPath + "\\Settings.ini"))
                //{
                //    GlobalVarLn.XCenter_ZPV = (double)iniRW.get_X_PU();
                //    GlobalVarLn.YCenter_ZPV = (double)iniRW.get_Y_PU();
                //}
                //else
                //{
                //    MessageBox.Show("Невозможно открыть INI файл");
                //    return;
                //}
                //777
                GlobalVarLn.XCenter_ZPV = GlobalVarLn.XPoint2_Sost;
                GlobalVarLn.YCenter_ZPV = GlobalVarLn.YPoint2_Sost;

                if ((GlobalVarLn.XCenter_ZPV == 0) || (GlobalVarLn.YCenter_ZPV == 0))
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Нет координат ПУ");
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                        {
                        //Azb???
                        MessageBox.Show("No mənşəyi (idareetme pultu)");
                    }
                    else 
                    {
                     MessageBox.Show("No CP coordinates");
                    }

                    return;
                }

                break;


            } // SWITCH
            // ----------------------------------------------------------------------

            // ......................................................................
            xtmp_ed = GlobalVarLn.XCenter_ZPV;
            ytmp_ed = GlobalVarLn.YCenter_ZPV;
            // ......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(xtmp_ed, ytmp_ed);
            GlobalVarLn.HCenter_ZPV = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            htmp_ed = GlobalVarLn.HCenter_ZPV;
            tbOwnHeight.Text = Convert.ToString(htmp_ed);
            // ......................................................................
            GlobalVarLn.fl_LineSightRange = 1;
            GlobalVarLn.flCoordZPV = 1;        // Центр ЗПВ выбран
            // ......................................................................
            // SP на карте

            // Убрать с карты
            //GlobalVarLn.axMapScreenGlobal.Repaint();

            // SP
            ClassMap.f_DrawSPXY(
                          GlobalVarLn.XCenter_ZPV,  // m на местности
                          GlobalVarLn.YCenter_ZPV,
                              ""
                         );
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;

            //333
            GlobalVarLn.BWGS84_ZPV = xtmp1_ed;
            GlobalVarLn.LWGS84_ZPV = ytmp1_ed;

            // .......................................................................

            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLong
                (
                // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLong_comm_ZPV   // приращение по долготе, угл.сек
                );
            // ................................................................ dLong

            // dLat .................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLat
                (
                // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLat_comm_ZPV        // приращение по долготе, угл.сек
                );
            // ................................................................. dLat

            // Lat,Long .............................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objClassMap1_ed.f_WGS84_SK42_Lat_Long
                   (
                // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       GlobalVarLn.dLat_comm_ZPV,       // приращение по долготе, угл.сек
                       GlobalVarLn.dLong_comm_ZPV,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref GlobalVarLn.LatKrG_comm_ZPV,   // широта
                       ref GlobalVarLn.LongKrG_comm_ZPV   // долгота
                   );
            // ............................................................ Lat,Long


            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            GlobalVarLn.LatKrR_comm_ZPV = (GlobalVarLn.LatKrG_comm_ZPV * Math.PI) / 180;
            GlobalVarLn.LongKrR_comm_ZPV = (GlobalVarLn.LongKrG_comm_ZPV * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LatKrG_comm_ZPV,

                // Выходные параметры 
                ref GlobalVarLn.Lat_Grad_comm_ZPV,
                ref GlobalVarLn.Lat_Min_comm_ZPV,
                ref GlobalVarLn.Lat_Sec_comm_ZPV
              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LongKrG_comm_ZPV,

                // Выходные параметры 
                ref GlobalVarLn.Long_Grad_comm_ZPV,
                ref GlobalVarLn.Long_Min_comm_ZPV,
                ref GlobalVarLn.Long_Sec_comm_ZPV
              );
            // .......................................................................

            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            objClassMap3_ed.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       GlobalVarLn.LatKrG_comm_ZPV,   // широта
                       GlobalVarLn.LongKrG_comm_ZPV,  // долгота

                       // Выходные параметры (km)
                       ref GlobalVarLn.XSP42_comm_ZPV,
                       ref GlobalVarLn.YSP42_comm_ZPV
                   );

            // km->m
            GlobalVarLn.XSP42_comm_ZPV = GlobalVarLn.XSP42_comm_ZPV * 1000;
            GlobalVarLn.YSP42_comm_ZPV = GlobalVarLn.YSP42_comm_ZPV * 1000;
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отобразить координаты в окошках

            ClassMap.Otobr_ZPV();
            // .......................................................................


        } // Button1: Center ZPV
        // ************************************************************************

        // ************************************************************************
        // Button "Принять" ЗПВ
        // ************************************************************************

        private void bAccept_Click(object sender, EventArgs e)
        {
            String ss = "";

            // Расчет зоны MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN 

            if (GlobalVarLn.flCoordZPV == 0)
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Центр зоны не выбран");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb???
                    MessageBox.Show("zonanın   merkezi    seçilmeyib");
                }
                else 
                {
                    MessageBox.Show("Zone center is not selected");
                }

                return;
            }
            // ----------------------------------------------------------------------

            if (GlobalVarLn.listPointDSR.Count != 0)
                GlobalVarLn.listPointDSR.Clear();
            // ----------------------------------------------------------------------

            // Ввод параметров ********************************************************
            // !!! Координаты центра ЗПВ уже расчитаны и введены по кнопке 'Центр ЗПВ'

            // ******************************************************** Ввод параметров

            // Центр ЗПВ **************************************************************
            // Координаты на местности в м

            GlobalVarLn.tpOwnCoordRect_ZPV.X = (int)GlobalVarLn.XCenter_ZPV;
            GlobalVarLn.tpOwnCoordRect_ZPV.Y = (int)GlobalVarLn.YCenter_ZPV;

            //if ((tbXRect.Text == "") || (tbYRect.Text == ""))
            //{
            //    MessageBox.Show("Некорректные координаты центра зоны");
            //    return;
           // }

            // ************************************************************** Центр ЗПВ

            // !!! Высоты *************************************************************
            // !!! GlobalVarLn.HCenter_ZPV введена по кнопке ЦентрЗоны

            // Средняя высота местности
            iMiddleHeight_comm = DefineMiddleHeight_Comm(GlobalVarLn.tpOwnCoordRect_ZPV, GlobalVarLn.axMapPointGlobalAdd,
                                                         GlobalVarLn.axMapScreenGlobal);
            tbMiddleHeight.Text = iMiddleHeight_comm.ToString();

            // ЦЕНТР ЗПВ
            // Высота антенны
            //HeightAntennOwn_comm = Convert.ToDouble(tbHAnt.Text);

            ss = tbHAnt.Text;
            try
            {
                HeightAntennOwn_comm = Convert.ToDouble(ss);
            }
            catch (SystemException)
            {
                try
                {
                    if (ss.IndexOf(",") > -1) ss = ss.Replace(',', '.');
                    else ss = ss.Replace('.', ',');
                    HeightAntennOwn_comm = Convert.ToDouble(ss);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимая высота антенны");
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                    {
                        //Azb?
                        MessageBox.Show("Yolverilməz Antenanın hündürlüyü göstəriciləri");
                    }
                    else 
                    {
                        MessageBox.Show("Invalid antenna height");
                    }

                    return;
                }
            }

            // Рельеф
            OwnHeight_comm = GlobalVarLn.HCenter_ZPV;
            // Общая высота СП
            HeightTotalOwn_comm = HeightAntennOwn_comm + OwnHeight_comm;
            // отобразить значение высоты
            ichislo = (long)(HeightTotalOwn_comm);
            tbHeightOwnObject.Text = Convert.ToString(ichislo);

            double xz = 0;
            // ОП
            // Антенна ОП
            //iOpponAnten_comm = Convert.ToInt32(tbOpponentAntenna.Text);
            ss = tbOpponentAntenna.Text;
            try
            {
                xz = Convert.ToDouble(ss);
            }
            catch (SystemException)
            {
                try
                {
                    if (ss.IndexOf(",") > -1) ss = ss.Replace(',', '.');
                    else ss = ss.Replace('.', ',');
                    xz = Convert.ToDouble(ss);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимая высота антенны");
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                    {
                        //Azb?
                        MessageBox.Show("Yolverilməz Antenanın hündürlüyü göstəriciləri");
                    }
                    else 
                    {
                        MessageBox.Show("Invalid antenna height");
                    }

                    return;
                }
            }
            iOpponAnten_comm=(int)xz;

            // Общая высота ОП
            HeightOpponent_comm = iMiddleHeight_comm + iOpponAnten_comm;
            // отобразить значение высоты
            ichislo = (long)(HeightOpponent_comm);
            tbHeightOpponObject.Text = Convert.ToString(ichislo);
            // ************************************************************* !!! Высоты

            // Расчет зоны ************************************************************

            // определить ДПВ
            iDSR = CountDSR((int)HeightTotalOwn_comm, (int)OwnHeight_comm, iMiddleHeight_comm, iMiddleHeight_comm + iOpponAnten_comm);
            tbDistSightRange.Text = iDSR.ToString();

            // рассчитать точки полигона
            CountPointLSR(GlobalVarLn.tpOwnCoordRect_ZPV, (int)HeightTotalOwn_comm,
                         (int)HeightAntennOwn_comm, (double)iDSR, iOpponAnten_comm);

            // отрисовать ЗПВ
            ClassMap.DrawLSR();
            //ClassMap.DrawLSR1();

            // ************************************************************ Расчет зоны

        } // Button "Принять"
        // MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN  Расчет зоны


        // Сопряженная пара PARA PARA PARA PARA PARA PARA PARA PARA PARA PARA PARA 
        //777*

        private void button2_Click(object sender, EventArgs e)
        {
            String ss = "";


            ClassMap.ClearZPV();


            // Centers ***********************************************************
            // SP1
            GlobalVarLn.XCenter1_ZPV = GlobalVarLn.XCenter_Sost;
            GlobalVarLn.YCenter1_ZPV = GlobalVarLn.YCenter_Sost;
            if ((GlobalVarLn.XCenter1_ZPV == 0) || (GlobalVarLn.YCenter1_ZPV == 0))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Нет координат АСП");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb???
                    MessageBox.Show("No mənşəyi (MS)");
                }
                else 
                {
                    MessageBox.Show("No station coordinates");
                }

                return;
            }

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.XCenter1_ZPV, GlobalVarLn.YCenter1_ZPV);
            GlobalVarLn.HCenter1_ZPV = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

            // SP2
            GlobalVarLn.XCenter2_ZPV = GlobalVarLn.XPoint1_Sost;
            GlobalVarLn.YCenter2_ZPV = GlobalVarLn.YPoint1_Sost;
            if ((GlobalVarLn.XCenter2_ZPV == 0) || (GlobalVarLn.YCenter2_ZPV == 0))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Нет координат АСП сопряженной");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb???
                    MessageBox.Show("No mənşəyi (Beraber işleyen manee stansiyası)");
                }
                else 
                {
                    MessageBox.Show("No JSmated coordinates");
                }

                return;
            }

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.XCenter2_ZPV, GlobalVarLn.YCenter2_ZPV);
            GlobalVarLn.HCenter2_ZPV = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            // *********************************************************** Centers

            // DRAW_SP ************************************************************

            GlobalVarLn.flCoordParaZPV = 1;        // 2Centers
/*
            // SP1
            ClassMap.f_DrawSPXY(
                          GlobalVarLn.XCenter1_ZPV,  // m на местности
                          GlobalVarLn.YCenter1_ZPV,
                              ""
                         );
            // SP2
            ClassMap.f_DrawSPXY(
                          GlobalVarLn.XCenter2_ZPV,  // m на местности
                          GlobalVarLn.YCenter2_ZPV,
                              ""
                         );
*/
            // ************************************************************ DRAW_SP


            // Calculation ********************************************************

            // ----------------------------------------------------------------------
            if (GlobalVarLn.flCoordParaZPV == 0)
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Центры зон не выбраны");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb???
                    MessageBox.Show("zonanın merkezi seçilmeyib");
                }
                else 
                {
                    MessageBox.Show("Zone centers are not selected");
                }

                return;
            }
            // ----------------------------------------------------------------------
            if (GlobalVarLn.listPointDSR1.Count != 0)
                GlobalVarLn.listPointDSR1.Clear();
            if (GlobalVarLn.listPointDSR2.Count != 0)
                GlobalVarLn.listPointDSR2.Clear();
            // ----------------------------------------------------------------------

            // Центр ЗПВ **************************************************************
            // Координаты на местности в м

            GlobalVarLn.tpOwnCoordRect1_ZPV.X = (int)GlobalVarLn.XCenter1_ZPV;
            GlobalVarLn.tpOwnCoordRect1_ZPV.Y = (int)GlobalVarLn.YCenter1_ZPV;
            GlobalVarLn.tpOwnCoordRect2_ZPV.X = (int)GlobalVarLn.XCenter2_ZPV;
            GlobalVarLn.tpOwnCoordRect2_ZPV.Y = (int)GlobalVarLn.YCenter2_ZPV;
            // ************************************************************** Центр ЗПВ

            // !!! Высоты *************************************************************
            // !!! GlobalVarLn.HCenter1_ZPV,GlobalVarLn.HCenter2_ZPV уже рассчитаны

            // ........................................................................
            // Средняя высота местности

            iMiddleHeight_comm = DefineMiddleHeight_Comm(GlobalVarLn.tpOwnCoordRect1_ZPV, GlobalVarLn.axMapPointGlobalAdd,
                                                         GlobalVarLn.axMapScreenGlobal);
            iMiddleHeight2_comm = DefineMiddleHeight_Comm(GlobalVarLn.tpOwnCoordRect2_ZPV, GlobalVarLn.axMapPointGlobalAdd,
                                                         GlobalVarLn.axMapScreenGlobal);
            // ........................................................................
            // ЦЕНТР ЗПВ
            // Высота антенны

            ss = tbHAnt.Text;
            try
            {
                HeightAntennOwn_comm = Convert.ToDouble(ss);
            }
            catch (SystemException)
            {
                try
                {
                    if (ss.IndexOf(",") > -1) ss = ss.Replace(',', '.');
                    else ss = ss.Replace('.', ',');
                    HeightAntennOwn_comm = Convert.ToDouble(ss);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимая высота антенны");
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                    {
                        //Azb?
                        MessageBox.Show("Yolverilməz Antenanın hündürlüyü göstəriciləri");
                    }
                    else 
                    {
                        //Azb?
                        MessageBox.Show("Invalid antenna height");
                    }

                    return;
                }
            }
            // ........................................................................

            // Рельеф (SP)
            OwnHeight_comm = GlobalVarLn.HCenter1_ZPV;
            OwnHeight2_comm = GlobalVarLn.HCenter2_ZPV;

            // Общая высота СП
            HeightTotalOwn_comm = HeightAntennOwn_comm + OwnHeight_comm;
            HeightTotalOwn2_comm = HeightAntennOwn_comm + OwnHeight2_comm;
            // ........................................................................
            // ОП
            // Антенна ОП

            double xz = 0;
            ss = tbOpponentAntenna.Text;
            try
            {
                xz = Convert.ToDouble(ss);
            }
            catch (SystemException)
            {
                try
                {
                    if (ss.IndexOf(",") > -1) ss = ss.Replace(',', '.');
                    else ss = ss.Replace('.', ',');
                    xz = Convert.ToDouble(ss);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимая высота антенны");
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                    {
                        //Azb?
                        MessageBox.Show("Yolverilməz Antenanın hündürlüyü göstəriciləri");
                    }
                    else 
                    {
                        MessageBox.Show("Invalid antenna height");
                    }

                    return;
                }
            }
            iOpponAnten_comm = (int)xz;
            // ........................................................................
            // Общая высота ОП

            HeightOpponent_comm = iMiddleHeight_comm + iOpponAnten_comm;
            HeightOpponent2_comm = iMiddleHeight2_comm + iOpponAnten_comm;
            // ........................................................................

            // ************************************************************* !!! Высоты

            // Расчет зоны ************************************************************

            // определить ДПВ
            iDSR = CountDSR1((int)HeightTotalOwn_comm, (int)OwnHeight_comm, iMiddleHeight_comm, iMiddleHeight_comm + iOpponAnten_comm);
            iDSR2 = CountDSR2((int)HeightTotalOwn2_comm, (int)OwnHeight2_comm, iMiddleHeight2_comm, iMiddleHeight2_comm + iOpponAnten_comm);

            // рассчитать точки полигона
            CountPointLSR1(
                          GlobalVarLn.tpOwnCoordRect1_ZPV,
                          (int)HeightTotalOwn_comm,
                          (int)HeightAntennOwn_comm,
                          (double)iDSR,
                          iOpponAnten_comm
                         );

            CountPointLSR2(
                          GlobalVarLn.tpOwnCoordRect2_ZPV,
                          (int)HeightTotalOwn2_comm,
                          (int)HeightAntennOwn_comm,
                          (double)iDSR2,
                          iOpponAnten_comm
                         );

            // DRAW
            ClassMap.DrawLSR_Z1();
            ClassMap.DrawLSR_Z2();

            // ************************************************************ Расчет зоны

            // ******************************************************** Calculation

        } // PARA
        // PARA PARA PARA PARA PARA PARA PARA PARA PARA PARA PARA  Сопряженная пара


        // ************************************************************************
        // Обработчик ComboBox "cbChooseSC": Выбор СК
        // ************************************************************************
        private void cbChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChooseSystemCoord_ZPV(cbChooseSC.SelectedIndex);

        } // SK
        // ************************************************************************


// FUNCTIONS_MY *********************************************************************


        // ************************************************************************
        // функция выбора системы координат
        // ************************************************************************

        private void ChooseSystemCoord_ZPV(int iSystemCoord)
        {
            gbOwnRect.Visible = false;
            gbOwnRect42.Visible = false;
            gbOwnRad.Visible = false;
            gbOwnDegMin.Visible = false;
            gbOwnDegMinSec.Visible = false;

            switch (iSystemCoord)
            {
                case 0: // Метры на местности

                    gbOwnRect.Visible = true;
                    gbOwnRect.Location = new Point(6, 9);

                    if (GlobalVarLn.flCoordZPV == 1)
                    {

                        //333
                        //ichislo = (long)(GlobalVarLn.XCenter_ZPV);
                        //tbXRect.Text = Convert.ToString(ichislo);
                        //ichislo = (long)(GlobalVarLn.YCenter_ZPV);
                        //tbYRect.Text = Convert.ToString(ichislo);

                        ichislo = (long)(GlobalVarLn.BWGS84_ZPV * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        GlobalVarLn.ObjFormLineSightRangeG.tbXRect.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LWGS84_ZPV * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        GlobalVarLn.ObjFormLineSightRangeG.tbYRect.Text = Convert.ToString(dchislo);


                    } // IF

                    break;

                case 1: // Метры 1942 года

                    gbOwnRect42.Visible = true;
                    gbOwnRect42.Location = new Point(6, 9);

                    if (GlobalVarLn.flCoordZPV == 1)
                    {
                        ichislo = (long)(GlobalVarLn.XSP42_comm_ZPV);
                        tbXRect42.Text = Convert.ToString(ichislo);
                        ichislo = (long)(GlobalVarLn.YSP42_comm_ZPV);
                        tbYRect42.Text = Convert.ToString(ichislo);

                    } // IF

                    break;

                case 2: // Радианы (Красовский)

                    gbOwnRad.Visible = true;
                    gbOwnRad.Location = new Point(6, 9);

                    if (GlobalVarLn.flCoordZPV == 1)
                    {

                        ichislo = (long)(GlobalVarLn.LatKrR_comm_ZPV * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBRad.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrR_comm_ZPV * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLRad.Text = Convert.ToString(dchislo);

                    } // IF

                    break;

                case 3: // Градусы (Красовский)

                    gbOwnDegMin.Visible = true;
                    gbOwnDegMin.Location = new Point(6, 9);

                    if (GlobalVarLn.flCoordZPV == 1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrG_comm_ZPV * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBMin1.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrG_comm_ZPV * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLMin1.Text = Convert.ToString(dchislo);

                    } // IF

                    break;

                case 4: // Градусы,мин,сек (Красовский)

                    gbOwnDegMinSec.Visible = true;
                    gbOwnDegMinSec.Location = new Point(6, 9);

                    if (GlobalVarLn.flCoordZPV == 1)
                    {
                        tbBDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_comm_ZPV);
                        tbBMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_comm_ZPV);
                        ichislo = (long)(GlobalVarLn.Lat_Sec_comm_ZPV);
                        tbBSec.Text = Convert.ToString(ichislo);
                        tbLDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_comm_ZPV);
                        tbLMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_comm_ZPV);
                        ichislo = (long)(GlobalVarLn.Long_Sec_comm_ZPV);
                        tbLSec.Text = Convert.ToString(ichislo);

                    } // IF

                    break;

                default:
                    break;

            } // SWITCH

        } // ChooseSystemCoord_ZPV
        // ************************************************************************


// ********************************************************************* FUNCTIONS_MY


// FUNCTIONS_ZPV ********************************************************************

        // ************************************************************************
        // Функция определения средней высота местности
        // ************************************************************************

        private int DefineMiddleHeight_Comm(Point tpReferencePoint, axMapPoint axMapPointTemp, AxaxcMapScreen AxaxcMapScreenTemp)
        {
            int iRadius = 30000;

            int iStep = 100;
            int iCount = 0;
            double dMiddleHeightStep = 0;
            int iMiddleHeight = 0;

            if ((tpReferencePoint.X > 0) & (tpReferencePoint.Y > 0))
            {
                int iMinX = 0;
                int iMinY = 0;
                int iMaxX = 0;
                int iMaxY = 0;

                iMinX = tpReferencePoint.X - iRadius;
                iMinY = tpReferencePoint.Y - iRadius;
                iMaxX = tpReferencePoint.X + iRadius;
                iMaxY = tpReferencePoint.Y + iRadius;

                // пройти по координатам карты с шагом Shag
                for (int i = iMinX; i < iMaxX; i = i + iStep)
                {
                    for (int j = iMinY; j < iMaxY; j = j + iStep)
                    {
                        double dSetX = 0;
                        double dSetY = 0;
                        dSetX = i;
                        dSetY = j;

                        axMapPointTemp.SetPoint(dSetX, dSetY);
                        dMiddleHeightStep = AxaxcMapScreenTemp.PointHeight_get(axMapPointTemp);

                        // 6_9_18
                        if (dMiddleHeightStep < 0)
                        {
                            dMiddleHeightStep = 0;
                        }

                        // увеличить счетчик на 1
                        iCount++;

                        // суммировать высоты
                        iMiddleHeight = (int)((double)iMiddleHeight + dMiddleHeightStep);

                    }
                }

                // средняя высота = сумма всех полученных высот/на кол-во пройденных точек     
                iMiddleHeight = (int)((double)iMiddleHeight / (double)iCount);

                if (iMiddleHeight < 0)
                    iMiddleHeight = 0;

            } // IF

            return iMiddleHeight;

        } // MiddleHeight
        // ************************************************************************


        // ZPV_GENERAL !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


        // ************************************************************************
        // функция расчета ДПВ
        // ************************************************************************

        public int CountDSR(Point p)
        {
            var iOpponAntenComm = iOpponAnten_comm;
            var heightAntennOwnComm = HeightAntennOwn_comm;
            var heightTotalOwnComm = HeightTotalOwn_comm;
            var iMiddleHeightComm = iMiddleHeight_comm;
            var OwnHeightComm = OwnHeight_comm;
            var HeightOpponentComm = HeightOpponent_comm;

            return CountDSR((int)heightTotalOwnComm, (int)OwnHeightComm, (int)iMiddleHeightComm, (int)HeightOpponentComm);

        }

        private int CountDSR(int iHeightTotalOwn, int iHeightOwnObj, int iHeightMiddle, int iHeightOpponentObj)
        {
            int iDSR = 0;
            int h1 = 0;
            int h2 = 0;

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if (iHeightMiddle < iHeightOwnObj)
            {
                h1 = iHeightTotalOwn - iHeightMiddle;
                h2 = iHeightOpponentObj - iHeightMiddle; // Hop=Hsredn+Hant

            } // iHeightMiddle < iHeightOwnObj

            else // iHeightMiddle > iHeightOwnObj
            {
                h1 = iHeightTotalOwn - iHeightOwnObj;
                h2 = iHeightOpponentObj - iHeightOwnObj; // Hop=Hsredn+Hant

            } // iHeightMiddle > iHeightOwnObj
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            iDSR = (int)(4.12 * (Math.Pow(h1, 0.5) + Math.Pow(h2, 0.5)));

            iDSR = iDSR * 1000;

            return iDSR;

        } // ДПВ
        // ************************************************************************

        // ************************************************************************
        // функция расчета точек ЗПВ
        // ************************************************************************

        void CountPointLSR(Point tpCenterLSR, 
                           int iHeightCenterLSR, // HeightTotalOwn_comm
                           int iHeightAnten,     //HeightAntennOwn_comm
                           double dDSR, 
                           int iHeightAntenOpponent) // iOpponAnten_comm
        {
            var points = CreateLineSightPolygon(tpCenterLSR);
            GlobalVarLn.listPointDSR.AddRange(points);
        }

       // --------------------------------------------------------------------------
        public List<Point> CreateLineSightPolygon(Point tpCenterLSR, int maxRadius = int.MaxValue)
        {
            var dsr = CountDSR(tpCenterLSR);

            var heightAntennOwnComm = HeightAntennOwn_comm;
            var heightTotalOwnComm = HeightTotalOwn_comm;
            var iOpponAntenComm = iOpponAnten_comm;


            var points = CreateLineSightPolygon(tpCenterLSR, (int)heightTotalOwnComm, dsr, iOpponAntenComm);

            for (int i = 0; i < points.Count; i++)
            {
                var p = points[i];

                double dx = p.X - tpCenterLSR.X;
                double dy = p.Y - tpCenterLSR.Y;

                var distance = Math.Sqrt(dx * dx + dy * dy);
                distance = Math.Min(distance, maxRadius);

                var x = tpCenterLSR.X + Math.Sin(i * Math.PI / 180) * distance;
                var y = tpCenterLSR.Y + Math.Cos(i * Math.PI / 180) * distance;

                points[i] = new Point((int)x, (int)y);
            }

            return points;
        }
        // --------------------------------------------------------------------------
        // 0809
        // 27_09_2018
        public List<Point> CreateLineSightPolygon(Point tpCenterLSR, 
                                                 int iHeightCenterLSR, // HeightTotalOwn_comm
                                                 double dDSR,
                                                 int iHeightAntenOpponent) // iOpponAnten_comm
        {
            var listKoordReal = new List<Point>();

            double h_Dob;
            double dH;
            double alfa;
            double dL;
            double VarL;
            double H_Line;
            int angleFi;

            // otl***
            //String strFileName;
            //strFileName = "RLF.txt";
            //StreamWriter srFile;
            //srFile = new StreamWriter(strFileName);

            // otl***
            //srFile.WriteLine("DSR =" + Convert.ToString(dDSR));

            // FORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFO
            // пройти в цикле по всем углам   (против часовой стрелки, 1 град)

            for (angleFi = 0; angleFi < 361; angleFi = angleFi + GlobalVarLn.iStepAngleInput_ZPV)
            {

                // otl***
                //srFile.WriteLine("Fi =" + Convert.ToString(angleFi));

                // .................................................................................
                // найти координаты точки реальной ДПВ (от центра на расстоянии ДПВ)

                KoordThree koord2;

                koord2.y = tpCenterLSR.Y + dDSR * Math.Cos((angleFi * Math.PI) / 180);
                koord2.x = tpCenterLSR.X + dDSR * Math.Sin((angleFi * Math.PI) / 180);

                var dSetX = koord2.x;
                var dSetY = koord2.y;

                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                koord2.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                //GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetY, dSetX);
                // koord2.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                if (koord2.h < 0)
                    koord2.h = 0;

                // антенна ОП
                koord2.h += iHeightAntenOpponent;
                // .................................................................................
                // otl***
                //srFile.WriteLine("(1)" + Convert.ToString(iHeightCenterLSR));
                //srFile.WriteLine("HC =" + Convert.ToString(iHeightCenterLSR));
                //srFile.WriteLine("H2 =" + Convert.ToString(koord2.h));
                // .................................................................................
                // изначально координаты Koord4 равны координатам точки реальной ДПВ

                KoordThree koord4;
                koord4.x = koord2.x;
                koord4.y = koord2.y;
                koord4.h = koord2.h;
                // .................................................................................

                // если значение высоты СП совпадает со значением высоты
                // точки реальной ДПВ
                if (Math.Abs(iHeightCenterLSR - koord2.h) < 1e-3)
                {
                    // увеличить первую на 2 м.
                    iHeightCenterLSR = iHeightCenterLSR + 2;
                }
                // .................................................................................

                // Hsp>Hdpv IF1********************************************************************     
                // если высота СП больше высоты точки реальной ДПВ

                // IF1
                KoordThree koord3;
                KoordThree koordPrev;

                // 27_09_2018
                KoordThree koordPrev1;

                if (iHeightCenterLSR > koord2.h)
                {
                    // -----------------------------------------------------------------------------
                    // otl***
                    //srFile.WriteLine("(2) FIRST");

                    // разница высот (см. рис)
                    dH = iHeightCenterLSR - koord2.h;

                    // угол альфа (см. рис)
                    //alfa = Math.Asin(dH / dDSR);
                    alfa = Math.Atan(dH / dDSR);

                    // координаты текущей точки, удаленной от СП на STEP_LENGTH метров
                    // и отклоненной на angle_fi угол от 0
                    koord3.y = tpCenterLSR.Y + GlobalVarLn.iStepLengthInput_ZPV * Math.Cos((angleFi * Math.PI) / 180);
                    koord3.x = tpCenterLSR.X + GlobalVarLn.iStepLengthInput_ZPV * Math.Sin((angleFi * Math.PI) / 180);
                    dSetX = koord3.x;
                    dSetY = koord3.y;
                    GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                    koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                    if (koord3.h < 0)
                        koord3.h = 0;

                    // расстояние от СП до текущей точки (см. рис)
                    dL = Math.Sqrt((tpCenterLSR.X - koord3.x) * (tpCenterLSR.X - koord3.x) +
                                   (tpCenterLSR.Y - koord3.y) * (tpCenterLSR.Y - koord3.y));

                    // otl***
                    //srFile.WriteLine("(3)");
                    //srFile.WriteLine("dL =" + Convert.ToString(dL));
                    //srFile.WriteLine("A =" + Convert.ToString(alfa));
                    //srFile.WriteLine("dH =" + Convert.ToString(dH));
                    //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));
                    // -----------------------------------------------------------------------------

                    // WHILE1 ----------------------------------------------------------------------
                    // пока не достигнута реальная ДПВ     

                    // WHILE1
                    while (dL < dDSR)
                    {
                        // (см. рис)
                        VarL = dDSR - dL;

                        // высота воображаемой линии в точки с координатами Koord3
                        //H_Line = VarL * Math.Sin(alfa) + koord2.h;
                        H_Line = VarL * Math.Tan(alfa) + koord2.h;

                        h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                        koord3.h = koord3.h + h_Dob;

                        // 27_09_2018
                        koordPrev1 = koord3;

                        // otl***
                        //srFile.WriteLine("(4)");
                        //srFile.WriteLine("HLine =" + Convert.ToString(H_Line));
                        //srFile.WriteLine("A =" + Convert.ToString(alfa));
                        //srFile.WriteLine("HDob =" + Convert.ToString(h_Dob));
                        //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        // IF2 .....................................................................
                        // если значение высоты воображаемой линии меньше поверхности земли в этой точке

                        // IF2
                        if (koord3.h > H_Line)
                        {
                            koordPrev.x = 0;
                            koordPrev.y = 0;
                            koordPrev.h = 0;

                            // Словили выход рельефа над линией видимости
                            koordPrev = koord3;

                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // otl***
                            //srFile.WriteLine("(5)");
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));
                            //srFile.WriteLine("HPrev =" + Convert.ToString(koordPrev.h));

                            // WHILE2 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                            // WHILE2
                            while (dL < dDSR)
                            {
                                // (см. рис)
                                VarL = dDSR - dL;

                                // расссчитать координаты в этой точке
                                koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                                koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);
                                dSetX = koord3.x;
                                dSetY = koord3.y;
                                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                                koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                                if (koord3.h < 0)
                                    koord3.h = 0;

                                h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                                koord3.h = koord3.h + h_Dob;

                                // 27_09_2018
                                //koordPrev1 = koord3;

                                // otl***
                                //srFile.WriteLine("6");
                                //srFile.WriteLine("dL=" + Convert.ToString(dL));
                                //srFile.WriteLine("H3=" + Convert.ToString(koord3.h));
                                //srFile.WriteLine("HDob=" + Convert.ToString(h_Dob));
                                //srFile.WriteLine("HPrev=" + Convert.ToString(koordPrev.h));

                                // Еще идем вверх
                                if (koord3.h > koordPrev.h)
                                {
                                    koordPrev = koord3;
                                    dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                                    // otl***
                                    //srFile.WriteLine("Dalshe1");

                                    // 27_09_2018
                                    koordPrev1 = koord3;
                                }
                                else // Пошли вниз
                                {
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                    // 1109

                                    // Уже не видим ОП
                                    if ((koordPrev.h - koord3.h) > iHeightAntenOpponent)
                                    {
                                        // 27_09_2018
                                        //koord3 = koordPrev;
                                        koord3 = koordPrev1;

                                        // otl***
                                        //srFile.WriteLine("Exit1");
                                        // выйти из цикла while2
                                        dL = dDSR + 1;
                                    }
                                    else // Еще видим ОП
                                    {
                                        // otl***
                                        //srFile.WriteLine("Exit1");
                                        // выйти из цикла while
                                        //dL = dDSR + 1;
                                        //koordPrev = koord3; // &&&&&&&&&&&

                                        dL = dL + GlobalVarLn.iStepLengthInput_ZPV;
                                        // otl***
                                        // srFile.WriteLine("Dalshe1_1");

                                        // 27_09_2018
                                        koordPrev1 = koord3;
                                    }
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                                } // ELSE

                            } // WHILE2
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> WHILE2

                            // присвоить Koord4 значения текущих координат
                            koord4.x = koord3.x;
                            koord4.y = koord3.y;
                            koord4.h = koord3.h;

                            // выйти из цикла while1
                            dL = dDSR + 1;

                        } // IF2 (высота рельефа больше воображаемой линии)
                        //  ..................................................................... IF2

                        // ELSE по IF2 ..............................................................
                        // если значение высоты воображаемой линии больше или равно
                        // поверхности земли в этой точке

                        else
                        {
                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // расссчитать координаты в этой точке
                            koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                            koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);

                            dSetX = koord3.x;
                            dSetY = koord3.y;

                            GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                            koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                            if (koord3.h < 0)
                                koord3.h = 0;

                            // otl***
                            //rFile.WriteLine("(5_1)");
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));
                            //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        } // ELSE po IF2 (высота воображаемой линии больше или равна рельефу в этой точке)
                        // .............................................................. ELSE по IF2

                    } // конец WHILE1 (dL<dDSR)
                    // ---------------------------------------------------------------------- WHILE1

                    // otl***
                    //srFile.WriteLine("END WHILE: dL<DSR");

                    // записать конечный результат расчета координат
                    // с углом angle_fi
                    listKoordReal.Add(new Point((int)koord4.x, (int)koord4.y));

                    // otl***
                    //srFile.WriteLine("(6)");
                    //srFile.WriteLine("X3 =" + Convert.ToString(koord3.x));
                    //rFile.WriteLine("Y3 =" + Convert.ToString(koord3.y));
                    //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));
                    //srFile.WriteLine("X4 =" + Convert.ToString(koord4.x));
                    //srFile.WriteLine("Y4 =" + Convert.ToString(koord4.y));
                    //srFile.WriteLine("H4 =" + Convert.ToString(koord4.h));

                } // IF1 
                // ******************************************************************* Hsp>Hdpv IF1

                // Hsp<Hdpv ELSE po IF1 ***********************************************************
                // если высота СП меньше высоты точки реальной ДПВ   

                else
                {
                    // разница высот (см. рис)
                    dH = koord2.h - iHeightCenterLSR;

                    // угол альфа (см. рис)
                    //alfa = Math.Asin(dH / dDSR);
                    alfa = Math.Atan(dH / dDSR);

                    // координаты текущей точки, удаленной от СП на len метров
                    // и отклоненной на angle_fi угол от 0
                    koord3.y = tpCenterLSR.Y + GlobalVarLn.iStepLengthInput_ZPV * Math.Cos((angleFi * Math.PI) / 180);
                    koord3.x = tpCenterLSR.X + GlobalVarLn.iStepLengthInput_ZPV * Math.Sin((angleFi * Math.PI) / 180);
                    dSetX = koord3.x;
                    dSetY = koord3.y;
                    GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                    koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                    if (koord3.h < 0)
                        koord3.h = 0;

                    // расстояние от СП до текущей точки (см. рис)
                    dL = Math.Sqrt((tpCenterLSR.X - koord3.x) * (tpCenterLSR.X - koord3.x) +
                                   (tpCenterLSR.Y - koord3.y) * (tpCenterLSR.Y - koord3.y));

                    // otl***
                    //srFile.WriteLine("(7)");
                    //srFile.WriteLine("dH =" + Convert.ToString(dH));
                    //srFile.WriteLine("A =" + Convert.ToString(alfa));
                    //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));
                    //srFile.WriteLine("dL =" + Convert.ToString(dL));

                    // WHILE3 ----------------------------------------------------------------------
                    // пока не достигнута реальная ДПВ     

                    // WHILE3
                    while (dL < dDSR)
                    {
                        // (см. рис)
                        VarL = dL;

                        // высота воображаемой линии в точки с координатами Koord3
                        //H_Line = VarL * Math.Sin(alfa) + koord2.h;
                        //H_Line = VarL * Math.Tan(alfa) + koord2.h;
                        H_Line = VarL * Math.Tan(alfa) + iHeightCenterLSR;

                        h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                        koord3.h = koord3.h + h_Dob;

                        // 27_09_2018
                        koordPrev1 = koord3;

                        // otl***
                        //srFile.WriteLine("(8)");
                        //srFile.WriteLine("HLine =" + Convert.ToString(H_Line));
                        //srFile.WriteLine("HDob =" + Convert.ToString(h_Dob));
                        //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        // IF3 .....................................................................
                        // если значение высоты воображаемой линии меньше поверхности земли в этой точке

                        // IF3
                        // Словили выход рельефа над линией видимости
                        if (koord3.h > H_Line)
                        {

                            koordPrev.x = 0;
                            koordPrev.y = 0;
                            koordPrev.h = 0;

                            koordPrev = koord3;

                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // otl***
                            //srFile.WriteLine("(9)");
                            //srFile.WriteLine("HPrev =" + Convert.ToString(koordPrev.h));
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));

                            // WHILE4 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                            // WHILE4
                            while (dL < dDSR)
                            {
                                VarL = dDSR - dL;  // ???????????????????

                                // расссчитать координаты в этой точке
                                koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                                koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);
                                dSetX = koord3.x;
                                dSetY = koord3.y;
                                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                                koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                                if (koord3.h < 0)
                                    koord3.h = 0;
                                h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                                koord3.h = koord3.h + h_Dob;

                                // 27_09_2018
                                //koordPrev1 = koord3;

                                // otl***
                                //srFile.WriteLine("(10)");
                                //srFile.WriteLine("HDob =" + Convert.ToString(h_Dob));
                                // srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                                // Еще идем вверх
                                if (koord3.h > koordPrev.h)
                                {
                                    koordPrev = koord3;
                                    dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                                    // 27_09_2018
                                    koordPrev1 = koord3;

                                    // otl***
                                    //srFile.WriteLine("dalse2");
                                }
                                else  // Пошли вниз
                                {
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                    // 1109

                                    // Уже не видим ОП
                                    if ((koordPrev.h - koord3.h) > iHeightAntenOpponent)
                                    {
                                        // 27_09_2018
                                        //koord3 = koordPrev;
                                        koord3 = koordPrev1;

                                        // otl***
                                        //srFile.WriteLine("Exit2");
                                        // выйти из цикла while4
                                        dL = dDSR + 1;

                                    }
                                    else // Еще видим ОП
                                    {
                                        // otl***
                                        //srFile.WriteLine("Exit1");
                                        // выйти из цикла while
                                        //dL = dDSR + 1;

                                        dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                                        // 27_09_2018
                                        koordPrev1 = koord3;

                                        // otl***
                                        //srFile.WriteLine("Dalshe2");
                                    }
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                                } // else

                            } // WHILE4 
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> WHILE4

                            // выйти из цикла while3
                            dL = dDSR + 1;

                            // присвоить Koord4 значения текущих координат
                            koord4.x = koord3.x;
                            koord4.y = koord3.y;
                            koord4.h = koord3.h;

                            // otl***
                            //srFile.WriteLine("(11)");
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));
                            //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        } // IF3 если значение высоты воображаемой линии меньше поверхности земли в этой точке
                        // ..................................................................... IF3

                        // ELSE po IF3 .............................................................
                        // если значение высоты воображаемой линии блольше или равно
                        // поверхности земли в этой точке   
                        else
                        {
                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // расссчитать координаты в этой точке
                            koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                            koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);
                            dSetX = koord3.x;
                            dSetY = koord3.y;
                            GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                            koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                            if (koord3.h < 0)
                                koord3.h = 0;

                            // otl***
                            // srFile.WriteLine("(12)");
                            // srFile.WriteLine("dL =" + Convert.ToString(dL));
                            // srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        } // Else po IF3
                        // ............................................................. ELSE po IF3

                    } // конец while3 (dL<dDSR)
                    // ---------------------------------------------------------------------- WHILE3

                    // записать конечный результат расчета координат
                    // с углом angle_fi
                    listKoordReal.Add(new Point((int)koord4.x, (int)koord4.y));

                } // ELSE po IF1 (Hsp<Hdpv)
                // *********************************************************** Hsp<Hdpv ELSE po IF1

            } // конец  for (angle_fi=0; angle_fi<361; angle_fi = angle_fi+STEP_ANGLE,j++ )   
            // FORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFO

            //srFile.Close();

            return listKoordReal;

        } // ZPV
        // ************************************************************************

        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ZPV_GENERAL

        // ZPV1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        // ************************************************************************
        // функция расчета ДПВ
        // ************************************************************************

        public int CountDSR1(Point p)
        {
            var iOpponAntenComm = iOpponAnten_comm;
            var heightAntennOwnComm = HeightAntennOwn_comm;
            var heightTotalOwnComm = HeightTotalOwn_comm;
            var iMiddleHeightComm = iMiddleHeight_comm;
            var OwnHeightComm = OwnHeight_comm;
            var HeightOpponentComm = HeightOpponent_comm;

            return CountDSR1((int)heightTotalOwnComm, (int)OwnHeightComm, (int)iMiddleHeightComm, (int)HeightOpponentComm);

        }

        private int CountDSR1(int iHeightTotalOwn, int iHeightOwnObj, int iHeightMiddle, int iHeightOpponentObj)
        {
            int iDSR = 0;
            int h1 = 0;
            int h2 = 0;

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if (iHeightMiddle < iHeightOwnObj)
            {
                h1 = iHeightTotalOwn - iHeightMiddle;
                h2 = iHeightOpponentObj - iHeightMiddle; // Hop=Hsredn+Hant

            } // iHeightMiddle < iHeightOwnObj

            else // iHeightMiddle > iHeightOwnObj
            {
                h1 = iHeightTotalOwn - iHeightOwnObj;
                h2 = iHeightOpponentObj - iHeightOwnObj; // Hop=Hsredn+Hant

            } // iHeightMiddle > iHeightOwnObj
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            iDSR = (int)(4.12 * (Math.Pow(h1, 0.5) + Math.Pow(h2, 0.5)));

            iDSR = iDSR * 1000;

            return iDSR;

        } // ДПВ
        // ************************************************************************

        // ************************************************************************
        // функция расчета точек ЗПВ
        // ************************************************************************

        void CountPointLSR1(Point tpCenterLSR,
                           int iHeightCenterLSR, // HeightTotalOwn_comm
                           int iHeightAnten,     //HeightAntennOwn_comm
                           double dDSR,
                           int iHeightAntenOpponent) // iOpponAnten_comm
        {
            var points = CreateLineSightPolygon1(tpCenterLSR);
            GlobalVarLn.listPointDSR1.AddRange(points);
        }

        // --------------------------------------------------------------------------
        public List<Point> CreateLineSightPolygon1(Point tpCenterLSR, int maxRadius = int.MaxValue)
        {
            var dsr = CountDSR1(tpCenterLSR);

            var heightAntennOwnComm = HeightAntennOwn_comm;
            var heightTotalOwnComm = HeightTotalOwn_comm;
            var iOpponAntenComm = iOpponAnten_comm;


            var points = CreateLineSightPolygon1(tpCenterLSR, (int)heightTotalOwnComm, dsr, iOpponAntenComm);

            for (int i = 0; i < points.Count; i++)
            {
                var p = points[i];

                double dx = p.X - tpCenterLSR.X;
                double dy = p.Y - tpCenterLSR.Y;

                var distance = Math.Sqrt(dx * dx + dy * dy);
                distance = Math.Min(distance, maxRadius);

                var x = tpCenterLSR.X + Math.Sin(i * Math.PI / 180) * distance;
                var y = tpCenterLSR.Y + Math.Cos(i * Math.PI / 180) * distance;

                points[i] = new Point((int)x, (int)y);
            }

            return points;
        }
        // --------------------------------------------------------------------------
        // 0809

        public List<Point> CreateLineSightPolygon1(Point tpCenterLSR,
                                                 int iHeightCenterLSR, // HeightTotalOwn_comm
                                                 double dDSR,
                                                 int iHeightAntenOpponent) // iOpponAnten_comm
        {
            var listKoordReal = new List<Point>();

            double h_Dob;
            double dH;
            double alfa;
            double dL;
            double VarL;
            double H_Line;
            int angleFi;

            // пройти в цикле по всем углам   
            for (angleFi = 0; angleFi < 361; angleFi = angleFi + GlobalVarLn.iStepAngleInput_ZPV)
            {

                // найти координаты точки реальной ДПВ
                KoordThree koord2;

                koord2.y = tpCenterLSR.Y + dDSR * Math.Cos((angleFi * Math.PI) / 180);
                koord2.x = tpCenterLSR.X + dDSR * Math.Sin((angleFi * Math.PI) / 180);

                var dSetX = koord2.x;
                var dSetY = koord2.y;

                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                koord2.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                koord2.h += iHeightAntenOpponent;

                // изначально координаты Koord4 равны
                // координатам точки реальной ДПВ
                KoordThree koord4;
                koord4.x = koord2.x;
                koord4.y = koord2.y;
                koord4.h = koord2.h;

                // если значение высоты СП совпадает со значением высоты
                // точки реальной ДПВ
                if (Math.Abs(iHeightCenterLSR - koord2.h) < 1e-3)
                {
                    // увеличить первую на 2 м.
                    iHeightCenterLSR = iHeightCenterLSR + 2;
                }

                // если высота СП больше высоты точки реальной ДПВ
                KoordThree koord3;
                KoordThree koordPrev;
                if (iHeightCenterLSR > koord2.h)
                {

                    // разница высот (см. рис)
                    dH = iHeightCenterLSR - koord2.h;

                    // угол альфа (см. рис)
                    alfa = Math.Atan(dH / dDSR);

                    // координаты текущей точки, удаленной от СП на STEP_LENGTH метров
                    // и отклоненной на angle_fi угол от 0
                    koord3.y = tpCenterLSR.Y + GlobalVarLn.iStepLengthInput_ZPV * Math.Cos((angleFi * Math.PI) / 180);
                    koord3.x = tpCenterLSR.X + GlobalVarLn.iStepLengthInput_ZPV * Math.Sin((angleFi * Math.PI) / 180);

                    dSetX = koord3.x;
                    dSetY = koord3.y;

                    GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                    koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                    // расстояние от СП до текущей точки (см. рис)
                    dL = Math.Sqrt((tpCenterLSR.X - koord3.x) * (tpCenterLSR.X - koord3.x) +
                                   (tpCenterLSR.Y - koord3.y) * (tpCenterLSR.Y - koord3.y));

                    // пока не достигнута реальная ДПВ     
                    while (dL < dDSR)
                    {
                        // (см. рис)
                        VarL = dDSR - dL;

                        // высота воображаемой линии в точки с координатами Koord3
                        H_Line = VarL * Math.Tan(alfa) + koord2.h;

                        h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                        koord3.h = koord3.h + h_Dob;

                        // если значение высоты воображаемой линии больше
                        // поверхности земли в этой точке
                        if (koord3.h > H_Line)
                        {

                            koordPrev.x = 0;
                            koordPrev.y = 0;
                            koordPrev.h = 0;

                            koordPrev = koord3;

                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            while (dL < dDSR)
                            {
                                // (см. рис)
                                VarL = dDSR - dL;

                                // расссчитать координаты в этой точке
                                koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                                koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);

                                dSetX = koord3.x;
                                dSetY = koord3.y;

                                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                                koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                                h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                                koord3.h = koord3.h + h_Dob;

                                if (koord3.h > koordPrev.h)
                                {
                                    koordPrev = koord3;
                                    dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                                }
                                else
                                {

                                    if (koordPrev.h - koord3.h > iHeightAntenOpponent)
                                    {
                                        koord3 = koordPrev;
                                        dL = dDSR + 1;

                                    }

                                    else
                                    {
                                        dL = dL + GlobalVarLn.iStepLengthInput_ZPV;
                                    }

                                }
                            }

                            // присвоить Koord4 значения текущих координат
                            koord4.x = koord3.x;
                            koord4.y = koord3.y;
                            koord4.h = koord3.h;

                            // выйти из цикла while
                            dL = dDSR + 1;
                        }

                        // если значение высоты воображаемой линии меньше или равно
                        // поверхности земли в этой точке
                        else
                        {
                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // расссчитать координаты в этой точке
                            koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                            koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);

                            dSetX = koord3.x;
                            dSetY = koord3.y;

                            GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                            koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                        }
                    } // конец while (dL<dDSR)

                    listKoordReal.Add(new Point((int)koord4.x, (int)koord4.y));

                } // конец  if (Koord1.h>Koord2.h)


// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                // если высота СП меньше высоты точки реальной ДПВ   
                else // if (Koord1.h<Koord2.h)
                {
                    // разница высот (см. рис)
                    dH = koord2.h - iHeightCenterLSR;

                    // угол альфа (см. рис)
                    alfa = Math.Atan(dH / dDSR);

                    // координаты текущей точки, удаленной от СП на len метров
                    // и отклоненной на angle_fi угол от 0
                    koord3.y = tpCenterLSR.Y + GlobalVarLn.iStepLengthInput_ZPV * Math.Cos((angleFi * Math.PI) / 180);
                    koord3.x = tpCenterLSR.X + GlobalVarLn.iStepLengthInput_ZPV * Math.Sin((angleFi * Math.PI) / 180);

                    dSetX = koord3.x;
                    dSetY = koord3.y;

                    GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                    koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                    // расстояние от СП до текущей точки (см. рис)
                    dL = Math.Sqrt((tpCenterLSR.X - koord3.x) * (tpCenterLSR.X - koord3.x) +
                                   (tpCenterLSR.Y - koord3.y) * (tpCenterLSR.Y - koord3.y));

                    // пока не достигнута реальная ДПВ
                    while (dL < dDSR)
                    {
                        // (см. рис)
                        VarL = dL;

                        // высота воображаемой линии в точки с координатами Koord3
                        H_Line = VarL * Math.Tan(alfa) + iHeightCenterLSR;

                        h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                        koord3.h = koord3.h + h_Dob;

                        // если значение высоты воображаемой линии больше
                        // поверхности земли в этой точке

                        // &&&
                        if (koord3.h > H_Line)
                        {

                            koordPrev.x = 0;
                            koordPrev.y = 0;
                            koordPrev.h = 0;

                            koordPrev = koord3;

                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            while (dL < dDSR)
                            {
                                VarL = dDSR - dL;

                                // высота воображаемой линии в точки с координатами Koord3

                                // расссчитать координаты в этой точке
                                koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                                koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);

                                dSetX = koord3.x;
                                dSetY = koord3.y;
                                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                                koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                                h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                                koord3.h = koord3.h + h_Dob;

                                if (koord3.h > koordPrev.h)
                                {
                                    koordPrev = koord3;
                                    dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                                }
                                else
                                {


                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                    // 1109

                                    // &&&
                                    if (koordPrev.h - koord3.h > iHeightAntenOpponent)
                                    {
                                        koord3 = koordPrev;
                                        // выйти из цикла while
                                        dL = dDSR + 1;

                                    }

                                    else
                                    {
                                        // &&&
                                        //koordPrev = koord3; // &&&
                                        dL = dL + GlobalVarLn.iStepLengthInput_ZPV;
                                    }
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


                                } // else
                            }


                            // выйти из цикла while
                            dL = dDSR + 1;

                            // присвоить Koord4 значения текущих координат
                            koord4.x = koord3.x;
                            koord4.y = koord3.y;
                            koord4.h = koord3.h;

                        }

                        // если значение высоты воображаемой линии меньше или равно
                        // поверхности земли в этой точке   
                        else
                        {
                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // расссчитать координаты в этой точке
                            koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                            koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);

                            dSetX = koord3.x;
                            dSetY = koord3.y;

                            GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                            koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);


                        }
                    } // конец while (dL<dDSR)


                    // записать конечный результат расчета координат
                    // с углом angle_fi
                    listKoordReal.Add(new Point((int)koord4.x, (int)koord4.y));
                } // конец else// if (Koord1.h>Koord2.h)
            } // конец  for (angle_fi=0; angle_fi<361; angle_fi = angle_fi+STEP_ANGLE,j++ )   


            return listKoordReal;
        } // ZPV1
        // ************************************************************************

        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ZPV1

        // ZPV2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        // ************************************************************************
        // функция расчета ДПВ
        // ************************************************************************

        public int CountDSR2(Point p)
        {
            var iOpponAntenComm = iOpponAnten_comm;
            var heightAntennOwnComm = HeightAntennOwn_comm;
            var heightTotalOwnComm = HeightTotalOwn2_comm;
            var iMiddleHeightComm = iMiddleHeight2_comm;
            var OwnHeightComm = OwnHeight2_comm;
            var HeightOpponentComm = HeightOpponent2_comm;

            return CountDSR2((int)heightTotalOwnComm, (int)OwnHeightComm, (int)iMiddleHeightComm, (int)HeightOpponentComm);

        }

        private int CountDSR2(int iHeightTotalOwn, int iHeightOwnObj, int iHeightMiddle, int iHeightOpponentObj)
        {
            int iDSR = 0;
            int h1 = 0;
            int h2 = 0;

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if (iHeightMiddle < iHeightOwnObj)
            {
                h1 = iHeightTotalOwn - iHeightMiddle;
                h2 = iHeightOpponentObj - iHeightMiddle; // Hop=Hsredn+Hant

            } // iHeightMiddle < iHeightOwnObj

            else // iHeightMiddle > iHeightOwnObj
            {
                h1 = iHeightTotalOwn - iHeightOwnObj;
                h2 = iHeightOpponentObj - iHeightOwnObj; // Hop=Hsredn+Hant

            } // iHeightMiddle > iHeightOwnObj
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            iDSR = (int)(4.12 * (Math.Pow(h1, 0.5) + Math.Pow(h2, 0.5)));

            iDSR = iDSR * 1000;

            return iDSR;

        } // ДПВ
        // ************************************************************************

        // ************************************************************************
        // функция расчета точек ЗПВ
        // ************************************************************************

        void CountPointLSR2(Point tpCenterLSR,
                           int iHeightCenterLSR, // HeightTotalOwn_comm
                           int iHeightAnten,     //HeightAntennOwn_comm
                           double dDSR,
                           int iHeightAntenOpponent) // iOpponAnten_comm
        {
            var points = CreateLineSightPolygon2(tpCenterLSR);
            GlobalVarLn.listPointDSR2.AddRange(points);
        }

        // --------------------------------------------------------------------------
        public List<Point> CreateLineSightPolygon2(Point tpCenterLSR, int maxRadius = int.MaxValue)
        {
            var dsr = CountDSR2(tpCenterLSR);

            var heightAntennOwnComm = HeightAntennOwn_comm;
            var heightTotalOwnComm = HeightTotalOwn2_comm;
            var iOpponAntenComm = iOpponAnten_comm;

            var points = CreateLineSightPolygon2(tpCenterLSR, (int)heightTotalOwnComm, dsr, iOpponAntenComm);

            for (int i = 0; i < points.Count; i++)
            {
                var p = points[i];

                double dx = p.X - tpCenterLSR.X;
                double dy = p.Y - tpCenterLSR.Y;

                var distance = Math.Sqrt(dx * dx + dy * dy);
                distance = Math.Min(distance, maxRadius);

                var x = tpCenterLSR.X + Math.Sin(i * Math.PI / 180) * distance;
                var y = tpCenterLSR.Y + Math.Cos(i * Math.PI / 180) * distance;

                points[i] = new Point((int)x, (int)y);
            }

            return points;
        }
        // --------------------------------------------------------------------------
        // 0809

        public List<Point> CreateLineSightPolygon2(Point tpCenterLSR,
                                                 int iHeightCenterLSR, // HeightTotalOwn_comm
                                                 double dDSR,
                                                 int iHeightAntenOpponent) // iOpponAnten_comm
        {
            var listKoordReal = new List<Point>();

            double h_Dob;
            double dH;
            double alfa;
            double dL;
            double VarL;
            double H_Line;
            int angleFi;

            // пройти в цикле по всем углам   
            for (angleFi = 0; angleFi < 361; angleFi = angleFi + GlobalVarLn.iStepAngleInput_ZPV)
            {

                // найти координаты точки реальной ДПВ
                KoordThree koord2;

                koord2.y = tpCenterLSR.Y + dDSR * Math.Cos((angleFi * Math.PI) / 180);
                koord2.x = tpCenterLSR.X + dDSR * Math.Sin((angleFi * Math.PI) / 180);

                var dSetX = koord2.x;
                var dSetY = koord2.y;

                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                koord2.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                koord2.h += iHeightAntenOpponent;

                // изначально координаты Koord4 равны
                // координатам точки реальной ДПВ
                KoordThree koord4;
                koord4.x = koord2.x;
                koord4.y = koord2.y;
                koord4.h = koord2.h;

                // если значение высоты СП совпадает со значением высоты
                // точки реальной ДПВ
                if (Math.Abs(iHeightCenterLSR - koord2.h) < 1e-3)
                {
                    // увеличить первую на 2 м.
                    iHeightCenterLSR = iHeightCenterLSR + 2;
                }

                // если высота СП больше высоты точки реальной ДПВ
                KoordThree koord3;
                KoordThree koordPrev;
                if (iHeightCenterLSR > koord2.h)
                {

                    // разница высот (см. рис)
                    dH = iHeightCenterLSR - koord2.h;

                    // угол альфа (см. рис)
                    alfa = Math.Atan(dH / dDSR);

                    // координаты текущей точки, удаленной от СП на STEP_LENGTH метров
                    // и отклоненной на angle_fi угол от 0
                    koord3.y = tpCenterLSR.Y + GlobalVarLn.iStepLengthInput_ZPV * Math.Cos((angleFi * Math.PI) / 180);
                    koord3.x = tpCenterLSR.X + GlobalVarLn.iStepLengthInput_ZPV * Math.Sin((angleFi * Math.PI) / 180);

                    dSetX = koord3.x;
                    dSetY = koord3.y;

                    GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                    koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                    // расстояние от СП до текущей точки (см. рис)
                    dL = Math.Sqrt((tpCenterLSR.X - koord3.x) * (tpCenterLSR.X - koord3.x) +
                                   (tpCenterLSR.Y - koord3.y) * (tpCenterLSR.Y - koord3.y));

                    // пока не достигнута реальная ДПВ     
                    while (dL < dDSR)
                    {
                        // (см. рис)
                        VarL = dDSR - dL;

                        // высота воображаемой линии в точки с координатами Koord3
                        H_Line = VarL * Math.Tan(alfa) + koord2.h;

                        h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                        koord3.h = koord3.h + h_Dob;

                        // если значение высоты воображаемой линии больше
                        // поверхности земли в этой точке
                        if (koord3.h > H_Line)
                        {

                            koordPrev.x = 0;
                            koordPrev.y = 0;
                            koordPrev.h = 0;

                            koordPrev = koord3;

                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            while (dL < dDSR)
                            {
                                // (см. рис)
                                VarL = dDSR - dL;

                                // расссчитать координаты в этой точке
                                koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                                koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);

                                dSetX = koord3.x;
                                dSetY = koord3.y;

                                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                                koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                                h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                                koord3.h = koord3.h + h_Dob;

                                if (koord3.h > koordPrev.h)
                                {
                                    koordPrev = koord3;
                                    dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                                }
                                else
                                {

                                    if (koordPrev.h - koord3.h > iHeightAntenOpponent)
                                    {
                                        koord3 = koordPrev;
                                        dL = dDSR + 1;

                                    }

                                    else
                                    {
                                        dL = dL + GlobalVarLn.iStepLengthInput_ZPV;
                                    }

                                }
                            }

                            // присвоить Koord4 значения текущих координат
                            koord4.x = koord3.x;
                            koord4.y = koord3.y;
                            koord4.h = koord3.h;

                            // выйти из цикла while
                            dL = dDSR + 1;
                        }

                        // если значение высоты воображаемой линии меньше или равно
                        // поверхности земли в этой точке
                        else
                        {
                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // расссчитать координаты в этой точке
                            koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                            koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);

                            dSetX = koord3.x;
                            dSetY = koord3.y;

                            GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                            koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                        }
                    } // конец while (dL<dDSR)

                    listKoordReal.Add(new Point((int)koord4.x, (int)koord4.y));

                } // конец  if (Koord1.h>Koord2.h)


// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                // если высота СП меньше высоты точки реальной ДПВ   
                else // if (Koord1.h<Koord2.h)
                {
                    // разница высот (см. рис)
                    dH = koord2.h - iHeightCenterLSR;

                    // угол альфа (см. рис)
                    alfa = Math.Atan(dH / dDSR);

                    // координаты текущей точки, удаленной от СП на len метров
                    // и отклоненной на angle_fi угол от 0
                    koord3.y = tpCenterLSR.Y + GlobalVarLn.iStepLengthInput_ZPV * Math.Cos((angleFi * Math.PI) / 180);
                    koord3.x = tpCenterLSR.X + GlobalVarLn.iStepLengthInput_ZPV * Math.Sin((angleFi * Math.PI) / 180);

                    dSetX = koord3.x;
                    dSetY = koord3.y;

                    GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                    koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                    // расстояние от СП до текущей точки (см. рис)
                    dL = Math.Sqrt((tpCenterLSR.X - koord3.x) * (tpCenterLSR.X - koord3.x) +
                                   (tpCenterLSR.Y - koord3.y) * (tpCenterLSR.Y - koord3.y));

                    // пока не достигнута реальная ДПВ
                    while (dL < dDSR)
                    {
                        // (см. рис)
                        VarL = dL;

                        // высота воображаемой линии в точки с координатами Koord3
                        H_Line = VarL * Math.Tan(alfa) + iHeightCenterLSR;

                        h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                        koord3.h = koord3.h + h_Dob;

                        // если значение высоты воображаемой линии больше
                        // поверхности земли в этой точке

                        // &&&
                        if (koord3.h > H_Line)
                        {

                            koordPrev.x = 0;
                            koordPrev.y = 0;
                            koordPrev.h = 0;

                            koordPrev = koord3;

                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            while (dL < dDSR)
                            {
                                VarL = dDSR - dL;

                                // высота воображаемой линии в точки с координатами Koord3

                                // расссчитать координаты в этой точке
                                koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                                koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);

                                dSetX = koord3.x;
                                dSetY = koord3.y;
                                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                                koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                                h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                                koord3.h = koord3.h + h_Dob;

                                if (koord3.h > koordPrev.h)
                                {
                                    koordPrev = koord3;
                                    dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                                }
                                else
                                {


                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                    // 1109

                                    // &&&
                                    if (koordPrev.h - koord3.h > iHeightAntenOpponent)
                                    {
                                        koord3 = koordPrev;
                                        // выйти из цикла while
                                        dL = dDSR + 1;

                                    }

                                    else
                                    {
                                        // &&&
                                        //koordPrev = koord3; // &&&
                                        dL = dL + GlobalVarLn.iStepLengthInput_ZPV;
                                    }
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


                                } // else
                            }


                            // выйти из цикла while
                            dL = dDSR + 1;

                            // присвоить Koord4 значения текущих координат
                            koord4.x = koord3.x;
                            koord4.y = koord3.y;
                            koord4.h = koord3.h;

                        }

                        // если значение высоты воображаемой линии меньше или равно
                        // поверхности земли в этой точке   
                        else
                        {
                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // расссчитать координаты в этой точке
                            koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                            koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);

                            dSetX = koord3.x;
                            dSetY = koord3.y;

                            GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                            koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);


                        }
                    } // конец while (dL<dDSR)


                    // записать конечный результат расчета координат
                    // с углом angle_fi
                    listKoordReal.Add(new Point((int)koord4.x, (int)koord4.y));
                } // конец else// if (Koord1.h>Koord2.h)
            } // конец  for (angle_fi=0; angle_fi<361; angle_fi = angle_fi+STEP_ANGLE,j++ )   


            return listKoordReal;
        } // ZPV2
        // ************************************************************************

        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ZPV2


// ******************************************************************** FUNCTIONS_ZPV



        // ************************************************************************
        // Closing
        // ************************************************************************
        private void FormLineSightRange_FormClosing(object sender, FormClosingEventArgs e)
        {
            GlobalVarLn.f_Open_ObjFormLineSightRange = 0;

            e.Cancel = true;
            Hide();

        } // Closing
        // ************************************************************************

        // ************************************************************************
        // Activated
        // ************************************************************************
        private void FormLineSightRange_Activated(object sender, EventArgs e)
        {

            GlobalVarLn.f_Open_ObjFormLineSightRange = 1;

        } // Activated
        // ************************************************************************

        private void lChooseSC_Click(object sender, EventArgs e)
        {

        }

        private void cbCenterLSR_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i=0;
        }




        private void LanguageChooser()
        {
            var cont = this.Controls;

            if (NumberOfLanguage.Equals(0))
            {
                ChangeLanguageToRu(cont);
                this.Text = "Зона прямой видимости (ЗПВ)";
            }
            if (NumberOfLanguage.Equals(1))
            {
                //ChangeLanguageToEng(cont);
            }
            if (NumberOfLanguage.Equals(2))
            {
                ChangeLanguageToAzer(cont);
                this.Text = "Birbaşa görünüş zonası (BGZ)";
            }
        }

        private void ChangeLanguageToRu(System.Windows.Forms.Control.ControlCollection cont)
        {
            string lang = "ru-RU";
            foreach (System.Windows.Forms.Control cc in cont)
            {
                if (cc is ComboBox)
                {

                    for (int i = 0; i < (cc as ComboBox).Items.Count; i++)
                    {
                        if (i == 0)
                        {
                            string a = resources.GetString(cc.Name + ".Items", new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                        else
                        {
                            string a = resources.GetString(cc.Name + ".Items" + i.ToString(), new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                    }
                }
                //}
                if (cc is Label || cc is Button || cc is RadioButton || cc is TabControl || cc is TabPage || cc is GroupBox)
                {
                    resources.ApplyResources(cc, cc.Name, new CultureInfo(lang));
                    //toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
                }
                if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl || cc is TabPage)
                {
                    ChangeLanguageToRu(cc.Controls);
                }
            }
        }

        private void ChangeLanguageToAzer(System.Windows.Forms.Control.ControlCollection cont)
        {
            string lang = "az-Latn";

            foreach (System.Windows.Forms.Control cc in cont)
            {
                if (cc is ComboBox)
                {

                    for (int i = 0; i < (cc as ComboBox).Items.Count; i++)
                    {
                        if (i == 0)
                        {
                            string a = resources.GetString(cc.Name + ".Items", new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                        else
                        {
                            string a = resources.GetString(cc.Name + ".Items" + i.ToString(), new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                    }
                }
                //}
                if (cc is Label || cc is Button || cc is RadioButton || cc is TabControl || cc is TabPage || cc is GroupBox)
                {
                    resources.ApplyResources(cc, cc.Name, new CultureInfo(lang));
                    //toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
                }
                if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl || cc is DataGridView || cc is TabPage)
                {
                    ChangeLanguageToAzer(cc.Controls);
                }

            }
        }


    } // Class
} // Namespace
