﻿namespace GrozaMap
{
    partial class ZonePowerAvail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.cbCenterLSR = new System.Windows.Forms.ComboBox();
            this.cbCap1 = new System.Windows.Forms.ComboBox();
            this.lCapacity = new System.Windows.Forms.Label();
            this.lSurface = new System.Windows.Forms.Label();
            this.cbSurface = new System.Windows.Forms.ComboBox();
            this.lWidthHindrance = new System.Windows.Forms.Label();
            this.cbWidthHindrance = new System.Windows.Forms.ComboBox();
            this.lCoeffOwn = new System.Windows.Forms.Label();
            this.tbCoeffOwn = new System.Windows.Forms.TextBox();
            this.lPowerOwn = new System.Windows.Forms.Label();
            this.tbPowerOwn = new System.Windows.Forms.TextBox();
            this.grbOwnObject = new System.Windows.Forms.GroupBox();
            this.tbHeightOwnObject = new System.Windows.Forms.TextBox();
            this.lHeightOwnObject = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tbHAnt = new System.Windows.Forms.TextBox();
            this.pCoordPoint = new System.Windows.Forms.Panel();
            this.gbOwnRect42 = new System.Windows.Forms.GroupBox();
            this.tbYRect42 = new System.Windows.Forms.TextBox();
            this.lYRect42 = new System.Windows.Forms.Label();
            this.tbXRect42 = new System.Windows.Forms.TextBox();
            this.lXRect42 = new System.Windows.Forms.Label();
            this.gbOwnDegMinSec = new System.Windows.Forms.GroupBox();
            this.tbLSec = new System.Windows.Forms.TextBox();
            this.tbBSec = new System.Windows.Forms.TextBox();
            this.lMin4 = new System.Windows.Forms.Label();
            this.lLDegMinSec = new System.Windows.Forms.Label();
            this.lMin3 = new System.Windows.Forms.Label();
            this.tbLMin2 = new System.Windows.Forms.TextBox();
            this.tbBMin2 = new System.Windows.Forms.TextBox();
            this.lSec2 = new System.Windows.Forms.Label();
            this.lSec1 = new System.Windows.Forms.Label();
            this.lDeg4 = new System.Windows.Forms.Label();
            this.lDeg3 = new System.Windows.Forms.Label();
            this.tbLDeg2 = new System.Windows.Forms.TextBox();
            this.tbBDeg2 = new System.Windows.Forms.TextBox();
            this.lBDegMinSec = new System.Windows.Forms.Label();
            this.cbOwnObject = new System.Windows.Forms.ComboBox();
            this.gbOwnDegMin = new System.Windows.Forms.GroupBox();
            this.lMin2 = new System.Windows.Forms.Label();
            this.lMin1 = new System.Windows.Forms.Label();
            this.tbLMin1 = new System.Windows.Forms.TextBox();
            this.tbBMin1 = new System.Windows.Forms.TextBox();
            this.lLDegMin = new System.Windows.Forms.Label();
            this.lBDegMin = new System.Windows.Forms.Label();
            this.lCenterLSR = new System.Windows.Forms.Label();
            this.tbOwnHeight = new System.Windows.Forms.TextBox();
            this.lOwnHeight = new System.Windows.Forms.Label();
            this.gbOwnRad = new System.Windows.Forms.GroupBox();
            this.tbLRad = new System.Windows.Forms.TextBox();
            this.lLRad = new System.Windows.Forms.Label();
            this.tbBRad = new System.Windows.Forms.TextBox();
            this.lBRad = new System.Windows.Forms.Label();
            this.gbOwnRect = new System.Windows.Forms.GroupBox();
            this.tbYRect = new System.Windows.Forms.TextBox();
            this.lYRect = new System.Windows.Forms.Label();
            this.tbXRect = new System.Windows.Forms.TextBox();
            this.lXRect = new System.Windows.Forms.Label();
            this.lChooseSC = new System.Windows.Forms.Label();
            this.cbChooseSC = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lTypeCommOpponent = new System.Windows.Forms.Label();
            this.cbTypeCommOpponent = new System.Windows.Forms.ComboBox();
            this.grbCoeffSupOpponent = new System.Windows.Forms.GroupBox();
            this.tbCoeffSupOpponent = new System.Windows.Forms.TextBox();
            this.cbCoeffSupOpponent = new System.Windows.Forms.ComboBox();
            this.lPolarOpponent = new System.Windows.Forms.Label();
            this.cbPolarOpponent = new System.Windows.Forms.ComboBox();
            this.lHeightReceiverOpponent = new System.Windows.Forms.Label();
            this.tbHeightReceiverOpponent = new System.Windows.Forms.TextBox();
            this.lHeightTransmitOpponent = new System.Windows.Forms.Label();
            this.tbHeightTransmitOpponent = new System.Windows.Forms.TextBox();
            this.lWidthSignal = new System.Windows.Forms.Label();
            this.tbWidthSignal = new System.Windows.Forms.TextBox();
            this.lRangeComm = new System.Windows.Forms.Label();
            this.tbRangeComm = new System.Windows.Forms.TextBox();
            this.lCoeffReceivertOpponent = new System.Windows.Forms.Label();
            this.tbCoeffReceiverOpponent = new System.Windows.Forms.TextBox();
            this.lCoeffTransmitOpponent = new System.Windows.Forms.Label();
            this.tbCoeffTransmitOpponent = new System.Windows.Forms.TextBox();
            this.lPowerOpponent = new System.Windows.Forms.Label();
            this.tbPowerOpponent = new System.Windows.Forms.TextBox();
            this.lFreq = new System.Windows.Forms.Label();
            this.tbFreq = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.chbXY = new System.Windows.Forms.CheckBox();
            this.cbHeightOwnObject = new System.Windows.Forms.ComboBox();
            this.tbRadiusZone = new System.Windows.Forms.TextBox();
            this.lRadiusZone = new System.Windows.Forms.Label();
            this.tbMaxDist = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.bAccept = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.grbDetail = new System.Windows.Forms.GroupBox();
            this.tbCorrectHeightOpponent = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbGamma = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbResultHeightOpponent = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbMinHeight = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbMiddleHeight = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbMaxDistance = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbCorrectHeightOwn = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbCoeffHE = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbCoeffQ = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbResultHeightOwn = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.grbOwnObject.SuspendLayout();
            this.pCoordPoint.SuspendLayout();
            this.gbOwnRect42.SuspendLayout();
            this.gbOwnDegMinSec.SuspendLayout();
            this.gbOwnDegMin.SuspendLayout();
            this.gbOwnRad.SuspendLayout();
            this.gbOwnRect.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.grbCoeffSupOpponent.SuspendLayout();
            this.grbDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(513, 227);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.cbCenterLSR);
            this.tabPage1.Controls.Add(this.cbCap1);
            this.tabPage1.Controls.Add(this.lCapacity);
            this.tabPage1.Controls.Add(this.lSurface);
            this.tabPage1.Controls.Add(this.cbSurface);
            this.tabPage1.Controls.Add(this.lWidthHindrance);
            this.tabPage1.Controls.Add(this.cbWidthHindrance);
            this.tabPage1.Controls.Add(this.tbMiddleHeight);
            this.tabPage1.Controls.Add(this.lCoeffOwn);
            this.tabPage1.Controls.Add(this.tbCoeffOwn);
            this.tabPage1.Controls.Add(this.lPowerOwn);
            this.tabPage1.Controls.Add(this.tbPowerOwn);
            this.tabPage1.Controls.Add(this.grbOwnObject);
            this.tabPage1.Controls.Add(this.pCoordPoint);
            this.tabPage1.Controls.Add(this.lChooseSC);
            this.tabPage1.Controls.Add(this.cbChooseSC);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(505, 201);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Средство подавления";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(2, 8);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 13);
            this.label14.TabIndex = 138;
            this.label14.Text = "Центр зоны";
            // 
            // cbCenterLSR
            // 
            this.cbCenterLSR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCenterLSR.FormattingEnabled = true;
            this.cbCenterLSR.Items.AddRange(new object[] {
            "X,Y",
            "АСП",
            "АСП сопр.",
            "ПУ"});
            this.cbCenterLSR.Location = new System.Drawing.Point(70, 5);
            this.cbCenterLSR.Name = "cbCenterLSR";
            this.cbCenterLSR.Size = new System.Drawing.Size(104, 21);
            this.cbCenterLSR.TabIndex = 135;
            // 
            // cbCap1
            // 
            this.cbCap1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCap1.FormattingEnabled = true;
            this.cbCap1.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.cbCap1.Location = new System.Drawing.Point(424, 122);
            this.cbCap1.Name = "cbCap1";
            this.cbCap1.Size = new System.Drawing.Size(62, 21);
            this.cbCap1.TabIndex = 101;
            // 
            // lCapacity
            // 
            this.lCapacity.AutoSize = true;
            this.lCapacity.Location = new System.Drawing.Point(188, 121);
            this.lCapacity.Name = "lCapacity";
            this.lCapacity.Size = new System.Drawing.Size(220, 13);
            this.lCapacity.TabIndex = 100;
            this.lCapacity.Text = "Пропуск. способность.................................";
            // 
            // lSurface
            // 
            this.lSurface.AutoSize = true;
            this.lSurface.Location = new System.Drawing.Point(172, 179);
            this.lSurface.Name = "lSurface";
            this.lSurface.Size = new System.Drawing.Size(152, 13);
            this.lSurface.TabIndex = 98;
            this.lSurface.Text = "Подстилающая поверхность";
            // 
            // cbSurface
            // 
            this.cbSurface.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSurface.FormattingEnabled = true;
            this.cbSurface.Items.AddRange(new object[] {
            "Сухая почва",
            "Влажная почва",
            "Пресная вода",
            "Морская вода",
            "Лес"});
            this.cbSurface.Location = new System.Drawing.Point(328, 176);
            this.cbSurface.Name = "cbSurface";
            this.cbSurface.Size = new System.Drawing.Size(158, 21);
            this.cbSurface.TabIndex = 97;
            // 
            // lWidthHindrance
            // 
            this.lWidthHindrance.AutoSize = true;
            this.lWidthHindrance.Location = new System.Drawing.Point(187, 146);
            this.lWidthHindrance.Name = "lWidthHindrance";
            this.lWidthHindrance.Size = new System.Drawing.Size(223, 13);
            this.lWidthHindrance.TabIndex = 96;
            this.lWidthHindrance.Text = "Ширина спектра помехи, кГц.......................";
            // 
            // cbWidthHindrance
            // 
            this.cbWidthHindrance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWidthHindrance.FormattingEnabled = true;
            this.cbWidthHindrance.Items.AddRange(new object[] {
            "3,5",
            "4,94",
            "7",
            "8,4",
            "10",
            "14,14",
            "20",
            "31,6",
            "50",
            "70,7",
            "100",
            "140",
            "200",
            "300",
            "500",
            "700",
            "1000",
            "1400",
            "2000"});
            this.cbWidthHindrance.Location = new System.Drawing.Point(424, 149);
            this.cbWidthHindrance.Name = "cbWidthHindrance";
            this.cbWidthHindrance.Size = new System.Drawing.Size(62, 21);
            this.cbWidthHindrance.TabIndex = 95;
            // 
            // lCoeffOwn
            // 
            this.lCoeffOwn.AutoSize = true;
            this.lCoeffOwn.Location = new System.Drawing.Point(184, 95);
            this.lCoeffOwn.Name = "lCoeffOwn";
            this.lCoeffOwn.Size = new System.Drawing.Size(229, 13);
            this.lCoeffOwn.TabIndex = 92;
            this.lCoeffOwn.Text = "Коэффициент усиления..................................";
            // 
            // tbCoeffOwn
            // 
            this.tbCoeffOwn.Location = new System.Drawing.Point(424, 95);
            this.tbCoeffOwn.Name = "tbCoeffOwn";
            this.tbCoeffOwn.Size = new System.Drawing.Size(60, 20);
            this.tbCoeffOwn.TabIndex = 91;
            this.tbCoeffOwn.Text = "4";
            // 
            // lPowerOwn
            // 
            this.lPowerOwn.AutoSize = true;
            this.lPowerOwn.Location = new System.Drawing.Point(183, 75);
            this.lPowerOwn.Name = "lPowerOwn";
            this.lPowerOwn.Size = new System.Drawing.Size(241, 13);
            this.lPowerOwn.TabIndex = 90;
            this.lPowerOwn.Text = "Мощность передачитка, Вт................................";
            // 
            // tbPowerOwn
            // 
            this.tbPowerOwn.Location = new System.Drawing.Point(424, 72);
            this.tbPowerOwn.Name = "tbPowerOwn";
            this.tbPowerOwn.Size = new System.Drawing.Size(60, 20);
            this.tbPowerOwn.TabIndex = 87;
            this.tbPowerOwn.Text = "1000";
            // 
            // grbOwnObject
            // 
            this.grbOwnObject.Controls.Add(this.tbHeightOwnObject);
            this.grbOwnObject.Controls.Add(this.lHeightOwnObject);
            this.grbOwnObject.Controls.Add(this.label12);
            this.grbOwnObject.Controls.Add(this.tbHAnt);
            this.grbOwnObject.Location = new System.Drawing.Point(187, 7);
            this.grbOwnObject.Name = "grbOwnObject";
            this.grbOwnObject.Size = new System.Drawing.Size(299, 59);
            this.grbOwnObject.TabIndex = 81;
            this.grbOwnObject.TabStop = false;
            this.grbOwnObject.Text = "Средство РП";
            // 
            // tbHeightOwnObject
            // 
            this.tbHeightOwnObject.Location = new System.Drawing.Point(224, 33);
            this.tbHeightOwnObject.MaxLength = 3;
            this.tbHeightOwnObject.Name = "tbHeightOwnObject";
            this.tbHeightOwnObject.Size = new System.Drawing.Size(59, 20);
            this.tbHeightOwnObject.TabIndex = 26;
            // 
            // lHeightOwnObject
            // 
            this.lHeightOwnObject.AutoSize = true;
            this.lHeightOwnObject.Location = new System.Drawing.Point(7, 33);
            this.lHeightOwnObject.Name = "lHeightOwnObject";
            this.lHeightOwnObject.Size = new System.Drawing.Size(95, 13);
            this.lHeightOwnObject.TabIndex = 21;
            this.lHeightOwnObject.Text = "Высота общая, м";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(114, 13);
            this.label12.TabIndex = 103;
            this.label12.Text = "Высота антенны,м....";
            // 
            // tbHAnt
            // 
            this.tbHAnt.Location = new System.Drawing.Point(224, 11);
            this.tbHAnt.Name = "tbHAnt";
            this.tbHAnt.Size = new System.Drawing.Size(59, 20);
            this.tbHAnt.TabIndex = 102;
            this.tbHAnt.Text = "2";
            // 
            // pCoordPoint
            // 
            this.pCoordPoint.Controls.Add(this.gbOwnRect42);
            this.pCoordPoint.Controls.Add(this.gbOwnDegMinSec);
            this.pCoordPoint.Controls.Add(this.cbOwnObject);
            this.pCoordPoint.Controls.Add(this.gbOwnDegMin);
            this.pCoordPoint.Controls.Add(this.lCenterLSR);
            this.pCoordPoint.Controls.Add(this.tbOwnHeight);
            this.pCoordPoint.Controls.Add(this.lOwnHeight);
            this.pCoordPoint.Controls.Add(this.gbOwnRad);
            this.pCoordPoint.Controls.Add(this.gbOwnRect);
            this.pCoordPoint.Location = new System.Drawing.Point(4, 32);
            this.pCoordPoint.Name = "pCoordPoint";
            this.pCoordPoint.Size = new System.Drawing.Size(172, 113);
            this.pCoordPoint.TabIndex = 80;
            // 
            // gbOwnRect42
            // 
            this.gbOwnRect42.Controls.Add(this.tbYRect42);
            this.gbOwnRect42.Controls.Add(this.lYRect42);
            this.gbOwnRect42.Controls.Add(this.tbXRect42);
            this.gbOwnRect42.Controls.Add(this.lXRect42);
            this.gbOwnRect42.Location = new System.Drawing.Point(8, 26);
            this.gbOwnRect42.Name = "gbOwnRect42";
            this.gbOwnRect42.Size = new System.Drawing.Size(153, 61);
            this.gbOwnRect42.TabIndex = 67;
            this.gbOwnRect42.TabStop = false;
            // 
            // tbYRect42
            // 
            this.tbYRect42.Location = new System.Drawing.Point(72, 35);
            this.tbYRect42.MaxLength = 7;
            this.tbYRect42.Name = "tbYRect42";
            this.tbYRect42.Size = new System.Drawing.Size(75, 20);
            this.tbYRect42.TabIndex = 5;
            // 
            // lYRect42
            // 
            this.lYRect42.AutoSize = true;
            this.lYRect42.Location = new System.Drawing.Point(4, 42);
            this.lYRect42.Name = "lYRect42";
            this.lYRect42.Size = new System.Drawing.Size(85, 13);
            this.lYRect42.TabIndex = 6;
            this.lYRect42.Text = "Y, м...................";
            // 
            // tbXRect42
            // 
            this.tbXRect42.Location = new System.Drawing.Point(72, 13);
            this.tbXRect42.MaxLength = 7;
            this.tbXRect42.Name = "tbXRect42";
            this.tbXRect42.Size = new System.Drawing.Size(75, 20);
            this.tbXRect42.TabIndex = 4;
            // 
            // lXRect42
            // 
            this.lXRect42.AutoSize = true;
            this.lXRect42.Location = new System.Drawing.Point(4, 20);
            this.lXRect42.Name = "lXRect42";
            this.lXRect42.Size = new System.Drawing.Size(85, 13);
            this.lXRect42.TabIndex = 3;
            this.lXRect42.Text = "X, м...................";
            // 
            // gbOwnDegMinSec
            // 
            this.gbOwnDegMinSec.Controls.Add(this.tbLSec);
            this.gbOwnDegMinSec.Controls.Add(this.tbBSec);
            this.gbOwnDegMinSec.Controls.Add(this.lMin4);
            this.gbOwnDegMinSec.Controls.Add(this.lLDegMinSec);
            this.gbOwnDegMinSec.Controls.Add(this.lMin3);
            this.gbOwnDegMinSec.Controls.Add(this.tbLMin2);
            this.gbOwnDegMinSec.Controls.Add(this.tbBMin2);
            this.gbOwnDegMinSec.Controls.Add(this.lSec2);
            this.gbOwnDegMinSec.Controls.Add(this.lSec1);
            this.gbOwnDegMinSec.Controls.Add(this.lDeg4);
            this.gbOwnDegMinSec.Controls.Add(this.lDeg3);
            this.gbOwnDegMinSec.Controls.Add(this.tbLDeg2);
            this.gbOwnDegMinSec.Controls.Add(this.tbBDeg2);
            this.gbOwnDegMinSec.Controls.Add(this.lBDegMinSec);
            this.gbOwnDegMinSec.Location = new System.Drawing.Point(6, 29);
            this.gbOwnDegMinSec.Name = "gbOwnDegMinSec";
            this.gbOwnDegMinSec.Size = new System.Drawing.Size(152, 61);
            this.gbOwnDegMinSec.TabIndex = 30;
            this.gbOwnDegMinSec.TabStop = false;
            this.gbOwnDegMinSec.Visible = false;
            // 
            // tbLSec
            // 
            this.tbLSec.Location = new System.Drawing.Point(125, 36);
            this.tbLSec.MaxLength = 3;
            this.tbLSec.Name = "tbLSec";
            this.tbLSec.Size = new System.Drawing.Size(25, 20);
            this.tbLSec.TabIndex = 23;
            // 
            // tbBSec
            // 
            this.tbBSec.Location = new System.Drawing.Point(125, 13);
            this.tbBSec.MaxLength = 3;
            this.tbBSec.Name = "tbBSec";
            this.tbBSec.Size = new System.Drawing.Size(25, 20);
            this.tbBSec.TabIndex = 18;
            // 
            // lMin4
            // 
            this.lMin4.AutoSize = true;
            this.lMin4.Location = new System.Drawing.Point(115, 38);
            this.lMin4.Name = "lMin4";
            this.lMin4.Size = new System.Drawing.Size(9, 13);
            this.lMin4.TabIndex = 27;
            this.lMin4.Text = "\'";
            // 
            // lLDegMinSec
            // 
            this.lLDegMinSec.AutoSize = true;
            this.lLDegMinSec.Location = new System.Drawing.Point(6, 39);
            this.lLDegMinSec.Name = "lLDegMinSec";
            this.lLDegMinSec.Size = new System.Drawing.Size(34, 13);
            this.lLDegMinSec.TabIndex = 17;
            this.lLDegMinSec.Text = "L.......";
            // 
            // lMin3
            // 
            this.lMin3.AutoSize = true;
            this.lMin3.Location = new System.Drawing.Point(115, 14);
            this.lMin3.Name = "lMin3";
            this.lMin3.Size = new System.Drawing.Size(9, 13);
            this.lMin3.TabIndex = 26;
            this.lMin3.Text = "\'";
            // 
            // tbLMin2
            // 
            this.tbLMin2.Location = new System.Drawing.Point(90, 36);
            this.tbLMin2.MaxLength = 2;
            this.tbLMin2.Name = "tbLMin2";
            this.tbLMin2.Size = new System.Drawing.Size(25, 20);
            this.tbLMin2.TabIndex = 22;
            // 
            // tbBMin2
            // 
            this.tbBMin2.Location = new System.Drawing.Point(90, 13);
            this.tbBMin2.MaxLength = 2;
            this.tbBMin2.Name = "tbBMin2";
            this.tbBMin2.Size = new System.Drawing.Size(25, 20);
            this.tbBMin2.TabIndex = 16;
            // 
            // lSec2
            // 
            this.lSec2.AutoSize = true;
            this.lSec2.Location = new System.Drawing.Point(150, 39);
            this.lSec2.Name = "lSec2";
            this.lSec2.Size = new System.Drawing.Size(9, 13);
            this.lSec2.TabIndex = 25;
            this.lSec2.Text = "\'";
            // 
            // lSec1
            // 
            this.lSec1.AutoSize = true;
            this.lSec1.Location = new System.Drawing.Point(150, 15);
            this.lSec1.Name = "lSec1";
            this.lSec1.Size = new System.Drawing.Size(9, 13);
            this.lSec1.TabIndex = 24;
            this.lSec1.Text = "\'";
            // 
            // lDeg4
            // 
            this.lDeg4.AutoSize = true;
            this.lDeg4.Location = new System.Drawing.Point(75, 38);
            this.lDeg4.Name = "lDeg4";
            this.lDeg4.Size = new System.Drawing.Size(13, 13);
            this.lDeg4.TabIndex = 21;
            this.lDeg4.Text = "^";
            // 
            // lDeg3
            // 
            this.lDeg3.AutoSize = true;
            this.lDeg3.Location = new System.Drawing.Point(75, 14);
            this.lDeg3.Name = "lDeg3";
            this.lDeg3.Size = new System.Drawing.Size(13, 13);
            this.lDeg3.TabIndex = 20;
            this.lDeg3.Text = "^";
            // 
            // tbLDeg2
            // 
            this.tbLDeg2.Location = new System.Drawing.Point(50, 36);
            this.tbLDeg2.MaxLength = 2;
            this.tbLDeg2.Name = "tbLDeg2";
            this.tbLDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbLDeg2.TabIndex = 19;
            // 
            // tbBDeg2
            // 
            this.tbBDeg2.Location = new System.Drawing.Point(50, 13);
            this.tbBDeg2.MaxLength = 2;
            this.tbBDeg2.Name = "tbBDeg2";
            this.tbBDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbBDeg2.TabIndex = 15;
            // 
            // lBDegMinSec
            // 
            this.lBDegMinSec.AutoSize = true;
            this.lBDegMinSec.Location = new System.Drawing.Point(4, 18);
            this.lBDegMinSec.Name = "lBDegMinSec";
            this.lBDegMinSec.Size = new System.Drawing.Size(59, 13);
            this.lBDegMinSec.TabIndex = 14;
            this.lBDegMinSec.Text = "B...............";
            // 
            // cbOwnObject
            // 
            this.cbOwnObject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOwnObject.FormattingEnabled = true;
            this.cbOwnObject.Location = new System.Drawing.Point(99, 5);
            this.cbOwnObject.Name = "cbOwnObject";
            this.cbOwnObject.Size = new System.Drawing.Size(65, 21);
            this.cbOwnObject.TabIndex = 65;
            // 
            // gbOwnDegMin
            // 
            this.gbOwnDegMin.Controls.Add(this.lMin2);
            this.gbOwnDegMin.Controls.Add(this.lMin1);
            this.gbOwnDegMin.Controls.Add(this.tbLMin1);
            this.gbOwnDegMin.Controls.Add(this.tbBMin1);
            this.gbOwnDegMin.Controls.Add(this.lLDegMin);
            this.gbOwnDegMin.Controls.Add(this.lBDegMin);
            this.gbOwnDegMin.Location = new System.Drawing.Point(8, 29);
            this.gbOwnDegMin.Name = "gbOwnDegMin";
            this.gbOwnDegMin.Size = new System.Drawing.Size(155, 58);
            this.gbOwnDegMin.TabIndex = 29;
            this.gbOwnDegMin.TabStop = false;
            this.gbOwnDegMin.Visible = false;
            // 
            // lMin2
            // 
            this.lMin2.AutoSize = true;
            this.lMin2.Location = new System.Drawing.Point(150, 36);
            this.lMin2.Name = "lMin2";
            this.lMin2.Size = new System.Drawing.Size(9, 13);
            this.lMin2.TabIndex = 19;
            this.lMin2.Text = "\'";
            // 
            // lMin1
            // 
            this.lMin1.AutoSize = true;
            this.lMin1.Location = new System.Drawing.Point(150, 12);
            this.lMin1.Name = "lMin1";
            this.lMin1.Size = new System.Drawing.Size(9, 13);
            this.lMin1.TabIndex = 18;
            this.lMin1.Text = "\'";
            // 
            // tbLMin1
            // 
            this.tbLMin1.Location = new System.Drawing.Point(63, 36);
            this.tbLMin1.MaxLength = 8;
            this.tbLMin1.Name = "tbLMin1";
            this.tbLMin1.Size = new System.Drawing.Size(81, 20);
            this.tbLMin1.TabIndex = 15;
            // 
            // tbBMin1
            // 
            this.tbBMin1.Location = new System.Drawing.Point(63, 12);
            this.tbBMin1.MaxLength = 8;
            this.tbBMin1.Name = "tbBMin1";
            this.tbBMin1.Size = new System.Drawing.Size(81, 20);
            this.tbBMin1.TabIndex = 13;
            // 
            // lLDegMin
            // 
            this.lLDegMin.AutoSize = true;
            this.lLDegMin.Location = new System.Drawing.Point(4, 41);
            this.lLDegMin.Name = "lLDegMin";
            this.lLDegMin.Size = new System.Drawing.Size(64, 13);
            this.lLDegMin.TabIndex = 12;
            this.lLDegMin.Text = "L.................";
            // 
            // lBDegMin
            // 
            this.lBDegMin.AutoSize = true;
            this.lBDegMin.Location = new System.Drawing.Point(4, 18);
            this.lBDegMin.Name = "lBDegMin";
            this.lBDegMin.Size = new System.Drawing.Size(59, 13);
            this.lBDegMin.TabIndex = 10;
            this.lBDegMin.Text = "B...............";
            // 
            // lCenterLSR
            // 
            this.lCenterLSR.AutoSize = true;
            this.lCenterLSR.Location = new System.Drawing.Point(5, 10);
            this.lCenterLSR.Name = "lCenterLSR";
            this.lCenterLSR.Size = new System.Drawing.Size(97, 13);
            this.lCenterLSR.TabIndex = 66;
            this.lCenterLSR.Text = "СП ........................";
            // 
            // tbOwnHeight
            // 
            this.tbOwnHeight.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbOwnHeight.Location = new System.Drawing.Point(113, 89);
            this.tbOwnHeight.Name = "tbOwnHeight";
            this.tbOwnHeight.Size = new System.Drawing.Size(48, 20);
            this.tbOwnHeight.TabIndex = 64;
            // 
            // lOwnHeight
            // 
            this.lOwnHeight.AutoSize = true;
            this.lOwnHeight.Location = new System.Drawing.Point(9, 96);
            this.lOwnHeight.Name = "lOwnHeight";
            this.lOwnHeight.Size = new System.Drawing.Size(116, 13);
            this.lOwnHeight.TabIndex = 63;
            this.lOwnHeight.Text = "H, м.............................";
            // 
            // gbOwnRad
            // 
            this.gbOwnRad.Controls.Add(this.tbLRad);
            this.gbOwnRad.Controls.Add(this.lLRad);
            this.gbOwnRad.Controls.Add(this.tbBRad);
            this.gbOwnRad.Controls.Add(this.lBRad);
            this.gbOwnRad.Location = new System.Drawing.Point(4, 30);
            this.gbOwnRad.Name = "gbOwnRad";
            this.gbOwnRad.Size = new System.Drawing.Size(160, 59);
            this.gbOwnRad.TabIndex = 28;
            this.gbOwnRad.TabStop = false;
            this.gbOwnRad.Visible = false;
            // 
            // tbLRad
            // 
            this.tbLRad.Location = new System.Drawing.Point(78, 36);
            this.tbLRad.MaxLength = 10;
            this.tbLRad.Name = "tbLRad";
            this.tbLRad.Size = new System.Drawing.Size(75, 20);
            this.tbLRad.TabIndex = 5;
            // 
            // lLRad
            // 
            this.lLRad.AutoSize = true;
            this.lLRad.Location = new System.Drawing.Point(6, 39);
            this.lLRad.Name = "lLRad";
            this.lLRad.Size = new System.Drawing.Size(94, 13);
            this.lLRad.TabIndex = 6;
            this.lLRad.Text = "L, рад...................";
            // 
            // tbBRad
            // 
            this.tbBRad.Location = new System.Drawing.Point(78, 13);
            this.tbBRad.MaxLength = 10;
            this.tbBRad.Name = "tbBRad";
            this.tbBRad.Size = new System.Drawing.Size(75, 20);
            this.tbBRad.TabIndex = 4;
            // 
            // lBRad
            // 
            this.lBRad.AutoSize = true;
            this.lBRad.Location = new System.Drawing.Point(4, 18);
            this.lBRad.Name = "lBRad";
            this.lBRad.Size = new System.Drawing.Size(95, 13);
            this.lBRad.TabIndex = 3;
            this.lBRad.Text = "B, рад...................";
            // 
            // gbOwnRect
            // 
            this.gbOwnRect.Controls.Add(this.tbYRect);
            this.gbOwnRect.Controls.Add(this.lYRect);
            this.gbOwnRect.Controls.Add(this.tbXRect);
            this.gbOwnRect.Controls.Add(this.lXRect);
            this.gbOwnRect.Location = new System.Drawing.Point(8, 26);
            this.gbOwnRect.Name = "gbOwnRect";
            this.gbOwnRect.Size = new System.Drawing.Size(156, 63);
            this.gbOwnRect.TabIndex = 17;
            this.gbOwnRect.TabStop = false;
            this.gbOwnRect.Visible = false;
            // 
            // tbYRect
            // 
            this.tbYRect.Location = new System.Drawing.Point(78, 36);
            this.tbYRect.MaxLength = 7;
            this.tbYRect.Name = "tbYRect";
            this.tbYRect.Size = new System.Drawing.Size(75, 20);
            this.tbYRect.TabIndex = 5;
            // 
            // lYRect
            // 
            this.lYRect.AutoSize = true;
            this.lYRect.Location = new System.Drawing.Point(4, 42);
            this.lYRect.Name = "lYRect";
            this.lYRect.Size = new System.Drawing.Size(85, 13);
            this.lYRect.TabIndex = 6;
            this.lYRect.Text = "Y, м...................";
            // 
            // tbXRect
            // 
            this.tbXRect.Location = new System.Drawing.Point(78, 13);
            this.tbXRect.MaxLength = 7;
            this.tbXRect.Name = "tbXRect";
            this.tbXRect.Size = new System.Drawing.Size(75, 20);
            this.tbXRect.TabIndex = 4;
            // 
            // lXRect
            // 
            this.lXRect.AutoSize = true;
            this.lXRect.Location = new System.Drawing.Point(4, 20);
            this.lXRect.Name = "lXRect";
            this.lXRect.Size = new System.Drawing.Size(85, 13);
            this.lXRect.TabIndex = 3;
            this.lXRect.Text = "X, м...................";
            // 
            // lChooseSC
            // 
            this.lChooseSC.AutoSize = true;
            this.lChooseSC.Location = new System.Drawing.Point(5, 154);
            this.lChooseSC.Name = "lChooseSC";
            this.lChooseSC.Size = new System.Drawing.Size(39, 13);
            this.lChooseSC.TabIndex = 79;
            this.lChooseSC.Text = "СК .....";
            // 
            // cbChooseSC
            // 
            this.cbChooseSC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChooseSC.DropDownWidth = 150;
            this.cbChooseSC.FormattingEnabled = true;
            this.cbChooseSC.Items.AddRange(new object[] {
            "Метры на мест-ти",
            "Метры 1942 г.",
            "Радианы",
            "Градусы",
            "Град, мин, сек"});
            this.cbChooseSC.Location = new System.Drawing.Point(51, 151);
            this.cbChooseSC.Name = "cbChooseSC";
            this.cbChooseSC.Size = new System.Drawing.Size(123, 21);
            this.cbChooseSC.TabIndex = 78;
            this.cbChooseSC.SelectedIndexChanged += new System.EventHandler(this.cbChooseSC_SelectedIndexChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.lTypeCommOpponent);
            this.tabPage2.Controls.Add(this.cbTypeCommOpponent);
            this.tabPage2.Controls.Add(this.grbCoeffSupOpponent);
            this.tabPage2.Controls.Add(this.lPolarOpponent);
            this.tabPage2.Controls.Add(this.cbPolarOpponent);
            this.tabPage2.Controls.Add(this.lHeightReceiverOpponent);
            this.tabPage2.Controls.Add(this.tbHeightReceiverOpponent);
            this.tabPage2.Controls.Add(this.lHeightTransmitOpponent);
            this.tabPage2.Controls.Add(this.tbHeightTransmitOpponent);
            this.tabPage2.Controls.Add(this.lWidthSignal);
            this.tabPage2.Controls.Add(this.tbWidthSignal);
            this.tabPage2.Controls.Add(this.lRangeComm);
            this.tabPage2.Controls.Add(this.tbRangeComm);
            this.tabPage2.Controls.Add(this.lCoeffReceivertOpponent);
            this.tabPage2.Controls.Add(this.tbCoeffReceiverOpponent);
            this.tabPage2.Controls.Add(this.lCoeffTransmitOpponent);
            this.tabPage2.Controls.Add(this.tbCoeffTransmitOpponent);
            this.tabPage2.Controls.Add(this.lPowerOpponent);
            this.tabPage2.Controls.Add(this.tbPowerOpponent);
            this.tabPage2.Controls.Add(this.lFreq);
            this.tabPage2.Controls.Add(this.tbFreq);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(505, 201);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Подавляемая линия";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // lTypeCommOpponent
            // 
            this.lTypeCommOpponent.AutoSize = true;
            this.lTypeCommOpponent.Location = new System.Drawing.Point(347, 119);
            this.lTypeCommOpponent.Name = "lTypeCommOpponent";
            this.lTypeCommOpponent.Size = new System.Drawing.Size(59, 13);
            this.lTypeCommOpponent.TabIndex = 129;
            this.lTypeCommOpponent.Text = "Вид связи";
            // 
            // cbTypeCommOpponent
            // 
            this.cbTypeCommOpponent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeCommOpponent.FormattingEnabled = true;
            this.cbTypeCommOpponent.Items.AddRange(new object[] {
            "АМ",
            "ЧМ",
            "ОМ",
            "ЧМн",
            "ФМн",
            "АМн",
            "ФМн ШПС"});
            this.cbTypeCommOpponent.Location = new System.Drawing.Point(411, 114);
            this.cbTypeCommOpponent.Name = "cbTypeCommOpponent";
            this.cbTypeCommOpponent.Size = new System.Drawing.Size(91, 21);
            this.cbTypeCommOpponent.TabIndex = 128;
            // 
            // grbCoeffSupOpponent
            // 
            this.grbCoeffSupOpponent.Controls.Add(this.tbCoeffSupOpponent);
            this.grbCoeffSupOpponent.Controls.Add(this.cbCoeffSupOpponent);
            this.grbCoeffSupOpponent.Location = new System.Drawing.Point(6, 143);
            this.grbCoeffSupOpponent.Name = "grbCoeffSupOpponent";
            this.grbCoeffSupOpponent.Size = new System.Drawing.Size(237, 46);
            this.grbCoeffSupOpponent.TabIndex = 127;
            this.grbCoeffSupOpponent.TabStop = false;
            this.grbCoeffSupOpponent.Text = "Коэфффициент подавления";
            // 
            // tbCoeffSupOpponent
            // 
            this.tbCoeffSupOpponent.Location = new System.Drawing.Point(176, 18);
            this.tbCoeffSupOpponent.MaxLength = 3;
            this.tbCoeffSupOpponent.Name = "tbCoeffSupOpponent";
            this.tbCoeffSupOpponent.Size = new System.Drawing.Size(51, 20);
            this.tbCoeffSupOpponent.TabIndex = 96;
            // 
            // cbCoeffSupOpponent
            // 
            this.cbCoeffSupOpponent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCoeffSupOpponent.FormattingEnabled = true;
            this.cbCoeffSupOpponent.Items.AddRange(new object[] {
            "Автоматический расчет",
            "Задать самостоятельно"});
            this.cbCoeffSupOpponent.Location = new System.Drawing.Point(6, 18);
            this.cbCoeffSupOpponent.Name = "cbCoeffSupOpponent";
            this.cbCoeffSupOpponent.Size = new System.Drawing.Size(148, 21);
            this.cbCoeffSupOpponent.TabIndex = 94;
            // 
            // lPolarOpponent
            // 
            this.lPolarOpponent.AutoSize = true;
            this.lPolarOpponent.Location = new System.Drawing.Point(9, 116);
            this.lPolarOpponent.Name = "lPolarOpponent";
            this.lPolarOpponent.Size = new System.Drawing.Size(119, 13);
            this.lPolarOpponent.TabIndex = 126;
            this.lPolarOpponent.Text = "Поляризация сигнала";
            // 
            // cbPolarOpponent
            // 
            this.cbPolarOpponent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPolarOpponent.FormattingEnabled = true;
            this.cbPolarOpponent.Items.AddRange(new object[] {
            "Линейная вертикальная",
            "Линейная горизонтальная",
            "Круговая правая",
            "Круговая левая"});
            this.cbPolarOpponent.Location = new System.Drawing.Point(134, 113);
            this.cbPolarOpponent.Name = "cbPolarOpponent";
            this.cbPolarOpponent.Size = new System.Drawing.Size(193, 21);
            this.cbPolarOpponent.TabIndex = 125;
            // 
            // lHeightReceiverOpponent
            // 
            this.lHeightReceiverOpponent.AutoSize = true;
            this.lHeightReceiverOpponent.Location = new System.Drawing.Point(249, 91);
            this.lHeightReceiverOpponent.Name = "lHeightReceiverOpponent";
            this.lHeightReceiverOpponent.Size = new System.Drawing.Size(182, 13);
            this.lHeightReceiverOpponent.TabIndex = 124;
            this.lHeightReceiverOpponent.Text = "Антенна приемника, м....................";
            // 
            // tbHeightReceiverOpponent
            // 
            this.tbHeightReceiverOpponent.Location = new System.Drawing.Point(442, 87);
            this.tbHeightReceiverOpponent.Name = "tbHeightReceiverOpponent";
            this.tbHeightReceiverOpponent.Size = new System.Drawing.Size(60, 20);
            this.tbHeightReceiverOpponent.TabIndex = 123;
            this.tbHeightReceiverOpponent.Text = "1";
            // 
            // lHeightTransmitOpponent
            // 
            this.lHeightTransmitOpponent.AutoSize = true;
            this.lHeightTransmitOpponent.Location = new System.Drawing.Point(249, 61);
            this.lHeightTransmitOpponent.Name = "lHeightTransmitOpponent";
            this.lHeightTransmitOpponent.Size = new System.Drawing.Size(187, 13);
            this.lHeightTransmitOpponent.TabIndex = 122;
            this.lHeightTransmitOpponent.Text = "Антенна передачитка, м...................";
            // 
            // tbHeightTransmitOpponent
            // 
            this.tbHeightTransmitOpponent.Location = new System.Drawing.Point(442, 61);
            this.tbHeightTransmitOpponent.Name = "tbHeightTransmitOpponent";
            this.tbHeightTransmitOpponent.Size = new System.Drawing.Size(60, 20);
            this.tbHeightTransmitOpponent.TabIndex = 121;
            this.tbHeightTransmitOpponent.Text = "1";
            // 
            // lWidthSignal
            // 
            this.lWidthSignal.AutoSize = true;
            this.lWidthSignal.Location = new System.Drawing.Point(251, 32);
            this.lWidthSignal.Name = "lWidthSignal";
            this.lWidthSignal.Size = new System.Drawing.Size(189, 13);
            this.lWidthSignal.TabIndex = 120;
            this.lWidthSignal.Text = "Ширина спектра, кГц.........................";
            // 
            // tbWidthSignal
            // 
            this.tbWidthSignal.Location = new System.Drawing.Point(442, 32);
            this.tbWidthSignal.Name = "tbWidthSignal";
            this.tbWidthSignal.Size = new System.Drawing.Size(60, 20);
            this.tbWidthSignal.TabIndex = 119;
            this.tbWidthSignal.Text = "25";
            // 
            // lRangeComm
            // 
            this.lRangeComm.AutoSize = true;
            this.lRangeComm.Location = new System.Drawing.Point(251, 9);
            this.lRangeComm.Name = "lRangeComm";
            this.lRangeComm.Size = new System.Drawing.Size(185, 13);
            this.lRangeComm.TabIndex = 118;
            this.lRangeComm.Text = "Дальность связи, м.........................";
            // 
            // tbRangeComm
            // 
            this.tbRangeComm.Location = new System.Drawing.Point(442, 6);
            this.tbRangeComm.Name = "tbRangeComm";
            this.tbRangeComm.Size = new System.Drawing.Size(60, 20);
            this.tbRangeComm.TabIndex = 117;
            this.tbRangeComm.Text = "3000";
            // 
            // lCoeffReceivertOpponent
            // 
            this.lCoeffReceivertOpponent.AutoSize = true;
            this.lCoeffReceivertOpponent.Location = new System.Drawing.Point(4, 89);
            this.lCoeffReceivertOpponent.Name = "lCoeffReceivertOpponent";
            this.lCoeffReceivertOpponent.Size = new System.Drawing.Size(154, 13);
            this.lCoeffReceivertOpponent.TabIndex = 116;
            this.lCoeffReceivertOpponent.Text = "КУ антенны приемника.........";
            this.lCoeffReceivertOpponent.Visible = false;
            // 
            // tbCoeffReceiverOpponent
            // 
            this.tbCoeffReceiverOpponent.Location = new System.Drawing.Point(171, 80);
            this.tbCoeffReceiverOpponent.Name = "tbCoeffReceiverOpponent";
            this.tbCoeffReceiverOpponent.Size = new System.Drawing.Size(60, 20);
            this.tbCoeffReceiverOpponent.TabIndex = 115;
            this.tbCoeffReceiverOpponent.Text = "10";
            this.tbCoeffReceiverOpponent.Visible = false;
            // 
            // lCoeffTransmitOpponent
            // 
            this.lCoeffTransmitOpponent.AutoSize = true;
            this.lCoeffTransmitOpponent.Location = new System.Drawing.Point(4, 59);
            this.lCoeffTransmitOpponent.Name = "lCoeffTransmitOpponent";
            this.lCoeffTransmitOpponent.Size = new System.Drawing.Size(162, 13);
            this.lCoeffTransmitOpponent.TabIndex = 114;
            this.lCoeffTransmitOpponent.Text = "КУ антенны передатчика ........";
            // 
            // tbCoeffTransmitOpponent
            // 
            this.tbCoeffTransmitOpponent.Location = new System.Drawing.Point(171, 54);
            this.tbCoeffTransmitOpponent.Name = "tbCoeffTransmitOpponent";
            this.tbCoeffTransmitOpponent.Size = new System.Drawing.Size(60, 20);
            this.tbCoeffTransmitOpponent.TabIndex = 113;
            this.tbCoeffTransmitOpponent.Text = "1";
            // 
            // lPowerOpponent
            // 
            this.lPowerOpponent.AutoSize = true;
            this.lPowerOpponent.Location = new System.Drawing.Point(4, 33);
            this.lPowerOpponent.Name = "lPowerOpponent";
            this.lPowerOpponent.Size = new System.Drawing.Size(163, 13);
            this.lPowerOpponent.TabIndex = 112;
            this.lPowerOpponent.Text = "Мощность передачитка, Вт......";
            // 
            // tbPowerOpponent
            // 
            this.tbPowerOpponent.Location = new System.Drawing.Point(171, 28);
            this.tbPowerOpponent.Name = "tbPowerOpponent";
            this.tbPowerOpponent.Size = new System.Drawing.Size(60, 20);
            this.tbPowerOpponent.TabIndex = 111;
            this.tbPowerOpponent.Text = "25";
            // 
            // lFreq
            // 
            this.lFreq.AutoSize = true;
            this.lFreq.Location = new System.Drawing.Point(4, 7);
            this.lFreq.Name = "lFreq";
            this.lFreq.Size = new System.Drawing.Size(152, 13);
            this.lFreq.TabIndex = 110;
            this.lFreq.Text = "Несущая частота, кГц...........";
            // 
            // tbFreq
            // 
            this.tbFreq.Location = new System.Drawing.Point(171, 4);
            this.tbFreq.Name = "tbFreq";
            this.tbFreq.Size = new System.Drawing.Size(60, 20);
            this.tbFreq.TabIndex = 109;
            this.tbFreq.Text = "40000";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(20, 416);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(133, 13);
            this.label13.TabIndex = 105;
            this.label13.Text = "Ручной выбор координат";
            this.label13.Visible = false;
            // 
            // chbXY
            // 
            this.chbXY.AutoSize = true;
            this.chbXY.Location = new System.Drawing.Point(158, 416);
            this.chbXY.Name = "chbXY";
            this.chbXY.Size = new System.Drawing.Size(15, 14);
            this.chbXY.TabIndex = 104;
            this.chbXY.UseVisualStyleBackColor = true;
            this.chbXY.Visible = false;
            // 
            // cbHeightOwnObject
            // 
            this.cbHeightOwnObject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbHeightOwnObject.FormattingEnabled = true;
            this.cbHeightOwnObject.Items.AddRange(new object[] {
            "Высота антенны+рельеф",
            "Высота антенны",
            "Задать самостоятельно"});
            this.cbHeightOwnObject.Location = new System.Drawing.Point(21, 566);
            this.cbHeightOwnObject.Name = "cbHeightOwnObject";
            this.cbHeightOwnObject.Size = new System.Drawing.Size(165, 21);
            this.cbHeightOwnObject.TabIndex = 27;
            this.cbHeightOwnObject.Visible = false;
            // 
            // tbRadiusZone
            // 
            this.tbRadiusZone.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbRadiusZone.Location = new System.Drawing.Point(98, 245);
            this.tbRadiusZone.Name = "tbRadiusZone";
            this.tbRadiusZone.Size = new System.Drawing.Size(58, 20);
            this.tbRadiusZone.TabIndex = 110;
            // 
            // lRadiusZone
            // 
            this.lRadiusZone.AutoSize = true;
            this.lRadiusZone.Location = new System.Drawing.Point(6, 245);
            this.lRadiusZone.Name = "lRadiusZone";
            this.lRadiusZone.Size = new System.Drawing.Size(86, 13);
            this.lRadiusZone.TabIndex = 111;
            this.lRadiusZone.Text = "Радиус зоны, м";
            // 
            // tbMaxDist
            // 
            this.tbMaxDist.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbMaxDist.Location = new System.Drawing.Point(208, 245);
            this.tbMaxDist.Name = "tbMaxDist";
            this.tbMaxDist.ReadOnly = true;
            this.tbMaxDist.Size = new System.Drawing.Size(58, 20);
            this.tbMaxDist.TabIndex = 112;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(162, 248);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 13);
            this.label11.TabIndex = 113;
            this.label11.Text = "ДПВ, м";
            // 
            // bAccept
            // 
            this.bAccept.Location = new System.Drawing.Point(369, 246);
            this.bAccept.Name = "bAccept";
            this.bAccept.Size = new System.Drawing.Size(77, 23);
            this.bAccept.TabIndex = 114;
            this.bAccept.Text = "Принять";
            this.bAccept.UseVisualStyleBackColor = true;
            this.bAccept.Click += new System.EventHandler(this.bAccept_Click);
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(448, 246);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(70, 23);
            this.bClear.TabIndex = 115;
            this.bClear.Text = "Очистить";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button1.Location = new System.Drawing.Point(278, 246);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 23);
            this.button1.TabIndex = 116;
            this.button1.Text = "Центр зоны";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // grbDetail
            // 
            this.grbDetail.Controls.Add(this.tbCorrectHeightOpponent);
            this.grbDetail.Controls.Add(this.label10);
            this.grbDetail.Controls.Add(this.tbGamma);
            this.grbDetail.Controls.Add(this.label9);
            this.grbDetail.Controls.Add(this.tbResultHeightOpponent);
            this.grbDetail.Controls.Add(this.label5);
            this.grbDetail.Controls.Add(this.tbMinHeight);
            this.grbDetail.Controls.Add(this.label6);
            this.grbDetail.Controls.Add(this.label7);
            this.grbDetail.Controls.Add(this.tbMaxDistance);
            this.grbDetail.Controls.Add(this.label8);
            this.grbDetail.Controls.Add(this.tbCorrectHeightOwn);
            this.grbDetail.Controls.Add(this.label4);
            this.grbDetail.Controls.Add(this.tbCoeffHE);
            this.grbDetail.Controls.Add(this.label3);
            this.grbDetail.Controls.Add(this.tbCoeffQ);
            this.grbDetail.Controls.Add(this.label2);
            this.grbDetail.Controls.Add(this.tbResultHeightOwn);
            this.grbDetail.Controls.Add(this.label1);
            this.grbDetail.Location = new System.Drawing.Point(16, 432);
            this.grbDetail.Name = "grbDetail";
            this.grbDetail.Size = new System.Drawing.Size(477, 128);
            this.grbDetail.TabIndex = 117;
            this.grbDetail.TabStop = false;
            this.grbDetail.Visible = false;
            // 
            // tbCorrectHeightOpponent
            // 
            this.tbCorrectHeightOpponent.Location = new System.Drawing.Point(411, 55);
            this.tbCorrectHeightOpponent.Name = "tbCorrectHeightOpponent";
            this.tbCorrectHeightOpponent.Size = new System.Drawing.Size(60, 20);
            this.tbCorrectHeightOpponent.TabIndex = 116;
            this.tbCorrectHeightOpponent.Text = "4";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(254, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(200, 13);
            this.label10.TabIndex = 115;
            this.label10.Text = "CorrectHeightOpponent...........................";
            // 
            // tbGamma
            // 
            this.tbGamma.Location = new System.Drawing.Point(159, 100);
            this.tbGamma.Name = "tbGamma";
            this.tbGamma.Size = new System.Drawing.Size(60, 20);
            this.tbGamma.TabIndex = 114;
            this.tbGamma.Text = "4";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(2, 107);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(175, 13);
            this.label9.TabIndex = 113;
            this.label9.Text = "Gamma............................................";
            // 
            // tbResultHeightOpponent
            // 
            this.tbResultHeightOpponent.Location = new System.Drawing.Point(411, 77);
            this.tbResultHeightOpponent.Name = "tbResultHeightOpponent";
            this.tbResultHeightOpponent.Size = new System.Drawing.Size(60, 20);
            this.tbResultHeightOpponent.TabIndex = 112;
            this.tbResultHeightOpponent.Text = "4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(254, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(193, 13);
            this.label5.TabIndex = 111;
            this.label5.Text = "ResultHeightOpponent..........................";
            // 
            // tbMinHeight
            // 
            this.tbMinHeight.Location = new System.Drawing.Point(411, 33);
            this.tbMinHeight.Name = "tbMinHeight";
            this.tbMinHeight.Size = new System.Drawing.Size(60, 20);
            this.tbMinHeight.TabIndex = 110;
            this.tbMinHeight.Text = "4";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(254, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(172, 13);
            this.label6.TabIndex = 109;
            this.label6.Text = "MinHeight.......................................";
            // 
            // tbMiddleHeight
            // 
            this.tbMiddleHeight.Location = new System.Drawing.Point(111, 175);
            this.tbMiddleHeight.Name = "tbMiddleHeight";
            this.tbMiddleHeight.Size = new System.Drawing.Size(60, 20);
            this.tbMiddleHeight.TabIndex = 108;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(254, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(186, 13);
            this.label7.TabIndex = 107;
            this.label7.Text = "MiddleHeight.......................................";
            // 
            // tbMaxDistance
            // 
            this.tbMaxDistance.Location = new System.Drawing.Point(411, 100);
            this.tbMaxDistance.Name = "tbMaxDistance";
            this.tbMaxDistance.Size = new System.Drawing.Size(60, 20);
            this.tbMaxDistance.TabIndex = 106;
            this.tbMaxDistance.Text = "4";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(254, 107);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(165, 13);
            this.label8.TabIndex = 105;
            this.label8.Text = "MaxDistance................................";
            // 
            // tbCorrectHeightOwn
            // 
            this.tbCorrectHeightOwn.Location = new System.Drawing.Point(159, 55);
            this.tbCorrectHeightOwn.Name = "tbCorrectHeightOwn";
            this.tbCorrectHeightOwn.Size = new System.Drawing.Size(60, 20);
            this.tbCorrectHeightOwn.TabIndex = 104;
            this.tbCorrectHeightOwn.Text = "4";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(175, 13);
            this.label4.TabIndex = 103;
            this.label4.Text = "CorrectHeightOwn...........................";
            // 
            // tbCoeffHE
            // 
            this.tbCoeffHE.Location = new System.Drawing.Point(159, 33);
            this.tbCoeffHE.Name = "tbCoeffHE";
            this.tbCoeffHE.Size = new System.Drawing.Size(60, 20);
            this.tbCoeffHE.TabIndex = 102;
            this.tbCoeffHE.Text = "4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 13);
            this.label3.TabIndex = 101;
            this.label3.Text = "CoeffHE.......................................";
            // 
            // tbCoeffQ
            // 
            this.tbCoeffQ.Location = new System.Drawing.Point(159, 12);
            this.tbCoeffQ.Name = "tbCoeffQ";
            this.tbCoeffQ.Size = new System.Drawing.Size(60, 20);
            this.tbCoeffQ.TabIndex = 100;
            this.tbCoeffQ.Text = "4";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(172, 13);
            this.label2.TabIndex = 99;
            this.label2.Text = "CoeffQ............................................";
            // 
            // tbResultHeightOwn
            // 
            this.tbResultHeightOwn.Location = new System.Drawing.Point(159, 77);
            this.tbResultHeightOwn.Name = "tbResultHeightOwn";
            this.tbResultHeightOwn.Size = new System.Drawing.Size(60, 20);
            this.tbResultHeightOwn.TabIndex = 98;
            this.tbResultHeightOwn.Text = "4";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 13);
            this.label1.TabIndex = 97;
            this.label1.Text = "ResultHeightOwn.............................";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(4, 179);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(104, 13);
            this.label15.TabIndex = 139;
            this.label15.Text = "Средняя высота, м";
            // 
            // ZonePowerAvail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 277);
            this.Controls.Add(this.cbHeightOwnObject);
            this.Controls.Add(this.chbXY);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.grbDetail);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.bAccept);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tbMaxDist);
            this.Controls.Add(this.lRadiusZone);
            this.Controls.Add(this.tbRadiusZone);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(500, 100);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ZonePowerAvail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Расчет энергодоступности по зонам";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.ZonePowerAvail_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ZonePowerAvail_FormClosing);
            this.Load += new System.EventHandler(this.ZonePowerAvail_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.grbOwnObject.ResumeLayout(false);
            this.grbOwnObject.PerformLayout();
            this.pCoordPoint.ResumeLayout(false);
            this.pCoordPoint.PerformLayout();
            this.gbOwnRect42.ResumeLayout(false);
            this.gbOwnRect42.PerformLayout();
            this.gbOwnDegMinSec.ResumeLayout(false);
            this.gbOwnDegMinSec.PerformLayout();
            this.gbOwnDegMin.ResumeLayout(false);
            this.gbOwnDegMin.PerformLayout();
            this.gbOwnRad.ResumeLayout(false);
            this.gbOwnRad.PerformLayout();
            this.gbOwnRect.ResumeLayout(false);
            this.gbOwnRect.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.grbCoeffSupOpponent.ResumeLayout(false);
            this.grbCoeffSupOpponent.PerformLayout();
            this.grbDetail.ResumeLayout(false);
            this.grbDetail.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label lChooseSC;
        private System.Windows.Forms.GroupBox grbOwnObject;
        private System.Windows.Forms.ComboBox cbHeightOwnObject;
        private System.Windows.Forms.Label lHeightOwnObject;
        private System.Windows.Forms.Panel pCoordPoint;
        private System.Windows.Forms.ComboBox cbOwnObject;
        private System.Windows.Forms.Label lCenterLSR;
        private System.Windows.Forms.Label lOwnHeight;
        private System.Windows.Forms.GroupBox gbOwnDegMin;
        private System.Windows.Forms.Label lMin2;
        private System.Windows.Forms.Label lMin1;
        public System.Windows.Forms.TextBox tbLMin1;
        public System.Windows.Forms.TextBox tbBMin1;
        private System.Windows.Forms.Label lLDegMin;
        private System.Windows.Forms.Label lBDegMin;
        private System.Windows.Forms.GroupBox gbOwnRect;
        public System.Windows.Forms.TextBox tbYRect;
        private System.Windows.Forms.Label lYRect;
        public System.Windows.Forms.TextBox tbXRect;
        private System.Windows.Forms.Label lXRect;
        private System.Windows.Forms.Label lWidthHindrance;
        private System.Windows.Forms.ComboBox cbWidthHindrance;
        private System.Windows.Forms.Label lCoeffOwn;
        private System.Windows.Forms.TextBox tbCoeffOwn;
        private System.Windows.Forms.Label lPowerOwn;
        private System.Windows.Forms.TextBox tbPowerOwn;
        private System.Windows.Forms.Label lSurface;
        private System.Windows.Forms.ComboBox cbSurface;
        private System.Windows.Forms.Label lRadiusZone;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button bAccept;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.GroupBox gbOwnRect42;
        public System.Windows.Forms.TextBox tbYRect42;
        private System.Windows.Forms.Label lYRect42;
        public System.Windows.Forms.TextBox tbXRect42;
        private System.Windows.Forms.Label lXRect42;
        private System.Windows.Forms.GroupBox gbOwnRad;
        public System.Windows.Forms.TextBox tbLRad;
        private System.Windows.Forms.Label lLRad;
        public System.Windows.Forms.TextBox tbBRad;
        private System.Windows.Forms.Label lBRad;
        private System.Windows.Forms.GroupBox gbOwnDegMinSec;
        public System.Windows.Forms.TextBox tbLSec;
        public System.Windows.Forms.TextBox tbBSec;
        private System.Windows.Forms.Label lMin4;
        private System.Windows.Forms.Label lMin3;
        public System.Windows.Forms.TextBox tbLMin2;
        public System.Windows.Forms.TextBox tbBMin2;
        private System.Windows.Forms.Label lSec2;
        private System.Windows.Forms.Label lSec1;
        private System.Windows.Forms.Label lDeg4;
        private System.Windows.Forms.Label lDeg3;
        public System.Windows.Forms.TextBox tbLDeg2;
        private System.Windows.Forms.Label lLDegMinSec;
        public System.Windows.Forms.TextBox tbBDeg2;
        private System.Windows.Forms.Label lBDegMinSec;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lCapacity;
        private System.Windows.Forms.ComboBox cbCap1;
        private System.Windows.Forms.Label lCoeffReceivertOpponent;
        private System.Windows.Forms.TextBox tbCoeffReceiverOpponent;
        private System.Windows.Forms.Label lCoeffTransmitOpponent;
        private System.Windows.Forms.TextBox tbCoeffTransmitOpponent;
        private System.Windows.Forms.Label lPowerOpponent;
        private System.Windows.Forms.TextBox tbPowerOpponent;
        private System.Windows.Forms.Label lFreq;
        private System.Windows.Forms.TextBox tbFreq;
        private System.Windows.Forms.Label lHeightReceiverOpponent;
        private System.Windows.Forms.TextBox tbHeightReceiverOpponent;
        private System.Windows.Forms.Label lHeightTransmitOpponent;
        private System.Windows.Forms.TextBox tbHeightTransmitOpponent;
        private System.Windows.Forms.Label lWidthSignal;
        private System.Windows.Forms.TextBox tbWidthSignal;
        private System.Windows.Forms.Label lRangeComm;
        private System.Windows.Forms.TextBox tbRangeComm;
        private System.Windows.Forms.Label lPolarOpponent;
        private System.Windows.Forms.ComboBox cbPolarOpponent;
        private System.Windows.Forms.GroupBox grbCoeffSupOpponent;
        private System.Windows.Forms.TextBox tbCoeffSupOpponent;
        private System.Windows.Forms.ComboBox cbCoeffSupOpponent;
        private System.Windows.Forms.Label lTypeCommOpponent;
        private System.Windows.Forms.ComboBox cbTypeCommOpponent;
        private System.Windows.Forms.GroupBox grbDetail;
        private System.Windows.Forms.TextBox tbCorrectHeightOpponent;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbGamma;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbResultHeightOpponent;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbMinHeight;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbMaxDistance;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbCorrectHeightOwn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbCoeffHE;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbCoeffQ;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbResultHeightOwn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbHAnt;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chbXY;
        public System.Windows.Forms.ComboBox cbCenterLSR;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.TextBox tbOwnHeight;
        public System.Windows.Forms.TextBox tbHeightOwnObject;
        public System.Windows.Forms.TextBox tbRadiusZone;
        public System.Windows.Forms.TextBox tbMaxDist;
        public System.Windows.Forms.ComboBox cbChooseSC;
        private System.Windows.Forms.Label label15;
        public System.Windows.Forms.TextBox tbMiddleHeight;

    }
}