﻿namespace GrozaMap
{
    partial class FormLineSightRange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.bAccept = new System.Windows.Forms.Button();
            this.grbOpponObject = new System.Windows.Forms.GroupBox();
            this.tbOpponentAntenna = new System.Windows.Forms.TextBox();
            this.tbHeightOpponObject = new System.Windows.Forms.TextBox();
            this.lHeightOpponObject = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.grbOwnObject = new System.Windows.Forms.GroupBox();
            this.tbHeightOwnObject = new System.Windows.Forms.TextBox();
            this.lHeightOwnObject = new System.Windows.Forms.Label();
            this.tbHAnt = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.lMiddleHeight = new System.Windows.Forms.Label();
            this.lDistSightRange = new System.Windows.Forms.Label();
            this.tbMiddleHeight = new System.Windows.Forms.TextBox();
            this.tbDistSightRange = new System.Windows.Forms.TextBox();
            this.lOwnHeight = new System.Windows.Forms.Label();
            this.lCenterLSR = new System.Windows.Forms.Label();
            this.cbCenterLSR = new System.Windows.Forms.ComboBox();
            this.cbChooseSC = new System.Windows.Forms.ComboBox();
            this.lChooseSC = new System.Windows.Forms.Label();
            this.pCoordPoint = new System.Windows.Forms.Panel();
            this.gbOwnRect = new System.Windows.Forms.GroupBox();
            this.tbYRect = new System.Windows.Forms.TextBox();
            this.lYRect = new System.Windows.Forms.Label();
            this.tbXRect = new System.Windows.Forms.TextBox();
            this.lXRect = new System.Windows.Forms.Label();
            this.gbOwnRect42 = new System.Windows.Forms.GroupBox();
            this.tbYRect42 = new System.Windows.Forms.TextBox();
            this.lYRect42 = new System.Windows.Forms.Label();
            this.tbXRect42 = new System.Windows.Forms.TextBox();
            this.lXRect42 = new System.Windows.Forms.Label();
            this.gbOwnRad = new System.Windows.Forms.GroupBox();
            this.tbLRad = new System.Windows.Forms.TextBox();
            this.lLRad = new System.Windows.Forms.Label();
            this.tbBRad = new System.Windows.Forms.TextBox();
            this.lBRad = new System.Windows.Forms.Label();
            this.gbOwnDegMinSec = new System.Windows.Forms.GroupBox();
            this.tbLSec = new System.Windows.Forms.TextBox();
            this.tbBSec = new System.Windows.Forms.TextBox();
            this.lMin4 = new System.Windows.Forms.Label();
            this.lMin3 = new System.Windows.Forms.Label();
            this.tbLMin2 = new System.Windows.Forms.TextBox();
            this.tbBMin2 = new System.Windows.Forms.TextBox();
            this.lSec2 = new System.Windows.Forms.Label();
            this.lSec1 = new System.Windows.Forms.Label();
            this.lDeg4 = new System.Windows.Forms.Label();
            this.lDeg3 = new System.Windows.Forms.Label();
            this.tbLDeg2 = new System.Windows.Forms.TextBox();
            this.lLDegMinSec = new System.Windows.Forms.Label();
            this.tbBDeg2 = new System.Windows.Forms.TextBox();
            this.lBDegMinSec = new System.Windows.Forms.Label();
            this.gbOwnDegMin = new System.Windows.Forms.GroupBox();
            this.tbLMin1 = new System.Windows.Forms.TextBox();
            this.tbBMin1 = new System.Windows.Forms.TextBox();
            this.lLDegMin = new System.Windows.Forms.Label();
            this.lBDegMin = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbOwnHeight = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.grbOpponObject.SuspendLayout();
            this.grbOwnObject.SuspendLayout();
            this.pCoordPoint.SuspendLayout();
            this.gbOwnRect.SuspendLayout();
            this.gbOwnRect42.SuspendLayout();
            this.gbOwnRad.SuspendLayout();
            this.gbOwnDegMinSec.SuspendLayout();
            this.gbOwnDegMin.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button1.Location = new System.Drawing.Point(3, 162);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(71, 23);
            this.button1.TabIndex = 165;
            this.button1.Text = "LOS center";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(146, 163);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(66, 23);
            this.bClear.TabIndex = 164;
            this.bClear.Text = "Clear";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click_1);
            // 
            // bAccept
            // 
            this.bAccept.Location = new System.Drawing.Point(77, 163);
            this.bAccept.Name = "bAccept";
            this.bAccept.Size = new System.Drawing.Size(65, 23);
            this.bAccept.TabIndex = 163;
            this.bAccept.Text = "Accept";
            this.bAccept.UseVisualStyleBackColor = true;
            this.bAccept.Click += new System.EventHandler(this.bAccept_Click);
            // 
            // grbOpponObject
            // 
            this.grbOpponObject.Controls.Add(this.tbOpponentAntenna);
            this.grbOpponObject.Controls.Add(this.tbHeightOpponObject);
            this.grbOpponObject.Controls.Add(this.lHeightOpponObject);
            this.grbOpponObject.Controls.Add(this.label1);
            this.grbOpponObject.Location = new System.Drawing.Point(183, 86);
            this.grbOpponObject.Name = "grbOpponObject";
            this.grbOpponObject.Size = new System.Drawing.Size(173, 65);
            this.grbOpponObject.TabIndex = 162;
            this.grbOpponObject.TabStop = false;
            this.grbOpponObject.Text = "Object of jam";
            // 
            // tbOpponentAntenna
            // 
            this.tbOpponentAntenna.Location = new System.Drawing.Point(116, 14);
            this.tbOpponentAntenna.Name = "tbOpponentAntenna";
            this.tbOpponentAntenna.Size = new System.Drawing.Size(51, 20);
            this.tbOpponentAntenna.TabIndex = 133;
            this.tbOpponentAntenna.Text = "2";
            // 
            // tbHeightOpponObject
            // 
            this.tbHeightOpponObject.Location = new System.Drawing.Point(116, 41);
            this.tbHeightOpponObject.MaxLength = 3;
            this.tbHeightOpponObject.Name = "tbHeightOpponObject";
            this.tbHeightOpponObject.Size = new System.Drawing.Size(52, 20);
            this.tbHeightOpponObject.TabIndex = 26;
            // 
            // lHeightOpponObject
            // 
            this.lHeightOpponObject.AutoSize = true;
            this.lHeightOpponObject.Location = new System.Drawing.Point(3, 45);
            this.lHeightOpponObject.Name = "lHeightOpponObject";
            this.lHeightOpponObject.Size = new System.Drawing.Size(110, 13);
            this.lHeightOpponObject.TabIndex = 21;
            this.lHeightOpponObject.Text = "Total height, m...........";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 134;
            this.label1.Text = "Antenna height, m .....";
            // 
            // grbOwnObject
            // 
            this.grbOwnObject.Controls.Add(this.tbHeightOwnObject);
            this.grbOwnObject.Controls.Add(this.lHeightOwnObject);
            this.grbOwnObject.Controls.Add(this.tbHAnt);
            this.grbOwnObject.Controls.Add(this.label17);
            this.grbOwnObject.Location = new System.Drawing.Point(182, 5);
            this.grbOwnObject.Name = "grbOwnObject";
            this.grbOwnObject.Size = new System.Drawing.Size(174, 74);
            this.grbOwnObject.TabIndex = 161;
            this.grbOwnObject.TabStop = false;
            this.grbOwnObject.Text = "Jammer";
            // 
            // tbHeightOwnObject
            // 
            this.tbHeightOwnObject.Location = new System.Drawing.Point(116, 45);
            this.tbHeightOwnObject.MaxLength = 3;
            this.tbHeightOwnObject.Name = "tbHeightOwnObject";
            this.tbHeightOwnObject.Size = new System.Drawing.Size(52, 20);
            this.tbHeightOwnObject.TabIndex = 26;
            // 
            // lHeightOwnObject
            // 
            this.lHeightOwnObject.AutoSize = true;
            this.lHeightOwnObject.Location = new System.Drawing.Point(3, 48);
            this.lHeightOwnObject.Name = "lHeightOwnObject";
            this.lHeightOwnObject.Size = new System.Drawing.Size(110, 13);
            this.lHeightOwnObject.TabIndex = 21;
            this.lHeightOwnObject.Text = "Total height, m...........";
            // 
            // tbHAnt
            // 
            this.tbHAnt.Location = new System.Drawing.Point(116, 19);
            this.tbHAnt.Name = "tbHAnt";
            this.tbHAnt.Size = new System.Drawing.Size(51, 20);
            this.tbHAnt.TabIndex = 109;
            this.tbHAnt.Text = "8";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 26);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(111, 13);
            this.label17.TabIndex = 110;
            this.label17.Text = "Antenna height, m .....";
            // 
            // lMiddleHeight
            // 
            this.lMiddleHeight.AutoSize = true;
            this.lMiddleHeight.Location = new System.Drawing.Point(176, 197);
            this.lMiddleHeight.Name = "lMiddleHeight";
            this.lMiddleHeight.Size = new System.Drawing.Size(93, 13);
            this.lMiddleHeight.TabIndex = 158;
            this.lMiddleHeight.Text = "Average height, m";
            // 
            // lDistSightRange
            // 
            this.lDistSightRange.AutoSize = true;
            this.lDistSightRange.Location = new System.Drawing.Point(15, 197);
            this.lDistSightRange.Name = "lDistSightRange";
            this.lDistSightRange.Size = new System.Drawing.Size(65, 13);
            this.lDistSightRange.TabIndex = 157;
            this.lDistSightRange.Text = "Range, m....";
            // 
            // tbMiddleHeight
            // 
            this.tbMiddleHeight.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbMiddleHeight.Location = new System.Drawing.Point(273, 195);
            this.tbMiddleHeight.Name = "tbMiddleHeight";
            this.tbMiddleHeight.ReadOnly = true;
            this.tbMiddleHeight.Size = new System.Drawing.Size(69, 20);
            this.tbMiddleHeight.TabIndex = 156;
            // 
            // tbDistSightRange
            // 
            this.tbDistSightRange.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbDistSightRange.Location = new System.Drawing.Point(88, 194);
            this.tbDistSightRange.Name = "tbDistSightRange";
            this.tbDistSightRange.Size = new System.Drawing.Size(71, 20);
            this.tbDistSightRange.TabIndex = 155;
            // 
            // lOwnHeight
            // 
            this.lOwnHeight.AutoSize = true;
            this.lOwnHeight.Location = new System.Drawing.Point(4, 114);
            this.lOwnHeight.Name = "lOwnHeight";
            this.lOwnHeight.Size = new System.Drawing.Size(77, 13);
            this.lOwnHeight.TabIndex = 153;
            this.lOwnHeight.Text = "Altitude, m.......";
            // 
            // lCenterLSR
            // 
            this.lCenterLSR.AutoSize = true;
            this.lCenterLSR.Location = new System.Drawing.Point(4, 6);
            this.lCenterLSR.Name = "lCenterLSR";
            this.lCenterLSR.Size = new System.Drawing.Size(61, 13);
            this.lCenterLSR.TabIndex = 137;
            this.lCenterLSR.Text = "LOS center";
            // 
            // cbCenterLSR
            // 
            this.cbCenterLSR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCenterLSR.FormattingEnabled = true;
            this.cbCenterLSR.Items.AddRange(new object[] {
            "X,Y",
            "JS",
            "Mated JS",
            "Control post"});
            this.cbCenterLSR.Location = new System.Drawing.Point(72, 3);
            this.cbCenterLSR.Name = "cbCenterLSR";
            this.cbCenterLSR.Size = new System.Drawing.Size(105, 21);
            this.cbCenterLSR.TabIndex = 134;
            this.cbCenterLSR.SelectedIndexChanged += new System.EventHandler(this.cbCenterLSR_SelectedIndexChanged);
            // 
            // cbChooseSC
            // 
            this.cbChooseSC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChooseSC.DropDownWidth = 150;
            this.cbChooseSC.FormattingEnabled = true;
            this.cbChooseSC.Items.AddRange(new object[] {
            "Degrees (WGS84)",
            "Meters (Gauss-Krueger)",
            "Radians (CS42)",
            "Degrees (CS42)",
            "Deg,min,sec(CS42)"});
            this.cbChooseSC.Location = new System.Drawing.Point(31, 136);
            this.cbChooseSC.Name = "cbChooseSC";
            this.cbChooseSC.Size = new System.Drawing.Size(140, 21);
            this.cbChooseSC.TabIndex = 223;
            this.cbChooseSC.SelectedIndexChanged += new System.EventHandler(this.cbChooseSC_SelectedIndexChanged);
            // 
            // lChooseSC
            // 
            this.lChooseSC.AutoSize = true;
            this.lChooseSC.Location = new System.Drawing.Point(3, 141);
            this.lChooseSC.Name = "lChooseSC";
            this.lChooseSC.Size = new System.Drawing.Size(21, 13);
            this.lChooseSC.TabIndex = 224;
            this.lChooseSC.Text = "CS";
            this.lChooseSC.Click += new System.EventHandler(this.lChooseSC_Click);
            // 
            // pCoordPoint
            // 
            this.pCoordPoint.Controls.Add(this.gbOwnRect);
            this.pCoordPoint.Controls.Add(this.gbOwnRect42);
            this.pCoordPoint.Controls.Add(this.gbOwnRad);
            this.pCoordPoint.Controls.Add(this.gbOwnDegMinSec);
            this.pCoordPoint.Controls.Add(this.gbOwnDegMin);
            this.pCoordPoint.Location = new System.Drawing.Point(4, 29);
            this.pCoordPoint.Name = "pCoordPoint";
            this.pCoordPoint.Size = new System.Drawing.Size(176, 76);
            this.pCoordPoint.TabIndex = 225;
            // 
            // gbOwnRect
            // 
            this.gbOwnRect.Controls.Add(this.tbYRect);
            this.gbOwnRect.Controls.Add(this.lYRect);
            this.gbOwnRect.Controls.Add(this.tbXRect);
            this.gbOwnRect.Controls.Add(this.lXRect);
            this.gbOwnRect.Location = new System.Drawing.Point(6, 9);
            this.gbOwnRect.Name = "gbOwnRect";
            this.gbOwnRect.Size = new System.Drawing.Size(165, 63);
            this.gbOwnRect.TabIndex = 67;
            this.gbOwnRect.TabStop = false;
            this.gbOwnRect.Visible = false;
            // 
            // tbYRect
            // 
            this.tbYRect.Location = new System.Drawing.Point(78, 36);
            this.tbYRect.MaxLength = 7;
            this.tbYRect.Name = "tbYRect";
            this.tbYRect.Size = new System.Drawing.Size(75, 20);
            this.tbYRect.TabIndex = 5;
            // 
            // lYRect
            // 
            this.lYRect.AutoSize = true;
            this.lYRect.Location = new System.Drawing.Point(6, 39);
            this.lYRect.Name = "lYRect";
            this.lYRect.Size = new System.Drawing.Size(70, 13);
            this.lYRect.TabIndex = 6;
            this.lYRect.Text = "L...................";
            // 
            // tbXRect
            // 
            this.tbXRect.Location = new System.Drawing.Point(78, 13);
            this.tbXRect.MaxLength = 7;
            this.tbXRect.Name = "tbXRect";
            this.tbXRect.Size = new System.Drawing.Size(75, 20);
            this.tbXRect.TabIndex = 4;
            // 
            // lXRect
            // 
            this.lXRect.AutoSize = true;
            this.lXRect.Location = new System.Drawing.Point(4, 20);
            this.lXRect.Name = "lXRect";
            this.lXRect.Size = new System.Drawing.Size(71, 13);
            this.lXRect.TabIndex = 3;
            this.lXRect.Text = "B...................";
            // 
            // gbOwnRect42
            // 
            this.gbOwnRect42.Controls.Add(this.tbYRect42);
            this.gbOwnRect42.Controls.Add(this.lYRect42);
            this.gbOwnRect42.Controls.Add(this.tbXRect42);
            this.gbOwnRect42.Controls.Add(this.lXRect42);
            this.gbOwnRect42.Location = new System.Drawing.Point(6, 9);
            this.gbOwnRect42.Name = "gbOwnRect42";
            this.gbOwnRect42.Size = new System.Drawing.Size(164, 63);
            this.gbOwnRect42.TabIndex = 27;
            this.gbOwnRect42.TabStop = false;
            // 
            // tbYRect42
            // 
            this.tbYRect42.Location = new System.Drawing.Point(78, 36);
            this.tbYRect42.MaxLength = 7;
            this.tbYRect42.Name = "tbYRect42";
            this.tbYRect42.Size = new System.Drawing.Size(75, 20);
            this.tbYRect42.TabIndex = 5;
            // 
            // lYRect42
            // 
            this.lYRect42.AutoSize = true;
            this.lYRect42.Location = new System.Drawing.Point(4, 39);
            this.lYRect42.Name = "lYRect42";
            this.lYRect42.Size = new System.Drawing.Size(76, 13);
            this.lYRect42.TabIndex = 6;
            this.lYRect42.Text = "Y, m................";
            // 
            // tbXRect42
            // 
            this.tbXRect42.Location = new System.Drawing.Point(78, 13);
            this.tbXRect42.MaxLength = 7;
            this.tbXRect42.Name = "tbXRect42";
            this.tbXRect42.Size = new System.Drawing.Size(75, 20);
            this.tbXRect42.TabIndex = 4;
            // 
            // lXRect42
            // 
            this.lXRect42.AutoSize = true;
            this.lXRect42.Location = new System.Drawing.Point(4, 20);
            this.lXRect42.Name = "lXRect42";
            this.lXRect42.Size = new System.Drawing.Size(76, 13);
            this.lXRect42.TabIndex = 3;
            this.lXRect42.Text = "X, m................";
            // 
            // gbOwnRad
            // 
            this.gbOwnRad.Controls.Add(this.tbLRad);
            this.gbOwnRad.Controls.Add(this.lLRad);
            this.gbOwnRad.Controls.Add(this.tbBRad);
            this.gbOwnRad.Controls.Add(this.lBRad);
            this.gbOwnRad.Location = new System.Drawing.Point(6, 9);
            this.gbOwnRad.Name = "gbOwnRad";
            this.gbOwnRad.Size = new System.Drawing.Size(160, 63);
            this.gbOwnRad.TabIndex = 28;
            this.gbOwnRad.TabStop = false;
            this.gbOwnRad.Visible = false;
            // 
            // tbLRad
            // 
            this.tbLRad.Location = new System.Drawing.Point(78, 36);
            this.tbLRad.MaxLength = 10;
            this.tbLRad.Name = "tbLRad";
            this.tbLRad.Size = new System.Drawing.Size(75, 20);
            this.tbLRad.TabIndex = 5;
            // 
            // lLRad
            // 
            this.lLRad.AutoSize = true;
            this.lLRad.Location = new System.Drawing.Point(4, 41);
            this.lLRad.Name = "lLRad";
            this.lLRad.Size = new System.Drawing.Size(67, 13);
            this.lLRad.TabIndex = 6;
            this.lLRad.Text = "L, rad...........";
            // 
            // tbBRad
            // 
            this.tbBRad.Location = new System.Drawing.Point(78, 13);
            this.tbBRad.MaxLength = 10;
            this.tbBRad.Name = "tbBRad";
            this.tbBRad.Size = new System.Drawing.Size(75, 20);
            this.tbBRad.TabIndex = 4;
            // 
            // lBRad
            // 
            this.lBRad.AutoSize = true;
            this.lBRad.Location = new System.Drawing.Point(4, 18);
            this.lBRad.Name = "lBRad";
            this.lBRad.Size = new System.Drawing.Size(68, 13);
            this.lBRad.TabIndex = 3;
            this.lBRad.Text = "B, rad...........";
            // 
            // gbOwnDegMinSec
            // 
            this.gbOwnDegMinSec.Controls.Add(this.tbLSec);
            this.gbOwnDegMinSec.Controls.Add(this.tbBSec);
            this.gbOwnDegMinSec.Controls.Add(this.lMin4);
            this.gbOwnDegMinSec.Controls.Add(this.lMin3);
            this.gbOwnDegMinSec.Controls.Add(this.tbLMin2);
            this.gbOwnDegMinSec.Controls.Add(this.tbBMin2);
            this.gbOwnDegMinSec.Controls.Add(this.lSec2);
            this.gbOwnDegMinSec.Controls.Add(this.lSec1);
            this.gbOwnDegMinSec.Controls.Add(this.lDeg4);
            this.gbOwnDegMinSec.Controls.Add(this.lDeg3);
            this.gbOwnDegMinSec.Controls.Add(this.tbLDeg2);
            this.gbOwnDegMinSec.Controls.Add(this.lLDegMinSec);
            this.gbOwnDegMinSec.Controls.Add(this.tbBDeg2);
            this.gbOwnDegMinSec.Controls.Add(this.lBDegMinSec);
            this.gbOwnDegMinSec.Location = new System.Drawing.Point(6, 9);
            this.gbOwnDegMinSec.Name = "gbOwnDegMinSec";
            this.gbOwnDegMinSec.Size = new System.Drawing.Size(160, 60);
            this.gbOwnDegMinSec.TabIndex = 30;
            this.gbOwnDegMinSec.TabStop = false;
            this.gbOwnDegMinSec.Visible = false;
            // 
            // tbLSec
            // 
            this.tbLSec.Location = new System.Drawing.Point(102, 36);
            this.tbLSec.MaxLength = 3;
            this.tbLSec.Name = "tbLSec";
            this.tbLSec.Size = new System.Drawing.Size(37, 20);
            this.tbLSec.TabIndex = 23;
            // 
            // tbBSec
            // 
            this.tbBSec.Location = new System.Drawing.Point(102, 12);
            this.tbBSec.MaxLength = 3;
            this.tbBSec.Name = "tbBSec";
            this.tbBSec.Size = new System.Drawing.Size(38, 20);
            this.tbBSec.TabIndex = 18;
            // 
            // lMin4
            // 
            this.lMin4.AutoSize = true;
            this.lMin4.Location = new System.Drawing.Point(91, 35);
            this.lMin4.Name = "lMin4";
            this.lMin4.Size = new System.Drawing.Size(9, 13);
            this.lMin4.TabIndex = 27;
            this.lMin4.Text = "\'";
            // 
            // lMin3
            // 
            this.lMin3.AutoSize = true;
            this.lMin3.Location = new System.Drawing.Point(92, 12);
            this.lMin3.Name = "lMin3";
            this.lMin3.Size = new System.Drawing.Size(9, 13);
            this.lMin3.TabIndex = 26;
            this.lMin3.Text = "\'";
            // 
            // tbLMin2
            // 
            this.tbLMin2.Location = new System.Drawing.Point(67, 35);
            this.tbLMin2.MaxLength = 2;
            this.tbLMin2.Name = "tbLMin2";
            this.tbLMin2.Size = new System.Drawing.Size(25, 20);
            this.tbLMin2.TabIndex = 22;
            // 
            // tbBMin2
            // 
            this.tbBMin2.Location = new System.Drawing.Point(67, 12);
            this.tbBMin2.MaxLength = 2;
            this.tbBMin2.Name = "tbBMin2";
            this.tbBMin2.Size = new System.Drawing.Size(25, 20);
            this.tbBMin2.TabIndex = 16;
            // 
            // lSec2
            // 
            this.lSec2.AutoSize = true;
            this.lSec2.Location = new System.Drawing.Point(145, 36);
            this.lSec2.Name = "lSec2";
            this.lSec2.Size = new System.Drawing.Size(9, 13);
            this.lSec2.TabIndex = 25;
            this.lSec2.Text = "\'";
            // 
            // lSec1
            // 
            this.lSec1.AutoSize = true;
            this.lSec1.Location = new System.Drawing.Point(146, 12);
            this.lSec1.Name = "lSec1";
            this.lSec1.Size = new System.Drawing.Size(9, 13);
            this.lSec1.TabIndex = 24;
            this.lSec1.Text = "\'";
            // 
            // lDeg4
            // 
            this.lDeg4.AutoSize = true;
            this.lDeg4.Location = new System.Drawing.Point(51, 38);
            this.lDeg4.Name = "lDeg4";
            this.lDeg4.Size = new System.Drawing.Size(13, 13);
            this.lDeg4.TabIndex = 21;
            this.lDeg4.Text = "^";
            // 
            // lDeg3
            // 
            this.lDeg3.AutoSize = true;
            this.lDeg3.Location = new System.Drawing.Point(51, 13);
            this.lDeg3.Name = "lDeg3";
            this.lDeg3.Size = new System.Drawing.Size(13, 13);
            this.lDeg3.TabIndex = 20;
            this.lDeg3.Text = "^";
            // 
            // tbLDeg2
            // 
            this.tbLDeg2.Location = new System.Drawing.Point(23, 36);
            this.tbLDeg2.MaxLength = 2;
            this.tbLDeg2.Name = "tbLDeg2";
            this.tbLDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbLDeg2.TabIndex = 19;
            // 
            // lLDegMinSec
            // 
            this.lLDegMinSec.AutoSize = true;
            this.lLDegMinSec.Location = new System.Drawing.Point(6, 36);
            this.lLDegMinSec.Name = "lLDegMinSec";
            this.lLDegMinSec.Size = new System.Drawing.Size(13, 13);
            this.lLDegMinSec.TabIndex = 17;
            this.lLDegMinSec.Text = "L";
            // 
            // tbBDeg2
            // 
            this.tbBDeg2.Location = new System.Drawing.Point(23, 13);
            this.tbBDeg2.MaxLength = 2;
            this.tbBDeg2.Name = "tbBDeg2";
            this.tbBDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbBDeg2.TabIndex = 15;
            // 
            // lBDegMinSec
            // 
            this.lBDegMinSec.AutoSize = true;
            this.lBDegMinSec.Location = new System.Drawing.Point(5, 14);
            this.lBDegMinSec.Name = "lBDegMinSec";
            this.lBDegMinSec.Size = new System.Drawing.Size(14, 13);
            this.lBDegMinSec.TabIndex = 14;
            this.lBDegMinSec.Text = "B";
            // 
            // gbOwnDegMin
            // 
            this.gbOwnDegMin.Controls.Add(this.tbLMin1);
            this.gbOwnDegMin.Controls.Add(this.tbBMin1);
            this.gbOwnDegMin.Controls.Add(this.lLDegMin);
            this.gbOwnDegMin.Controls.Add(this.lBDegMin);
            this.gbOwnDegMin.Location = new System.Drawing.Point(6, 9);
            this.gbOwnDegMin.Name = "gbOwnDegMin";
            this.gbOwnDegMin.Size = new System.Drawing.Size(159, 63);
            this.gbOwnDegMin.TabIndex = 29;
            this.gbOwnDegMin.TabStop = false;
            this.gbOwnDegMin.Visible = false;
            // 
            // tbLMin1
            // 
            this.tbLMin1.Location = new System.Drawing.Point(75, 36);
            this.tbLMin1.MaxLength = 8;
            this.tbLMin1.Name = "tbLMin1";
            this.tbLMin1.Size = new System.Drawing.Size(64, 20);
            this.tbLMin1.TabIndex = 15;
            // 
            // tbBMin1
            // 
            this.tbBMin1.Location = new System.Drawing.Point(75, 13);
            this.tbBMin1.MaxLength = 8;
            this.tbBMin1.Name = "tbBMin1";
            this.tbBMin1.Size = new System.Drawing.Size(64, 20);
            this.tbBMin1.TabIndex = 13;
            // 
            // lLDegMin
            // 
            this.lLDegMin.AutoSize = true;
            this.lLDegMin.Location = new System.Drawing.Point(20, 39);
            this.lLDegMin.Name = "lLDegMin";
            this.lLDegMin.Size = new System.Drawing.Size(49, 13);
            this.lLDegMin.TabIndex = 12;
            this.lLDegMin.Text = "L............";
            // 
            // lBDegMin
            // 
            this.lBDegMin.AutoSize = true;
            this.lBDegMin.Location = new System.Drawing.Point(20, 16);
            this.lBDegMin.Name = "lBDegMin";
            this.lBDegMin.Size = new System.Drawing.Size(50, 13);
            this.lBDegMin.TabIndex = 10;
            this.lBDegMin.Text = "B............";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 624);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 13);
            this.label2.TabIndex = 63;
            this.label2.Text = "H, м.............................";
            this.label2.Visible = false;
            // 
            // tbOwnHeight
            // 
            this.tbOwnHeight.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbOwnHeight.Location = new System.Drawing.Point(90, 112);
            this.tbOwnHeight.Name = "tbOwnHeight";
            this.tbOwnHeight.Size = new System.Drawing.Size(81, 20);
            this.tbOwnHeight.TabIndex = 64;
            // 
            // button2
            // 
            this.button2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button2.Location = new System.Drawing.Point(218, 162);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(131, 23);
            this.button2.TabIndex = 226;
            this.button2.Text = "Mating pair";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // FormLineSightRange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 221);
            this.Controls.Add(this.tbDistSightRange);
            this.Controls.Add(this.tbMiddleHeight);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.pCoordPoint);
            this.Controls.Add(this.tbOwnHeight);
            this.Controls.Add(this.lChooseSC);
            this.Controls.Add(this.cbChooseSC);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.bAccept);
            this.Controls.Add(this.grbOpponObject);
            this.Controls.Add(this.grbOwnObject);
            this.Controls.Add(this.lMiddleHeight);
            this.Controls.Add(this.lOwnHeight);
            this.Controls.Add(this.lCenterLSR);
            this.Controls.Add(this.cbCenterLSR);
            this.Controls.Add(this.lDistSightRange);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(850, 100);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormLineSightRange";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Line of sight zone (LOS)";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.FormLineSightRange_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormLineSightRange_FormClosing);
            this.Load += new System.EventHandler(this.FormLineSightRange_Load);
            this.grbOpponObject.ResumeLayout(false);
            this.grbOpponObject.PerformLayout();
            this.grbOwnObject.ResumeLayout(false);
            this.grbOwnObject.PerformLayout();
            this.pCoordPoint.ResumeLayout(false);
            this.gbOwnRect.ResumeLayout(false);
            this.gbOwnRect.PerformLayout();
            this.gbOwnRect42.ResumeLayout(false);
            this.gbOwnRect42.PerformLayout();
            this.gbOwnRad.ResumeLayout(false);
            this.gbOwnRad.PerformLayout();
            this.gbOwnDegMinSec.ResumeLayout(false);
            this.gbOwnDegMinSec.PerformLayout();
            this.gbOwnDegMin.ResumeLayout(false);
            this.gbOwnDegMin.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        public System.Windows.Forms.ComboBox cbCenterLSR;
        private System.Windows.Forms.Label lCenterLSR;
        private System.Windows.Forms.Label lOwnHeight;
        private System.Windows.Forms.Label lMiddleHeight;
        private System.Windows.Forms.Label lDistSightRange;
        private System.Windows.Forms.GroupBox grbOwnObject;
        private System.Windows.Forms.Label lHeightOwnObject;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox grbOpponObject;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lHeightOpponObject;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Button bAccept;
        public System.Windows.Forms.TextBox tbHAnt;
        public System.Windows.Forms.TextBox tbOpponentAntenna;
        private System.Windows.Forms.Label lChooseSC;
        public System.Windows.Forms.TextBox tbMiddleHeight;
        public System.Windows.Forms.TextBox tbDistSightRange;
        public System.Windows.Forms.TextBox tbHeightOwnObject;
        public System.Windows.Forms.TextBox tbHeightOpponObject;
        public System.Windows.Forms.ComboBox cbChooseSC;
        private System.Windows.Forms.Panel pCoordPoint;
        private System.Windows.Forms.GroupBox gbOwnRect;
        public System.Windows.Forms.TextBox tbYRect;
        private System.Windows.Forms.Label lYRect;
        public System.Windows.Forms.TextBox tbXRect;
        private System.Windows.Forms.Label lXRect;
        private System.Windows.Forms.GroupBox gbOwnDegMin;
        public System.Windows.Forms.TextBox tbLMin1;
        public System.Windows.Forms.TextBox tbBMin1;
        private System.Windows.Forms.Label lLDegMin;
        private System.Windows.Forms.Label lBDegMin;
        private System.Windows.Forms.GroupBox gbOwnDegMinSec;
        public System.Windows.Forms.TextBox tbLSec;
        public System.Windows.Forms.TextBox tbBSec;
        private System.Windows.Forms.Label lMin4;
        private System.Windows.Forms.Label lMin3;
        public System.Windows.Forms.TextBox tbLMin2;
        public System.Windows.Forms.TextBox tbBMin2;
        private System.Windows.Forms.Label lSec2;
        private System.Windows.Forms.Label lSec1;
        private System.Windows.Forms.Label lDeg4;
        private System.Windows.Forms.Label lDeg3;
        public System.Windows.Forms.TextBox tbLDeg2;
        private System.Windows.Forms.Label lLDegMinSec;
        public System.Windows.Forms.TextBox tbBDeg2;
        private System.Windows.Forms.Label lBDegMinSec;
        private System.Windows.Forms.GroupBox gbOwnRect42;
        public System.Windows.Forms.TextBox tbYRect42;
        private System.Windows.Forms.Label lYRect42;
        public System.Windows.Forms.TextBox tbXRect42;
        private System.Windows.Forms.Label lXRect42;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gbOwnRad;
        public System.Windows.Forms.TextBox tbLRad;
        private System.Windows.Forms.Label lLRad;
        public System.Windows.Forms.TextBox tbBRad;
        private System.Windows.Forms.Label lBRad;
        public System.Windows.Forms.TextBox tbOwnHeight;
        private System.Windows.Forms.Button button2;
    }
}