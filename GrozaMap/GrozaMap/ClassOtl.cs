﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Куски для отладки
namespace GrozaMap
{
    class ClassOtl
    {
        // MapForm ****************************************************************************

        // Конструктор ----------------------------------------------------------------------

        // .................................................................................................
        // WCF
        //создаем сервис

        // 3
        /*
                    var service = new ServiceImplementation();
                    //подписываемся на событие HelloReceived
                    service.AirPlaneReceived += Service_AirPlaneReceived;
                    service.CurrentCoordsUpdated += service_CurrentCoordsUpdated; // service_CurrentCoordsUpdated;
                    service.CoordsIRIUpdated += service_CoordsIRIUpdated;
                    service.CoordsIRI_PPRChUpdated += service_CoordsIRI_PPRChUpdated;
                    service.PelengsIRI_PPRChUpdated += service_PelengsIRI_PPRChUpdated;
                    service.DirectionAntennasUpdated += service_DirectionAntennasUpdated;
                    service.CheckGNSSUpdated += new EventHandler<byte>(service_CheckGNSSUpdated);
                    //стартуем сервер
                    var svh = new ServiceHost(service);
                    svh.AddServiceEndpoint(typeof(Contract.IService), new NetTcpBinding(), "net.tcp://localhost:8000");
                    svh.Open();
        */
        // .................................................................................................

        // ---------------------------------------------------------------------- Конструктор

        // PELENG_OLD -----------------------------------------------------------------------


        // Peleng_IRI_PPRCH *******************************************************
        // INTERRUPT
        // !!! Теперь это только пеленги
        // 23_10_2018

        // 3
        /*
                private object threadLock_2 = new object();
                void service_PelengsIRI_PPRChUpdated(object sender, TCoordsIRI_PPRCh[] coordsIRI)
                {

                    // 19_10_2018 ****************************************************************
                    int countPel = 0;
                    double a1 = 0;
                    double b1 = 0;
                    double c1 = 0;
                    double d1 = 0;
                    double f1 = 0;
                    double g1 = 0;
                    double a2 = 0;
                    double b2 = 0;
                    double c2 = 0;
                    double d2 = 0;
                    double f2 = 0;
                    double g2 = 0;
                    double x184 = 0;
                    double y184 = 0;
                    double x284 = 0;
                    double y284 = 0;

                    double[] arr_Pel_XYZ1 = new double[GlobalVarLn.numberofdots * 3];
                    double[] arr_Pel_XYZ2 = new double[GlobalVarLn.numberofdots * 3];


                    double azgr1 = -1;
                    double azgr2 = -1;
                    PelIRI objPelIRI = new PelIRI();
                    PelIRI objPelIRI2 = new PelIRI();  // For IRI

                    int flg = 0;
                    int ind = 0;

                    int AdDl = 0;
                    //Color clr = Color.Red;
                    // ------------------------------------------------------------------------

                    // ------------------------------------------------------------------------
                    lock (threadLock_2)
                    {

                        // !!! Идут только пеленги (флпаг 3)/только ИРИ(флаг 0/1)/ удалить все (флаг 2)
                        AdDl = (int)coordsIRI[0].bDelIRI;

                        // Delete All ************************************************************
                        // Delete Peleng+IRI

                        if (AdDl == 2)
                        {

                            // Del Peleng
                            for (int iii20 = (GlobalVarLn.list_PelIRI.Count - 1); iii20 >= 0; iii20--)
                            {
                                GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii20]);
                            }
                            // DelIRI
                            for (int iii21 = (GlobalVarLn.list_PelIRI2.Count - 1); iii21 >= 0; iii21--)
                            {
                                GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii21]);
                            }

                            axaxcMapScreen1.Repaint();
                            return;

                        }  // AdDl=2
                        // ************************************************************ Delete All

                        // Peleng ****************************************************************
                        // Del(Q1=Q2=-1)/Add Peleng
                        // !!! Old Peleng delete

                        if (
                            (AdDl == 3) &&
                            (chbPeleng.Checked == true)
                            )
                        {
                            // ........................................................................
                            // Все старые пеленги удалить

                            for (int iii22 = (GlobalVarLn.list_PelIRI.Count - 1); iii22 >= 0; iii22--)
                            {
                                GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii22]);
                            }
                            // ........................................................................

                            // Add_Struct_ID ..........................................................
                            // Потом добавляем в лист массив пришедших структур

                            // FOR1_1
                            for (countPel = 0; countPel < coordsIRI.Length; countPel++)
                            {
                                if (
                                     (coordsIRI[countPel].iQ1 != -1) ||
                                     (coordsIRI[countPel].iQ2 != -1)
                                   )
                                {
                                    objPelIRI.ID = coordsIRI[countPel].iID;
                                    objPelIRI.XGPS_IRI = -1;
                                    objPelIRI.YGPS_IRI = -1;
                                    objPelIRI.flPelMain2 = 0;
                                    objPelIRI.arr_Pel1 = new double[10000];     // R,Широта,долгота
                                    objPelIRI.arr_Pel2 = new double[10000];     // R,Широта,долгота
                                    objPelIRI.Lat = -1;
                                    objPelIRI.Long = -1;
                                    objPelIRI.AddDel = (int)coordsIRI[countPel].bDelIRI; // 3 fore Peleng

                                    if ((coordsIRI[countPel].iQ1 != -1) && (chbPeleng.Checked == true))
                                        objPelIRI.Pel1 = (double)(coordsIRI[countPel].iQ1 / 10d);
                                    else
                                        objPelIRI.Pel1 = -1;

                                    if ((coordsIRI[countPel].iQ2 != -1) && (chbPeleng.Checked == true))
                                    {
                                        objPelIRI.Pel2 = (double)(coordsIRI[countPel].iQ2 / 10d);
                                        objPelIRI.flPelMain2 = 1;
                                    }
                                    else
                                    {
                                        objPelIRI.Pel2 = -1;
                                        objPelIRI.flPelMain2 = 0;
                                    }

                                    GlobalVarLn.list_PelIRI.Add(objPelIRI);

                                } //IF(есть пеленг) 

                            } //FOR1_1
                            // .......................................................... Add_Struct_ID

                            // Calculate  .............................................................

                            GlobalVarLn.X1_PelMain = (int)GlobalVarLn.list1_Sost[0].X_m;  // SP1
                            GlobalVarLn.Y1_PelMain = (int)GlobalVarLn.list1_Sost[0].Y_m;
                            GlobalVarLn.X1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].X_m; // SP2
                            GlobalVarLn.Y1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].Y_m;

                            // FOR33 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            for (countPel = 0; countPel < coordsIRI.Length; countPel++)
                            {

                                if ((coordsIRI[countPel].iQ1 != -1) || (coordsIRI[countPel].iQ2 != -1))
                                {
                                    if (coordsIRI[countPel].iQ1 != -1)
                                    {
                                        azgr1 = (double)(coordsIRI[countPel].iQ1 / 10d); // Peleng1, grad
                                    }
                                    if (coordsIRI[countPel].iQ2 != -1)
                                    {
                                        azgr2 = (double)(coordsIRI[countPel].iQ2 / 10d); // Peleng2
                                    }

                                    x184 = GlobalVarLn.X1_PelMain;
                                    y184 = GlobalVarLn.Y1_PelMain;
                                    x284 = GlobalVarLn.X1_1_PelMain;
                                    y284 = GlobalVarLn.Y1_1_PelMain;

                                    mapPlaneToGeo(GlobalVarLn.hmapl, ref x184, ref y184);
                                    mapPlaneToGeo(GlobalVarLn.hmapl, ref x284, ref y284);

                                    // rad->grad
                                    x184 = (x184 * 180) / Math.PI;
                                    y184 = (y184 * 180) / Math.PI;
                                    x284 = (x284 * 180) / Math.PI;
                                    y284 = (y284 * 180) / Math.PI;

                                    ClassMap classMap = new ClassMap();
                                    ClassMap classMap1 = new ClassMap();

                                    // Peleng1
                                    if (coordsIRI[countPel].iQ1 != -1)
                                    {
                                        PelIRI[] mass1 = GlobalVarLn.list_PelIRI.ToArray();
                                        classMap.f_Peleng(azgr1, GlobalVarLn.distance, GlobalVarLn.numberofdots, x184, y184,
                                                          ref a1, ref b1, ref c1, ref d1, ref f1, ref g1,
                                                          ref mass1[countPel].arr_Pel1, ref arr_Pel_XYZ1);
                                        GlobalVarLn.list_PelIRI = mass1.ToList();
                                    } // Peleng1

                                    // Peleng2
                                    if (GlobalVarLn.list_PelIRI[countPel].flPelMain2 == 1)
                                    {
                                        PelIRI[] mass2 = GlobalVarLn.list_PelIRI.ToArray();
                                        classMap1.f_Peleng(azgr2, GlobalVarLn.distance, GlobalVarLn.numberofdots, x284, y284,
                                                           ref a2, ref b2, ref c2, ref d2, ref f2, ref g2,
                                                           ref mass2[countPel].arr_Pel2, ref arr_Pel_XYZ2);
                                        GlobalVarLn.list_PelIRI = mass2.ToList();

                                    } // Peleng2


                                } // if ((coordsIRI[countPel].iQ1 != -1) || (coordsIRI[countPel].iQ2 != -1))

                            } // FOR33
                            //  ............................................................. Calculate

                            axaxcMapScreen1.Repaint();
                            return;

                        }  // AdDl=3chbPeleng.Checked == true
                        // **************************************************************** Peleng

                    } // threadLock_2
                    // **************************************************************** 19_10_2018

                } // INTERRUPT
                // ******************************************************* Peleng_IRI_PPRCH
        */

        // ----------------------------------------------------------------------- PELENG_OLD

        // OLD_IRI --------------------------------------------------------------------------

        // IRI_PPRCH ***********************************************************************
        // INTERRUPT
        // !!! Теперь это только IRI PPRCH
        // Variant2

        // 22_10_2018
        //private object threadLock_5 = new object();

        // 3
        /*
                void service_CoordsIRI_PPRChUpdated(object sender, List<TCoordsIRI_PPRCh> listCoordsIRI_PPRCh)
                {
                    // 2504
                    double dds = 250; // m

                    int AdDl = 0;
                    int countPel = 0;
                    int countPel1 = 0;

                    lock (threadLock_5)
                    {
                        // !!! Идут только пеленги (флпаг 3)/только ИРИ(флаг 0/1)/ удалить все (флаг 2)
                        AdDl = (int)listCoordsIRI_PPRCh[0].bDelIRI;

                        PelIRI objPelIRI2 = new PelIRI();  // For IRI

                        // Delete All ************************************************************
                        // Delete Peleng+IRI

                        if (AdDl == 2)
                        {

                            // Del Peleng
                            for (int iii20 = (GlobalVarLn.list_PelIRI.Count - 1); iii20 >= 0; iii20--)
                            {
                                GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii20]);
                            }
                            // DelIRI
                            for (int iii21 = (GlobalVarLn.list_PelIRI2.Count - 1); iii21 >= 0; iii21--)
                            {
                                GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii21]);
                            }

                            axaxcMapScreen1.Repaint();
                            return;

                        }  // AdDl=2
                        // ************************************************************ Delete All

                        // Delete ID *************************************************************
                        // Удалить конкретный ID

                        if (AdDl == 1)
                        {
                            // Del_Struct_ID ..........................................................
                            // удаляем все структуры с таким IDi 
                            // Придет в 

                            // FOR111
                            for (countPel = 0; countPel < listCoordsIRI_PPRCh.Count; countPel++)
                            {
                                int ffg1 = (int)listCoordsIRI_PPRCh[countPel].bDelIRI;

                                if (ffg1 == 1) // Del
                                {
                                    // For22
                                    for (int iii33 = (GlobalVarLn.list_PelIRI2.Count - 1); iii33 >= 0; iii33--)
                                    {
                                        if (GlobalVarLn.list_PelIRI2[iii33].ID == listCoordsIRI_PPRCh[countPel].iID)
                                            GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii33]);

                                    } //for22

                                } // IF

                            } // FOR111

                            axaxcMapScreen1.Repaint();
                            return;

                        } // AdDl==1 Del
                        // ************************************************************* Delete ID

                        // Add IRI ****************************************************************

                        if (AdDl == 0)
                        {
                            // ------------------------------------------------------------------------
                            // 2504

                            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            bool SignRepaint = false;
                            int i1 = 0;
                            int i2 = 0;
                            int i3 = 0;
                            int i4 = 0;
                            bool flexist = false;
                            int ffg = 0;
                            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            // Убрать в старом те, которых нет в новых

                            for (i1 = (GlobalVarLn.list_PelIRI2.Count - 1); i1 >= 0; i1--) // old
                            {
                                flexist = false;
                                for (i2 = 0; i2 < listCoordsIRI_PPRCh.Count; i2++) // new
                                {
                                    ffg = (int)listCoordsIRI_PPRCh[i2].bDelIRI;

                                    if (
                                        (GlobalVarLn.list_PelIRI2[i1].ID == listCoordsIRI_PPRCh[i2].iID) &&
                                        (listCoordsIRI_PPRCh[i2].dLatitude != -1) &&
                                        (listCoordsIRI_PPRCh[i2].dLongitude != -1) &&
                                        (ffg == 0)
                                       )
                                    {
                                        flexist = true;
                                    }

                                } // i2

                                if (flexist == false)
                                {
                                    GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[i1]);
                                    SignRepaint = true;
                                    //i1 = -1;
                                }

                            } // i1 old

                            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            // 

                            // FOR1_5 вперед по новому
                            for (countPel = 0; countPel < listCoordsIRI_PPRCh.Count; countPel++) // new
                            {

                                ffg = (int)listCoordsIRI_PPRCh[countPel].bDelIRI;

                                if (
                                     (listCoordsIRI_PPRCh[countPel].dLatitude != -1) &&
                                     (listCoordsIRI_PPRCh[countPel].dLongitude != -1) &&
                                     (ffg == 0)
                                   )
                                {

                                    flexist = false;
                                    for (int iii99 = (GlobalVarLn.list_PelIRI2.Count - 1); iii99 >= 0; iii99--)
                                    {

                                        if (GlobalVarLn.list_PelIRI2[iii99].ID == listCoordsIRI_PPRCh[countPel].iID)
                                        {
                                            // есть с таким ID
                                            flexist = true;
                                            double sss = 0;
                                            sss = ClassMap.f_D_2Points(GlobalVarLn.list_PelIRI2[iii99].Lat, GlobalVarLn.list_PelIRI2[iii99].Long, listCoordsIRI_PPRCh[countPel].dLatitude, listCoordsIRI_PPRCh[countPel].dLongitude, 1);
                                            if (sss > dds)
                                            {
                                                SignRepaint = true;
                                                // Убираем старый
                                                GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii99]);

                                                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                                // добавляем новый

                                                objPelIRI2.ID = listCoordsIRI_PPRCh[countPel].iID;
                                                objPelIRI2.XGPS_IRI = -1;
                                                objPelIRI2.YGPS_IRI = -1;
                                                objPelIRI2.flPelMain2 = 0;
                                                objPelIRI2.arr_Pel1 = new double[10000];     // R,Широта,долгота
                                                objPelIRI2.arr_Pel2 = new double[10000];     // R,Широта,долгота
                                                objPelIRI2.Pel1 = -1;
                                                objPelIRI2.Pel2 = -1;

                                                if ((listCoordsIRI_PPRCh[countPel].dLatitude != -1) && (listCoordsIRI_PPRCh[countPel].dLongitude != -1))
                                                {
                                                    objPelIRI2.Lat = listCoordsIRI_PPRCh[countPel].dLatitude;
                                                    objPelIRI2.Long = listCoordsIRI_PPRCh[countPel].dLongitude;
                                                    objPelIRI2.color = ColorFromSharpString(listCoordsIRI_PPRCh[countPel].sColorIRI);
                                                    objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;
                                                }
                                                else
                                                {
                                                    objPelIRI2.Lat = -1;
                                                    objPelIRI2.Long = -1;
                                                    objPelIRI2.XGPS_IRI = -1;
                                                    objPelIRI2.YGPS_IRI = -1;
                                                    objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;

                                                }

                                                GlobalVarLn.list_PelIRI2.Add(objPelIRI2);

                                                ClassMap.f_GetGPS_IRI_2(GlobalVarLn.list_PelIRI2.Count - 1);

                                                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                                            } // S>ds

                                        }  // ID==

                                    }  // for old назад

                                    if (flexist == false) // это новый ID -> добавляем его
                                    {
                                        SignRepaint = true;

                                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                        // добавляем новый

                                        objPelIRI2.ID = listCoordsIRI_PPRCh[countPel].iID;
                                        objPelIRI2.XGPS_IRI = -1;
                                        objPelIRI2.YGPS_IRI = -1;
                                        objPelIRI2.flPelMain2 = 0;
                                        objPelIRI2.arr_Pel1 = new double[10000];     // R,Широта,долгота
                                        objPelIRI2.arr_Pel2 = new double[10000];     // R,Широта,долгота
                                        objPelIRI2.Pel1 = -1;
                                        objPelIRI2.Pel2 = -1;

                                        if ((listCoordsIRI_PPRCh[countPel].dLatitude != -1) && (listCoordsIRI_PPRCh[countPel].dLongitude != -1))
                                        {
                                            objPelIRI2.Lat = listCoordsIRI_PPRCh[countPel].dLatitude;
                                            objPelIRI2.Long = listCoordsIRI_PPRCh[countPel].dLongitude;
                                            objPelIRI2.color = ColorFromSharpString(listCoordsIRI_PPRCh[countPel].sColorIRI);
                                            objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;
                                        }
                                        else
                                        {
                                            objPelIRI2.Lat = -1;
                                            objPelIRI2.Long = -1;
                                            objPelIRI2.XGPS_IRI = -1;
                                            objPelIRI2.YGPS_IRI = -1;
                                            objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;

                                        }

                                        GlobalVarLn.list_PelIRI2.Add(objPelIRI2);

                                        ClassMap.f_GetGPS_IRI_2(GlobalVarLn.list_PelIRI2.Count - 1);

                                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                                    } // // это новый ID

                                } //IF(есть координаты) 

                            } //FOR1_5 new
                            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                            if (SignRepaint == true)
                                axaxcMapScreen1.Repaint();


                        } // AdDl==0
                        // **************************************************************** Add IRI

                    } // threadLock_5

                } // INTERRUPT
                  // *********************************************************************** IRI_PPRCH
        */

        // -------------------------------------------------------------------------- OLD_IRI

        // SECTOR_OLD -----------------------------------------------------------------------

        // SECTOR ****************************************************************
        //Sect ПРИНЯТЬ

        /// <summary>
        /// Направление антенн
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        // 3
        /*
                void service_DirectionAntennasUpdated(object sender, TDirectionAntennas e)
                {

                    // -------------------------------------------------------------------
                    // Направление секторов

                    GlobalVarLn.luch1 = (double)e.LPA13; // grad
                    GlobalVarLn.luch2 = (double)e.LPA24;
                    GlobalVarLn.luch3 = (double)e.ARD1;
                    GlobalVarLn.luch4 = (double)e.ARD2;
                    GlobalVarLn.luch5 = (double)e.ARD3;
                    // -------------------------------------------------------------------
                    if (GlobalVarLn.luch1 != -1)
                        GlobalVarLn.fllSect1 = 1;
                    else
                        GlobalVarLn.fllSect1 = 0;

                    if (GlobalVarLn.luch2 != -1)
                        GlobalVarLn.fllSect2 = 1;
                    else
                        GlobalVarLn.fllSect2 = 0;

                    if (GlobalVarLn.luch3 != -1)
                        GlobalVarLn.fllSect3 = 1;
                    else
                        GlobalVarLn.fllSect3 = 0;

                    if (GlobalVarLn.luch4 != -1)
                        GlobalVarLn.fllSect4 = 1;
                    else
                        GlobalVarLn.fllSect4 = 0;

                    if (GlobalVarLn.luch5 != -1)
                        GlobalVarLn.fllSect5 = 1;
                    else
                        GlobalVarLn.fllSect5 = 0;
                    // -------------------------------------------------------------------

                    // IF***
                    if (
                        ((GlobalVarLn.fllSect1 == 1) ||
                         (GlobalVarLn.fllSect2 == 1) ||
                         (GlobalVarLn.fllSect3 == 1) ||
                         (GlobalVarLn.fllSect4 == 1) ||
                         (GlobalVarLn.fllSect5 == 1)) &&
                        (chbSect.Checked == true)
                       )
                    {
                        // .................................................................
                        if (GlobalVarLn.f_luch == 0)
                        {
                            GlobalVarLn.f_luch = 1;
                            GlobalVarLn.luch1_dubl = GlobalVarLn.luch1;
                            GlobalVarLn.luch2_dubl = GlobalVarLn.luch2;
                            GlobalVarLn.luch3_dubl = GlobalVarLn.luch3;
                            GlobalVarLn.luch4_dubl = GlobalVarLn.luch4;
                            GlobalVarLn.luch5_dubl = GlobalVarLn.luch5;

                            GlobalVarLn.flsect = 0;
                            // Убрать с карты
                            GlobalVarLn.axMapScreenGlobal.Repaint();

                            GlobalVarLn.flsect = 1;

                            ClassMap.Sector(
                                            (double)GlobalVarLn.XCenter_Sost,
                                            (double)GlobalVarLn.YCenter_Sost
                                          );
                        } // f_luch==0
                        // .................................................................
                        // f_luch=1

                        else
                        {
                            // Те же значения
                            if (
                                (GlobalVarLn.luch1 == GlobalVarLn.luch1_dubl) &&
                                (GlobalVarLn.luch2 == GlobalVarLn.luch2_dubl) &&
                                (GlobalVarLn.luch3 == GlobalVarLn.luch3_dubl) &&
                                (GlobalVarLn.luch4 == GlobalVarLn.luch4_dubl) &&
                                (GlobalVarLn.luch5 == GlobalVarLn.luch5_dubl)
                              )
                            {
                                return;
                            } // Те же значения

                            // Новые
                            else
                            {
                                GlobalVarLn.luch1_dubl = GlobalVarLn.luch1;
                                GlobalVarLn.luch2_dubl = GlobalVarLn.luch2;
                                GlobalVarLn.luch3_dubl = GlobalVarLn.luch3;
                                GlobalVarLn.luch4_dubl = GlobalVarLn.luch4;
                                GlobalVarLn.luch5_dubl = GlobalVarLn.luch5;

                                GlobalVarLn.flsect = 0;
                                // Убрать с карты
                                GlobalVarLn.axMapScreenGlobal.Repaint();

                                GlobalVarLn.flsect = 1;

                                ClassMap.Sector(
                                                (double)GlobalVarLn.XCenter_Sost,
                                                (double)GlobalVarLn.YCenter_Sost
                                              );

                            } // Новые

                        } // f_luch=1
                        // .................................................................

                    } // IF***
                    // -------------------------------------------------------------------

                } // P/P
        */

        // ----------------------------------------------------------------------- SECTOR_OLD

        // Button9_Otladka_Peleng ------------------------------------------------------------

        /*
            double azgr1 = 17;
            double azgr2 = 249;

            GlobalVarLn.X1_PelMain = (int)GlobalVarLn.list1_Sost[0].X_m;
            GlobalVarLn.Y1_PelMain = (int)GlobalVarLn.list1_Sost[0].Y_m;
            GlobalVarLn.X1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].X_m;
            GlobalVarLn.Y1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].Y_m;

            // ................................................................
            double a1 = 0;
            double b1 = 0;
            double c1 = 0;
            double d1 = 0;
            double f1 = 0;
            double g1 = 0;
            double a2 = 0;
            double b2 = 0;
            double c2 = 0;
            double d2 = 0;
            double f2 = 0;
            double g2 = 0;
            double x184 = 0;
            double y184 = 0;
            double x284 = 0;
            double y284 = 0;

            double[] arr_Pel_XYZ1 = new double[GlobalVarLn.numberofdots * 3];
            double[] arr_Pel_XYZ2 = new double[GlobalVarLn.numberofdots * 3];
            ClassMap classMap = new ClassMap();
            ClassMap classMap1 = new ClassMap();

            x184 = GlobalVarLn.X1_PelMain;
            y184 = GlobalVarLn.Y1_PelMain;
            x284 = GlobalVarLn.X1_1_PelMain;
            y284 = GlobalVarLn.Y1_1_PelMain;

            //mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref x184, ref y184);
            //mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref x284, ref y284);
            mapPlaneToGeo(GlobalVarLn.hmapl, ref x184, ref y184);
            mapPlaneToGeo(GlobalVarLn.hmapl, ref x284, ref y284);

            // rad->grad
            x184 = (x184 * 180) / Math.PI;
            y184 = (y184 * 180) / Math.PI;
            x284 = (x284 * 180) / Math.PI;
            y284 = (y284 * 180) / Math.PI;


            classMap.f_Peleng(azgr1, GlobalVarLn.distance, GlobalVarLn.numberofdots, x184, y184,
                              ref a1, ref b1, ref c1, ref d1, ref f1, ref g1,
                              ref GlobalVarLn.arr_Pel1, ref arr_Pel_XYZ1);
            classMap1.f_Peleng(azgr2, GlobalVarLn.distance, GlobalVarLn.numberofdots, x284, y284,
                               ref a2, ref b2, ref c2, ref d2, ref f2, ref g2,
                               ref GlobalVarLn.arr_Pel2, ref arr_Pel_XYZ2);
          
        */

        /*
                        int ind2 = 0;
                        for (int i2 = 0; i2 < GlobalVarLn.numberofdots; i2++)
                        {
                            // grad->rad
                            GlobalVarLn.arr_Pel1[ind2 + 1] = (GlobalVarLn.arr_Pel1[ind2 + 1] * Math.PI) / 180;
                            GlobalVarLn.arr_Pel1[ind2 + 2] = (GlobalVarLn.arr_Pel1[ind2 + 2] * Math.PI) / 180;
                            GlobalVarLn.arr_Pel2[ind2 + 1] = (GlobalVarLn.arr_Pel2[ind2 + 1] * Math.PI) / 180;
                            GlobalVarLn.arr_Pel2[ind2 + 2] = (GlobalVarLn.arr_Pel2[ind2 + 2] * Math.PI) / 180;
                            ind2 += 3;
                        }

                        int ind = 0;
                        for (int i = 0; i < GlobalVarLn.numberofdots; i++)
                        {
                            mapGeoToPlane(GlobalVarLn.hmapl, ref GlobalVarLn.arr_Pel1[ind + 1], ref GlobalVarLn.arr_Pel1[ind + 2]);
                            mapPlaneToPicture(GlobalVarLn.hmapl, ref GlobalVarLn.arr_Pel1[ind + 1], ref GlobalVarLn.arr_Pel1[ind + 2]);
                            mapGeoToPlane(GlobalVarLn.hmapl, ref GlobalVarLn.arr_Pel2[ind + 1], ref GlobalVarLn.arr_Pel2[ind + 2]);
                            mapPlaneToPicture(GlobalVarLn.hmapl, ref GlobalVarLn.arr_Pel2[ind + 1], ref GlobalVarLn.arr_Pel2[ind + 2]);
                            ind += 3;
                        }
        */
        // gggggggggggggggggggggggggggggggggggggggggggggggggggggg

        /*
                        Graphics graph = axaxcMapScreen1.CreateGraphics();
                        Brush brushRed2 = new SolidBrush(Color.Red);

                        int ind1 = 0;
                        for (int i1 = 0; i1 < GlobalVarLn.numberofdots; i1++)
                        {
                            graph.FillEllipse(
                                              brushRed2,
                                              (int)GlobalVarLn.arr_Pel1[ind1 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft,
                                              (int)GlobalVarLn.arr_Pel1[ind1 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop,
                                              5,
                                              5
                                              );
                            graph.FillEllipse(
                                              brushRed2,
                                              (int)GlobalVarLn.arr_Pel2[ind1 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft,
                                              (int)GlobalVarLn.arr_Pel2[ind1 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop,
                                              5,
                                              5
                                              );
                            ind1 += 3;

                        }
         */

        /*
                        Point pnt1_1=new Point();
                        Point pnt2_1=new Point();
                        Point pnt1_2=new Point();
                        Point pnt2_2=new Point();
                        Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
                        Pen pen1 = new Pen(Color.Red, 2.0f);
                        int ind3 = 0;
                        for (int i3 = 0; i3 < GlobalVarLn.numberofdots; i3++)
                        {
                            if (i3 == 0)
                            {
                                pnt1_1.X = (int)GlobalVarLn.arr_Pel1[ind3 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft;
                                pnt1_1.Y = (int)GlobalVarLn.arr_Pel1[ind3 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop;
                                pnt1_2.X = (int)GlobalVarLn.arr_Pel2[ind3 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft;
                                pnt1_2.Y = (int)GlobalVarLn.arr_Pel2[ind3 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop;

                            }
                            else
                            {
                                pnt2_1.X = (int)GlobalVarLn.arr_Pel1[ind3 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft;
                                pnt2_1.Y = (int)GlobalVarLn.arr_Pel1[ind3 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop;
                                pnt2_2.X = (int)GlobalVarLn.arr_Pel2[ind3 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft;
                                pnt2_2.Y = (int)GlobalVarLn.arr_Pel2[ind3 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop;
                                graph.DrawLine(pen1, pnt1_1, pnt2_1);
                                graph.DrawLine(pen1, pnt1_2, pnt2_2);
                                pnt1_1.X = pnt2_1.X;
                                pnt1_1.Y = pnt2_1.Y;
                                pnt1_2.X = pnt2_2.X;
                                pnt1_2.Y = pnt2_2.Y;

                            }

                            ind3 += 3;

                        } // FOR
        */

        //GlobalVarLn.flPelMain2 = 1;
        //axaxcMapScreen1.Repaint();
        //ClassMap.f_ReDrawPeleng();


        /*
                        Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
                        Pen pen1 = new Pen(Color.Red, 2.0f);
                        if (graph != null)
                        {
                            graph.DrawPolygon(pen1, lst1);
                            graph.DrawPolygon(pen1, lst2);
                        }
        */

        /*
                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    double dLat1 = 0;
                    double dLong1 = 0;
                    // Эллипсоид Красовского, град
                    double LatKrG1 = 0;
                    double LongKrG1 = 0;
                    // Гаусс-крюгер(СК42) м
                    double XSP421 = 0;
                    double YSP421 = 0;

                    double dLat2 = 0;
                    double dLong2 = 0;
                    // Эллипсоид Красовского, град
                    double LatKrG2 = 0;
                    double LongKrG2 = 0;
                    // Гаусс-крюгер(СК42) м
                    double XSP422 = 0;
                    double YSP422 = 0;

                    double X_IRI = 0;
                    double Y_IRI = 0;

                    ClassMap objClassMap1_ed = new ClassMap();
                    ClassMap objClassMap2_ed = new ClassMap();
                    ClassMap objClassMap3_ed = new ClassMap();
                    ClassMap objClassMap4_ed = new ClassMap();
                    ClassMap objClassMap5_ed = new ClassMap();
                    ClassMap objClassMap6_ed = new ClassMap();
                    ClassMap objClassMap7_ed = new ClassMap();
                    ClassMap objClassMap8_ed = new ClassMap();
                    ClassMap objClassMap9_ed = new ClassMap();
                    ClassMap objClassMap10_ed = new ClassMap();

                    // WGS84,grad
                    double xtmp1_ed = 53.881244;
                    double ytmp1_ed = 27.606453;
                    double xtmp2_ed = 53.9216687;
                    double ytmp2_ed = 27.6799367;

                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // WGS84(эллипсоид)->элл.Красовского
                    objClassMap1_ed.f_dLong
                        (
                            // Входные параметры (град,км)
                            xtmp1_ed,   // широта
                            ytmp1_ed,  // долгота
                            0,     // высота

                            // DATUM,m
                            GlobalVarLn.dXdat_comm,
                            GlobalVarLn.dYdat_comm,
                            GlobalVarLn.dZdat_comm,

                            ref dLong1   // приращение по долготе, угл.сек
                        );
                    objClassMap1_ed.f_dLat
                        (
                            // Входные параметры (град,км)
                            xtmp1_ed,   // широта
                            ytmp1_ed,  // долгота
                            0,     // высота

                            // DATUM,m
                            GlobalVarLn.dXdat_comm,
                            GlobalVarLn.dYdat_comm,
                            GlobalVarLn.dZdat_comm,

                            ref dLat1        // приращение по долготе, угл.сек
                        );
                    objClassMap1_ed.f_WGS84_SK42_Lat_Long
                           (
                               // Входные параметры (град,км)
                               xtmp1_ed,   // широта
                               ytmp1_ed,  // долгота
                               0,     // высота
                               dLat1,       // приращение по долготе, угл.сек
                               dLong1,      // приращение по долготе, угл.сек

                               // Выходные параметры (grad)
                               ref LatKrG1,   // широта
                               ref LongKrG1   // долгота
                           );
                    // SK42(элл.)->Крюгер 
                    objClassMap3_ed.f_SK42_Krug
                           (
                               // Входные параметры (!!! grad)
                               // !!! эллипсоид Красовского
                               LatKrG1,   // широта
                               LongKrG1,  // долгота

                               // Выходные параметры (km)
                               ref XSP421,
                               ref YSP421
                           );

                    // km->m
                    //XSP421 = XSP421 * 1000;
                    //YSP421 = YSP421 * 1000;
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // WGS84(эллипсоид)->элл.Красовского
                    objClassMap4_ed.f_dLong
                        (
                            // Входные параметры (град,км)
                            xtmp2_ed,   // широта
                            ytmp2_ed,  // долгота
                            0,     // высота

                            // DATUM,m
                            GlobalVarLn.dXdat_comm,
                            GlobalVarLn.dYdat_comm,
                            GlobalVarLn.dZdat_comm,

                            ref dLong2   // приращение по долготе, угл.сек
                        );
                    objClassMap4_ed.f_dLat
                        (
                            // Входные параметры (град,км)
                            xtmp2_ed,   // широта
                            ytmp2_ed,  // долгота
                            0,     // высота

                            // DATUM,m
                            GlobalVarLn.dXdat_comm,
                            GlobalVarLn.dYdat_comm,
                            GlobalVarLn.dZdat_comm,

                            ref dLat2        // приращение по долготе, угл.сек
                        );
                    objClassMap4_ed.f_WGS84_SK42_Lat_Long
                           (
                               // Входные параметры (град,км)
                               xtmp2_ed,   // широта
                               ytmp2_ed,  // долгота
                               0,     // высота
                               dLat2,       // приращение по долготе, угл.сек
                               dLong2,      // приращение по долготе, угл.сек

                               // Выходные параметры (grad)
                               ref LatKrG2,   // широта
                               ref LongKrG2   // долгота
                           );
                    // SK42(элл.)->Крюгер 
                    objClassMap6_ed.f_SK42_Krug
                           (
                               // Входные параметры (!!! grad)
                               // !!! эллипсоид Красовского
                               LatKrG2,   // широта
                               LongKrG2,  // долгота

                               // Выходные параметры (km)
                               ref XSP422,
                               ref YSP422
                           );

                    // km->m
                    //XSP422 = XSP422 * 1000;
                    //YSP422 = YSP422 * 1000;

                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    objClassMap7_ed.f_Triang2(
                                   // km
                                   XSP421,
                                   YSP421,
                                   XSP422,
                                   YSP422,
                                   // Пеленг(град)
                                   azgr1,
                                   azgr2,
                                   30,
                                   // Координаты искомого ИРИ (км) CK42
                                   ref X_IRI,
                                   ref Y_IRI
                                            );
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // Эллипсоид Красовского (grad)
                    objClassMap8_ed.f_Krug_SK42
                    (
                     // Входные параметры (км)
                     X_IRI,
                     Y_IRI,

                     // Выходные параметры (grad)
                     ref LatKrG1,   // широта
                     ref LongKrG1   // долгота
                     );
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // элл.Красовского->WGS84(эллипсоид)
                    objClassMap9_ed.f_dLong
                        (
                            // Входные параметры (град,км)
                            LatKrG1,   // широта
                            LongKrG1,  // долгота
                            0,     // высота

                            // DATUM,m
                            GlobalVarLn.dXdat_comm,
                            GlobalVarLn.dYdat_comm,
                            GlobalVarLn.dZdat_comm,

                            ref dLong1   // приращение по долготе, угл.сек
                        );
                    objClassMap9_ed.f_dLat
                        (
                            // Входные параметры (град,км)
                            LatKrG1,   // широта
                            LongKrG1,  // долгота
                            0,     // высота

                            // DATUM,m
                            GlobalVarLn.dXdat_comm,
                            GlobalVarLn.dYdat_comm,
                            GlobalVarLn.dZdat_comm,

                            ref dLat1        // приращение по долготе, угл.сек
                        );
                    objClassMap9_ed.f_SK42_WGS84_Lat_Long
                           (
                               // Входные параметры (град,км)
                               LatKrG1,   // широта
                               LongKrG1,  // долгота
                               0,     // высота
                               dLat1,       // приращение по долготе, угл.сек
                               dLong1,      // приращение по долготе, угл.сек

                               // Выходные параметры (grad) WGS84
                               ref xtmp1_ed,   // широта
                               ref ytmp1_ed   // долгота
                           );

                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                    // WGS84(эллипсоид)->элл.Красовского 
                    objClassMap10_ed.f_dLong
                        (
                            // Входные параметры (град,км)
                            xtmp1_ed,   // широта
                            ytmp1_ed,  // долгота
                            0,     // высота

                            // DATUM,m
                            GlobalVarLn.dXdat_comm,
                            GlobalVarLn.dYdat_comm,
                            GlobalVarLn.dZdat_comm,

                            ref dLong1   // приращение по долготе, угл.сек
                        );
                    objClassMap10_ed.f_dLat
                        (
                            // Входные параметры (град,км)
                            xtmp1_ed,   // широта
                            ytmp1_ed,  // долгота
                            0,     // высота

                            // DATUM,m
                            GlobalVarLn.dXdat_comm,
                            GlobalVarLn.dYdat_comm,
                            GlobalVarLn.dZdat_comm,

                            ref dLat1        // приращение по долготе, угл.сек
                        );
                    objClassMap10_ed.f_WGS84_SK42_Lat_Long
                           (
                               // Входные параметры (град,км)
                               xtmp1_ed,   // широта
                               ytmp1_ed,  // долгота
                               0,     // высота
                               dLat1,       // приращение по долготе, угл.сек
                               dLong1,      // приращение по долготе, угл.сек

                               // Выходные параметры (grad)
                               ref LatKrG1,   // широта
                               ref LongKrG1   // долгота
                           );
                    // ellipsKras -> M real
                    xtmp1_ed = LatKrG1;
                    ytmp1_ed = LongKrG1;
                    // Grad->rad
                    xtmp1_ed = (xtmp1_ed * Math.PI) / 180;
                    ytmp1_ed = (ytmp1_ed * Math.PI) / 180;
                    mapGeoToPlane(GlobalVarLn.hmapl, ref xtmp1_ed, ref ytmp1_ed);

                    // Redraw
                    ClassMap.f_Pol_GPS_IRI(
                                               xtmp1_ed,
                                               ytmp1_ed,
                                               Color.Blue,
                                               ""
                                              );

                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

         */
        // -------------------------------------------------------------------------------------

        // ...............................................................................................

        // ------------------------------------------------------------ Button9_Otladka_Peleng

        // INTERRUPT_PELENG_IRI_OLD -----------------------------------------------------------

        // Interrupt_Peleng_IRI ---------------------------------------------------
        // INTERRUPT
        // !!! Для ФРЧ

        //private double prevPel1 = -1;
        //private double prevPel2 = -1;

        // 3
        /*
                private object threadLock = new object();
                void service_CoordsIRIUpdated(object sender, TCoordsIRI[] coordsIRI)
                {
                    lock (threadLock)
                    {

                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        //  01_10_2018
                        // Delete all

                        byte bDel = coordsIRI[0].bDelIRI; // 0 - add IRI, 1 - delete IRI 2- del all 3- IRI Не трогать (только пеленги)

                        // При удалении всего придет одна структура
                        if (bDel == 2)
                        {
                            // Del Peleng1,2
                            if (chbPeleng.Checked == true)
                            {
                                // FRCH
                                GlobalVarLn.PrevP1 = -1;
                                GlobalVarLn.PrevP2 = -1;
                                GlobalVarLn.flPelMain2 = 0;   // 2-й пеленг
                                GlobalVarLn.fl_PelIRI_1 = 0; // 1-й пеленг
                                GlobalVarLn.flag_eq_draw1 = 0;
                                GlobalVarLn.flag_eq_draw2 = 0;
                                GlobalVarLn.PELENGG_1 = -1;
                                GlobalVarLn.PELENGG_2 = -1;

                            }

                            // IRI, есть Repaint
                            ClassMap.f_DelGPS_IRI(0, 2);

                            return;

                        } // bDel=2
                          // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        // PELENG ..................................................................

                        if ((GlobalVarLn.list1_Sost.Count != 0) && (bDel == 3))
                        {
                            if (chbPeleng.Checked == true)
                            {
                                double azgr1 = -1;
                                double azgr2 = -1;

                                if (coordsIRI[0].tDirections.iQ1 != -1)
                                    azgr1 = (double)(coordsIRI[0].tDirections.iQ1 / 10d); // Peleng1, grad
                                if (coordsIRI[0].tDirections.iQ2 != -1)
                                    azgr2 = (double)(coordsIRI[0].tDirections.iQ2 / 10d); // Peleng2

                                if ((coordsIRI[0].tDirections.iQ1 != -1) && (GlobalVarLn.list1_Sost[0].X_m != 0) && (GlobalVarLn.list1_Sost[0].Y_m != 0))
                                    GlobalVarLn.fl_PelIRI_1 = 1;
                                else
                                    GlobalVarLn.fl_PelIRI_1 = 0;

                                if ((coordsIRI[0].tDirections.iQ2 != -1) && (GlobalVarLn.list1_Sost[1].X_m != 0) && (GlobalVarLn.list1_Sost[1].Y_m != 0))
                                    GlobalVarLn.flPelMain2 = 1;
                                else
                                    GlobalVarLn.flPelMain2 = 0;

                                // 28_09_2018
                                GlobalVarLn.PELENGG_1 = azgr1;
                                GlobalVarLn.PELENGG_2 = azgr2;

                                // 28_09_2018
                                if (
                                    (
                                    (Math.Abs(azgr1 - GlobalVarLn.PrevP1)) > 1 ||
                                    (Math.Abs(azgr2 - GlobalVarLn.PrevP2)) > 1 ||
                                    (GlobalVarLn.flag_eq_draw1 == 1) ||
                                    (GlobalVarLn.flag_eq_draw2 == 1)
                                    ) &&
                                    ((GlobalVarLn.fl_PelIRI_1 == 1) || (GlobalVarLn.flPelMain2 == 1))
                                   )
                                {
                                    GlobalVarLn.flag_eq_draw1 = 0;
                                    GlobalVarLn.flag_eq_draw2 = 0;

                                    GlobalVarLn.X1_PelMain = (int)GlobalVarLn.list1_Sost[0].X_m;  // SP1
                                    GlobalVarLn.Y1_PelMain = (int)GlobalVarLn.list1_Sost[0].Y_m;
                                    GlobalVarLn.X1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].X_m; // SP2
                                    GlobalVarLn.Y1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].Y_m;

                                    double a1 = 0;
                                    double b1 = 0;
                                    double c1 = 0;
                                    double d1 = 0;
                                    double f1 = 0;
                                    double g1 = 0;
                                    double a2 = 0;
                                    double b2 = 0;
                                    double c2 = 0;
                                    double d2 = 0;
                                    double f2 = 0;
                                    double g2 = 0;
                                    double x184 = 0;
                                    double y184 = 0;
                                    double x284 = 0;
                                    double y284 = 0;

                                    double[] arr_Pel_XYZ1 = new double[GlobalVarLn.numberofdots * 3];
                                    double[] arr_Pel_XYZ2 = new double[GlobalVarLn.numberofdots * 3];
                                    ClassMap classMap = new ClassMap();
                                    ClassMap classMap1 = new ClassMap();

                                    x184 = GlobalVarLn.X1_PelMain;
                                    y184 = GlobalVarLn.Y1_PelMain;
                                    x284 = GlobalVarLn.X1_1_PelMain;
                                    y284 = GlobalVarLn.Y1_1_PelMain;

                                    mapPlaneToGeo(GlobalVarLn.hmapl, ref x184, ref y184);
                                    mapPlaneToGeo(GlobalVarLn.hmapl, ref x284, ref y284);

                                    // rad->grad
                                    x184 = (x184 * 180) / Math.PI;
                                    y184 = (y184 * 180) / Math.PI;
                                    x284 = (x284 * 180) / Math.PI;
                                    y284 = (y284 * 180) / Math.PI;

                                    // Peleng1
                                    if (GlobalVarLn.fl_PelIRI_1 == 1)

                                    {
                                        classMap.f_Peleng(azgr1, GlobalVarLn.distance, GlobalVarLn.numberofdots, x184, y184,
                                                          ref a1, ref b1, ref c1, ref d1, ref f1, ref g1,
                                                          ref GlobalVarLn.arr_Pel1, ref arr_Pel_XYZ1);
                                    }
                                    // Peleng2
                                    if (GlobalVarLn.flPelMain2 == 1)
                                    {
                                        classMap1.f_Peleng(azgr2, GlobalVarLn.distance, GlobalVarLn.numberofdots, x284, y284,
                                                           ref a2, ref b2, ref c2, ref d2, ref f2, ref g2,
                                                           ref GlobalVarLn.arr_Pel2, ref arr_Pel_XYZ2);
                                    }

                                    axaxcMapScreen1.Repaint();

                                } // !!! См. IF указанное выше - это конец этого IF


                                // 02_10_2018
                                if (
                                    ((azgr1 != GlobalVarLn.PrevP1) && (azgr1 == -1)) ||
                                    ((azgr2 != GlobalVarLn.PrevP2) && (azgr2 == -1))
                                   )
                                {
                                    if (azgr1 == -1)
                                    {
                                        GlobalVarLn.fl_PelIRI_1 = 0; // 1-й пеленг
                                        GlobalVarLn.flag_eq_draw1 = 0;
                                        GlobalVarLn.PELENGG_1 = -1;
                                    }

                                    if (azgr2 == -1)
                                    {
                                        GlobalVarLn.flPelMain2 = 0;   // 2-й пеленг
                                        GlobalVarLn.flag_eq_draw2 = 0;
                                        GlobalVarLn.PELENGG_2 = -1;
                                    }
                                    // Убрать с карты
                                    GlobalVarLn.axMapScreenGlobal.Repaint();
                                }

                                GlobalVarLn.PrevP1 = azgr1;
                                GlobalVarLn.PrevP2 = azgr2;

                            } // Chacked==true

                        } // GlobalVarLn.list1_Sost.Count != 0 && bDel==3
                          // .................................................................. PELENG

                        // IRI ......................................................................
                        // IRI
                        // 02_10_2018

                        //  01_10_2018
                        //byte bDel = coordsIRI.bDelIRI; // 0 - add, 1 - delete 2- del all IRI
                        //if (GlobalVarLn.AdDlGPS_IRI == 2)
                        //    ClassMap.f_DelGPS_IRI(0, 2);

                        if (bDel == 3)
                            return;

                        // FOR1
                        for (int yjy = 0; yjy < coordsIRI.Length; yjy++)
                        {

                            if ((coordsIRI[yjy].dLat != -1) && (coordsIRI[yjy].dLon != -1))
                            {
                                GlobalVarLn.flGPS_IRI = 1;

                                byte bDel1 = coordsIRI[yjy].bDelIRI; // 0 - add, 1 - delete 

                                GlobalVarLn.AdDlGPS_IRI = (int)bDel;
                                GlobalVarLn.LatGPS_IRI = coordsIRI[yjy].dLat;
                                GlobalVarLn.LongGPS_IRI = coordsIRI[yjy].dLon;
                                GlobalVarLn.IDGPS_IRI = coordsIRI[yjy].iID;

                                double freqd = coordsIRI[yjy].iFreq / 10d;

                                GlobalVarLn.sGPS_IRI = Convert.ToString(freqd);

                                GlobalVarLn.ColGPS_IRI = ColorFromSharpString(coordsIRI[yjy].sColorIRI);

                                if (GlobalVarLn.AdDlGPS_IRI == 0)
                                {
                                    ClassMap.f_GetGPS_IRI(GlobalVarLn.IDGPS_IRI);
                                }
                                else
                                {
                                    ClassMap.f_DelGPS_IRI(GlobalVarLn.IDGPS_IRI, bDel);
                                }

                            } // Lat,Long!=-1 

                        } // FOR1
                          // ...................................................................... IRI


                    } // lock (threadLock)
                } // INTERRUPT
                // --------------------------------------------------- Interrupt_Peleng_IRI
        */

        // ----------------------------------------------------------- INTERRUPT_PELENG_IRI_OLD

        // PELENG_IRI_PPRCH_OLD ---------------------------------------------------------------

        // Peleng_IRI_PPRCH *******************************************************
        // INTERRUPT

        //private object threadLock_2 = new object();
        //void service_CoordsIRI_PPRChUpdated(object sender, TCoordsIRI_PPRCh[] coordsIRI)
        //{


        /*
                 // ------------------------------------------------------------------------
                    int countPel = 0;
                    double a1 = 0;
                    double b1 = 0;
                    double c1 = 0;
                    double d1 = 0;
                    double f1 = 0;
                    double g1 = 0;
                    double a2 = 0;
                    double b2 = 0;
                    double c2 = 0;
                    double d2 = 0;
                    double f2 = 0;
                    double g2 = 0;
                    double x184 = 0;
                    double y184 = 0;
                    double x284 = 0;
                    double y284 = 0;

                    double[] arr_Pel_XYZ1 = new double[GlobalVarLn.numberofdots * 3];
                    double[] arr_Pel_XYZ2 = new double[GlobalVarLn.numberofdots * 3];

                    double azgr1 = -1;
                    double azgr2 = -1;
                    PelIRI objPelIRI = new PelIRI();

                    int flg = 0;
                    int ind = 0;

                    int AdDl = 0;
                    //Color clr = Color.Red;
                    // ------------------------------------------------------------------------

                    // ------------------------------------------------------------------------
                    lock (threadLock_2)
                    {

                     AdDl = (int)coordsIRI[countPel].bDelIRI; // 0 - add, 1 - delete;

                    // Delete All .............................................................

                     if (AdDl == 2)
                     {

                      for (int iii20 = (GlobalVarLn.list_PelIRI.Count - 1); iii20 >= 0; iii20--)
                      {
                       GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii20]);
                      }

                      axaxcMapScreen1.Repaint();
                      return;

                     } 
                     // ............................................................. Delete All

                     // Delete ID ..............................................................

                     if (AdDl == 1)
                     {

                         for (int iii21 = (GlobalVarLn.list_PelIRI.Count - 1); iii21 >= 0; iii21--)
                         {
                             if (GlobalVarLn.list_PelIRI[iii21].ID == coordsIRI[countPel].iID)
                               GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii21]);
                         }

                         axaxcMapScreen1.Repaint();
                         return;

                     }
                     // .............................................................. Delete ID

                     // Del_Struct_ID ..........................................................
                    // Сначала удаляем все структуры с таким IDi (со всеми ID из входного массива структур)

                    // FOR1
                    for (countPel = 0; countPel < coordsIRI.Length; countPel++ )
                    {
                        // for2
                        for (int iii = (GlobalVarLn.list_PelIRI.Count - 1); iii >= 0; iii--)
                        {
                            if (GlobalVarLn.list_PelIRI[iii].ID == coordsIRI[countPel].iID)
                                 GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii]);

                        } //for2

                    } //FOR1
                    // .......................................................... Del_Struct_ID

                    // Add_Struct_ID ..........................................................
                    // Потом добавляем в лист массив пришедших структур

                    // FOR1_1
                    for (countPel = 0; countPel < coordsIRI.Length; countPel++)
                    {

                        if (
                             (coordsIRI[countPel].iQ1 != -1) ||
                             ((coordsIRI[countPel].dLatitude != -1) && (coordsIRI[countPel].dLongitude != -1))
                           )
                        {
                            objPelIRI.ID = coordsIRI[countPel].iID;
                            objPelIRI.XGPS_IRI = -1;
                            objPelIRI.YGPS_IRI = -1;
                            objPelIRI.flPelMain2 = 0;
                            objPelIRI.arr_Pel1 = new double[10000];     // R,Широта,долгота
                            objPelIRI.arr_Pel2 = new double[10000];     // R,Широта,долгота

                            if ((coordsIRI[countPel].iQ1 != -1) && (chbPeleng.Checked == true))
                                objPelIRI.Pel1 = (double)(coordsIRI[countPel].iQ1/10d);
                            else
                                objPelIRI.Pel1 = -1;

                            if ((coordsIRI[countPel].iQ2 != -1) && (chbPeleng.Checked == true))
                            {
                                objPelIRI.Pel2 = (double)(coordsIRI[countPel].iQ2 / 10d);
                                objPelIRI.flPelMain2 = 1;
                            }
                            else
                            {
                                objPelIRI.Pel2 = -1;
                                objPelIRI.flPelMain2 = 0;
                            }

                            if ((coordsIRI[countPel].dLatitude != -1) && (coordsIRI[countPel].dLongitude != -1))
                            {
                                objPelIRI.Lat = coordsIRI[countPel].dLatitude;
                                objPelIRI.Long = coordsIRI[countPel].dLongitude;
                                objPelIRI.color = ColorFromSharpString(coordsIRI[countPel].sColorIRI);
                                //objPelIRI.color = clr;

                                objPelIRI.AddDel = (int)coordsIRI[countPel].bDelIRI; // 0 - add, 1 - delete;
                            }
                            else
                            {
                                objPelIRI.Lat = -1;
                                objPelIRI.Long = -1;
                                objPelIRI.XGPS_IRI = -1;
                                objPelIRI.YGPS_IRI = -1;
                                objPelIRI.AddDel = (int)coordsIRI[countPel].bDelIRI; // 0 - add, 1 - delete;

                            }

                            GlobalVarLn.list_PelIRI.Add(objPelIRI);

                        } //IF(есть пеленг или координаты) 

                    } //FOR1_1
                    // .......................................................... Add_Struct_ID

                    // Remove from List .......................................................
                    // Если в элементе List стали все -1

                    // for5
                    for (int iii7 = (GlobalVarLn.list_PelIRI.Count - 1); iii7 >=0; iii7--)
                    {
                        if (
                            (GlobalVarLn.list_PelIRI[iii7].Pel1 == -1)&&
                            (GlobalVarLn.list_PelIRI[iii7].Pel2 == -1)&&
                            (GlobalVarLn.list_PelIRI[iii7].Lat == -1)&&
                            (GlobalVarLn.list_PelIRI[iii7].Long == -1)
                           )
                        {
                            GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii7]);
                        }

                    } //for5
                    // ....................................................... Remove from List

                    // PELENG &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                    // PELENG 

                    if (GlobalVarLn.list1_Sost.Count != 0)
                    {
                     GlobalVarLn.X1_PelMain = (int)GlobalVarLn.list1_Sost[0].X_m;  // SP1
                     GlobalVarLn.Y1_PelMain = (int)GlobalVarLn.list1_Sost[0].Y_m;
                     GlobalVarLn.X1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].X_m; // SP2
                     GlobalVarLn.Y1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].Y_m;

                     if (chbPeleng.Checked == true)
                     {

                      // FOR33 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                      for (countPel = 0; countPel < coordsIRI.Length; countPel++ )
                      {
                       // 28_09_2018
                       if ((coordsIRI[countPel].iQ1 != -1) || (coordsIRI[countPel].iQ2 != -1))
                       {
                          // for55
                          flg = 0;
                          for (int iii1 = 0; iii1 < GlobalVarLn.list_PelIRI.Count; iii1++)
                          {
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // Нашли этот элемент в List
                            // 28_09_2018

                            double nbm = (double)(coordsIRI[countPel].iQ1 / 10d);
                            double nbm2 = (double)(coordsIRI[countPel].iQ2 / 10d);

                            if (
                                (GlobalVarLn.list_PelIRI[iii1].ID == coordsIRI[countPel].iID) &&
                                ((GlobalVarLn.list_PelIRI[iii1].Pel1 == nbm) || (GlobalVarLn.list_PelIRI[iii1].Pel1 == -1 && coordsIRI[countPel].iQ1 == -1)) &&
                                ((GlobalVarLn.list_PelIRI[iii1].Pel2 == nbm2) || (GlobalVarLn.list_PelIRI[iii1].Pel2 == -1 && coordsIRI[countPel].iQ2 == -1))
                               )
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // Нашли этот элемент в List
                            //if (
                            //    (GlobalVarLn.list_PelIRI[iii1].ID == coordsIRI[countPel].iID)&&
                            //    (GlobalVarLn.list_PelIRI[iii1].Pel1 == (coordsIRI[countPel].iQ1/10d))&&
                            //    (GlobalVarLn.list_PelIRI[iii1].Pel2 == (coordsIRI[countPel].iQ2/10d))
                            //   )
                                {
                                 ind = iii1;
                                 flg = 1;
                                 }
                            } //for55

                            // Элемент нашли в листе ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            if (flg == 1)
                            {
                                          if (coordsIRI[countPel].iQ1 != -1)
                                          {
                                              azgr1 = (double)(coordsIRI[countPel].iQ1 / 10d); // Peleng1, grad
                                          }
                                          if (coordsIRI[countPel].iQ2 != -1)
                                          {
                                              azgr2 = (double)(coordsIRI[countPel].iQ2 / 10d); // Peleng2
                                          }

                                              x184 = GlobalVarLn.X1_PelMain;
                                              y184 = GlobalVarLn.Y1_PelMain;
                                              x284 = GlobalVarLn.X1_1_PelMain;
                                              y284 = GlobalVarLn.Y1_1_PelMain;

                                              mapPlaneToGeo(GlobalVarLn.hmapl, ref x184, ref y184);
                                              mapPlaneToGeo(GlobalVarLn.hmapl, ref x284, ref y284);

                                              // rad->grad
                                              x184 = (x184 * 180) / Math.PI;
                                              y184 = (y184 * 180) / Math.PI;
                                              x284 = (x284 * 180) / Math.PI;
                                              y284 = (y284 * 180) / Math.PI;

                                              ClassMap classMap = new ClassMap();
                                              ClassMap classMap1 = new ClassMap();

                                              // 28_09_2018
                                              // Peleng1
                                              if (coordsIRI[countPel].iQ1 != -1)
                                              {
                                                  PelIRI[] mass1 = GlobalVarLn.list_PelIRI.ToArray();
                                                  classMap.f_Peleng(azgr1, GlobalVarLn.distance, GlobalVarLn.numberofdots, x184, y184,
                                                                    ref a1, ref b1, ref c1, ref d1, ref f1, ref g1,
                                                                    ref mass1[ind].arr_Pel1, ref arr_Pel_XYZ1);
                                                  GlobalVarLn.list_PelIRI = mass1.ToList();
                                              } // Peleng1

                                              // Peleng2
                                              if (GlobalVarLn.list_PelIRI[ind].flPelMain2 == 1)
                                              {
                                                  PelIRI[] mass2 = GlobalVarLn.list_PelIRI.ToArray();
                                                  classMap1.f_Peleng(azgr2, GlobalVarLn.distance, GlobalVarLn.numberofdots, x284, y284,
                                                                     ref a2, ref b2, ref c2, ref d2, ref f2, ref g2,
                                                                     ref mass2[ind].arr_Pel2, ref arr_Pel_XYZ2);
                                                  GlobalVarLn.list_PelIRI = mass2.ToList();

                                              } // Peleng2

                           } //IF(flg==1)
                           // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,, Элемент нашли в листе

                         } // if ((coordsIRI[countPel].iQ1 != -1) || (coordsIRI[countPel].iQ2 != -1))
                         } // FOR33
                         // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FOR33


                        } // Chacked==true
                        } // GlobalVarLn.list1_Sost.Count != 0
                        // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& PELENG

                        // IRI &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                        // IRI 

                        // FOR99
                       for (countPel = 0; countPel < coordsIRI.Length; countPel++ )
                       {

                       if ((coordsIRI[countPel].dLatitude != -1) && (coordsIRI[countPel].dLongitude != -1))
                       {
                           // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                          // for66
                          flg = 0;
                          for (int iii2 = 0; iii2 < GlobalVarLn.list_PelIRI.Count; iii2++)
                          {
                              if (
                                  (GlobalVarLn.list_PelIRI[iii2].ID == coordsIRI[countPel].iID)&&
                                  (GlobalVarLn.list_PelIRI[iii2].Lat==coordsIRI[countPel].dLatitude)&&
                                  (GlobalVarLn.list_PelIRI[iii2].Long == coordsIRI[countPel].dLongitude)
                                 )
                              {
                                  ind = iii2;
                                  flg = 1;
                              }

                          } //for66
                          // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

                          // Элемент нашли в листе ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

                          if (flg == 1)
                          {
                                  if (GlobalVarLn.list_PelIRI[ind].AddDel == 0)
                                  {
                                      ClassMap.f_GetGPS_IRI_2(ind);
                                  }

                                  else
                                  {
                                      //ClassMap.f_DelGPS_IRI_2(GlobalVarLn.list_PelIRI[ind].ID);
                                  }

                           } //IF(flg==1)
                           // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,, Элемент нашли в листе

                      } // Lat,Long!=-1 
                      } // FOR99
                       // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& IRI

                      //?????????????????
                      axaxcMapScreen1.Repaint();

                  } // lock (threadLock_2)
                    // ------------------------------------------------------------------------

        */
        // 19_10_2018

        /*
                    // 19_10_2018 ****************************************************************
                    int countPel = 0;
                    double a1 = 0;
                    double b1 = 0;
                    double c1 = 0;
                    double d1 = 0;
                    double f1 = 0;
                    double g1 = 0;
                    double a2 = 0;
                    double b2 = 0;
                    double c2 = 0;
                    double d2 = 0;
                    double f2 = 0;
                    double g2 = 0;
                    double x184 = 0;
                    double y184 = 0;
                    double x284 = 0;
                    double y284 = 0;

                    double[] arr_Pel_XYZ1 = new double[GlobalVarLn.numberofdots * 3];
                    double[] arr_Pel_XYZ2 = new double[GlobalVarLn.numberofdots * 3];

                    double azgr1 = -1;
                    double azgr2 = -1;
                    PelIRI objPelIRI = new PelIRI();
                    PelIRI objPelIRI2 = new PelIRI();  // For IRI

                    int flg = 0;
                    int ind = 0;

                    int AdDl = 0;
                    //Color clr = Color.Red;
                    // ------------------------------------------------------------------------

                    // ------------------------------------------------------------------------
                    lock (threadLock_2)
                    {

                        // !!! Идут только пеленги (флпаг 3)/только ИРИ(флаг 0/1)/ удалить все (флаг 2)
                        AdDl = (int)coordsIRI[0].bDelIRI; 

                        // Delete All ************************************************************
                        // Delete Peleng+IRI

                        if (AdDl == 2)
                        {

                            // Del Peleng
                            for (int iii20 = (GlobalVarLn.list_PelIRI.Count - 1); iii20 >= 0; iii20--)
                            {
                                GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii20]);
                            }
                            // DelIRI
                            for (int iii21 = (GlobalVarLn.list_PelIRI2.Count - 1); iii21 >= 0; iii21--)
                            {
                                GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii21]);
                            }

                            axaxcMapScreen1.Repaint();
                            return;

                        }  // AdDl=2
                        // ************************************************************ Delete All

                        // Peleng ****************************************************************
                        // Del(Q1=Q2=-1)/Add Peleng
                        // !!! Old Peleng delete

                        if (
                            (AdDl == 3)&&
                            (chbPeleng.Checked == true)
                            )
                        {
                            // ........................................................................
                            // Все старые пеленги удалить

                            for (int iii22 = (GlobalVarLn.list_PelIRI.Count - 1); iii22 >= 0; iii22--)
                            {
                                GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii22]);
                            }
                            // ........................................................................

                            // Add_Struct_ID ..........................................................
                            // Потом добавляем в лист массив пришедших структур

                            // FOR1_1
                            for (countPel = 0; countPel < coordsIRI.Length; countPel++)
                            {
                                if (
                                     (coordsIRI[countPel].iQ1 != -1) ||
                                     (coordsIRI[countPel].iQ2 != -1)
                                   )
                                {
                                    objPelIRI.ID = coordsIRI[countPel].iID;
                                    objPelIRI.XGPS_IRI = -1;
                                    objPelIRI.YGPS_IRI = -1;
                                    objPelIRI.flPelMain2 = 0;
                                    objPelIRI.arr_Pel1 = new double[10000];     // R,Широта,долгота
                                    objPelIRI.arr_Pel2 = new double[10000];     // R,Широта,долгота
                                    objPelIRI.Lat = -1;
                                    objPelIRI.Long = -1;
                                    objPelIRI.AddDel = (int)coordsIRI[countPel].bDelIRI; // 3 fore Peleng

                                    if ((coordsIRI[countPel].iQ1 != -1) && (chbPeleng.Checked == true))
                                        objPelIRI.Pel1 = (double)(coordsIRI[countPel].iQ1 / 10d);
                                    else
                                        objPelIRI.Pel1 = -1;

                                    if ((coordsIRI[countPel].iQ2 != -1) && (chbPeleng.Checked == true))
                                    {
                                        objPelIRI.Pel2 = (double)(coordsIRI[countPel].iQ2 / 10d);
                                        objPelIRI.flPelMain2 = 1;
                                    }
                                    else
                                    {
                                        objPelIRI.Pel2 = -1;
                                        objPelIRI.flPelMain2 = 0;
                                    }

                                    GlobalVarLn.list_PelIRI.Add(objPelIRI);

                                } //IF(есть пеленг) 

                            } //FOR1_1
                            // .......................................................... Add_Struct_ID

                            // Calculate  .............................................................

                            GlobalVarLn.X1_PelMain = (int)GlobalVarLn.list1_Sost[0].X_m;  // SP1
                            GlobalVarLn.Y1_PelMain = (int)GlobalVarLn.list1_Sost[0].Y_m;
                            GlobalVarLn.X1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].X_m; // SP2
                            GlobalVarLn.Y1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].Y_m;

                            // FOR33 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            for (countPel = 0; countPel < coordsIRI.Length; countPel++)
                            {

                                if ((coordsIRI[countPel].iQ1 != -1) || (coordsIRI[countPel].iQ2 != -1))
                                {
                                    if (coordsIRI[countPel].iQ1 != -1)
                                    {
                                        azgr1 = (double)(coordsIRI[countPel].iQ1 / 10d); // Peleng1, grad
                                    }
                                    if (coordsIRI[countPel].iQ2 != -1)
                                    {
                                        azgr2 = (double)(coordsIRI[countPel].iQ2 / 10d); // Peleng2
                                    }

                                    x184 = GlobalVarLn.X1_PelMain;
                                    y184 = GlobalVarLn.Y1_PelMain;
                                    x284 = GlobalVarLn.X1_1_PelMain;
                                    y284 = GlobalVarLn.Y1_1_PelMain;

                                    mapPlaneToGeo(GlobalVarLn.hmapl, ref x184, ref y184);
                                    mapPlaneToGeo(GlobalVarLn.hmapl, ref x284, ref y284);

                                    // rad->grad
                                    x184 = (x184 * 180) / Math.PI;
                                    y184 = (y184 * 180) / Math.PI;
                                    x284 = (x284 * 180) / Math.PI;
                                    y284 = (y284 * 180) / Math.PI;

                                    ClassMap classMap = new ClassMap();
                                    ClassMap classMap1 = new ClassMap();

                                    // Peleng1
                                    if (coordsIRI[countPel].iQ1 != -1)
                                    {
                                        PelIRI[] mass1 = GlobalVarLn.list_PelIRI.ToArray();
                                        classMap.f_Peleng(azgr1, GlobalVarLn.distance, GlobalVarLn.numberofdots, x184, y184,
                                                          ref a1, ref b1, ref c1, ref d1, ref f1, ref g1,
                                                          ref mass1[countPel].arr_Pel1, ref arr_Pel_XYZ1);
                                        GlobalVarLn.list_PelIRI = mass1.ToList();
                                    } // Peleng1

                                    // Peleng2
                                    if (GlobalVarLn.list_PelIRI[countPel].flPelMain2 == 1)
                                    {
                                        PelIRI[] mass2 = GlobalVarLn.list_PelIRI.ToArray();
                                        classMap1.f_Peleng(azgr2, GlobalVarLn.distance, GlobalVarLn.numberofdots, x284, y284,
                                                           ref a2, ref b2, ref c2, ref d2, ref f2, ref g2,
                                                           ref mass2[countPel].arr_Pel2, ref arr_Pel_XYZ2);
                                        GlobalVarLn.list_PelIRI = mass2.ToList();

                                    } // Peleng2


                                } // if ((coordsIRI[countPel].iQ1 != -1) || (coordsIRI[countPel].iQ2 != -1))

                            } // FOR33
                            //  ............................................................. Calculate

                            axaxcMapScreen1.Repaint();
                            return;

                        }  // AdDl=3chbPeleng.Checked == true
                        // **************************************************************** Peleng

                       // IRI ********************************************************************

                        // Del_Struct_ID ..........................................................
                        // Сначала удаляем все структуры с таким IDi (со всеми ID из входного массива структур)

                        // FOR1
                        for (countPel = 0; countPel < coordsIRI.Length; countPel++)
                        {
                            // for2
                            for (int iii = (GlobalVarLn.list_PelIRI2.Count - 1); iii >= 0; iii--)
                            {
                                if (GlobalVarLn.list_PelIRI2[iii].ID == coordsIRI[countPel].iID)
                                    GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii]);

                            } //for2

                        } //FOR1
                        // .......................................................... Del_Struct_ID

                        // Add_Struct_ID ..........................................................
                        // Потом добавляем в лист массив пришедших структур

                        // FOR1_5
                        for (countPel = 0; countPel < coordsIRI.Length; countPel++)
                        {
                            int ffg=(int)coordsIRI[countPel].bDelIRI;

                            if (
                                 (coordsIRI[countPel].dLatitude != -1) &&
                                 (coordsIRI[countPel].dLongitude != -1)&&
                                 (ffg==0)
                               )
                            {
                                objPelIRI2.ID = coordsIRI[countPel].iID;
                                objPelIRI2.XGPS_IRI = -1;
                                objPelIRI2.YGPS_IRI = -1;
                                objPelIRI2.flPelMain2 = 0;
                                objPelIRI2.arr_Pel1 = new double[10000];     // R,Широта,долгота
                                objPelIRI2.arr_Pel2 = new double[10000];     // R,Широта,долгота
                                objPelIRI2.Pel1 = -1;
                                objPelIRI2.Pel2 = -1;

                                if ((coordsIRI[countPel].dLatitude != -1) && (coordsIRI[countPel].dLongitude != -1))
                                {
                                    objPelIRI2.Lat = coordsIRI[countPel].dLatitude;
                                    objPelIRI2.Long = coordsIRI[countPel].dLongitude;
                                    objPelIRI2.color = ColorFromSharpString(coordsIRI[countPel].sColorIRI);
                                    objPelIRI2.AddDel = (int)coordsIRI[countPel].bDelIRI; // 0 - add, 1 - delete;
                                }
                                else
                                {
                                    objPelIRI2.Lat = -1;
                                    objPelIRI2.Long = -1;
                                    objPelIRI2.XGPS_IRI = -1;
                                    objPelIRI2.YGPS_IRI = -1;
                                    objPelIRI2.AddDel = (int)coordsIRI[countPel].bDelIRI; // 0 - add, 1 - delete;

                                }

                                GlobalVarLn.list_PelIRI2.Add(objPelIRI2);

                                ClassMap.f_GetGPS_IRI_2(GlobalVarLn.list_PelIRI2.Count-1);

                            } //IF(есть координаты) 

                        } //FOR1_5
                        // .......................................................... Add_Struct_ID

                        axaxcMapScreen1.Repaint();

                        // ******************************************************************** IRI

                    } // threadLock_2
                    // **************************************************************** 19_10_2018
          */

        //    } // INTERRUPT
        // ******************************************************* Peleng_IRI_PPRCH

        // --------------------------------------------------------------- PELENG_IRI_PPRCH_OLD

        // BUTTON11 ----------------------------------------------------------------------------
        // 3
        /*
                    if (GlobalVarLn.shtmr1 == 0)
                    {
                        timer1.Start();
                        GlobalVarLn.shtmr1 += 1;
                        return;
                    }
                    else
                    {
                        timer1.Stop();
                        GlobalVarLn.shtmr1 -= 1;
                        return;
                    }
        */

        // 3
        /*
                    TCoordsIRI_PPRCh coordsIRI = new TCoordsIRI_PPRCh();
                    List<TCoordsIRI_PPRCh> listCoordsIRI_PPRCh = new List<TCoordsIRI_PPRCh>();
                    Color clr = Color.Red;
        */

        // 3
        /*
                    if (GlobalVarLn.shPelIRI == 0)
                    {
                        coordsIRI.bDelIRI = 0;
                        coordsIRI.iID = 1;
                        coordsIRI.dLatitude = 56.649194;
                        coordsIRI.dLongitude = 24.388778;
                        listCoordsIRI_PPRCh.Add(coordsIRI);

                        coordsIRI.bDelIRI = 0;
                        coordsIRI.iID = 2;
                        coordsIRI.dLatitude = 56.650417;
                        coordsIRI.dLongitude = 24.454536;
                        listCoordsIRI_PPRCh.Add(coordsIRI);

                        coordsIRI.bDelIRI = 0;
                        coordsIRI.iID = 3;
                        coordsIRI.dLatitude = 56.613391;
                        coordsIRI.dLongitude = 24.39187;
                        listCoordsIRI_PPRCh.Add(coordsIRI);

                        coordsIRI.bDelIRI = 0;
                        coordsIRI.iID = 7;
                        coordsIRI.dLatitude = 56.614923;
                        coordsIRI.dLongitude = 24.457059;
                        listCoordsIRI_PPRCh.Add(coordsIRI);

                        GlobalVarLn.shPelIRI += 1;
                    }

                    else
                    {
                        for (int i = (listCoordsIRI_PPRCh.Count - 1); i >= 0; i--)
                        {
                            listCoordsIRI_PPRCh.Remove(listCoordsIRI_PPRCh[i]);
                        }


                        coordsIRI.bDelIRI = 0;
                        coordsIRI.iID = 4;
                        coordsIRI.dLatitude = 56.652315;
                        coordsIRI.dLongitude = 24.518753;
                        listCoordsIRI_PPRCh.Add(coordsIRI);

                        coordsIRI.bDelIRI = 0;
                        coordsIRI.iID = 2;
                        coordsIRI.dLatitude = 56.653399;
                        coordsIRI.dLongitude = 24.58377;
                        listCoordsIRI_PPRCh.Add(coordsIRI);

                        coordsIRI.bDelIRI = 0;
                        coordsIRI.iID = 5;
                        coordsIRI.dLatitude = 56.617228;
                        coordsIRI.dLongitude = 24.587116;
                        listCoordsIRI_PPRCh.Add(coordsIRI);

                        GlobalVarLn.shPelIRI -= 1;
                    }

                    double dds = 250; // m

                    int AdDl = 0;
                    int countPel = 0;
                    int countPel1 = 0;

                    // !!! Идут только пеленги (флпаг 3)/только ИРИ(флаг 0/1)/ удалить все (флаг 2)
                    AdDl = (int)listCoordsIRI_PPRCh[0].bDelIRI;

                    PelIRI objPelIRI2 = new PelIRI();  // For IRI

                    // Delete All ************************************************************
                    // Delete Peleng+IRI

                    if (AdDl == 2)
                    {

                        // Del Peleng
                        for (int iii20 = (GlobalVarLn.list_PelIRI.Count - 1); iii20 >= 0; iii20--)
                        {
                            GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii20]);
                        }
                        // DelIRI
                        for (int iii21 = (GlobalVarLn.list_PelIRI2.Count - 1); iii21 >= 0; iii21--)
                        {
                            GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii21]);
                        }

                        axaxcMapScreen1.Repaint();
                        return;

                    }  // AdDl=2
                       // ************************************************************ Delete All

                    // Delete ID *************************************************************
                    // Удалить конкретный ID

                    if (AdDl == 1)
                    {
                        // Del_Struct_ID ..........................................................
                        // удаляем все структуры с таким IDi 
                        // Придет в 

                        // FOR111
                        for (countPel = 0; countPel < listCoordsIRI_PPRCh.Count; countPel++)
                        {
                            int ffg1 = (int)listCoordsIRI_PPRCh[countPel].bDelIRI;

                            if (ffg1 == 1) // Del
                            {
                                // For22
                                for (int iii33 = (GlobalVarLn.list_PelIRI2.Count - 1); iii33 >= 0; iii33--)
                                {
                                    if (GlobalVarLn.list_PelIRI2[iii33].ID == listCoordsIRI_PPRCh[countPel].iID)
                                        GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii33]);

                                } //for22

                            } // IF

                        } // FOR111

                        axaxcMapScreen1.Repaint();
                        return;

                    } // AdDl==1 Del
                      // ************************************************************* Delete ID

                    // Add IRI ****************************************************************

                    if (AdDl == 0)
                    {

                        // ------------------------------------------------------------------------
                        // 2504

                        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        bool SignRepaint = false;
                        int i1 = 0;
                        int i2 = 0;
                        int i3 = 0;
                        int i4 = 0;
                        bool flexist = false;
                        int ffg = 0;
                        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        // Убрать в старом те, которых нет в новых

                        for (i1 = (GlobalVarLn.list_PelIRI2.Count - 1); i1 >= 0; i1--) // old
                        {
                            flexist = false;
                            for (i2 = 0; i2 < listCoordsIRI_PPRCh.Count; i2++) // new
                            {
                                ffg = (int)listCoordsIRI_PPRCh[i2].bDelIRI;

                                if (
                                    (GlobalVarLn.list_PelIRI2[i1].ID == listCoordsIRI_PPRCh[i2].iID) &&
                                    (listCoordsIRI_PPRCh[i2].dLatitude != -1) &&
                                    (listCoordsIRI_PPRCh[i2].dLongitude != -1) &&
                                    (ffg == 0)
                                   )
                                {
                                    flexist = true;
                                }

                            } // i2

                            if (flexist == false)
                            {
                                GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[i1]);
                                SignRepaint = true;
                                //i1 = -1;
                            }

                        } // i1 old

                        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        // 

                        // FOR1_5 вперед по новому
                        for (countPel = 0; countPel < listCoordsIRI_PPRCh.Count; countPel++) // new
                        {

                            ffg = (int)listCoordsIRI_PPRCh[countPel].bDelIRI;

                            if (
                                 (listCoordsIRI_PPRCh[countPel].dLatitude != -1) &&
                                 (listCoordsIRI_PPRCh[countPel].dLongitude != -1) &&
                                 (ffg == 0)
                               )
                            {

                                flexist = false;
                                for (int iii99 = (GlobalVarLn.list_PelIRI2.Count - 1); iii99 >= 0; iii99--)
                                {

                                    if (GlobalVarLn.list_PelIRI2[iii99].ID == listCoordsIRI_PPRCh[countPel].iID)
                                    {
                                        // есть с таким ID
                                        flexist = true;
                                        double sss = 0;
                                        sss = ClassMap.f_D_2Points(GlobalVarLn.list_PelIRI2[iii99].Lat, GlobalVarLn.list_PelIRI2[iii99].Long, listCoordsIRI_PPRCh[countPel].dLatitude, listCoordsIRI_PPRCh[countPel].dLongitude, 1);
                                        if (sss > dds)
                                        {
                                            SignRepaint = true;
                                            // Убираем старый
                                            GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii99]);

                                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                            // добавляем новый

                                            objPelIRI2.ID = listCoordsIRI_PPRCh[countPel].iID;
                                            objPelIRI2.XGPS_IRI = -1;
                                            objPelIRI2.YGPS_IRI = -1;
                                            objPelIRI2.flPelMain2 = 0;
                                            objPelIRI2.arr_Pel1 = new double[10000];     // R,Широта,долгота
                                            objPelIRI2.arr_Pel2 = new double[10000];     // R,Широта,долгота
                                            objPelIRI2.Pel1 = -1;
                                            objPelIRI2.Pel2 = -1;

                                            if ((listCoordsIRI_PPRCh[countPel].dLatitude != -1) && (listCoordsIRI_PPRCh[countPel].dLongitude != -1))
                                            {
                                                objPelIRI2.Lat = listCoordsIRI_PPRCh[countPel].dLatitude;
                                                objPelIRI2.Long = listCoordsIRI_PPRCh[countPel].dLongitude;
                                                objPelIRI2.color = clr;
                                                objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;
                                            }
                                            else
                                            {
                                                objPelIRI2.Lat = -1;
                                                objPelIRI2.Long = -1;
                                                objPelIRI2.XGPS_IRI = -1;
                                                objPelIRI2.YGPS_IRI = -1;
                                                objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;

                                            }

                                            GlobalVarLn.list_PelIRI2.Add(objPelIRI2);

                                            ClassMap.f_GetGPS_IRI_2(GlobalVarLn.list_PelIRI2.Count - 1);

                                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                                        } // S>ds

                                    }  // ID==

                                }  // for old назад

                                if (flexist == false) // это новый ID -> добавляем его
                                {
                                    SignRepaint = true;

                                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                    // добавляем новый

                                    objPelIRI2.ID = listCoordsIRI_PPRCh[countPel].iID;
                                    objPelIRI2.XGPS_IRI = -1;
                                    objPelIRI2.YGPS_IRI = -1;
                                    objPelIRI2.flPelMain2 = 0;
                                    objPelIRI2.arr_Pel1 = new double[10000];     // R,Широта,долгота
                                    objPelIRI2.arr_Pel2 = new double[10000];     // R,Широта,долгота
                                    objPelIRI2.Pel1 = -1;
                                    objPelIRI2.Pel2 = -1;

                                    if ((listCoordsIRI_PPRCh[countPel].dLatitude != -1) && (listCoordsIRI_PPRCh[countPel].dLongitude != -1))
                                    {
                                        objPelIRI2.Lat = listCoordsIRI_PPRCh[countPel].dLatitude;
                                        objPelIRI2.Long = listCoordsIRI_PPRCh[countPel].dLongitude;
                                        objPelIRI2.color = clr;
                                        objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;
                                    }
                                    else
                                    {
                                        objPelIRI2.Lat = -1;
                                        objPelIRI2.Long = -1;
                                        objPelIRI2.XGPS_IRI = -1;
                                        objPelIRI2.YGPS_IRI = -1;
                                        objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;

                                    }

                                    GlobalVarLn.list_PelIRI2.Add(objPelIRI2);

                                    ClassMap.f_GetGPS_IRI_2(GlobalVarLn.list_PelIRI2.Count - 1);

                                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                                } // // это новый ID

                            } //IF(есть координаты) 

                        } //FOR1_5 new
                          // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                        if (SignRepaint == true)
                            axaxcMapScreen1.Repaint();


                    } // AdDl==0
                      // **************************************************************** Add IRI
        */

        // ---------------------------------------------------------------------------- BUTTON11

        // GPS_OLD -----------------------------------------------------------------------------
        // GPS_SOST ***************************************************************
        // GPSSPPU
        // !!! Get to (WGS84)

        // 3
        /*
                public void service_CurrentCoordsUpdated(object sender, TCoordsGNSS[] coordsGNSS)
                {

                    // NEW *******************************************************************

                    // -----------------------------------------------------------------------
                    // Нажата ли птичка на АРМ (посылка координат от GPS)

                    TCoordsGNSS[] coordsGNSS_1 = new TCoordsGNSS[3];
                    coordsGNSS_1[0].Lat = coordsGNSS[0].Lat;
                    coordsGNSS_1[0].Lon = coordsGNSS[0].Lon;
                    if (coordsGNSS.Length == 2)
                    {
                        coordsGNSS_1[1].Lat = coordsGNSS[1].Lat;
                        coordsGNSS_1[1].Lon = coordsGNSS[1].Lon;
                    }
                    else if (coordsGNSS.Length == 3)
                    {
                        coordsGNSS_1[1].Lat = coordsGNSS[1].Lat;
                        coordsGNSS_1[1].Lon = coordsGNSS[1].Lon;
                        coordsGNSS_1[2].Lat = coordsGNSS[2].Lat;
                        coordsGNSS_1[2].Lon = coordsGNSS[2].Lon;
                    }

                    // -----------------------------------------------------------------------
                    // Пошли координаты -1

                    LF objLF = new LF();
                    int fsp1_1 = 0;
                    int fsp2_1 = 0;
                    int fpu_1 = 0;

                    // ..................................................................
                    // SP1 -> идут -1

                    if (
                       //(fl == true) &&
                       (GlobalVarLn.bCheckGNSS) &&
                       //777Otl
                       ((coordsGNSS_1[0].Lat == -1) || (coordsGNSS_1[0].Lon == -1))
                      )
                    {
                        // раньше были координаты (свои/GPS)
                        if (
                           (GlobalVarLn.XCenterGRAD_Dubl_Sost != -1) &&
                           (GlobalVarLn.YCenterGRAD_Dubl_Sost != -1)
                           )
                        {
                            GlobalVarLn.XCenterGRAD_Sost = -1;
                            GlobalVarLn.YCenterGRAD_Sost = -1;
                            GlobalVarLn.XCenterGRAD_Dubl_Sost = -1;
                            GlobalVarLn.YCenterGRAD_Dubl_Sost = -1;
                            GlobalVarLn.XCenter_Sost = 0;
                            GlobalVarLn.YCenter_Sost = 0;
                            GlobalVarLn.HCenter_Sost = 0;

                            objLF.X_m = 0;
                            objLF.Y_m = 0;
                            objLF.H_m = 0;

                            objLF.indzn = iniRW.get_ZnakSP1();
                            GlobalVarLn.indz1_Sost = objLF.indzn;
                            GlobalVarLn.objFormSostG.pbSP.Image = GlobalVarLn.objFormSostG.imageList1.Images[GlobalVarLn.indz1_Sost];

                            GlobalVarLn.list1_Sost[0] = objLF;

                            GlobalVarLn.flCoordSP_Sost = 0;
                            fsp1_1 = 1;

                            GlobalVarLn.objFormSostG.tbXRect.Text = "";
                            GlobalVarLn.objFormSostG.tbYRect.Text = "";
                            GlobalVarLn.objFormSostG.tbXRect42.Text = "";
                            GlobalVarLn.objFormSostG.tbYRect42.Text = "";
                            GlobalVarLn.objFormSostG.tbBRad.Text = "";
                            GlobalVarLn.objFormSostG.tbLRad.Text = "";
                            GlobalVarLn.objFormSostG.tbBMin1.Text = "";
                            GlobalVarLn.objFormSostG.tbLMin1.Text = "";
                            GlobalVarLn.objFormSostG.tbBDeg2.Text = "";
                            GlobalVarLn.objFormSostG.tbBMin2.Text = "";
                            GlobalVarLn.objFormSostG.tbBSec.Text = "";
                            GlobalVarLn.objFormSostG.tbLDeg2.Text = "";
                            GlobalVarLn.objFormSostG.tbLMin2.Text = "";
                            GlobalVarLn.objFormSostG.tbLSec.Text = "";

                            GlobalVarLn.objFormSostG.tbOwnHeight.Text = "";

                            // --------------------------------------------------------------------------------------
                            // 14_09_2018
                            // Для пеленгов

                            // Нажата птичка для отрисовки пеленгов
                            if (GlobalVarLn.flPelMain == 1)
                            {
                                // FRCH
                                GlobalVarLn.PrevP1 = -1;
                                GlobalVarLn.fl_PelIRI_1 = 0; // 1-й пеленг
                                GlobalVarLn.flag_eq_draw1 = 0;

                                //------------------------------------------------------------------------
                                //PPRCH
                                for (int iii = (GlobalVarLn.list_PelIRI.Count - 1); iii >= 0; iii--)
                                {
                                    PelIRI[] mass = GlobalVarLn.list_PelIRI.ToArray();
                                    mass[iii].Pel1 = -1;
                                    GlobalVarLn.list_PelIRI = mass.ToList();

                                    if (
                                        (GlobalVarLn.list_PelIRI[iii].Pel1 == -1) &&
                                        (GlobalVarLn.list_PelIRI[iii].Pel2 == -1) &&
                                        (GlobalVarLn.list_PelIRI[iii].Lat == -1) &&
                                        (GlobalVarLn.list_PelIRI[iii].Long == -1)
                                       )
                                    {
                                        GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii]);
                                    }
                                }

                            }
                            //------------------------------------------------------------------------
                        }

                    }  // -1 SP1
                    // ..................................................................
                    // SP2

                    if (
                       (GlobalVarLn.bCheckGNSS) &&
                        //777Otl
                        ((coordsGNSS_1[1].Lat == -1) || (coordsGNSS_1[1].Lon == -1))
                      )
                    {
                        // раньше были координаты (свои/GPS)
                        if (
                           (GlobalVarLn.XPoint1GRAD_Dubl_Sost != -1) &&
                           (GlobalVarLn.YPoint1GRAD_Dubl_Sost != -1)
                           )
                        {
                            GlobalVarLn.XPoint1GRAD_Sost = -1;
                            GlobalVarLn.YPoint1GRAD_Sost = -1;
                            GlobalVarLn.XPoint1GRAD_Dubl_Sost = -1;
                            GlobalVarLn.YPoint1GRAD_Dubl_Sost = -1;
                            GlobalVarLn.XPoint1_Sost = 0;
                            GlobalVarLn.YPoint1_Sost = 0;
                            GlobalVarLn.HPoint1_Sost = 0;

                            objLF.X_m = 0;
                            objLF.Y_m = 0;
                            objLF.H_m = 0;

                            // 13_09_2018
                            //objLF.indzn = 0;
                            GlobalVarLn.indz2_Sost = iniRW.get_ZnakSP2();
                            GlobalVarLn.objFormSostG.pictureBox1.Image = GlobalVarLn.objFormSostG.imageList2.Images[GlobalVarLn.indz2_Sost];
                            objLF.indzn = GlobalVarLn.indz2_Sost;

                            GlobalVarLn.list1_Sost[1] = objLF;

                            GlobalVarLn.flCoordYS1_Sost = 0;
                            fsp2_1 = 1;

                            GlobalVarLn.objFormSostG.tbPt1XRect.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1YRect.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1XRect42.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1YRect42.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1BRad.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1LRad.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1BMin1.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1LMin1.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1BDeg2.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1BMin2.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1BSec.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1LDeg2.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1LMin2.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1LSec.Text = "";

                            GlobalVarLn.objFormSostG.tbPt1Height.Text = "";

                            // --------------------------------------------------------------------------------------
                            // 14_09_2018
                            // Для пеленгов

                            // Нажата птичка для отрисовки пеленгов
                            if (GlobalVarLn.flPelMain == 1)
                            {
                                // FRCH
                                GlobalVarLn.PrevP2 = -1;
                                GlobalVarLn.flPelMain2 = 0;   // 2-й пеленг
                                GlobalVarLn.flag_eq_draw2 = 0;
                                //------------------------------------------------------------------------
                                // PPRCH
                                for (int iii = (GlobalVarLn.list_PelIRI.Count - 1); iii >= 0; iii--)
                                {


                                    PelIRI[] mass = GlobalVarLn.list_PelIRI.ToArray();
                                    mass[iii].Pel2 = -1;
                                    GlobalVarLn.list_PelIRI = mass.ToList();

                                    if (
                                        (GlobalVarLn.list_PelIRI[iii].Pel1 == -1) &&
                                        (GlobalVarLn.list_PelIRI[iii].Pel2 == -1) &&
                                        (GlobalVarLn.list_PelIRI[iii].Lat == -1) &&
                                        (GlobalVarLn.list_PelIRI[iii].Long == -1)
                                       )
                                    {
                                        GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii]);
                                    }
                                }
                                //------------------------------------------------------------------------

                                // Убрать с карты
                                //GlobalVarLn.axMapScreenGlobal.Repaint();

                            } // Нажата птичка для отрисовки пеленгов
                              // --------------------------------------------------------------------------------------


                        }

                    }  // -1 SP2
                    // ..................................................................
                    // PU


                    if (
                       //(fl == true) &&
                       (GlobalVarLn.bCheckGNSS) &&
                        //777Otl
                        ((coordsGNSS_1[2].Lat == -1) || (coordsGNSS_1[2].Lon == -1))
                      )
                    {
                        // раньше были координаты (свои/GPS)
                        if (
                           (GlobalVarLn.XPoint2GRAD_Dubl_Sost != -1) &&
                           (GlobalVarLn.YPoint2GRAD_Dubl_Sost != -1)
                           )
                        {
                            GlobalVarLn.XPoint2GRAD_Sost = -1;
                            GlobalVarLn.YPoint2GRAD_Sost = -1;
                            GlobalVarLn.XPoint2GRAD_Dubl_Sost = -1;
                            GlobalVarLn.YPoint2GRAD_Dubl_Sost = -1;
                            GlobalVarLn.XPoint2_Sost = 0;
                            GlobalVarLn.YPoint2_Sost = 0;
                            GlobalVarLn.HPoint2_Sost = 0;

                            objLF.X_m = 0;
                            objLF.Y_m = 0;
                            objLF.H_m = 0;

                            // 13_09_2018
                            //objLF.indzn = 0;
                            // !!! Значки читаем с INI файла
                            GlobalVarLn.indz3_Sost = iniRW.get_ZnakPU();
                            GlobalVarLn.objFormSostG.pictureBox2.Image = GlobalVarLn.objFormSostG.imageList3.Images[GlobalVarLn.indz3_Sost];
                            objLF.indzn = GlobalVarLn.indz3_Sost;

                            GlobalVarLn.list1_Sost[2] = objLF;

                            GlobalVarLn.flCoordYS2_Sost = 0;

                            fpu_1 = 1;
                            GlobalVarLn.objFormSostG.tbPt2XRect.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2YRect.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2XRect42.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2YRect42.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2BRad.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2LRad.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2BMin1.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2LMin1.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2BDeg2.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2BMin2.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2BSec.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2LDeg2.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2LMin2.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2LSec.Text = "";
                            //GlobalVarLn.objFormSostG.tbOwnHeight.Text = "";
                            //GlobalVarLn.objFormSostG.tbPt1Height.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2Height.Text = "";

                        }

                    }  // -1 PU
                    // ..................................................................

                    if ((fsp1_1 == 1) || (fsp2_1 == 1) || (fpu_1 == 1))
                    {
                        // Убрать с карты
                        GlobalVarLn.axMapScreenGlobal.Repaint();
                        // Redraw
                        ClassMap.f_Map_Redraw_Sost();

                    }
                    // ..................................................................

                    // ------------------------------------------------------------------------
                    // Checked==true

                    if (
                        (GlobalVarLn.bCheckGNSS) &&
                        (
                        //777Otl
                        ((coordsGNSS_1[0].Lat != -1) && (coordsGNSS_1[0].Lon != -1)) ||
                        ((coordsGNSS_1[1].Lat != -1) && (coordsGNSS_1[1].Lon != -1)) ||
                        ((coordsGNSS_1[2].Lat != -1) && (coordsGNSS_1[2].Lon != -1))

                        )
                        )
                    {
                        // .......................................................................
                        // 1-й раз

                        if ((GlobalVarLn.fl_First_GPS == 0))
                        {

                            GlobalVarLn.fl_First_GPS = 1;

                            // SP1
                            //777Otl
                            if ((coordsGNSS_1[0].Lat != -1) && (coordsGNSS_1[0].Lon != -1))

                            {
                                //777Otl
                                GlobalVarLn.XCenterGRAD_Sost = coordsGNSS_1[0].Lat;
                                GlobalVarLn.YCenterGRAD_Sost = coordsGNSS_1[0].Lon;

                                GlobalVarLn.XCenterGRAD_Dubl_Sost = GlobalVarLn.XCenterGRAD_Sost;
                                GlobalVarLn.YCenterGRAD_Dubl_Sost = GlobalVarLn.YCenterGRAD_Sost;
                            }

                            // SP2
                            //777Otl
                            //if ((GlobalVarLn.lll[1].Lat != -1) && (GlobalVarLn.lll[1].Long != -1))
                            if ((coordsGNSS_1[1].Lat != -1) && (coordsGNSS_1[1].Lon != -1))
                            {
                                GlobalVarLn.XPoint1GRAD_Sost = coordsGNSS_1[1].Lat;
                                GlobalVarLn.YPoint1GRAD_Sost = coordsGNSS_1[1].Lon;

                                GlobalVarLn.XPoint1GRAD_Dubl_Sost = GlobalVarLn.XPoint1GRAD_Sost;
                                GlobalVarLn.YPoint1GRAD_Dubl_Sost = GlobalVarLn.YPoint1GRAD_Sost;
                            }

                            // PU
                            //777Otl
                            if ((coordsGNSS_1[2].Lat != -1) && (coordsGNSS_1[2].Lon != -1))
                            {
                                //777Otl
                                GlobalVarLn.XPoint2GRAD_Sost = coordsGNSS_1[2].Lat;
                                GlobalVarLn.YPoint2GRAD_Sost = coordsGNSS_1[2].Lon;

                                GlobalVarLn.XPoint2GRAD_Dubl_Sost = GlobalVarLn.XPoint2GRAD_Sost;
                                GlobalVarLn.YPoint2GRAD_Dubl_Sost = GlobalVarLn.YPoint2GRAD_Sost;
                            }

                            ClassMap.f_GetGPS();

                        } // 1-й раз
                        // .......................................................................
                        // НЕ 1-й раз
                        // 2504

                        else
                        {

                            int fsp1 = 0;
                            int fsp2 = 0;
                            int fpu = 0;
                            double dds = 30; // m
                            double sss = 0;

                            // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                            // SP1
                            if ((coordsGNSS_1[0].Lat != -1) && (coordsGNSS_1[0].Lon != -1))
                            {
                                sss = ClassMap.f_D_2Points(GlobalVarLn.XCenterGRAD_Dubl_Sost, GlobalVarLn.YCenterGRAD_Dubl_Sost, GlobalVarLn.XCenterGRAD_Sost, GlobalVarLn.YCenterGRAD_Sost, 1);

                                //Положение SP1 изменилось
                                if (
                                    ((coordsGNSS_1[0].Lat != GlobalVarLn.XCenterGRAD_Dubl_Sost) ||
                                    (coordsGNSS_1[0].Lon != GlobalVarLn.YCenterGRAD_Dubl_Sost)) &&
                                    (sss > dds)
                                   )
                                //   //(GlobalVarLn.flTmrGPS == 1)
                                //  )
                                {
                                    fsp1 = 1;

                                    GlobalVarLn.XCenterGRAD_Sost = coordsGNSS_1[0].Lat;
                                    GlobalVarLn.YCenterGRAD_Sost = coordsGNSS_1[0].Lon;

                                    GlobalVarLn.XCenterGRAD_Dubl_Sost = GlobalVarLn.XCenterGRAD_Sost;
                                    GlobalVarLn.YCenterGRAD_Dubl_Sost = GlobalVarLn.YCenterGRAD_Sost;

                                } // Положение SP1 изменилось

                            } // SP1
                            // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                            // SP2

                            if ((coordsGNSS_1[1].Lat != -1) && (coordsGNSS_1[1].Lon != -1))

                            {
                                sss = ClassMap.f_D_2Points(GlobalVarLn.XPoint1GRAD_Dubl_Sost, GlobalVarLn.YPoint1GRAD_Dubl_Sost, GlobalVarLn.XPoint1GRAD_Sost, GlobalVarLn.YPoint1GRAD_Sost, 1);

                                //Положение SP2 изменилось
                                if (
                                    ((coordsGNSS_1[1].Lat != GlobalVarLn.XPoint1GRAD_Dubl_Sost) ||
                                    (coordsGNSS_1[1].Lon != GlobalVarLn.YPoint1GRAD_Dubl_Sost)) &&
                                    (sss > dds)
                                   )
                                //    //(GlobalVarLn.flTmrGPS == 1)
                                //   )

                                {
                                    fsp2 = 1;

                                    GlobalVarLn.XPoint1GRAD_Sost = coordsGNSS_1[1].Lat;
                                    GlobalVarLn.YPoint1GRAD_Sost = coordsGNSS_1[1].Lon;

                                    GlobalVarLn.XPoint1GRAD_Dubl_Sost = GlobalVarLn.XPoint1GRAD_Sost;
                                    GlobalVarLn.YPoint1GRAD_Dubl_Sost = GlobalVarLn.YPoint1GRAD_Sost;

                                } // Положение SP2 изменилось

                            } // SP2
                            // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                            // PU

                            if ((coordsGNSS_1[2].Lat != -1) && (coordsGNSS_1[2].Lon != -1))

                            {
                                sss = ClassMap.f_D_2Points(GlobalVarLn.XPoint2GRAD_Dubl_Sost, GlobalVarLn.YPoint2GRAD_Dubl_Sost, GlobalVarLn.XPoint2GRAD_Sost, GlobalVarLn.YPoint2GRAD_Sost, 1);

                                //Положение PU изменилось
                                if (
                                    ((coordsGNSS_1[2].Lat != GlobalVarLn.XPoint2GRAD_Dubl_Sost) ||
                                    (coordsGNSS_1[2].Lon != GlobalVarLn.YPoint2GRAD_Dubl_Sost)) &&
                                    (sss > dds)
                                   )
                                //   //(GlobalVarLn.flTmrGPS == 1)
                                //  )
                                {
                                    fpu = 1;
                                    GlobalVarLn.XPoint2GRAD_Sost = coordsGNSS_1[2].Lat;
                                    GlobalVarLn.YPoint2GRAD_Sost = coordsGNSS_1[2].Lon;

                                    GlobalVarLn.XPoint2GRAD_Dubl_Sost = GlobalVarLn.XPoint2GRAD_Sost;
                                    GlobalVarLn.YPoint2GRAD_Dubl_Sost = GlobalVarLn.YPoint2GRAD_Sost;

                                } // Положение PU изменилось

                            } // PU
                            // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                            if ((fsp1 == 1) || (fsp2 == 1) || (fpu == 1))
                            {
                                ClassMap.f_GetGPS();
                            }


                        } // НЕ 1-й раз
                        // .......................................................................

                    } // if (Checked == true&& Coord!=-1)
                    // ------------------------------------------------------------------------

                    // ******************************************************************* NEW



                    ClassMap.f_LoadSostIni();



                }  // GetSostGPS
        */

        // 3

        /*
                private void timer1_Tick(object sender, EventArgs e)
                {
                    //777Otl -> !!!No delete->Otladka GPS_Sostav
                                //AirPlane objs = new AirPlane();
                                //objs.Lat = GlobalVarLn.lll[0].Lat+0.0001; // SP1
                                //objs.Long = GlobalVarLn.lll[0].Long+0.0001;
                                //GlobalVarLn.lll[0] = objs;
                    Stopwatch sw = new Stopwatch();
                    sw.Start();

                    TCoordsIRI_PPRCh coordsIRI = new TCoordsIRI_PPRCh();
                    List<TCoordsIRI_PPRCh> listCoordsIRI_PPRCh = new List<TCoordsIRI_PPRCh>();
                    Color clr = Color.Red;

                    if (GlobalVarLn.shPelIRI == 0)
                    {
                        coordsIRI.bDelIRI = 0;
                        coordsIRI.iID = 1;
                        coordsIRI.dLatitude = 56.649194;
                        coordsIRI.dLongitude = 24.388778;
                        listCoordsIRI_PPRCh.Add(coordsIRI);

                        coordsIRI.bDelIRI = 0;
                        coordsIRI.iID = 2;
                        coordsIRI.dLatitude = 56.650417;
                        coordsIRI.dLongitude = 24.454536;
                        listCoordsIRI_PPRCh.Add(coordsIRI);

                        coordsIRI.bDelIRI = 0;
                        coordsIRI.iID = 3;
                        coordsIRI.dLatitude = 56.613391;
                        coordsIRI.dLongitude = 24.39187;
                        listCoordsIRI_PPRCh.Add(coordsIRI);

                        coordsIRI.bDelIRI = 0;
                        coordsIRI.iID = 7;
                        coordsIRI.dLatitude = 56.614923;
                        coordsIRI.dLongitude = 24.457059;
                        listCoordsIRI_PPRCh.Add(coordsIRI);

                        GlobalVarLn.shPelIRI += 1;
                    }

                    else
                    {
                        for (int i = (listCoordsIRI_PPRCh.Count - 1); i >= 0; i--)
                        {
                            listCoordsIRI_PPRCh.Remove(listCoordsIRI_PPRCh[i]);
                        }


                        coordsIRI.bDelIRI = 0;
                        coordsIRI.iID = 4;
                        coordsIRI.dLatitude = 56.652315;
                        coordsIRI.dLongitude = 24.518753;
                        listCoordsIRI_PPRCh.Add(coordsIRI);

                        coordsIRI.bDelIRI = 0;
                        coordsIRI.iID = 2;
                        coordsIRI.dLatitude = 56.653399;
                        coordsIRI.dLongitude = 24.58377;
                        listCoordsIRI_PPRCh.Add(coordsIRI);

                        coordsIRI.bDelIRI = 0;
                        coordsIRI.iID = 5;
                        coordsIRI.dLatitude = 56.617228;
                        coordsIRI.dLongitude = 24.587116;
                        listCoordsIRI_PPRCh.Add(coordsIRI);

                        GlobalVarLn.shPelIRI -= 1;
                    }


                    // 2504
                    double dds = 250; // m

                    int AdDl = 0;
                    int countPel = 0;
                    int countPel1 = 0;

                    // !!! Идут только пеленги (флпаг 3)/только ИРИ(флаг 0/1)/ удалить все (флаг 2)
                    AdDl = (int)listCoordsIRI_PPRCh[0].bDelIRI;

                    PelIRI objPelIRI2 = new PelIRI();  // For IRI

                    // Delete All ************************************************************
                    // Delete Peleng+IRI

                    if (AdDl == 2)
                    {

                        // Del Peleng
                        for (int iii20 = (GlobalVarLn.list_PelIRI.Count - 1); iii20 >= 0; iii20--)
                        {
                            GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii20]);
                        }
                        // DelIRI
                        for (int iii21 = (GlobalVarLn.list_PelIRI2.Count - 1); iii21 >= 0; iii21--)
                        {
                            GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii21]);
                        }

                        axaxcMapScreen1.Repaint();
                        return;

                    }  // AdDl=2
                    // ************************************************************ Delete All

                    // Delete ID *************************************************************
                    // Удалить конкретный ID

                    if (AdDl == 1)
                    {
                        // Del_Struct_ID ..........................................................
                        // удаляем все структуры с таким IDi 
                        // Придет в 

                        // FOR111
                        for (countPel = 0; countPel < listCoordsIRI_PPRCh.Count; countPel++)
                        {
                            int ffg1 = (int)listCoordsIRI_PPRCh[countPel].bDelIRI;

                            if (ffg1 == 1) // Del
                            {
                                // For22
                                for (int iii33 = (GlobalVarLn.list_PelIRI2.Count - 1); iii33 >= 0; iii33--)
                                {
                                    if (GlobalVarLn.list_PelIRI2[iii33].ID == listCoordsIRI_PPRCh[countPel].iID)
                                        GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii33]);

                                } //for22

                            } // IF

                        } // FOR111

                        axaxcMapScreen1.Repaint();
                        return;

                    } // AdDl==1 Del
                    // ************************************************************* Delete ID

                    // Add IRI ****************************************************************

                    if (AdDl == 0)
                    {

                        // ------------------------------------------------------------------------
                        // 2504

                        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        bool SignRepaint = false;
                        int i1 = 0;
                        int i2 = 0;
                        int i3 = 0;
                        int i4 = 0;
                        bool flexist = false;
                        int ffg = 0;
                        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        // Убрать в старом те, которых нет в новых

                        for (i1 = (GlobalVarLn.list_PelIRI2.Count - 1); i1 >= 0; i1--) // old
                        {
                            flexist = false;
                            for (i2 = 0; i2 < listCoordsIRI_PPRCh.Count; i2++) // new
                            {
                                ffg = (int)listCoordsIRI_PPRCh[i2].bDelIRI;

                                if (
                                    (GlobalVarLn.list_PelIRI2[i1].ID == listCoordsIRI_PPRCh[i2].iID) &&
                                    (listCoordsIRI_PPRCh[i2].dLatitude != -1) &&
                                    (listCoordsIRI_PPRCh[i2].dLongitude != -1) &&
                                    (ffg == 0)
                                   )
                                {
                                    flexist = true;
                                }

                            } // i2

                            if (flexist == false)
                            {
                                GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[i1]);
                                SignRepaint = true;
                                //i1 = -1;
                            }

                        } // i1 old

                        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        // 

                        // FOR1_5 вперед по новому
                        for (countPel = 0; countPel < listCoordsIRI_PPRCh.Count; countPel++) // new
                        {

                            ffg = (int)listCoordsIRI_PPRCh[countPel].bDelIRI;

                            if (
                                 (listCoordsIRI_PPRCh[countPel].dLatitude != -1) &&
                                 (listCoordsIRI_PPRCh[countPel].dLongitude != -1) &&
                                 (ffg == 0)
                               )
                            {

                                flexist = false;
                                for (int iii99 = (GlobalVarLn.list_PelIRI2.Count - 1); iii99 >= 0; iii99--)
                                {

                                    if (GlobalVarLn.list_PelIRI2[iii99].ID == listCoordsIRI_PPRCh[countPel].iID)
                                    {
                                        // есть с таким ID
                                        flexist = true;
                                        double sss = 0;
                                        sss = ClassMap.f_D_2Points(GlobalVarLn.list_PelIRI2[iii99].Lat, GlobalVarLn.list_PelIRI2[iii99].Long, listCoordsIRI_PPRCh[countPel].dLatitude, listCoordsIRI_PPRCh[countPel].dLongitude, 1);
                                        if (sss > dds)
                                        {
                                            SignRepaint = true;
                                            // Убираем старый
                                            GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii99]);

                                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                            // добавляем новый

                                            objPelIRI2.ID = listCoordsIRI_PPRCh[countPel].iID;
                                            objPelIRI2.XGPS_IRI = -1;
                                            objPelIRI2.YGPS_IRI = -1;
                                            objPelIRI2.flPelMain2 = 0;
                                            objPelIRI2.arr_Pel1 = new double[10000];     // R,Широта,долгота
                                            objPelIRI2.arr_Pel2 = new double[10000];     // R,Широта,долгота
                                            objPelIRI2.Pel1 = -1;
                                            objPelIRI2.Pel2 = -1;

                                            if ((listCoordsIRI_PPRCh[countPel].dLatitude != -1) && (listCoordsIRI_PPRCh[countPel].dLongitude != -1))
                                            {
                                                objPelIRI2.Lat = listCoordsIRI_PPRCh[countPel].dLatitude;
                                                objPelIRI2.Long = listCoordsIRI_PPRCh[countPel].dLongitude;
                                                objPelIRI2.color = clr;
                                                objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;
                                            }
                                            else
                                            {
                                                objPelIRI2.Lat = -1;
                                                objPelIRI2.Long = -1;
                                                objPelIRI2.XGPS_IRI = -1;
                                                objPelIRI2.YGPS_IRI = -1;
                                                objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;

                                            }

                                            GlobalVarLn.list_PelIRI2.Add(objPelIRI2);

                                            ClassMap.f_GetGPS_IRI_2(GlobalVarLn.list_PelIRI2.Count - 1);

                                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                                        } // S>ds

                                    }  // ID==

                                }  // for old назад

                                if (flexist == false) // это новый ID -> добавляем его
                                {
                                    SignRepaint = true;

                                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                    // добавляем новый

                                    objPelIRI2.ID = listCoordsIRI_PPRCh[countPel].iID;
                                    objPelIRI2.XGPS_IRI = -1;
                                    objPelIRI2.YGPS_IRI = -1;
                                    objPelIRI2.flPelMain2 = 0;
                                    objPelIRI2.arr_Pel1 = new double[10000];     // R,Широта,долгота
                                    objPelIRI2.arr_Pel2 = new double[10000];     // R,Широта,долгота
                                    objPelIRI2.Pel1 = -1;
                                    objPelIRI2.Pel2 = -1;

                                    if ((listCoordsIRI_PPRCh[countPel].dLatitude != -1) && (listCoordsIRI_PPRCh[countPel].dLongitude != -1))
                                    {
                                        objPelIRI2.Lat = listCoordsIRI_PPRCh[countPel].dLatitude;
                                        objPelIRI2.Long = listCoordsIRI_PPRCh[countPel].dLongitude;
                                        objPelIRI2.color = clr;
                                        objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;
                                    }
                                    else
                                    {
                                        objPelIRI2.Lat = -1;
                                        objPelIRI2.Long = -1;
                                        objPelIRI2.XGPS_IRI = -1;
                                        objPelIRI2.YGPS_IRI = -1;
                                        objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;

                                    }

                                    GlobalVarLn.list_PelIRI2.Add(objPelIRI2);

                                    ClassMap.f_GetGPS_IRI_2(GlobalVarLn.list_PelIRI2.Count - 1);

                                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                                } // // это новый ID

                            } //IF(есть координаты) 

                        } //FOR1_5 new
                        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                        if (SignRepaint == true)
                            axaxcMapScreen1.Repaint();


                    } // AdDl==0
                    // **************************************************************** Add IRI


                    int yyy = 0;
                    yyy = yyy;


                    sw.Stop();
                    // Get the elapsed time as a TimeSpan value.
                    TimeSpan ts = sw.Elapsed;

                    // Format and display the TimeSpan value.
                    string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}",
                        ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);
                    //MessageBox.Show("RunTime " + elapsedTime);
                    //Console.WriteLine("RunTime " + elapsedTime);

                }
        */


        // *************************************************************** GPS_SOST
        // ----------------------------------------------------------------------------- GPS_OLD

        // AIRPLANE_OLD ------------------------------------------------------------------------
        // AirPlane ***************************************************************
        // Lena 

        // 3
        /*
                private void Service_AirPlaneReceived(object sender, TDataADSBReceiver[] tDadaADSBReceiver)
                {
                    int ii = 0;
                    int ii1 = 0;
                    int fi = 0;
                    String s1 = "";

                    // ---------------------------------------------------------------------------------------
                    // tDadaADSBReceiver!=null

                    if (tDadaADSBReceiver != null)
                    {

                        // ....................................................................................
                        // 1-й сеанс получения данных

                        if (GlobalVarLn.fl_AirPlane == 0)
                        {
                            GlobalVarLn.fl_AirPlane = 1;

                            if (chbair.Checked == true)
                                GlobalVarLn.fl_AirPlaneVisible = 1;

                            for (ii = 0; ii < tDadaADSBReceiver.Length; ii++)
                            {
                                // Добавляем 1-й цикл структур в лист

                                s1 = String.Copy(tDadaADSBReceiver[ii].sLatitude);
                                try
                                {
                                    GlobalVarLn.Lat_air = Convert.ToDouble(s1);
                                }
                                catch (SystemException)
                                {
                                    if (s1.IndexOf(",") > -1)
                                        s1 = s1.Replace(',', '.');
                                    else
                                        s1 = s1.Replace('.', ',');

                                    GlobalVarLn.Lat_air = Convert.ToDouble(s1);
                                }

                                s1 = String.Copy(tDadaADSBReceiver[ii].sLongitude);
                                try
                                {
                                    GlobalVarLn.Long_air = Convert.ToDouble(s1);
                                }
                                catch (SystemException)
                                {
                                    if (s1.IndexOf(",") > -1)
                                        s1 = s1.Replace(',', '.');
                                    else
                                        s1 = s1.Replace('.', ',');
                                    GlobalVarLn.Long_air = Convert.ToDouble(s1);

                                }

                                GlobalVarLn.sNum_air = String.Copy(tDadaADSBReceiver[ii].sICAO);

                                ClassMap.f_AddAirPlane1(
                                                       GlobalVarLn.Lat_air,
                                                       GlobalVarLn.Long_air,
                                                       GlobalVarLn.sNum_air,
                                                       0
                                                      );

                            } // FOR

                            if (chbair.Checked == true)
                            {
                                // Перерисовка самолетов
                                ClassMap.f_ReDrawAirPlane1();
                            }

                        } // fl_AirPlane == 0
                          // ....................................................................................
                          // НЕ 1-й сеанс получения данных

                        else
                        {
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Проверить на исчезновение самолетов (Если в текущей партии какого-то самолета
                            // нет, значит он исчез)

                            // FOR1 (Свой List с конца)
                            for (ii = (GlobalVarLn.Number_air - 1); ii >= 0; ii--)
                            {

                                fi = 0;
                                // FOR2
                                for (ii1 = 0; ii1 < tDadaADSBReceiver.Length; ii1++)
                                {
                                    s1 = String.Copy(tDadaADSBReceiver[ii1].sICAO);
                                    if (String.Compare(GlobalVarLn.list_air[ii].sNum, s1) == 0)
                                        fi = 1;
                                } // FOR2

                                if (fi == 0)  // Этого самолета уже нет
                                    ClassMap.f_DelAirPlane(GlobalVarLn.list_air[ii].sNum);


                            } // FOR1
                              // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


                            for (ii = 0; ii < tDadaADSBReceiver.Length; ii++)
                            {
                                // Добавляем цикл структур в лист
                                s1 = String.Copy(tDadaADSBReceiver[ii].sLatitude);
                                try
                                {
                                    GlobalVarLn.Lat_air = Convert.ToDouble(s1);
                                }
                                catch (SystemException)
                                {
                                    if (s1.IndexOf(",") > -1)
                                        s1 = s1.Replace(',', '.');
                                    else
                                        s1 = s1.Replace('.', ',');
                                    GlobalVarLn.Lat_air = Convert.ToDouble(s1);
                                }

                                s1 = String.Copy(tDadaADSBReceiver[ii].sLongitude);
                                try
                                {
                                    GlobalVarLn.Long_air = Convert.ToDouble(s1);
                                }
                                catch (SystemException)
                                {
                                    if (s1.IndexOf(",") > -1)
                                        s1 = s1.Replace(',', '.');
                                    else
                                        s1 = s1.Replace('.', ',');
                                    GlobalVarLn.Long_air = Convert.ToDouble(s1);

                                }

                                GlobalVarLn.sNum_air = String.Copy(tDadaADSBReceiver[ii].sICAO);


                                ClassMap.f_AddAirPlane1(
                                                       GlobalVarLn.Lat_air,
                                                       GlobalVarLn.Long_air,
                                                       GlobalVarLn.sNum_air,
                                                       1
                                                      );

                            } // FOR

                            if (chbair.Checked == true)
                            {
                                // Перерисовка самолетов
                                ClassMap.f_ReDrawAirPlane1();
                            }

                        } // fl_AirPlane == 1
                          // ....................................................................................


                    } // tDadaADSBReceiver!=null
                      // ---------------------------------------------------------------------------------------
                      // tDadaADSBReceiver==null -> убрать все

                    else
                    {
                        ClassMap.f_DelAirPlaneAll();
                    }
                    // ---------------------------------------------------------------------------------------

                } // AirPlane
        */
        // ************************************************************************************ AirPlane


        // ------------------------------------------------------------------------ AIRPLANE_OLD

        // Button1 ----------------------------------------------------------------------------

        /*

                     if (chbSect.Checked == true)
                    {
                        GlobalVarLn.luch1 = 0; // grad
                        GlobalVarLn.luch2 = 10;
                        GlobalVarLn.luch3 = 90;
                        GlobalVarLn.luch4 = 180;
                        GlobalVarLn.luch5 = 300;

                        GlobalVarLn.flsect = 1;
                        GlobalVarLn.fllSect1 = 1;
                        GlobalVarLn.fllSect2 = 1;
                        GlobalVarLn.fllSect3 = 1;
                        GlobalVarLn.fllSect4 = 1;
                        GlobalVarLn.fllSect5 = 1;

                        // .....................................................................
                        if (GlobalVarLn.f_luch == 0)
                        {
                            GlobalVarLn.f_luch = 1;
                            GlobalVarLn.luch1_dubl = GlobalVarLn.luch1;
                            GlobalVarLn.luch2_dubl = GlobalVarLn.luch2;
                            GlobalVarLn.luch3_dubl = GlobalVarLn.luch3;
                            GlobalVarLn.luch4_dubl = GlobalVarLn.luch4;
                            GlobalVarLn.luch5_dubl = GlobalVarLn.luch5;

                            GlobalVarLn.flsect = 0;
                            // Убрать с карты
                            GlobalVarLn.axMapScreenGlobal.Repaint();

                            GlobalVarLn.flsect = 1;

                            ClassMap.Sector(
                                            (double)GlobalVarLn.XCenter_Sost,
                                            (double)GlobalVarLn.YCenter_Sost
                                          );
                        }
                        // .....................................................................
                        else
                        {
                            if (
                                (GlobalVarLn.luch1 == GlobalVarLn.luch1_dubl) &&
                                (GlobalVarLn.luch2 == GlobalVarLn.luch2_dubl) &&
                                (GlobalVarLn.luch3 == GlobalVarLn.luch3_dubl) &&
                                (GlobalVarLn.luch4 == GlobalVarLn.luch4_dubl) &&
                                (GlobalVarLn.luch5 == GlobalVarLn.luch5_dubl)
                              )
                            {
                                return;
                            }

                            else
                            {
                                GlobalVarLn.luch1_dubl = GlobalVarLn.luch1;
                                GlobalVarLn.luch2_dubl = GlobalVarLn.luch2;
                                GlobalVarLn.luch3_dubl = GlobalVarLn.luch3;
                                GlobalVarLn.luch4_dubl = GlobalVarLn.luch4;
                                GlobalVarLn.luch5_dubl = GlobalVarLn.luch5;

                                GlobalVarLn.flsect = 0;
                                // Убрать с карты
                                GlobalVarLn.axMapScreenGlobal.Repaint();

                                GlobalVarLn.flsect = 1;

                                ClassMap.Sector(
                                                (double)GlobalVarLn.XCenter_Sost,
                                                (double)GlobalVarLn.YCenter_Sost
                                              );

                            }

                        }
                        // .....................................................................


                    } // chbSect.Checked == true


         */

        // ---------------------------------------------------------------------------- Button1

        // Button3 ----------------------------------------------------------------------------
        /*
                    // Peleng
                    if (objFormPeleng == null || objFormPeleng.IsDisposed)
                    {
                        objFormPeleng = new FormPeleng(ref axaxcMapScreen1);
                        objFormPeleng.Show();
                    }
                    else
                    {
                        objFormPeleng.Show();
                    }


                    //FormPeleng objFormPeleng = new FormPeleng(ref axaxcMapScreen1);
                    //objFormPeleng.Show();
        */
        // ---------------------------------------------------------------------------- Button3

        // Button4 ----------------------------------------------------------------------------
        // GPSSPPU
        // !!! Otladka Sostav-> NO delete

        // NEW *******************************************************************

        // 3
        /*
                    // -----------------------------------------------------------------------
                    // Нажата ли птичка на АРМ (посылка координат от GPS)

                    TCoordsGNSS[] coordsGNSS_1 = new TCoordsGNSS[3];

                    GlobalVarLn.ssaa += 1;

                    if (GlobalVarLn.ssaa == 1)
                    {
                        coordsGNSS_1[0].Lat = 54.051;
                        coordsGNSS_1[0].Lon = 26.084;
                    }
                    if (GlobalVarLn.ssaa == 2)
                    {
                        coordsGNSS_1[0].Lat = 54.037822;
                        coordsGNSS_1[0].Lon = 25.981732;
                    }

                    GlobalVarLn.bCheckGNSS = true;
                    // -----------------------------------------------------------------------
                    // Пошли координаты -1

                    LF objLF = new LF();
                    int fsp1_1 = 0;
                    int fsp2_1 = 0;
                    int fpu_1 = 0;

                    // ..................................................................
                    // SP1 -> идут -1

                    if (
                       //(fl == true) &&
                       (GlobalVarLn.bCheckGNSS) &&
                       //777Otl
                       ((coordsGNSS_1[0].Lat == -1) || (coordsGNSS_1[0].Lon == -1))
                      )
                    {
                        // раньше были координаты (свои/GPS)
                        if (
                           (GlobalVarLn.XCenterGRAD_Dubl_Sost != -1) &&
                           (GlobalVarLn.YCenterGRAD_Dubl_Sost != -1)
                           )
                        {
                            GlobalVarLn.XCenterGRAD_Sost = -1;
                            GlobalVarLn.YCenterGRAD_Sost = -1;
                            GlobalVarLn.XCenterGRAD_Dubl_Sost = -1;
                            GlobalVarLn.YCenterGRAD_Dubl_Sost = -1;
                            GlobalVarLn.XCenter_Sost = 0;
                            GlobalVarLn.YCenter_Sost = 0;
                            GlobalVarLn.HCenter_Sost = 0;

                            objLF.X_m = 0;
                            objLF.Y_m = 0;
                            objLF.H_m = 0;

                            objLF.indzn = iniRW.get_ZnakSP1();
                            GlobalVarLn.indz1_Sost = objLF.indzn;
                            GlobalVarLn.objFormSostG.pbSP.Image = GlobalVarLn.objFormSostG.imageList1.Images[GlobalVarLn.indz1_Sost];

                            GlobalVarLn.list1_Sost[0] = objLF;

                            GlobalVarLn.flCoordSP_Sost = 0;
                            fsp1_1 = 1;

                            GlobalVarLn.objFormSostG.tbXRect.Text = "";
                            GlobalVarLn.objFormSostG.tbYRect.Text = "";
                            GlobalVarLn.objFormSostG.tbXRect42.Text = "";
                            GlobalVarLn.objFormSostG.tbYRect42.Text = "";
                            GlobalVarLn.objFormSostG.tbBRad.Text = "";
                            GlobalVarLn.objFormSostG.tbLRad.Text = "";
                            GlobalVarLn.objFormSostG.tbBMin1.Text = "";
                            GlobalVarLn.objFormSostG.tbLMin1.Text = "";
                            GlobalVarLn.objFormSostG.tbBDeg2.Text = "";
                            GlobalVarLn.objFormSostG.tbBMin2.Text = "";
                            GlobalVarLn.objFormSostG.tbBSec.Text = "";
                            GlobalVarLn.objFormSostG.tbLDeg2.Text = "";
                            GlobalVarLn.objFormSostG.tbLMin2.Text = "";
                            GlobalVarLn.objFormSostG.tbLSec.Text = "";

                            GlobalVarLn.objFormSostG.tbOwnHeight.Text = "";

                            // --------------------------------------------------------------------------------------
                            // 14_09_2018
                            // Для пеленгов

                            // Нажата птичка для отрисовки пеленгов
                            if (GlobalVarLn.flPelMain == 1)
                            {
                                // FRCH
                                GlobalVarLn.PrevP1 = -1;
                                GlobalVarLn.fl_PelIRI_1 = 0; // 1-й пеленг
                                GlobalVarLn.flag_eq_draw1 = 0;

                                //------------------------------------------------------------------------
                                //PPRCH
                                for (int iii = (GlobalVarLn.list_PelIRI.Count - 1); iii >= 0; iii--)
                                {
                                    PelIRI[] mass = GlobalVarLn.list_PelIRI.ToArray();
                                    mass[iii].Pel1 = -1;
                                    GlobalVarLn.list_PelIRI = mass.ToList();

                                    if (
                                        (GlobalVarLn.list_PelIRI[iii].Pel1 == -1) &&
                                        (GlobalVarLn.list_PelIRI[iii].Pel2 == -1) &&
                                        (GlobalVarLn.list_PelIRI[iii].Lat == -1) &&
                                        (GlobalVarLn.list_PelIRI[iii].Long == -1)
                                       )
                                    {
                                        GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii]);
                                    }
                                }

                            }
                            //------------------------------------------------------------------------
                        }

                    }  // -1 SP1
                    // ..................................................................
                    // SP2

                    if (
                       (GlobalVarLn.bCheckGNSS) &&
                        //777Otl
                        ((coordsGNSS_1[1].Lat == -1) || (coordsGNSS_1[1].Lon == -1))
                      )
                    {
                        // раньше были координаты (свои/GPS)
                        if (
                           (GlobalVarLn.XPoint1GRAD_Dubl_Sost != -1) &&
                           (GlobalVarLn.YPoint1GRAD_Dubl_Sost != -1)
                           )
                        {
                            GlobalVarLn.XPoint1GRAD_Sost = -1;
                            GlobalVarLn.YPoint1GRAD_Sost = -1;
                            GlobalVarLn.XPoint1GRAD_Dubl_Sost = -1;
                            GlobalVarLn.YPoint1GRAD_Dubl_Sost = -1;
                            GlobalVarLn.XPoint1_Sost = 0;
                            GlobalVarLn.YPoint1_Sost = 0;
                            GlobalVarLn.HPoint1_Sost = 0;

                            objLF.X_m = 0;
                            objLF.Y_m = 0;
                            objLF.H_m = 0;

                            // 13_09_2018
                            //objLF.indzn = 0;
                            GlobalVarLn.indz2_Sost = iniRW.get_ZnakSP2();
                            GlobalVarLn.objFormSostG.pictureBox1.Image = GlobalVarLn.objFormSostG.imageList2.Images[GlobalVarLn.indz2_Sost];
                            objLF.indzn = GlobalVarLn.indz2_Sost;

                            GlobalVarLn.list1_Sost[1] = objLF;

                            GlobalVarLn.flCoordYS1_Sost = 0;
                            fsp2_1 = 1;

                            GlobalVarLn.objFormSostG.tbPt1XRect.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1YRect.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1XRect42.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1YRect42.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1BRad.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1LRad.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1BMin1.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1LMin1.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1BDeg2.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1BMin2.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1BSec.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1LDeg2.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1LMin2.Text = "";
                            GlobalVarLn.objFormSostG.tbPt1LSec.Text = "";

                            GlobalVarLn.objFormSostG.tbPt1Height.Text = "";

                            // --------------------------------------------------------------------------------------
                            // 14_09_2018
                            // Для пеленгов

                            // Нажата птичка для отрисовки пеленгов
                            if (GlobalVarLn.flPelMain == 1)
                            {
                                // FRCH
                                GlobalVarLn.PrevP2 = -1;
                                GlobalVarLn.flPelMain2 = 0;   // 2-й пеленг
                                GlobalVarLn.flag_eq_draw2 = 0;
                                //------------------------------------------------------------------------
                                // PPRCH
                                for (int iii = (GlobalVarLn.list_PelIRI.Count - 1); iii >= 0; iii--)
                                {


                                    PelIRI[] mass = GlobalVarLn.list_PelIRI.ToArray();
                                    mass[iii].Pel2 = -1;
                                    GlobalVarLn.list_PelIRI = mass.ToList();

                                    if (
                                        (GlobalVarLn.list_PelIRI[iii].Pel1 == -1) &&
                                        (GlobalVarLn.list_PelIRI[iii].Pel2 == -1) &&
                                        (GlobalVarLn.list_PelIRI[iii].Lat == -1) &&
                                        (GlobalVarLn.list_PelIRI[iii].Long == -1)
                                       )
                                    {
                                        GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii]);
                                    }
                                }
                                //------------------------------------------------------------------------

                                // Убрать с карты
                                //GlobalVarLn.axMapScreenGlobal.Repaint();

                            } // Нажата птичка для отрисовки пеленгов
                            // --------------------------------------------------------------------------------------

                        }

                    }  // -1 SP2
                    // ..................................................................
                    // PU

                    if (
                       //(fl == true) &&
                       (GlobalVarLn.bCheckGNSS) &&
                        //777Otl
                        ((coordsGNSS_1[2].Lat == -1) || (coordsGNSS_1[2].Lon == -1))
                      )
                    {
                        // раньше были координаты (свои/GPS)
                        if (
                           (GlobalVarLn.XPoint2GRAD_Dubl_Sost != -1) &&
                           (GlobalVarLn.YPoint2GRAD_Dubl_Sost != -1)
                           )
                        {
                            GlobalVarLn.XPoint2GRAD_Sost = -1;
                            GlobalVarLn.YPoint2GRAD_Sost = -1;
                            GlobalVarLn.XPoint2GRAD_Dubl_Sost = -1;
                            GlobalVarLn.YPoint2GRAD_Dubl_Sost = -1;
                            GlobalVarLn.XPoint2_Sost = 0;
                            GlobalVarLn.YPoint2_Sost = 0;
                            GlobalVarLn.HPoint2_Sost = 0;

                            objLF.X_m = 0;
                            objLF.Y_m = 0;
                            objLF.H_m = 0;

                            // 13_09_2018
                            //objLF.indzn = 0;
                            // !!! Значки читаем с INI файла
                            GlobalVarLn.indz3_Sost = iniRW.get_ZnakPU();
                            GlobalVarLn.objFormSostG.pictureBox2.Image = GlobalVarLn.objFormSostG.imageList3.Images[GlobalVarLn.indz3_Sost];
                            objLF.indzn = GlobalVarLn.indz3_Sost;

                            GlobalVarLn.list1_Sost[2] = objLF;

                            GlobalVarLn.flCoordYS2_Sost = 0;

                            fpu_1 = 1;
                            GlobalVarLn.objFormSostG.tbPt2XRect.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2YRect.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2XRect42.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2YRect42.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2BRad.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2LRad.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2BMin1.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2LMin1.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2BDeg2.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2BMin2.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2BSec.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2LDeg2.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2LMin2.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2LSec.Text = "";
                            GlobalVarLn.objFormSostG.tbPt2Height.Text = "";

                        }

                    }  // -1 PU
                    // ..................................................................

                    if ((fsp1_1 == 1) || (fsp2_1 == 1) || (fpu_1 == 1))
                    {
                        // Убрать с карты
                        GlobalVarLn.axMapScreenGlobal.Repaint();
                        // Redraw
                        ClassMap.f_Map_Redraw_Sost();

                    }
                    // ..................................................................

                    // ------------------------------------------------------------------------
                    // Checked==true

                    if (
                        (GlobalVarLn.bCheckGNSS) &&
                        (
                        //777Otl
                        ((coordsGNSS_1[0].Lat != -1) && (coordsGNSS_1[0].Lon != -1)) ||
                        ((coordsGNSS_1[1].Lat != -1) && (coordsGNSS_1[1].Lon != -1)) ||
                        ((coordsGNSS_1[2].Lat != -1) && (coordsGNSS_1[2].Lon != -1))

                        )
                        )
                    {
                        // .......................................................................
                        // 1-й раз

                        if ((GlobalVarLn.fl_First_GPS == 0))
                        {

                            GlobalVarLn.fl_First_GPS = 1;

                            // SP1
                            //777Otl
                            if ((coordsGNSS_1[0].Lat != -1) && (coordsGNSS_1[0].Lon != -1))
                            {
                                //777Otl
                                GlobalVarLn.XCenterGRAD_Sost = coordsGNSS_1[0].Lat;
                                GlobalVarLn.YCenterGRAD_Sost = coordsGNSS_1[0].Lon;

                                GlobalVarLn.XCenterGRAD_Dubl_Sost = GlobalVarLn.XCenterGRAD_Sost;
                                GlobalVarLn.YCenterGRAD_Dubl_Sost = GlobalVarLn.YCenterGRAD_Sost;
                            }

                            // SP2
                            //777Otl
                            if ((coordsGNSS_1[1].Lat != -1) && (coordsGNSS_1[1].Lon != -1))
                            {
                                GlobalVarLn.XPoint1GRAD_Sost = coordsGNSS_1[1].Lat;
                                GlobalVarLn.YPoint1GRAD_Sost = coordsGNSS_1[1].Lon;

                                GlobalVarLn.XPoint1GRAD_Dubl_Sost = GlobalVarLn.XPoint1GRAD_Sost;
                                GlobalVarLn.YPoint1GRAD_Dubl_Sost = GlobalVarLn.YPoint1GRAD_Sost;
                            }

                            // PU
                            //777Otl
                            if ((coordsGNSS_1[2].Lat != -1) && (coordsGNSS_1[2].Lon != -1))
                            {
                                //777Otl
                                GlobalVarLn.XPoint2GRAD_Sost = coordsGNSS_1[2].Lat;
                                GlobalVarLn.YPoint2GRAD_Sost = coordsGNSS_1[2].Lon;

                                GlobalVarLn.XPoint2GRAD_Dubl_Sost = GlobalVarLn.XPoint2GRAD_Sost;
                                GlobalVarLn.YPoint2GRAD_Dubl_Sost = GlobalVarLn.YPoint2GRAD_Sost;
                            }

                            ClassMap.f_GetGPS();

                        } // 1-й раз
                        // .......................................................................
                        // НЕ 1-й раз

                        else
                        {

                            int fsp1 = 0;
                            int fsp2 = 0;
                            int fpu = 0;

                            // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                            // SP1
                            //777Otl
                            if ((coordsGNSS_1[0].Lat != -1) && (coordsGNSS_1[0].Lon != -1))
                            {
                                //Положение SP1 изменилось
                                //777Otl
                                //if (
                                //   (
                                //    (coordsGNSS_1[0].Lat != GlobalVarLn.XCenterGRAD_Dubl_Sost) ||
                                //    (coordsGNSS_1[0].Lon != GlobalVarLn.YCenterGRAD_Dubl_Sost)

                                //   ) 
                                //   //(GlobalVarLn.flTmrGPS == 1)
                                //  )
                                {
                                    fsp1 = 1;
                                    //777Otl
                                    GlobalVarLn.XCenterGRAD_Sost = coordsGNSS_1[0].Lat;
                                    GlobalVarLn.YCenterGRAD_Sost = coordsGNSS_1[0].Lon;

                                    GlobalVarLn.XCenterGRAD_Dubl_Sost = GlobalVarLn.XCenterGRAD_Sost;
                                    GlobalVarLn.YCenterGRAD_Dubl_Sost = GlobalVarLn.YCenterGRAD_Sost;

                                } // Положение SP1 изменилось

                            } // SP1
                            // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                            // SP2

                            //777Otl
                            if ((coordsGNSS_1[1].Lat != -1) && (coordsGNSS_1[1].Lon != -1))
                            {
                                //Положение SP2 изменилось
                                {
                                    fsp2 = 1;
                                    //777Otl
                                    GlobalVarLn.XPoint1GRAD_Sost = coordsGNSS_1[1].Lat;
                                    GlobalVarLn.YPoint1GRAD_Sost = coordsGNSS_1[1].Lon;

                                    GlobalVarLn.XPoint1GRAD_Dubl_Sost = GlobalVarLn.XPoint1GRAD_Sost;
                                    GlobalVarLn.YPoint1GRAD_Dubl_Sost = GlobalVarLn.YPoint1GRAD_Sost;

                                } // Положение SP2 изменилось

                            } // SP2
                            // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                            // PU

                            //777Otl
                            if ((coordsGNSS_1[2].Lat != -1) && (coordsGNSS_1[2].Lon != -1))
                            {
                                //Положение PU изменилось
                                //if (
                                //   (
                                //    //777Otl
                                //    (coordsGNSS_1[2].Lat != GlobalVarLn.XPoint2GRAD_Dubl_Sost) ||
                                //    (coordsGNSS_1[2].Lon != GlobalVarLn.YPoint2GRAD_Dubl_Sost)

                                //   ) 
                                //   //(GlobalVarLn.flTmrGPS == 1)
                                //  )
                                {
                                    fpu = 1;
                                    //777Otl
                                    GlobalVarLn.XPoint2GRAD_Sost = coordsGNSS_1[2].Lat;
                                    GlobalVarLn.YPoint2GRAD_Sost = coordsGNSS_1[2].Lon;

                                    GlobalVarLn.XPoint2GRAD_Dubl_Sost = GlobalVarLn.XPoint2GRAD_Sost;
                                    GlobalVarLn.YPoint2GRAD_Dubl_Sost = GlobalVarLn.YPoint2GRAD_Sost;

                                } // Положение PU изменилось

                            } // PU
                            // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                            if ((fsp1 == 1) || (fsp2 == 1) || (fpu == 1))
                            {
                                ClassMap.f_GetGPS();
                            }


                        } // НЕ 1-й раз
                        // .......................................................................

                    } // if (Checked == true&& Coord!=-1)
                    // ------------------------------------------------------------------------
        */

        // ******************************************************************* NEW

        // ---------------------------------------------------------------------------- Button4

        // LinaPasha --------------------------------------------------------------------------

        // LinaPasha************************************************************************************ 

        // 3
        /*        
                Contract.IService service;

                private void ServiceAPMCreate()
                {
                    var scf = new ChannelFactory<Contract.IService>(new NetTcpBinding(), "net.tcp://localhost:8001");
                    service = scf.CreateChannel();
                }

                private void SendMapIsOpenToAPM(bool isOpen)
                {
                    //if (MapIsOpenned)
                    TrySendToAPM(() => service.MapIsOpen(isOpen));
                }

                void objFormSost_ClickGetGNSS(double Lat, double Lon, double Alt)
                {
                    USR_DLL.TCoordsGNSS[] tCoordsGNSS = new USR_DLL.TCoordsGNSS[1];
                    tCoordsGNSS[0].Lat = Lat;
                    tCoordsGNSS[0].Lon = Lon;
                    tCoordsGNSS[0].Alt = Alt;
                    SendCoordsGNSSToAPM(tCoordsGNSS);
                }

                void objFormSost_ClickGetGNSS2(double Lat, double Lon, double Alt)
                {
                    USR_DLL.TCoordsGNSS[] tCoordsGNSS = new USR_DLL.TCoordsGNSS[2];
                    tCoordsGNSS[1].Lat = Lat;
                    tCoordsGNSS[1].Lon = Lon;
                    tCoordsGNSS[1].Alt = Alt;
                    SendCoordsGNSSToAPM(tCoordsGNSS);
                }

                private void SendCoordsGNSSToAPM(USR_DLL.TCoordsGNSS[] tCoordsGNSS)
                {
                    //if (MapIsOpenned)
                    TrySendToAPM(() => service.MapCurrentCoords(tCoordsGNSS));
                }

                private void TrySendToAPM(Action action)
                {
                    Task.Run(() =>
                    {
                        try
                        {
                            action();
                        }
                        catch
                        {
                            //создаем клиент
                            var scf = new ChannelFactory<Contract.IService>(new NetTcpBinding(), "net.tcp://localhost:8001");
                            service = scf.CreateChannel();

                            try
                            {
                                action();
                            }
                            catch
                            {
                            }
                        }
                    });
                }
        */
        // ************************************************************************************LinaPasha 

        // -------------------------------------------------------------------------- LinaPasha

        // Загрузка таблицы самолетов ---------------------------------------------------------

        // Otl .............................................................
        /*
                    TempADSB obj = new TempADSB();
                    obj.Coordinates = new Coord();
                    obj.ICAO = "11";
                    obj.Coordinates.Latitude = 54.858;
                    obj.Coordinates.Longitude = 29.357;
                    airplanes.Add(obj);

                    TempADSB obj1 = new TempADSB();
                    obj1.Coordinates = new Coord();
                    obj1.ICAO = "11";
                    obj1.Coordinates.Latitude = 54.926;
                    obj1.Coordinates.Longitude = 29.498;
                    airplanes.Add(obj1);

                    TempADSB obj2 = new TempADSB();
                    obj2.Coordinates = new Coord();
                    obj2.ICAO = "11";
                    obj2.Coordinates.Latitude = 54.996;
                    obj2.Coordinates.Longitude = 29.617;
                    airplanes.Add(obj2);

                    TempADSB obj3 = new TempADSB();
                    obj3.Coordinates = new Coord();
                    obj3.ICAO = "22";
                    obj3.Coordinates.Latitude = 54.888;
                    obj3.Coordinates.Longitude = 28.685;
                    airplanes.Add(obj3);

                    TempADSB obj4= new TempADSB();
                    obj4.Coordinates = new Coord();
                    obj4.ICAO = "22";
                    obj4.Coordinates.Latitude = 54.868;
                    obj4.Coordinates.Longitude = 28.745;
                    airplanes.Add(obj4);

                    TempADSB obj5 = new TempADSB();
                    obj5.Coordinates = new Coord();
                    obj5.ICAO = "11";
                    obj5.Coordinates.Latitude = 55.065;
                    obj5.Coordinates.Longitude = 29.756;
                    airplanes.Add(obj5);

                    TempADSB obj6 = new TempADSB();
                    obj6.Coordinates = new Coord();
                    obj6.ICAO = "11";
                    obj6.Coordinates.Latitude = 55.138;
                    obj6.Coordinates.Longitude = 29.887;
                    airplanes.Add(obj6);

                    TempADSB obj7 = new TempADSB();
                    obj7.Coordinates = new Coord();
                    obj7.ICAO = "11";
                    obj7.Coordinates.Latitude = 55.167;
                    obj7.Coordinates.Longitude = 29.943;
                    airplanes.Add(obj7);

                    TempADSB obj8 = new TempADSB();
                    obj8.Coordinates = new Coord();
                    obj8.ICAO = "22";
                    obj8.Coordinates.Latitude = 54.835;
                    obj8.Coordinates.Longitude = 28.806;
                    airplanes.Add(obj8);
        */
        // ............................................................. Otl

        // --------------------------------------------------------- Загрузка таблицы самолетов

        // Загрузка ИРИ ФРЧ -------------------------------------------------------------------

        // Otl ----------------------------------------------------------
        /*
                                TempFWS obj;

                                Random rand = new Random();
                                Random rand1 = new Random();
                                int j = 0;
                                int j1 = 0;
                                int f = 50;
                                double lt_temp = 0;
                                double ln_temp = 0;

                                for (j1 = 0; j1 < 10; j1++)
                                {
                                    for (j = 0; j < 5; j++)
                                    {
                                        lt_temp = Convert.ToDouble(rand.Next(GlobalVarLn.lat1_temp1, GlobalVarLn.lat2_temp1)) / 100000;
                                        ln_temp = Convert.ToDouble(rand1.Next(GlobalVarLn.lon1_temp1, GlobalVarLn.lon2_temp1)) / 100000;
                                        obj = new TempFWS();
                                        obj.Coordinates = new Coord();
                                        obj.Coordinates.Latitude = lt_temp;
                                        obj.Coordinates.Longitude = ln_temp;
                                        obj.FreqKHz = f;
                                        tempfws.Add(obj);
                                    }
                                    if (j1 < 4)
                                        f += 50;
                                    else
                                        f += 250;
                                }
        */
        // ---------------------------------------------------------- Otl

        // ------------------------------------------------------------------- Загрузка ИРИ ФРЧ

        // Загрузка пеленгов ------------------------------------------------------------------

        // Otl ----------------------------------------------------------
        /*
                    TempFWS obj = new TempFWS();
                    obj.Coordinates = new Coord();
                    obj.ListQ = new System.Collections.ObjectModel.ObservableCollection<JamDirect>();
                    JamDirect obj1 = new JamDirect();
                    obj1.IsOwn = true;
                    obj1.Bearing = 45;
                    obj.ListQ.Add(obj1);
                    JamDirect obj2 = new JamDirect();
                    obj2.IsOwn = false;
                    obj2.Bearing = 315;
                    obj.ListQ.Add(obj2);
                    obj.IsSelected = true;

                    tempfws.Add(obj);
        */
        // ---------------------------------------------------------- Otl

        // ------------------------------------------------------------------ Загрузка пеленгов

        // Загрузка ИРИ ППРЧ -------------------------------------------------------------------

        // Otl ----------------------------------------------------------

        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // Две пачки


        /*


                    // NEW



                    if (GlobalVarLn.sh1 == 0)
                    {
                        // ..........................................
                        // 1-я пачка (3 ИРИ)

                        TableReconFHSS obj1 = new TableReconFHSS();
                        obj1.Id = 66;
                        tablereconfhss.Add(obj1);

                        TableSourceFHSS obj2 = new TableSourceFHSS();
                        obj2.Coordinates = new Coord();
                        obj2.Id = 1;
                        obj2.IdFHSS = 66;
                        obj2.Coordinates.Latitude = 54.808;
                        obj2.Coordinates.Longitude = 27.622;
                        tablesourcefhss.Add(obj2);
                        TableSourceFHSS obj3 = new TableSourceFHSS();
                        obj3.Coordinates = new Coord();
                        obj3.Coordinates.Latitude = 54.808;
                        obj3.Coordinates.Longitude = 27.684;
                        obj3.Id = 2;
                        obj3.IdFHSS = 66;
                        tablesourcefhss.Add(obj3);
                        TableSourceFHSS obj4 = new TableSourceFHSS();
                        obj4.Coordinates = new Coord();
                        obj4.Coordinates.Latitude = 54.806;
                        obj4.Coordinates.Longitude = 27.745;
                        obj4.Id = 3;
                        obj4.IdFHSS = 66;
                        tablesourcefhss.Add(obj4);
                        // ..........................................
                        // 2-я пачка (2 ИРИ)

                        TableReconFHSS obj1_1 = new TableReconFHSS();
                        obj1_1.Id = 77;
                        tablereconfhss.Add(obj1_1);

                        TableSourceFHSS obj2_1 = new TableSourceFHSS();
                        obj2_1.Coordinates = new Coord();
                        obj2_1.Id = 4;
                        obj2_1.IdFHSS = 77;
                        obj2_1.Coordinates.Latitude = 54.772;
                        obj2_1.Coordinates.Longitude = 27.622;
                        tablesourcefhss.Add(obj2_1);
                        TableSourceFHSS obj3_1 = new TableSourceFHSS();
                        obj3_1.Coordinates = new Coord();
                        obj3_1.Coordinates.Latitude = 54.771;
                        obj3_1.Coordinates.Longitude = 27.683;
                        obj3_1.Id = 5;
                        obj3_1.IdFHSS = 77;
                        tablesourcefhss.Add(obj3_1);

                        GlobalVarLn.sh1 += 1;
                        // ..........................................
                    } // sh1=0
                      // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                      //  Добавилась 3-яя пачка (1 ИРИ)

                    else if (GlobalVarLn.sh1 == 1)
                    {
                        // ..........................................
                        // 1-я пачка (3 ИРИ)

                        TableReconFHSS obj1 = new TableReconFHSS();
                        obj1.Id = 66;
                        tablereconfhss.Add(obj1);

                        TableSourceFHSS obj2 = new TableSourceFHSS();
                        obj2.Coordinates = new Coord();
                        obj2.Id = 1;
                        obj2.IdFHSS = 66;
                        obj2.Coordinates.Latitude = 54.808;
                        obj2.Coordinates.Longitude = 27.622;
                        tablesourcefhss.Add(obj2);
                        TableSourceFHSS obj3 = new TableSourceFHSS();
                        obj3.Coordinates = new Coord();
                        obj3.Coordinates.Latitude = 54.808;
                        obj3.Coordinates.Longitude = 27.684;
                        obj3.Id = 2;
                        obj3.IdFHSS = 66;
                        tablesourcefhss.Add(obj3);
                        TableSourceFHSS obj4 = new TableSourceFHSS();
                        obj4.Coordinates = new Coord();
                        obj4.Coordinates.Latitude = 54.806;
                        obj4.Coordinates.Longitude = 27.745;
                        obj4.Id = 3;
                        obj4.IdFHSS = 66;
                        tablesourcefhss.Add(obj4);
                        // ..........................................
                        // 2-я пачка (2 ИРИ)

                        TableReconFHSS obj1_1 = new TableReconFHSS();
                        obj1_1.Id = 77;
                        tablereconfhss.Add(obj1_1);

                        TableSourceFHSS obj2_1 = new TableSourceFHSS();
                        obj2_1.Coordinates = new Coord();
                        obj2_1.Id = 4;
                        obj2_1.IdFHSS = 77;
                        obj2_1.Coordinates.Latitude = 54.772;
                        obj2_1.Coordinates.Longitude = 27.622;
                        tablesourcefhss.Add(obj2_1);
                        TableSourceFHSS obj3_1 = new TableSourceFHSS();
                        obj3_1.Coordinates = new Coord();
                        obj3_1.Coordinates.Latitude = 54.771;
                        obj3_1.Coordinates.Longitude = 27.683;
                        obj3_1.Id = 5;
                        obj3_1.IdFHSS = 77;
                        tablesourcefhss.Add(obj3_1);
                        // ..........................................
                        // 3-я пачка (1 ИРИ)

                        TableReconFHSS obj1_2 = new TableReconFHSS();
                        obj1_2.Id = 99;
                        tablereconfhss.Add(obj1_2);

                        TableSourceFHSS obj2_2 = new TableSourceFHSS();
                        obj2_2.Coordinates = new Coord();
                        obj2_2.Id = 7;
                        obj2_2.IdFHSS = 99;
                        obj2_2.Coordinates.Latitude = 54.751;
                        obj2_2.Coordinates.Longitude = 27.778;
                        tablesourcefhss.Add(obj2_2);
                        // ..........................................

                        GlobalVarLn.sh1 += 1;

                    } // sh1==1
                      // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                      //  Удалилась 2-я пачка (2 ИРИ)

                    else if (GlobalVarLn.sh1 == 2)
                    {
                        // ..........................................
                        // 1-я пачка (3 ИРИ)

                        TableReconFHSS obj1 = new TableReconFHSS();
                        obj1.Id = 66;
                        tablereconfhss.Add(obj1);

                        TableSourceFHSS obj2 = new TableSourceFHSS();
                        obj2.Coordinates = new Coord();
                        obj2.Id = 1;
                        obj2.IdFHSS = 66;
                        obj2.Coordinates.Latitude = 54.808;
                        obj2.Coordinates.Longitude = 27.622;
                        tablesourcefhss.Add(obj2);
                        TableSourceFHSS obj3 = new TableSourceFHSS();
                        obj3.Coordinates = new Coord();
                        obj3.Coordinates.Latitude = 54.808;
                        obj3.Coordinates.Longitude = 27.684;
                        obj3.Id = 2;
                        obj3.IdFHSS = 66;
                        tablesourcefhss.Add(obj3);
                        TableSourceFHSS obj4 = new TableSourceFHSS();
                        obj4.Coordinates = new Coord();
                        obj4.Coordinates.Latitude = 54.806;
                        obj4.Coordinates.Longitude = 27.745;
                        obj4.Id = 3;
                        obj4.IdFHSS = 66;
                        tablesourcefhss.Add(obj4);
                        // ..........................................
                        // 2-я пачка (2 ИРИ)

                        //TableReconFHSS obj1_1 = new TableReconFHSS();
                        //obj1_1.Id = 77;
                        //tablereconfhss.Add(obj1_1);

                        //TableSourceFHSS obj2_1 = new TableSourceFHSS();
                        //obj2_1.Coordinates = new Coord();
                        //obj2_1.Id = 4;
                        //obj2_1.IdFHSS = 77;
                        //obj2_1.Coordinates.Latitude = 54.772;
                        //obj2_1.Coordinates.Longitude = 27.622;
                        //tablesourcefhss.Add(obj2_1);
                        //TableSourceFHSS obj3_1 = new TableSourceFHSS();
                        //obj3_1.Coordinates = new Coord();
                        //obj3_1.Coordinates.Latitude = 54.771;
                        //obj3_1.Coordinates.Longitude = 27.683;
                        //obj3_1.Id = 5;
                        //obj3_1.IdFHSS = 77;
                        //tablesourcefhss.Add(obj3_1);
                        // ..........................................
                        // 3-я пачка (1 ИРИ)

                        TableReconFHSS obj1_2 = new TableReconFHSS();
                        obj1_2.Id = 99;
                        tablereconfhss.Add(obj1_2);

                        TableSourceFHSS obj2_2 = new TableSourceFHSS();
                        obj2_2.Coordinates = new Coord();
                        obj2_2.Id = 7;
                        obj2_2.IdFHSS = 99;
                        obj2_2.Coordinates.Latitude = 54.751;
                        obj2_2.Coordinates.Longitude = 27.778;
                        tablesourcefhss.Add(obj2_2);
                        // ..........................................

                        GlobalVarLn.sh1 += 1;

                    }
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    //  Добавился ИРИ в 1-й пачке (4 ИРИ)

                    else if (GlobalVarLn.sh1 == 3)
                    {
                        // ..........................................
                        // 1-я пачка (4 ИРИ)

                        TableReconFHSS obj1 = new TableReconFHSS();
                        obj1.Id = 66;
                        tablereconfhss.Add(obj1);

                        TableSourceFHSS obj2 = new TableSourceFHSS();
                        obj2.Coordinates = new Coord();
                        obj2.Id = 1;
                        obj2.IdFHSS = 66;
                        obj2.Coordinates.Latitude = 54.808;
                        obj2.Coordinates.Longitude = 27.622;
                        tablesourcefhss.Add(obj2);
                        TableSourceFHSS obj3 = new TableSourceFHSS();
                        obj3.Coordinates = new Coord();
                        obj3.Coordinates.Latitude = 54.808;
                        obj3.Coordinates.Longitude = 27.684;
                        obj3.Id = 2;
                        obj3.IdFHSS = 66;
                        tablesourcefhss.Add(obj3);
                        TableSourceFHSS obj4 = new TableSourceFHSS();
                        obj4.Coordinates = new Coord();
                        obj4.Coordinates.Latitude = 54.806;
                        obj4.Coordinates.Longitude = 27.745;
                        obj4.Id = 3;
                        obj4.IdFHSS = 66;
                        tablesourcefhss.Add(obj4);
                        TableSourceFHSS obj5 = new TableSourceFHSS();
                        obj5.Coordinates = new Coord();
                        obj5.Coordinates.Latitude = 54.807;
                        obj5.Coordinates.Longitude = 27.807;
                        obj5.Id = 4;
                        obj5.IdFHSS = 66;
                        tablesourcefhss.Add(obj5);

                        // ..........................................
                        // 2-я пачка (2 ИРИ)
                        // ..........................................
                        // 3-я пачка (1 ИРИ)

                        TableReconFHSS obj1_2 = new TableReconFHSS();
                        obj1_2.Id = 99;
                        tablereconfhss.Add(obj1_2);

                        TableSourceFHSS obj2_2 = new TableSourceFHSS();
                        obj2_2.Coordinates = new Coord();
                        obj2_2.Id = 7;
                        obj2_2.IdFHSS = 99;
                        obj2_2.Coordinates.Latitude = 54.751;
                        obj2_2.Coordinates.Longitude = 27.778;
                        tablesourcefhss.Add(obj2_2);
                        // ..........................................

                        GlobalVarLn.sh1 += 1;

                    } // sh1==3
                      // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                      //  Удалился ИРИ из 1-й пачки (3 ИРИ)

                    else if (GlobalVarLn.sh1 == 4)
                    {
                        // ..........................................
                        // 1-я пачка (3 ИРИ)

                        TableReconFHSS obj1 = new TableReconFHSS();
                        obj1.Id = 66;
                        tablereconfhss.Add(obj1);

                        //TableSourceFHSS obj2 = new TableSourceFHSS();
                        //obj2.Coordinates = new Coord();
                        //obj2.Id = 1;
                        //obj2.IdFHSS = 66;
                        //obj2.Coordinates.Latitude = 54.808;
                        //obj2.Coordinates.Longitude = 27.622;
                        //tablesourcefhss.Add(obj2);
                        TableSourceFHSS obj3 = new TableSourceFHSS();
                        obj3.Coordinates = new Coord();
                        obj3.Coordinates.Latitude = 54.808;
                        obj3.Coordinates.Longitude = 27.684;
                        obj3.Id = 2;
                        obj3.IdFHSS = 66;
                        tablesourcefhss.Add(obj3);
                        TableSourceFHSS obj4 = new TableSourceFHSS();
                        obj4.Coordinates = new Coord();
                        obj4.Coordinates.Latitude = 54.806;
                        obj4.Coordinates.Longitude = 27.745;
                        obj4.Id = 3;
                        obj4.IdFHSS = 66;
                        tablesourcefhss.Add(obj4);
                        TableSourceFHSS obj5 = new TableSourceFHSS();
                        obj5.Coordinates = new Coord();
                        obj5.Coordinates.Latitude = 54.807;
                        obj5.Coordinates.Longitude = 27.807;
                        obj5.Id = 4;
                        obj5.IdFHSS = 66;
                        tablesourcefhss.Add(obj5);

                        // ..........................................
                        // 2-я пачка (2 ИРИ)
                        // ..........................................
                        // 3-я пачка (1 ИРИ)

                        TableReconFHSS obj1_2 = new TableReconFHSS();
                        obj1_2.Id = 99;
                        tablereconfhss.Add(obj1_2);

                        TableSourceFHSS obj2_2 = new TableSourceFHSS();
                        obj2_2.Coordinates = new Coord();
                        obj2_2.Id = 7;
                        obj2_2.IdFHSS = 99;
                        obj2_2.Coordinates.Latitude = 54.751;
                        obj2_2.Coordinates.Longitude = 27.778;
                        tablesourcefhss.Add(obj2_2);
                        // ..........................................

                        GlobalVarLn.sh1 += 1;

                    }
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    //  Ничего не изменилось

                    else if (GlobalVarLn.sh1 == 5)
                    {
                        // ..........................................
                        // 1-я пачка (3 ИРИ)

                        TableReconFHSS obj1 = new TableReconFHSS();
                        obj1.Id = 66;
                        tablereconfhss.Add(obj1);

                        //TableSourceFHSS obj2 = new TableSourceFHSS();
                        //obj2.Coordinates = new Coord();
                        //obj2.Id = 1;
                        //obj2.IdFHSS = 66;
                        //obj2.Coordinates.Latitude = 54.808;
                        //obj2.Coordinates.Longitude = 27.622;
                        //tablesourcefhss.Add(obj2);
                        TableSourceFHSS obj3 = new TableSourceFHSS();
                        obj3.Coordinates = new Coord();
                        obj3.Coordinates.Latitude = 54.808;
                        obj3.Coordinates.Longitude = 27.684;
                        obj3.Id = 2;
                        obj3.IdFHSS = 66;
                        tablesourcefhss.Add(obj3);
                        TableSourceFHSS obj4 = new TableSourceFHSS();
                        obj4.Coordinates = new Coord();
                        obj4.Coordinates.Latitude = 54.806;
                        obj4.Coordinates.Longitude = 27.745;
                        obj4.Id = 3;
                        obj4.IdFHSS = 66;
                        tablesourcefhss.Add(obj4);
                        TableSourceFHSS obj5 = new TableSourceFHSS();
                        obj5.Coordinates = new Coord();
                        obj5.Coordinates.Latitude = 54.807;
                        obj5.Coordinates.Longitude = 27.807;
                        obj5.Id = 4;
                        obj5.IdFHSS = 66;
                        tablesourcefhss.Add(obj5);

                        // ..........................................
                        // 2-я пачка (2 ИРИ)
                        // ..........................................
                        // 3-я пачка (1 ИРИ)

                        TableReconFHSS obj1_2 = new TableReconFHSS();
                        obj1_2.Id = 99;
                        tablereconfhss.Add(obj1_2);

                        TableSourceFHSS obj2_2 = new TableSourceFHSS();
                        obj2_2.Coordinates = new Coord();
                        obj2_2.Id = 7;
                        obj2_2.IdFHSS = 99;
                        obj2_2.Coordinates.Latitude = 54.751;
                        obj2_2.Coordinates.Longitude = 27.778;
                        tablesourcefhss.Add(obj2_2);
                        // ..........................................

                        GlobalVarLn.sh1 += 1;

                    }
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        */

        /*
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // Две пачки

                    if (GlobalVarLn.sh1==0)
                    {
                        // ..........................................
                        // 1-я пачка (3 ИРИ)

                        TableReconFHSS obj1 = new TableReconFHSS();
                        obj1.ListSource = new List<TableSourceFHSS>();

                        TableSourceFHSS obj2 = new TableSourceFHSS();
                        obj2.Coordinates = new Coord();
                        obj2.Id = 1;
                        obj2.Coordinates.Latitude = 54.808;
                        obj2.Coordinates.Longitude = 27.622;
                        obj1.ListSource.Add(obj2);
                        TableSourceFHSS obj3 = new TableSourceFHSS();
                        obj3.Coordinates = new Coord();
                        obj3.Coordinates.Latitude = 54.808;
                        obj3.Coordinates.Longitude = 27.684;
                        obj3.Id = 2;
                        obj1.ListSource.Add(obj3);
                        TableSourceFHSS obj4 = new TableSourceFHSS();
                        obj4.Coordinates = new Coord();
                        obj4.Coordinates.Latitude = 54.806;
                        obj4.Coordinates.Longitude = 27.745;
                        obj4.Id = 3;
                        obj1.ListSource.Add(obj4);

                        obj1.Id = 66;

                        tablereconfhss.Add(obj1);
                        // ..........................................
                        // 2-я пачка (2 ИРИ)

                        TableReconFHSS obj1_1 = new TableReconFHSS();
                        obj1_1.ListSource = new List<TableSourceFHSS>();

                        TableSourceFHSS obj2_1 = new TableSourceFHSS();
                        obj2_1.Coordinates = new Coord();
                        obj2_1.Coordinates.Latitude = 54.772;
                        obj2_1.Coordinates.Longitude = 27.622;
                        obj2_1.Id = 5;
                        obj1_1.ListSource.Add(obj2_1);
                        TableSourceFHSS obj3_1 = new TableSourceFHSS();
                        obj3_1.Coordinates = new Coord();
                        obj3_1.Coordinates.Latitude = 54.771;
                        obj3_1.Coordinates.Longitude = 27.683;
                        obj3_1.Id = 6;
                        obj1_1.ListSource.Add(obj3_1);

                        obj1_1.Id = 77;

                        tablereconfhss.Add(obj1_1);

                        GlobalVarLn.sh1 += 1;
                        // ..........................................
                    } // sh1=0
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    //  Добавилась 3-яя пачка (1 ИРИ)

                    else if(GlobalVarLn.sh1==1)
                    {
                        // ..........................................
                        // 1-я пачка (3 ИРИ)

                        TableReconFHSS obj1 = new TableReconFHSS();
                        obj1.ListSource = new List<TableSourceFHSS>();

                        TableSourceFHSS obj2 = new TableSourceFHSS();
                        obj2.Coordinates = new Coord();
                        obj2.Id = 1;
                        obj2.Coordinates.Latitude = 54.808;
                        obj2.Coordinates.Longitude = 27.622;
                        obj1.ListSource.Add(obj2);
                        TableSourceFHSS obj3 = new TableSourceFHSS();
                        obj3.Coordinates = new Coord();
                        obj3.Coordinates.Latitude = 54.808;
                        obj3.Coordinates.Longitude = 27.684;
                        obj3.Id = 2;
                        obj1.ListSource.Add(obj3);
                        TableSourceFHSS obj4 = new TableSourceFHSS();
                        obj4.Coordinates = new Coord();
                        obj4.Coordinates.Latitude = 54.806;
                        obj4.Coordinates.Longitude = 27.745;
                        obj4.Id = 3;
                        obj1.ListSource.Add(obj4);

                        obj1.Id = 66;

                        tablereconfhss.Add(obj1);
                        // ..........................................
                        // 2-я пачка (2 ИРИ)

                        TableReconFHSS obj1_1 = new TableReconFHSS();
                        obj1_1.ListSource = new List<TableSourceFHSS>();

                        TableSourceFHSS obj2_1 = new TableSourceFHSS();
                        obj2_1.Coordinates = new Coord();
                        obj2_1.Coordinates.Latitude = 54.772;
                        obj2_1.Coordinates.Longitude = 27.622;
                        obj2_1.Id = 5;
                        obj1_1.ListSource.Add(obj2_1);
                        TableSourceFHSS obj3_1 = new TableSourceFHSS();
                        obj3_1.Coordinates = new Coord();
                        obj3_1.Coordinates.Latitude = 54.771;
                        obj3_1.Coordinates.Longitude = 27.683;
                        obj3_1.Id = 6;
                        obj1_1.ListSource.Add(obj3_1);

                        obj1_1.Id = 77;

                        tablereconfhss.Add(obj1_1);
                        // ..........................................
                        // 3-я пачка (1 ИРИ)

                        TableReconFHSS obj1_2 = new TableReconFHSS();
                        obj1_2.ListSource = new List<TableSourceFHSS>();

                        TableSourceFHSS obj2_2 = new TableSourceFHSS();
                        obj2_2.Coordinates = new Coord();
                        obj2_2.Coordinates.Latitude = 54.751;
                        obj2_2.Coordinates.Longitude = 27.778;
                        obj2_2.Id = 7;
                        obj1_2.ListSource.Add(obj2_2);

                        obj1_2.Id = 99;

                        tablereconfhss.Add(obj1_2);
                        // ..........................................

                        GlobalVarLn.sh1 += 1;

                    } // sh1==1
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    //  Удалилась 2-я пачка (2 ИРИ)

                    else if (GlobalVarLn.sh1 == 2)
                    {
                        // ..........................................
                        // 1-я пачка (3 ИРИ)

                        TableReconFHSS obj1 = new TableReconFHSS();
                        obj1.ListSource = new List<TableSourceFHSS>();

                        TableSourceFHSS obj2 = new TableSourceFHSS();
                        obj2.Coordinates = new Coord();
                        obj2.Id = 1;
                        obj2.Coordinates.Latitude = 54.808;
                        obj2.Coordinates.Longitude = 27.622;
                        obj1.ListSource.Add(obj2);
                        TableSourceFHSS obj3 = new TableSourceFHSS();
                        obj3.Coordinates = new Coord();
                        obj3.Coordinates.Latitude = 54.808;
                        obj3.Coordinates.Longitude = 27.684;
                        obj3.Id = 2;
                        obj1.ListSource.Add(obj3);
                        TableSourceFHSS obj4 = new TableSourceFHSS();
                        obj4.Coordinates = new Coord();
                        obj4.Coordinates.Latitude = 54.806;
                        obj4.Coordinates.Longitude = 27.745;
                        obj4.Id = 3;
                        obj1.ListSource.Add(obj4);

                        obj1.Id = 66;

                        tablereconfhss.Add(obj1);
                        // ..........................................
                        // 2-я пачка (2 ИРИ)
                        //TableReconFHSS obj1_1 = new TableReconFHSS();
                        //obj1_1.ListSource = new List<TableSourceFHSS>();
                        //
                        //TableSourceFHSS obj2_1 = new TableSourceFHSS();
                        //obj2_1.Coordinates = new Coord();
                        //obj2_1.Coordinates.Latitude = 54.772;
                        //obj2_1.Coordinates.Longitude = 27.622;
                        //obj2_1.Id = 5;
                        //obj1_1.ListSource.Add(obj2_1);
                        //TableSourceFHSS obj3_1 = new TableSourceFHSS();
                        //obj3_1.Coordinates = new Coord();
                        //obj3_1.Coordinates.Latitude = 54.771;
                        //obj3_1.Coordinates.Longitude = 27.683;
                        //obj3_1.Id = 6;
                        //obj1_1.ListSource.Add(obj3_1);
                        //
                        //obj1_1.Id = 77;
                        //
                        //tablereconfhss.Add(obj1_1);
                        // ..........................................
                        // 3-я пачка (1 ИРИ)

                        TableReconFHSS obj1_2 = new TableReconFHSS();
                        obj1_2.ListSource = new List<TableSourceFHSS>();

                        TableSourceFHSS obj2_2 = new TableSourceFHSS();
                        obj2_2.Coordinates = new Coord();
                        obj2_2.Coordinates.Latitude = 54.751;
                        obj2_2.Coordinates.Longitude = 27.778;
                        obj2_2.Id = 7;
                        obj1_2.ListSource.Add(obj2_2);

                        obj1_2.Id = 99;

                        tablereconfhss.Add(obj1_2);
                        // ..........................................

                        GlobalVarLn.sh1 += 1;

                    } // sh1==2
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    //  Добавился ИРИ в 1-й пачке (4 ИРИ)

                    else if (GlobalVarLn.sh1 == 3)
                    {
                        // ..........................................
                        // 1-я пачка (4 ИРИ)

                        TableReconFHSS obj1 = new TableReconFHSS();
                        obj1.ListSource = new List<TableSourceFHSS>();

                        TableSourceFHSS obj2 = new TableSourceFHSS();
                        obj2.Coordinates = new Coord();
                        obj2.Id = 1;
                        obj2.Coordinates.Latitude = 54.808;
                        obj2.Coordinates.Longitude = 27.622;
                        obj1.ListSource.Add(obj2);
                        TableSourceFHSS obj3 = new TableSourceFHSS();
                        obj3.Coordinates = new Coord();
                        obj3.Coordinates.Latitude = 54.808;
                        obj3.Coordinates.Longitude = 27.684;
                        obj3.Id = 2;
                        obj1.ListSource.Add(obj3);
                        TableSourceFHSS obj4 = new TableSourceFHSS();
                        obj4.Coordinates = new Coord();
                        obj4.Coordinates.Latitude = 54.806;
                        obj4.Coordinates.Longitude = 27.745;
                        obj4.Id = 3;
                        obj1.ListSource.Add(obj4);
                        TableSourceFHSS obj5 = new TableSourceFHSS();
                        obj5.Coordinates = new Coord();
                        obj5.Coordinates.Latitude = 54.807;
                        obj5.Coordinates.Longitude = 27.807;
                        obj5.Id = 4;
                        obj1.ListSource.Add(obj5);

                        obj1.Id = 66;

                        tablereconfhss.Add(obj1);
                        // ..........................................
                        // 2-я пачка (2 ИРИ)
                        // ..........................................
                        // 3-я пачка (1 ИРИ)

                        TableReconFHSS obj1_2 = new TableReconFHSS();
                        obj1_2.ListSource = new List<TableSourceFHSS>();

                        TableSourceFHSS obj2_2 = new TableSourceFHSS();
                        obj2_2.Coordinates = new Coord();
                        obj2_2.Coordinates.Latitude = 54.751;
                        obj2_2.Coordinates.Longitude = 27.778;
                        obj2_2.Id = 7;
                        obj1_2.ListSource.Add(obj2_2);

                        obj1_2.Id = 99;

                        tablereconfhss.Add(obj1_2);
                        // ..........................................

                        GlobalVarLn.sh1 += 1;

                    } // sh1==3
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    //  Удалился ИРИ из 1-й пачки (3 ИРИ)

                    else if (GlobalVarLn.sh1 == 4)
                    {
                        // ..........................................
                        // 1-я пачка (4 ИРИ)

                        TableReconFHSS obj1 = new TableReconFHSS();
                        obj1.ListSource = new List<TableSourceFHSS>();

                        //TableSourceFHSS obj2 = new TableSourceFHSS();
                        //obj2.Coordinates = new Coord();
                        //obj2.Id = 1;
                        //obj2.Coordinates.Latitude = 54.808;
                        //obj2.Coordinates.Longitude = 27.622;
                        //obj1.ListSource.Add(obj2);
                        TableSourceFHSS obj3 = new TableSourceFHSS();
                        obj3.Coordinates = new Coord();
                        obj3.Coordinates.Latitude = 54.808;
                        obj3.Coordinates.Longitude = 27.684;
                        obj3.Id = 2;
                        obj1.ListSource.Add(obj3);
                        TableSourceFHSS obj4 = new TableSourceFHSS();
                        obj4.Coordinates = new Coord();
                        obj4.Coordinates.Latitude = 54.806;
                        obj4.Coordinates.Longitude = 27.745;
                        obj4.Id = 3;
                        obj1.ListSource.Add(obj4);
                        TableSourceFHSS obj5 = new TableSourceFHSS();
                        obj5.Coordinates = new Coord();
                        obj5.Coordinates.Latitude = 54.807;
                        obj5.Coordinates.Longitude = 27.807;
                        obj5.Id = 4;
                        obj1.ListSource.Add(obj5);

                        obj1.Id = 66;

                        tablereconfhss.Add(obj1);
                        // ..........................................
                        // 2-я пачка (2 ИРИ)
                        // ..........................................
                        // 3-я пачка (1 ИРИ)

                        TableReconFHSS obj1_2 = new TableReconFHSS();
                        obj1_2.ListSource = new List<TableSourceFHSS>();

                        TableSourceFHSS obj2_2 = new TableSourceFHSS();
                        obj2_2.Coordinates = new Coord();
                        obj2_2.Coordinates.Latitude = 54.751;
                        obj2_2.Coordinates.Longitude = 27.778;
                        obj2_2.Id = 7;
                        obj1_2.ListSource.Add(obj2_2);

                        obj1_2.Id = 99;

                        tablereconfhss.Add(obj1_2);
                        // ..........................................

                        GlobalVarLn.sh1 += 1;

                    } // sh1==4
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    //  Ничего не изменилось

                    else if (GlobalVarLn.sh1 == 5)
                    {
                        // ..........................................
                        // 1-я пачка (4 ИРИ)

                        TableReconFHSS obj1 = new TableReconFHSS();
                        obj1.ListSource = new List<TableSourceFHSS>();

                        TableSourceFHSS obj3 = new TableSourceFHSS();
                        obj3.Coordinates = new Coord();
                        obj3.Coordinates.Latitude = 54.808;
                        obj3.Coordinates.Longitude = 27.684;
                        obj3.Id = 2;
                        obj1.ListSource.Add(obj3);
                        TableSourceFHSS obj4 = new TableSourceFHSS();
                        obj4.Coordinates = new Coord();
                        obj4.Coordinates.Latitude = 54.806;
                        obj4.Coordinates.Longitude = 27.745;
                        obj4.Id = 3;
                        obj1.ListSource.Add(obj4);
                        TableSourceFHSS obj5 = new TableSourceFHSS();
                        obj5.Coordinates = new Coord();
                        obj5.Coordinates.Latitude = 54.807;
                        obj5.Coordinates.Longitude = 27.807;
                        obj5.Id = 4;
                        obj1.ListSource.Add(obj5);

                        obj1.Id = 66;

                        tablereconfhss.Add(obj1);
                        // ..........................................
                        // 2-я пачка (2 ИРИ)
                        // ..........................................
                        // 3-я пачка (1 ИРИ)

                        TableReconFHSS obj1_2 = new TableReconFHSS();
                        obj1_2.ListSource = new List<TableSourceFHSS>();

                        TableSourceFHSS obj2_2 = new TableSourceFHSS();
                        obj2_2.Coordinates = new Coord();
                        obj2_2.Coordinates.Latitude = 54.751;
                        obj2_2.Coordinates.Longitude = 27.778;
                        obj2_2.Id = 7;
                        obj1_2.ListSource.Add(obj2_2);

                        obj1_2.Id = 99;

                        tablereconfhss.Add(obj1_2);
                        // ..........................................

                        GlobalVarLn.sh1 += 1;

                    } // sh1==5
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        */
        // ---------------------------------------------------------- Otl

        // ------------------------------------------------------------------- Загрузка ИРИ ППРЧ


        // **************************************************************************** MapForm

    }
}
