﻿namespace GrozaMap
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.tcParam = new System.Windows.Forms.TabControl();
            this.tpOwnObject = new System.Windows.Forms.TabPage();
            this.cbCenterLSR = new System.Windows.Forms.ComboBox();
            this.tbHAnt = new System.Windows.Forms.TextBox();
            this.tbHeightOwnObject = new System.Windows.Forms.TextBox();
            this.lHeightOwnObject = new System.Windows.Forms.Label();
            this.tbCoeffOwn = new System.Windows.Forms.TextBox();
            this.tbPowerOwn = new System.Windows.Forms.TextBox();
            this.cbSurface = new System.Windows.Forms.ComboBox();
            this.lSurface = new System.Windows.Forms.Label();
            this.cbWidthHindrance = new System.Windows.Forms.ComboBox();
            this.lWidthHindrance = new System.Windows.Forms.Label();
            this.pCoordPoint = new System.Windows.Forms.Panel();
            this.gbOwnRect = new System.Windows.Forms.GroupBox();
            this.tbYRect = new System.Windows.Forms.TextBox();
            this.lYRect = new System.Windows.Forms.Label();
            this.tbXRect = new System.Windows.Forms.TextBox();
            this.lXRect = new System.Windows.Forms.Label();
            this.tbOwnHeight = new System.Windows.Forms.TextBox();
            this.gbOwnDegMin = new System.Windows.Forms.GroupBox();
            this.tbLMin1 = new System.Windows.Forms.TextBox();
            this.tbBMin1 = new System.Windows.Forms.TextBox();
            this.lLDegMin = new System.Windows.Forms.Label();
            this.lBDegMin = new System.Windows.Forms.Label();
            this.gbOwnDegMinSec = new System.Windows.Forms.GroupBox();
            this.tbLSec = new System.Windows.Forms.TextBox();
            this.tbBSec = new System.Windows.Forms.TextBox();
            this.lMin4 = new System.Windows.Forms.Label();
            this.lMin3 = new System.Windows.Forms.Label();
            this.tbLMin2 = new System.Windows.Forms.TextBox();
            this.tbBMin2 = new System.Windows.Forms.TextBox();
            this.lSec2 = new System.Windows.Forms.Label();
            this.lSec1 = new System.Windows.Forms.Label();
            this.lDeg4 = new System.Windows.Forms.Label();
            this.lDeg3 = new System.Windows.Forms.Label();
            this.tbLDeg2 = new System.Windows.Forms.TextBox();
            this.lLDegMinSec = new System.Windows.Forms.Label();
            this.tbBDeg2 = new System.Windows.Forms.TextBox();
            this.lBDegMinSec = new System.Windows.Forms.Label();
            this.gbOwnRect42 = new System.Windows.Forms.GroupBox();
            this.tbYRect42 = new System.Windows.Forms.TextBox();
            this.lYRect42 = new System.Windows.Forms.Label();
            this.tbXRect42 = new System.Windows.Forms.TextBox();
            this.lXRect42 = new System.Windows.Forms.Label();
            this.lOwnHeight = new System.Windows.Forms.Label();
            this.gbOwnRad = new System.Windows.Forms.GroupBox();
            this.tbLRad = new System.Windows.Forms.TextBox();
            this.lLRad = new System.Windows.Forms.Label();
            this.tbBRad = new System.Windows.Forms.TextBox();
            this.lBRad = new System.Windows.Forms.Label();
            this.lCoeffOwn = new System.Windows.Forms.Label();
            this.lPowerOwn = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.tpOpponentObject = new System.Windows.Forms.TabPage();
            this.label21 = new System.Windows.Forms.Label();
            this.tbCoeffSupOpponent = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.nudCountOpponent = new System.Windows.Forms.NumericUpDown();
            this.lCountOpponent = new System.Windows.Forms.Label();
            this.cbPolarOpponent = new System.Windows.Forms.ComboBox();
            this.lPolarOpponent = new System.Windows.Forms.Label();
            this.tbRangeComm = new System.Windows.Forms.TextBox();
            this.lRangeComm = new System.Windows.Forms.Label();
            this.tbHeightReceiverOpponent = new System.Windows.Forms.TextBox();
            this.lHeightReceiverOpponent = new System.Windows.Forms.Label();
            this.tbHeightTransmitOpponent = new System.Windows.Forms.TextBox();
            this.lHeightTransmitOpponent = new System.Windows.Forms.Label();
            this.tbWidthSignal = new System.Windows.Forms.TextBox();
            this.lWidthSignal = new System.Windows.Forms.Label();
            this.tbFreq = new System.Windows.Forms.TextBox();
            this.lFreq = new System.Windows.Forms.Label();
            this.tbCoeffTransmitOpponent = new System.Windows.Forms.TextBox();
            this.lCoeffTransmitOpponent = new System.Windows.Forms.Label();
            this.tbPowerOpponent = new System.Windows.Forms.TextBox();
            this.lPowerOpponent = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.gbPoint2 = new System.Windows.Forms.GroupBox();
            this.tbPt2Height = new System.Windows.Forms.TextBox();
            this.lPt2Height = new System.Windows.Forms.Label();
            this.gbPt2DegMinSec = new System.Windows.Forms.GroupBox();
            this.tbPt2LSec = new System.Windows.Forms.TextBox();
            this.tbPt2BSec = new System.Windows.Forms.TextBox();
            this.lPt2Min4 = new System.Windows.Forms.Label();
            this.lPt2Min3 = new System.Windows.Forms.Label();
            this.tbPt2LMin2 = new System.Windows.Forms.TextBox();
            this.tbPt2BMin2 = new System.Windows.Forms.TextBox();
            this.lPt2Sec2 = new System.Windows.Forms.Label();
            this.lPt2Sec1 = new System.Windows.Forms.Label();
            this.lPt2Deg4 = new System.Windows.Forms.Label();
            this.lPt2Deg3 = new System.Windows.Forms.Label();
            this.tbPt2LDeg2 = new System.Windows.Forms.TextBox();
            this.lPt2LDegMinSec = new System.Windows.Forms.Label();
            this.tbPt2BDeg2 = new System.Windows.Forms.TextBox();
            this.lPt2BDegMinSec = new System.Windows.Forms.Label();
            this.gbPt2Rad = new System.Windows.Forms.GroupBox();
            this.tbPt2LRad = new System.Windows.Forms.TextBox();
            this.lPt2LRad = new System.Windows.Forms.Label();
            this.tbPt2BRad = new System.Windows.Forms.TextBox();
            this.lPt2BRad = new System.Windows.Forms.Label();
            this.gbPt2Rect = new System.Windows.Forms.GroupBox();
            this.tbPt2YRect = new System.Windows.Forms.TextBox();
            this.lPt2YRect = new System.Windows.Forms.Label();
            this.tbPt2XRect = new System.Windows.Forms.TextBox();
            this.lPt2XRect = new System.Windows.Forms.Label();
            this.gbPt2DegMin = new System.Windows.Forms.GroupBox();
            this.tbPt2LMin1 = new System.Windows.Forms.TextBox();
            this.tbPt2BMin1 = new System.Windows.Forms.TextBox();
            this.lPt2LDegMin = new System.Windows.Forms.Label();
            this.lPt2BDegMin = new System.Windows.Forms.Label();
            this.gbPt2Rect42 = new System.Windows.Forms.GroupBox();
            this.tbPt2YRect42 = new System.Windows.Forms.TextBox();
            this.lPt2YRect42 = new System.Windows.Forms.Label();
            this.tbPt2XRect42 = new System.Windows.Forms.TextBox();
            this.lPt2XRect42 = new System.Windows.Forms.Label();
            this.gbPoint1 = new System.Windows.Forms.GroupBox();
            this.tbPt1Height = new System.Windows.Forms.TextBox();
            this.lPt1Height = new System.Windows.Forms.Label();
            this.gbPt1DegMinSec = new System.Windows.Forms.GroupBox();
            this.tbPt1LSec = new System.Windows.Forms.TextBox();
            this.tbPt1BSec = new System.Windows.Forms.TextBox();
            this.lPt1Min4 = new System.Windows.Forms.Label();
            this.lPt1Min3 = new System.Windows.Forms.Label();
            this.tbPt1LMin2 = new System.Windows.Forms.TextBox();
            this.tbPt1BMin2 = new System.Windows.Forms.TextBox();
            this.lPt1Sec2 = new System.Windows.Forms.Label();
            this.lPt1Sec1 = new System.Windows.Forms.Label();
            this.lPt1Deg4 = new System.Windows.Forms.Label();
            this.lPt1Deg3 = new System.Windows.Forms.Label();
            this.tbPt1LDeg2 = new System.Windows.Forms.TextBox();
            this.lPt1LDegMinSec = new System.Windows.Forms.Label();
            this.tbPt1BDeg2 = new System.Windows.Forms.TextBox();
            this.lPt1BDegMinSec = new System.Windows.Forms.Label();
            this.gbPt1Rad = new System.Windows.Forms.GroupBox();
            this.tbPt1LRad = new System.Windows.Forms.TextBox();
            this.lPt1LRad = new System.Windows.Forms.Label();
            this.tbPt1BRad = new System.Windows.Forms.TextBox();
            this.lPt1BRad = new System.Windows.Forms.Label();
            this.gbPt1Rect = new System.Windows.Forms.GroupBox();
            this.tbPt1YRect = new System.Windows.Forms.TextBox();
            this.lPt1YRect = new System.Windows.Forms.Label();
            this.tbPt1XRect = new System.Windows.Forms.TextBox();
            this.lPt1XRect = new System.Windows.Forms.Label();
            this.gbPt1DegMin = new System.Windows.Forms.GroupBox();
            this.tbPt1LMin1 = new System.Windows.Forms.TextBox();
            this.tbPt1BMin1 = new System.Windows.Forms.TextBox();
            this.lPt1LDegMin = new System.Windows.Forms.Label();
            this.lPt1BDegMin = new System.Windows.Forms.Label();
            this.gbPt1Rect42 = new System.Windows.Forms.GroupBox();
            this.tbPt1YRect42 = new System.Windows.Forms.TextBox();
            this.lPt1YRect42 = new System.Windows.Forms.Label();
            this.tbPt1XRect42 = new System.Windows.Forms.TextBox();
            this.lPt1XRect42 = new System.Windows.Forms.Label();
            this.tbCoeffReceiverOpponent = new System.Windows.Forms.TextBox();
            this.lCoeffReceivertOpponent = new System.Windows.Forms.Label();
            this.lCapacity = new System.Windows.Forms.Label();
            this.cbCap1 = new System.Windows.Forms.ComboBox();
            this.cbTypeCommOpponent = new System.Windows.Forms.ComboBox();
            this.lTypeCommOpponent = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.rbPoint2 = new System.Windows.Forms.RadioButton();
            this.rbPoint1 = new System.Windows.Forms.RadioButton();
            this.label15 = new System.Windows.Forms.Label();
            this.chbXY = new System.Windows.Forms.CheckBox();
            this.lChooseSC = new System.Windows.Forms.Label();
            this.cbOwnObject = new System.Windows.Forms.ComboBox();
            this.lCenterLSR = new System.Windows.Forms.Label();
            this.cbChooseSC = new System.Windows.Forms.ComboBox();
            this.cbHeightOwnObject = new System.Windows.Forms.ComboBox();
            this.grbCoeffSupOpponent = new System.Windows.Forms.GroupBox();
            this.cbCoeffSupOpponent = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.chbXY1 = new System.Windows.Forms.CheckBox();
            this.cbCommChooseSC = new System.Windows.Forms.ComboBox();
            this.lCommChooseSC = new System.Windows.Forms.Label();
            this.tbRadiusZone = new System.Windows.Forms.TextBox();
            this.lRadiusZone = new System.Windows.Forms.Label();
            this.tbResult = new System.Windows.Forms.TextBox();
            this.tbMaxDist = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tbHindSignal = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.bAccept = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.grbDetail = new System.Windows.Forms.GroupBox();
            this.tbCorrectHeightOpponent = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbGamma = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbResultHeightOpponent = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbMinHeight = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbMiddleHeight = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbMaxDistance = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbCorrectHeightOwn = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbCoeffHE = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbCoeffQ = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbResultHeightOwn = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbMidH = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.tbHindSignal2 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tcParam.SuspendLayout();
            this.tpOwnObject.SuspendLayout();
            this.pCoordPoint.SuspendLayout();
            this.gbOwnRect.SuspendLayout();
            this.gbOwnDegMin.SuspendLayout();
            this.gbOwnDegMinSec.SuspendLayout();
            this.gbOwnRect42.SuspendLayout();
            this.gbOwnRad.SuspendLayout();
            this.tpOpponentObject.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCountOpponent)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.gbPoint2.SuspendLayout();
            this.gbPt2DegMinSec.SuspendLayout();
            this.gbPt2Rad.SuspendLayout();
            this.gbPt2Rect.SuspendLayout();
            this.gbPt2DegMin.SuspendLayout();
            this.gbPt2Rect42.SuspendLayout();
            this.gbPoint1.SuspendLayout();
            this.gbPt1DegMinSec.SuspendLayout();
            this.gbPt1Rad.SuspendLayout();
            this.gbPt1Rect.SuspendLayout();
            this.gbPt1DegMin.SuspendLayout();
            this.gbPt1Rect42.SuspendLayout();
            this.grbCoeffSupOpponent.SuspendLayout();
            this.grbDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcParam
            // 
            resources.ApplyResources(this.tcParam, "tcParam");
            this.tcParam.Controls.Add(this.tpOwnObject);
            this.tcParam.Controls.Add(this.tpOpponentObject);
            this.tcParam.Controls.Add(this.tabPage1);
            this.tcParam.Name = "tcParam";
            this.tcParam.SelectedIndex = 0;
            // 
            // tpOwnObject
            // 
            resources.ApplyResources(this.tpOwnObject, "tpOwnObject");
            this.tpOwnObject.BackColor = System.Drawing.SystemColors.Control;
            this.tpOwnObject.Controls.Add(this.cbCenterLSR);
            this.tpOwnObject.Controls.Add(this.tbHAnt);
            this.tpOwnObject.Controls.Add(this.tbHeightOwnObject);
            this.tpOwnObject.Controls.Add(this.lHeightOwnObject);
            this.tpOwnObject.Controls.Add(this.tbCoeffOwn);
            this.tpOwnObject.Controls.Add(this.tbPowerOwn);
            this.tpOwnObject.Controls.Add(this.cbSurface);
            this.tpOwnObject.Controls.Add(this.lSurface);
            this.tpOwnObject.Controls.Add(this.cbWidthHindrance);
            this.tpOwnObject.Controls.Add(this.lWidthHindrance);
            this.tpOwnObject.Controls.Add(this.pCoordPoint);
            this.tpOwnObject.Controls.Add(this.lCoeffOwn);
            this.tpOwnObject.Controls.Add(this.lPowerOwn);
            this.tpOwnObject.Controls.Add(this.label17);
            this.tpOwnObject.Controls.Add(this.label38);
            this.tpOwnObject.Name = "tpOwnObject";
            // 
            // cbCenterLSR
            // 
            resources.ApplyResources(this.cbCenterLSR, "cbCenterLSR");
            this.cbCenterLSR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCenterLSR.FormattingEnabled = true;
            this.cbCenterLSR.Items.AddRange(new object[] {
            resources.GetString("cbCenterLSR.Items"),
            resources.GetString("cbCenterLSR.Items1"),
            resources.GetString("cbCenterLSR.Items2"),
            resources.GetString("cbCenterLSR.Items3")});
            this.cbCenterLSR.Name = "cbCenterLSR";
            // 
            // tbHAnt
            // 
            resources.ApplyResources(this.tbHAnt, "tbHAnt");
            this.tbHAnt.Name = "tbHAnt";
            // 
            // tbHeightOwnObject
            // 
            resources.ApplyResources(this.tbHeightOwnObject, "tbHeightOwnObject");
            this.tbHeightOwnObject.Name = "tbHeightOwnObject";
            // 
            // lHeightOwnObject
            // 
            resources.ApplyResources(this.lHeightOwnObject, "lHeightOwnObject");
            this.lHeightOwnObject.Name = "lHeightOwnObject";
            // 
            // tbCoeffOwn
            // 
            resources.ApplyResources(this.tbCoeffOwn, "tbCoeffOwn");
            this.tbCoeffOwn.Name = "tbCoeffOwn";
            // 
            // tbPowerOwn
            // 
            resources.ApplyResources(this.tbPowerOwn, "tbPowerOwn");
            this.tbPowerOwn.Name = "tbPowerOwn";
            // 
            // cbSurface
            // 
            resources.ApplyResources(this.cbSurface, "cbSurface");
            this.cbSurface.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSurface.FormattingEnabled = true;
            this.cbSurface.Items.AddRange(new object[] {
            resources.GetString("cbSurface.Items"),
            resources.GetString("cbSurface.Items1"),
            resources.GetString("cbSurface.Items2"),
            resources.GetString("cbSurface.Items3"),
            resources.GetString("cbSurface.Items4")});
            this.cbSurface.Name = "cbSurface";
            // 
            // lSurface
            // 
            resources.ApplyResources(this.lSurface, "lSurface");
            this.lSurface.Name = "lSurface";
            // 
            // cbWidthHindrance
            // 
            resources.ApplyResources(this.cbWidthHindrance, "cbWidthHindrance");
            this.cbWidthHindrance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWidthHindrance.FormattingEnabled = true;
            this.cbWidthHindrance.Items.AddRange(new object[] {
            resources.GetString("cbWidthHindrance.Items"),
            resources.GetString("cbWidthHindrance.Items1"),
            resources.GetString("cbWidthHindrance.Items2"),
            resources.GetString("cbWidthHindrance.Items3"),
            resources.GetString("cbWidthHindrance.Items4"),
            resources.GetString("cbWidthHindrance.Items5"),
            resources.GetString("cbWidthHindrance.Items6"),
            resources.GetString("cbWidthHindrance.Items7"),
            resources.GetString("cbWidthHindrance.Items8"),
            resources.GetString("cbWidthHindrance.Items9"),
            resources.GetString("cbWidthHindrance.Items10"),
            resources.GetString("cbWidthHindrance.Items11"),
            resources.GetString("cbWidthHindrance.Items12"),
            resources.GetString("cbWidthHindrance.Items13"),
            resources.GetString("cbWidthHindrance.Items14"),
            resources.GetString("cbWidthHindrance.Items15"),
            resources.GetString("cbWidthHindrance.Items16"),
            resources.GetString("cbWidthHindrance.Items17"),
            resources.GetString("cbWidthHindrance.Items18")});
            this.cbWidthHindrance.Name = "cbWidthHindrance";
            this.cbWidthHindrance.SelectedIndexChanged += new System.EventHandler(this.cbWidthHindrance_SelectedIndexChanged);
            // 
            // lWidthHindrance
            // 
            resources.ApplyResources(this.lWidthHindrance, "lWidthHindrance");
            this.lWidthHindrance.Name = "lWidthHindrance";
            // 
            // pCoordPoint
            // 
            resources.ApplyResources(this.pCoordPoint, "pCoordPoint");
            this.pCoordPoint.Controls.Add(this.gbOwnRect);
            this.pCoordPoint.Controls.Add(this.tbOwnHeight);
            this.pCoordPoint.Controls.Add(this.gbOwnDegMin);
            this.pCoordPoint.Controls.Add(this.gbOwnDegMinSec);
            this.pCoordPoint.Controls.Add(this.gbOwnRect42);
            this.pCoordPoint.Controls.Add(this.lOwnHeight);
            this.pCoordPoint.Controls.Add(this.gbOwnRad);
            this.pCoordPoint.Name = "pCoordPoint";
            // 
            // gbOwnRect
            // 
            resources.ApplyResources(this.gbOwnRect, "gbOwnRect");
            this.gbOwnRect.Controls.Add(this.tbYRect);
            this.gbOwnRect.Controls.Add(this.lYRect);
            this.gbOwnRect.Controls.Add(this.tbXRect);
            this.gbOwnRect.Controls.Add(this.lXRect);
            this.gbOwnRect.Name = "gbOwnRect";
            this.gbOwnRect.TabStop = false;
            // 
            // tbYRect
            // 
            resources.ApplyResources(this.tbYRect, "tbYRect");
            this.tbYRect.Name = "tbYRect";
            // 
            // lYRect
            // 
            resources.ApplyResources(this.lYRect, "lYRect");
            this.lYRect.Name = "lYRect";
            // 
            // tbXRect
            // 
            resources.ApplyResources(this.tbXRect, "tbXRect");
            this.tbXRect.Name = "tbXRect";
            // 
            // lXRect
            // 
            resources.ApplyResources(this.lXRect, "lXRect");
            this.lXRect.Name = "lXRect";
            // 
            // tbOwnHeight
            // 
            resources.ApplyResources(this.tbOwnHeight, "tbOwnHeight");
            this.tbOwnHeight.BackColor = System.Drawing.SystemColors.Menu;
            this.tbOwnHeight.Name = "tbOwnHeight";
            // 
            // gbOwnDegMin
            // 
            resources.ApplyResources(this.gbOwnDegMin, "gbOwnDegMin");
            this.gbOwnDegMin.Controls.Add(this.tbLMin1);
            this.gbOwnDegMin.Controls.Add(this.tbBMin1);
            this.gbOwnDegMin.Controls.Add(this.lLDegMin);
            this.gbOwnDegMin.Controls.Add(this.lBDegMin);
            this.gbOwnDegMin.Name = "gbOwnDegMin";
            this.gbOwnDegMin.TabStop = false;
            // 
            // tbLMin1
            // 
            resources.ApplyResources(this.tbLMin1, "tbLMin1");
            this.tbLMin1.Name = "tbLMin1";
            // 
            // tbBMin1
            // 
            resources.ApplyResources(this.tbBMin1, "tbBMin1");
            this.tbBMin1.Name = "tbBMin1";
            // 
            // lLDegMin
            // 
            resources.ApplyResources(this.lLDegMin, "lLDegMin");
            this.lLDegMin.Name = "lLDegMin";
            // 
            // lBDegMin
            // 
            resources.ApplyResources(this.lBDegMin, "lBDegMin");
            this.lBDegMin.Name = "lBDegMin";
            // 
            // gbOwnDegMinSec
            // 
            resources.ApplyResources(this.gbOwnDegMinSec, "gbOwnDegMinSec");
            this.gbOwnDegMinSec.Controls.Add(this.tbLSec);
            this.gbOwnDegMinSec.Controls.Add(this.tbBSec);
            this.gbOwnDegMinSec.Controls.Add(this.lMin4);
            this.gbOwnDegMinSec.Controls.Add(this.lMin3);
            this.gbOwnDegMinSec.Controls.Add(this.tbLMin2);
            this.gbOwnDegMinSec.Controls.Add(this.tbBMin2);
            this.gbOwnDegMinSec.Controls.Add(this.lSec2);
            this.gbOwnDegMinSec.Controls.Add(this.lSec1);
            this.gbOwnDegMinSec.Controls.Add(this.lDeg4);
            this.gbOwnDegMinSec.Controls.Add(this.lDeg3);
            this.gbOwnDegMinSec.Controls.Add(this.tbLDeg2);
            this.gbOwnDegMinSec.Controls.Add(this.lLDegMinSec);
            this.gbOwnDegMinSec.Controls.Add(this.tbBDeg2);
            this.gbOwnDegMinSec.Controls.Add(this.lBDegMinSec);
            this.gbOwnDegMinSec.Name = "gbOwnDegMinSec";
            this.gbOwnDegMinSec.TabStop = false;
            // 
            // tbLSec
            // 
            resources.ApplyResources(this.tbLSec, "tbLSec");
            this.tbLSec.Name = "tbLSec";
            // 
            // tbBSec
            // 
            resources.ApplyResources(this.tbBSec, "tbBSec");
            this.tbBSec.Name = "tbBSec";
            // 
            // lMin4
            // 
            resources.ApplyResources(this.lMin4, "lMin4");
            this.lMin4.Name = "lMin4";
            // 
            // lMin3
            // 
            resources.ApplyResources(this.lMin3, "lMin3");
            this.lMin3.Name = "lMin3";
            // 
            // tbLMin2
            // 
            resources.ApplyResources(this.tbLMin2, "tbLMin2");
            this.tbLMin2.Name = "tbLMin2";
            // 
            // tbBMin2
            // 
            resources.ApplyResources(this.tbBMin2, "tbBMin2");
            this.tbBMin2.Name = "tbBMin2";
            // 
            // lSec2
            // 
            resources.ApplyResources(this.lSec2, "lSec2");
            this.lSec2.Name = "lSec2";
            // 
            // lSec1
            // 
            resources.ApplyResources(this.lSec1, "lSec1");
            this.lSec1.Name = "lSec1";
            // 
            // lDeg4
            // 
            resources.ApplyResources(this.lDeg4, "lDeg4");
            this.lDeg4.Name = "lDeg4";
            // 
            // lDeg3
            // 
            resources.ApplyResources(this.lDeg3, "lDeg3");
            this.lDeg3.Name = "lDeg3";
            // 
            // tbLDeg2
            // 
            resources.ApplyResources(this.tbLDeg2, "tbLDeg2");
            this.tbLDeg2.Name = "tbLDeg2";
            // 
            // lLDegMinSec
            // 
            resources.ApplyResources(this.lLDegMinSec, "lLDegMinSec");
            this.lLDegMinSec.Name = "lLDegMinSec";
            // 
            // tbBDeg2
            // 
            resources.ApplyResources(this.tbBDeg2, "tbBDeg2");
            this.tbBDeg2.Name = "tbBDeg2";
            // 
            // lBDegMinSec
            // 
            resources.ApplyResources(this.lBDegMinSec, "lBDegMinSec");
            this.lBDegMinSec.Name = "lBDegMinSec";
            // 
            // gbOwnRect42
            // 
            resources.ApplyResources(this.gbOwnRect42, "gbOwnRect42");
            this.gbOwnRect42.Controls.Add(this.tbYRect42);
            this.gbOwnRect42.Controls.Add(this.lYRect42);
            this.gbOwnRect42.Controls.Add(this.tbXRect42);
            this.gbOwnRect42.Controls.Add(this.lXRect42);
            this.gbOwnRect42.Name = "gbOwnRect42";
            this.gbOwnRect42.TabStop = false;
            // 
            // tbYRect42
            // 
            resources.ApplyResources(this.tbYRect42, "tbYRect42");
            this.tbYRect42.Name = "tbYRect42";
            // 
            // lYRect42
            // 
            resources.ApplyResources(this.lYRect42, "lYRect42");
            this.lYRect42.Name = "lYRect42";
            // 
            // tbXRect42
            // 
            resources.ApplyResources(this.tbXRect42, "tbXRect42");
            this.tbXRect42.Name = "tbXRect42";
            // 
            // lXRect42
            // 
            resources.ApplyResources(this.lXRect42, "lXRect42");
            this.lXRect42.Name = "lXRect42";
            // 
            // lOwnHeight
            // 
            resources.ApplyResources(this.lOwnHeight, "lOwnHeight");
            this.lOwnHeight.Name = "lOwnHeight";
            // 
            // gbOwnRad
            // 
            resources.ApplyResources(this.gbOwnRad, "gbOwnRad");
            this.gbOwnRad.Controls.Add(this.tbLRad);
            this.gbOwnRad.Controls.Add(this.lLRad);
            this.gbOwnRad.Controls.Add(this.tbBRad);
            this.gbOwnRad.Controls.Add(this.lBRad);
            this.gbOwnRad.Name = "gbOwnRad";
            this.gbOwnRad.TabStop = false;
            // 
            // tbLRad
            // 
            resources.ApplyResources(this.tbLRad, "tbLRad");
            this.tbLRad.Name = "tbLRad";
            // 
            // lLRad
            // 
            resources.ApplyResources(this.lLRad, "lLRad");
            this.lLRad.Name = "lLRad";
            // 
            // tbBRad
            // 
            resources.ApplyResources(this.tbBRad, "tbBRad");
            this.tbBRad.Name = "tbBRad";
            // 
            // lBRad
            // 
            resources.ApplyResources(this.lBRad, "lBRad");
            this.lBRad.Name = "lBRad";
            // 
            // lCoeffOwn
            // 
            resources.ApplyResources(this.lCoeffOwn, "lCoeffOwn");
            this.lCoeffOwn.Name = "lCoeffOwn";
            // 
            // lPowerOwn
            // 
            resources.ApplyResources(this.lPowerOwn, "lPowerOwn");
            this.lPowerOwn.Name = "lPowerOwn";
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // label38
            // 
            resources.ApplyResources(this.label38, "label38");
            this.label38.Name = "label38";
            // 
            // tpOpponentObject
            // 
            resources.ApplyResources(this.tpOpponentObject, "tpOpponentObject");
            this.tpOpponentObject.BackColor = System.Drawing.SystemColors.Control;
            this.tpOpponentObject.Controls.Add(this.label21);
            this.tpOpponentObject.Controls.Add(this.tbCoeffSupOpponent);
            this.tpOpponentObject.Controls.Add(this.label20);
            this.tpOpponentObject.Controls.Add(this.nudCountOpponent);
            this.tpOpponentObject.Controls.Add(this.lCountOpponent);
            this.tpOpponentObject.Controls.Add(this.cbPolarOpponent);
            this.tpOpponentObject.Controls.Add(this.lPolarOpponent);
            this.tpOpponentObject.Controls.Add(this.tbRangeComm);
            this.tpOpponentObject.Controls.Add(this.lRangeComm);
            this.tpOpponentObject.Controls.Add(this.tbHeightReceiverOpponent);
            this.tpOpponentObject.Controls.Add(this.lHeightReceiverOpponent);
            this.tpOpponentObject.Controls.Add(this.tbHeightTransmitOpponent);
            this.tpOpponentObject.Controls.Add(this.lHeightTransmitOpponent);
            this.tpOpponentObject.Controls.Add(this.tbWidthSignal);
            this.tpOpponentObject.Controls.Add(this.lWidthSignal);
            this.tpOpponentObject.Controls.Add(this.tbFreq);
            this.tpOpponentObject.Controls.Add(this.lFreq);
            this.tpOpponentObject.Controls.Add(this.tbCoeffTransmitOpponent);
            this.tpOpponentObject.Controls.Add(this.lCoeffTransmitOpponent);
            this.tpOpponentObject.Controls.Add(this.tbPowerOpponent);
            this.tpOpponentObject.Controls.Add(this.lPowerOpponent);
            this.tpOpponentObject.Controls.Add(this.label18);
            this.tpOpponentObject.Name = "tpOpponentObject";
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.Name = "label21";
            // 
            // tbCoeffSupOpponent
            // 
            resources.ApplyResources(this.tbCoeffSupOpponent, "tbCoeffSupOpponent");
            this.tbCoeffSupOpponent.Name = "tbCoeffSupOpponent";
            // 
            // label20
            // 
            resources.ApplyResources(this.label20, "label20");
            this.label20.Name = "label20";
            // 
            // nudCountOpponent
            // 
            resources.ApplyResources(this.nudCountOpponent, "nudCountOpponent");
            this.nudCountOpponent.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudCountOpponent.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudCountOpponent.Name = "nudCountOpponent";
            this.nudCountOpponent.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudCountOpponent.ValueChanged += new System.EventHandler(this.nudCountOpponent_ValueChanged);
            // 
            // lCountOpponent
            // 
            resources.ApplyResources(this.lCountOpponent, "lCountOpponent");
            this.lCountOpponent.Name = "lCountOpponent";
            // 
            // cbPolarOpponent
            // 
            resources.ApplyResources(this.cbPolarOpponent, "cbPolarOpponent");
            this.cbPolarOpponent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPolarOpponent.FormattingEnabled = true;
            this.cbPolarOpponent.Items.AddRange(new object[] {
            resources.GetString("cbPolarOpponent.Items"),
            resources.GetString("cbPolarOpponent.Items1"),
            resources.GetString("cbPolarOpponent.Items2"),
            resources.GetString("cbPolarOpponent.Items3")});
            this.cbPolarOpponent.Name = "cbPolarOpponent";
            // 
            // lPolarOpponent
            // 
            resources.ApplyResources(this.lPolarOpponent, "lPolarOpponent");
            this.lPolarOpponent.Name = "lPolarOpponent";
            // 
            // tbRangeComm
            // 
            resources.ApplyResources(this.tbRangeComm, "tbRangeComm");
            this.tbRangeComm.Name = "tbRangeComm";
            // 
            // lRangeComm
            // 
            resources.ApplyResources(this.lRangeComm, "lRangeComm");
            this.lRangeComm.Name = "lRangeComm";
            // 
            // tbHeightReceiverOpponent
            // 
            resources.ApplyResources(this.tbHeightReceiverOpponent, "tbHeightReceiverOpponent");
            this.tbHeightReceiverOpponent.Name = "tbHeightReceiverOpponent";
            // 
            // lHeightReceiverOpponent
            // 
            resources.ApplyResources(this.lHeightReceiverOpponent, "lHeightReceiverOpponent");
            this.lHeightReceiverOpponent.Name = "lHeightReceiverOpponent";
            // 
            // tbHeightTransmitOpponent
            // 
            resources.ApplyResources(this.tbHeightTransmitOpponent, "tbHeightTransmitOpponent");
            this.tbHeightTransmitOpponent.Name = "tbHeightTransmitOpponent";
            // 
            // lHeightTransmitOpponent
            // 
            resources.ApplyResources(this.lHeightTransmitOpponent, "lHeightTransmitOpponent");
            this.lHeightTransmitOpponent.Name = "lHeightTransmitOpponent";
            // 
            // tbWidthSignal
            // 
            resources.ApplyResources(this.tbWidthSignal, "tbWidthSignal");
            this.tbWidthSignal.Name = "tbWidthSignal";
            // 
            // lWidthSignal
            // 
            resources.ApplyResources(this.lWidthSignal, "lWidthSignal");
            this.lWidthSignal.Name = "lWidthSignal";
            // 
            // tbFreq
            // 
            resources.ApplyResources(this.tbFreq, "tbFreq");
            this.tbFreq.Name = "tbFreq";
            // 
            // lFreq
            // 
            resources.ApplyResources(this.lFreq, "lFreq");
            this.lFreq.Name = "lFreq";
            // 
            // tbCoeffTransmitOpponent
            // 
            resources.ApplyResources(this.tbCoeffTransmitOpponent, "tbCoeffTransmitOpponent");
            this.tbCoeffTransmitOpponent.Name = "tbCoeffTransmitOpponent";
            // 
            // lCoeffTransmitOpponent
            // 
            resources.ApplyResources(this.lCoeffTransmitOpponent, "lCoeffTransmitOpponent");
            this.lCoeffTransmitOpponent.Name = "lCoeffTransmitOpponent";
            // 
            // tbPowerOpponent
            // 
            resources.ApplyResources(this.tbPowerOpponent, "tbPowerOpponent");
            this.tbPowerOpponent.Name = "tbPowerOpponent";
            // 
            // lPowerOpponent
            // 
            resources.ApplyResources(this.lPowerOpponent, "lPowerOpponent");
            this.lPowerOpponent.Name = "lPowerOpponent";
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // tabPage1
            // 
            resources.ApplyResources(this.tabPage1, "tabPage1");
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.gbPoint2);
            this.tabPage1.Controls.Add(this.gbPoint1);
            this.tabPage1.Name = "tabPage1";
            // 
            // gbPoint2
            // 
            resources.ApplyResources(this.gbPoint2, "gbPoint2");
            this.gbPoint2.Controls.Add(this.tbPt2Height);
            this.gbPoint2.Controls.Add(this.lPt2Height);
            this.gbPoint2.Controls.Add(this.gbPt2DegMinSec);
            this.gbPoint2.Controls.Add(this.gbPt2Rad);
            this.gbPoint2.Controls.Add(this.gbPt2Rect);
            this.gbPoint2.Controls.Add(this.gbPt2DegMin);
            this.gbPoint2.Controls.Add(this.gbPt2Rect42);
            this.gbPoint2.Name = "gbPoint2";
            this.gbPoint2.TabStop = false;
            // 
            // tbPt2Height
            // 
            resources.ApplyResources(this.tbPt2Height, "tbPt2Height");
            this.tbPt2Height.BackColor = System.Drawing.SystemColors.Menu;
            this.tbPt2Height.Name = "tbPt2Height";
            // 
            // lPt2Height
            // 
            resources.ApplyResources(this.lPt2Height, "lPt2Height");
            this.lPt2Height.Name = "lPt2Height";
            // 
            // gbPt2DegMinSec
            // 
            resources.ApplyResources(this.gbPt2DegMinSec, "gbPt2DegMinSec");
            this.gbPt2DegMinSec.Controls.Add(this.tbPt2LSec);
            this.gbPt2DegMinSec.Controls.Add(this.tbPt2BSec);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2Min4);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2Min3);
            this.gbPt2DegMinSec.Controls.Add(this.tbPt2LMin2);
            this.gbPt2DegMinSec.Controls.Add(this.tbPt2BMin2);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2Sec2);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2Sec1);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2Deg4);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2Deg3);
            this.gbPt2DegMinSec.Controls.Add(this.tbPt2LDeg2);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2LDegMinSec);
            this.gbPt2DegMinSec.Controls.Add(this.tbPt2BDeg2);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2BDegMinSec);
            this.gbPt2DegMinSec.Name = "gbPt2DegMinSec";
            this.gbPt2DegMinSec.TabStop = false;
            // 
            // tbPt2LSec
            // 
            resources.ApplyResources(this.tbPt2LSec, "tbPt2LSec");
            this.tbPt2LSec.Name = "tbPt2LSec";
            // 
            // tbPt2BSec
            // 
            resources.ApplyResources(this.tbPt2BSec, "tbPt2BSec");
            this.tbPt2BSec.Name = "tbPt2BSec";
            // 
            // lPt2Min4
            // 
            resources.ApplyResources(this.lPt2Min4, "lPt2Min4");
            this.lPt2Min4.Name = "lPt2Min4";
            // 
            // lPt2Min3
            // 
            resources.ApplyResources(this.lPt2Min3, "lPt2Min3");
            this.lPt2Min3.Name = "lPt2Min3";
            // 
            // tbPt2LMin2
            // 
            resources.ApplyResources(this.tbPt2LMin2, "tbPt2LMin2");
            this.tbPt2LMin2.Name = "tbPt2LMin2";
            // 
            // tbPt2BMin2
            // 
            resources.ApplyResources(this.tbPt2BMin2, "tbPt2BMin2");
            this.tbPt2BMin2.Name = "tbPt2BMin2";
            // 
            // lPt2Sec2
            // 
            resources.ApplyResources(this.lPt2Sec2, "lPt2Sec2");
            this.lPt2Sec2.Name = "lPt2Sec2";
            // 
            // lPt2Sec1
            // 
            resources.ApplyResources(this.lPt2Sec1, "lPt2Sec1");
            this.lPt2Sec1.Name = "lPt2Sec1";
            // 
            // lPt2Deg4
            // 
            resources.ApplyResources(this.lPt2Deg4, "lPt2Deg4");
            this.lPt2Deg4.Name = "lPt2Deg4";
            // 
            // lPt2Deg3
            // 
            resources.ApplyResources(this.lPt2Deg3, "lPt2Deg3");
            this.lPt2Deg3.Name = "lPt2Deg3";
            // 
            // tbPt2LDeg2
            // 
            resources.ApplyResources(this.tbPt2LDeg2, "tbPt2LDeg2");
            this.tbPt2LDeg2.Name = "tbPt2LDeg2";
            // 
            // lPt2LDegMinSec
            // 
            resources.ApplyResources(this.lPt2LDegMinSec, "lPt2LDegMinSec");
            this.lPt2LDegMinSec.Name = "lPt2LDegMinSec";
            // 
            // tbPt2BDeg2
            // 
            resources.ApplyResources(this.tbPt2BDeg2, "tbPt2BDeg2");
            this.tbPt2BDeg2.Name = "tbPt2BDeg2";
            // 
            // lPt2BDegMinSec
            // 
            resources.ApplyResources(this.lPt2BDegMinSec, "lPt2BDegMinSec");
            this.lPt2BDegMinSec.Name = "lPt2BDegMinSec";
            // 
            // gbPt2Rad
            // 
            resources.ApplyResources(this.gbPt2Rad, "gbPt2Rad");
            this.gbPt2Rad.Controls.Add(this.tbPt2LRad);
            this.gbPt2Rad.Controls.Add(this.lPt2LRad);
            this.gbPt2Rad.Controls.Add(this.tbPt2BRad);
            this.gbPt2Rad.Controls.Add(this.lPt2BRad);
            this.gbPt2Rad.Name = "gbPt2Rad";
            this.gbPt2Rad.TabStop = false;
            // 
            // tbPt2LRad
            // 
            resources.ApplyResources(this.tbPt2LRad, "tbPt2LRad");
            this.tbPt2LRad.Name = "tbPt2LRad";
            // 
            // lPt2LRad
            // 
            resources.ApplyResources(this.lPt2LRad, "lPt2LRad");
            this.lPt2LRad.Name = "lPt2LRad";
            // 
            // tbPt2BRad
            // 
            resources.ApplyResources(this.tbPt2BRad, "tbPt2BRad");
            this.tbPt2BRad.Name = "tbPt2BRad";
            // 
            // lPt2BRad
            // 
            resources.ApplyResources(this.lPt2BRad, "lPt2BRad");
            this.lPt2BRad.Name = "lPt2BRad";
            // 
            // gbPt2Rect
            // 
            resources.ApplyResources(this.gbPt2Rect, "gbPt2Rect");
            this.gbPt2Rect.Controls.Add(this.tbPt2YRect);
            this.gbPt2Rect.Controls.Add(this.lPt2YRect);
            this.gbPt2Rect.Controls.Add(this.tbPt2XRect);
            this.gbPt2Rect.Controls.Add(this.lPt2XRect);
            this.gbPt2Rect.Name = "gbPt2Rect";
            this.gbPt2Rect.TabStop = false;
            // 
            // tbPt2YRect
            // 
            resources.ApplyResources(this.tbPt2YRect, "tbPt2YRect");
            this.tbPt2YRect.Name = "tbPt2YRect";
            // 
            // lPt2YRect
            // 
            resources.ApplyResources(this.lPt2YRect, "lPt2YRect");
            this.lPt2YRect.Name = "lPt2YRect";
            // 
            // tbPt2XRect
            // 
            resources.ApplyResources(this.tbPt2XRect, "tbPt2XRect");
            this.tbPt2XRect.Name = "tbPt2XRect";
            // 
            // lPt2XRect
            // 
            resources.ApplyResources(this.lPt2XRect, "lPt2XRect");
            this.lPt2XRect.Name = "lPt2XRect";
            // 
            // gbPt2DegMin
            // 
            resources.ApplyResources(this.gbPt2DegMin, "gbPt2DegMin");
            this.gbPt2DegMin.Controls.Add(this.tbPt2LMin1);
            this.gbPt2DegMin.Controls.Add(this.tbPt2BMin1);
            this.gbPt2DegMin.Controls.Add(this.lPt2LDegMin);
            this.gbPt2DegMin.Controls.Add(this.lPt2BDegMin);
            this.gbPt2DegMin.Name = "gbPt2DegMin";
            this.gbPt2DegMin.TabStop = false;
            this.gbPt2DegMin.Enter += new System.EventHandler(this.gbPt2DegMin_Enter);
            // 
            // tbPt2LMin1
            // 
            resources.ApplyResources(this.tbPt2LMin1, "tbPt2LMin1");
            this.tbPt2LMin1.Name = "tbPt2LMin1";
            // 
            // tbPt2BMin1
            // 
            resources.ApplyResources(this.tbPt2BMin1, "tbPt2BMin1");
            this.tbPt2BMin1.Name = "tbPt2BMin1";
            // 
            // lPt2LDegMin
            // 
            resources.ApplyResources(this.lPt2LDegMin, "lPt2LDegMin");
            this.lPt2LDegMin.Name = "lPt2LDegMin";
            // 
            // lPt2BDegMin
            // 
            resources.ApplyResources(this.lPt2BDegMin, "lPt2BDegMin");
            this.lPt2BDegMin.Name = "lPt2BDegMin";
            // 
            // gbPt2Rect42
            // 
            resources.ApplyResources(this.gbPt2Rect42, "gbPt2Rect42");
            this.gbPt2Rect42.Controls.Add(this.tbPt2YRect42);
            this.gbPt2Rect42.Controls.Add(this.lPt2YRect42);
            this.gbPt2Rect42.Controls.Add(this.tbPt2XRect42);
            this.gbPt2Rect42.Controls.Add(this.lPt2XRect42);
            this.gbPt2Rect42.Name = "gbPt2Rect42";
            this.gbPt2Rect42.TabStop = false;
            // 
            // tbPt2YRect42
            // 
            resources.ApplyResources(this.tbPt2YRect42, "tbPt2YRect42");
            this.tbPt2YRect42.Name = "tbPt2YRect42";
            // 
            // lPt2YRect42
            // 
            resources.ApplyResources(this.lPt2YRect42, "lPt2YRect42");
            this.lPt2YRect42.Name = "lPt2YRect42";
            // 
            // tbPt2XRect42
            // 
            resources.ApplyResources(this.tbPt2XRect42, "tbPt2XRect42");
            this.tbPt2XRect42.Name = "tbPt2XRect42";
            // 
            // lPt2XRect42
            // 
            resources.ApplyResources(this.lPt2XRect42, "lPt2XRect42");
            this.lPt2XRect42.Name = "lPt2XRect42";
            // 
            // gbPoint1
            // 
            resources.ApplyResources(this.gbPoint1, "gbPoint1");
            this.gbPoint1.Controls.Add(this.tbPt1Height);
            this.gbPoint1.Controls.Add(this.lPt1Height);
            this.gbPoint1.Controls.Add(this.gbPt1DegMinSec);
            this.gbPoint1.Controls.Add(this.gbPt1Rad);
            this.gbPoint1.Controls.Add(this.gbPt1Rect);
            this.gbPoint1.Controls.Add(this.gbPt1DegMin);
            this.gbPoint1.Controls.Add(this.gbPt1Rect42);
            this.gbPoint1.Name = "gbPoint1";
            this.gbPoint1.TabStop = false;
            // 
            // tbPt1Height
            // 
            resources.ApplyResources(this.tbPt1Height, "tbPt1Height");
            this.tbPt1Height.BackColor = System.Drawing.SystemColors.Menu;
            this.tbPt1Height.Name = "tbPt1Height";
            // 
            // lPt1Height
            // 
            resources.ApplyResources(this.lPt1Height, "lPt1Height");
            this.lPt1Height.Name = "lPt1Height";
            // 
            // gbPt1DegMinSec
            // 
            resources.ApplyResources(this.gbPt1DegMinSec, "gbPt1DegMinSec");
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1LSec);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1BSec);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Min4);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Min3);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1LMin2);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1BMin2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Sec2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Sec1);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Deg4);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Deg3);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1LDeg2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1LDegMinSec);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1BDeg2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1BDegMinSec);
            this.gbPt1DegMinSec.Name = "gbPt1DegMinSec";
            this.gbPt1DegMinSec.TabStop = false;
            // 
            // tbPt1LSec
            // 
            resources.ApplyResources(this.tbPt1LSec, "tbPt1LSec");
            this.tbPt1LSec.Name = "tbPt1LSec";
            // 
            // tbPt1BSec
            // 
            resources.ApplyResources(this.tbPt1BSec, "tbPt1BSec");
            this.tbPt1BSec.Name = "tbPt1BSec";
            // 
            // lPt1Min4
            // 
            resources.ApplyResources(this.lPt1Min4, "lPt1Min4");
            this.lPt1Min4.Name = "lPt1Min4";
            // 
            // lPt1Min3
            // 
            resources.ApplyResources(this.lPt1Min3, "lPt1Min3");
            this.lPt1Min3.Name = "lPt1Min3";
            // 
            // tbPt1LMin2
            // 
            resources.ApplyResources(this.tbPt1LMin2, "tbPt1LMin2");
            this.tbPt1LMin2.Name = "tbPt1LMin2";
            // 
            // tbPt1BMin2
            // 
            resources.ApplyResources(this.tbPt1BMin2, "tbPt1BMin2");
            this.tbPt1BMin2.Name = "tbPt1BMin2";
            this.tbPt1BMin2.TextChanged += new System.EventHandler(this.tbPt1BMin2_TextChanged);
            // 
            // lPt1Sec2
            // 
            resources.ApplyResources(this.lPt1Sec2, "lPt1Sec2");
            this.lPt1Sec2.Name = "lPt1Sec2";
            // 
            // lPt1Sec1
            // 
            resources.ApplyResources(this.lPt1Sec1, "lPt1Sec1");
            this.lPt1Sec1.Name = "lPt1Sec1";
            // 
            // lPt1Deg4
            // 
            resources.ApplyResources(this.lPt1Deg4, "lPt1Deg4");
            this.lPt1Deg4.Name = "lPt1Deg4";
            // 
            // lPt1Deg3
            // 
            resources.ApplyResources(this.lPt1Deg3, "lPt1Deg3");
            this.lPt1Deg3.Name = "lPt1Deg3";
            // 
            // tbPt1LDeg2
            // 
            resources.ApplyResources(this.tbPt1LDeg2, "tbPt1LDeg2");
            this.tbPt1LDeg2.Name = "tbPt1LDeg2";
            // 
            // lPt1LDegMinSec
            // 
            resources.ApplyResources(this.lPt1LDegMinSec, "lPt1LDegMinSec");
            this.lPt1LDegMinSec.Name = "lPt1LDegMinSec";
            // 
            // tbPt1BDeg2
            // 
            resources.ApplyResources(this.tbPt1BDeg2, "tbPt1BDeg2");
            this.tbPt1BDeg2.Name = "tbPt1BDeg2";
            // 
            // lPt1BDegMinSec
            // 
            resources.ApplyResources(this.lPt1BDegMinSec, "lPt1BDegMinSec");
            this.lPt1BDegMinSec.Name = "lPt1BDegMinSec";
            // 
            // gbPt1Rad
            // 
            resources.ApplyResources(this.gbPt1Rad, "gbPt1Rad");
            this.gbPt1Rad.Controls.Add(this.tbPt1LRad);
            this.gbPt1Rad.Controls.Add(this.lPt1LRad);
            this.gbPt1Rad.Controls.Add(this.tbPt1BRad);
            this.gbPt1Rad.Controls.Add(this.lPt1BRad);
            this.gbPt1Rad.Name = "gbPt1Rad";
            this.gbPt1Rad.TabStop = false;
            // 
            // tbPt1LRad
            // 
            resources.ApplyResources(this.tbPt1LRad, "tbPt1LRad");
            this.tbPt1LRad.Name = "tbPt1LRad";
            // 
            // lPt1LRad
            // 
            resources.ApplyResources(this.lPt1LRad, "lPt1LRad");
            this.lPt1LRad.Name = "lPt1LRad";
            // 
            // tbPt1BRad
            // 
            resources.ApplyResources(this.tbPt1BRad, "tbPt1BRad");
            this.tbPt1BRad.Name = "tbPt1BRad";
            // 
            // lPt1BRad
            // 
            resources.ApplyResources(this.lPt1BRad, "lPt1BRad");
            this.lPt1BRad.Name = "lPt1BRad";
            // 
            // gbPt1Rect
            // 
            resources.ApplyResources(this.gbPt1Rect, "gbPt1Rect");
            this.gbPt1Rect.Controls.Add(this.tbPt1YRect);
            this.gbPt1Rect.Controls.Add(this.lPt1YRect);
            this.gbPt1Rect.Controls.Add(this.tbPt1XRect);
            this.gbPt1Rect.Controls.Add(this.lPt1XRect);
            this.gbPt1Rect.Name = "gbPt1Rect";
            this.gbPt1Rect.TabStop = false;
            // 
            // tbPt1YRect
            // 
            resources.ApplyResources(this.tbPt1YRect, "tbPt1YRect");
            this.tbPt1YRect.Name = "tbPt1YRect";
            // 
            // lPt1YRect
            // 
            resources.ApplyResources(this.lPt1YRect, "lPt1YRect");
            this.lPt1YRect.Name = "lPt1YRect";
            // 
            // tbPt1XRect
            // 
            resources.ApplyResources(this.tbPt1XRect, "tbPt1XRect");
            this.tbPt1XRect.Name = "tbPt1XRect";
            // 
            // lPt1XRect
            // 
            resources.ApplyResources(this.lPt1XRect, "lPt1XRect");
            this.lPt1XRect.Name = "lPt1XRect";
            // 
            // gbPt1DegMin
            // 
            resources.ApplyResources(this.gbPt1DegMin, "gbPt1DegMin");
            this.gbPt1DegMin.Controls.Add(this.tbPt1LMin1);
            this.gbPt1DegMin.Controls.Add(this.tbPt1BMin1);
            this.gbPt1DegMin.Controls.Add(this.lPt1LDegMin);
            this.gbPt1DegMin.Controls.Add(this.lPt1BDegMin);
            this.gbPt1DegMin.Name = "gbPt1DegMin";
            this.gbPt1DegMin.TabStop = false;
            // 
            // tbPt1LMin1
            // 
            resources.ApplyResources(this.tbPt1LMin1, "tbPt1LMin1");
            this.tbPt1LMin1.Name = "tbPt1LMin1";
            // 
            // tbPt1BMin1
            // 
            resources.ApplyResources(this.tbPt1BMin1, "tbPt1BMin1");
            this.tbPt1BMin1.Name = "tbPt1BMin1";
            // 
            // lPt1LDegMin
            // 
            resources.ApplyResources(this.lPt1LDegMin, "lPt1LDegMin");
            this.lPt1LDegMin.Name = "lPt1LDegMin";
            // 
            // lPt1BDegMin
            // 
            resources.ApplyResources(this.lPt1BDegMin, "lPt1BDegMin");
            this.lPt1BDegMin.Name = "lPt1BDegMin";
            // 
            // gbPt1Rect42
            // 
            resources.ApplyResources(this.gbPt1Rect42, "gbPt1Rect42");
            this.gbPt1Rect42.Controls.Add(this.tbPt1YRect42);
            this.gbPt1Rect42.Controls.Add(this.lPt1YRect42);
            this.gbPt1Rect42.Controls.Add(this.tbPt1XRect42);
            this.gbPt1Rect42.Controls.Add(this.lPt1XRect42);
            this.gbPt1Rect42.Name = "gbPt1Rect42";
            this.gbPt1Rect42.TabStop = false;
            this.gbPt1Rect42.Enter += new System.EventHandler(this.gbPt1Rect42_Enter);
            // 
            // tbPt1YRect42
            // 
            resources.ApplyResources(this.tbPt1YRect42, "tbPt1YRect42");
            this.tbPt1YRect42.Name = "tbPt1YRect42";
            // 
            // lPt1YRect42
            // 
            resources.ApplyResources(this.lPt1YRect42, "lPt1YRect42");
            this.lPt1YRect42.Name = "lPt1YRect42";
            // 
            // tbPt1XRect42
            // 
            resources.ApplyResources(this.tbPt1XRect42, "tbPt1XRect42");
            this.tbPt1XRect42.Name = "tbPt1XRect42";
            // 
            // lPt1XRect42
            // 
            resources.ApplyResources(this.lPt1XRect42, "lPt1XRect42");
            this.lPt1XRect42.Name = "lPt1XRect42";
            // 
            // tbCoeffReceiverOpponent
            // 
            resources.ApplyResources(this.tbCoeffReceiverOpponent, "tbCoeffReceiverOpponent");
            this.tbCoeffReceiverOpponent.Name = "tbCoeffReceiverOpponent";
            // 
            // lCoeffReceivertOpponent
            // 
            resources.ApplyResources(this.lCoeffReceivertOpponent, "lCoeffReceivertOpponent");
            this.lCoeffReceivertOpponent.Name = "lCoeffReceivertOpponent";
            // 
            // lCapacity
            // 
            resources.ApplyResources(this.lCapacity, "lCapacity");
            this.lCapacity.Name = "lCapacity";
            // 
            // cbCap1
            // 
            resources.ApplyResources(this.cbCap1, "cbCap1");
            this.cbCap1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCap1.FormattingEnabled = true;
            this.cbCap1.Items.AddRange(new object[] {
            resources.GetString("cbCap1.Items"),
            resources.GetString("cbCap1.Items1"),
            resources.GetString("cbCap1.Items2"),
            resources.GetString("cbCap1.Items3")});
            this.cbCap1.Name = "cbCap1";
            // 
            // cbTypeCommOpponent
            // 
            resources.ApplyResources(this.cbTypeCommOpponent, "cbTypeCommOpponent");
            this.cbTypeCommOpponent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeCommOpponent.FormattingEnabled = true;
            this.cbTypeCommOpponent.Items.AddRange(new object[] {
            resources.GetString("cbTypeCommOpponent.Items"),
            resources.GetString("cbTypeCommOpponent.Items1"),
            resources.GetString("cbTypeCommOpponent.Items2"),
            resources.GetString("cbTypeCommOpponent.Items3"),
            resources.GetString("cbTypeCommOpponent.Items4"),
            resources.GetString("cbTypeCommOpponent.Items5"),
            resources.GetString("cbTypeCommOpponent.Items6")});
            this.cbTypeCommOpponent.Name = "cbTypeCommOpponent";
            // 
            // lTypeCommOpponent
            // 
            resources.ApplyResources(this.lTypeCommOpponent, "lTypeCommOpponent");
            this.lTypeCommOpponent.Name = "lTypeCommOpponent";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // rbPoint2
            // 
            resources.ApplyResources(this.rbPoint2, "rbPoint2");
            this.rbPoint2.Name = "rbPoint2";
            this.rbPoint2.UseVisualStyleBackColor = true;
            // 
            // rbPoint1
            // 
            resources.ApplyResources(this.rbPoint1, "rbPoint1");
            this.rbPoint1.Checked = true;
            this.rbPoint1.Name = "rbPoint1";
            this.rbPoint1.TabStop = true;
            this.rbPoint1.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // chbXY
            // 
            resources.ApplyResources(this.chbXY, "chbXY");
            this.chbXY.Name = "chbXY";
            this.chbXY.UseVisualStyleBackColor = true;
            // 
            // lChooseSC
            // 
            resources.ApplyResources(this.lChooseSC, "lChooseSC");
            this.lChooseSC.Name = "lChooseSC";
            // 
            // cbOwnObject
            // 
            resources.ApplyResources(this.cbOwnObject, "cbOwnObject");
            this.cbOwnObject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOwnObject.FormattingEnabled = true;
            this.cbOwnObject.Name = "cbOwnObject";
            // 
            // lCenterLSR
            // 
            resources.ApplyResources(this.lCenterLSR, "lCenterLSR");
            this.lCenterLSR.Name = "lCenterLSR";
            // 
            // cbChooseSC
            // 
            resources.ApplyResources(this.cbChooseSC, "cbChooseSC");
            this.cbChooseSC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChooseSC.DropDownWidth = 150;
            this.cbChooseSC.FormattingEnabled = true;
            this.cbChooseSC.Items.AddRange(new object[] {
            resources.GetString("cbChooseSC.Items"),
            resources.GetString("cbChooseSC.Items1"),
            resources.GetString("cbChooseSC.Items2"),
            resources.GetString("cbChooseSC.Items3"),
            resources.GetString("cbChooseSC.Items4")});
            this.cbChooseSC.Name = "cbChooseSC";
            this.cbChooseSC.SelectedIndexChanged += new System.EventHandler(this.cbChooseSC_SelectedIndexChanged);
            // 
            // cbHeightOwnObject
            // 
            resources.ApplyResources(this.cbHeightOwnObject, "cbHeightOwnObject");
            this.cbHeightOwnObject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbHeightOwnObject.FormattingEnabled = true;
            this.cbHeightOwnObject.Items.AddRange(new object[] {
            resources.GetString("cbHeightOwnObject.Items"),
            resources.GetString("cbHeightOwnObject.Items1"),
            resources.GetString("cbHeightOwnObject.Items2")});
            this.cbHeightOwnObject.Name = "cbHeightOwnObject";
            // 
            // grbCoeffSupOpponent
            // 
            resources.ApplyResources(this.grbCoeffSupOpponent, "grbCoeffSupOpponent");
            this.grbCoeffSupOpponent.Controls.Add(this.cbCoeffSupOpponent);
            this.grbCoeffSupOpponent.Name = "grbCoeffSupOpponent";
            this.grbCoeffSupOpponent.TabStop = false;
            // 
            // cbCoeffSupOpponent
            // 
            resources.ApplyResources(this.cbCoeffSupOpponent, "cbCoeffSupOpponent");
            this.cbCoeffSupOpponent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCoeffSupOpponent.FormattingEnabled = true;
            this.cbCoeffSupOpponent.Items.AddRange(new object[] {
            resources.GetString("cbCoeffSupOpponent.Items"),
            resources.GetString("cbCoeffSupOpponent.Items1")});
            this.cbCoeffSupOpponent.Name = "cbCoeffSupOpponent";
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            // 
            // chbXY1
            // 
            resources.ApplyResources(this.chbXY1, "chbXY1");
            this.chbXY1.Name = "chbXY1";
            this.chbXY1.UseVisualStyleBackColor = true;
            // 
            // cbCommChooseSC
            // 
            resources.ApplyResources(this.cbCommChooseSC, "cbCommChooseSC");
            this.cbCommChooseSC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCommChooseSC.DropDownWidth = 150;
            this.cbCommChooseSC.FormattingEnabled = true;
            this.cbCommChooseSC.Items.AddRange(new object[] {
            resources.GetString("cbCommChooseSC.Items"),
            resources.GetString("cbCommChooseSC.Items1"),
            resources.GetString("cbCommChooseSC.Items2"),
            resources.GetString("cbCommChooseSC.Items3"),
            resources.GetString("cbCommChooseSC.Items4")});
            this.cbCommChooseSC.Name = "cbCommChooseSC";
            this.cbCommChooseSC.SelectedIndexChanged += new System.EventHandler(this.cbCommChooseSC_SelectedIndexChanged);
            // 
            // lCommChooseSC
            // 
            resources.ApplyResources(this.lCommChooseSC, "lCommChooseSC");
            this.lCommChooseSC.Name = "lCommChooseSC";
            // 
            // tbRadiusZone
            // 
            resources.ApplyResources(this.tbRadiusZone, "tbRadiusZone");
            this.tbRadiusZone.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbRadiusZone.Name = "tbRadiusZone";
            this.tbRadiusZone.TextChanged += new System.EventHandler(this.tbRadiusZone_TextChanged);
            // 
            // lRadiusZone
            // 
            resources.ApplyResources(this.lRadiusZone, "lRadiusZone");
            this.lRadiusZone.Name = "lRadiusZone";
            // 
            // tbResult
            // 
            resources.ApplyResources(this.tbResult, "tbResult");
            this.tbResult.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbResult.Name = "tbResult";
            // 
            // tbMaxDist
            // 
            resources.ApplyResources(this.tbMaxDist, "tbMaxDist");
            this.tbMaxDist.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbMaxDist.Name = "tbMaxDist";
            this.tbMaxDist.ReadOnly = true;
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // tbHindSignal
            // 
            resources.ApplyResources(this.tbHindSignal, "tbHindSignal");
            this.tbHindSignal.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbHindSignal.Name = "tbHindSignal";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // bAccept
            // 
            resources.ApplyResources(this.bAccept, "bAccept");
            this.bAccept.Name = "bAccept";
            this.bAccept.UseVisualStyleBackColor = true;
            this.bAccept.Click += new System.EventHandler(this.bAccept_Click_1);
            // 
            // bClear
            // 
            resources.ApplyResources(this.bClear, "bClear");
            this.bClear.Name = "bClear";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            resources.ApplyResources(this.button2, "button2");
            this.button2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            resources.ApplyResources(this.button3, "button3");
            this.button3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button3.Name = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // grbDetail
            // 
            resources.ApplyResources(this.grbDetail, "grbDetail");
            this.grbDetail.Controls.Add(this.tbCorrectHeightOpponent);
            this.grbDetail.Controls.Add(this.label10);
            this.grbDetail.Controls.Add(this.tbGamma);
            this.grbDetail.Controls.Add(this.label9);
            this.grbDetail.Controls.Add(this.tbResultHeightOpponent);
            this.grbDetail.Controls.Add(this.label5);
            this.grbDetail.Controls.Add(this.tbMinHeight);
            this.grbDetail.Controls.Add(this.label6);
            this.grbDetail.Controls.Add(this.tbMiddleHeight);
            this.grbDetail.Controls.Add(this.label7);
            this.grbDetail.Controls.Add(this.tbMaxDistance);
            this.grbDetail.Controls.Add(this.label8);
            this.grbDetail.Controls.Add(this.tbCorrectHeightOwn);
            this.grbDetail.Controls.Add(this.label4);
            this.grbDetail.Controls.Add(this.tbCoeffHE);
            this.grbDetail.Controls.Add(this.label3);
            this.grbDetail.Controls.Add(this.tbCoeffQ);
            this.grbDetail.Controls.Add(this.label2);
            this.grbDetail.Controls.Add(this.tbResultHeightOwn);
            this.grbDetail.Controls.Add(this.label1);
            this.grbDetail.Name = "grbDetail";
            this.grbDetail.TabStop = false;
            // 
            // tbCorrectHeightOpponent
            // 
            resources.ApplyResources(this.tbCorrectHeightOpponent, "tbCorrectHeightOpponent");
            this.tbCorrectHeightOpponent.Name = "tbCorrectHeightOpponent";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // tbGamma
            // 
            resources.ApplyResources(this.tbGamma, "tbGamma");
            this.tbGamma.Name = "tbGamma";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // tbResultHeightOpponent
            // 
            resources.ApplyResources(this.tbResultHeightOpponent, "tbResultHeightOpponent");
            this.tbResultHeightOpponent.Name = "tbResultHeightOpponent";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // tbMinHeight
            // 
            resources.ApplyResources(this.tbMinHeight, "tbMinHeight");
            this.tbMinHeight.Name = "tbMinHeight";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // tbMiddleHeight
            // 
            resources.ApplyResources(this.tbMiddleHeight, "tbMiddleHeight");
            this.tbMiddleHeight.Name = "tbMiddleHeight";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // tbMaxDistance
            // 
            resources.ApplyResources(this.tbMaxDistance, "tbMaxDistance");
            this.tbMaxDistance.Name = "tbMaxDistance";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // tbCorrectHeightOwn
            // 
            resources.ApplyResources(this.tbCorrectHeightOwn, "tbCorrectHeightOwn");
            this.tbCorrectHeightOwn.Name = "tbCorrectHeightOwn";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // tbCoeffHE
            // 
            resources.ApplyResources(this.tbCoeffHE, "tbCoeffHE");
            this.tbCoeffHE.Name = "tbCoeffHE";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // tbCoeffQ
            // 
            resources.ApplyResources(this.tbCoeffQ, "tbCoeffQ");
            this.tbCoeffQ.Name = "tbCoeffQ";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // tbResultHeightOwn
            // 
            resources.ApplyResources(this.tbResultHeightOwn, "tbResultHeightOwn");
            this.tbResultHeightOwn.Name = "tbResultHeightOwn";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // tbMidH
            // 
            resources.ApplyResources(this.tbMidH, "tbMidH");
            this.tbMidH.Name = "tbMidH";
            // 
            // label37
            // 
            resources.ApplyResources(this.label37, "label37");
            this.label37.Name = "label37";
            // 
            // tbHindSignal2
            // 
            resources.ApplyResources(this.tbHindSignal2, "tbHindSignal2");
            this.tbHindSignal2.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbHindSignal2.Name = "tbHindSignal2";
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // Form2
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label19);
            this.Controls.Add(this.tbHindSignal2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.lTypeCommOpponent);
            this.Controls.Add(this.cbTypeCommOpponent);
            this.Controls.Add(this.rbPoint2);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lCapacity);
            this.Controls.Add(this.tbMidH);
            this.Controls.Add(this.cbCap1);
            this.Controls.Add(this.grbCoeffSupOpponent);
            this.Controls.Add(this.rbPoint1);
            this.Controls.Add(this.cbHeightOwnObject);
            this.Controls.Add(this.grbDetail);
            this.Controls.Add(this.lCenterLSR);
            this.Controls.Add(this.lCoeffReceivertOpponent);
            this.Controls.Add(this.tbCoeffReceiverOpponent);
            this.Controls.Add(this.chbXY1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.cbOwnObject);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.chbXY);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lCommChooseSC);
            this.Controls.Add(this.cbCommChooseSC);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.bAccept);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.tbHindSignal);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.tbMaxDist);
            this.Controls.Add(this.lChooseSC);
            this.Controls.Add(this.tbResult);
            this.Controls.Add(this.lRadiusZone);
            this.Controls.Add(this.cbChooseSC);
            this.Controls.Add(this.tbRadiusZone);
            this.Controls.Add(this.tcParam);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form2";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.Form2_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form2_FormClosing);
            this.Load += new System.EventHandler(this.Form2_Load);
            this.tcParam.ResumeLayout(false);
            this.tpOwnObject.ResumeLayout(false);
            this.tpOwnObject.PerformLayout();
            this.pCoordPoint.ResumeLayout(false);
            this.pCoordPoint.PerformLayout();
            this.gbOwnRect.ResumeLayout(false);
            this.gbOwnRect.PerformLayout();
            this.gbOwnDegMin.ResumeLayout(false);
            this.gbOwnDegMin.PerformLayout();
            this.gbOwnDegMinSec.ResumeLayout(false);
            this.gbOwnDegMinSec.PerformLayout();
            this.gbOwnRect42.ResumeLayout(false);
            this.gbOwnRect42.PerformLayout();
            this.gbOwnRad.ResumeLayout(false);
            this.gbOwnRad.PerformLayout();
            this.tpOpponentObject.ResumeLayout(false);
            this.tpOpponentObject.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCountOpponent)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.gbPoint2.ResumeLayout(false);
            this.gbPoint2.PerformLayout();
            this.gbPt2DegMinSec.ResumeLayout(false);
            this.gbPt2DegMinSec.PerformLayout();
            this.gbPt2Rad.ResumeLayout(false);
            this.gbPt2Rad.PerformLayout();
            this.gbPt2Rect.ResumeLayout(false);
            this.gbPt2Rect.PerformLayout();
            this.gbPt2DegMin.ResumeLayout(false);
            this.gbPt2DegMin.PerformLayout();
            this.gbPt2Rect42.ResumeLayout(false);
            this.gbPt2Rect42.PerformLayout();
            this.gbPoint1.ResumeLayout(false);
            this.gbPoint1.PerformLayout();
            this.gbPt1DegMinSec.ResumeLayout(false);
            this.gbPt1DegMinSec.PerformLayout();
            this.gbPt1Rad.ResumeLayout(false);
            this.gbPt1Rad.PerformLayout();
            this.gbPt1Rect.ResumeLayout(false);
            this.gbPt1Rect.PerformLayout();
            this.gbPt1DegMin.ResumeLayout(false);
            this.gbPt1DegMin.PerformLayout();
            this.gbPt1Rect42.ResumeLayout(false);
            this.gbPt1Rect42.PerformLayout();
            this.grbCoeffSupOpponent.ResumeLayout(false);
            this.grbDetail.ResumeLayout(false);
            this.grbDetail.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TabControl tcParam;
        private System.Windows.Forms.TabPage tpOwnObject;
        private System.Windows.Forms.Label lPowerOwn;
        private System.Windows.Forms.TextBox tbCoeffOwn;
        private System.Windows.Forms.Label lCoeffOwn;
        private System.Windows.Forms.TextBox tbPowerOwn;
        private System.Windows.Forms.ComboBox cbSurface;
        private System.Windows.Forms.Label lSurface;
        private System.Windows.Forms.ComboBox cbWidthHindrance;
        private System.Windows.Forms.Label lWidthHindrance;
        private System.Windows.Forms.Label lChooseSC;
        private System.Windows.Forms.Panel pCoordPoint;
        private System.Windows.Forms.ComboBox cbOwnObject;
        private System.Windows.Forms.Label lCenterLSR;
        private System.Windows.Forms.Label lOwnHeight;
        private System.Windows.Forms.GroupBox gbOwnDegMinSec;
        public System.Windows.Forms.TextBox tbLSec;
        public System.Windows.Forms.TextBox tbBSec;
        private System.Windows.Forms.Label lMin4;
        private System.Windows.Forms.Label lMin3;
        public System.Windows.Forms.TextBox tbLMin2;
        public System.Windows.Forms.TextBox tbBMin2;
        private System.Windows.Forms.Label lSec2;
        private System.Windows.Forms.Label lSec1;
        private System.Windows.Forms.Label lDeg4;
        private System.Windows.Forms.Label lDeg3;
        public System.Windows.Forms.TextBox tbLDeg2;
        private System.Windows.Forms.Label lLDegMinSec;
        public System.Windows.Forms.TextBox tbBDeg2;
        private System.Windows.Forms.Label lBDegMinSec;
        private System.Windows.Forms.GroupBox gbOwnRect42;
        public System.Windows.Forms.TextBox tbYRect42;
        private System.Windows.Forms.Label lYRect42;
        public System.Windows.Forms.TextBox tbXRect42;
        private System.Windows.Forms.Label lXRect42;
        private System.Windows.Forms.GroupBox gbOwnDegMin;
        public System.Windows.Forms.TextBox tbLMin1;
        public System.Windows.Forms.TextBox tbBMin1;
        private System.Windows.Forms.Label lLDegMin;
        private System.Windows.Forms.Label lBDegMin;
        private System.Windows.Forms.GroupBox gbOwnRad;
        public System.Windows.Forms.TextBox tbLRad;
        private System.Windows.Forms.Label lLRad;
        public System.Windows.Forms.TextBox tbBRad;
        private System.Windows.Forms.Label lBRad;
        private System.Windows.Forms.ComboBox cbChooseSC;
        private System.Windows.Forms.ComboBox cbHeightOwnObject;
        private System.Windows.Forms.Label lHeightOwnObject;
        private System.Windows.Forms.TabPage tpOpponentObject;
        private System.Windows.Forms.GroupBox grbCoeffSupOpponent;
        private System.Windows.Forms.TextBox tbCoeffSupOpponent;
        private System.Windows.Forms.ComboBox cbCoeffSupOpponent;
        private System.Windows.Forms.ComboBox cbTypeCommOpponent;
        private System.Windows.Forms.Label lTypeCommOpponent;
        private System.Windows.Forms.ComboBox cbPolarOpponent;
        private System.Windows.Forms.Label lPolarOpponent;
        private System.Windows.Forms.TextBox tbRangeComm;
        private System.Windows.Forms.Label lRangeComm;
        private System.Windows.Forms.TextBox tbHeightReceiverOpponent;
        private System.Windows.Forms.Label lHeightReceiverOpponent;
        private System.Windows.Forms.TextBox tbHeightTransmitOpponent;
        private System.Windows.Forms.Label lHeightTransmitOpponent;
        private System.Windows.Forms.TextBox tbWidthSignal;
        private System.Windows.Forms.Label lWidthSignal;
        private System.Windows.Forms.TextBox tbFreq;
        private System.Windows.Forms.Label lFreq;
        private System.Windows.Forms.TextBox tbCoeffReceiverOpponent;
        private System.Windows.Forms.Label lCoeffReceivertOpponent;
        private System.Windows.Forms.TextBox tbCoeffTransmitOpponent;
        private System.Windows.Forms.Label lCoeffTransmitOpponent;
        private System.Windows.Forms.TextBox tbPowerOpponent;
        private System.Windows.Forms.Label lPowerOpponent;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ComboBox cbCommChooseSC;
        private System.Windows.Forms.Label lCommChooseSC;
        private System.Windows.Forms.NumericUpDown nudCountOpponent;
        private System.Windows.Forms.GroupBox gbPoint2;
        private System.Windows.Forms.Label lPt2Height;
        private System.Windows.Forms.GroupBox gbPt2DegMinSec;
        public System.Windows.Forms.TextBox tbPt2LSec;
        public System.Windows.Forms.TextBox tbPt2BSec;
        private System.Windows.Forms.Label lPt2Min4;
        private System.Windows.Forms.Label lPt2Min3;
        public System.Windows.Forms.TextBox tbPt2LMin2;
        public System.Windows.Forms.TextBox tbPt2BMin2;
        private System.Windows.Forms.Label lPt2Sec2;
        private System.Windows.Forms.Label lPt2Sec1;
        private System.Windows.Forms.Label lPt2Deg4;
        private System.Windows.Forms.Label lPt2Deg3;
        public System.Windows.Forms.TextBox tbPt2LDeg2;
        private System.Windows.Forms.Label lPt2LDegMinSec;
        public System.Windows.Forms.TextBox tbPt2BDeg2;
        private System.Windows.Forms.Label lPt2BDegMinSec;
        private System.Windows.Forms.GroupBox gbPt2DegMin;
        public System.Windows.Forms.TextBox tbPt2LMin1;
        public System.Windows.Forms.TextBox tbPt2BMin1;
        private System.Windows.Forms.Label lPt2LDegMin;
        private System.Windows.Forms.Label lPt2BDegMin;
        private System.Windows.Forms.GroupBox gbPt2Rect42;
        public System.Windows.Forms.TextBox tbPt2YRect42;
        private System.Windows.Forms.Label lPt2YRect42;
        public System.Windows.Forms.TextBox tbPt2XRect42;
        private System.Windows.Forms.Label lPt2XRect42;
        private System.Windows.Forms.GroupBox gbPt2Rad;
        public System.Windows.Forms.TextBox tbPt2LRad;
        private System.Windows.Forms.Label lPt2LRad;
        public System.Windows.Forms.TextBox tbPt2BRad;
        private System.Windows.Forms.Label lPt2BRad;
        private System.Windows.Forms.GroupBox gbPt2Rect;
        public System.Windows.Forms.TextBox tbPt2YRect;
        private System.Windows.Forms.Label lPt2YRect;
        public System.Windows.Forms.TextBox tbPt2XRect;
        private System.Windows.Forms.Label lPt2XRect;
        private System.Windows.Forms.GroupBox gbPoint1;
        private System.Windows.Forms.Label lPt1Height;
        private System.Windows.Forms.GroupBox gbPt1DegMinSec;
        public System.Windows.Forms.TextBox tbPt1LSec;
        public System.Windows.Forms.TextBox tbPt1BSec;
        private System.Windows.Forms.Label lPt1Min4;
        private System.Windows.Forms.Label lPt1Min3;
        public System.Windows.Forms.TextBox tbPt1LMin2;
        public System.Windows.Forms.TextBox tbPt1BMin2;
        private System.Windows.Forms.Label lPt1Sec2;
        private System.Windows.Forms.Label lPt1Sec1;
        private System.Windows.Forms.Label lPt1Deg4;
        private System.Windows.Forms.Label lPt1Deg3;
        public System.Windows.Forms.TextBox tbPt1LDeg2;
        private System.Windows.Forms.Label lPt1LDegMinSec;
        public System.Windows.Forms.TextBox tbPt1BDeg2;
        private System.Windows.Forms.Label lPt1BDegMinSec;
        private System.Windows.Forms.GroupBox gbPt1DegMin;
        public System.Windows.Forms.TextBox tbPt1LMin1;
        public System.Windows.Forms.TextBox tbPt1BMin1;
        private System.Windows.Forms.Label lPt1LDegMin;
        private System.Windows.Forms.Label lPt1BDegMin;
        private System.Windows.Forms.GroupBox gbPt1Rect42;
        public System.Windows.Forms.TextBox tbPt1YRect42;
        private System.Windows.Forms.Label lPt1YRect42;
        public System.Windows.Forms.TextBox tbPt1XRect42;
        private System.Windows.Forms.Label lPt1XRect42;
        private System.Windows.Forms.GroupBox gbPt1Rad;
        public System.Windows.Forms.TextBox tbPt1LRad;
        private System.Windows.Forms.Label lPt1LRad;
        public System.Windows.Forms.TextBox tbPt1BRad;
        private System.Windows.Forms.Label lPt1BRad;
        private System.Windows.Forms.GroupBox gbPt1Rect;
        public System.Windows.Forms.TextBox tbPt1YRect;
        private System.Windows.Forms.Label lPt1YRect;
        public System.Windows.Forms.TextBox tbPt1XRect;
        private System.Windows.Forms.Label lPt1XRect;
        private System.Windows.Forms.Label lCountOpponent;
        private System.Windows.Forms.Label lRadiusZone;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button bAccept;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox gbOwnRect;
        public System.Windows.Forms.TextBox tbYRect;
        private System.Windows.Forms.Label lYRect;
        public System.Windows.Forms.TextBox tbXRect;
        private System.Windows.Forms.Label lXRect;
        private System.Windows.Forms.ComboBox cbCap1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox grbDetail;
        private System.Windows.Forms.TextBox tbCorrectHeightOpponent;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbGamma;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbResultHeightOpponent;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbMinHeight;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbMiddleHeight;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbMaxDistance;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbCorrectHeightOwn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbCoeffHE;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbCoeffQ;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbResultHeightOwn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chbXY;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox chbXY1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.RadioButton rbPoint2;
        public System.Windows.Forms.RadioButton rbPoint1;
        private System.Windows.Forms.TextBox tbHAnt;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lCapacity;
        public System.Windows.Forms.ComboBox cbCenterLSR;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.TextBox tbMidH;
        private System.Windows.Forms.Label label37;
        public System.Windows.Forms.TextBox tbOwnHeight;
        public System.Windows.Forms.TextBox tbHeightOwnObject;
        public System.Windows.Forms.TextBox tbPt2Height;
        public System.Windows.Forms.TextBox tbPt1Height;
        public System.Windows.Forms.TextBox tbRadiusZone;
        public System.Windows.Forms.TextBox tbResult;
        public System.Windows.Forms.TextBox tbMaxDist;
        public System.Windows.Forms.TextBox tbHindSignal;
        public System.Windows.Forms.TextBox tbHindSignal2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
    }
}