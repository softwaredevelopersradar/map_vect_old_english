﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;
using System.Reflection;
using System.Globalization;

namespace GrozaMap
{
    public partial class Form2 : Form
    {
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        private AxaxcMapScreen axaxcMapScreen;

        //private Point tpOwnCoordRect42;
        //private Point tpOwnCoordRect;
        //private Point tpPoint1Rect;
        //private Point tpPoint2Rect;


        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private double dchislo;
        private long ichislo;
        private double LAMBDA;

        // .....................................................................
        // Координаты СП,ys

        //private uint flCoordSP_comm; // =1-> Выбрали СП
        //private uint flCoordYS1_comm; // =1-> Выбрали YS1
        //private uint flCoordYS2_comm; // =1-> Выбрали YS2

        /*
                // Координаты СП на местности в м
                private double XSP_comm;
                private double YSP_comm;
                private double XYS1_comm;
                private double YYS1_comm;
                private double XYS2_comm;
                private double YYS2_comm;

                // DATUM
                private double dXdat_comm;
                private double dYdat_comm;
                private double dZdat_comm;

                private double dLat_comm;
                private double dLong_comm;

                // Эллипсоид Красовского, град
                private double LatKrG_comm;
                private double LongKrG_comm;
                // Эллипсоид Красовского, rad
                private double LatKrR_comm;
                private double LongKrR_comm;
                // Эллипсоид Красовского, град,мин,сек
                private int Lat_Grad_comm;
                private int Lat_Min_comm;
                private double Lat_Sec_comm;
                private int Long_Grad_comm;
                private int Long_Min_comm;
                private double Long_Sec_comm;
                // Гаусс-крюгер(СК42) м
                private double XSP42_comm;
                private double YSP42_comm;

                // Эллипсоид Красовского, град
                private double LatKrG_YS1_comm;
                private double LongKrG_YS1_comm;
                // Эллипсоид Красовского, rad
                private double LatKrR_YS1_comm;
                private double LongKrR_YS1_comm;
                // Эллипсоид Красовского, град,мин,сек
                private int Lat_Grad_YS1_comm;
                private int Lat_Min_YS1_comm;
                private double Lat_Sec_YS1_comm;
                private int Long_Grad_YS1_comm;
                private int Long_Min_YS1_comm;
                private double Long_Sec_YS1_comm;
                // Гаусс-крюгер(СК42) м
                private double XYS142_comm;
                private double YYS142_comm;

                // Эллипсоид Красовского, град
                private double LatKrG_YS2_comm;
                private double LongKrG_YS2_comm;
                // Эллипсоид Красовского, rad
                private double LatKrR_YS2_comm;
                private double LongKrR_YS2_comm;
                // Эллипсоид Красовского, град,мин,сек
                private int Lat_Grad_YS2_comm;
                private int Lat_Min_YS2_comm;
                private double Lat_Sec_YS2_comm;
                private int Long_Grad_YS2_comm;
                private int Long_Min_YS2_comm;
                private double Long_Sec_YS2_comm;
                // Гаусс-крюгер(СК42) м
                private double XYS242_comm;
                private double YYS242_comm;
        */
        // ......................................................................
        // Основные параметры

        private double OwnHeight_comm;
        private double Point1Height_comm;
        private double Point2Height_comm;
        //private double HeightOwnObject_comm;
        private double PowerOwn_comm;
        private double CoeffOwn_comm;
        //private double RadiusZone_comm;
        //private double MaxDist_comm;

        private int i_HeightOwnObject_comm;
        private int i_Cap1_comm;
        private int i_WidthHindrance_comm;
        private int i_Surface_comm;
        private double Cap1_comm;
        private double WidthHindrance_comm;
        //private double Surface_comm;

        // Высота средства подавления
        private double HeightAntennOwn_comm;
        private double HeightTotalOwn_comm;

        // Для подавляемой линии
        private double Freq_comm;
        private double PowerOpponent_comm;
        private double CoeffTransmitOpponent_comm;
        private double CoeffReceiverOpponent_comm;
        private double RangeComm_comm;
        private double WidthSignal_comm;
        private double HeightTransmitOpponent_comm;
        private double HeightReceiverOpponent_comm;
        private double CoeffSupOpponent_comm;
        private int i_PolarOpponent_comm;
        private int i_CoeffSupOpponent_comm;
        private int i_TypeCommOpponent_comm;

        // ......................................................................
        // Зона

        private double dCoeffQ_comm;
        private double dCoeffHE_comm;
        private int iCorrectHeightOwn_comm;
        private int iResultHeightOwn_comm;
        private int iMiddleHeight_comm;
        private int iMinHeight_comm;
        private int iCorrectHeightOpponent_comm;
        private int iResultHeightOpponent_comm;
        private long iMaxDistance_comm;
        private double dGamma_comm;
        //private long liRadiusZone_comm;

        // ......................................................................
        private int iDistJammerComm1; // расстояние от УС1 до средства подаления
        private int iDistJammerComm2; // расстояние от УС2 до средства подаления
        private int iDistBetweenComm;
        private bool blResultSupress;
        private bool blResultSupress2;



        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 

        ComponentResourceManager resources = new ComponentResourceManager(typeof(Form2));
        private int NumberOfLanguage;

        public Form2(ref AxaxcMapScreen axaxcMapScreen1)
        {
            InitializeComponent();


            axaxcMapScreen = axaxcMapScreen1;

            dchislo = 0;
            ichislo = 0;

            LAMBDA = 300000;

            //flCoordSP_comm = 0; // =1-> Выбрали СП
            //flCoordYS1_comm = 0; // =1-> Выбрали YS1
            //flCoordYS2_comm = 0; // =1-> Выбрали YS2

            // .....................................................................
            // Координаты СП

            /*
                        // Координаты СП на местности в м
                        XSP_comm = 0;
                        YSP_comm = 0;
                        XYS1_comm = 0;
                        YYS1_comm = 0;
                        XYS2_comm = 0;
                        YYS2_comm = 0;

                        // DATUM
                        // ГОСТ 51794_2008
                        dXdat_comm = 25;
                        dYdat_comm = -141;
                        dZdat_comm = -80;

                        dLat_comm = 0;
                        dLong_comm = 0;

                        // Эллипсоид Красовского, град
                        LatKrG_comm = 0;
                        LongKrG_comm = 0;
                        // Эллипсоид Красовского, rad
                        LatKrR_comm = 0;
                        LongKrR_comm = 0;
                        // Эллипсоид Красовского, град,мин,сек
                        Lat_Grad_comm = 0;
                        Lat_Min_comm = 0;
                        Lat_Sec_comm = 0;
                        Long_Grad_comm = 0;
                        Long_Min_comm = 0;
                        Long_Sec_comm = 0;
                        // Гаусс-крюгер(СК42) м
                        XSP42_comm = 0;
                        YSP42_comm = 0;

                        // Эллипсоид Красовского, град
                        LatKrG_YS1_comm = 0;
                        LongKrG_YS1_comm = 0;
                        // Эллипсоид Красовского, rad
                        LatKrR_YS1_comm = 0;
                        LongKrR_YS1_comm = 0;
                        // Эллипсоид Красовского, град,мин,сек
                        Lat_Grad_YS1_comm = 0;
                        Lat_Min_YS1_comm = 0;
                        Lat_Sec_YS1_comm = 0;
                        Long_Grad_YS1_comm = 0;
                        Long_Min_YS1_comm = 0;
                        Long_Sec_YS1_comm = 0;
                        // Гаусс-крюгер(СК42) м
                        XYS142_comm = 0;
                        YYS142_comm = 0;

                        // Эллипсоид Красовского, град
                        LatKrG_YS2_comm = 0;
                        LongKrG_YS2_comm = 0;
                        // Эллипсоид Красовского, rad
                        LatKrR_YS2_comm = 0;
                        LongKrR_YS2_comm = 0;
                        // Эллипсоид Красовского, град,мин,сек
                        Lat_Grad_YS2_comm = 0;
                        Lat_Min_YS2_comm = 0;
                        Lat_Sec_YS2_comm = 0;
                        Long_Grad_YS2_comm = 0;
                        Long_Min_YS2_comm = 0;
                        Long_Sec_YS2_comm = 0;
                        // Гаусс-крюгер(СК42) м
                        XYS242_comm = 0;
                        YYS242_comm = 0;
            */
            // ......................................................................
            // Основные параметры

            OwnHeight_comm = 0;
            Point1Height_comm = 0;
            Point2Height_comm = 0;
            //HeightOwnObject_comm = 0;
            PowerOwn_comm = 0;
            CoeffOwn_comm = 0;
            //RadiusZone_comm = 0;
            //MaxDist_comm = 0;

            i_HeightOwnObject_comm = 0;
            i_Cap1_comm = 0;
            i_WidthHindrance_comm = 0;
            i_Surface_comm = 0;
            Cap1_comm = 0;
            WidthHindrance_comm = 0;
            //Surface_comm = 0;

            // Высота средства подавления
            // ??????????????????????
            HeightAntennOwn_comm = 0;
            HeightTotalOwn_comm = 0;

            // Для подавляемой линии
            Freq_comm = 0;
            PowerOpponent_comm = 0;
            CoeffTransmitOpponent_comm = 0;
            CoeffReceiverOpponent_comm = 0;
            RangeComm_comm = 0;
            WidthSignal_comm = 0;
            HeightTransmitOpponent_comm = 0;
            HeightReceiverOpponent_comm = 0;
            CoeffSupOpponent_comm = 0;
            i_PolarOpponent_comm = 0;
            i_CoeffSupOpponent_comm = 0;
            i_TypeCommOpponent_comm = 0;

            // ......................................................................
            // Зона

            dCoeffQ_comm = 0;
            dCoeffHE_comm = 0;
            iCorrectHeightOwn_comm = 0;
            iResultHeightOwn_comm = 0;
            iMiddleHeight_comm = 0;
            iMinHeight_comm = 0;
            iCorrectHeightOpponent_comm = 0;
            iResultHeightOpponent_comm = 0;
            iMaxDistance_comm = 0;
            dGamma_comm = 0;
            //liRadiusZone_comm = 0;

            // ......................................................................
            iDistJammerComm1 = 0; // расстояние от УС1 до средства подаления
            iDistJammerComm2 = 0; // расстояние от УС2 до средства подаления
            iDistBetweenComm = 0;
            blResultSupress = false;
            blResultSupress2 = false;

        } // Конструктор
          // ***********************************************************  Конструктор


        // Обработчик кнопки "Принять"
        private void bAccept_Click(object sender, EventArgs e)
        {
            ;
        }

        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void Form2_Load(object sender, EventArgs e)
        {
            string strExePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            string PathFolder1 = Path.GetDirectoryName(strExePath);

            string strPathLatest = PathFolder1 + "\\INI\\Common.ini";
            foreach (string line in File.ReadLines(strPathLatest))
            {
                if (line.Contains("Language "))
                {
                    string LanguageAsNumber = line.Split(new string[] { "=" }, StringSplitOptions.None).Last();
                    NumberOfLanguage = Int32.Parse(LanguageAsNumber);
                    break;
                }
            }
            LanguageChooser();

            // ----------------------------------------------------------------------
            gbOwnRect.Visible = true;
            gbOwnRect.Location = new Point(8, 26);

            gbOwnRect42.Visible = false;
            gbOwnRad.Visible = false;
            gbOwnDegMin.Visible = false;
            gbOwnDegMinSec.Visible = false;
            // ----------------------------------------------------------------------
            gbPt1Rect.Visible = true;
            gbPt1Rect.Location = new Point(6, 11);
            gbPt2Rect.Visible = true;
            gbPt2Rect.Location = new Point(6, 11);

            gbPt1Rect42.Visible = false;
            gbPt1Rad.Visible = false;
            gbPt1DegMin.Visible = false;
            gbPt1DegMinSec.Visible = false;

            gbPt2Rect42.Visible = false;
            gbPt2Rad.Visible = false;
            gbPt2DegMin.Visible = false;
            gbPt2DegMinSec.Visible = false;
            // ----------------------------------------------------------------------
            // Все ComboBox

            cbChooseSC.SelectedIndex = 0;
            //cbCommChooseSC.SelectedIndex = 0;

            // Выбор СП
            cbCenterLSR.SelectedIndex = 0;

            // Пропускная способность
            cbCap1.SelectedIndex = 0;
            // Ширина спектра помехи
            cbWidthHindrance.SelectedIndex = 0;
            // Подстилающая поверхность
            cbSurface.SelectedIndex = 0;

            // Поляризация сигнала
            cbPolarOpponent.SelectedIndex = 0;
            // Вид связи
            cbTypeCommOpponent.SelectedIndex = 0;

            // Средство РП
            //cbHeightOwnObject.SelectedIndex = 0;
            // Коэффициент подавления
            //cbCoeffSupOpponent.SelectedIndex = 0;
            // TextBox
            //tbCoeffSupOpponent.Text = "2,3";
            //chbXY.Checked = false;
            //chbXY1.Checked = false;
            // ----------------------------------------------------------------------
            // Переменные

            GlobalVarLn.Numb_CommPowerAvail = (int)nudCountOpponent.Value;
            GlobalVarLn.fl_CommPowerAvail = 0; // Отрисовка зоны
            GlobalVarLn.flCoordSP_comm = 0; // =1-> Выбрали СП
            GlobalVarLn.flCoordYS1_comm = 0;
            GlobalVarLn.flCoordYS2_comm = 0;
            // ----------------------------------------------------------------------

        } // LoadForm

        // ************************************************************************
        // Очистка
        // ************************************************************************

        private void bClear_Click(object sender, EventArgs e)
        {
            ClassMap.ClearForm2();

        }  // Clear
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox "cbChooseSC": Выбор СК 
        // ************************************************************************

        private void cbChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChooseSystemCoord_Comm(cbChooseSC.SelectedIndex);

        } // Обработчик ComboBox "cbChooseSC": Выбор СК
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox "cbCommChooseSC": Выбор СК YS
        // ************************************************************************

        private void cbCommChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ChooseSystemCoordYS_Comm(cbCommChooseSC.SelectedIndex);
            ;

        } // Обработчик ComboBox "cbCommChooseSC": Выбор СК
        // ************************************************************************

        // ************************************************************************
        // Обработчик "Изменить число УС"
        // ************************************************************************

        private void nudCountOpponent_ValueChanged(object sender, EventArgs e)
        {
            // ........................................................................
            GlobalVarLn.Numb_CommPowerAvail = (int)nudCountOpponent.Value;
            // ........................................................................
            if (GlobalVarLn.Numb_CommPowerAvail == 1)
            {
                gbPt2Rect.Enabled = false;
                gbPt2Rect42.Enabled = false;
                gbPt2Rad.Enabled = false;
                gbPt2DegMin.Enabled = false;
                gbPt2DegMinSec.Enabled = false;

            } // 1YS
              // ........................................................................
            else
            {
                gbPt2Rect.Enabled = true;
                gbPt2Rect42.Enabled = true;
                gbPt2Rad.Enabled = true;
                gbPt2DegMin.Enabled = true;
                gbPt2DegMinSec.Enabled = true;

            } // 2YS
              // ........................................................................

        }
        // ************************************************************************

        // ************************************************************************
        // Обработчик Button1 "СП": Выбор СП
        // ************************************************************************

        private void button1_Click(object sender, EventArgs e)
        {

            double xtmp_ed, ytmp_ed, htmp_ed;
            double xtmp1_ed, ytmp1_ed;
            int ind1 = 0;
            int ind2 = 0;

            xtmp_ed = 0;
            ytmp_ed = 0;
            htmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;
            // ......................................................................
            ClassMap objClassMap1_ed = new ClassMap();
            ClassMap objClassMap2_ed = new ClassMap();
            ClassMap objClassMap3_ed = new ClassMap();
            // ......................................................................
            ind1 = GlobalVarLn.objFormSuppressionG.cbChooseSC.SelectedIndex;
            ind2 = GlobalVarLn.objFormSuppressionG.cbCenterLSR.SelectedIndex;

            ClassMap.ClearForm2();

            GlobalVarLn.objFormSuppressionG.cbChooseSC.SelectedIndex = ind1;
            GlobalVarLn.objFormSuppressionG.cbCenterLSR.SelectedIndex = ind2;

            // ----------------------------------------------------------------------
            // Enter CenterZone

            switch (cbCenterLSR.SelectedIndex)
            {
                case 0: // Мышь на карте

                    // !!! реальные координаты на местности карты в м (Plane)
                    GlobalVarLn.XCenter_comm = GlobalVarLn.MapX1;
                    GlobalVarLn.YCenter_comm = GlobalVarLn.MapY1;

                    if ((GlobalVarLn.XCenter_comm == 0) || (GlobalVarLn.YCenter_comm == 0))
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("Не выбрана СП");
                        }
                        else if (GlobalVarLn.fl_Azb == 1)
                        {
                            //Azb???
                            MessageBox.Show("MS seçilməyib");
                        }
                        else
                        {
                            MessageBox.Show("JS is not selected");
                        }

                        return;
                    }

                    break;

                case 1: // Выбор из списка СП: АСП

                    //if (System.IO.File.Exists(Application.StartupPath + "\\Settings.ini"))
                    //{
                    //    GlobalVarLn.XCenter_comm = (double)iniRW.get_X_ASP();
                    //    GlobalVarLn.YCenter_comm = (double)iniRW.get_Y_ASP();
                    //}
                    //else
                    //{
                    //    MessageBox.Show("Невозможно открыть INI файл");
                    //    return;
                    //}
                    //777
                    GlobalVarLn.XCenter_comm = GlobalVarLn.XCenter_Sost;
                    GlobalVarLn.YCenter_comm = GlobalVarLn.YCenter_Sost;

                    if ((GlobalVarLn.XCenter_comm == 0) || (GlobalVarLn.YCenter_comm == 0))
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("Нет координат АСП");
                        }
                        else if (GlobalVarLn.fl_Azb == 1)
                        {
                            //Azb???
                            MessageBox.Show("No mənşəyi (MS)");

                        }
                        else
                        {
                            MessageBox.Show("No station coordinates");

                        }

                        return;
                    }

                    break;

                case 2: // Выбор из списка СП: АСПсопр.

                    //if (System.IO.File.Exists(Application.StartupPath + "\\Settings.ini"))
                    //{
                    //    GlobalVarLn.XCenter_comm = (double)iniRW.get_X_ASPS();
                    //    GlobalVarLn.YCenter_comm = (double)iniRW.get_Y_ASPS();
                    //}
                    //else
                    //{
                    //    MessageBox.Show("Невозможно открыть INI файл");
                    //    return;
                    //}
                    //777
                    GlobalVarLn.XCenter_comm = GlobalVarLn.XPoint1_Sost;
                    GlobalVarLn.YCenter_comm = GlobalVarLn.YPoint1_Sost;

                    if ((GlobalVarLn.XCenter_comm == 0) || (GlobalVarLn.YCenter_comm == 0))
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("Нет координат АСП сопряженной");
                        }
                        else if (GlobalVarLn.fl_Azb == 1)
                        {
                            //Azb???
                            MessageBox.Show("No mənşəyi (Beraber işleyen manee stansiyası)");
                        }
                        else
                        {
                            MessageBox.Show("No JSmated coordinates");
                        }

                        return;

                    }

                    break;

                case 3: // Выбор из списка СП: PU

                    //if (System.IO.File.Exists(Application.StartupPath + "\\Settings.ini"))
                    //{
                    //    GlobalVarLn.XCenter_comm = (double)iniRW.get_X_PU();
                    //    GlobalVarLn.YCenter_comm = (double)iniRW.get_Y_PU();
                    //}
                    //else
                    //{
                    //    MessageBox.Show("Невозможно открыть INI файл");
                    //    return;
                    //}
                    //777
                    GlobalVarLn.XCenter_comm = GlobalVarLn.XPoint2_Sost;
                    GlobalVarLn.YCenter_comm = GlobalVarLn.YPoint2_Sost;

                    if ((GlobalVarLn.XCenter_comm == 0) || (GlobalVarLn.YCenter_comm == 0))
                    {
                        if (GlobalVarLn.fl_Azb == 0)
                        {
                            MessageBox.Show("Нет координат ПУ");
                        }
                        else if (GlobalVarLn.fl_Azb == 1)
                        {
                            //Azb???
                            MessageBox.Show("No mənşəyi (idareetme pultu)");
                        }
                        else
                        {
                            MessageBox.Show("No control post coordinates");
                        }

                        return;
                    }

                    break;


            } // SWITCH
            // ----------------------------------------------------------------------

            // ......................................................................
            xtmp_ed = GlobalVarLn.XCenter_comm;
            ytmp_ed = GlobalVarLn.YCenter_comm;
            // ......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(xtmp_ed, ytmp_ed);
            GlobalVarLn.HCenter_comm = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            htmp_ed = GlobalVarLn.HCenter_comm;
            tbOwnHeight.Text = Convert.ToString(htmp_ed);
            // ......................................................................
            GlobalVarLn.fl_CommPowerAvail = 1;
            GlobalVarLn.flCoordSP_comm = 1;        // Центр выбран
            // ......................................................................
            // SP на карте

            // Убрать с карты
            //GlobalVarLn.axMapScreenGlobal.Repaint();

            // SP
            ClassMap.f_DrawSPXY(
                          GlobalVarLn.XCenter_comm,  // m на местности
                          GlobalVarLn.YCenter_comm,
                              ""
                         );
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;

            //333
            GlobalVarLn.BWGS84_1_comm2 = xtmp1_ed;
            GlobalVarLn.LWGS84_1_comm2 = ytmp1_ed;
            // .......................................................................

            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLong
                (
                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLong_comm1   // приращение по долготе, угл.сек
                );
            // ................................................................ dLong

            // dLat .................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLat
                (
                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLat_comm1        // приращение по долготе, угл.сек
                );
            // ................................................................. dLat

            // Lat,Long .............................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objClassMap1_ed.f_WGS84_SK42_Lat_Long
                   (
                       // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       GlobalVarLn.dLat_comm1,       // приращение по долготе, угл.сек
                       GlobalVarLn.dLong_comm1,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref GlobalVarLn.LatKrG_comm1,   // широта
                       ref GlobalVarLn.LongKrG_comm1   // долгота
                   );
            // ............................................................ Lat,Long


            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            GlobalVarLn.LatKrR_comm1 = (GlobalVarLn.LatKrG_comm1 * Math.PI) / 180;
            GlobalVarLn.LongKrR_comm1 = (GlobalVarLn.LongKrG_comm1 * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LatKrG_comm1,

                // Выходные параметры 
                ref GlobalVarLn.Lat_Grad_comm1,
                ref GlobalVarLn.Lat_Min_comm1,
                ref GlobalVarLn.Lat_Sec_comm1
              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LongKrG_comm1,

                // Выходные параметры 
                ref GlobalVarLn.Long_Grad_comm1,
                ref GlobalVarLn.Long_Min_comm1,
                ref GlobalVarLn.Long_Sec_comm1
              );
            // .......................................................................

            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            objClassMap3_ed.f_SK42_Krug
                   (
                       // Входные параметры (!!! grad)
                       // !!! эллипсоид Красовского
                       GlobalVarLn.LatKrG_comm1,   // широта
                       GlobalVarLn.LongKrG_comm1,  // долгота

                       // Выходные параметры (km)
                       ref GlobalVarLn.XSP42_comm1,
                       ref GlobalVarLn.YSP42_comm1
                   );

            // km->m
            GlobalVarLn.XSP42_comm1 = GlobalVarLn.XSP42_comm1 * 1000;
            GlobalVarLn.YSP42_comm1 = GlobalVarLn.YSP42_comm1 * 1000;
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отобразить координаты в окошках

            ClassMap.OtobrSP_Form2();
            // .......................................................................

        } // Button1->SP
        // ***************************************************************************

        // ************************************************************************
        // Обработчик Button2 "YS1": Выбор YS1
        // ************************************************************************

        private void button2_Click(object sender, EventArgs e)
        {

            double xtmp_ed, ytmp_ed, htmp_ed;
            double xtmp1_ed, ytmp1_ed;

            xtmp_ed = 0;
            ytmp_ed = 0;
            htmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;
            // ......................................................................
            ClassMap objClassMap1_ed = new ClassMap();
            ClassMap objClassMap2_ed = new ClassMap();
            ClassMap objClassMap3_ed = new ClassMap();
            // ......................................................................

            // ----------------------------------------------------------------------
            // Enter YS1

            // Мышь на карте

            // !!! реальные координаты на местности карты в м (Plane)
            GlobalVarLn.XPoint1_comm = GlobalVarLn.MapX1;
            GlobalVarLn.YPoint1_comm = GlobalVarLn.MapY1;

            if ((GlobalVarLn.XPoint1_comm == 0) || (GlobalVarLn.YPoint1_comm == 0))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Не выбран УС1");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb
                    MessageBox.Show("RQ1 seçilməyib");
                }
                else
                {
                    MessageBox.Show("СС1 is not selected");
                }

                return;
            }

            if (GlobalVarLn.flCoordSP_comm == 0)
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Не выбран центр зоны");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb???
                    MessageBox.Show("zonanın merkezi seçilmeyib");
                }
                else
                {
                    MessageBox.Show("Zone center is not selected");
                }

                return;
            }
            if (GlobalVarLn.flCoordYS1_comm == 1)
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("УС1 уже выбран. Очистите данные");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb???
                    MessageBox.Show(" Rabitə qovşağı 1 seçilib. Melumatları silmek");
                }
                else
                {
                    MessageBox.Show(" СС1 is already selected. Clear data.");
                }

                return;
            }

            // ----------------------------------------------------------------------

            // ......................................................................
            xtmp_ed = GlobalVarLn.XPoint1_comm;
            ytmp_ed = GlobalVarLn.YPoint1_comm;
            // ......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(xtmp_ed, ytmp_ed);
            GlobalVarLn.HPoint1_comm = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            htmp_ed = GlobalVarLn.HPoint1_comm;
            tbPt1Height.Text = Convert.ToString(htmp_ed);
            // ......................................................................
            GlobalVarLn.flCoordYS1_comm = 1;        // YS1 выбран
            // ......................................................................
            // YS1 на карте

            // Убрать с карты
            //GlobalVarLn.axMapScreenGlobal.Repaint();

            if (GlobalVarLn.fl_Azb == 0)
            {
                // YS1
                ClassMap.f_Map_Pol_XY_stat(
                              GlobalVarLn.XPoint1_comm,  // m
                              GlobalVarLn.YPoint1_comm,
                              2,    // BLUE
                              "УС1"
                             //Azb
                             //"RQ1"
                             );
            }
            else if (GlobalVarLn.fl_Azb == 1)
            {
                // YS1
                ClassMap.f_Map_Pol_XY_stat(
                              GlobalVarLn.XPoint1_comm,  // m
                              GlobalVarLn.YPoint1_comm,
                              2,    // BLUE
                                    //Azb
                             "RQ1"
                             );
            }
            else
            {
                // YS1
                ClassMap.f_Map_Pol_XY_stat(
                              GlobalVarLn.XPoint1_comm,  // m
                              GlobalVarLn.YPoint1_comm,
                              2,    // BLUE
                                    //Azb
                             "CC1"
                             );
            }

            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;

            //333
            GlobalVarLn.BWGS84_2_comm2 = xtmp1_ed;
            GlobalVarLn.LWGS84_2_comm2 = ytmp1_ed;
            // .......................................................................

            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLong
                (
                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLong_comm1   // приращение по долготе, угл.сек
                );
            // ................................................................ dLong

            // dLat .................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLat
                (
                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLat_comm1        // приращение по долготе, угл.сек
                );
            // ................................................................. dLat

            // Lat,Long .............................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objClassMap1_ed.f_WGS84_SK42_Lat_Long
                   (
                       // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       GlobalVarLn.dLat_comm1,       // приращение по долготе, угл.сек
                       GlobalVarLn.dLong_comm1,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref GlobalVarLn.LatKrG_YS1_comm1,   // широта
                       ref GlobalVarLn.LongKrG_YS1_comm1   // долгота
                   );
            // ............................................................ Lat,Long


            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            GlobalVarLn.LatKrR_YS1_comm1 = (GlobalVarLn.LatKrG_YS1_comm1 * Math.PI) / 180;
            GlobalVarLn.LongKrR_YS1_comm1 = (GlobalVarLn.LongKrG_YS1_comm1 * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LatKrG_YS1_comm1,

                // Выходные параметры 
                ref GlobalVarLn.Lat_Grad_YS1_comm1,
                ref GlobalVarLn.Lat_Min_YS1_comm1,
                ref GlobalVarLn.Lat_Sec_YS1_comm1
              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LongKrG_YS1_comm1,

                // Выходные параметры 
                ref GlobalVarLn.Long_Grad_YS1_comm1,
                ref GlobalVarLn.Long_Min_YS1_comm1,
                ref GlobalVarLn.Long_Sec_YS1_comm1
              );
            // .......................................................................

            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            objClassMap3_ed.f_SK42_Krug
                   (
                       // Входные параметры (!!! grad)
                       // !!! эллипсоид Красовского
                       GlobalVarLn.LatKrG_YS1_comm1,   // широта
                       GlobalVarLn.LongKrG_YS1_comm1,  // долгота

                       // Выходные параметры (km)
                       ref GlobalVarLn.XYS142_comm1,
                       ref GlobalVarLn.YYS142_comm1
                   );

            // km->m
            GlobalVarLn.XYS142_comm1 = GlobalVarLn.XYS142_comm1 * 1000;
            GlobalVarLn.YYS142_comm1 = GlobalVarLn.YYS142_comm1 * 1000;
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отобразить координаты в окошках

            ClassMap.OtobrYS1_Form2();
            // .......................................................................

        } // Button2: YS1
        // ************************************************************************

        // ************************************************************************
        // Обработчик Button3 "YS2": Выбор YS2
        // ************************************************************************

        private void button3_Click(object sender, EventArgs e)
        {
            double xtmp_ed, ytmp_ed, htmp_ed;
            double xtmp1_ed, ytmp1_ed;

            xtmp_ed = 0;
            ytmp_ed = 0;
            htmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;
            // ......................................................................
            ClassMap objClassMap1_ed = new ClassMap();
            ClassMap objClassMap2_ed = new ClassMap();
            ClassMap objClassMap3_ed = new ClassMap();
            // ......................................................................
            if (GlobalVarLn.Numb_CommPowerAvail == 1)
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Число YC равно 1");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb???
                    MessageBox.Show("Rabite kanallarının sayı 1 beraberdir");
                }
                else
                {
                    MessageBox.Show("The number of СС is 1");
                }

                return;
            }
            // ......................................................................

            if (GlobalVarLn.flCoordSP_comm == 0)
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Не выбран центр зоны");
                    //Azb
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    MessageBox.Show("zonanın merkezi  seçilmeyib");
                }
                else
                {
                    MessageBox.Show("Zone center is not selected");
                }

                return;
            }

            if (GlobalVarLn.flCoordYS1_comm == 0)
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Не выбран УС1");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb
                    MessageBox.Show("RQ1 seçilməyib");
                }
                else
                {
                    MessageBox.Show("CC1 is not selected");
                }

                return;
            }

            if (GlobalVarLn.flCoordYS2_comm == 1)
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("УС2 уже выбран. Очистите данные");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb???
                    MessageBox.Show(" Rabitə qovşağı 2 seçilib. Melumatları silmek");

                }
                else
                {
                    MessageBox.Show(" СС2 is already selected. Clear data.");

                }

                return;
            }

            // ----------------------------------------------------------------------
            // Enter YS2

            // Мышь на карте

            // !!! реальные координаты на местности карты в м (Plane)
            GlobalVarLn.XPoint2_comm = GlobalVarLn.MapX1;
            GlobalVarLn.YPoint2_comm = GlobalVarLn.MapY1;

            if ((GlobalVarLn.XPoint2_comm == 0) || (GlobalVarLn.YPoint2_comm == 0))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Не выбран УС2");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb
                    MessageBox.Show("RQ2 seçilməyib");
                }
                else
                {
                    MessageBox.Show("CC2 is not selected");
                }

                return;
            }
            // ----------------------------------------------------------------------

            // ......................................................................
            xtmp_ed = GlobalVarLn.XPoint2_comm;
            ytmp_ed = GlobalVarLn.YPoint2_comm;
            // ......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(xtmp_ed, ytmp_ed);
            GlobalVarLn.HPoint2_comm = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            htmp_ed = GlobalVarLn.HPoint2_comm;
            tbPt2Height.Text = Convert.ToString(htmp_ed);
            // ......................................................................
            GlobalVarLn.flCoordYS2_comm = 1;        // YS2 выбран
            // ......................................................................
            // YS1 на карте

            // Убрать с карты
            //GlobalVarLn.axMapScreenGlobal.Repaint();

            if (GlobalVarLn.fl_Azb == 0)
            {
                // YS2
                ClassMap.f_Map_Pol_XY_stat(
                              GlobalVarLn.XPoint2_comm,  // m
                              GlobalVarLn.YPoint2_comm,
                              2,    // BLUE
                              "УС2"
                             //"RQ2"
                             );
            }
            else if (GlobalVarLn.fl_Azb == 1)
            {
                ClassMap.f_Map_Pol_XY_stat(
                              GlobalVarLn.XPoint2_comm,  // m
                              GlobalVarLn.YPoint2_comm,
                              2,    // BLUE
                                    //Azb
                              "RQ2"
                              );
            }
            else
            {
                ClassMap.f_Map_Pol_XY_stat(
                              GlobalVarLn.XPoint2_comm,  // m
                              GlobalVarLn.YPoint2_comm,
                              2,    // BLUE
                                    //Azb
                              "CC2"
                              );
            }

            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;

            //333
            GlobalVarLn.BWGS84_3_comm2 = xtmp1_ed;
            GlobalVarLn.LWGS84_3_comm2 = ytmp1_ed;
            // .......................................................................

            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLong
                (
                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLong_comm1   // приращение по долготе, угл.сек
                );
            // ................................................................ dLong

            // dLat .................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLat
                (
                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLat_comm1        // приращение по долготе, угл.сек
                );
            // ................................................................. dLat

            // Lat,Long .............................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objClassMap1_ed.f_WGS84_SK42_Lat_Long
                   (
                       // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       GlobalVarLn.dLat_comm1,       // приращение по долготе, угл.сек
                       GlobalVarLn.dLong_comm1,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref GlobalVarLn.LatKrG_YS2_comm1,   // широта
                       ref GlobalVarLn.LongKrG_YS2_comm1   // долгота
                   );
            // ............................................................ Lat,Long


            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            GlobalVarLn.LatKrR_YS2_comm1 = (GlobalVarLn.LatKrG_YS2_comm1 * Math.PI) / 180;
            GlobalVarLn.LongKrR_YS2_comm1 = (GlobalVarLn.LongKrG_YS2_comm1 * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LatKrG_YS2_comm1,

                // Выходные параметры 
                ref GlobalVarLn.Lat_Grad_YS2_comm1,
                ref GlobalVarLn.Lat_Min_YS2_comm1,
                ref GlobalVarLn.Lat_Sec_YS2_comm1
              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LongKrG_YS2_comm1,

                // Выходные параметры 
                ref GlobalVarLn.Long_Grad_YS2_comm1,
                ref GlobalVarLn.Long_Min_YS2_comm1,
                ref GlobalVarLn.Long_Sec_YS2_comm1
              );
            // .......................................................................

            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            objClassMap3_ed.f_SK42_Krug
                   (
                       // Входные параметры (!!! grad)
                       // !!! эллипсоид Красовского
                       GlobalVarLn.LatKrG_YS2_comm1,   // широта
                       GlobalVarLn.LongKrG_YS2_comm1,  // долгота

                       // Выходные параметры (km)
                       ref GlobalVarLn.XYS242_comm1,
                       ref GlobalVarLn.YYS242_comm1
                   );

            // km->m
            GlobalVarLn.XYS242_comm1 = GlobalVarLn.XYS242_comm1 * 1000;
            GlobalVarLn.YYS242_comm1 = GlobalVarLn.YYS242_comm1 * 1000;
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отобразить координаты в окошках

            ClassMap.OtobrYS2_Form2();
            // .......................................................................

        } // Button3: YS2
        // ************************************************************************

        // ************************************************************************
        // Button "Принять" 
        // ************************************************************************

        private void bAccept_Click_1(object sender, EventArgs e)
        {

            // ....................................................................
            if (GlobalVarLn.flCoordSP_comm == 0)
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Не выбрана СП");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb
                    MessageBox.Show("MS seçilməyib");
                }
                else
                {
                    MessageBox.Show("JS is not selected");
                }

                return;
            }
            if (GlobalVarLn.flCoordYS1_comm == 0)
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Не выбран УС1");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb
                    MessageBox.Show("RQ1 seçilməyib");
                }
                else
                {
                    MessageBox.Show("CC1 is not selected");
                }

                return;
            }
            if ((nudCountOpponent.Value == 2) && (GlobalVarLn.flCoordYS2_comm == 0))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Не выбран УС2");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb
                    MessageBox.Show("RQ2 seçilməyib");
                }
                else
                {
                    MessageBox.Show("CC2 is not selected");
                }

                return;
            }
            // ....................................................................

            // Центр zone *************************************************************
            // Координаты на местности в м

            GlobalVarLn.tpOwnCoordRect_comm.X = (int)GlobalVarLn.XCenter_comm;
            GlobalVarLn.tpOwnCoordRect_comm.Y = (int)GlobalVarLn.YCenter_comm;

            //if ((tbXRect.Text == "") || (tbYRect.Text == ""))
            //{
            //    MessageBox.Show("Некорректные координаты центра зоны (метры на местности)");
            //    return;
            //}
            // ************************************************************* Центр Zone

            // YS1 *********************************************************************
            // Координаты на местности в м

            GlobalVarLn.tpPoint1Rect.X = (int)GlobalVarLn.XPoint1_comm;
            GlobalVarLn.tpPoint1Rect.Y = (int)GlobalVarLn.YPoint1_comm;

            //if ((tbPt1XRect.Text == "") || (tbPt1YRect.Text == ""))
            //{
            //    MessageBox.Show("Некорректные координаты УС1 (метры на местности)");
            //    return;
            //}
            // ********************************************************************* YS1

            // YS2 *********************************************************************
            // Координаты на местности в м

            GlobalVarLn.tpPoint2Rect.X = (int)GlobalVarLn.XPoint2_comm;
            GlobalVarLn.tpPoint2Rect.Y = (int)GlobalVarLn.YPoint2_comm;

            //if (((tbPt2XRect.Text == "") || (tbPt2YRect.Text == "")) && (GlobalVarLn.Numb_CommPowerAvail == 2))
            //{
            //    MessageBox.Show("Некорректные координаты УС2 (метры на местности)");
            //    return;
            //}
            // ********************************************************************* YS2

            // !!! Высоты *************************************************************
            // !!! GlobalVarLn.HCenter_comm введена по кнопке ЦентрЗоны

            // .........................................................................
            // Средняя высота местности

            iMiddleHeight_comm = DefineMiddleHeight_Comm(GlobalVarLn.tpOwnCoordRect_comm, GlobalVarLn.axMapPointGlobalAdd,
                                                         GlobalVarLn.axMapScreenGlobal);
            tbMidH.Text = iMiddleHeight_comm.ToString();
            // .........................................................................
            // ЦЕНТР zone

            // Высота антенны
            //HeightAntennOwn_comm = Convert.ToDouble(tbHAnt.Text);
            String ss = "";
            ss = tbHAnt.Text;
            try
            {
                HeightAntennOwn_comm = Convert.ToDouble(ss);
            }
            catch (SystemException)
            {
                try
                {
                    if (ss.IndexOf(",") > -1) ss = ss.Replace(',', '.');
                    else ss = ss.Replace('.', ',');
                    HeightAntennOwn_comm = Convert.ToDouble(ss);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимая высота антенны");
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                    {
                        //Azb?
                        MessageBox.Show("Yolverilməz Antenanın hündürlüyü göstəriciləri");
                    }
                    else
                    {
                        MessageBox.Show("Invalid antenna height");
                    }

                    return;
                }
            }

            // Рельеф
            OwnHeight_comm = GlobalVarLn.HCenter_comm;

            // Общая высота СП
            HeightTotalOwn_comm = HeightAntennOwn_comm + OwnHeight_comm;
            // отобразить значение высоты
            ichislo = (long)(HeightTotalOwn_comm);
            tbHeightOwnObject.Text = Convert.ToString(ichislo);
            // .........................................................................
            // YS1
            // Рельеф
            Point1Height_comm = GlobalVarLn.HPoint1_comm;

            // YS2
            // Рельеф
            Point2Height_comm = GlobalVarLn.HPoint2_comm;

            // антенна передатчика
            //HeightTransmitOpponent_comm = Convert.ToDouble(tbHeightTransmitOpponent.Text);
            ss = tbHeightTransmitOpponent.Text;
            try
            {
                HeightTransmitOpponent_comm = Convert.ToDouble(ss);
            }
            catch (SystemException)
            {
                try
                {
                    if (ss.IndexOf(",") > -1) ss = ss.Replace(',', '.');
                    else ss = ss.Replace('.', ',');
                    HeightTransmitOpponent_comm = Convert.ToDouble(ss);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимая высота антенны");
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                    {
                        //Azb?
                        MessageBox.Show("Yolverilməz Antenanın hündürlüyü göstəriciləri");
                    }
                    else
                    {
                        MessageBox.Show("Invalid antenna height");
                    }

                    return;
                }
            }

            // антенна приемника
            //HeightReceiverOpponent_comm = Convert.ToDouble(tbHeightReceiverOpponent.Text);
            ss = tbHeightReceiverOpponent.Text;
            try
            {
                HeightReceiverOpponent_comm = Convert.ToDouble(ss);
            }
            catch (SystemException)
            {
                try
                {
                    if (ss.IndexOf(",") > -1) ss = ss.Replace(',', '.');
                    else ss = ss.Replace('.', ',');
                    HeightReceiverOpponent_comm = Convert.ToDouble(ss);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимая высота антенны");
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                    {
                        //Azb?
                        MessageBox.Show("Yolverilməz antenanın hündürlüyü göstəriciləri");
                    }
                    else
                    {
                        MessageBox.Show("Invalid antenna height");
                    }

                    return;
                }
            }

            // ************************************************************* !!! Высоты

            // Ввод параметров ********************************************************
            // !!! Координаты СП уже расчитаны и введены по кнопке СП

            String s1 = "";


            // Мощность передатчика
            s1 = tbPowerOwn.Text;
            try
            {
                //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                PowerOwn_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    else s1 = s1.Replace('.', ',');
                    PowerOwn_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимое значение мощности");
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                    {
                        //Azb???
                        MessageBox.Show("Yolverilməz parametr göstəriciləri (Güc)");
                    }
                    else
                    {
                        MessageBox.Show("Invalid power value");
                    }

                    return;
                }
            }
            if ((PowerOwn_comm < 100) || (PowerOwn_comm > 2000))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Параметр 'мощность' выходит за пределы 100Вт - 2000Вт");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb
                    MessageBox.Show("Susdurulacaq vasitənin gücü susdurma dairəsində deyil 100Vt - 2000Vt ");
                }
                else
                {
                    MessageBox.Show("Parameter 'power' out of range 100Vt - 2000Vt ");
                }

                return;
            }

            // Коэффициент усиления
            s1 = tbCoeffOwn.Text;
            try
            {
                //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                CoeffOwn_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    else s1 = s1.Replace('.', ',');
                    CoeffOwn_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимое значение коэффициента усиления");
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                    {
                        //Azb???
                        MessageBox.Show("Yolverilməz parametr göstəriciləri (Gücləndirmə əmsalı)");
                    }
                    else
                    {
                        MessageBox.Show("Invalid gain value");
                    }

                    return;
                }
            }
            if ((CoeffOwn_comm < 1) || (CoeffOwn_comm > 25))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Параметр 'коэффициент усиления' выходит за пределы 1-25");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb
                    MessageBox.Show("Susdurulacaq vasitənin gücləndirmə əmsalının göstəriciləri susdurma dairəsindən uzaqdadır 1-25");
                }
                else
                {
                    MessageBox.Show("Parameter 'gain' out of range 1-25");
                }

                return;
            }

            // ComboBox (Индексы)
            //i_HeightOwnObject_comm = cbHeightOwnObject.SelectedIndex; // Средство РП
            //i_Cap1_comm = cbCap1.SelectedIndex; // Пропускная способность
            i_WidthHindrance_comm = cbWidthHindrance.SelectedIndex; // Ширина спектра помехи
            i_Surface_comm = cbSurface.SelectedIndex; // Подстилающая поверхность

            //WidthHindrance_comm = Convert.ToDouble(cbWidthHindrance.Text); // Ширина спектра помехи
            s1 = cbWidthHindrance.Text; // Ширина спектра помехи
            try
            {
                WidthHindrance_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    else s1 = s1.Replace('.', ',');
                    WidthHindrance_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимое значение ширины спектра помехи");
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                    {
                        //Azb???
                        MessageBox.Show("");
                    }
                    else
                    {
                        MessageBox.Show("Invalid spectrum width value");
                    }

                    return;
                }
            }

            // -----------------------------------------------------------------------
            // Для линии связи

            // Несущая частота
            s1 = tbFreq.Text;
            try
            {
                //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                Freq_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    else s1 = s1.Replace('.', ',');
                    Freq_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимое значение несущей частоты");
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                    {
                        //Azb???
                        MessageBox.Show("Yolverilməz parametr göstəriciləri (Tezlik)");
                    }
                    else
                    {
                        MessageBox.Show("Invalid frequency");
                    }

                    return;
                }
            }
            if ((Freq_comm < 30000) || (Freq_comm > 1215000))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Параметр 'несущая частота' выходит за пределы 30000кГц-1215000кГц");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb
                    MessageBox.Show("Susdurulacaq hədəfin tezlik göstəricisi susdurma dairəsində deyil 30000kHs-1215000kHs");
                }
                else
                {
                    MessageBox.Show("Parameter 'frequency' out of range 30000kHs-1215000kHs");
                }

                return;
            }

            // Мощность передатчика
            s1 = tbPowerOpponent.Text;
            try
            {
                //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                PowerOpponent_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    else s1 = s1.Replace('.', ',');
                    PowerOpponent_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимое значение мощности");
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                    {
                        //Azb???
                        MessageBox.Show("Yolverilməz parametr göstəriciləri (Güc)");
                    }
                    else
                    {
                        MessageBox.Show("Invalid power value");
                    }

                    return;
                }
            }
            if ((PowerOpponent_comm < 5) || (PowerOpponent_comm > 250))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Параметр 'мощность' выходит за пределы 5Вт - 250Вт");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb
                    MessageBox.Show("Susdurulacaq hədəfin gücü susdurma dairəsində deyil 5Вт - 250Вт");
                }
                else
                {
                    MessageBox.Show("Parameter 'power' out of range 5W - 250W");
                }

                return;
            }

            // коэффициент усиления передатчика		 
            s1 = tbCoeffTransmitOpponent.Text;
            try
            {
                //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                CoeffTransmitOpponent_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    else s1 = s1.Replace('.', ',');
                    CoeffTransmitOpponent_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимое значение коэффициента усиления передатчика");
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                    {
                        //Azb???
                        MessageBox.Show("Yolverilməz parametr göstəriciləri (Gücləndirmə əmsalı)");
                    }
                    else
                    {
                        MessageBox.Show("Invalid transmitter gain");
                    }

                    return;
                }
            }
            if ((CoeffTransmitOpponent_comm < 1) || (CoeffTransmitOpponent_comm > 10))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Параметр 'коэффициент усиления передатчика' выходит за пределы 1-10");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb
                    MessageBox.Show("Hədəfin ötürücüsünün güclənmə əmsalı  susdurma dairəsindən kənardır 1-10");
                }
                else
                {
                    MessageBox.Show("Parameter 'transmitter gain' out of range 1-10");
                }

                return;
            }

            /*
                        // коэффициент усиления приемника
                        s1 = tbCoeffReceiverOpponent.Text;
                        try
                        {
                            //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            CoeffReceiverOpponent_comm = Convert.ToDouble(s1);
                        }
                        catch (SystemException)
                        {
                            try
                            {
                                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                                else s1 = s1.Replace('.', ',');
                                CoeffReceiverOpponent_comm = Convert.ToDouble(s1);
                            }
                            catch
                            {
                                MessageBox.Show("Недопустимое значение коэффициента усиления приемника");
                                return;
                            }
                        }
            */
            // Дальность связи
            s1 = tbRangeComm.Text;
            try
            {
                //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                RangeComm_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    else s1 = s1.Replace('.', ',');
                    RangeComm_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимое значение дальности связи");
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                    {
                        //Azb???
                        MessageBox.Show("Yolverilməz parametr göstəriciləri (Rabitə uzaqlığı)");

                    }
                    else
                    {
                        MessageBox.Show("Invalid communication range");

                    }

                    return;
                }
            }
            if ((RangeComm_comm < 100) || (RangeComm_comm > 100000))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Параметр 'дальность связи' выходит за пределы 100м - 100000м");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb
                    MessageBox.Show("Susdurulacaq hədəfin göstəriciləri susdurma dairəsindən uzaqdadır 100m - 100000m");
                }
                else
                {
                    MessageBox.Show("Parameter 'communication range' out of range 100m - 100000m");
                }

                return;
            }
            GlobalVarLn.R_CommPA = RangeComm_comm;


            // Ширина спектра
            s1 = tbWidthSignal.Text;
            try
            {
                //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                WidthSignal_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    else s1 = s1.Replace('.', ',');
                    WidthSignal_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимое значение ширины спектра");
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                    {
                        //Azb???
                        MessageBox.Show("Yolverilməz parametr göstəriciləri (Spektorun eni)");
                    }
                    else
                    {
                        MessageBox.Show("Invalid spectrum width");
                    }

                    return;
                }
            }
            if ((WidthSignal_comm < 3) || (WidthSignal_comm > 5000))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Параметр 'ширина спектра объекта подавления' выходит за пределы 3кГц - 5000кГц");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb
                    MessageBox.Show("Susdurulacaq hədəfin elektromaqnit dalğalarının enliyi susdurma dairəsində deyil 3kHs - 5000kHs");
                }
                else
                {
                    MessageBox.Show("Parameter 'spectrum width' out of range 3kHs - 5000kHs");
                }

                return;
            }

            // Коэффициент подавления
            s1 = tbCoeffSupOpponent.Text;
            try
            {
                //if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                CoeffSupOpponent_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    else s1 = s1.Replace('.', ',');
                    CoeffSupOpponent_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        MessageBox.Show("Недопустимое значение коэффициента подавления");
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                    {
                        //Azb???
                        MessageBox.Show("Yolverilməz parametr göstəriciləri (Susdurma əmsalı)");
                    }
                    else
                    {
                        MessageBox.Show("Invalid J/S ratio");
                    }

                    return;
                }
            }
            if (CoeffSupOpponent_comm < 0.5)
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Коэффициент подавления должен быть больше 0.5");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb???
                    MessageBox.Show("Susdurma əmsalı <0.5");
                }
                else
                {
                    MessageBox.Show("J/S ratio must be greater than 0.5");
                }

                return;
            }

            // ComboBox (Индексы)
            i_PolarOpponent_comm = cbPolarOpponent.SelectedIndex;       // Поляризация
            //i_CoeffSupOpponent_comm = cbCoeffSupOpponent.SelectedIndex; // Коэффициент подавления
            i_TypeCommOpponent_comm = cbTypeCommOpponent.SelectedIndex; // Вид связи
                                                                        // -----------------------------------------------------------------------

            // ******************************************************** Ввод параметров


            // Расчет зоны ************************************************************


            // Kq
            dCoeffQ_comm = DefineCoeffQ_Comm(Freq_comm, i_Surface_comm);
            //tbCoeffQ.Text = dCoeffQ_comm.ToString();

            // K_HE
            dCoeffHE_comm = DefineCoeffHE_Comm(Freq_comm, dCoeffQ_comm);
            //tbCoeffHE.Text = dCoeffHE_comm.ToString();

            // Эффективная высота SP
            iCorrectHeightOwn_comm = DefineCorrectHeightOwn_Comm((int)HeightAntennOwn_comm, dCoeffHE_comm);
            //tbCorrectHeightOwn.Text = iCorrectHeightOwn_comm.ToString();

            // Hsp=Hant+Hrel
            iResultHeightOwn_comm = DefineResultHeightOwn_Comm(iCorrectHeightOwn_comm, (int)OwnHeight_comm);
            //tbResultHeightOwn.Text = iResultHeightOwn_comm.ToString();

            // MIN from Hrel_SP and Hmiddle
            iMinHeight_comm = DefineMinHeight_Comm(GlobalVarLn.tpOwnCoordRect_comm, iMiddleHeight_comm);
            //tbMinHeight.Text = iMinHeight_comm.ToString();

            // Heffect_OP (по Hant_PRM)
            iCorrectHeightOpponent_comm = DefineCorrectHeightOpponent_Comm((int)HeightReceiverOpponent_comm, dCoeffHE_comm);
            //tbCorrectHeightOpponent.Text = iCorrectHeightOpponent_comm.ToString();

            // Hop=hant_PRM+Hmiddle
            iResultHeightOpponent_comm = DefineResultHeightOpponent_Comm(iCorrectHeightOpponent_comm, iMiddleHeight_comm);
            //tbResultHeightOpponent.Text = iResultHeightOpponent_comm.ToString();

            iMaxDistance_comm = DefineMaxDistance_Comm(iResultHeightOwn_comm, iResultHeightOpponent_comm, iMinHeight_comm);
            //tbMaxDistance.Text = iMaxDistance_comm.ToString();
            tbMaxDist.Text = iMaxDistance_comm.ToString();  // ДПВ

            // Растояние SP-YS1
            iDistJammerComm1 = DefineDistance_Comm(GlobalVarLn.tpOwnCoordRect_comm, GlobalVarLn.tpPoint1Rect);
            // Растояние SP-YS2
            iDistJammerComm2 = DefineDistance_Comm(GlobalVarLn.tpOwnCoordRect_comm, GlobalVarLn.tpPoint2Rect);
            // Растояние YS1-YS2
            iDistBetweenComm = DefineDistance_Comm(GlobalVarLn.tpPoint1Rect, GlobalVarLn.tpPoint2Rect);

            if (nudCountOpponent.Value == 1)
            {
                iDistBetweenComm = (int)RangeComm_comm; // Дальность связи
            }

            GlobalVarLn.fl_NoSup1 = 0;
            GlobalVarLn.fl_NoSup2 = 0;

            // ...........................................................
            if (iMaxDistance_comm < iDistJammerComm1)
            {
                //ClassMap.tbResult.Text = "УС не подавляется";

                if (GlobalVarLn.fl_Azb == 0)
                {
                    ClassMap.f_Sup_S(
                                  GlobalVarLn.XPoint1_comm,  // m
                                  GlobalVarLn.YPoint1_comm,
                                  2,
                                  "УС1 не подавляется"
                                    //Azb??

                                    );
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    ClassMap.f_Sup_S(
                                  GlobalVarLn.XPoint1_comm,  // m
                                  GlobalVarLn.YPoint1_comm,
                                  2,
                                  "RQ1 susdurulmur"
                                    //Azb??

                                    );
                }
                else
                {
                    ClassMap.f_Sup_S(
                                  GlobalVarLn.XPoint1_comm,  // m
                                  GlobalVarLn.YPoint1_comm,
                                  2,
                                  "СС1 not jammed"
                                    //Azb??

                                    );
                }


                GlobalVarLn.fl_NoSup1 = 1;

                if (GlobalVarLn.Numb_CommPowerAvail == 1)
                {
                    ClassMap.f_Sup_Circle(
                                  GlobalVarLn.XPoint1_comm,  // m
                                  GlobalVarLn.YPoint1_comm,
                                  GlobalVarLn.R_CommPA
                                    );

                    return;
                }
            } // iMaxDistance_comm < iDistJammerComm1
            // ...........................................................
            if ((GlobalVarLn.Numb_CommPowerAvail == 2) && (iMaxDistance_comm < iDistJammerComm2))
            {

                if (GlobalVarLn.fl_Azb == 0)
                {
                    ClassMap.f_Sup_S(
                                  GlobalVarLn.XPoint2_comm,  // m
                                  GlobalVarLn.YPoint2_comm,
                                  2,
                                  "УС2 не подавляется"
                                    //Azb???
                                    );
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    ClassMap.f_Sup_S(
                                  GlobalVarLn.XPoint2_comm,  // m
                                  GlobalVarLn.YPoint2_comm,
                                  2,
                                  "RQ2 susdurulmur"
                                    //Azb???
                                    );
                }
                else
                {
                    ClassMap.f_Sup_S(
                                  GlobalVarLn.XPoint2_comm,  // m
                                  GlobalVarLn.YPoint2_comm,
                                  2,
                                  "СС2 not jammed"
                                    //Azb???
                                    );
                }

                GlobalVarLn.fl_NoSup2 = 1;

            } // iMaxDistance_comm < iDistJammerComm1

            // ...........................................................

            // Gamma
            dGamma_comm = DefineGamma_Comm(i_PolarOpponent_comm);
            //tbGamma.Text = dGamma_comm.ToString();

            // Dsp_ys1
            //liRadiusZone_comm = DefineRadiusZone_Comm();
            tbRadiusZone.Text = iDistJammerComm1.ToString();

            // RESULT_Suppression
            blResultSupress = false;
            blResultSupress2 = false;

            if (GlobalVarLn.fl_NoSup1 == 0)
            {
                blResultSupress = DefineCommSupress_Comm();
                if (blResultSupress == true)
                    //tbResult.Text = "УС подавляется";
                    GlobalVarLn.fl_NoSup1 = 0;
                else
                    //tbResult.Text = "УС не подавляется";
                    GlobalVarLn.fl_NoSup1 = 1;
            }

            if (GlobalVarLn.fl_NoSup2 == 0)
            {
                blResultSupress2 = DefineCommSupress_Comm2();
                if (blResultSupress2 == true)
                    //tbResult.Text = "УС подавляется";
                    GlobalVarLn.fl_NoSup2 = 0;
                else
                    //tbResult.Text = "УС не подавляется";
                    GlobalVarLn.fl_NoSup2 = 1;
            }

            GlobalVarLn.fl_ZSupYS = 1;
            ClassMap.f_Map_Redraw_CommPowerAvail1();
            // ************************************************************ Расчет зоны


        } // Принять
        // ************************************************************************

        // FUNCTIONS MY ***********************************************************


        // DDD
        // ************************************************************************
        // функция выбора системы координат SP
        // ************************************************************************

        private void ChooseSystemCoord_Comm(int iSystemCoord)
        {

            gbOwnRect.Visible = false;
            gbOwnRect42.Visible = false;
            gbOwnRad.Visible = false;
            gbOwnDegMin.Visible = false;
            gbOwnDegMinSec.Visible = false;
            gbPt1Rect.Visible = false;
            gbPt1Rect42.Visible = false;
            gbPt1Rad.Visible = false;
            gbPt1DegMin.Visible = false;
            gbPt1DegMinSec.Visible = false;
            gbPt2Rect.Visible = false;
            gbPt2Rect42.Visible = false;
            gbPt2Rad.Visible = false;
            gbPt2DegMin.Visible = false;
            gbPt2DegMinSec.Visible = false;


            switch (iSystemCoord)
            {
                case 0: // Метры на местности

                    // SP
                    gbOwnRect.Visible = true;
                    gbOwnRect.Location = new Point(8, 26);
                    // YS1
                    gbPt1Rect.Visible = true;
                    gbPt1Rect.Location = new Point(6, 11);
                    // YS2
                    gbPt2Rect.Visible = true;
                    gbPt2Rect.Location = new Point(6, 11);

                    // SP
                    if (GlobalVarLn.flCoordSP_comm == 1)
                    {
                        //333
                        //ichislo = (long)(GlobalVarLn.XCenter_comm);
                        //tbXRect.Text = Convert.ToString(ichislo);
                        //ichislo = (long)(GlobalVarLn.YCenter_comm);
                        //tbYRect.Text = Convert.ToString(ichislo);

                        ichislo = (long)(GlobalVarLn.BWGS84_1_comm2 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbXRect.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LWGS84_1_comm2 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbYRect.Text = Convert.ToString(dchislo);


                    } // IF

                    // YS1
                    if (GlobalVarLn.flCoordYS1_comm == 1)
                    {
                        //333
                        //ichislo = (long)(GlobalVarLn.XPoint1_comm);
                        //tbPt1XRect.Text = Convert.ToString(ichislo);
                        //ichislo = (long)(GlobalVarLn.YPoint1_comm);
                        //tbPt1YRect.Text = Convert.ToString(ichislo);

                        ichislo = (long)(GlobalVarLn.BWGS84_2_comm2 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1XRect.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LWGS84_2_comm2 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1YRect.Text = Convert.ToString(dchislo);

                    }
                    // YS2
                    if (GlobalVarLn.flCoordYS2_comm == 1)
                    {
                        //333
                        //ichislo = (long)(GlobalVarLn.XPoint2_comm);
                        //tbPt2XRect.Text = Convert.ToString(ichislo);
                        //ichislo = (long)(GlobalVarLn.YPoint2_comm);
                        //tbPt2YRect.Text = Convert.ToString(ichislo);

                        ichislo = (long)(GlobalVarLn.BWGS84_3_comm2 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt2XRect.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LWGS84_3_comm2 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt2YRect.Text = Convert.ToString(dchislo);

                    }

                    break;

                case 1: // Метры 1942 года

                    // SP
                    gbOwnRect42.Visible = true;
                    gbOwnRect42.Location = new Point(8, 27);
                    // YS1
                    gbPt1Rect42.Visible = true;
                    gbPt1Rect42.Location = new Point(6, 11);
                    // YS2
                    gbPt2Rect42.Visible = true;
                    gbPt2Rect42.Location = new Point(6, 11);

                    // SP
                    if (GlobalVarLn.flCoordSP_comm == 1)
                    {
                        ichislo = (long)(GlobalVarLn.XSP42_comm1);
                        tbXRect42.Text = Convert.ToString(ichislo);
                        ichislo = (long)(GlobalVarLn.YSP42_comm1);
                        tbYRect42.Text = Convert.ToString(ichislo);

                    } // IF
                    // YS1
                    if (GlobalVarLn.flCoordYS1_comm == 1)
                    {
                        ichislo = (long)(GlobalVarLn.XYS142_comm1);
                        tbPt1XRect42.Text = Convert.ToString(ichislo);
                        ichislo = (long)(GlobalVarLn.YYS142_comm1);
                        tbPt1YRect42.Text = Convert.ToString(ichislo);

                    }
                    // YS2
                    if (GlobalVarLn.flCoordYS2_comm == 1)
                    {
                        ichislo = (long)(GlobalVarLn.XYS242_comm1);
                        tbPt2XRect42.Text = Convert.ToString(ichislo);
                        ichislo = (long)(GlobalVarLn.YYS242_comm1);
                        tbPt2YRect42.Text = Convert.ToString(ichislo);

                    }

                    break;

                case 2: // Радианы (Красовский)

                    // SP
                    gbOwnRad.Visible = true;
                    gbOwnRad.Location = new Point(8, 27);
                    // YS1
                    gbPt1Rad.Visible = true;
                    gbPt1Rad.Location = new Point(6, 11);
                    // YS2
                    gbPt2Rad.Visible = true;
                    gbPt2Rad.Location = new Point(6, 11);

                    // SP
                    if (GlobalVarLn.flCoordSP_comm == 1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrR_comm1 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBRad.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrR_comm1 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLRad.Text = Convert.ToString(dchislo);


                    } // IF
                    // YS1
                    if (GlobalVarLn.flCoordYS1_comm == 1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrR_YS1_comm1 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1BRad.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrR_YS1_comm1 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1LRad.Text = Convert.ToString(dchislo);

                    }
                    // YS2
                    if (GlobalVarLn.flCoordYS2_comm == 1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrR_YS2_comm1 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt2BRad.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrR_YS2_comm1 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt2LRad.Text = Convert.ToString(dchislo);

                    }

                    break;

                case 3: // Градусы (Красовский)

                    // SP
                    gbOwnDegMin.Visible = true;
                    gbOwnDegMin.Location = new Point(8, 27);
                    // YS1
                    gbPt1DegMin.Visible = true;
                    gbPt1DegMin.Location = new Point(6, 11);
                    // YS2
                    gbPt2DegMin.Visible = true;
                    gbPt2DegMin.Location = new Point(6, 11);

                    // SP
                    if (GlobalVarLn.flCoordSP_comm == 1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrG_comm1 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBMin1.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrG_comm1 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLMin1.Text = Convert.ToString(dchislo);


                    } // IF
                    // YS1
                    if (GlobalVarLn.flCoordYS1_comm == 1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrG_YS1_comm1 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1BMin1.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrG_YS1_comm1 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1LMin1.Text = Convert.ToString(dchislo);

                    }
                    // YS2
                    if (GlobalVarLn.flCoordYS2_comm == 1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrG_YS2_comm1 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt2BMin1.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrG_YS2_comm1 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt2LMin1.Text = Convert.ToString(dchislo);

                    }

                    break;

                case 4: // Градусы,мин,сек (Красовский)

                    // SP
                    gbOwnDegMinSec.Visible = true;
                    gbOwnDegMinSec.Location = new Point(8, 27);
                    // YS1
                    gbPt1DegMinSec.Visible = true;
                    gbPt1DegMinSec.Location = new Point(6, 11);
                    // YS2
                    gbPt2DegMinSec.Visible = true;
                    gbPt2DegMinSec.Location = new Point(6, 11);

                    // SP
                    if (GlobalVarLn.flCoordSP_comm == 1)
                    {
                        tbBDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_comm1);
                        tbBMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_comm1);
                        ichislo = (long)(GlobalVarLn.Lat_Sec_comm1);
                        tbBSec.Text = Convert.ToString(ichislo);
                        tbLDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_comm1);
                        tbLMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_comm1);
                        ichislo = (long)(GlobalVarLn.Long_Sec_comm1);
                        tbLSec.Text = Convert.ToString(ichislo);


                    } // IF
                    // YS1
                    if (GlobalVarLn.flCoordYS1_comm == 1)
                    {
                        tbPt1BDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_YS1_comm1);
                        tbPt1BMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_YS1_comm1);
                        ichislo = (long)(GlobalVarLn.Lat_Sec_YS1_comm1);
                        tbPt1BSec.Text = Convert.ToString(ichislo);
                        tbPt1LDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_YS1_comm1);
                        tbPt1LMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_YS1_comm1);
                        ichislo = (long)(GlobalVarLn.Long_Sec_YS1_comm1);
                        tbPt1LSec.Text = Convert.ToString(ichislo);

                    }
                    // YS2
                    if (GlobalVarLn.flCoordYS2_comm == 1)
                    {
                        tbPt2BDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_YS2_comm1);
                        tbPt2BMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_YS2_comm1);
                        ichislo = (long)(GlobalVarLn.Lat_Sec_YS2_comm1);
                        tbPt2BSec.Text = Convert.ToString(ichislo);
                        tbPt2LDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_YS2_comm1);
                        tbPt2LMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_YS2_comm1);
                        ichislo = (long)(GlobalVarLn.Long_Sec_YS2_comm1);
                        tbPt2LSec.Text = Convert.ToString(ichislo);

                    }

                    break;

                default:
                    break;

            } // SWITCH


        } // ChooseSystemCoord_Comm
        // ************************************************************************


        // *********************************************************** FUNCTIONS MY


        // FUNCTIONS_ZONE *********************************************************


        // ************************************************************************
        // Функция определения средней высота местности
        // ************************************************************************

        private int DefineMiddleHeight_Comm(Point tpReferencePoint, axMapPoint axMapPointTemp, AxaxcMapScreen AxaxcMapScreenTemp)
        {
            int iRadius = 30000;

            int iStep = 100;
            int iCount = 0;
            double dMiddleHeightStep = 0;
            int iMiddleHeight = 0;

            if ((tpReferencePoint.X > 0) & (tpReferencePoint.Y > 0))
            {
                int iMinX = 0;
                int iMinY = 0;
                int iMaxX = 0;
                int iMaxY = 0;

                iMinX = tpReferencePoint.X - iRadius;
                iMinY = tpReferencePoint.Y - iRadius;
                iMaxX = tpReferencePoint.X + iRadius;
                iMaxY = tpReferencePoint.Y + iRadius;

                // пройти по координатам карты с шагом Shag
                for (int i = iMinX; i < iMaxX; i = i + iStep)
                {
                    for (int j = iMinY; j < iMaxY; j = j + iStep)
                    {
                        double dSetX = 0;
                        double dSetY = 0;
                        dSetX = i;
                        dSetY = j;

                        axMapPointTemp.SetPoint(dSetX, dSetY);
                        dMiddleHeightStep = AxaxcMapScreenTemp.PointHeight_get(axMapPointTemp);

                        // 6_9_18
                        if (dMiddleHeightStep < 0)
                        {
                            dMiddleHeightStep = 0;
                        }

                        // увеличить счетчик на 1
                        iCount++;

                        // суммировать высоты
                        iMiddleHeight = (int)((double)iMiddleHeight + dMiddleHeightStep);

                    }
                }

                // средняя высота = сумма всех полученных высот/на кол-во пройденных точек     
                iMiddleHeight = (int)((double)iMiddleHeight / (double)iCount);

                if (iMiddleHeight < 0)
                    iMiddleHeight = 0;

            } // IF

            return iMiddleHeight;

        } // MiddleHeight
        // ************************************************************************

        // ************************************************************************
        // Kq
        // ************************************************************************
        private double DefineCoeffQ_Comm(double dFreq, int iCodeSurface)
        {
            double dCoeffQ = 0;

            double dLambda = 0;
            double dEpsilon = 0;
            double dSigma = 0;

            dLambda = LAMBDA / dFreq;

            switch (iCodeSurface)
            {
                case 0:
                    dEpsilon = 4;
                    dSigma = 0.001;
                    break;

                case 1:
                    dEpsilon = 10;
                    dSigma = 0.01;
                    break;

                case 2:
                    dEpsilon = 80;
                    dSigma = 0.001;
                    break;

                case 3:
                    dEpsilon = 80;
                    dSigma = 4;
                    break;

                case 4:
                    dEpsilon = 7;
                    dSigma = 0.001;
                    break;
            }

            dCoeffQ = Math.Sqrt((dEpsilon - 1) * (dEpsilon - 1) + (60 * dLambda * dSigma) * (60 * dLambda * dSigma)) /
                                          (dEpsilon * dEpsilon + (60 * dLambda * dSigma) * (60 * dLambda * dSigma));

            return dCoeffQ;

        } // Kq
        // ************************************************************************

        // ************************************************************************
        // функция определения коэффициента CoeffHE
        // ************************************************************************

        private double DefineCoeffHE_Comm(double dFreq, double dCoeffQ)
        {
            double dCoeffHE = 0;
            double dLambda = 0;

            dLambda = LAMBDA / dFreq;

            dCoeffHE = dLambda * dLambda / (4 * Math.PI * Math.PI * dCoeffQ);

            return dCoeffHE;

        } // K_HE
        // ************************************************************************

        // ************************************************************************
        // функция определения скорректированной высоты средства подавления для расчета
        // (эффективная высота)
        // ************************************************************************
        private int DefineCorrectHeightOwn_Comm(int iHeightAntennOwn, double dCoeffHE)
        {
            int iTotalHeightOwn = 0;

            iTotalHeightOwn = (int)(Math.Sqrt(iHeightAntennOwn * iHeightAntennOwn + dCoeffHE * dCoeffHE));

            return iTotalHeightOwn;
        }
        // ************************************************************************

        // ************************************************************************
        // Hsp=Hant+Hrel
        // ************************************************************************
        private int DefineResultHeightOwn_Comm(int iTotalHeightOwn, int iHeightPlaceOwn)
        {
            int iResultHeightOwn = 0;

            //iResultHeightOwn = iTotalHeightOwn+iHeightPlaceOwn;

            iResultHeightOwn = (int)HeightAntennOwn_comm + iHeightPlaceOwn;

            return iResultHeightOwn;

        } // H_TotalSP
        // ************************************************************************

        // ************************************************************************
        // MIN from  Hrel_SP and Hmiddle
        // ************************************************************************
        private int DefineMinHeight_Comm(Point tpReferencePoint, int iMiddleHeight)
        {
            int iMinHeight = 0;

            if ((tpReferencePoint.X > 0) & (tpReferencePoint.Y > 0))
            {
                double dSetX = 0;
                double dSetY = 0;

                int iHeightRefPoint = 0;

                dSetX = tpReferencePoint.X;
                dSetY = tpReferencePoint.Y;

                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                iHeightRefPoint = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                iMinHeight = Math.Min(iHeightRefPoint, iMiddleHeight);
            }

            return iMinHeight;
        } // Hkorr_SP
        // ************************************************************************

        // ************************************************************************
        // Эффективная высота линии подавления
        // ************************************************************************
        private int DefineCorrectHeightOpponent_Comm(int iHeightOpponent, double dCoeffHE) //  не знаю iHeightOpponent  edit 9
        {
            int iCorrectHeightOpponent = 0;

            iCorrectHeightOpponent = (int)(Math.Sqrt(iHeightOpponent * iHeightOpponent + dCoeffHE * dCoeffHE));

            return iCorrectHeightOpponent;

        }  // Hkorr_OP
        // ************************************************************************

        // ************************************************************************
        // Hop=Hant_PRM+Hmiddle
        // ************************************************************************

        private int DefineResultHeightOpponent_Comm(int iCorrectHeightOpponent, int iMiddleHeight)
        {
            int iiResultHeightOpponent = 0;

            ////iResultHeightOpponent = iCorrectHeightOpponent+iMiddleHeight;
            //iiResultHeightOpponent = dHeightReceiverOpponent+iMiddleHeight;

            iiResultHeightOpponent = (int)HeightReceiverOpponent_comm + iMiddleHeight;


            return iiResultHeightOpponent;

        } // Hop=Hant_PRM+Hmiddle
        // ************************************************************************

        // ************************************************************************
        // ДПВ
        // ************************************************************************
        private long DefineMaxDistance_Comm(int iResultHeightOwn, int iResultHeightOpponent, int iMinHeight)
        {
            int iMaxDistance = 0;

            double dRes1 = 0;
            double dRes2 = 0;

            dRes1 = iResultHeightOwn - iMinHeight;
            dRes2 = iResultHeightOpponent - iMinHeight;

            iMaxDistance = (int)(Math.Sqrt(dRes1) + Math.Sqrt(dRes2));

            iMaxDistance = (int)(4.12 * iMaxDistance * 1000);

            return iMaxDistance;

        } // ДПВ
        // ************************************************************************

        // ************************************************************************
        // функция определения расстояния
        // ************************************************************************
        private int DefineDistance_Comm(Point tpPoint1Coord, Point tpPoint2Coord)
        {
            double dDistance = 0;

            double dDist1 = 0;
            double dDist2 = 0;

            dDist1 = ((double)tpPoint1Coord.X - (double)tpPoint2Coord.X) *
                    ((double)tpPoint1Coord.X - (double)tpPoint2Coord.X);
            dDist2 = ((double)tpPoint1Coord.Y - (double)tpPoint2Coord.Y) *
                     ((double)tpPoint1Coord.Y - (double)tpPoint2Coord.Y);

            dDistance = dDist1 + dDist2;

            dDistance = Math.Sqrt(dDistance);

            return (int)dDistance;

        } // D
        // ************************************************************************

        // ************************************************************************
        // Gamma
        // ************************************************************************
        private double DefineGamma_Comm(int iCodePolarOpponent)
        {
            double dGamma = 0;

            switch (iCodePolarOpponent)
            {
                case 0:
                    dGamma = 1;
                    break;

                case 1:
                    dGamma = 0;
                    break;

                case 2:
                    dGamma = 0.5;
                    break;

                case 3:
                    dGamma = 0.5;
                    break;

            }
            return dGamma;

        } // Gamma
        // ************************************************************************

        // ************************************************************************
        // определение результата подавления/неподавления УС
        // ************************************************************************
        private bool DefineCommSupress_Comm()
        {

            bool blResult = false;

            double dLambda = 0;
            dLambda = LAMBDA / Freq_comm;

            ////////////////////////
            // old
            //double dAddWeakHindrance = 0;
            //dAddWeakHindrance = 4*sin((2*M_PI*iCorrectHeightOwn*iCorrectHeightOpponent)/(dLambda*iDistBetweenComm))*sin((2*M_PI*iCorrectHeightOwn*iCorrectHeightOpponent)/(dLambda*iDistBetweenComm));

            double dAddWeakHindranceTwo = 0;
            double dCoeffK = 0;


            //dAddWeakHindranceTwo = 0;
            //dAddWeakHindranceTwo = 4*sin((2*M_PI*iCorrectHeightOwn*iCorrectHeightOpponent)/(dLambda*iDistJammerComm1))*sin((2*M_PI*iCorrectHeightOwn*iCorrectHeightOpponent)/(dLambda*iDistJammerComm1));
            //dCoeffK = (iPowerOwn*iCoeffOwn*iDistBetweenComm*iDistBetweenComm*dAddWeakHindranceTwo*iWidthSignal*1000*dGamma)/
            //(iPowerOpponent*iCoeffTransmitOpponent*iDistJammerComm1*iDistJammerComm1*dAddWeakHindranceTwo*dWidthHindrance);


            // new
            double dCorrectHeightOppTransmit = 0;
            dCorrectHeightOppTransmit = Math.Sqrt(HeightTransmitOpponent_comm * HeightTransmitOpponent_comm + dCoeffHE_comm * dCoeffHE_comm);

            double dAddWeakHindrance = 0;
            dAddWeakHindrance = 4 * Math.Sin((2 * Math.PI * dCorrectHeightOppTransmit * Convert.ToDouble(iCorrectHeightOpponent_comm)) /
                              (dLambda * Convert.ToDouble(iDistBetweenComm))) * Math.Sin((2 * Math.PI * dCorrectHeightOppTransmit *
                              Convert.ToDouble(iCorrectHeightOpponent_comm)) / (dLambda * Convert.ToDouble(iDistBetweenComm)));


            double dCoeffKConst = 0;
            double dVAr = 0;
            double dVAr1 = 0;

            if (WidthSignal_comm >= WidthHindrance_comm)
                dVAr1 = 1;
            else
                dVAr1 = WidthSignal_comm / WidthHindrance_comm;

            dCoeffKConst = (PowerOwn_comm * CoeffOwn_comm * dGamma_comm * dVAr1) /
                (PowerOpponent_comm * CoeffTransmitOpponent_comm);

            dAddWeakHindranceTwo = 0;
            dAddWeakHindranceTwo = 4 * Math.Sin((2 * Math.PI * Convert.ToDouble(iCorrectHeightOwn_comm) * Convert.ToDouble(iCorrectHeightOpponent_comm)) /
                (dLambda * Convert.ToDouble(iDistJammerComm1))) * Math.Sin((2 * Math.PI * Convert.ToDouble(iCorrectHeightOwn_comm) *
                Convert.ToDouble(iCorrectHeightOpponent_comm)) / (dLambda * Convert.ToDouble(iDistJammerComm1)));

            dVAr = dAddWeakHindranceTwo / dAddWeakHindrance;

            double dCoeffK1 = 0;

            dCoeffK1 = (RangeComm_comm * RangeComm_comm) / (Convert.ToDouble(iDistJammerComm1 * iDistJammerComm1));
            dCoeffK = dCoeffK1 * dCoeffKConst * dVAr;

            ////////////////////////

            if (dCoeffK >= CoeffSupOpponent_comm)
                blResult = true;
            else
                blResult = false;


            ichislo = (long)(dCoeffK * 100);
            dchislo = ((double)ichislo) / 100;
            tbHindSignal.Text = Convert.ToString(dchislo);
            //tbHindSignal.Text = dCoeffK.ToString();

            return blResult;

        } // RESULT_POD_YS
        // ************************************************************************

        // ************************************************************************
        // определение результата подавления/неподавления УС
        // ************************************************************************
        private bool DefineCommSupress_Comm2()
        {

            bool blResult = false;

            double dLambda = 0;
            dLambda = LAMBDA / Freq_comm;

            ////////////////////////
            // old
            //double dAddWeakHindrance = 0;
            //dAddWeakHindrance = 4*sin((2*M_PI*iCorrectHeightOwn*iCorrectHeightOpponent)/(dLambda*iDistBetweenComm))*sin((2*M_PI*iCorrectHeightOwn*iCorrectHeightOpponent)/(dLambda*iDistBetweenComm));

            double dAddWeakHindranceTwo = 0;
            double dCoeffK = 0;

            // new
            double dCorrectHeightOppTransmit = 0;
            dCorrectHeightOppTransmit = Math.Sqrt(HeightTransmitOpponent_comm * HeightTransmitOpponent_comm + dCoeffHE_comm * dCoeffHE_comm);

            double dAddWeakHindrance = 0;
            dAddWeakHindrance = 4 * Math.Sin((2 * Math.PI * dCorrectHeightOppTransmit * Convert.ToDouble(iCorrectHeightOpponent_comm)) /
                              (dLambda * Convert.ToDouble(iDistBetweenComm))) * Math.Sin((2 * Math.PI * dCorrectHeightOppTransmit *
                              Convert.ToDouble(iCorrectHeightOpponent_comm)) / (dLambda * Convert.ToDouble(iDistBetweenComm)));


            double dCoeffKConst = 0;
            double dVAr = 0;
            double dVAr1 = 0;

            if (WidthSignal_comm >= WidthHindrance_comm)
                dVAr1 = 1;
            else
                dVAr1 = WidthSignal_comm / WidthHindrance_comm;

            dCoeffKConst = (PowerOwn_comm * CoeffOwn_comm * dGamma_comm * dVAr1) /
                (PowerOpponent_comm * CoeffTransmitOpponent_comm);

            dAddWeakHindranceTwo = 0;
            dAddWeakHindranceTwo = 4 * Math.Sin((2 * Math.PI * Convert.ToDouble(iCorrectHeightOwn_comm) * Convert.ToDouble(iCorrectHeightOpponent_comm)) /
                (dLambda * Convert.ToDouble(iDistJammerComm1))) * Math.Sin((2 * Math.PI * Convert.ToDouble(iCorrectHeightOwn_comm) *
                Convert.ToDouble(iCorrectHeightOpponent_comm)) / (dLambda * Convert.ToDouble(iDistJammerComm2)));

            dVAr = dAddWeakHindranceTwo / dAddWeakHindrance;

            double dCoeffK1 = 0;

            dCoeffK1 = (RangeComm_comm * RangeComm_comm) / (Convert.ToDouble(iDistJammerComm2 * iDistJammerComm2));
            dCoeffK = dCoeffK1 * dCoeffKConst * dVAr;

            ////////////////////////

            if (dCoeffK >= CoeffSupOpponent_comm)
                blResult = true;
            else
                blResult = false;


            ichislo = (long)(dCoeffK * 100);
            dchislo = ((double)ichislo) / 100;
            tbHindSignal2.Text = Convert.ToString(dchislo);
            //tbHindSignal.Text = dCoeffK.ToString();

            return blResult;

        } // RESULT_POD_YS2
          // ************************************************************************

        // ********************************************************* FUNCTIONS_ZONE


        // ************************************************************************
        /*
        private long DefineRadiusZone_Comm()
        {
            double dResult = 0;
            int iStepDist = 0;
            iStepDist = 50;
            double dLambda = 0;
            dLambda = LAMBDA / Freq_comm;

            double dCorrectHeightOppTransmit = 0;
            dCorrectHeightOppTransmit = Math.Sqrt(HeightTransmitOpponent_comm * HeightTransmitOpponent_comm + dCoeffHE_comm * dCoeffHE_comm);

            double dAddWeakHindrance = 0;
            dAddWeakHindrance = 4 * Math.Sin((2 * Math.PI * dCorrectHeightOppTransmit * iCorrectHeightOpponent_comm) /
                               (dLambda * RangeComm_comm)) * Math.Sin((2 * Math.PI * dCorrectHeightOppTransmit * iCorrectHeightOpponent_comm) /
                               (dLambda * RangeComm_comm));

            double dAddWeakHindranceTwo = 0;

            double dCoeffK = 0;

            double dCoeffKConst = 0;

            double dVAr = 0;
            double dVAr1 = 0;

            if (WidthSignal_comm >= WidthHindrance_comm)
                dVAr1 = 1;
            else
                dVAr1 = Convert.ToDouble(WidthSignal_comm) / Convert.ToDouble(WidthHindrance_comm);

            dCoeffKConst = Convert.ToDouble((PowerOwn_comm * CoeffOwn_comm * dGamma_comm * dVAr1)) /
                          (Convert.ToDouble(PowerOpponent_comm * CoeffTransmitOpponent_comm));

            long iMinDistance = 0;
            long i = (long)iMaxDistance_comm;
            while (i > iMinDistance)
            {
                dAddWeakHindranceTwo = 0;
                dAddWeakHindranceTwo = 4 * Math.Sin((2 * Math.PI * iCorrectHeightOwn_comm * iCorrectHeightOpponent_comm) /
                                      (dLambda * i)) * Math.Sin((2 * Math.PI * iCorrectHeightOwn_comm * iCorrectHeightOpponent_comm) / (dLambda * i));

                dVAr = dAddWeakHindranceTwo / dAddWeakHindrance;

                double dCoeffK1 = 0;

                dCoeffK1 = (Convert.ToDouble(RangeComm_comm * RangeComm_comm)) / (Convert.ToDouble(i * i));
                dCoeffK = dCoeffK1 * dCoeffKConst * dVAr;

                //dCoeffK = (iPowerOwn*iCoeffOwn*iRangeComm*iRangeComm*iWidthSignal*1000*dGamma*dVAr)/
                //(iPowerOpponent*iCoeffTransmitOpponent*i*i*dWidthHindrance);

                if (dCoeffK > CoeffSupOpponent_comm)
                {
                    dResult = 0;
                    dResult = i;
                    i = iMinDistance;
                }
                i -= iStepDist;
            }
            return (long)dResult;
        }
         */
        // ************************************************************************


        // ************************************************************************
        // Closing, Activated
        // ************************************************************************
        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            GlobalVarLn.f_Open_ObjCommPowerAvail = 0;

            e.Cancel = true;
            Hide();

        } // Closing

        private void Form2_Activated(object sender, EventArgs e)
        {
            GlobalVarLn.f_Open_ObjCommPowerAvail = 1;

        } // Activated
          // ************************************************************************

        private void tbRadiusZone_TextChanged(object sender, EventArgs e)
        {
            ;
        }
        private void gbPt1Rect42_Enter(object sender, EventArgs e)
        {
            ;
        }
        private void tbPt1BMin2_TextChanged(object sender, EventArgs e)
        {
            ;
        }
        private void gbPt2DegMin_Enter(object sender, EventArgs e)
        {

        }

        private void cbWidthHindrance_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void LanguageChooser()
        {
            var cont = this.Controls;

            if (NumberOfLanguage.Equals(0))
            {
                ChangeLanguageToRu(cont);
                this.Text = "Расчет энергодоступности по узлам связи";
            }
            if (NumberOfLanguage.Equals(1))
            {
                ChangeLanguageToEng(cont);
            }
            if (NumberOfLanguage.Equals(2))
            {
                ChangeLanguageToAzer(cont);
                this.Text = "Qovşaqlar üzrə qidalandırılmanın hesabatı";
            }
        }

        private void ChangeLanguageToEng(System.Windows.Forms.Control.ControlCollection cont)
        {
            this.Text = "Calculation of energy availability by communication centers(СС)";
            bAccept.Text = "Accept";
            bClear.Text = "Clear";
            button1.Text = "JS";
            button2.Text = "CC1";
            button3.Text = "CC2";
            cbCenterLSR.Items.Clear();
            cbCenterLSR.Items.Add("JS");
            cbCenterLSR.Items.Add("Mated JS");
            cbCenterLSR.Items.Add("Control post");
            cbChooseSC.Items.Clear();
            cbChooseSC.Items.Add("Degrees(WGS84)");
            cbChooseSC.Items.Add("Meters(Gauss-Krueger)");
            cbChooseSC.Items.Add("Radians(CS42)");
            cbChooseSC.Items.Add("Degrees(CS42)");
            cbChooseSC.Items.Add("Deg, min, sec(CS42)");
            cbPolarOpponent.Items.Clear();
            cbPolarOpponent.Items.Add("Linear vertical");
            cbPolarOpponent.Items.Add("Linear horizontal");
            cbPolarOpponent.Items.Add("Circular right");
            cbPolarOpponent.Items.Add("Circular left");
            cbSurface.Items.Clear();
            cbSurface.Items.Add("Dry soil");
            cbSurface.Items.Add("Wet soil");
            cbSurface.Items.Add("Fresh water");
            cbSurface.Items.Add("Sea water");
            cbSurface.Items.Add("Forest");
            gbPoint1.Text = "CC 1";
            gbPoint2.Text = " CC 2";
            label13.Text = "Range LOS, m";
            label14.Text = "K(jamming/ signal) CC1";
            label17.Text = "Antenna height, m....................";
            label18.Text = "J / S ratio...................................";
            label19.Text = "K(jamming / signal) CC2";
            label20.Text = "polarization";
            label21.Text = "centers";
            label37.Text = "Average height, m";
            label38.Text = "Center of zone";
            lBRad.Text = "B, rad...................";
            lChooseSC.Text = "CS";
            lCoeffOwn.Text = "Gain...........................................";
            lCoeffTransmitOpponent.Text = "Transmitter antenna gain ..........";
            lCountOpponent.Text = "Number of communication";
            lFreq.Text = "Carrier frequency, KHz................";
            lHeightOwnObject.Text = "Total height, m.........................";
            lHeightReceiverOpponent.Text = "Receiver antenna, m..........";
            lHeightTransmitOpponent.Text = "Transmitter antenna, m.......";
            lLRad.Text = "L, rad...................";
            lOwnHeight.Text = "H, m...................";
            lPolarOpponent.Text = "Signal";
            lPowerOpponent.Text = "Power, W...................................";
            lPowerOwn.Text = "Power, W..................................";
            lPt1BRad.Text = "B, rad...................";
            lPt1Height.Text = "H, m.............................";
            lPt1LRad.Text = "L, rad...................";
            lPt1XRect42.Text = "X, m...................";
            lPt1YRect42.Text = "Y, m...................";
            lPt2BRad.Text = "B, rad...................";
            lPt2Height.Text = "H, m.............................";
            lPt2LRad.Text = "L, rad...................";
            lPt2XRect42.Text = "X, m...................";
            lPt2YRect42.Text = "Y, m...................";
            lRadiusZone.Text = "Range of jam, m";
            lRangeComm.Text = "Communication range, m....";
            lSurface.Text = "Underlying surface";
            lWidthHindrance.Text = "Jamming spectrum width, KHz....";
            lWidthSignal.Text = "Spectrum width, KHz..........";
            lXRect42.Text = "X, m...................";
            lYRect42.Text = "Y, m...................";
            tabPage1.Text = "CC coordinates";
            tpOpponentObject.Text = "CC parameters";
            tpOwnObject.Text = "Jammer station(JS)";
        }

        private void ChangeLanguageToRu(System.Windows.Forms.Control.ControlCollection cont)
        {
            string lang = "ru-RU";
            foreach (System.Windows.Forms.Control cc in cont)
            {
                if (cc is ComboBox)
                {

                    for (int i = 0; i < (cc as ComboBox).Items.Count; i++)
                    {
                        if (i == 0)
                        {
                            string a = resources.GetString(cc.Name + ".Items", new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                        else
                        {
                            string a = resources.GetString(cc.Name + ".Items" + i.ToString(), new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                    }
                }
                //}
                if (cc is Label || cc is Button || cc is RadioButton || cc is TabControl || cc is TabPage || cc is GroupBox)
                {
                    resources.ApplyResources(cc, cc.Name, new CultureInfo(lang));
                    //toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
                }
                if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl || cc is TabPage)
                {
                    ChangeLanguageToRu(cc.Controls);
                }
            }
        }

        private void ChangeLanguageToAzer(System.Windows.Forms.Control.ControlCollection cont)
        {
            string lang = "az-Latn";

            foreach (System.Windows.Forms.Control cc in cont)
            {
                if (cc is ComboBox)
                {

                    for (int i = 0; i < (cc as ComboBox).Items.Count; i++)
                    {
                        if (i == 0)
                        {
                            string a = resources.GetString(cc.Name + ".Items", new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                        else
                        {
                            string a = resources.GetString(cc.Name + ".Items" + i.ToString(), new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                    }
                }
                //}
                if (cc is Label || cc is Button || cc is RadioButton || cc is TabControl || cc is TabPage || cc is GroupBox)
                {
                    resources.ApplyResources(cc, cc.Name, new CultureInfo(lang));
                    //toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
                }
                if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl || cc is DataGridView || cc is TabPage)
                {
                    ChangeLanguageToAzer(cc.Controls);
                }

            }
        }

    } // Class
} // Namespace
