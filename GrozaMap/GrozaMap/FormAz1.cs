﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using AxaxGisToolKit;
using System.Runtime.InteropServices;
using System.IO;
using System.Reflection;
using System.Globalization;

namespace GrozaMap
{
    public partial class FormAz1 : Form
    {

        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        private AxaxcMapScreen axaxcMapScreen;

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private double dchislo;
        private long ichislo;
        //private double LAMBDA;

        // .....................................................................
        // Координаты центра ЗПВ

        // Координаты центра ЗПВ на местности в м
        //private double XSP_comm;
        //private double YSP_comm;

/*
        // DATUM
        private double dXdat_comm;
        private double dYdat_comm;
        private double dZdat_comm;

        private double dLat_comm;
        private double dLong_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_comm;
        private double LongKrG_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_comm;
        private double LongKrR_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_comm;
        private int Lat_Min_comm;
        private double Lat_Sec_comm;
        private int Long_Grad_comm;
        private int Long_Min_comm;
        private double Long_Sec_comm;
        // Гаусс-крюгер(СК42) м
        private double XSP42_comm;
        private double YSP42_comm;
*/
        // Конструктор *********************************************************** 

        ComponentResourceManager resources = new ComponentResourceManager(typeof(FormAz1));
        private int NumberOfLanguage;

        public FormAz1(ref AxaxcMapScreen axaxcMapScreen1)
        {
            InitializeComponent();

           

           

            axaxcMapScreen = axaxcMapScreen1;

            dchislo = 0;
            ichislo = 0;
            //LAMBDA = 300000;

            // .....................................................................
            // Координаты центра ЗПВ

            // Координаты центра ЗПВ на местности в м
            //XSP_comm = 0;
            //YSP_comm = 0;

/*
            // DATUM
            // ГОСТ 51794_2008
            dXdat_comm = 25;
            dYdat_comm = -141;
            dZdat_comm = -80;

            dLat_comm = 0;
            dLong_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_comm = 0;
            LongKrG_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_comm = 0;
            LongKrR_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_comm = 0;
            Lat_Min_comm = 0;
            Lat_Sec_comm = 0;
            Long_Grad_comm = 0;
            Long_Min_comm = 0;
            Long_Sec_comm = 0;
            // Гаусс-крюгер(СК42) м
            XSP42_comm = 0;
            YSP42_comm = 0;
*/

        } // Конструктор
        // ***********************************************************  Конструктор

        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void FormAz1_Load(object sender, EventArgs e)
        {
            string strExePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            string PathFolder1 = Path.GetDirectoryName(strExePath);

            string strPathLatest = PathFolder1 + "\\INI\\Common.ini";
            foreach (string line in File.ReadLines(strPathLatest))
            {
                if (line.Contains("Language "))
                {
                    string LanguageAsNumber = line.Split(new string[] { "=" }, StringSplitOptions.None).Last();
                    NumberOfLanguage = Int32.Parse(LanguageAsNumber);
                    break;
                }
            }
            LanguageChooser();

            // ----------------------------------------------------------------------
            gbOwnRect.Visible = true;
            gbOwnRect.Location = new Point(11, 11);

            gbOwnRect42.Visible = false;
            gbOwnRad.Visible = false;
            gbOwnDegMin.Visible = false;
            gbOwnDegMinSec.Visible = false;

            cbChooseSC.SelectedIndex = 0;
            // ----------------------------------------------------------------------
            // Переменные

            GlobalVarLn.flCoord_Az1 = 0; // =1-> Выбрали object
            // ----------------------------------------------------------------------
            // Очистка dataGridView

            dataGridView1.ClearSelection();
            while (dataGridView1.Rows.Count != 0)
                dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

            if (GlobalVarLn.fl_Azb == 0)
            {
                dataGridView1.Rows.Add("АСП", "", "");
                dataGridView1.Rows.Add("АСПсопр", "", "");
                dataGridView1.Rows.Add("ПУ", "", "");

                dataGridView1.Columns[1].HeaderText = "X, м";
                dataGridView1.Columns[2].HeaderText = "Y, м";
                dataGridView1.Columns[3].HeaderText = "Азимут, град";
            }
            else if (GlobalVarLn.fl_Azb == 2)
            {
                //Azb???
                dataGridView1.Rows.Add("AMS", "", "");
                dataGridView1.Rows.Add("BG AMS", "", "");
                dataGridView1.Rows.Add("İM", "", "");

                dataGridView1.Columns[1].HeaderText = "X, m";
                dataGridView1.Columns[2].HeaderText = "Y, m";
                dataGridView1.Columns[3].HeaderText = "Azimut, dərəcə";
            }
            else 
            {
                //Azb???
                dataGridView1.Rows.Add("JS", "", "");
                dataGridView1.Rows.Add("JSmated", "", "");
                dataGridView1.Rows.Add("CP", "", "");

                dataGridView1.Columns[1].HeaderText = "X, m";
                dataGridView1.Columns[2].HeaderText = "Y, m";
                dataGridView1.Columns[3].HeaderText = "Azimuth, deg";
            }



            //Az???
            //dataGridView1.Rows.Add("MS", "", "");
            //dataGridView1.Rows.Add("АСПсопр", "", "");
            //dataGridView1.Rows.Add("ПУ", "", "");

            // ----------------------------------------------------------------------

        } // Load
        // ************************************************************************

        // ************************************************************************
        // Очистка
        // ************************************************************************        
        private void bClear_Click(object sender, EventArgs e)
        {

            ClassMap.ClearAz1();

        } // Clear
        // ************************************************************************        

        // ************************************************************************        
        // ACCEPT
        // ************************************************************************        
        private void button2_Click(object sender, EventArgs e)
        {

            double xtmp_ed, ytmp_ed, htmp_ed;
            double xtmp1_ed, ytmp1_ed;
            double X_Coordl3_1;
            double Y_Coordl3_1;
            double X_Coordl3_2;
            double Y_Coordl3_2;
            double DXX3;
            double DYY3;
            double azz;

            xtmp_ed = 0;
            ytmp_ed = 0;
            htmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;
            X_Coordl3_1=0;
            Y_Coordl3_1=0;
            X_Coordl3_2=0;
            Y_Coordl3_2=0;
            DXX3=0;
            DYY3=0;
            azz=0;

            // OBJECT ****************************************************************
            // Ввод объекта

            ClassMap objClassMap1_ed = new ClassMap();
            ClassMap objClassMap2_ed = new ClassMap();
            ClassMap objClassMap3_ed = new ClassMap();
            // ......................................................................
            // !!! реальные координаты на местности карты в м (Plane)

            GlobalVarLn.XCenter_Az1 = GlobalVarLn.MapX1;
            GlobalVarLn.YCenter_Az1 = GlobalVarLn.MapY1;
            // ......................................................................
            if ((GlobalVarLn.XCenter_Az1 == 0) || (GlobalVarLn.YCenter_Az1 == 0))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Не введен объект");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb???
                    MessageBox.Show("Hedef işarelenmiyib");

                }
                else 
                {
                    MessageBox.Show("Object is not entered");

                }

                return;
            }
            // ......................................................................
            xtmp_ed = GlobalVarLn.XCenter_Az1;
            ytmp_ed = GlobalVarLn.YCenter_Az1;
            // ......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(xtmp_ed, ytmp_ed);
            GlobalVarLn.HCenter_Az1 = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            htmp_ed = GlobalVarLn.HCenter_Az1;
            tbOwnHeight.Text = Convert.ToString(htmp_ed);
            // ......................................................................
            GlobalVarLn.flCoord_Az1 = 1; // object выбран
            // ......................................................................
            // Убрать с карты
            GlobalVarLn.axMapScreenGlobal.Repaint();
            // Redraw
            ClassMap.f_Rect_Az1(GlobalVarLn.XCenter_Az1, GlobalVarLn.YCenter_Az1);
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;

            //333
            GlobalVarLn.BWGS84_Az1 = xtmp1_ed;
            GlobalVarLn.LWGS84_Az1 = ytmp1_ed;
            // .......................................................................

            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLong
                (
                // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLong_comm_Az1   // приращение по долготе, угл.сек
                );
            // ................................................................ dLong

            // dLat .................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLat
                (
                // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLat_comm_Az1        // приращение по долготе, угл.сек
                );
            // ................................................................. dLat

            // Lat,Long .............................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objClassMap1_ed.f_WGS84_SK42_Lat_Long
                   (
                // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       GlobalVarLn.dLat_comm_Az1,       // приращение по долготе, угл.сек
                       GlobalVarLn.dLong_comm_Az1,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref GlobalVarLn.LatKrG_comm_Az1,   // широта
                       ref GlobalVarLn.LongKrG_comm_Az1   // долгота
                   );
            // ............................................................ Lat,Long


            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            GlobalVarLn.LatKrR_comm_Az1 = (GlobalVarLn.LatKrG_comm_Az1 * Math.PI) / 180;
            GlobalVarLn.LongKrR_comm_Az1 = (GlobalVarLn.LongKrG_comm_Az1 * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LatKrG_comm_Az1,

                // Выходные параметры 
                ref GlobalVarLn.Lat_Grad_comm_Az1,
                ref GlobalVarLn.Lat_Min_comm_Az1,
                ref GlobalVarLn.Lat_Sec_comm_Az1
              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LongKrG_comm_Az1,

                // Выходные параметры 
                ref GlobalVarLn.Long_Grad_comm_Az1,
                ref GlobalVarLn.Long_Min_comm_Az1,
                ref GlobalVarLn.Long_Sec_comm_Az1
              );
            // .......................................................................

            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            objClassMap3_ed.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       GlobalVarLn.LatKrG_comm_Az1,   // широта
                       GlobalVarLn.LongKrG_comm_Az1,  // долгота

                       // Выходные параметры (km)
                       ref GlobalVarLn.XSP42_comm_Az1,
                       ref GlobalVarLn.YSP42_comm_Az1
                   );

            // km->m
            GlobalVarLn.XSP42_comm_Az1 = GlobalVarLn.XSP42_comm_Az1 * 1000;
            GlobalVarLn.YSP42_comm_Az1 = GlobalVarLn.YSP42_comm_Az1 * 1000;
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отобразить координаты в окошках

            ClassMap.Otobr_Az1();
            // .......................................................................


           // **************************************************************** OBJECT

           // INI ******************************************************************
            ClassMap objClassMap10 = new ClassMap();


            // SP1 _________________________________________________________________

            // ......................................................................
            // !!! реальные координаты на местности карты в м (Plane)

            //if (System.IO.File.Exists(Application.StartupPath + "\\Settings.ini"))
            //{
            //    GlobalVarLn.XSP1_Az1 = (double)iniRW.get_X_ASP();
            //    GlobalVarLn.YSP1_Az1 = (double)iniRW.get_Y_ASP();
            //}
            //else
           // {
           //     MessageBox.Show("Невозможно открыть INI файл");
           //     return;
           // }

            //777
            GlobalVarLn.XSP1_Az1 = GlobalVarLn.XCenter_Sost;
            GlobalVarLn.YSP1_Az1 = GlobalVarLn.YCenter_Sost;

            if ((GlobalVarLn.XSP1_Az1 == 0) || (GlobalVarLn.YSP1_Az1 == 0))
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    //MessageBox.Show("Нет координат АСП");
                    dataGridView1.Rows[0].Cells[1].Value = "Нет";
                    dataGridView1.Rows[0].Cells[2].Value = "Нет";
                    dataGridView1.Rows[0].Cells[3].Value = "Нет";
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb
                    dataGridView1.Rows[0].Cells[1].Value = "yoxdur"; 
                    dataGridView1.Rows[0].Cells[2].Value = "yoxdur"; 
                    dataGridView1.Rows[0].Cells[3].Value = "yoxdur"; 
                }
                else 
                {
                    //Azb
                    dataGridView1.Rows[0].Cells[1].Value = "No";
                    dataGridView1.Rows[0].Cells[2].Value = "No";
                    dataGridView1.Rows[0].Cells[3].Value = "No";
                }

                goto SP2Az1;
            }

            // SK42 ..................................................................

            double mrelX=0;
            double mrelY=0;
            double mrelX42=0;
            double mrelY42=0;

            // m rel
            mrelX = GlobalVarLn.XSP1_Az1;
            mrelY = GlobalVarLn.YSP1_Az1;

            // rad
            MapCore.mapPlaneToGeo((int)GlobalVarLn.hmapl, ref mrelX, ref mrelY);
            // grad
            mrelX = (mrelX * 180) / Math.PI;
            mrelY = (mrelY * 180) / Math.PI;

            // SK42(элл.)->Крюгер 
            ClassMap objClassMap_Az1 = new ClassMap();
            objClassMap_Az1.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       mrelX,   // широта
                       mrelY,  // долгота

                       // Выходные параметры (km)
                       ref mrelX42,
                       ref mrelY42
                   );

            // km->m
            mrelX42 = mrelX42 * 1000;
            mrelY42 = mrelY42 * 1000;

            // SP1
            //dataGridView1.Rows[0].Cells[1].Value=(int)GlobalVarLn.XSP1_Az1;
            //dataGridView1.Rows[0].Cells[2].Value=(int)GlobalVarLn.YSP1_Az1;
            dataGridView1.Rows[0].Cells[1].Value = (int)mrelX42;
            dataGridView1.Rows[0].Cells[2].Value = (int)mrelY42;
            // .................................................................. SK42
            // Azimuth

                X_Coordl3_1 = GlobalVarLn.XSP1_Az1;
                Y_Coordl3_1 = GlobalVarLn.YSP1_Az1;
                X_Coordl3_2 = GlobalVarLn.XCenter_Az1;
                Y_Coordl3_2 = GlobalVarLn.YCenter_Az1;

                // Разность координат для расчета азимута
                // На карте X - вверх
                DXX3 = Y_Coordl3_2 - Y_Coordl3_1;
                DYY3 = X_Coordl3_2 - X_Coordl3_1;

                // grad
                azz = objClassMap10.f_Def_Azimuth(DYY3, DXX3);

                //ichislo = (long) (azz * 100);
                //dchislo = ((double) ichislo) / 100;
                dataGridView1.Rows[0].Cells[3].Value = (int) azz;

            // ......................................................................

            // _________________________________________________________________ SP1

            // SP2 _________________________________________________________________

SP2Az1:
                // ......................................................................
                // !!! реальные координаты на местности карты в м (Plane)

                //GlobalVarLn.XSP2_Az1 = (double)iniRW.get_X_ASPS();
                //GlobalVarLn.YSP2_Az1 = (double)iniRW.get_Y_ASPS();

                //777
                GlobalVarLn.XSP2_Az1 = GlobalVarLn.XPoint1_Sost;
                GlobalVarLn.YSP2_Az1 = GlobalVarLn.YPoint1_Sost;

                if ((GlobalVarLn.XSP2_Az1 == 0) || (GlobalVarLn.YSP2_Az1 == 0))
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        //MessageBox.Show("Нет координат АСП сопряженной");
                        dataGridView1.Rows[1].Cells[1].Value = "Нет";
                        dataGridView1.Rows[1].Cells[2].Value = "Нет";
                        dataGridView1.Rows[1].Cells[3].Value = "Нет";
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                    {
                        //Azb
                        dataGridView1.Rows[1].Cells[1].Value = "yoxdur"; 
                        dataGridView1.Rows[1].Cells[2].Value = "yoxdur"; 
                        dataGridView1.Rows[1].Cells[3].Value = "yoxdur"; 
                    }
                else 
                {
                    dataGridView1.Rows[1].Cells[1].Value = "No";
                    dataGridView1.Rows[1].Cells[2].Value = "No";
                    dataGridView1.Rows[1].Cells[3].Value = "No";
                }

                goto PUAz1;
                }

                // SK42 ..................................................................
                double mrelX42_1 = 0;
                double mrelY42_1 = 0;

                // m rel
                mrelX = GlobalVarLn.XSP2_Az1;
                mrelY = GlobalVarLn.YSP2_Az1;

                // rad
                MapCore.mapPlaneToGeo((int)GlobalVarLn.hmapl, ref mrelX, ref mrelY);
                // grad
                mrelX = (mrelX * 180) / Math.PI;
                mrelY = (mrelY * 180) / Math.PI;

                // SK42(элл.)->Крюгер 
                ClassMap objClassMap1_Az1 = new ClassMap();
                objClassMap1_Az1.f_SK42_Krug
                       (
                    // Входные параметры (!!! grad)
                    // !!! эллипсоид Красовского
                           mrelX,   // широта
                           mrelY,  // долгота

                           // Выходные параметры (km)
                           ref mrelX42_1,
                           ref mrelY42_1
                       );

                // km->m
                mrelX42_1 = mrelX42_1 * 1000;
                mrelY42_1 = mrelY42_1 * 1000;

                // SP2
                dataGridView1.Rows[1].Cells[1].Value = (int)mrelX42_1;
                dataGridView1.Rows[1].Cells[2].Value = (int)mrelY42_1;
                // .................................................................. SK42

                // ......................................................................
                // Azimuth

                X_Coordl3_1 = GlobalVarLn.XSP2_Az1;
                Y_Coordl3_1 = GlobalVarLn.YSP2_Az1;
                X_Coordl3_2 = GlobalVarLn.XCenter_Az1;
                Y_Coordl3_2 = GlobalVarLn.YCenter_Az1;

                // Разность координат для расчета азимута
                // На карте X - вверх
                DXX3 = Y_Coordl3_2 - Y_Coordl3_1;
                DYY3 = X_Coordl3_2 - X_Coordl3_1;

                // grad
                azz = objClassMap10.f_Def_Azimuth(DYY3, DXX3);

                //ichislo = (long) (azz * 100);
                //dchislo = ((double) ichislo) / 100;
                dataGridView1.Rows[1].Cells[3].Value = (int)azz;

            // ......................................................................

            // _________________________________________________________________ SP2

            // PU _________________________________________________________________

PUAz1:
                // ......................................................................
                // !!! реальные координаты на местности карты в м (Plane)

                //GlobalVarLn.XPU_Az1 = (double)iniRW.get_X_PU();
                //GlobalVarLn.YPU_Az1 = (double)iniRW.get_Y_PU();
                //777
                GlobalVarLn.XPU_Az1 = GlobalVarLn.XPoint2_Sost;
                GlobalVarLn.YPU_Az1 = GlobalVarLn.YPoint2_Sost;

                if ((GlobalVarLn.XPU_Az1 == 0) || (GlobalVarLn.YPU_Az1 == 0))
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        //MessageBox.Show("Нет координат ПУ");
                        dataGridView1.Rows[2].Cells[1].Value = "Нет";
                        dataGridView1.Rows[2].Cells[2].Value = "Нет";
                        dataGridView1.Rows[2].Cells[3].Value = "Нет";
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                    {
                        //Azb
                        dataGridView1.Rows[2].Cells[1].Value = "yoxdur"; 
                        dataGridView1.Rows[2].Cells[2].Value = "yoxdur"; 
                        dataGridView1.Rows[2].Cells[3].Value = "yoxdur"; 
                    }
                else 
                {
                    dataGridView1.Rows[2].Cells[1].Value = "No";
                    dataGridView1.Rows[2].Cells[2].Value = "No";
                    dataGridView1.Rows[2].Cells[3].Value = "No";
                }

                return;
                }

                // SK42 ..................................................................
                double mrelX42_2 = 0;
                double mrelY42_2 = 0;

                // m rel
                mrelX = GlobalVarLn.XPU_Az1;
                mrelY = GlobalVarLn.YPU_Az1;

                // rad
                MapCore.mapPlaneToGeo((int)GlobalVarLn.hmapl, ref mrelX, ref mrelY);
                // grad
                mrelX = (mrelX * 180) / Math.PI;
                mrelY = (mrelY * 180) / Math.PI;

                // SK42(элл.)->Крюгер 
                ClassMap objClassMap2_Az1 = new ClassMap();
                objClassMap2_Az1.f_SK42_Krug
                       (
                    // Входные параметры (!!! grad)
                    // !!! эллипсоид Красовского
                           mrelX,   // широта
                           mrelY,  // долгота

                           // Выходные параметры (km)
                           ref mrelX42_2,
                           ref mrelY42_2
                       );

                // km->m
                mrelX42_2 = mrelX42_2 * 1000;
                mrelY42_2 = mrelY42_2 * 1000;

                // PU
                dataGridView1.Rows[2].Cells[1].Value = (int)mrelX42_2;
                dataGridView1.Rows[2].Cells[2].Value = (int)mrelY42_2;
                // .................................................................. SK42

                // ......................................................................
                // Azimuth

                X_Coordl3_1 = GlobalVarLn.XPU_Az1;
                Y_Coordl3_1 = GlobalVarLn.YPU_Az1;
                X_Coordl3_2 = GlobalVarLn.XCenter_Az1;
                Y_Coordl3_2 = GlobalVarLn.YCenter_Az1;

                // Разность координат для расчета азимута
                // На карте X - вверх
                DXX3 = Y_Coordl3_2 - Y_Coordl3_1;
                DYY3 = X_Coordl3_2 - X_Coordl3_1;

                // grad
                azz = objClassMap10.f_Def_Azimuth(DYY3, DXX3);

                //ichislo = (long) (azz * 100);
                //dchislo = ((double) ichislo) / 100;
                dataGridView1.Rows[2].Cells[3].Value = (int)azz;

            // ......................................................................

            // _________________________________________________________________ PU


            // ****************************************************************** INI


        } // ACCEPT
        // ************************************************************************        

        // ************************************************************************
        // Обработчик ComboBox "cbChooseSC": Выбор СК
        // ************************************************************************
        private void cbChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {

            ChooseSystemCoord_Az1(cbChooseSC.SelectedIndex);

        } // SK
        // ************************************************************************

        // FUNCTIONS **************************************************************

        // ************************************************************************
        // функция выбора системы координат
        // ************************************************************************

        private void ChooseSystemCoord_Az1(int iSystemCoord)
        {
            gbOwnRect.Visible = false;
            gbOwnRect42.Visible = false;
            gbOwnRad.Visible = false;
            gbOwnDegMin.Visible = false;
            gbOwnDegMinSec.Visible = false;

            switch (iSystemCoord)
            {
                case 0: // Метры на местности

                    gbOwnRect.Visible = true;
                    gbOwnRect.Location = new Point(11, 11);

                    if (GlobalVarLn.flCoord_Az1 == 1)
                    {
                        //333
                        //ichislo = (long)(GlobalVarLn.XCenter_Az1);
                        //tbXRect.Text = Convert.ToString(ichislo);
                        //ichislo = (long)(GlobalVarLn.YCenter_Az1);
                        //tbYRect.Text = Convert.ToString(ichislo);

                        ichislo = (long)(GlobalVarLn.BWGS84_Az1 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbXRect.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LWGS84_Az1 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbYRect.Text = Convert.ToString(dchislo);


                    } // IF

                    break;

                case 1: // Метры 1942 года

                    gbOwnRect42.Visible = true;
                    gbOwnRect42.Location = new Point(11, 11);

                    if (GlobalVarLn.flCoord_Az1 == 1)
                    {
                        ichislo = (long)(GlobalVarLn.XSP42_comm_Az1);
                        tbXRect42.Text = Convert.ToString(ichislo);
                        ichislo = (long)(GlobalVarLn.YSP42_comm_Az1);
                        tbYRect42.Text = Convert.ToString(ichislo);

                    } // IF

                    break;

                case 2: // Радианы (Красовский)

                    gbOwnRad.Visible = true;
                    gbOwnRad.Location = new Point(11, 11);

                    if (GlobalVarLn.flCoord_Az1 == 1)
                    {

                        ichislo = (long)(GlobalVarLn.LatKrR_comm_Az1 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBRad.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrR_comm_Az1 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLRad.Text = Convert.ToString(dchislo);

                    } // IF

                    break;

                case 3: // Градусы (Красовский)

                    gbOwnDegMin.Visible = true;
                    gbOwnDegMin.Location = new Point(11, 11);

                    if (GlobalVarLn.flCoord_Az1 == 1)
                    {
                        ichislo = (long)(GlobalVarLn.LatKrG_comm_Az1 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBMin1.Text = Convert.ToString(dchislo);
                        ichislo = (long)(GlobalVarLn.LongKrG_comm_Az1 * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLMin1.Text = Convert.ToString(dchislo);


                    } // IF

                    break;

                case 4: // Градусы,мин,сек (Красовский)

                    gbOwnDegMinSec.Visible = true;
                    gbOwnDegMinSec.Location = new Point(11, 11);

                    if (GlobalVarLn.flCoord_Az1 == 1)
                    {
                        tbBDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_comm_Az1);
                        tbBMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_comm_Az1);
                        ichislo = (long)(GlobalVarLn.Lat_Sec_comm_Az1);
                        tbBSec.Text = Convert.ToString(ichislo);
                        tbLDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_comm_Az1);
                        tbLMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_comm_Az1);
                        ichislo = (long)(GlobalVarLn.Long_Sec_comm_Az1);
                        tbLSec.Text = Convert.ToString(ichislo);

                    } // IF

                    break;

                default:
                    break;

            } // SWITCH

        } // ChooseSystemCoord_Az1
        // ************************************************************************

        // ************************************************************** FUNCTIONS

        // ************************************************************************
        // Closing
        // ************************************************************************
        private void FormAz1_FormClosing(object sender, FormClosingEventArgs e)
        {
            GlobalVarLn.f_Open_objFormAz1 = 0;

            e.Cancel = true;
            Hide();

        } // Closing
        // ************************************************************************

        // ************************************************************************
        // Activated
        // ************************************************************************
        private void FormAz1_Activated(object sender, EventArgs e)
        {
            GlobalVarLn.f_Open_objFormAz1 = 1;

        }

        private void lChooseSC_Click(object sender, EventArgs e)
        {

        } // Activated
        // ************************************************************************

        private void LanguageChooser()
        {
            var cont = this.Controls;

            if (NumberOfLanguage.Equals(0))
            {
                ChangeLanguageToRu(cont);
                this.Text = "Азимут";
            }
            if (NumberOfLanguage.Equals(1))
            {
                //ChangeLanguageToEng(cont);
            }
            if (NumberOfLanguage.Equals(2))
            {
                ChangeLanguageToAzer(cont);
                this.Text = "Azimut ";
            }
        }

        private void ChangeLanguageToRu(System.Windows.Forms.Control.ControlCollection cont)
        {
            string lang = "ru-RU";
            foreach (System.Windows.Forms.Control cc in cont)
            {
                if (cc is ComboBox)
                {

                    for (int i = 0; i < (cc as ComboBox).Items.Count; i++)
                    {
                        if (i == 0)
                        {
                            string a = resources.GetString(cc.Name + ".Items", new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                        else
                        {
                            string a = resources.GetString(cc.Name + ".Items" + i.ToString(), new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                    }
                }
                //}
                if (cc is Label || cc is Button || cc is RadioButton || cc is TabControl || cc is TabPage)
                {
                    resources.ApplyResources(cc, cc.Name, new CultureInfo(lang));
                    //toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
                }
                if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl || cc is TabPage)
                {
                    ChangeLanguageToRu(cc.Controls);
                }
            }
        }

        private void ChangeLanguageToAzer(System.Windows.Forms.Control.ControlCollection cont)
        {
            string lang = "az-Latn";

            foreach (System.Windows.Forms.Control cc in cont)
            {
                if (cc is ComboBox)
                {

                    for (int i = 0; i < (cc as ComboBox).Items.Count; i++)
                    {
                        if (i == 0)
                        {
                            string a = resources.GetString(cc.Name + ".Items", new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                        else
                        {
                            string a = resources.GetString(cc.Name + ".Items" + i.ToString(), new CultureInfo(lang));
                            (cc as ComboBox).Items[i] = a;
                        }
                    }
                }
                //}
                if (cc is Label || cc is Button || cc is RadioButton || cc is TabControl || cc is TabPage)
                {
                    resources.ApplyResources(cc, cc.Name, new CultureInfo(lang));
                    //toolTip.SetToolTip(cc, resources.GetString(cc.Name + ".ToolTip", new CultureInfo(lang)));
                }
                if (cc is Panel || cc is GroupBox || cc is ListBox || cc is TabControl || cc is DataGridView || cc is TabPage)
                {
                    ChangeLanguageToAzer(cc.Controls);
                }

            }
        }

    } // Class
} // Namespace
