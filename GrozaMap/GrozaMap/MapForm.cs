﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;
using System.Reflection;

using System.ServiceModel;

using System.Diagnostics;
using System.Threading;

using System.Globalization;

using ModelsTablesDBLib;
using ClientDataBase;

namespace GrozaMap
{

    public partial class MapForm : Form
    {

        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        private bool MapIsOpenned = false;
        private bool HeightMatrixIsOpenned = false;

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        // Lena

        Form1 form1;
        Form2 ObjCommPowerAvail;
        FormAirPlane ObjFormAirPlane;
        FormAz objFormAz;
        FormAz1 objFormAz1;
        FormLineSightRange ObjFormLineSightRange;
        FormPeleng objFormPeleng;
        FormS objFormS;
        //FormWay objFormWay;
        public FormWay objFormWay;
        public ZonePowerAvail objZonePowerAvail;
        public FormBearing objFormBearing;
        public FormSost objFormSost;
        public FormSuppression objFormSuppression;

        public static int hmapl1;

        public static bool blAirObject = false;
        //public static System.Windows.Forms.CheckBox chbair1;

        //private double dchislo;
        //private long ichislo;

        // 3
        //TDataADSBReceiver[] tDadaADSBReceiver;

        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        int iCounter = 0;
        ComponentResourceManager resources = new ComponentResourceManager(typeof(MapForm));
        private int NumberOfLanguage;

        // Конструктор ***********************************************************
        public MapForm()
        {
            InitializeComponent();

            // 3
            //ServiceAPMCreate(); // создать клиент

            form1 = new Form1(ref axaxcMapScreen1);
            ObjCommPowerAvail = new Form2(ref axaxcMapScreen1);
            ObjFormAirPlane = new FormAirPlane(ref axaxcMapScreen1);
            objFormAz = new FormAz(ref axaxcMapScreen1);
            objFormAz1 = new FormAz1(ref axaxcMapScreen1);
            ObjFormLineSightRange = new FormLineSightRange(ref axaxcMapScreen1);
            objFormPeleng = new FormPeleng(ref axaxcMapScreen1);
            objFormS = new FormS(ref axaxcMapScreen1);
            objFormWay = new FormWay(ref axaxcMapScreen1);
            objZonePowerAvail = new ZonePowerAvail(ref axaxcMapScreen1);
            objFormBearing = new FormBearing(ref axaxcMapScreen1);
            objFormSost = new FormSost(ref axaxcMapScreen1);
            objFormSuppression = new FormSuppression(ref axaxcMapScreen1);

            // 3
            //objFormSost.ClickGetGNSS += objFormSost_ClickGetGNSS;
            //objFormSost.ClickGetGNSS2 += objFormSost_ClickGetGNSS2;

            // .................................................................................................

            // Otl
            // 3
            //tDadaADSBReceiver = new TDataADSBReceiver[10];

            /////////////////////////////////////////////////////////////////////////////////////////////////
            //30.01.2018 в Init.ini хранятся имя папки и файла матр. высот
            //MapCore.openMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, Application.StartupPath + iniRW.get_map_path(), "\\Init.ini");
            MapCore.openMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, Application.StartupPath + iniRW.get_map_path1(), "\\Settings.ini");

            // 3
            //SendMapIsOpenToAPM(MapIsOpenned); //обратный контракт

            //MapCore.openMatrixFunc(ref HeightMatrixIsOpenned, ref axaxcMapScreen1, Application.StartupPath + iniRW.get_map_mtw());
            MapCore.openMatrixFunc(ref HeightMatrixIsOpenned, ref axaxcMapScreen1, Application.StartupPath + iniRW.get_map_mtw1());


            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            MouseWheel += MapForm_MouseWheel;

            axaxMapSelDlg.cMapView = axaxcMapScreen1.C_CONTAINER;

        }

        // IRI_PPRCH ***********************************************************************
        // INTERRUPT
        // !!! Теперь это только IRI PPRCH
        // Variant2

        // 22_10_2018
        private object threadLock_5 = new object();
        // *********************************************************************** IRI_PPRCH

        void service_CheckGNSSUpdated(object sender, byte e)
        {
            GlobalVarLn.bCheckGNSS = Convert.ToBoolean(e);
        }

        // chbSect -> Сейчас это траектории самолетов
        private void chbSect_CheckedChanged(object sender, EventArgs e)
        {
            if (chbSect.Checked == true)
            {
                GlobalVarLn.flair_traj = true;

                if (
                    (GlobalVarLn.fl_AirPlane == 1) &&
                    (GlobalVarLn.fl_AirPlaneVisible == 1)
                   )
                {
                    GlobalVarLn.axMapScreenGlobal.Repaint();
                }

            }
            else
            {
                GlobalVarLn.flair_traj = false;

                if (
                    (GlobalVarLn.fl_AirPlane == 1) &&
                    (GlobalVarLn.fl_AirPlaneVisible == 1)
                   )
                {
                    GlobalVarLn.axMapScreenGlobal.Repaint();
                }

            }

            // 3
            /*
                        // ----------------------------------------------------------
                        if (chbSect.Checked == true)
                        {
                            // ....................................................

                            if (
                                (GlobalVarLn.fllSect1 == 1) ||
                                 (GlobalVarLn.fllSect2 == 1) ||
                                 (GlobalVarLn.fllSect3 == 1) ||
                                 (GlobalVarLn.fllSect4 == 1) ||
                                 (GlobalVarLn.fllSect5 == 1)
                               )
                            {
                                GlobalVarLn.flsect = 1;

                                ClassMap.Sector(
                                               (double)GlobalVarLn.XCenter_Sost,
                                               (double)GlobalVarLn.YCenter_Sost
                                              );
                            }

                        } // chbSect.Checked == true
                        // ----------------------------------------------------------
                        else
                        {
                            ClassMap.Sector_Clear();


                        } // chbSect.Checked == false
                          // ----------------------------------------------------------
            */
        }

            // **************************************************************** SECTOR

            // PELENG_OTLADKA *********************************************************
            private void button9_Click(object sender, EventArgs e)
        {

        } //  Button9

        // ********************************************************* PELENG_OTLADKA

        // Peleng_IRI_FRCH ********************************************************

        // Color ------------------------------------------------------------------
        /// <summary>
        /// Convert HEX to color
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private static System.Drawing.Color ConvertToColor(String s)
        {
            //s = s.Remove(0, 1);
            string str = s.Remove(0, 1);
            int R;
            int G;
            int B;
            System.Drawing.Color rtn = System.Drawing.Color.Empty;
            try
            {
                R = Convert.ToByte(str.Substring(0, 2), 16);
                G = Convert.ToByte(str.Substring(2, 2), 16);
                B = Convert.ToByte(str.Substring(4, 2), 16);
                rtn = Color.FromArgb(R, G, B);
            }
            catch (Exception)
            {
                //doing nothing
            }

            return rtn;
        }
        // ------------------------------------------------------------------ Color

        // Interrupt_Peleng_IRI ---------------------------------------------------
        // INTERRUPT
        // !!! Для ФРЧ
        private double prevPel1 = -1;
        private double prevPel2 = -1;
        // --------------------------------------------------- Interrupt_Peleng_IRI

        // Color ------------------------------------------------------------------

        private Color ColorFromSharpString(string sharpString)
        {
            try
            {
                string hexColor = sharpString.Replace("#", "FF");
                int hexValue = int.Parse(hexColor, System.Globalization.NumberStyles.HexNumber);
                return Color.FromArgb(hexValue);
            }
            catch { return Color.FromArgb(0, 0, 0, 0); }
        }
        // ------------------------------------------------------------------ Color

        // ******************************************************** Peleng_IRI_FRCH

        // *****************************************************************************************
        // Обработчик кнопки Button11
        // *****************************************************************************************
        //2501otl
        private void button11_Click(object sender, EventArgs e)
        {
            ;

        } // Button11
        // *****************************************************************************************

        // GPS_PELENG *************************************************************

        private void fpell(double az1, int x11, int y11, ref int x21, ref int y21)
        {
            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;

            x1 = (double)x11;
            y1 = (double)y11;
            x2 = (double)x21;
            y2 = (double)y21;

            double az = 0;
            // l = Дальность отрисовки пеленга в метрах
            //double l = 15000;
            double l = 1000000;

            az = (az1 * Math.PI) / 180;

            if ((az >= 0) && (az <= Math.PI / 2))
            {
                //x2 = x1 - l * Math.Cos(az);
                x2 = x1 + l * Math.Cos(az);
                y2 = y1 + l * Math.Sin(az);
            }
            else if ((az > Math.PI / 2) && (az <= Math.PI))
            {
                //x2 = x1 + l * Math.Sin(az-Math.PI/2);
                x2 = x1 - l * Math.Sin(az - Math.PI / 2);
                y2 = y1 + l * Math.Cos(az - Math.PI / 2);
            }
            else if ((az > Math.PI) && (az <= (3 * Math.PI / 2)))
            {
                //x2 = x1 + l * Math.Cos(az - Math.PI );
                x2 = x1 - l * Math.Cos(az - Math.PI);
                y2 = y1 - l * Math.Sin(az - Math.PI);
            }
            else if ((az > (3 * Math.PI / 2)) && (az <= (2 * Math.PI)))
            {
                //x2 = x1 - l * Math.Sin(az - (3 * Math.PI / 2));
                x2 = x1 + l * Math.Sin(az - (3 * Math.PI / 2));
                y2 = y1 - l * Math.Cos(az - (3 * Math.PI / 2));
            }
            else
            {   //Azb
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Недопустимое значение азимута");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    MessageBox.Show("Yolverilməz azimut göstəriciləri");
                }
                else 
                {
                    MessageBox.Show("Invalid azimuth value");
                }

                return;
            }

            y21 = (int)y2;
            x21 = (int)x2;


        }// fpel

        // ******************************************************************************************
        private void f_Map_Line_Pl(
                                  double X111,
                                  double Y111,
                                  double X222,
                                  double Y222

                                 )
        {
            // -------------------------------------------------------------------------------------
            Graphics graph = axaxcMapScreen1.CreateGraphics();

            Pen penRed2 = new Pen(Color.Red, 2);
            Brush brushRed2 = new SolidBrush(Color.Red);
            // -------------------------------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X111, ref Y111);
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X222, ref Y222);
            // -------------------------------------------------------------------------------------
            if (graph != null)
            {

                Point point1 = new Point((int)X111 - axaxcMapScreen1.MapLeft, (int)Y111 - axaxcMapScreen1.MapTop);
                Point point2 = new Point((int)X222 - axaxcMapScreen1.MapLeft, (int)Y222 - axaxcMapScreen1.MapTop);

                graph.DrawLine(penRed2, point1, point2);
            }
            // -------------------------------------------------------------------------------------

        }

        private void f_RedrawPelMain()
        {
            double xx1 = 0;
            double yy1 = 0;
            double xx2 = 0;
            double yy2 = 0;

            xx1 = (double)GlobalVarLn.X1_PelMain;
            yy1 = (double)GlobalVarLn.Y1_PelMain;
            xx2 = (double)GlobalVarLn.X2_PelMain;
            yy2 = (double)GlobalVarLn.Y2_PelMain;
            f_Map_Line_Pl(xx1, yy1, xx2, yy2);

            if (GlobalVarLn.flPelMain2 == 1)
            {
                xx1 = (double)GlobalVarLn.X1_1_PelMain;
                yy1 = (double)GlobalVarLn.Y1_1_PelMain;
                xx2 = (double)GlobalVarLn.X2_1_PelMain;
                yy2 = (double)GlobalVarLn.Y2_1_PelMain;
                f_Map_Line_Pl(xx1, yy1, xx2, yy2);
            }
        }


        // ************************************************************* GPS_PELENG

        // MOUSE_WHEEL ******************************************************************
        //Обработка колёсика
        // ******************************************************************************
        void MapForm_MouseWheel(object sender, MouseEventArgs e)
        {
            if (MapIsOpenned == true)
            {

                Rectangle rp = axaxcMapScreen1.Bounds;
                Rectangle rp2 = panel2.Bounds;

                if ((e.X >= rp.Left && e.X <= rp.Right && e.Y >= rp.Top && e.Y <= rp.Bottom) && (e.X >= rp2.Left && e.X <= rp2.Right && e.Y >= rp2.Top && e.Y <= rp2.Bottom))
                {
                    if (e.Delta != 0)
                    {

                        //Sect
                        int fff = 0;

                        if (e.Delta > 0)
                        {
                            // ........................................................
                            //Sect

                            if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
                            {
                                fff = 1;
                                GlobalVarLn.flsect = 0;
                            }
                            // ........................................................

                            MapCore.ZoomInFunc(ref axaxcMapScreen1);
                        }
                        if (e.Delta < 0)
                        {
                            // ........................................................
                            //Sect

                            if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
                            {
                                fff = 1;
                                GlobalVarLn.flsect = 0;
                            }
                            // ........................................................

                            MapCore.ZoomOutFunc(ref axaxcMapScreen1);
                        }

                        // ........................................................
                        //Sect 

                        if (fff == 1)
                        {
                            GlobalVarLn.flsect = 1;
                            ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
                        }
                        // ..........................................................

                    }
                }

            } // MapIsOpenned == true
        }

        // ****************************************************************** MOUSE_WHEEL

        // ******************************************************************************
        //Закрытие формы
        // ******************************************************************************

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MapIsOpenned == true)
            {
                MapCore.closeMatrixFunc(ref HeightMatrixIsOpenned, ref axaxcMapScreen1);
                //MapCore.closeMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, Application.StartupPath + "\\Init.ini");
                MapCore.closeMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, Application.StartupPath + "\\Settings.ini");


                // 3
                //SendMapIsOpenToAPM(MapIsOpenned); //обратный контракт
            }
        } // Close
        // ******************************************************************************

        // ******************************************************************************
        // Menu: открыть карту
        // ******************************************************************************

        private void openMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == false)
            {
                //MapCore.openMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, ref axaxOpenMapDialog1, Application.StartupPath + "\\Init.ini");
                MapCore.openMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, ref openFileDialog1, Application.StartupPath + "\\Settings.ini");

                // Lena
                GlobalVarLn.hmapl = (int)axaxcMapScreen1.MapHandle;
                hmapl1 = (int)axaxcMapScreen1.MapHandle;

                // 3
                //SendMapIsOpenToAPM(MapIsOpenned); //обратный контракт
            }
            //Azb
            else
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Карта уже открыта");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    MessageBox.Show("Xəritə açıqdır");
                }
                else 
                {
                    MessageBox.Show("Map is already opened");
                }

            }

        } // Menu: открыть карту
        // ******************************************************************************

        // ******************************************************************************
        // Menu: закрыть карту
        // ******************************************************************************

        private void closeMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                //MapCore.closeMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, Application.StartupPath + "\\Init.ini");
                MapCore.closeMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, Application.StartupPath + "\\Settings.ini");


                // 3
                //SendMapIsOpenToAPM(MapIsOpenned); //обратный контракт
            }
        } // Menu: close map
        // ******************************************************************************

        // ******************************************************************************
        // Menu: открыть матрицу высот
        // ******************************************************************************

        private void openHeightMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                if (HeightMatrixIsOpenned == false)
                {
                    MapCore.openMatrixFunc(ref HeightMatrixIsOpenned, ref axaxcMapScreen1, ref openFileDialog1);
                }
                else
                {
                    if (GlobalVarLn.fl_Azb == 0)
                    {
                        //Azb
                        MessageBox.Show("Матрица высот уже открыта");
                    }
                    else if (GlobalVarLn.fl_Azb == 1)
                    {
                        MessageBox.Show("Hündürlük matrisası açıqdır");
                    }
                    else 
                    {
                        MessageBox.Show("The Height Matrix is already opened");
                    }

                }
            }
            else
            {
                //Azb
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Карта не открыта");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    MessageBox.Show("Xəritə açıq deyil");
                }
                else 
                {
                    MessageBox.Show("Map is not opened");
                }

            }
        }
        // ******************************************************************************

        // ******************************************************************************
        // Menu:закрыть матрицу высот
        // ******************************************************************************

        private void closeHeightMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MapCore.closeMatrixFunc(ref HeightMatrixIsOpenned, ref axaxcMapScreen1);
        }
        // ******************************************************************************
        // увеличить масштаб
        // ******************************************************************************

        private void ZoomIn_Click(object sender, EventArgs e)
        {
            // ..........................................................................
            //Sect увеличить масштаб

            int fff = 0;
            if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            {
                fff = 1;
                GlobalVarLn.flsect = 0;
            }
            // ..........................................................................

            MapCore.ZoomInFunc(ref axaxcMapScreen1);
            label5.Text = string.Format("S 1: {0}", Convert.ToString((int)axaxcMapScreen1.ViewScale));

            // ..........................................................................
            //Sect увеличить масштаб

            if (fff == 1)
            {
                GlobalVarLn.flsect = 1;
                ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
            }
            // ..........................................................................

        }
        // ******************************************************************************

        // ******************************************************************************
        // уменьшить масштаб
        // ******************************************************************************

        private void ZoomOut_Click(object sender, EventArgs e)
        {
            // ..........................................................................
            //Sect уменьшить масштаб

            int fff = 0;
            if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            {
                fff = 1;
                GlobalVarLn.flsect = 0;
            }
            // ..........................................................................

            MapCore.ZoomOutFunc(ref axaxcMapScreen1);
            label5.Text = string.Format("S 1: {0}", Convert.ToString((int)axaxcMapScreen1.ViewScale));

            // ..........................................................................
            //Sect уменьшить масштаб

            if (fff == 1)
            {
                GlobalVarLn.flsect = 1;
                ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
            }
            // ..........................................................................

        }
        // ******************************************************************************
        // исходный масштаб
        // ******************************************************************************

        private void ZoomInitial_Click(object sender, EventArgs e)
        {
            // ............................................................................
            //Sect исходный масштаб

            int fff = 0;
            if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            {
                fff = 1;
                GlobalVarLn.flsect = 0;
                if (axaxcMapScreen1.ViewScale == 500000)
                    axaxcMapScreen1.Repaint();

            }
            // ............................................................................

            //MapCore.ZoomInitialFunc(ref axaxcMapScreen1);

            axaxcMapScreen1.ViewScale = 500000;
            label5.Text = string.Format("S 1: {0}", Convert.ToString((int)axaxcMapScreen1.ViewScale));

            // ............................................................................
            //Sect исходный масштаб

            if (fff == 1)
            {
                GlobalVarLn.flsect = 1;
                ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
            }
            // ............................................................................

        }
        // ******************************************************************************

        // ******************************************************************************
        //Sect
        // Otladka

        //Форма по кнопке 1
        public CheckPointForm checkPointForm;
        private void button1_Click(object sender, EventArgs e)
        {
        } // P/P
        // ******************************************************************************

        //Клик правой кнопкой мыши
        private double righteX;
        private double righteY;

        //Клик левой кнопкой мыши и премещение
        private bool waspressleft = false;
        private double startlefteX;
        private double startlefteY;
        private double movelefteX;
        private double movelefteY;


        // MOUSE_DOWN **************************************************************************************************
        //Обработка мыши на карте

        private void axaxcMapScreen1_OnMapMouseDown(object sender, AxaxGisToolKit.IaxMapScreenEvents_OnMapMouseDownEvent e)
        {

            // 1210
            GlobalVarLn.MousePress = true;

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
            // Для маршрута

            // -----------------------------------------------------------------------------------
            // Lena

            ClassMap.f_XYMap(e.x, e.y);

            // Маршрут
            if (
                (GlobalVarLn.blWay_stat == true) &&
                (GlobalVarLn.f_Open_objFormWay == 1)
               )
            {
                objFormWay.f_Way(
                               GlobalVarLn.MapX1,
                               GlobalVarLn.MapY1
                              );
            }
            // -----------------------------------------------------------------------------------

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

            // Sostav *****************************************************************************************
            /*
                        if (GlobalVarLn.blWay_stat == true)
                        {
                            objFormWay.f_Way(
                                           GlobalVarLn.MapX1,
                                           GlobalVarLn.MapY1
                                          );
                        }
                        // -----------------------------------------------------------------------------------
            */

            // ***************************************************************************************** Sostav

            if (e.button == 0)
            {
                waspressleft = true;
                startlefteX = e.x;
                startlefteY = e.y;

                MapCore.mapPlaneToPicture((int)axaxcMapScreen1.MapHandle, ref startlefteX, ref startlefteY);

            }
            if (e.button == 1)
            {
                righteX = e.x;
                righteY = e.y;
            }

        }
        // ************************************************************************************************** MOUSE_DOWN

        // MOUSE_MOVE *************************************************************************************************
        // MouseMove

        private void axaxcMapScreen1_OnMapMouseMove(object sender, AxaxGisToolKit.IaxMapScreenEvents_OnMapMouseMoveEvent e)
        {
            if (waspressleft == true)
            {

                movelefteX = e.x;
                movelefteY = e.y;

                MapCore.mapPlaneToPicture((int)axaxcMapScreen1.MapHandle, ref movelefteX, ref movelefteY);

                this.Cursor = Cursors.SizeAll;
                axaxcMapScreen1.MapLeft -= (int)(movelefteX - startlefteX);
                axaxcMapScreen1.MapTop -= (int)(movelefteY - startlefteY);


            }


            // Координаты внизу********************************************************************
            if (MapIsOpenned == true)
            {

                double moveX1 = 0;
                double moveY1 = 0;
                double moveHH = 0;

                moveX1 = e.x;
                moveY1 = e.y;
                //label2.Text = string.Format("X, m = {0}", Convert.ToString((int)moveX1));
                //label3.Text = string.Format("Y, m = {0}", Convert.ToString((int)moveY1));

                // m rel
                GlobalVarLn.X_Down_mrel = moveX1;
                GlobalVarLn.Y_Down_mrel = moveY1;

                // rad
                MapCore.mapPlaneToGeo((int)axaxcMapScreen1.MapHandle, ref GlobalVarLn.X_Down_mrel, ref GlobalVarLn.Y_Down_mrel);
                // grad
                GlobalVarLn.X_Down_mrel = (GlobalVarLn.X_Down_mrel * 180) / Math.PI;
                GlobalVarLn.Y_Down_mrel = (GlobalVarLn.Y_Down_mrel * 180) / Math.PI;

                // SK42(элл.)->Крюгер ****************************************************
                // Преобразование геодезических координат (широта, долгота, высота) 
                // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
                // проекции Гаусса-Крюгера
                // Входные параметры -> !!!grad

                ClassMap objClassMap_glob = new ClassMap();

                objClassMap_glob.f_SK42_Krug
                       (
                           // Входные параметры (!!! grad)
                           // !!! эллипсоид Красовского
                           GlobalVarLn.X_Down_mrel,   // широта
                           GlobalVarLn.Y_Down_mrel,  // долгота

                           // Выходные параметры (km)
                           ref GlobalVarLn.X42_mrel,
                           ref GlobalVarLn.Y42_mrel
                       );

                // km->m
                GlobalVarLn.X42_mrel = GlobalVarLn.X42_mrel * 1000;
                GlobalVarLn.Y42_mrel = GlobalVarLn.Y42_mrel * 1000;

                label2.Text = string.Format("X, m = {0}", Convert.ToString((int)GlobalVarLn.X42_mrel));
                label3.Text = string.Format("Y, m = {0}", Convert.ToString((int)GlobalVarLn.Y42_mrel));

                // **************************************************** SK42(элл.)->Крюгер

                // .......................................................................
                // H

                GlobalVarLn.axMapPointGlobalAdd.SetPoint(moveX1, moveY1);
                moveHH = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                label4.Text = string.Format("H, m = {0}", Convert.ToString((int)moveHH));

                MapCore.mapPlaneToGeo((int)axaxcMapScreen1.MapHandle, ref moveX1, ref moveY1);
                moveX1 = (moveX1 * 180) / Math.PI;
                moveY1 = (moveY1 * 180) / Math.PI;

                if (GlobalVarLn.fl_Azb == 0)
                {
                    lLat.Text = string.Format("Широта,° = {0}", moveX1.ToString("0.000"));
                    label1.Text = string.Format("Долгота,° = {0}", moveY1.ToString("0.000"));
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb ???
                    lLat.Text = string.Format("Enlik,° = {0}", moveX1.ToString("0.000"));
                    label1.Text = string.Format("Uzunluq,° = {0}", moveY1.ToString("0.000"));
                }
                else 
                {
                    lLat.Text = string.Format("Latitude,° = {0}", moveX1.ToString("0.000"));
                    label1.Text = string.Format("Longitude,° = {0}", moveY1.ToString("0.000"));
                }

                // ******************************************************************** Координаты внизу

                label5.Text = string.Format("S 1: {0}", Convert.ToString((int)axaxcMapScreen1.ViewScale));

            } // MapIsOpened

        } // MouseMove
        // ************************************************************************************************* MOUSE_MOVE

        // MOUSE_UP ****************************************************************************************************
        private void axaxcMapScreen1_OnMapMouseUp(object sender, AxaxGisToolKit.IaxMapScreenEvents_OnMapMouseUpEvent e)
        {
            if (e.button == 0)
            {
                // 1210
                GlobalVarLn.MousePress = false;

                // .....................................................................................................
                int fff = 0;
                // Sect Mouse_up
                if (
                    // !!! Это было до сектора                    
                    //((GlobalVarLn.fl_PelMain == 1) && (GlobalVarLn.f_Open_objFormBearing == 1)) ||
                    ((GlobalVarLn.fl_PelMain == 1)) ||

                    //((GlobalVarLn.fl_LineSightRange == 1) && (GlobalVarLn.f_Open_ObjFormLineSightRange==1)) ||
                    (GlobalVarLn.fl_LineSightRange == 1) ||

                    //((GlobalVarLn.flCoordSP_sup == 1) && (GlobalVarLn.f_Open_objFormSuppression == 1)) ||
                    (GlobalVarLn.flCoordSP_sup == 1) ||

                    ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
                   )
                {
                    if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
                    {
                        fff = 1;
                        GlobalVarLn.flsect = 0;
                    }

                    // !!! Это было до сектора
                    // Убрать с карты
                    GlobalVarLn.axMapScreenGlobal.Repaint();

                    if (fff == 1)
                    {
                        GlobalVarLn.flsect = 1;
                        ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
                    }


                }
                // .....................................................................................................

                waspressleft = false;
            }

            this.Cursor = Cursors.Default;
        }
        // **************************************************************************************************** MOUSE_UP

        //Флаги перерисовки
        bool paintRadioSources = true;
        bool paintStations = true;
        bool paintArcPoints = true;
        bool paintPeleng = true;
        bool infopaint = true;


        // ПЕРЕРИСОВКА КАРТЫ ***********************************************************************************************

        //Перисовка карты
        private void axaxcMapScreen1_OnMapPaint(object sender, AxaxGisToolKit.IaxMapScreenEvents_OnMapPaintEvent e)
        {

            // LENA **********************************************************************************
            // Lena (перерисовка)

            // ------------------------------------------------------------------------------------
            // Пеленг

            if (GlobalVarLn.fl_Peleng_stat == 1)
            {
                //ClassMap.f_ReDrawPeleng();
            }


            // 14_09_2018 ?????????????? Выяснить почему две функции вызываются
            if (chbPeleng.Checked == true)
            {
                // 14_09_2018
                //if(GlobalVarLn.fl_PelIRI_1==1)
                // Для ФРЧ
                ClassMap.f_ReDrawPeleng();

                // Для ППРЧ
                ClassMap.f_ReDrawPeleng_2();
            }
            // -----------------------------------------------------------------------------------
            // !!!IRI_2 -> всегда
            // ИРИ ППРЧ

            // 3
            //ClassMap.f_RedrawIRI_2();
            ClassMap.f_RedrawIRI_PPRH();

            // -----------------------------------------------------------------------------------
            // S

            if (GlobalVarLn.fl_S1_stat == 1)
            {
                ClassMap.f_ReDrawS_stat();
            }
            // -----------------------------------------------------------------------------------
            // Маршрут

            if (GlobalVarLn.flEndWay_stat == 1)
            {
                objFormWay.f_WayReDraw();

            }
            // -----------------------------------------------------------------------------------
            // Самолеты

            if (GlobalVarLn.fl_AirPlaneVisible == 1)
            {

                // Перерисовка самолетов
                //ClassMap.f_ReDrawAirPlane1();
                ClassMap.f_ReDrawAirPlaneBD();

            }
            // -------------------------------------------------------------------------------------
            // Энергодоступность по УС

            if (GlobalVarLn.fl_CommPowerAvail == 1)
            {
                ClassMap.f_Map_Redraw_CommPowerAvail1();
            }
            // -------------------------------------------------------------------------------------
            //2106

            /*
                        int fff1 = 0;
                        int fff2 = 0;
                        int fff3 = 0;

                        if (GlobalVarLn.fl_RepSect == 0)
                        {
                            if (
                                (GlobalVarLn.fl_LineSightRange == 1) &&
                                (GlobalVarLn.MousePress == false)
                               )
                            {
                                fff1 = 1;
                                GlobalVarLn.fl_LineSightRange = 0;
                            }
                            if (
                                (GlobalVarLn.fl_PelMain == 1) &&
                                (GlobalVarLn.MousePress == false)
                                )
                            {
                                fff2 = 1;
                                GlobalVarLn.fl_PelMain = 0;
                            }

                            if (
                                (GlobalVarLn.flsect == 1) &&
                                (GlobalVarLn.MousePress == false) &&
                                (chbSect.Checked == true)
                               )
                            {
                                fff3 = 1;
                                GlobalVarLn.flsect = 0;
                            }

                            if ((fff1 == 1) || (fff2 == 1) || (fff3 == 1))
                            {
                                GlobalVarLn.fl_RepSect = 1;
                                GlobalVarLn.axMapScreenGlobal.Repaint();
                                GlobalVarLn.fl_RepSect = 0;
                            }

                            if (fff1 == 1)
                            {
                                fff1 = 0;
                                GlobalVarLn.fl_LineSightRange = 1;
                            }
                            if (fff2 == 1)
                            {
                                fff2 = 0;
                                GlobalVarLn.fl_PelMain = 1;
                            }
                            if (fff3 == 1)
                            {
                                fff3 = 0;
                                GlobalVarLn.flsect = 1;
                            }

                        }
             */
            // -------------------------------------------------------------------------------------
            // ЗПВ

            if (
                (GlobalVarLn.fl_LineSightRange == 1) &&
                (GlobalVarLn.MousePress == false)
               )
            {
                // SP
                ClassMap.f_DrawSPXY(
                              GlobalVarLn.XCenter_ZPV,  // m на местности
                              GlobalVarLn.YCenter_ZPV,
                                  ""
                             );


                ClassMap.DrawLSR();
            }

            if (
                (GlobalVarLn.flCoordParaZPV == 1) &&
                (GlobalVarLn.MousePress == false)
               )
            {
                ClassMap.DrawLSR_Z1();
                ClassMap.DrawLSR_Z2();

            }
            // -------------------------------------------------------------------------------------
            // Состав комплекса

            // GPSSPPU
            //if (GlobalVarLn.fl_Sost == 1)
            if (
               (GlobalVarLn.flCoordSP_Sost == 1) ||
               (GlobalVarLn.flCoordYS1_Sost == 1) ||
               (GlobalVarLn.flCoordYS2_Sost == 1)
               )
            {
                ClassMap.f_Map_Redraw_Sost();
            }
            // -------------------------------------------------------------------------------------
            //2106
            //if (GlobalVarLn.flscrolsect == 1)
            //{
            //    GlobalVarLn.flscrolsect = 0;
            //    return;
            //}

            // Bearing

            if (
                (GlobalVarLn.fl_PelMain == 1) &&
                (GlobalVarLn.MousePress == false) &&
                (GlobalVarLn.xxxY == 0) &&
                (GlobalVarLn.xxxY1 == 0)

               )
            {

                if (GlobalVarLn.ftmr == 0)
                {
                    ClassMap.f_ReDraw_Map_Bearing1();
                    GlobalVarLn.ftmr = 1;
                    GlobalVarLn.stopWatch.Start();
                }
                else
                {

                    GlobalVarLn.ftmr = 0;
                    GlobalVarLn.stopWatch.Stop();
                    GlobalVarLn.ts = GlobalVarLn.stopWatch.Elapsed;


                    if (
                        (GlobalVarLn.ts.Hours == 0) &&
                        (GlobalVarLn.ts.Minutes == 0) &&
                        (GlobalVarLn.ts.Seconds <= 0.1)
                        )
                    {
                        ;
                    }
                    else
                    {
                        //ZZZ
                        ClassMap.f_ReDraw_Map_Bearing1();

                    }

                } // GlobalVarLn.ftmr == 1

            } // GlobalVarLn.fl_PelMain == 1

            // ------------------------------------------------------------------------------------
            // Az1

            if (GlobalVarLn.flCoord_Az1 == 1)
            {
                ClassMap.f_Rect_Az1(GlobalVarLn.XCenter_Az1, GlobalVarLn.YCenter_Az1);
            }
            // -------------------------------------------------------------------------------------
            // Зона подавления

            if (
                (GlobalVarLn.flCoordSP_sup == 1) &&
                (GlobalVarLn.MousePress == false)
               )
            {
                ClassMap.f_Map_Redraw_Zon_Suppression();
            }
            // -------------------------------------------------------------------------------------
            //GPSIRI
            // ИРИ ФРЧ

            if (GlobalVarLn.flGPS_IRI == 1)
            {
                ClassMap.f_Map_Redraw_GPS_IRI();
            }

            // -------------------------------------------------------------------------------------
            //Sect перерисовка

            if (
                (GlobalVarLn.flsect == 1) &&
                (GlobalVarLn.MousePress == false) &&
                (chbSect.Checked == true)

               )
            {
                // .........................................................................

                if (GlobalVarLn.ftmr_Sect == 0)
                {

                    if (GlobalVarLn.flscrolsect == 1)
                    {
                        GlobalVarLn.flscrolsect = 0;
                        GlobalVarLn.ftmr_Sect = 1;
                        GlobalVarLn.stopWatch2.Start();
                        return;
                    }

                    ClassMap.Sector(
                                      (double)GlobalVarLn.XCenter_Sost,
                                      (double)GlobalVarLn.YCenter_Sost
                                    );

                    GlobalVarLn.ftmr_Sect = 1;
                    GlobalVarLn.stopWatch2.Start();
                }
                else
                {

                    GlobalVarLn.ftmr_Sect = 0;
                    GlobalVarLn.stopWatch2.Stop();
                    GlobalVarLn.ts_Sect = GlobalVarLn.stopWatch2.Elapsed;


                    if (
                        (GlobalVarLn.ts_Sect.Hours == 0) &&
                        (GlobalVarLn.ts_Sect.Minutes == 0) &&
                        (GlobalVarLn.ts_Sect.Seconds <= 0.1)
                        )
                    {
                        int ii = 0;
                        ii = ii;
                    }
                    else
                    {
                        //2106
                        //GlobalVarLn.flsect = 0;
                        // Убрать с карты
                        //GlobalVarLn.axMapScreenGlobal.Repaint();
                        //GlobalVarLn.flsect = 1;

                        ClassMap.Sector(
                                          (double)GlobalVarLn.XCenter_Sost,
                                          (double)GlobalVarLn.YCenter_Sost
                                        );

                    }

                } // GlobalVarLn.ftmr_Sect == 1

                // .........................................................................

            } // Sector
            // ********************************************************************************** LENA

        } // Перерисовка карты
        // *********************************************************************************************** ПЕРЕРИСОВКА КАРТЫ


        //Радиоисточники
        private List<MapCore.RadioSource> radioSources = new List<MapCore.RadioSource>();

        //Станции
        private List<MapCore.Station> stations = new List<MapCore.Station>();

        //Точки кривой
        private List<PointF> ArcPoints = new List<PointF>();

        // Снять координаты
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                if (checkPointForm != null && !checkPointForm.IsDisposed)
                {
                    checkPointForm.FillTextBoxes((int)axaxcMapScreen1.MapHandle, righteX, righteY);

                }
            }
        }

        // добавить ИРИ
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                MapCore.RadioSource temp = new MapCore.RadioSource(righteX, righteY);
                MapCore.DrawAndSaveTriangle(ref temp, ref axaxcMapScreen1);
                radioSources.Add(temp);
            }
        }

        // удалить ИРИ
        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                MapCore.DeleteTriangle(righteX, righteY, ref radioSources, ref axaxcMapScreen1);
            }
        }

        // добавить точку для дуги
        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                ArcPoints.Add(new PointF((float)righteX, (float)righteY));
            }
        }

        //нарисовать дугу
        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                if (ArcPoints.Count >= 2)
                    MapCore.DrawArc(ArcPoints.ToArray(), ref axaxcMapScreen1);
            }
        }

        //очистить точки для дуги
        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                ArcPoints.Clear();
                axaxcMapScreen1.Repaint();
            }
        }

        //Добавить станцию
        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                MapCore.Station temp = new MapCore.Station(righteX, righteY);
                MapCore.DrawAndSaveStation(ref temp, ref axaxcMapScreen1, imageList1);
                stations.Add(temp);
            }
        }

        //Удалить станцию
        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                MapCore.DeleteStation(righteX, righteY, ref stations, ref axaxcMapScreen1);
            }
        }

        //Отрисовать пеленг
        private void toolStripMenuItem9_Click(object sender, EventArgs e)
        {
            double a = 0;
            double b = 0;
            double c = 0;
            double d = 0;
            double f = 0;
            double g = 0;

            var currRSIndex = MapCore.GetRadioSourceIndex(righteX, righteY, ref radioSources, ref axaxcMapScreen1);

            if (currRSIndex != -1)
            {
                /*
                                double peleng = 255;
                                double distance = 200;
                                uint numberofdots = 1000;

                                double[] arr_Pel = new double[numberofdots * 3];
                                double[] arr_Pel_XYZ = new double[numberofdots * 3];

                                ClassMap classMap = new ClassMap();
                                classMap.f_Peleng(peleng, distance, numberofdots, radioSources[currRSIndex].geoX, radioSources[currRSIndex].geoY, ref a, ref b, ref c, ref d, ref f, ref g, ref arr_Pel, ref arr_Pel_XYZ);

                                double[] latitude = new double[numberofdots];
                                double[] longitude = new double[numberofdots];

                                MapCore.One3NumArrtoTwo1NumArrs(arr_Pel, ref latitude, ref longitude);

                                var temp = radioSources[currRSIndex];
                                temp.RadioSourceSetLatLon(latitude, longitude);
                                radioSources[currRSIndex] = temp;

                                MapCore.DrawPeleng(radioSources[currRSIndex].latitude, radioSources[currRSIndex].longitude, ref axaxcMapScreen1);
                 */
            }
        }

        //Отобразить/скрыть текст
        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {
            MapCore.SetInfoPaint(righteX, righteY, ref radioSources, ref axaxcMapScreen1);
        }


        // LENA ************************************************************************************
        // Lena

        // *****************************************************************************************
        // Загрузка головной формы
        // *****************************************************************************************

        private void MapForm_Load(object sender, EventArgs e)
        {
            string pathToMap = "";
            string pathToMTW = "";

            string strExePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            string PathFolder1 = Path.GetDirectoryName(strExePath);

            string strPathLatest = PathFolder1 + "\\INI\\Common.ini";
            foreach (string line in File.ReadLines(strPathLatest))
            {
                if (line.Contains("Language "))
                {
                    string LanguageAsNumber = line.Split(new string[] { "=" }, StringSplitOptions.None).Last();
                    NumberOfLanguage = Int32.Parse(LanguageAsNumber);
                    break;
                }

            }
            LanguageChooser();

            GlobalVarLn.stopWatch.Stop();


            GlobalVarLn.flPelMain = 0;
            GlobalVarLn.flPelMain2 = 0;
            chbPeleng.Checked = false;

            // --------------------------------------------------
            GlobalVarLn.objFormBearingG = objFormBearing;
            GlobalVarLn.objFormSostG = objFormSost;
            GlobalVarLn.objFormWayG = objFormWay;
            GlobalVarLn.objFormAzG = objFormAz;
            GlobalVarLn.objFormAz1G = objFormAz1;
            GlobalVarLn.ObjFormLineSightRangeG = ObjFormLineSightRange;
            GlobalVarLn.objZonePowerAvailG = objZonePowerAvail;
            GlobalVarLn.ObjCommPowerAvailG = ObjCommPowerAvail;
            GlobalVarLn.objFormSuppressionG = objFormSuppression;
            // --------------------------------------------------

            GlobalVarLn.axMapScreenGlobal = axaxcMapScreen1;

            // Привязка Point к Screen
            GlobalVarLn.axMapPointGlobalAdd = new axGisToolKit.axMapPoint();
            GlobalVarLn.axMapPointGlobalAdd.cMapView = axaxcMapScreen1.C_CONTAINER;
            GlobalVarLn.axMapPointGlobalAdd.PlaceInp = axGisToolKit.TxPPLACE.PP_PLANE;
            GlobalVarLn.axMapPointGlobalAdd.PlaceOut = axGisToolKit.TxPPLACE.PP_PLANE;

            GlobalVarLn.hmapl = (int)axaxcMapScreen1.MapHandle;
            hmapl1 = (int)axaxcMapScreen1.MapHandle;


            // *********************************************************************************************
            // Значки
            String nn;
            String nn1;
            String s;
            long iz;
            // ..............................................................................................
            // SP1
            GlobalVarLn.objFormSostG.imageList1.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\Jammer\\";
                DirectoryInfo di = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi in di.GetFiles("*.png"))
                {

                    nn = fi.Name;
                    nn1 = fi.DirectoryName + "\\" + fi.Name;
                    GlobalVarLn.objFormSostG.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi_1 in di.GetFiles("*.bmp"))
                {

                    nn = fi_1.Name;
                    nn1 = fi_1.DirectoryName + "\\" + fi_1.Name;
                    GlobalVarLn.objFormSostG.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi_1 in di.GetFiles("*.jpeg"))
                {

                    nn = fi_1.Name;
                    nn1 = fi_1.DirectoryName + "\\" + fi_1.Name;
                    GlobalVarLn.objFormSostG.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }

                //Azb
                if (GlobalVarLn.fl_Azb == 0)
                {
                  if (iz == 0) MessageBox.Show("Нет списка значков");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    if (iz == 0) MessageBox.Show("Nişan yoxdur");
                }
                else 
                {
                    if (iz == 0) MessageBox.Show("No sign list");
                }

            }
            catch
            {
                //Azb
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Нет списка значков");
                }
                else
                {
                    MessageBox.Show("Nişan yoxdur");
                }

            }
            // ..............................................................................................
            // SP2
            GlobalVarLn.objFormSostG.imageList2.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\JammerSopr\\";
                DirectoryInfo di1 = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi1 in di1.GetFiles("*.png"))
                {

                    nn = fi1.Name;
                    nn1 = fi1.DirectoryName + "\\" + fi1.Name;
                    GlobalVarLn.objFormSostG.imageList2.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi1_1 in di1.GetFiles("*.bmp"))
                {

                    nn = fi1_1.Name;
                    nn1 = fi1_1.DirectoryName + "\\" + fi1_1.Name;
                    GlobalVarLn.objFormSostG.imageList2.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi1_1 in di1.GetFiles("*.jpeg"))
                {

                    nn = fi1_1.Name;
                    nn1 = fi1_1.DirectoryName + "\\" + fi1_1.Name;
                    GlobalVarLn.objFormSostG.imageList2.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }

                //Azb
                if (GlobalVarLn.fl_Azb == 0)
                {
                    if (iz == 0) MessageBox.Show("Нет списка значков");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    if (iz == 0) MessageBox.Show("Nişan yoxdur");
                }
                else 
                {
                    if (iz == 0) MessageBox.Show("No sign list");
                }

            }
            catch
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    //Azb
                    MessageBox.Show("Нет списка значков");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    MessageBox.Show("Nişan yoxdur");
                }
                else 
                {
                    MessageBox.Show("No sign list");
                }

            }
            //...............................................................................................
            // PU

            GlobalVarLn.objFormSostG.imageList3.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\PU\\";
                DirectoryInfo di2 = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi2 in di2.GetFiles("*.png"))
                {

                    nn = fi2.Name;
                    nn1 = fi2.DirectoryName + "\\" + fi2.Name;
                    GlobalVarLn.objFormSostG.imageList3.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi2_1 in di2.GetFiles("*.bmp"))
                {

                    nn = fi2_1.Name;
                    nn1 = fi2_1.DirectoryName + "\\" + fi2_1.Name;
                    GlobalVarLn.objFormSostG.imageList3.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi2_1 in di2.GetFiles("*.jpeg"))
                {

                    nn = fi2_1.Name;
                    nn1 = fi2_1.DirectoryName + "\\" + fi2_1.Name;
                    GlobalVarLn.objFormSostG.imageList3.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                if (GlobalVarLn.fl_Azb == 0)
                {
                    //Azb
                    if (iz == 0) MessageBox.Show("Нет списка значков");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    if (iz == 0) MessageBox.Show("Nişan yoxdur");
                }
                else 
                {
                    if (iz == 0) MessageBox.Show("No sign list");
                }

            }
            catch
            {
                //Azb
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Нет списка значков");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    MessageBox.Show("Nişan yoxdur");
                }
                else 
                {
                    MessageBox.Show("No sign list");
                }

            }
            //...............................................................................................

            label5.Text = string.Format("S 1: {0}", Convert.ToString((int)axaxcMapScreen1.ViewScale));

            // ----------------------------------------------------------------------
            // !!! Первоначальная загрузка составы комплекса с INI файла

            // 19-11_2018
            // GPSSPPU
            //ClassMap.f_LoadSostIni();
            ClassMap.ClearSost();

            // 3
            //ClassMap.f_LoadSostIni();


            // 13_09_2018
            // !!! Значки читаем с INI файла
            GlobalVarLn.indz1_Sost = iniRW.get_ZnakSP1();
            GlobalVarLn.indz2_Sost = iniRW.get_ZnakSP2();
            GlobalVarLn.indz3_Sost = iniRW.get_ZnakPU();
            GlobalVarLn.objFormSostG.pbSP.Image = GlobalVarLn.objFormSostG.imageList1.Images[GlobalVarLn.indz1_Sost];
            GlobalVarLn.objFormSostG.pictureBox1.Image = GlobalVarLn.objFormSostG.imageList2.Images[GlobalVarLn.indz2_Sost];
            GlobalVarLn.objFormSostG.pictureBox2.Image = GlobalVarLn.objFormSostG.imageList3.Images[GlobalVarLn.indz3_Sost];

            // ----------------------------------------------------------------------
            // Флаг языка

            //if (System.IO.File.Exists(Application.StartupPath + "\\Init.ini"))
            if (System.IO.File.Exists(Application.StartupPath + "\\Settings.ini"))
            {
                GlobalVarLn.fl_Azb = (int)iniRW.get_fl_Azb();
            }
            else
            {
                //Azb
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Невозможно открыть INI файл");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    MessageBox.Show("fayl açıq deyil");
                }
                else
                {
                    MessageBox.Show("Unable to open INI file");
                }

                return;
            }

            // ---------------------------------------------------------------------
            //2501
            GlobalVarLn.list_PelIRI.Clear();
            // ---------------------------------------------------------------------
            // Инициализация секторов
            //Sect

            ClassMap.Sector_INI();
            // ---------------------------------------------------------------------



            // OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
            //777Otl-> NO delete->Otladka GPS_Sostav

            /*
                        AirPlane objs = new AirPlane();
                        objs.Lat = 54.226731; // SP1
                        objs.Long = 28.50241;
                        GlobalVarLn.lll.Add(objs);
                        objs.Lat = 54.236027; // SP2
                        objs.Long = 28.526574;
                        //objs.Lat = -1; // SP2
                        //objs.Long = -1;
                        GlobalVarLn.lll.Add(objs);
                        objs.Lat = 54.221148; // PU
                        objs.Long = 28.5427;
                        GlobalVarLn.lll.Add(objs);
            */
            //timer1.Start();
            // OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO



            CultureInfo current = System.Threading.Thread.CurrentThread.CurrentUICulture;
            if (current.TwoLetterISOLanguageName != "fr")
            {
                CultureInfo newCulture = CultureInfo.CreateSpecificCulture("en-US");
                System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture;
                // Make current UI culture consistent with current culture.
                System.Threading.Thread.CurrentThread.CurrentCulture = newCulture;
            }


        } // Загрузка головной формы
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button2: открыть окно отображения координат
        // *****************************************************************************************
        private void button2_Click(object sender, EventArgs e)
        {

            //ToolTip.("Координаты");

            if (form1 == null || form1.IsDisposed)
            {
                form1 = new Form1(ref axaxcMapScreen1);
                form1.Show();
            }
            else
            {
                form1.Show();
            }

            //// Окно отображения координат
            //Form1 form1 = new Form1(ref axaxcMapScreen1);
            //form1.Show();

        } // Button2

        // *****************************************************************************************
        // Обработчик кнопки Button3: открыть окно отображения пеленга
        // *****************************************************************************************
        private void button3_Click(object sender, EventArgs e)
        {

        } // Button3

        // *****************************************************************************************
        // Обработчик кнопки Button4: расстояние между двумя пунктами
        // *****************************************************************************************
        // GPSSPPU
        // !!! Otladka Sostav-> NO delete

        private void button4_Click(object sender, EventArgs e)

        {
        } // Button4

        // ************************************************************************************ LENA
        // Обработчик кнопки Button5: Azimuth
        // *****************************************************************************************
        private void button5_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            //GlobalVarLn.f_Open_objFormAz = 1;
            //ClassMap.f_RemoveFrm(3);

            GlobalVarLn.f_Open_objFormAz1 = 1;
            ClassMap.f_RemoveFrm(8);
            // -------------------------------------------------------------------------------------

            if (objFormAz1 == null || objFormAz1.IsDisposed)
            {
                objFormAz1 = new FormAz1(ref axaxcMapScreen1);
                objFormAz1.Show();
            }
            else
            {
                objFormAz1.Show();
            }


        } // Button5
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button6: Маршрут
        // *****************************************************************************************

        private void button6_Click(object sender, EventArgs e)
        {

            // -------------------------------------------------------------------------------------
            GlobalVarLn.f_Open_objFormWay = 1;
            ClassMap.f_RemoveFrm(2);
            // -------------------------------------------------------------------------------------

            if (objFormWay == null || objFormWay.IsDisposed)
            {
                objFormWay = new FormWay(ref axaxcMapScreen1);
                objFormWay.Show();

                // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
                // Для маршрута
                // 1-я загрузка формы

                // .....................................................................................
                // Очистка dataGridView

                objFormWay.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatWay_stat; i++)
                {
                    objFormWay.dataGridView1.Rows.Add("", "", "");
                }
                // .....................................................................................
                if (objFormWay.chbWay.Checked == true)
                {
                    GlobalVarLn.blWay_stat = true;
                    GlobalVarLn.flEndWay_stat = 1;
                }

                // way
                //Array.Clear(GlobalVarLn.mas_LW_stat, 0, 10000);
                //Array.Clear(GlobalVarLn.mas_XW_stat, 0, 10000);
                //Array.Clear(GlobalVarLn.mas_YW_stat, 0, 10000);
                GlobalVarLn.list_way.Clear();

                GlobalVarLn.iW_stat = 0;
                GlobalVarLn.X_StartW_stat = 0;
                GlobalVarLn.Y_StartW_stat = 0;
                GlobalVarLn.LW_stat = 0;
                // .....................................................................................

                // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormWay.Show();

                // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
                // Для маршрута
                // НЕ 1-я загрузка формы

                if (objFormWay.chbWay.Checked == true)
                {
                    GlobalVarLn.blWay_stat = true;
                    GlobalVarLn.flEndWay_stat = 1;
                }
                // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

            }
            // -------------------------------------------------------------------------------------


        } // Button6
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button7: Расчет энергодоступности по зонам 
        // *****************************************************************************************

        private void button7_Click(object sender, EventArgs e)
        {

            /*
                        // -------------------------------------------------------------------------------------
                        GlobalVarLn.f_Open_objZonePowerAvail = 1;
                        ClassMap.f_RemoveFrm(5);
                        // -------------------------------------------------------------------------------------

                        if (objZonePowerAvail == null || objZonePowerAvail.IsDisposed)
                        {
                            objZonePowerAvail = new ZonePowerAvail(ref axaxcMapScreen1);
                            objZonePowerAvail.Show();
                        }
                        else
                        {
                            objZonePowerAvail.Show();
                        }
            */

            // -------------------------------------------------------------------------------------
            GlobalVarLn.f_Open_objFormSuppression = 1;
            ClassMap.f_RemoveFrm(9);
            // -------------------------------------------------------------------------------------

            if (objFormSuppression == null || objFormSuppression.IsDisposed)
            {
                objFormSuppression = new FormSuppression(ref axaxcMapScreen1);
                objFormSuppression.Show();
            }
            else
            {
                objFormSuppression.Show();
            }
            // -------------------------------------------------------------------------------------

        } // Button7
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button8: Расчет энергодоступности по зонам для УС
        // *****************************************************************************************

        private void button8_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.f_Open_ObjCommPowerAvail = 1;
            ClassMap.f_RemoveFrm(6);
            // -------------------------------------------------------------------------------------

            if (ObjCommPowerAvail == null || ObjCommPowerAvail.IsDisposed)
            {
                ObjCommPowerAvail = new Form2(ref axaxcMapScreen1);
                ObjCommPowerAvail.Show();
            }
            else
            {
                ObjCommPowerAvail.Show();
            }

            //Form2 ObjCommPowerAvail = new Form2(ref axaxcMapScreen1);
            //ObjCommPowerAvail.Show();


        } // Button8
          // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button9: Самолеты
        // *****************************************************************************************

        //        private void button9_Click(object sender, EventArgs e)
        //        {




        /*
                    if (ObjFormAirPlane == null || ObjFormAirPlane.IsDisposed)
                    {
                        ObjFormAirPlane = new FormAirPlane(ref axaxcMapScreen1);
                        ObjFormAirPlane.Show();
                    }
                    else
                    {
                        ObjFormAirPlane.Show();
                    }
        */
        //FormAirPlane ObjFormAirPlane = new FormAirPlane(ref axaxcMapScreen1);
        //ObjFormAirPlane.Show();

        //        } // Button9
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button10: ЗПВ
        // *****************************************************************************************

        private void button10_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.f_Open_ObjFormLineSightRange = 1;
            ClassMap.f_RemoveFrm(4);
            // -------------------------------------------------------------------------------------

            if (ObjFormLineSightRange == null || ObjFormLineSightRange.IsDisposed)
            {
                ObjFormLineSightRange = new FormLineSightRange(ref axaxcMapScreen1);
                ObjFormLineSightRange.Show();
            }
            else
            {
                ObjFormLineSightRange.Show();
            }


        } // Button10
        // *****************************************************************************************
        // Воздушная обстановка
        // *****************************************************************************************

        private void chbair_CheckedChanged(object sender, EventArgs e)
        {
            blAirObject = chbair.Checked;

            if (blAirObject == false)
            {
                GlobalVarLn.fl_AirPlaneVisible = 0;

                // 3
                //if (GlobalVarLn.list_air.Count != 0)
                if (GlobalVarLn.list_ClassAirPlane.Count != 0)
                {
                        // Убрать с карты
                        GlobalVarLn.axMapScreenGlobal.Repaint();
                }
            }

            else if (blAirObject == true)
            {
                GlobalVarLn.fl_AirPlaneVisible = 1;

                // 3
                //if (GlobalVarLn.list_air.Count != 0)
                if (GlobalVarLn.list_ClassAirPlane.Count != 0)
                {
                    // Убрать с карты and redraw
                    GlobalVarLn.axMapScreenGlobal.Repaint();
                }

            }

            //chbair1.Checked = blAirObject;

        }
        // *****************************************************************************************

        // Птичка пеленгов *************************************************************************

        private void chbPeleng_CheckedChanged(object sender, EventArgs e)
        {
            // Checked=false -----------------------------------------------------------------------

            if (chbPeleng.Checked == false)
            {
                GlobalVarLn.chbpfrh = false;

                // 14_09_2018
                GlobalVarLn.PrevP1 = -1;
                GlobalVarLn.PrevP2 = -1;

                GlobalVarLn.flPelMain = 0;
                GlobalVarLn.flPelMain2 = 0;

                // 14_09_2018
                GlobalVarLn.flag_eq_draw1 = 0;
                GlobalVarLn.flag_eq_draw2 = 0;

                // 14_09_2018
                GlobalVarLn.fl_PelIRI_1 = -1;
                // .................................................................
                // FOR

                for (int iii = (GlobalVarLn.list_PelIRI.Count - 1); iii >= 0; iii--)
                {


                    PelIRI[] mass = GlobalVarLn.list_PelIRI.ToArray();
                    mass[iii].Pel1 = -1;
                    mass[iii].Pel2 = -1;
                    GlobalVarLn.list_PelIRI = mass.ToList();

                    if (
                        (GlobalVarLn.list_PelIRI[iii].Pel1 == -1) &&
                        (GlobalVarLn.list_PelIRI[iii].Pel2 == -1) &&
                        (GlobalVarLn.list_PelIRI[iii].Lat == -1) &&
                        (GlobalVarLn.list_PelIRI[iii].Long == -1)
                       )
                    {
                        GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii]);
                    }
                } // FOR
                // .................................................................

                // Убрать с карты
                GlobalVarLn.axMapScreenGlobal.Repaint();
            }
            // ----------------------------------------------------------------------- Checked=false

            // Checked = true ----------------------------------------------------------------------
            else
            {
                GlobalVarLn.chbpfrh = true;

                GlobalVarLn.flPelMain = 1;
                // 14_09_2018
                GlobalVarLn.flag_eq_draw1 = 1;
                GlobalVarLn.flag_eq_draw2 = 1;
            }
            // ---------------------------------------------------------------------- Checked = true

        } // ZPel
        // ************************************************************************* Птичка пеленгов

        // *****************************************************************************************
        // Слои
        // *****************************************************************************************

        private void составКартыToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (axaxMapSelDlg.Execute(axaxcMapScreen1.ViewSelect, false) == true)
                axaxcMapScreen1.Invalidate();
        } // Слои

        // *****************************************************************************************

        // *****************************************************************************************
        // Ввод состава комплекса
        // *****************************************************************************************
        private void button12_Click(object sender, EventArgs e)
        {
            GlobalVarLn.f_Open_objFormSost = 1;
            ClassMap.f_RemoveFrm(1);

            if (objFormSost == null || objFormSost.IsDisposed)
            {
                objFormSost = new FormSost(ref axaxcMapScreen1);
                objFormSost.Show();
            }
            else
            {
                objFormSost.Show();
            }


            //ClassMap.f_LoadSostIni();

        } // Sostav
        // *****************************************************************************************

        // *****************************************************************************************
        // Зоны пеленгования
        // *****************************************************************************************
        private void bZoneDirectFind_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.f_Open_objFormBearing = 1;
            ClassMap.f_RemoveFrm(7);
            // -------------------------------------------------------------------------------------

            if (objFormBearing == null || objFormBearing.IsDisposed)
            {
                objFormBearing = new FormBearing(ref axaxcMapScreen1);
                objFormBearing.Show();
            }
            else
            {
                objFormBearing.Show();
            }
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ;
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        // **************************************************************************************       
        // навестись на АСП
        // **************************************************************************************       

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            double lt = 0;
            double ln = 0;

            lt = GlobalVarLn.XCenter_Sost;
            ln = GlobalVarLn.YCenter_Sost;


            if (
                ((GlobalVarLn.XCenter_Sost == 0) || (GlobalVarLn.YCenter_Sost == 0)) &&
                ((GlobalVarLn.XPoint1_Sost == 0) || (GlobalVarLn.YPoint1_Sost == 0)) &&
                ((GlobalVarLn.XPoint1_Sost == 0) || (GlobalVarLn.YPoint1_Sost == 0))
               )
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    MessageBox.Show("Нет координат");
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    //Azb?
                    MessageBox.Show("Koordinatlar yoxdur");
                }
                else 
                {
                    MessageBox.Show("No coordinates");
                }

                return;
            }

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref lt, ref ln);

            axaxcMapScreen1.MapLeft = (int)(lt - axaxcMapScreen1.Width / 2);
            axaxcMapScreen1.MapTop = (int)(ln - axaxcMapScreen1.Height / 2);

            // ................................................................................
            //Sect  АСП

            int fff = 0;
            if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            {
                fff = 1;
                GlobalVarLn.flsect = 0;
            }

            axaxcMapScreen1.Repaint();

            //Sect
            if (fff == 1)
            {
                GlobalVarLn.flsect = 1;
                ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
            }
            // ................................................................................

        } // Point_ASP
        // **************************************************************************************       



        //
        private void MapForm_Scroll(object sender, ScrollEventArgs e)
        {
            ;
        }

        // ***************************************************************************************
        // ScrollBar
        // ??? Приходит два раза подряд 
        // e.wPARAM=0(Up)/1(Down)/8(2-й раз)???


        // ---------------------------------------------------------------------------------------
        private void axaxcMapScreen1_OnVScroll(object sender, IaxMapScreenEvents_OnVScrollEvent e)
        {
            //int i = 0;
            //GlobalVarLn.shyyy += 1;
            //e.lPARAM = e.lPARAM;
            //e.wPARAM = e.wPARAM;
            //e.msgResult = e.msgResult;
            //e.message = e.message;
            //if(e.wPARAM==8)
            //  i = 10;
            //if (GlobalVarLn.shyyy >= 10)
            //    i = 20;


            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            /*
                        GlobalVarLn.xxxY++;

                        if (GlobalVarLn.xxxY == 1)
                        {
                            GlobalVarLn.fl_scrb = 1;
                            //GlobalVarLn.axMapScreenGlobal.Repaint();
                        }


                        if (GlobalVarLn.xxxY == 2)
                        {
                            GlobalVarLn.xxxY = 0;
                            GlobalVarLn.axMapScreenGlobal.Repaint();
                        }
             */
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            //Sect VScroll

            //int fff = 0;
            //if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            //{
            //    fff = 1;
            //    GlobalVarLn.flsect = 0;
            //}

            //axaxcMapScreen1.Repaint();

            //Sect
            //if (fff == 1)
            //{
            //    GlobalVarLn.flsect = 1;
            //    ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
            //}
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            if ((chbSect.Checked == true) && (GlobalVarLn.fl_PelMain == 1))
                GlobalVarLn.flscrolsect = 1;

            if ((chbSect.Checked == true) && (GlobalVarLn.fl_PelMain == 0))
            {
                axaxcMapScreen1.Repaint();
            }

        }
        // ---------------------------------------------------------------------------------------

        private void axaxcMapScreen1_OnHScroll(object sender, IaxMapScreenEvents_OnHScrollEvent e)
        {
            //int i = 0;
            //GlobalVarLn.shyyy += 1;
            //e.lPARAM = e.lPARAM;
            //e.wPARAM = e.wPARAM;
            //e.masResult = e.masResult;
            //e.message = e.message;
            //if(e.wPARAM!=0)
            //i = 10;
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            /*
                        GlobalVarLn.xxxY1++;
                        if (GlobalVarLn.xxxY1 == 1)
                        {
                            GlobalVarLn.fl_scrb = 1;
                        }
                        if (GlobalVarLn.xxxY1 == 2)
                        {
                            GlobalVarLn.xxxY1 = 0;
                            GlobalVarLn.axMapScreenGlobal.Repaint();
                        }
             */
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            //Sect HScroll

            //int fff = 0;
            //if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            //{
            //   fff = 1;
            //   GlobalVarLn.flsect = 0;
            //}

            //axaxcMapScreen1.Repaint();

            //Sect
            //if (fff == 1)
            //{
            //    GlobalVarLn.flsect = 1;
            //    ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
            //}
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            if ((chbSect.Checked == true) && (GlobalVarLn.fl_PelMain == 1))
                GlobalVarLn.flscrolsect = 1;

            if ((chbSect.Checked == true) && (GlobalVarLn.fl_PelMain == 0))
            {
                axaxcMapScreen1.Repaint();
            }
        }
        // ---------------------------------------------------------------------------------------

        // ***************************************************************************************

        private void MapForm_MouseDown(object sender, MouseEventArgs e)
        {
            ;
        }

        // **************************************************************************************       


        // ************************************************************************************ LENA

        // LinaPasha************************************************************************************ 

        // ************************************************************************************LinaPasha 

        private void LanguageChooser()
        {
            var cont = this.Controls;

            if (NumberOfLanguage.Equals(0))
            {
                ChangeLanguageToRu(cont);
                this.Text = "Карта";
            }
            if (NumberOfLanguage.Equals(1))
            {
                //ChangeLanguageToEng(cont);
            }
            if (NumberOfLanguage.Equals(2))
            {
                ChangeLanguageToAzer(cont);
                this.Text = "Xəritə";
            }

        }

        private void ChangeLanguageToRu(System.Windows.Forms.Control.ControlCollection cont)
        {
            View.Text = "Упрощенный вид карты";
            label6.Text = "БД";
            button1.Text = "Проверка";
            toolTip1.SetToolTip(button10, " Зона прямой видимости (ЗПВ)");
            button11.Text = "Слои";
            toolTip1.SetToolTip(button12, "Состав комплекса");
            button2.Text = "XYZ";
            toolTip1.SetToolTip(button2, "Координаты");
            button3.Text = "Пел";
            button4.Text = "S";
            toolTip1.SetToolTip(button5, "Азимут");
            //button6.Text = "";
            toolTip1.SetToolTip(button6, "Маршрут");
            toolTip1.SetToolTip(button7, "Расчет энергодоступности по зонам");
            toolTip1.SetToolTip(button8, "Расчет энергодоступности по узлам связи");
            toolTip1.SetToolTip(bZoneDirectFind, "Зона ошибок местоопределения");
            chbair.Text = "Воздушная обстановка";
            chbPeleng.Text = "Пеленг";
            closeHeightMatrixToolStripMenuItem.Text = "Закрыть матрицу высот";
            closeMapToolStripMenuItem.Text = "Закрыть карту";
            fileToolStripMenuItem.Text = "Карта";
            menuStrip1.Text = "menuStrip1";
            openHeightMatrixToolStripMenuItem.Text = "Открыть матрицу высот";
            openMapToolStripMenuItem.Text = "Открыть карту";
            statusStrip1.Text = "statusStrip1";
            toolStripButton1.Text = "АСП";
            ZoomIn.Text = "Увеличить масштаб";
            ZoomInitial.Text = "Исходный масштаб";
            ZoomOut.Text = "Уменьшить масштаб";
            настройкиToolStripMenuItem.Text = "Настройки";
            составКартыToolStripMenuItem.Text = "Состав карты";
            chbSect.Text = "Траектории воздушных судов";
        }

        private void ChangeLanguageToAzer(System.Windows.Forms.Control.ControlCollection cont)
        {

            button1.Text = "Check";
            toolTip1.SetToolTip(button10, "Birbaşa görünüş zonası");
            label6.Text = "VB";
            button11.Text = "Laylar";
            toolTip1.SetToolTip(button12, "Kompleksin tərkibi ");
            button2.Text = "XYZ";
            toolTip1.SetToolTip(button2, "Koordinatlar");
            button3.Text = "Pelenq";
            button4.Text = "S";
            toolTip1.SetToolTip(button5, "Azimut ");
            //button6.Text = "";
            toolTip1.SetToolTip(button6, "Marşrut");
            toolTip1.SetToolTip(button7, "Zonalar üzrə qidalandırılmanın hesabatı");
            toolTip1.SetToolTip(button8, "Rabitə üzrə elektrik gücünün bölüşdürülməs hesabı");
            toolTip1.SetToolTip(bZoneDirectFind, "Yerin müəyyənləşdirilməsində səhv sahəsi");

            chbair.Text = "Hava vəziyyəti";
            chbPeleng.Text = "Pelenq";
            closeHeightMatrixToolStripMenuItem.Text = "Hündürlük matrisasını bağla";
            closeMapToolStripMenuItem.Text = "Xəritəni bağla";
            fileToolStripMenuItem.Text = "Xəritə";
            menuStrip1.Text = "menuStrip1";
            openHeightMatrixToolStripMenuItem.Text = "Hündürlük matrisalarını aç ";
            openMapToolStripMenuItem.Text = "Xəritəni aç";
            statusStrip1.Text = "statusStrip1";
            toolStripButton1.Text = "AMS";
            ZoomIn.Text = "Miqyası böyüt";
            ZoomInitial.Text = "Ilkin vəziyyət";
            ZoomOut.Text = "Miqyası azalt";

            настройкиToolStripMenuItem.Text = "Tənzimləmə";
            составКартыToolStripMenuItem.Text = "Xəritənin tərkibi";
            chbSect.Text = "anten istiqaməti";
        }

        // SECTOR_EVENTS ********************************************************************
        //Sect Events

        private void MapForm_AutoSizeChanged(object sender, EventArgs e)
        {
            int i = 0;
            i = i;
        }

        // --------------------------------------------------------------------------------------
        // Нажатие в верхнем правом углу формы квадратика "Растянуть на весь экран" (WindowsState=Maximized)
        // Нажатие в верхнем правом углу формы квадратикa "Стянуть" (WindowsState=Normal)
        // Нажатие в верхнем правом углу формы черточки "Стянуть вниз экрана" (WindowsState=Minimized)
        // Вернуть форму с низа экрана на место (WindowsState=Normal/Maximized)
        // --------------------------------------------------------------------------------------

        private void MapForm_Resize(object sender, EventArgs e)
        {

            // ................................................................................
            //Sect 

            //if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            if (chbSect.Checked == true)
            {
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                if (this.WindowState == FormWindowState.Maximized)
                {
                    // Нажатие в верхнем правом углу формы квадратика "Растянуть на весь экран"
                    if ((GlobalVarLn.ffstate == 0) || (GlobalVarLn.ffstate == 3))
                    {
                        GlobalVarLn.flsect = 0;
                        axaxcMapScreen1.Repaint();
                        GlobalVarLn.flsect = 1;
                        ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);

                        GlobalVarLn.ffstate = 1;
                        return;
                    }

                    // Вернуть форму с низа экрана на место
                    if (GlobalVarLn.ffstate == 2)
                    {

                        GlobalVarLn.flsect = 0;
                        axaxcMapScreen1.Repaint();
                        GlobalVarLn.flsect = 1;
                        ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);

                        GlobalVarLn.ffstate = 1;
                        return;
                    }


                } // Max

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                else if (this.WindowState == FormWindowState.Normal)
                {
                    // Нажатие в верхнем правом углу формы квадратика "Стянуть экран обратно"
                    if (GlobalVarLn.ffstate == 1)
                    {
                        GlobalVarLn.flsect = 0;
                        axaxcMapScreen1.Repaint();
                        GlobalVarLn.flsect = 1;
                        ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);

                        GlobalVarLn.ffstate = 3;
                        return;
                    }

                    // Вернуть форму с низа экрана на место
                    if (GlobalVarLn.ffstate == 2)
                    {
                        GlobalVarLn.flsect = 0;
                        axaxcMapScreen1.Repaint();
                        GlobalVarLn.flsect = 1;
                        ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);

                        GlobalVarLn.ffstate = 3;
                        return;
                    }

                } // Norm
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                else if (this.WindowState == FormWindowState.Minimized)
                {
                    //GlobalVarLn.flsect = 0;
                    //axaxcMapScreen1.Repaint();
                    //GlobalVarLn.flsect = 1;
                    //ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);

                    // Нажатие в верхнем правом углу формы черточки "Отправить экран вниз"
                    GlobalVarLn.ffstate = 2;
                    return;

                } // Norm
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            }
            // ................................................................................

        } // Resize

        // --------------------------------------------------------------------------------------
        // Растягивание(стягивание) формы стрелкой
        // --------------------------------------------------------------------------------------
        //Sect
        // Начало действия
        private void MapForm_ResizeBegin(object sender, EventArgs e)
        {
            /*
                        if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
                        {
                            GlobalVarLn.flsect = 0;
                            axaxcMapScreen1.Repaint();
                            GlobalVarLn.flsect = 1;
                            ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
                        }
            */
        }
        // Конец действия
        private void MapForm_ResizeEnd(object sender, EventArgs e)
        {
            if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            {
                GlobalVarLn.flsect = 0;
                axaxcMapScreen1.Repaint();
                GlobalVarLn.flsect = 1;
                ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);

            }

        }
        // --------------------------------------------------------------------------------------



        private void MapForm_MouseHover(object sender, EventArgs e)
        {
            ;
        }

        private void panel1_MouseHover(object sender, EventArgs e)
        {
            ;
        }

        private void button12_MouseHover(object sender, EventArgs e)
        {
            ;

        }

        private void button12_MouseEnter(object sender, EventArgs e)
        {
            ;
        }

        private void button12_MouseLeave(object sender, EventArgs e)
        {
            if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            {
                //GlobalVarLn.flsect = 0;
                //axaxcMapScreen1.Repaint();
                //GlobalVarLn.flsect = 1;
                //ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
            }

        }

        private void button12_MouseMove(object sender, MouseEventArgs e)
        {
            ;
        }

        private void panel1_MouseLeave(object sender, EventArgs e)
        {
            ;
        }

        private void panel3_MouseEnter(object sender, EventArgs e)
        {
            ;
        }

        private void MapForm_MouseEnter(object sender, EventArgs e)
        {
            if ((GlobalVarLn.flsect == 1) && (chbSect.Checked == true))
            {
                //GlobalVarLn.flsect = 0;
                //axaxcMapScreen1.Repaint();
                //GlobalVarLn.flsect = 1;
                //ClassMap.Sector((double)GlobalVarLn.XCenter_Sost, (double)GlobalVarLn.YCenter_Sost);
            }

        }

        private void View_Click(object sender, EventArgs e)
        {
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(0));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(1));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(2));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(3));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(4));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(5));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(6));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(7));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(8));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(9));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(10));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(11));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(12));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(13));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(14));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(15));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(16));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(17));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(18));
            //Console.WriteLine(axaxcMapScreen1.ViewSelect.Layers_get(19));
            SimpleView(View.Checked);
        }

        private void SimpleView(bool value)
        {
            axaxcMapScreen1.Selecting = true;

            axaxcMapScreen1.ViewSelect.Layers_set(0, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(1, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(2, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(3, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(4, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(5, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(6, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(7, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(8, true);
            axaxcMapScreen1.ViewSelect.Layers_set(9, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(10, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(11, true);
            axaxcMapScreen1.ViewSelect.Layers_set(12, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(13, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(14, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(15, true);
            axaxcMapScreen1.ViewSelect.Layers_set(16, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(17, true);
            axaxcMapScreen1.ViewSelect.Layers_set(18, !value);
            axaxcMapScreen1.ViewSelect.Layers_set(19, !value);

            // Отключение подписей рек
            axaxcMapScreen1.ViewSelect.set_KeyObject("T0091082000", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910820001", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910820002", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910820003", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910820004", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910820005", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910820006", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910820007", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910820008", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910820009", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T009108200010", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T009108200011", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T009108200012", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T009108200013", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T009108200014", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T009108200015", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T009108200016", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T009108200017", !value);

            //Отключение рек и озёр
            axaxcMapScreen1.ViewSelect.set_KeyObject("L0031410000", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("L00314100001", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("L0031431110", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("L0031431120", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("L0031431300", !value);

            axaxcMapScreen1.ViewSelect.set_KeyObject("S0031120000", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("S00311200001", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("S0031131000", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("S00311310001", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("S0031410000", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("S00314100001", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T0092172000", !value);

            // Отключение мелких городов
            //axaxcMapScreen1.ViewSelect.set_KeyObject("P00410000006", !value);

            //Отключение подписей гор
            axaxcMapScreen1.ViewSelect.set_KeyObject("T0091050000", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910500001", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910500002", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910500003", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910500004", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910500005", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910500006", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910500007", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910500008", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910500009", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T009105000010", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00921700001", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T0092170000", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910600009", !value);

            // Отключение подписей мелких городов
            axaxcMapScreen1.ViewSelect.set_KeyObject("T0091040000", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910400001", !value);
            //axaxcMapScreen1.ViewSelect.set_KeyObject("T00910400002", !value);
            //axaxcMapScreen1.ViewSelect.set_KeyObject("T00910400003", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910400004", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910400005", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910400006", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910400007", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910400008", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00910400009", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T0091200000", !value);
            //axaxcMapScreen1.ViewSelect.set_KeyObject("T0091120000", !value);
            //axaxcMapScreen1.ViewSelect.set_KeyObject("T0091170000", !value);

            // Отключение подписей чего-то
            axaxcMapScreen1.ViewSelect.set_KeyObject("T0091130000", !value);

            // Отключение подписей островов
            axaxcMapScreen1.ViewSelect.set_KeyObject("T0093140000", !value);
            axaxcMapScreen1.ViewSelect.set_KeyObject("T00931400004", !value);

            //axaxcMapScreen1.ScreenRepaint
            axaxcMapScreen1.Repaint();
        }

        // ******************************************************************** SECTOR_EVENTS

       // DATA_BASE ***************************************************************************

        // ButtonClick >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        ClientDB clientDB;

        private void butDb_Click(object sender, EventArgs e)
        {
            try
            {
                if (clientDB != null)
                {
                    clientDB.Disconnect();
                    clientDB = null;
                }
                else
                {

                    string s = "";

                    try
                    {
                        int prt = iniRW.get_Port();
                        string s1 = iniRW.get_IPaddress();
                        s = s1 + ":" + Convert.ToString(prt);
                    }
                    catch
                    {
                        MessageBox.Show("Нет IP и номера порта");
                    }

                    GlobalVarLn.fltbl = false;
                    Name = "Map";
                    //clientDB = new ClientDB(this.Name, "127.0.0.1:8302");
                    clientDB = new ClientDB(this.Name, s);
                    InitClientDB();
                    clientDB.ConnectAsync();
                } // else
            }
            catch (ClientDataBase.Exceptions.ExceptionClient exceptClient)
            { }
            catch (Exception)
            { }

        }
        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ButtonClick

        // Init_ClientDB >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        void InitClientDB()
        {
            clientDB.OnConnect += HandlerConnect;
            clientDB.OnDisconnect += HandlerDisconnect;
            clientDB.OnErrorDataBase += HandlerErrorDataBase;
        }
        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Init_ClientDB

        // ФРЧ пеленги >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // ????????????????????????????????

        private void AddRangeTempFWS(object sender, InheritorsEventArgs.TableEventArs<TempFWS> e)
        {

            if (GlobalVarLn.fltbl == false)
                return;

            // ...............................................................
            // Пеленги

            GlobalVarLn.listTableTempFWS_Pel = e.Table;
            if (this.InvokeRequired)
                Invoke((Action)(() => { ClassMap.GetTempFWS_Pel(GlobalVarLn.listTableTempFWS_Pel); }));
            else
                ClassMap.GetTempFWS_Pel(GlobalVarLn.listTableTempFWS_Pel);

            // Otl
            // Otkommentirovat

            // ...............................................................
            // Если нет координат -> удаляем элемент из списка

            if (e.Table.Count != 0)//
            {
                for (int iij = (e.Table.Count - 1); iij >= 0; iij--)
                {
                    if ((e.Table[iij].Coordinates.Latitude == -1) || (e.Table[iij].Coordinates.Longitude == -1))
                        e.Table.Remove(e.Table[iij]);
                }
            }
            // ...............................................................

            GlobalVarLn.listTableTempFWS = e.Table;

            if (this.InvokeRequired)
                Invoke((Action)(() => { ClassMap.GetTempFWS_DB(GlobalVarLn.listTableTempFWS); }));
            else
                ClassMap.GetTempFWS_DB(GlobalVarLn.listTableTempFWS);
            // ...............................................................

            if (GlobalVarLn.flfrh == false)
            {
                // 18.07
                GlobalVarLn.flfrh = true;
                //timer2.Start();
            }
        }

        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ФРЧ пеленги

        // UpdateTablaASP >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        private void UpTableASP(object sender, InheritorsEventArgs.TableEventArs<TableASP> e)
        {
            //throw new NotImplementedException();

            // Otl
            /*
                        e.Table[0].ISOwn = true;
                        if(e.Table.Count==2)
                         e.Table[1].ISOwn = false;
            */

            if (GlobalVarLn.fltbl == false)
                return;

            GlobalVarLn.listTableASP = e.Table;

            //e.Table[0].Id  // int

            // As
            if (this.InvokeRequired)
                Invoke((Action)(() =>{ClassMap.GetASP_DB(GlobalVarLn.listTableASP);}));
            else
                ClassMap.GetASP_DB(GlobalVarLn.listTableASP);

        }
        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> UpdateTablaASP

        // UpdateTableAirplanes >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        // -----------------------------------------------------------------------------------
        List<ClassAirPlane> listAirPlane = new List<ClassAirPlane>();
        private void UpTableADSB(object sender, InheritorsEventArgs.TableEventArs<TempADSB> e)
        {
            if (GlobalVarLn.fltbl == false)
                return;

            GlobalVarLn.listTableADSB = e.Table;

            // ...............................................................

            if (this.InvokeRequired)
                Invoke((Action)(() => { ClassMap.GetADSB_DB2(GlobalVarLn.listTableADSB); }));
            else
                ClassMap.GetADSB_DB2(GlobalVarLn.listTableADSB);

            /*
                        List<string> listICAO = (from t in e.Table let icao = t.ICAO select icao).Distinct().ToList();
                        foreach (var icao in listICAO)
                        {
                            List<AirPlane2> temp = e.Table.Where(rec => rec.ICAO == icao).Select(rec => new AirPlane2() { Lat = rec.Coordinates.Latitude, Long = rec.Coordinates.Longitude }).ToList();
                            listAirPlane.Add(new ClassAirPlane() { sNum = icao, list_air2 = temp });
                        }

                        if (this.InvokeRequired)
                            Invoke((Action)(() => { ClassMap.GetADSB_DB2(GlobalVarLn.listTableADSB); }));
                        else
                            ClassMap.GetADSB_DB2(GlobalVarLn.listTableADSB);
            */
        }
        // -----------------------------------------------------------------------------------

        private void AddRecordADSB(object sender, TempADSB newRecord)
        {
            if (GlobalVarLn.fltbl == false)
                return;

            // ..................................................................................
            /*
                        if (listAirPlane.FirstOrDefault(rec => rec.sNum == newRecord.ICAO) == null)
                        {
                            listAirPlane.Add(new ClassAirPlane() { sNum = newRecord.ICAO });
                        }
                        listAirPlane.First(rec => rec.sNum == newRecord.ICAO).list_air2.Add(new AirPlane2() { Lat = newRecord.Coordinates.Latitude, Long = newRecord.Coordinates.Longitude });
            */
            // ..................................................................................
            // AAA

            if (GlobalVarLn.listTableADSB.Count != 0)
            {
                for (int i = (GlobalVarLn.listTableADSB.Count - 1); i >= 0; i--)
                    GlobalVarLn.listTableADSB.Remove(GlobalVarLn.listTableADSB[i]);
            }


            if(GlobalVarLn.flair==false)
            {
                GlobalVarLn.flair = true;
                timer1.Start();
            }

            if(
                (newRecord.Coordinates.Latitude!=-1) &&
                (newRecord.Coordinates.Longitude!=-1)&&
                (newRecord.Coordinates.Latitude!=0) &&
                (newRecord.Coordinates.Longitude!=0)
                //(newRecord.Coordinates.Latitude >= GlobalVarLn.lat_min) &&
                //(newRecord.Coordinates.Latitude <= GlobalVarLn.lat_max) &&
                //(newRecord.Coordinates.Longitude >= GlobalVarLn.lon_min) &&
                //(newRecord.Coordinates.Longitude <= GlobalVarLn.lon_max)

                )
                GlobalVarLn.listTableADSB.Add(newRecord);

            //GlobalVarLn.listTableADSB1.Add(newRecord);

            // ...............................................................

            if (this.InvokeRequired)
                Invoke((Action)(() =>
                {
                    if (GlobalVarLn.listTableADSB.Count != 0)
                        ClassMap.GetADSB_DB2(GlobalVarLn.listTableADSB);
                }));
            else
            {
                if (GlobalVarLn.listTableADSB.Count != 0)
                    ClassMap.GetADSB_DB2(GlobalVarLn.listTableADSB);
            }
            // ..................................................................................

        }
        // -----------------------------------------------------------------------------------

        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> UpdateTableAirplanes

        // UpdateTableTempFWS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // ИРИ ФРЧ
        // Пеленги ФРЧ

        private void UpTableTempFWS(object sender, InheritorsEventArgs.TableEventArs<TempFWS> e)
        {
            if (GlobalVarLn.fltbl == false)
                return;

            // ...............................................................
            // Пеленги

            GlobalVarLn.listTableTempFWS_Pel = e.Table;
            if (this.InvokeRequired)
                Invoke((Action)(() => { ClassMap.GetTempFWS_Pel(GlobalVarLn.listTableTempFWS_Pel); }));
            else
                ClassMap.GetTempFWS_Pel(GlobalVarLn.listTableTempFWS_Pel);

            // Otl
            // Otkommentirovat

            // ...............................................................
            // Если нет координат -> удаляем элемент из списка

            if (e.Table.Count != 0)
            {
                for (int iij = (e.Table.Count - 1); iij >= 0; iij--)
                {
                    if ((e.Table[iij].Coordinates.Latitude == -1) || (e.Table[iij].Coordinates.Longitude == -1))
                        e.Table.Remove(e.Table[iij]);
                }
            }
            // ...............................................................

            GlobalVarLn.listTableTempFWS = e.Table;

            if (this.InvokeRequired)
                Invoke((Action)(() => { ClassMap.GetTempFWS_DB(GlobalVarLn.listTableTempFWS); }));
            else
                ClassMap.GetTempFWS_DB(GlobalVarLn.listTableTempFWS);
            // ...............................................................

              if(GlobalVarLn.flfrh==false)
            {
                GlobalVarLn.flfrh = true;
                //timer2.Start();
            }

        }
        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> UpdateTableTempFWS

        // UpdateTableReconFHSS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // ИРИ ППРЧ

        private void UpTableTableReconFHSS(object sender, InheritorsEventArgs.TableEventArs<TableReconFHSS> e)
        {
            if (GlobalVarLn.fltbl == false)
                return;

            /*
                        // ...............................................................
                        // Если нет координат -> удаляем элемент из списка

                        // FOR1
                        for (int iij = (e.Table.Count - 1); iij >= 0; iij--)
                        {
                            // FOR2
                            for (int iij1 = (e.Table[iij].ListSource.Count - 1); iij1 >= 0; iij1--)
                            {
                                if ((e.Table[iij].ListSource[iij1].Coordinates.Latitude == -1) ||
                                    (e.Table[iij].ListSource[iij1].Coordinates.Longitude == -1))
                                {
                                    e.Table[iij].ListSource.Remove(e.Table[iij].ListSource[iij1]);
                                }

                            } // FOR2

                            if (e.Table[iij].ListSource.Count == 0)
                                e.Table.Remove(e.Table[iij]);

                        } // FOR1
                        // ...............................................................

                        GlobalVarLn.listTableTableReconFHSS = e.Table;
                        if (this.InvokeRequired)
                            Invoke((Action)(() => { ClassMap.GetTableReconFHSS_DB(GlobalVarLn.listTableTableReconFHSS); }));
                        else
                            ClassMap.GetTableReconFHSS_DB(GlobalVarLn.listTableTableReconFHSS);

                        // ...............................................................
            */

            GlobalVarLn.listTableTableReconFHSS = e.Table;

        }
        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> UpdateTableReconFHSS

        // UpdateTableSourceFHSS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // ИРИ ППРЧ

        private void UpTableTableSourceFHSS(object sender, InheritorsEventArgs.TableEventArs<TableSourceFHSS> e)
        {
            if (GlobalVarLn.fltbl == false)
                return;

            // ...............................................................
            // Если нет координат -> удаляем элемент из списка

            /*
                        // FOR1
                        for (int iij = (e.Table.Count - 1); iij >= 0; iij--)
                        {
                            // FOR2
                            for (int iij1 = (e.Table[iij].ListSource.Count - 1); iij1 >= 0; iij1--)
                            {
                                if ((e.Table[iij].ListSource[iij1].Coordinates.Latitude == -1) ||
                                    (e.Table[iij].ListSource[iij1].Coordinates.Longitude == -1))
                                {
                                    e.Table[iij].ListSource.Remove(e.Table[iij].ListSource[iij1]);
                                }

                            } // FOR2

                            if (e.Table[iij].ListSource.Count == 0)
                                e.Table.Remove(e.Table[iij]);

                        } // FOR1
            */

            // FOR1
            if (e.Table.Count != 0)
            {
                for (int iij1 = (e.Table.Count - 1); iij1 >= 0; iij1--)
                {
                    if ((e.Table[iij1].Coordinates.Latitude == -1) ||
                        (e.Table[iij1].Coordinates.Longitude == -1))
                    {
                        e.Table.Remove(e.Table[iij1]);
                    }

                } // FOR1
            }
            // ...............................................................

            GlobalVarLn.listTableTableSourceFHSS = e.Table;
            // ...............................................................

            // Clear
            if (GlobalVarLn.list_ClassTableReconFHSS_Dop1.Count != 0)
            {
                for (int iiij5 = (GlobalVarLn.list_ClassTableReconFHSS_Dop1.Count - 1); iiij5 >= 0; iiij5--)
                {
                    GlobalVarLn.list_ClassTableReconFHSS_Dop1.Remove(GlobalVarLn.list_ClassTableReconFHSS_Dop1[iiij5]);
                }
            }

            // Переписываем принятые таблицы в свой массив
            // FOR2
            for (int iiij2 = 0; iiij2 < GlobalVarLn.listTableTableReconFHSS.Count; iiij2++)
            {
                ClassTableReconFHSS_Dop objcl = new ClassTableReconFHSS_Dop();
                objcl.lst_PelIRI2 = new List<PelIRI>();
                objcl.Id = GlobalVarLn.listTableTableReconFHSS[iiij2].Id;
                objcl.FreqMinKHz = GlobalVarLn.listTableTableReconFHSS[iiij2].FreqMinKHz;
                objcl.FreqMaxKHz = GlobalVarLn.listTableTableReconFHSS[iiij2].FreqMaxKHz;

                // Создаем пачку ИРИ с одним основным ID
                // FOR3
                if (GlobalVarLn.listTableTableSourceFHSS.Count != 0)
                {
                    for (int iiij3 = (GlobalVarLn.listTableTableSourceFHSS.Count - 1); iiij3 >= 0; iiij3--)
                    {
                        if (GlobalVarLn.listTableTableSourceFHSS[iiij3].IdFHSS == objcl.Id)
                        {
                            PelIRI objpl = new PelIRI();
                            objpl.Lat = GlobalVarLn.listTableTableSourceFHSS[iiij3].Coordinates.Latitude;
                            objpl.Long = GlobalVarLn.listTableTableSourceFHSS[iiij3].Coordinates.Longitude;
                            objpl.Num = GlobalVarLn.listTableTableSourceFHSS[iiij3].Id;
                            objcl.lst_PelIRI2.Add(objpl);
                            GlobalVarLn.listTableTableSourceFHSS.Remove(GlobalVarLn.listTableTableSourceFHSS[iiij3]);
                        }

                    } // FOR3
                }

                if (objcl.lst_PelIRI2.Count != 0)
                    GlobalVarLn.list_ClassTableReconFHSS_Dop1.Add(objcl);

            } // FOR2
            // ...............................................................

            if (this.InvokeRequired)
                Invoke((Action)(() => { ClassMap.GetTableReconFHSS_DB(GlobalVarLn.list_ClassTableReconFHSS_Dop1); }));
            else
                ClassMap.GetTableReconFHSS_DB(GlobalVarLn.list_ClassTableReconFHSS_Dop1);

            // ...............................................................

        }
        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> UpdateTableSourceFHSS

        // ErrorDB >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        private void HandlerErrorDataBase(object sender, InheritorsEventArgs.OperationTableEventArgs e)
        {
            // As
            if (this.InvokeRequired)
                this.Invoke((Action)(() => { MessageBox.Show(e.GetMessage); }));
            else
                MessageBox.Show(e.GetMessage);


        }
        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ErrorDB

        // Disconnect >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        private void HandlerDisconnect(object sender, InheritorsEventArgs.ClientEventArgs e)
        {
            if(clientDB != null)
            {
                // Updated table of ASP (СП)
                (clientDB.Tables[NameTable.TableASP] as ITableUpdate<TableASP>).OnUpTable -= UpTableASP;

                // Update table of ADSB (Airplanes)
                (clientDB.Tables[NameTable.TempADSB] as ITableUpdate<TempADSB>).OnUpTable -= UpTableADSB;
                (clientDB.Tables[NameTable.TempADSB] as ITableUpRecord<TempADSB>).OnAddRecord -= AddRecordADSB;

                // Update table of ИРИ ФРЧ
                (clientDB.Tables[NameTable.TempFWS] as ITableUpdate<TempFWS>).OnUpTable -= UpTableTempFWS;

                (clientDB.Tables[NameTable.TempFWS] as ITableAddRange<TempFWS>).OnAddRange -= AddRangeTempFWS;

                // Update table of ИРИ ППРЧ
                (clientDB.Tables[NameTable.TableReconFHSS] as ITableUpdate<TableReconFHSS>).OnUpTable -= UpTableTableReconFHSS;

                // Update table of ИРИ ППРЧ
                (clientDB.Tables[NameTable.TableSourceFHSS] as ITableUpdate<TableSourceFHSS>).OnUpTable -= UpTableTableSourceFHSS;
            }

            timer1.Stop();
            timer2.Stop();
            GlobalVarLn.flair = false;

            // As
            if (butDb.InvokeRequired)
                butDb.Invoke((Action)(() => { butDb.BackColor = System.Drawing.Color.Red; }));
            else
                butDb.BackColor = System.Drawing.Color.Red;

            //GlobalVarLn.bOnDB = false;
        }
        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Disconnect

        // Connect >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // Load of tables 

        // As
        private async void HandlerConnect(object sender, InheritorsEventArgs.ClientEventArgs e)
        {
            // As
            if (butDb.InvokeRequired)
                butDb.Invoke((Action)(() =>{ butDb.BackColor = Color.LightGreen; }));
            else
                butDb.BackColor = Color.LightGreen;
            //GlobalVarLn.bOnDB = true;

            // -----------------------------------------------------------------
            // JS

            var stations = await clientDB.Tables[NameTable.TableASP].LoadAsync<TableASP>();

            GlobalVarLn.listTableASP = stations;

            // Otl
/*
            if (GlobalVarLn.listTableASP.Count != 0)
            {
                GlobalVarLn.listTableASP[0].ISOwn = true;
                if (GlobalVarLn.listTableASP.Count == 2)
                    GlobalVarLn.listTableASP[1].ISOwn = false;
            }
*/
            ClassMap.GetASP_DB(GlobalVarLn.listTableASP);

            // -----------------------------------------------------------------
            // Airplanes

            // .................................................................
            var airplanes = await clientDB.Tables[NameTable.TempADSB].LoadAsync<TempADSB>();
            // .................................................................

            /*
                        List<string> listICAO = (from t in airplanes let icao = t.ICAO select icao).Distinct().ToList();

                        foreach (var icao in listICAO)
                        {
                            List<AirPlane2> temp = airplanes.Where(rec => rec.ICAO == icao).Select(rec => new AirPlane2() { Lat = rec.Coordinates.Latitude, Long = rec.Coordinates.Longitude }).ToList();
                            listAirPlane.Add(new ClassAirPlane() { sNum = icao, list_air2 = temp });
                        }
            */
            // .................................................................

            // Otl .............................................................
            // ............................................................. Otl

            GlobalVarLn.listTableADSB = airplanes;

            // ...............................................................
            // Если нет координат -> удаляем элемент из списка

            // AAA

            if (GlobalVarLn.listTableADSB.Count != 0)
            {
                for (int iijl = (GlobalVarLn.listTableADSB.Count - 1); iijl >= 0; iijl--)
                {
                    if (
                        (GlobalVarLn.listTableADSB[iijl].Coordinates.Latitude == -1) ||
                        (GlobalVarLn.listTableADSB[iijl].Coordinates.Longitude == -1)||
                        (GlobalVarLn.listTableADSB[iijl].Coordinates.Latitude == 0)||
                        (GlobalVarLn.listTableADSB[iijl].Coordinates.Longitude == 0)
                        //(GlobalVarLn.listTableADSB[iijl].Coordinates.Latitude > GlobalVarLn.lat_max) ||
                        //(GlobalVarLn.listTableADSB[iijl].Coordinates.Latitude < GlobalVarLn.lat_min) ||
                        //(GlobalVarLn.listTableADSB[iijl].Coordinates.Longitude > GlobalVarLn.lon_max) ||
                        //(GlobalVarLn.listTableADSB[iijl].Coordinates.Longitude < GlobalVarLn.lon_min) 
                        )
                        GlobalVarLn.listTableADSB.Remove(GlobalVarLn.listTableADSB[iijl]);
                }
            }

            // ...............................................................

            ClassMap.GetADSB_DB2(GlobalVarLn.listTableADSB);
            // .................................................................

            // -----------------------------------------------------------------
            // ИРИ ФРЧ

            var tempfws = await clientDB.Tables[NameTable.TempFWS].LoadAsync<TempFWS>();

            //tempfws[0].ListQ

            // Otl ----------------------------------------------------------
            // ---------------------------------------------------------- Otl

            // ...............................................................
            // Пеленги

            // Otl ----------------------------------------------------------
            // ---------------------------------------------------------- Otl

            GlobalVarLn.listTableTempFWS_Pel = tempfws;
            ClassMap.GetTempFWS_Pel(GlobalVarLn.listTableTempFWS_Pel);


            // Otl
            // Otkommenirovat

            // ...............................................................
            // Если нет координат -> удаляем элемент из списка

            if (tempfws.Count != 0)
            {
                for (int iij = (tempfws.Count - 1); iij >= 0; iij--)
                {
                    if ((tempfws[iij].Coordinates.Latitude == -1) || (tempfws[iij].Coordinates.Longitude == -1))
                        tempfws.Remove(tempfws[iij]);
                }
            }
            // ...............................................................
            // Если есть элементы с одинаковыми координатами
            // ...............................................................
            GlobalVarLn.listTableTempFWS = tempfws;

            ClassMap.GetTempFWS_DB(GlobalVarLn.listTableTempFWS);
            // ...............................................................

            // -----------------------------------------------------------------
            // ИРИ ППРЧ

            var tablereconfhss = await clientDB.Tables[NameTable.TableReconFHSS].LoadAsync<TableReconFHSS>();
            var tablesourcefhss = await clientDB.Tables[NameTable.TableSourceFHSS].LoadAsync<TableSourceFHSS>();


            // Otl ----------------------------------------------------------
            // Две пачки

            // ---------------------------------------------------------- Otl

            // ...............................................................
            // Если нет координат -> удаляем элемент из списка

            /*
                        // FOR1
                        for (int iiij = (tablereconfhss.Count - 1); iiij >= 0; iiij--)
                        {
                            // FOR2
                            for (int iiij1 = (tablereconfhss[iiij].ListSource.Count - 1); iiij1 >= 0; iiij1--)
                            {
                                if ((tablereconfhss[iiij].ListSource[iiij1].Coordinates.Latitude == -1) ||
                                    (tablereconfhss[iiij].ListSource[iiij1].Coordinates.Longitude == -1))
                                {
                                    tablereconfhss[iiij].ListSource.Remove(tablereconfhss[iiij].ListSource[iiij1]);
                                }

                            } // FOR2

                            if (tablereconfhss[iiij].ListSource.Count == 0)
                                tablereconfhss.Remove(tablereconfhss[iiij]);

                        } // FOR1
            */
            // ...............................................................

            // FOR1
            if (tablesourcefhss.Count != 0)
            {
                for (int iiij1 = (tablesourcefhss.Count - 1); iiij1 >= 0; iiij1--)
                {
                    if ((tablesourcefhss[iiij1].Coordinates.Latitude == -1) ||
                        (tablesourcefhss[iiij1].Coordinates.Longitude == -1))
                    {
                        tablesourcefhss.Remove(tablesourcefhss[iiij1]);
                    }

                } // FOR1
            }
            // ...............................................................
            GlobalVarLn.listTableTableReconFHSS = tablereconfhss;
            GlobalVarLn.listTableTableSourceFHSS = tablesourcefhss;
            // ...............................................................
            // Переписываем принятые таблицы в свой массив

            if (GlobalVarLn.list_ClassTableReconFHSS_Dop1.Count != 0)
            {
                // Clear
                for (int iiij5 = (GlobalVarLn.list_ClassTableReconFHSS_Dop1.Count - 1); iiij5 >= 0; iiij5--)
                {
                    GlobalVarLn.list_ClassTableReconFHSS_Dop1.Remove(GlobalVarLn.list_ClassTableReconFHSS_Dop1[iiij5]);
                }
            }

            // FOR2
            for (int iiij2 =0; iiij2 < GlobalVarLn.listTableTableReconFHSS.Count; iiij2++)
            {
                ClassTableReconFHSS_Dop objcl = new ClassTableReconFHSS_Dop();
                objcl.lst_PelIRI2=new List<PelIRI>();
                objcl.Id = GlobalVarLn.listTableTableReconFHSS[iiij2].Id;
                objcl.FreqMinKHz = GlobalVarLn.listTableTableReconFHSS[iiij2].FreqMinKHz;
                objcl.FreqMaxKHz = GlobalVarLn.listTableTableReconFHSS[iiij2].FreqMaxKHz;

                // Создаем пачку ИРИ с одним основным ID
                // FOR3
                if (GlobalVarLn.listTableTableSourceFHSS.Count != 0)
                {
                    for (int iiij3 = (GlobalVarLn.listTableTableSourceFHSS.Count - 1); iiij3 >= 0; iiij3--)
                    {
                        if (GlobalVarLn.listTableTableSourceFHSS[iiij3].IdFHSS == objcl.Id)
                        {
                            PelIRI objpl = new PelIRI();
                            objpl.Lat = GlobalVarLn.listTableTableSourceFHSS[iiij3].Coordinates.Latitude;
                            objpl.Long = GlobalVarLn.listTableTableSourceFHSS[iiij3].Coordinates.Longitude;
                            objpl.Num = GlobalVarLn.listTableTableSourceFHSS[iiij3].Id;
                            objcl.lst_PelIRI2.Add(objpl);
                            GlobalVarLn.listTableTableSourceFHSS.Remove(GlobalVarLn.listTableTableSourceFHSS[iiij3]);
                        }

                    } // FOR3
                }

                if (objcl.lst_PelIRI2.Count != 0)
                    GlobalVarLn.list_ClassTableReconFHSS_Dop1.Add(objcl);

            } // FOR2
            // ...............................................................

            ClassMap.GetTableReconFHSS_DB(GlobalVarLn.list_ClassTableReconFHSS_Dop1);
            // ...............................................................

            // -----------------------------------------------------------------
            GlobalVarLn.fltbl = true;

            // Updated table of ASP (СП)
            (clientDB.Tables[NameTable.TableASP] as ITableUpdate<TableASP>).OnUpTable += UpTableASP;

            // Update table of ADSB (Airplanes)
            (clientDB.Tables[NameTable.TempADSB] as ITableUpdate<TempADSB>).OnUpTable += UpTableADSB;
            (clientDB.Tables[NameTable.TempADSB] as ITableUpRecord<TempADSB>).OnAddRecord += AddRecordADSB;

            // Update table of ИРИ ФРЧ
            (clientDB.Tables[NameTable.TempFWS] as ITableUpdate<TempFWS>).OnUpTable += UpTableTempFWS;

            (clientDB.Tables[NameTable.TempFWS] as ITableAddRange<TempFWS>).OnAddRange += AddRangeTempFWS;

            // Update table of ИРИ ППРЧ
            (clientDB.Tables[NameTable.TableReconFHSS] as ITableUpdate<TableReconFHSS>).OnUpTable += UpTableTableReconFHSS;

            // Update table of ИРИ ППРЧ
            (clientDB.Tables[NameTable.TableSourceFHSS] as ITableUpdate<TableSourceFHSS>).OnUpTable += UpTableTableSourceFHSS;

        }

        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Connect

        // Tmr >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        // -------------------------------------------------------------------------------
        // Tmr1 (Airplanes)

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (
                (GlobalVarLn.fl_AirPlane == 1) &&
                (GlobalVarLn.fl_AirPlaneVisible == 1)
               )
            {
                GlobalVarLn.axMapScreenGlobal.Repaint();
            }

        } // Tmr1
        // -------------------------------------------------------------------------------
        // Tmr2 (frh)

        private void timer2_Tick(object sender, EventArgs e)
        {
            GlobalVarLn.axMapScreenGlobal.Repaint();
        }
        // -------------------------------------------------------------------------------

        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Tmr

        // *************************************************************************** DATA_BASE


    } // Class
} // Namespace

