﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace GrozaMap
{
    public partial class FormAz : Form
    {
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        //private MapForm objMapForm8;
        private AxaxcMapScreen axaxcMapScreen;

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        public double dchislo1;
        public long ichislo1;
        public double X_Coordl3_1;
        public double Y_Coordl3_1;
        public double X_Coordl3_2;
        public double Y_Coordl3_2;
        public double X_Coordl3_3;
        public double Y_Coordl3_3;
        public double Azimuth;
        public double Azimuth1;
        public double DXX3;
        public double DYY3;
        public double sect;
        // ......................................................................

        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 

        public FormAz(ref AxaxcMapScreen axaxcMapScreen1)
        {
            InitializeComponent();

            axaxcMapScreen = axaxcMapScreen1;

            dchislo1 = 0;
            ichislo1 = 0;
            X_Coordl3_1=0;
            Y_Coordl3_1=0;
            X_Coordl3_2 = 0;
            Y_Coordl3_2 = 0;
            X_Coordl3_3 = 0;
            Y_Coordl3_3 = 0;
            Azimuth=0;
            Azimuth1 = 0;
            DXX3=0;
            DYY3=0;
            sect=0;

            // ......................................................................

        } // Конструктор
        // ***********************************************************  Конструктор

        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void FormAz_Load(object sender, EventArgs e)
        {
            ;
        } // Load
        // ************************************************************************








        private void label2_Click(object sender, EventArgs e)
        {
            ;
        }

        // *****************************************************************************************
        // Обработчик кнопки Button2: SP
        // *****************************************************************************************

        private void button2_Click(object sender, EventArgs e)
        {

            // ......................................................................
            // Отображение координат карты, полученных кликом мыши на карте
            // !!! MapX1,MapY1 -> реальные координаты на местности карты в м (Plane)

            // m->km (SP)
            X_Coordl3_1 = GlobalVarLn.MapX1 / 1000;
            Y_Coordl3_1 = GlobalVarLn.MapY1 / 1000;

            ichislo1 = (long)(X_Coordl3_1 * 10000000);
            dchislo1 = ((double)ichislo1) / 10000000;
            textBox4.Text = Convert.ToString(dchislo1);   // X, карта

            ichislo1 = (long)(Y_Coordl3_1 * 10000000);
            dchislo1 = ((double)ichislo1) / 10000000;
            textBox5.Text = Convert.ToString(dchislo1);   // Y, карта

            // ......................................................................
            // Треугольник на карте

            f_Map_Pol_XY(
                         //ClassMap.MapX1,
                         //ClassMap.MapY1
                          X_Coordl3_1 * 1000,  // m
                          Y_Coordl3_1 * 1000
                         );
            // ......................................................................



        } // Button2: SP
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button1: Azimuth1
        // *****************************************************************************************

        private void button1_Click(object sender, EventArgs e)
        {

            // ......................................................................
            // Отображение координат карты, полученных кликом мыши на карте
            // !!! MapX1,MapY1 -> реальные координаты на местности карты в м (Plane)

            // m->km (SP)
            X_Coordl3_2 = GlobalVarLn.MapX1 / 1000;
            Y_Coordl3_2 = GlobalVarLn.MapY1 / 1000;

            ichislo1 = (long)(X_Coordl3_2 * 10000000);
            dchislo1 = ((double)ichislo1) / 10000000;
            textBox1.Text = Convert.ToString(dchislo1);   // X, карта

            ichislo1 = (long)(Y_Coordl3_2 * 10000000);
            dchislo1 = ((double)ichislo1) / 10000000;
            textBox2.Text = Convert.ToString(dchislo1);   // Y, карта

            // ......................................................................
            // Квадрат на карте

            f_Map_Rect_XY3(
            
                          X_Coordl3_2*1000,
                          Y_Coordl3_2*1000
                         );
            // ......................................................................
            // Соединить линией

            f_Map_Line_XY1(

                          X_Coordl3_1 * 1000,
                          Y_Coordl3_1 * 1000,
                          X_Coordl3_2 * 1000,
                          Y_Coordl3_2 * 1000
                         );

            // ......................................................................

            ClassMap objClassMap6 = new ClassMap();
            // ......................................................................
            // Разность координат для расчета азимута

            // На карте X - вверх
            DXX3 = Y_Coordl3_2 - Y_Coordl3_1; // km
            DYY3 = X_Coordl3_2 - X_Coordl3_1; // km
            // ......................................................................

            // grad
            Azimuth=objClassMap6.f_Def_Azimuth(
                                               //ClassMap.MapY1, // m
                                               //ClassMap.MapX1
                                               DYY3,
                                               DXX3
                                               );
            // ......................................................................
            ichislo1 = (long)(Azimuth * 10000000);
            dchislo1 = ((double)ichislo1) / 10000000;
            textBox3.Text = Convert.ToString(dchislo1);   
            // ......................................................................


        } // Button1
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button3: Azimuth2
        // *****************************************************************************************

        private void button3_Click(object sender, EventArgs e)
        {
            // ......................................................................
            // Отображение координат карты, полученных кликом мыши на карте
            // !!! MapX1,MapY1 -> реальные координаты на местности карты в м (Plane)

            // m->km (SP)
            X_Coordl3_3 = GlobalVarLn.MapX1 / 1000;
            Y_Coordl3_3 = GlobalVarLn.MapY1 / 1000;

            ichislo1 = (long)(X_Coordl3_3 * 10000000);
            dchislo1 = ((double)ichislo1) / 10000000;
            textBox6.Text = Convert.ToString(dchislo1);   // X, карта

            ichislo1 = (long)(Y_Coordl3_3 * 10000000);
            dchislo1 = ((double)ichislo1) / 10000000;
            textBox7.Text = Convert.ToString(dchislo1);   // Y, карта

            // ......................................................................
            // Квадрат на карте

            f_Map_Rect_XY3(

                          X_Coordl3_3 * 1000,
                          Y_Coordl3_3 * 1000
                         );
            // ......................................................................
            // Соединить линией

            f_Map_Line_XY1(

                          X_Coordl3_1 * 1000,
                          Y_Coordl3_1 * 1000,
                          X_Coordl3_3 * 1000,
                          Y_Coordl3_3 * 1000
                         );

            // ......................................................................

            ClassMap objClassMap7 = new ClassMap();
            // ......................................................................
            // Разность координат для расчета азимута

            // На карте X - вверх
            DXX3 = Y_Coordl3_3 - Y_Coordl3_1; // km
            DYY3 = X_Coordl3_3 - X_Coordl3_1; // km
            // ......................................................................

            // grad
            Azimuth1 = objClassMap7.f_Def_Azimuth(
                                               //ClassMap.MapY1, // m
                                               //ClassMap.MapX1
                                               DYY3,
                                               DXX3
                                               );
            // ......................................................................
            ichislo1 = (long)(Azimuth1 * 10000000);
            dchislo1 = ((double)ichislo1) / 10000000;
            textBox8.Text = Convert.ToString(dchislo1);   
            // ......................................................................
            // Сектор

            sect = objClassMap7.f_Sect(Azimuth,Azimuth1);

            ichislo1 = (long)(sect * 10000000);
            dchislo1 = ((double)ichislo1) / 10000000;
            textBox9.Text = Convert.ToString(dchislo1);   
            // ......................................................................


        } // Button3
        // *****************************************************************************************


        // FUNCTIONS ********************************************************************************

        // ******************************************************************************************
        // Нарисовать метку по координатам (красный прямоугольник)
        //
        // Входные параметры:
        // X - X, m
        // Y - Y, m
        // ******************************************************************************************
        public void f_Map_Rect_XY3(

                                   double X,
                                  double Y
                                   )
        {

            // -------------------------------------------------------------------------------------
            Graphics graph = axaxcMapScreen.CreateGraphics();

            Pen penRed2 = new Pen(Color.Red, 2);
            Brush brushRed2 = new SolidBrush(Color.Red);
            // -------------------------------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении

            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {
                graph.FillRectangle(brushRed2, (int)X - axaxcMapScreen.MapLeft, (int)Y - axaxcMapScreen.MapTop, 7, 7);

                // Надпись
                //TitleObject(typeStation, lat, lon);
            }

            // -------------------------------------------------------------------------------------

            //objMapForm8.graph.Dispose();


        } // P/P f_Map_Rect_XY3

        private void label6_Click(object sender, EventArgs e)
        {
            ; 
        } 

        // *************************************************************************************

        // ******************************************************************************************
        // Нарисовать метку по координатам (красный треугольник)
        //
        // Входные параметры:
        // X - X, m
        // Y - Y, m
        // ******************************************************************************************
        public void f_Map_Pol_XY(
                                         double X,
                                         double Y
                                        )
        {

            // -------------------------------------------------------------------------------------
            Graphics graph = axaxcMapScreen.CreateGraphics();

            Pen penRed1 = new Pen(Color.Red, 2);
            Brush brushRed1 = new SolidBrush(Color.Red);
            // -------------------------------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении

            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                Point[] toDraw = new Point[3];
                toDraw[0].X = (int)X - (axaxcMapScreen.MapLeft + 7);
                toDraw[0].Y = (int)Y - (axaxcMapScreen.MapTop - 7);
                toDraw[1].X = (int)X - axaxcMapScreen.MapLeft;
                toDraw[1].Y = (int)Y - (axaxcMapScreen.MapTop + 7);
                toDraw[2].X = (int)X - (axaxcMapScreen.MapLeft - 7);
                toDraw[2].Y = (int)Y - (axaxcMapScreen.MapTop - 7);
                graph.FillPolygon(brushRed1, toDraw);

            }

            // -------------------------------------------------------------------------------------

        } // P/P f_Map_Pol_XY
        // *************************************************************************************

        // ******************************************************************************************
        // Нарисовать линию по координатам
        //
        // Входные параметры:
        // X1,X2 - X, m
        // Y1,Y2 - Y, m
        // ******************************************************************************************
        public void f_Map_Line_XY1(
                                  double X1,
                                  double Y1,
                                  double X2,
                                  double Y2

                                 )
        {

            // -------------------------------------------------------------------------------------
            Graphics graph = axaxcMapScreen.CreateGraphics();

            Pen penRed2 = new Pen(Color.Red, 2);
            Brush brushRed2 = new SolidBrush(Color.Red);

            // -------------------------------------------------------------------------------------

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X1, ref Y1);
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X2, ref Y2);

            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                Point point1 = new Point((int)X1 - axaxcMapScreen.MapLeft, (int)Y1 - axaxcMapScreen.MapTop);
                Point point2 = new Point((int)X2 - axaxcMapScreen.MapLeft, (int)Y2 - axaxcMapScreen.MapTop);

                graph.DrawLine(penRed2, point1, point2);
            }

            // -------------------------------------------------------------------------------------

        } // P/P f_Map_Line_XY1

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            ;
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            ;
        }






        // ********************************************************************************* FUNCTIONS

        // *********************************************************************************
        // Closing, Activated
        private void FormAz_FormClosing(object sender, FormClosingEventArgs e)
        {
            GlobalVarLn.f_Open_objFormAz = 0;

            e.Cancel = true;
            Hide();
        } // Closing

        private void FormAz_Activated(object sender, EventArgs e)
        {
            GlobalVarLn.f_Open_objFormAz = 1;

        } // Activated
        // *********************************************************************************


    } // Class
} // namespace
