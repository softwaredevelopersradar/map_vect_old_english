﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace GrozaMap
{
    partial class MapForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MapForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openHeightMatrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeHeightMatrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.настройкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.составКартыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ZoomIn = new System.Windows.Forms.ToolStripButton();
            this.ZoomOut = new System.Windows.Forms.ToolStripButton();
            this.ZoomInitial = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button12 = new System.Windows.Forms.Button();
            this.bZoneDirectFind = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.axaxMapSelDlg = new AxaxGisToolKit.AxaxMapSelectDialog();
            this.axaxcMapScreen1 = new AxaxGisToolKit.AxaxcMapScreen();
            this.panel3 = new System.Windows.Forms.Panel();
            this.View = new System.Windows.Forms.CheckBox();
            this.chbSect = new System.Windows.Forms.CheckBox();
            this.chbPeleng = new System.Windows.Forms.CheckBox();
            this.chbair = new System.Windows.Forms.CheckBox();
            this.axaxMapPoint1 = new AxaxGisToolKit.AxaxMapPoint();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.butDb = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lLat = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.axaxOpenMapDialog1 = new AxaxGisToolKit.AxaxOpenMapDialog();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axaxMapSelDlg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axaxcMapScreen1)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axaxMapPoint1)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axaxOpenMapDialog1)).BeginInit();
            this.panel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.настройкиToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(742, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.TabStop = true;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openMapToolStripMenuItem,
            this.closeMapToolStripMenuItem,
            this.openHeightMatrixToolStripMenuItem,
            this.closeHeightMatrixToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.fileToolStripMenuItem.Text = "Map";
            // 
            // openMapToolStripMenuItem
            // 
            this.openMapToolStripMenuItem.Name = "openMapToolStripMenuItem";
            this.openMapToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.openMapToolStripMenuItem.Text = "Open Map";
            this.openMapToolStripMenuItem.Click += new System.EventHandler(this.openMapToolStripMenuItem_Click);
            // 
            // closeMapToolStripMenuItem
            // 
            this.closeMapToolStripMenuItem.Name = "closeMapToolStripMenuItem";
            this.closeMapToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.closeMapToolStripMenuItem.Text = "Close Map";
            this.closeMapToolStripMenuItem.Click += new System.EventHandler(this.closeMapToolStripMenuItem_Click);
            // 
            // openHeightMatrixToolStripMenuItem
            // 
            this.openHeightMatrixToolStripMenuItem.Name = "openHeightMatrixToolStripMenuItem";
            this.openHeightMatrixToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.openHeightMatrixToolStripMenuItem.Text = "Open the Height Matrix";
            this.openHeightMatrixToolStripMenuItem.Click += new System.EventHandler(this.openHeightMatrixToolStripMenuItem_Click);
            // 
            // closeHeightMatrixToolStripMenuItem
            // 
            this.closeHeightMatrixToolStripMenuItem.Name = "closeHeightMatrixToolStripMenuItem";
            this.closeHeightMatrixToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.closeHeightMatrixToolStripMenuItem.Text = "Close the Height Matrix";
            this.closeHeightMatrixToolStripMenuItem.Click += new System.EventHandler(this.closeHeightMatrixToolStripMenuItem_Click);
            // 
            // настройкиToolStripMenuItem
            // 
            this.настройкиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.составКартыToolStripMenuItem});
            this.настройкиToolStripMenuItem.Name = "настройкиToolStripMenuItem";
            this.настройкиToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.настройкиToolStripMenuItem.Text = "Setting";
            // 
            // составКартыToolStripMenuItem
            // 
            this.составКартыToolStripMenuItem.Name = "составКартыToolStripMenuItem";
            this.составКартыToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.составКартыToolStripMenuItem.Text = "Map composition";
            this.составКартыToolStripMenuItem.Click += new System.EventHandler(this.составКартыToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ZoomIn,
            this.ZoomOut,
            this.ZoomInitial,
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.ShowItemToolTips = false;
            this.toolStrip1.Size = new System.Drawing.Size(742, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.TabStop = true;
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // ZoomIn
            // 
            this.ZoomIn.Image = ((System.Drawing.Image)(resources.GetObject("ZoomIn.Image")));
            this.ZoomIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ZoomIn.Name = "ZoomIn";
            this.ZoomIn.Size = new System.Drawing.Size(99, 22);
            this.ZoomIn.Text = "Increase scale";
            this.ZoomIn.Click += new System.EventHandler(this.ZoomIn_Click);
            // 
            // ZoomOut
            // 
            this.ZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("ZoomOut.Image")));
            this.ZoomOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ZoomOut.Name = "ZoomOut";
            this.ZoomOut.Size = new System.Drawing.Size(103, 22);
            this.ZoomOut.Text = "Decrease scale";
            this.ZoomOut.Click += new System.EventHandler(this.ZoomOut_Click);
            // 
            // ZoomInitial
            // 
            this.ZoomInitial.Image = ((System.Drawing.Image)(resources.GetObject("ZoomInitial.Image")));
            this.ZoomInitial.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ZoomInitial.Name = "ZoomInitial";
            this.ZoomInitial.Size = new System.Drawing.Size(80, 22);
            this.ZoomInitial.Text = "Base scale";
            this.ZoomInitial.Click += new System.EventHandler(this.ZoomInitial_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.AutoSize = false;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(100, 22);
            this.toolStripButton1.Text = "JS";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 594);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(742, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.TabStop = true;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button12);
            this.panel1.Controls.Add(this.bZoneDirectFind);
            this.panel1.Controls.Add(this.button11);
            this.panel1.Controls.Add(this.button10);
            this.panel1.Controls.Add(this.button9);
            this.panel1.Controls.Add(this.button8);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(702, 49);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(40, 545);
            this.panel1.TabIndex = 5;
            this.panel1.MouseLeave += new System.EventHandler(this.panel1_MouseLeave);
            this.panel1.MouseHover += new System.EventHandler(this.panel1_MouseHover);
            // 
            // button12
            // 
            this.button12.Image = ((System.Drawing.Image)(resources.GetObject("button12.Image")));
            this.button12.Location = new System.Drawing.Point(-1, 0);
            this.button12.Margin = new System.Windows.Forms.Padding(0);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(40, 36);
            this.button12.TabIndex = 22;
            this.toolTip1.SetToolTip(this.button12, "Complex composition");
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            this.button12.MouseEnter += new System.EventHandler(this.button12_MouseEnter);
            this.button12.MouseLeave += new System.EventHandler(this.button12_MouseLeave);
            this.button12.MouseHover += new System.EventHandler(this.button12_MouseHover);
            this.button12.MouseMove += new System.Windows.Forms.MouseEventHandler(this.button12_MouseMove);
            // 
            // bZoneDirectFind
            // 
            this.bZoneDirectFind.Image = ((System.Drawing.Image)(resources.GetObject("bZoneDirectFind.Image")));
            this.bZoneDirectFind.Location = new System.Drawing.Point(0, 213);
            this.bZoneDirectFind.Name = "bZoneDirectFind";
            this.bZoneDirectFind.Size = new System.Drawing.Size(40, 36);
            this.bZoneDirectFind.TabIndex = 21;
            this.toolTip1.SetToolTip(this.bZoneDirectFind, "Zone of location errors ");
            this.bZoneDirectFind.UseVisualStyleBackColor = true;
            this.bZoneDirectFind.Click += new System.EventHandler(this.bZoneDirectFind_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(-1, 393);
            this.button11.Margin = new System.Windows.Forms.Padding(0);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(40, 37);
            this.button11.TabIndex = 10;
            this.button11.Text = "Слои";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Visible = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button10
            // 
            this.button10.Image = ((System.Drawing.Image)(resources.GetObject("button10.Image")));
            this.button10.Location = new System.Drawing.Point(0, 105);
            this.button10.Margin = new System.Windows.Forms.Padding(0);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(40, 36);
            this.button10.TabIndex = 9;
            this.toolTip1.SetToolTip(this.button10, "Line of sight zone (LOS)");
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button9
            // 
            this.button9.Image = ((System.Drawing.Image)(resources.GetObject("button9.Image")));
            this.button9.Location = new System.Drawing.Point(0, 357);
            this.button9.Margin = new System.Windows.Forms.Padding(0);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(40, 36);
            this.button9.TabIndex = 8;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Visible = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.Image = ((System.Drawing.Image)(resources.GetObject("button8.Image")));
            this.button8.Location = new System.Drawing.Point(-1, 176);
            this.button8.Margin = new System.Windows.Forms.Padding(0);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(40, 36);
            this.button8.TabIndex = 7;
            this.toolTip1.SetToolTip(this.button8, "Calculation of energy availability by communication centers");
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.Image = ((System.Drawing.Image)(resources.GetObject("button7.Image")));
            this.button7.Location = new System.Drawing.Point(-1, 140);
            this.button7.Margin = new System.Windows.Forms.Padding(0);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(40, 36);
            this.button7.TabIndex = 6;
            this.toolTip1.SetToolTip(this.button7, "Calculation of energy availability by zones");
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.Location = new System.Drawing.Point(-1, 36);
            this.button6.Margin = new System.Windows.Forms.Padding(0);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(40, 36);
            this.button6.TabIndex = 5;
            this.button6.Text = "\r\n";
            this.toolTip1.SetToolTip(this.button6, "Route");
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.Location = new System.Drawing.Point(0, 70);
            this.button5.Margin = new System.Windows.Forms.Padding(0);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(40, 36);
            this.button5.TabIndex = 4;
            this.toolTip1.SetToolTip(this.button5, "Azimuth");
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.Location = new System.Drawing.Point(0, 321);
            this.button4.Margin = new System.Windows.Forms.Padding(0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(40, 36);
            this.button4.TabIndex = 3;
            this.button4.Text = "S";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(-1, 430);
            this.button3.Margin = new System.Windows.Forms.Padding(0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(40, 36);
            this.button3.TabIndex = 2;
            this.button3.Text = "Пел";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(-1, 573);
            this.button2.Margin = new System.Windows.Forms.Padding(0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(40, 36);
            this.button2.TabIndex = 1;
            this.button2.Text = "XYZ";
            this.toolTip1.SetToolTip(this.button2, "Координаты");
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(0, 466);
            this.button1.Margin = new System.Windows.Forms.Padding(0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(40, 36);
            this.button1.TabIndex = 0;
            this.button1.Text = "Check";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.axaxMapSelDlg);
            this.panel2.Controls.Add(this.axaxcMapScreen1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.axaxMapPoint1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 49);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(702, 545);
            this.panel2.TabIndex = 8;
            // 
            // axaxMapSelDlg
            // 
            this.axaxMapSelDlg.Location = new System.Drawing.Point(160, 136);
            this.axaxMapSelDlg.Name = "axaxMapSelDlg";
            this.axaxMapSelDlg.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axaxMapSelDlg.OcxState")));
            this.axaxMapSelDlg.Size = new System.Drawing.Size(32, 32);
            this.axaxMapSelDlg.TabIndex = 18;
            // 
            // axaxcMapScreen1
            // 
            this.axaxcMapScreen1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axaxcMapScreen1.Location = new System.Drawing.Point(0, 27);
            this.axaxcMapScreen1.Name = "axaxcMapScreen1";
            this.axaxcMapScreen1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axaxcMapScreen1.OcxState")));
            this.axaxcMapScreen1.Size = new System.Drawing.Size(702, 518);
            this.axaxcMapScreen1.TabIndex = 9;
            this.axaxcMapScreen1.OnVScroll += new AxaxGisToolKit.IaxMapScreenEvents_OnVScrollEventHandler(this.axaxcMapScreen1_OnVScroll);
            this.axaxcMapScreen1.OnHScroll += new AxaxGisToolKit.IaxMapScreenEvents_OnHScrollEventHandler(this.axaxcMapScreen1_OnHScroll);
            this.axaxcMapScreen1.OnMapMouseMove += new AxaxGisToolKit.IaxMapScreenEvents_OnMapMouseMoveEventHandler(this.axaxcMapScreen1_OnMapMouseMove);
            this.axaxcMapScreen1.OnMapMouseUp += new AxaxGisToolKit.IaxMapScreenEvents_OnMapMouseUpEventHandler(this.axaxcMapScreen1_OnMapMouseUp);
            this.axaxcMapScreen1.OnMapMouseDown += new AxaxGisToolKit.IaxMapScreenEvents_OnMapMouseDownEventHandler(this.axaxcMapScreen1_OnMapMouseDown);
            this.axaxcMapScreen1.OnMapPaint += new AxaxGisToolKit.IaxMapScreenEvents_OnMapPaintEventHandler(this.axaxcMapScreen1_OnMapPaint);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.View);
            this.panel3.Controls.Add(this.chbSect);
            this.panel3.Controls.Add(this.chbPeleng);
            this.panel3.Controls.Add(this.chbair);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(702, 27);
            this.panel3.TabIndex = 8;
            this.panel3.MouseEnter += new System.EventHandler(this.panel3_MouseEnter);
            // 
            // View
            // 
            this.View.AutoSize = true;
            this.View.Location = new System.Drawing.Point(432, 5);
            this.View.Name = "View";
            this.View.Size = new System.Drawing.Size(118, 17);
            this.View.TabIndex = 108;
            this.View.Text = "Simplified map view";
            this.View.UseVisualStyleBackColor = true;
            this.View.Click += new System.EventHandler(this.View_Click);
            // 
            // chbSect
            // 
            this.chbSect.AutoSize = true;
            this.chbSect.Location = new System.Drawing.Point(160, 5);
            this.chbSect.Name = "chbSect";
            this.chbSect.Size = new System.Drawing.Size(113, 17);
            this.chbSect.TabIndex = 107;
            this.chbSect.Text = "Aircraft trajectories";
            this.chbSect.UseVisualStyleBackColor = true;
            this.chbSect.CheckedChanged += new System.EventHandler(this.chbSect_CheckedChanged);
            // 
            // chbPeleng
            // 
            this.chbPeleng.AutoSize = true;
            this.chbPeleng.Location = new System.Drawing.Point(354, 5);
            this.chbPeleng.Name = "chbPeleng";
            this.chbPeleng.Size = new System.Drawing.Size(62, 17);
            this.chbPeleng.TabIndex = 106;
            this.chbPeleng.Text = "Bearing";
            this.chbPeleng.UseVisualStyleBackColor = true;
            this.chbPeleng.CheckedChanged += new System.EventHandler(this.chbPeleng_CheckedChanged);
            // 
            // chbair
            // 
            this.chbair.AutoSize = true;
            this.chbair.Location = new System.Drawing.Point(5, 5);
            this.chbair.Name = "chbair";
            this.chbair.Size = new System.Drawing.Size(59, 17);
            this.chbair.TabIndex = 105;
            this.chbair.Text = "Aircraft";
            this.chbair.UseVisualStyleBackColor = true;
            this.chbair.CheckedChanged += new System.EventHandler(this.chbair_CheckedChanged);
            // 
            // axaxMapPoint1
            // 
            this.axaxMapPoint1.Location = new System.Drawing.Point(33, 34);
            this.axaxMapPoint1.Name = "axaxMapPoint1";
            this.axaxMapPoint1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axaxMapPoint1.OcxState")));
            this.axaxMapPoint1.Size = new System.Drawing.Size(32, 32);
            this.axaxMapPoint1.TabIndex = 7;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "1.ico");
            // 
            // butDb
            // 
            this.butDb.BackColor = System.Drawing.Color.Red;
            this.butDb.Location = new System.Drawing.Point(589, 25);
            this.butDb.Name = "butDb";
            this.butDb.Size = new System.Drawing.Size(23, 23);
            this.butDb.TabIndex = 17;
            this.toolTip1.SetToolTip(this.butDb, "Подключение к базе данных");
            this.butDb.UseVisualStyleBackColor = false;
            this.butDb.Click += new System.EventHandler(this.butDb_Click);
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel5.BackColor = System.Drawing.SystemColors.Control;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.lLat);
            this.panel5.Location = new System.Drawing.Point(1, 594);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(115, 26);
            this.panel5.TabIndex = 11;
            // 
            // lLat
            // 
            this.lLat.AutoSize = true;
            this.lLat.Location = new System.Drawing.Point(5, 5);
            this.lLat.Name = "lLat";
            this.lLat.Size = new System.Drawing.Size(0, 13);
            this.lLat.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.label1);
            this.panel4.Location = new System.Drawing.Point(117, 594);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(115, 26);
            this.panel4.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel6.Controls.Add(this.label2);
            this.panel6.Location = new System.Drawing.Point(233, 594);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(115, 26);
            this.panel6.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel7.BackColor = System.Drawing.SystemColors.Control;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel7.Controls.Add(this.label3);
            this.panel7.Location = new System.Drawing.Point(349, 594);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(115, 26);
            this.panel7.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 0;
            // 
            // panel8
            // 
            this.panel8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel8.BackColor = System.Drawing.SystemColors.Control;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel8.Controls.Add(this.label4);
            this.panel8.Location = new System.Drawing.Point(465, 594);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(115, 26);
            this.panel8.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 0;
            // 
            // axaxOpenMapDialog1
            // 
            this.axaxOpenMapDialog1.Location = new System.Drawing.Point(373, 334);
            this.axaxOpenMapDialog1.Name = "axaxOpenMapDialog1";
            this.axaxOpenMapDialog1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axaxOpenMapDialog1.OcxState")));
            this.axaxOpenMapDialog1.Size = new System.Drawing.Size(32, 32);
            this.axaxOpenMapDialog1.TabIndex = 7;
            // 
            // panel9
            // 
            this.panel9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel9.Controls.Add(this.label5);
            this.panel9.Location = new System.Drawing.Point(581, 594);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(115, 26);
            this.panel9.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 0;
            // 
            // timer1
            // 
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(616, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "DB";
            // 
            // timer2
            // 
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // MapForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 616);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.butDb);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.axaxOpenMapDialog1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(50, 50);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MapForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Map";
            this.AutoSizeChanged += new System.EventHandler(this.MapForm_AutoSizeChanged);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.MapForm_Load);
            this.ResizeBegin += new System.EventHandler(this.MapForm_ResizeBegin);
            this.ResizeEnd += new System.EventHandler(this.MapForm_ResizeEnd);
            this.Scroll += new System.Windows.Forms.ScrollEventHandler(this.MapForm_Scroll);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MapForm_MouseDown);
            this.MouseEnter += new System.EventHandler(this.MapForm_MouseEnter);
            this.MouseHover += new System.EventHandler(this.MapForm_MouseHover);
            this.Resize += new System.EventHandler(this.MapForm_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axaxMapSelDlg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axaxcMapScreen1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axaxMapPoint1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axaxOpenMapDialog1)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            using (StreamWriter w = File.AppendText("logTheard.txt"))
            {
                w.WriteLine(e.Exception.Message);
                w.WriteLine("--------");
            }
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            using (StreamWriter w = File.AppendText("logDom.txt"))
            {
                w.WriteLine((e.ExceptionObject as Exception).Message);
                w.WriteLine("--------");
            }
        }
        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openHeightMatrixToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeHeightMatrixToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton ZoomIn;
        private System.Windows.Forms.ToolStripButton ZoomOut;
        private System.Windows.Forms.ToolStripButton ZoomInitial;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Panel panel1;
        private AxaxGisToolKit.AxaxOpenMapDialog axaxOpenMapDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ImageList imageList1;
        private AxaxGisToolKit.AxaxMapPoint axaxMapPoint1;
        private AxaxGisToolKit.AxaxcMapScreen axaxcMapScreen1;
        public System.Windows.Forms.CheckBox chbair;
        public System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem составКартыToolStripMenuItem;
        private AxaxGisToolKit.AxaxMapSelectDialog axaxMapSelDlg;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lLat;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button bZoneDirectFind;
        private System.Windows.Forms.Button button12;
        public System.Windows.Forms.CheckBox chbPeleng;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.CheckBox chbSect;
        public System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.CheckBox View;
        private System.Windows.Forms.Button butDb;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.Timer timer2;
    }

}

