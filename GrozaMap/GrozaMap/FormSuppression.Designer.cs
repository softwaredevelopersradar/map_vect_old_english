﻿namespace GrozaMap
{
    partial class FormSuppression
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbPt1DegMinSec = new System.Windows.Forms.GroupBox();
            this.tbPt1LSec = new System.Windows.Forms.TextBox();
            this.tbPt1BSec = new System.Windows.Forms.TextBox();
            this.lPt1Min4 = new System.Windows.Forms.Label();
            this.lPt1Min3 = new System.Windows.Forms.Label();
            this.tbPt1LMin2 = new System.Windows.Forms.TextBox();
            this.tbPt1BMin2 = new System.Windows.Forms.TextBox();
            this.lPt1Sec2 = new System.Windows.Forms.Label();
            this.lPt1Sec1 = new System.Windows.Forms.Label();
            this.lPt1Deg4 = new System.Windows.Forms.Label();
            this.lPt1Deg3 = new System.Windows.Forms.Label();
            this.tbPt1LDeg2 = new System.Windows.Forms.TextBox();
            this.lPt1LDegMinSec = new System.Windows.Forms.Label();
            this.tbPt1BDeg2 = new System.Windows.Forms.TextBox();
            this.lPt1BDegMinSec = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.tbRadiusZone1 = new System.Windows.Forms.TextBox();
            this.lRadiusZone = new System.Windows.Forms.Label();
            this.tbRadiusZone = new System.Windows.Forms.TextBox();
            this.bAccept = new System.Windows.Forms.Button();
            this.tcParam = new System.Windows.Forms.TabControl();
            this.tpOwnObject = new System.Windows.Forms.TabPage();
            this.gbOwnRect = new System.Windows.Forms.GroupBox();
            this.tbYRect = new System.Windows.Forms.TextBox();
            this.lYRect = new System.Windows.Forms.Label();
            this.tbXRect = new System.Windows.Forms.TextBox();
            this.lXRect = new System.Windows.Forms.Label();
            this.tbHeightOwnObject = new System.Windows.Forms.TextBox();
            this.gbOwnRad = new System.Windows.Forms.GroupBox();
            this.tbLRad = new System.Windows.Forms.TextBox();
            this.lLRad = new System.Windows.Forms.Label();
            this.tbBRad = new System.Windows.Forms.TextBox();
            this.lBRad = new System.Windows.Forms.Label();
            this.gbOwnRect42 = new System.Windows.Forms.GroupBox();
            this.tbYRect42 = new System.Windows.Forms.TextBox();
            this.lYRect42 = new System.Windows.Forms.Label();
            this.tbXRect42 = new System.Windows.Forms.TextBox();
            this.lXRect42 = new System.Windows.Forms.Label();
            this.tbHAnt = new System.Windows.Forms.TextBox();
            this.gbOwnDegMinSec = new System.Windows.Forms.GroupBox();
            this.tbLSec = new System.Windows.Forms.TextBox();
            this.tbBSec = new System.Windows.Forms.TextBox();
            this.lMin4 = new System.Windows.Forms.Label();
            this.lMin3 = new System.Windows.Forms.Label();
            this.tbLMin2 = new System.Windows.Forms.TextBox();
            this.tbBMin2 = new System.Windows.Forms.TextBox();
            this.lSec2 = new System.Windows.Forms.Label();
            this.lSec1 = new System.Windows.Forms.Label();
            this.lDeg4 = new System.Windows.Forms.Label();
            this.lDeg3 = new System.Windows.Forms.Label();
            this.tbLDeg2 = new System.Windows.Forms.TextBox();
            this.lLDegMinSec = new System.Windows.Forms.Label();
            this.tbBDeg2 = new System.Windows.Forms.TextBox();
            this.lBDegMinSec = new System.Windows.Forms.Label();
            this.gbOwnDegMin = new System.Windows.Forms.GroupBox();
            this.tbLMin1 = new System.Windows.Forms.TextBox();
            this.tbBMin1 = new System.Windows.Forms.TextBox();
            this.lLDegMin = new System.Windows.Forms.Label();
            this.lBDegMin = new System.Windows.Forms.Label();
            this.tbOwnHeight = new System.Windows.Forms.TextBox();
            this.lHeightOwnObject = new System.Windows.Forms.Label();
            this.lOwnHeight = new System.Windows.Forms.Label();
            this.tbCoeffOwn = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.cbCenterLSR = new System.Windows.Forms.ComboBox();
            this.tbPowerOwn = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.lPowerOwn = new System.Windows.Forms.Label();
            this.lCoeffOwn = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tbPt1Height = new System.Windows.Forms.TextBox();
            this.gbPt1Rect = new System.Windows.Forms.GroupBox();
            this.tbPt1YRect = new System.Windows.Forms.TextBox();
            this.lPt1YRect = new System.Windows.Forms.Label();
            this.tbPt1XRect = new System.Windows.Forms.TextBox();
            this.lPt1XRect = new System.Windows.Forms.Label();
            this.gbPt1DegMin = new System.Windows.Forms.GroupBox();
            this.tbPt1LMin1 = new System.Windows.Forms.TextBox();
            this.tbPt1BMin1 = new System.Windows.Forms.TextBox();
            this.lPt1LDegMin = new System.Windows.Forms.Label();
            this.lPt1BDegMin = new System.Windows.Forms.Label();
            this.gbPt1Rect42 = new System.Windows.Forms.GroupBox();
            this.tbPt1YRect42 = new System.Windows.Forms.TextBox();
            this.lPt1YRect42 = new System.Windows.Forms.Label();
            this.tbPt1XRect42 = new System.Windows.Forms.TextBox();
            this.lPt1XRect42 = new System.Windows.Forms.Label();
            this.tbCoeffSupOwn = new System.Windows.Forms.TextBox();
            this.gbPt1Rad = new System.Windows.Forms.GroupBox();
            this.tbPt1LRad = new System.Windows.Forms.TextBox();
            this.lPt1LRad = new System.Windows.Forms.Label();
            this.tbPt1BRad = new System.Windows.Forms.TextBox();
            this.lPt1BRad = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.lPt1Height = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.tbOpponentAntenna1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbOpponentAntenna = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.cbChooseSC = new System.Windows.Forms.ComboBox();
            this.cbHeightOwnObject = new System.Windows.Forms.ComboBox();
            this.cbPt1HeightOwnObject = new System.Windows.Forms.ComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.tbMidH = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.gbPt1DegMinSec.SuspendLayout();
            this.tcParam.SuspendLayout();
            this.tpOwnObject.SuspendLayout();
            this.gbOwnRect.SuspendLayout();
            this.gbOwnRad.SuspendLayout();
            this.gbOwnRect42.SuspendLayout();
            this.gbOwnDegMinSec.SuspendLayout();
            this.gbOwnDegMin.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.gbPt1Rect.SuspendLayout();
            this.gbPt1DegMin.SuspendLayout();
            this.gbPt1Rect42.SuspendLayout();
            this.gbPt1Rad.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbPt1DegMinSec
            // 
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1LSec);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1BSec);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Min4);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Min3);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1LMin2);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1BMin2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Sec2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Sec1);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Deg4);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Deg3);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1LDeg2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1LDegMinSec);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1BDeg2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1BDegMinSec);
            this.gbPt1DegMinSec.Location = new System.Drawing.Point(10, 35);
            this.gbPt1DegMinSec.Name = "gbPt1DegMinSec";
            this.gbPt1DegMinSec.Size = new System.Drawing.Size(160, 63);
            this.gbPt1DegMinSec.TabIndex = 35;
            this.gbPt1DegMinSec.TabStop = false;
            // 
            // tbPt1LSec
            // 
            this.tbPt1LSec.Location = new System.Drawing.Point(112, 38);
            this.tbPt1LSec.MaxLength = 3;
            this.tbPt1LSec.Name = "tbPt1LSec";
            this.tbPt1LSec.Size = new System.Drawing.Size(25, 20);
            this.tbPt1LSec.TabIndex = 23;
            // 
            // tbPt1BSec
            // 
            this.tbPt1BSec.Location = new System.Drawing.Point(112, 12);
            this.tbPt1BSec.MaxLength = 3;
            this.tbPt1BSec.Name = "tbPt1BSec";
            this.tbPt1BSec.Size = new System.Drawing.Size(25, 20);
            this.tbPt1BSec.TabIndex = 18;
            // 
            // lPt1Min4
            // 
            this.lPt1Min4.AutoSize = true;
            this.lPt1Min4.Location = new System.Drawing.Point(103, 41);
            this.lPt1Min4.Name = "lPt1Min4";
            this.lPt1Min4.Size = new System.Drawing.Size(9, 13);
            this.lPt1Min4.TabIndex = 27;
            this.lPt1Min4.Text = "\'";
            // 
            // lPt1Min3
            // 
            this.lPt1Min3.AutoSize = true;
            this.lPt1Min3.Location = new System.Drawing.Point(103, 15);
            this.lPt1Min3.Name = "lPt1Min3";
            this.lPt1Min3.Size = new System.Drawing.Size(9, 13);
            this.lPt1Min3.TabIndex = 26;
            this.lPt1Min3.Text = "\'";
            // 
            // tbPt1LMin2
            // 
            this.tbPt1LMin2.Location = new System.Drawing.Point(78, 38);
            this.tbPt1LMin2.MaxLength = 2;
            this.tbPt1LMin2.Name = "tbPt1LMin2";
            this.tbPt1LMin2.Size = new System.Drawing.Size(25, 20);
            this.tbPt1LMin2.TabIndex = 22;
            // 
            // tbPt1BMin2
            // 
            this.tbPt1BMin2.Location = new System.Drawing.Point(78, 14);
            this.tbPt1BMin2.MaxLength = 2;
            this.tbPt1BMin2.Name = "tbPt1BMin2";
            this.tbPt1BMin2.Size = new System.Drawing.Size(25, 20);
            this.tbPt1BMin2.TabIndex = 16;
            // 
            // lPt1Sec2
            // 
            this.lPt1Sec2.AutoSize = true;
            this.lPt1Sec2.Location = new System.Drawing.Point(139, 37);
            this.lPt1Sec2.Name = "lPt1Sec2";
            this.lPt1Sec2.Size = new System.Drawing.Size(9, 13);
            this.lPt1Sec2.TabIndex = 25;
            this.lPt1Sec2.Text = "\'";
            // 
            // lPt1Sec1
            // 
            this.lPt1Sec1.AutoSize = true;
            this.lPt1Sec1.Location = new System.Drawing.Point(136, 13);
            this.lPt1Sec1.Name = "lPt1Sec1";
            this.lPt1Sec1.Size = new System.Drawing.Size(9, 13);
            this.lPt1Sec1.TabIndex = 24;
            this.lPt1Sec1.Text = "\'";
            // 
            // lPt1Deg4
            // 
            this.lPt1Deg4.AutoSize = true;
            this.lPt1Deg4.Location = new System.Drawing.Point(65, 36);
            this.lPt1Deg4.Name = "lPt1Deg4";
            this.lPt1Deg4.Size = new System.Drawing.Size(13, 13);
            this.lPt1Deg4.TabIndex = 21;
            this.lPt1Deg4.Text = "^";
            // 
            // lPt1Deg3
            // 
            this.lPt1Deg3.AutoSize = true;
            this.lPt1Deg3.Location = new System.Drawing.Point(65, 13);
            this.lPt1Deg3.Name = "lPt1Deg3";
            this.lPt1Deg3.Size = new System.Drawing.Size(13, 13);
            this.lPt1Deg3.TabIndex = 20;
            this.lPt1Deg3.Text = "^";
            // 
            // tbPt1LDeg2
            // 
            this.tbPt1LDeg2.Location = new System.Drawing.Point(38, 37);
            this.tbPt1LDeg2.MaxLength = 2;
            this.tbPt1LDeg2.Name = "tbPt1LDeg2";
            this.tbPt1LDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbPt1LDeg2.TabIndex = 19;
            // 
            // lPt1LDegMinSec
            // 
            this.lPt1LDegMinSec.AutoSize = true;
            this.lPt1LDegMinSec.Location = new System.Drawing.Point(5, 41);
            this.lPt1LDegMinSec.Name = "lPt1LDegMinSec";
            this.lPt1LDegMinSec.Size = new System.Drawing.Size(46, 13);
            this.lPt1LDegMinSec.TabIndex = 17;
            this.lPt1LDegMinSec.Text = "L...........";
            // 
            // tbPt1BDeg2
            // 
            this.tbPt1BDeg2.Location = new System.Drawing.Point(38, 13);
            this.tbPt1BDeg2.MaxLength = 2;
            this.tbPt1BDeg2.Name = "tbPt1BDeg2";
            this.tbPt1BDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbPt1BDeg2.TabIndex = 15;
            // 
            // lPt1BDegMinSec
            // 
            this.lPt1BDegMinSec.AutoSize = true;
            this.lPt1BDegMinSec.Location = new System.Drawing.Point(5, 18);
            this.lPt1BDegMinSec.Name = "lPt1BDegMinSec";
            this.lPt1BDegMinSec.Size = new System.Drawing.Size(59, 13);
            this.lPt1BDegMinSec.TabIndex = 14;
            this.lPt1BDegMinSec.Text = "B...............";
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button1.Location = new System.Drawing.Point(291, 191);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(47, 23);
            this.button1.TabIndex = 141;
            this.button1.Text = "JS";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button2.Location = new System.Drawing.Point(344, 192);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(78, 23);
            this.button2.TabIndex = 142;
            this.button2.Text = "Object of jam";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(358, 221);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(67, 23);
            this.bClear.TabIndex = 140;
            this.bClear.Text = "Clear";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(0, 231);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(156, 13);
            this.label13.TabIndex = 136;
            this.label13.Text = "Radius of the depletion zone, m";
            // 
            // tbRadiusZone1
            // 
            this.tbRadiusZone1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbRadiusZone1.Location = new System.Drawing.Point(162, 228);
            this.tbRadiusZone1.Name = "tbRadiusZone1";
            this.tbRadiusZone1.ReadOnly = true;
            this.tbRadiusZone1.Size = new System.Drawing.Size(75, 20);
            this.tbRadiusZone1.TabIndex = 135;
            this.tbRadiusZone1.TextChanged += new System.EventHandler(this.tbRadiusZone1_TextChanged);
            // 
            // lRadiusZone
            // 
            this.lRadiusZone.AutoSize = true;
            this.lRadiusZone.Location = new System.Drawing.Point(1, 205);
            this.lRadiusZone.Name = "lRadiusZone";
            this.lRadiusZone.Size = new System.Drawing.Size(151, 13);
            this.lRadiusZone.TabIndex = 133;
            this.lRadiusZone.Text = "Radius of the jamming zone, m";
            // 
            // tbRadiusZone
            // 
            this.tbRadiusZone.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbRadiusZone.Location = new System.Drawing.Point(161, 205);
            this.tbRadiusZone.Name = "tbRadiusZone";
            this.tbRadiusZone.Size = new System.Drawing.Size(76, 20);
            this.tbRadiusZone.TabIndex = 132;
            // 
            // bAccept
            // 
            this.bAccept.Location = new System.Drawing.Point(291, 220);
            this.bAccept.Name = "bAccept";
            this.bAccept.Size = new System.Drawing.Size(59, 23);
            this.bAccept.TabIndex = 139;
            this.bAccept.Text = "Accept";
            this.bAccept.UseVisualStyleBackColor = true;
            this.bAccept.Click += new System.EventHandler(this.bAccept_Click);
            // 
            // tcParam
            // 
            this.tcParam.Controls.Add(this.tpOwnObject);
            this.tcParam.Controls.Add(this.tabPage1);
            this.tcParam.Location = new System.Drawing.Point(7, 13);
            this.tcParam.Name = "tcParam";
            this.tcParam.SelectedIndex = 0;
            this.tcParam.Size = new System.Drawing.Size(422, 152);
            this.tcParam.TabIndex = 131;
            // 
            // tpOwnObject
            // 
            this.tpOwnObject.BackColor = System.Drawing.SystemColors.Control;
            this.tpOwnObject.Controls.Add(this.gbOwnRect);
            this.tpOwnObject.Controls.Add(this.tbHeightOwnObject);
            this.tpOwnObject.Controls.Add(this.gbOwnRad);
            this.tpOwnObject.Controls.Add(this.gbOwnRect42);
            this.tpOwnObject.Controls.Add(this.tbHAnt);
            this.tpOwnObject.Controls.Add(this.gbOwnDegMinSec);
            this.tpOwnObject.Controls.Add(this.gbOwnDegMin);
            this.tpOwnObject.Controls.Add(this.tbOwnHeight);
            this.tpOwnObject.Controls.Add(this.lHeightOwnObject);
            this.tpOwnObject.Controls.Add(this.lOwnHeight);
            this.tpOwnObject.Controls.Add(this.tbCoeffOwn);
            this.tpOwnObject.Controls.Add(this.label38);
            this.tpOwnObject.Controls.Add(this.cbCenterLSR);
            this.tpOwnObject.Controls.Add(this.tbPowerOwn);
            this.tpOwnObject.Controls.Add(this.label17);
            this.tpOwnObject.Controls.Add(this.lPowerOwn);
            this.tpOwnObject.Controls.Add(this.lCoeffOwn);
            this.tpOwnObject.Location = new System.Drawing.Point(4, 22);
            this.tpOwnObject.Name = "tpOwnObject";
            this.tpOwnObject.Padding = new System.Windows.Forms.Padding(3);
            this.tpOwnObject.Size = new System.Drawing.Size(414, 126);
            this.tpOwnObject.TabIndex = 0;
            this.tpOwnObject.Text = "Jammer station (JS)";
            // 
            // gbOwnRect
            // 
            this.gbOwnRect.Controls.Add(this.tbYRect);
            this.gbOwnRect.Controls.Add(this.lYRect);
            this.gbOwnRect.Controls.Add(this.tbXRect);
            this.gbOwnRect.Controls.Add(this.lXRect);
            this.gbOwnRect.Location = new System.Drawing.Point(10, 35);
            this.gbOwnRect.Name = "gbOwnRect";
            this.gbOwnRect.Size = new System.Drawing.Size(160, 63);
            this.gbOwnRect.TabIndex = 67;
            this.gbOwnRect.TabStop = false;
            // 
            // tbYRect
            // 
            this.tbYRect.Location = new System.Drawing.Point(78, 36);
            this.tbYRect.MaxLength = 7;
            this.tbYRect.Name = "tbYRect";
            this.tbYRect.Size = new System.Drawing.Size(75, 20);
            this.tbYRect.TabIndex = 5;
            // 
            // lYRect
            // 
            this.lYRect.AutoSize = true;
            this.lYRect.Location = new System.Drawing.Point(6, 39);
            this.lYRect.Name = "lYRect";
            this.lYRect.Size = new System.Drawing.Size(49, 13);
            this.lYRect.TabIndex = 6;
            this.lYRect.Text = "L............";
            // 
            // tbXRect
            // 
            this.tbXRect.Location = new System.Drawing.Point(78, 13);
            this.tbXRect.MaxLength = 7;
            this.tbXRect.Name = "tbXRect";
            this.tbXRect.Size = new System.Drawing.Size(75, 20);
            this.tbXRect.TabIndex = 4;
            // 
            // lXRect
            // 
            this.lXRect.AutoSize = true;
            this.lXRect.Location = new System.Drawing.Point(4, 20);
            this.lXRect.Name = "lXRect";
            this.lXRect.Size = new System.Drawing.Size(53, 13);
            this.lXRect.TabIndex = 3;
            this.lXRect.Text = "B.............";
            // 
            // tbHeightOwnObject
            // 
            this.tbHeightOwnObject.Location = new System.Drawing.Point(348, 32);
            this.tbHeightOwnObject.MaxLength = 3;
            this.tbHeightOwnObject.Name = "tbHeightOwnObject";
            this.tbHeightOwnObject.Size = new System.Drawing.Size(60, 20);
            this.tbHeightOwnObject.TabIndex = 26;
            // 
            // gbOwnRad
            // 
            this.gbOwnRad.Controls.Add(this.tbLRad);
            this.gbOwnRad.Controls.Add(this.lLRad);
            this.gbOwnRad.Controls.Add(this.tbBRad);
            this.gbOwnRad.Controls.Add(this.lBRad);
            this.gbOwnRad.Location = new System.Drawing.Point(10, 35);
            this.gbOwnRad.Name = "gbOwnRad";
            this.gbOwnRad.Size = new System.Drawing.Size(160, 63);
            this.gbOwnRad.TabIndex = 28;
            this.gbOwnRad.TabStop = false;
            // 
            // tbLRad
            // 
            this.tbLRad.Location = new System.Drawing.Point(78, 36);
            this.tbLRad.MaxLength = 10;
            this.tbLRad.Name = "tbLRad";
            this.tbLRad.Size = new System.Drawing.Size(75, 20);
            this.tbLRad.TabIndex = 5;
            // 
            // lLRad
            // 
            this.lLRad.AutoSize = true;
            this.lLRad.Location = new System.Drawing.Point(4, 41);
            this.lLRad.Name = "lLRad";
            this.lLRad.Size = new System.Drawing.Size(91, 13);
            this.lLRad.TabIndex = 6;
            this.lLRad.Text = "L, rad...................";
            // 
            // tbBRad
            // 
            this.tbBRad.Location = new System.Drawing.Point(78, 13);
            this.tbBRad.MaxLength = 10;
            this.tbBRad.Name = "tbBRad";
            this.tbBRad.Size = new System.Drawing.Size(75, 20);
            this.tbBRad.TabIndex = 4;
            // 
            // lBRad
            // 
            this.lBRad.AutoSize = true;
            this.lBRad.Location = new System.Drawing.Point(4, 18);
            this.lBRad.Name = "lBRad";
            this.lBRad.Size = new System.Drawing.Size(92, 13);
            this.lBRad.TabIndex = 3;
            this.lBRad.Text = "B, rad...................";
            // 
            // gbOwnRect42
            // 
            this.gbOwnRect42.Controls.Add(this.tbYRect42);
            this.gbOwnRect42.Controls.Add(this.lYRect42);
            this.gbOwnRect42.Controls.Add(this.tbXRect42);
            this.gbOwnRect42.Controls.Add(this.lXRect42);
            this.gbOwnRect42.Location = new System.Drawing.Point(10, 35);
            this.gbOwnRect42.Name = "gbOwnRect42";
            this.gbOwnRect42.Size = new System.Drawing.Size(159, 63);
            this.gbOwnRect42.TabIndex = 27;
            this.gbOwnRect42.TabStop = false;
            // 
            // tbYRect42
            // 
            this.tbYRect42.Location = new System.Drawing.Point(78, 36);
            this.tbYRect42.MaxLength = 7;
            this.tbYRect42.Name = "tbYRect42";
            this.tbYRect42.Size = new System.Drawing.Size(75, 20);
            this.tbYRect42.TabIndex = 5;
            // 
            // lYRect42
            // 
            this.lYRect42.AutoSize = true;
            this.lYRect42.Location = new System.Drawing.Point(4, 39);
            this.lYRect42.Name = "lYRect42";
            this.lYRect42.Size = new System.Drawing.Size(85, 13);
            this.lYRect42.TabIndex = 6;
            this.lYRect42.Text = "Y, m...................";
            // 
            // tbXRect42
            // 
            this.tbXRect42.Location = new System.Drawing.Point(78, 13);
            this.tbXRect42.MaxLength = 7;
            this.tbXRect42.Name = "tbXRect42";
            this.tbXRect42.Size = new System.Drawing.Size(75, 20);
            this.tbXRect42.TabIndex = 4;
            // 
            // lXRect42
            // 
            this.lXRect42.AutoSize = true;
            this.lXRect42.Location = new System.Drawing.Point(4, 20);
            this.lXRect42.Name = "lXRect42";
            this.lXRect42.Size = new System.Drawing.Size(85, 13);
            this.lXRect42.TabIndex = 3;
            this.lXRect42.Text = "X, m...................";
            // 
            // tbHAnt
            // 
            this.tbHAnt.Location = new System.Drawing.Point(348, 8);
            this.tbHAnt.Name = "tbHAnt";
            this.tbHAnt.Size = new System.Drawing.Size(60, 20);
            this.tbHAnt.TabIndex = 110;
            this.tbHAnt.Text = "8";
            this.tbHAnt.TextChanged += new System.EventHandler(this.tbHAnt_TextChanged);
            // 
            // gbOwnDegMinSec
            // 
            this.gbOwnDegMinSec.Controls.Add(this.tbLSec);
            this.gbOwnDegMinSec.Controls.Add(this.tbBSec);
            this.gbOwnDegMinSec.Controls.Add(this.lMin4);
            this.gbOwnDegMinSec.Controls.Add(this.lMin3);
            this.gbOwnDegMinSec.Controls.Add(this.tbLMin2);
            this.gbOwnDegMinSec.Controls.Add(this.tbBMin2);
            this.gbOwnDegMinSec.Controls.Add(this.lSec2);
            this.gbOwnDegMinSec.Controls.Add(this.lSec1);
            this.gbOwnDegMinSec.Controls.Add(this.lDeg4);
            this.gbOwnDegMinSec.Controls.Add(this.lDeg3);
            this.gbOwnDegMinSec.Controls.Add(this.tbLDeg2);
            this.gbOwnDegMinSec.Controls.Add(this.lLDegMinSec);
            this.gbOwnDegMinSec.Controls.Add(this.tbBDeg2);
            this.gbOwnDegMinSec.Controls.Add(this.lBDegMinSec);
            this.gbOwnDegMinSec.Location = new System.Drawing.Point(10, 35);
            this.gbOwnDegMinSec.Name = "gbOwnDegMinSec";
            this.gbOwnDegMinSec.Size = new System.Drawing.Size(159, 60);
            this.gbOwnDegMinSec.TabIndex = 30;
            this.gbOwnDegMinSec.TabStop = false;
            // 
            // tbLSec
            // 
            this.tbLSec.Location = new System.Drawing.Point(102, 36);
            this.tbLSec.MaxLength = 3;
            this.tbLSec.Name = "tbLSec";
            this.tbLSec.Size = new System.Drawing.Size(25, 20);
            this.tbLSec.TabIndex = 23;
            // 
            // tbBSec
            // 
            this.tbBSec.Location = new System.Drawing.Point(102, 12);
            this.tbBSec.MaxLength = 3;
            this.tbBSec.Name = "tbBSec";
            this.tbBSec.Size = new System.Drawing.Size(25, 20);
            this.tbBSec.TabIndex = 18;
            // 
            // lMin4
            // 
            this.lMin4.AutoSize = true;
            this.lMin4.Location = new System.Drawing.Point(91, 35);
            this.lMin4.Name = "lMin4";
            this.lMin4.Size = new System.Drawing.Size(9, 13);
            this.lMin4.TabIndex = 27;
            this.lMin4.Text = "\'";
            // 
            // lMin3
            // 
            this.lMin3.AutoSize = true;
            this.lMin3.Location = new System.Drawing.Point(92, 12);
            this.lMin3.Name = "lMin3";
            this.lMin3.Size = new System.Drawing.Size(9, 13);
            this.lMin3.TabIndex = 26;
            this.lMin3.Text = "\'";
            // 
            // tbLMin2
            // 
            this.tbLMin2.Location = new System.Drawing.Point(67, 35);
            this.tbLMin2.MaxLength = 2;
            this.tbLMin2.Name = "tbLMin2";
            this.tbLMin2.Size = new System.Drawing.Size(25, 20);
            this.tbLMin2.TabIndex = 22;
            // 
            // tbBMin2
            // 
            this.tbBMin2.Location = new System.Drawing.Point(67, 12);
            this.tbBMin2.MaxLength = 2;
            this.tbBMin2.Name = "tbBMin2";
            this.tbBMin2.Size = new System.Drawing.Size(25, 20);
            this.tbBMin2.TabIndex = 16;
            // 
            // lSec2
            // 
            this.lSec2.AutoSize = true;
            this.lSec2.Location = new System.Drawing.Point(132, 36);
            this.lSec2.Name = "lSec2";
            this.lSec2.Size = new System.Drawing.Size(9, 13);
            this.lSec2.TabIndex = 25;
            this.lSec2.Text = "\'";
            // 
            // lSec1
            // 
            this.lSec1.AutoSize = true;
            this.lSec1.Location = new System.Drawing.Point(132, 12);
            this.lSec1.Name = "lSec1";
            this.lSec1.Size = new System.Drawing.Size(9, 13);
            this.lSec1.TabIndex = 24;
            this.lSec1.Text = "\'";
            // 
            // lDeg4
            // 
            this.lDeg4.AutoSize = true;
            this.lDeg4.Location = new System.Drawing.Point(51, 38);
            this.lDeg4.Name = "lDeg4";
            this.lDeg4.Size = new System.Drawing.Size(13, 13);
            this.lDeg4.TabIndex = 21;
            this.lDeg4.Text = "^";
            // 
            // lDeg3
            // 
            this.lDeg3.AutoSize = true;
            this.lDeg3.Location = new System.Drawing.Point(51, 13);
            this.lDeg3.Name = "lDeg3";
            this.lDeg3.Size = new System.Drawing.Size(13, 13);
            this.lDeg3.TabIndex = 20;
            this.lDeg3.Text = "^";
            // 
            // tbLDeg2
            // 
            this.tbLDeg2.Location = new System.Drawing.Point(23, 36);
            this.tbLDeg2.MaxLength = 2;
            this.tbLDeg2.Name = "tbLDeg2";
            this.tbLDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbLDeg2.TabIndex = 19;
            // 
            // lLDegMinSec
            // 
            this.lLDegMinSec.AutoSize = true;
            this.lLDegMinSec.Location = new System.Drawing.Point(6, 36);
            this.lLDegMinSec.Name = "lLDegMinSec";
            this.lLDegMinSec.Size = new System.Drawing.Size(64, 13);
            this.lLDegMinSec.TabIndex = 17;
            this.lLDegMinSec.Text = "L.................";
            // 
            // tbBDeg2
            // 
            this.tbBDeg2.Location = new System.Drawing.Point(23, 13);
            this.tbBDeg2.MaxLength = 2;
            this.tbBDeg2.Name = "tbBDeg2";
            this.tbBDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbBDeg2.TabIndex = 15;
            // 
            // lBDegMinSec
            // 
            this.lBDegMinSec.AutoSize = true;
            this.lBDegMinSec.Location = new System.Drawing.Point(3, 16);
            this.lBDegMinSec.Name = "lBDegMinSec";
            this.lBDegMinSec.Size = new System.Drawing.Size(59, 13);
            this.lBDegMinSec.TabIndex = 14;
            this.lBDegMinSec.Text = "B...............";
            // 
            // gbOwnDegMin
            // 
            this.gbOwnDegMin.Controls.Add(this.tbLMin1);
            this.gbOwnDegMin.Controls.Add(this.tbBMin1);
            this.gbOwnDegMin.Controls.Add(this.lLDegMin);
            this.gbOwnDegMin.Controls.Add(this.lBDegMin);
            this.gbOwnDegMin.Location = new System.Drawing.Point(10, 35);
            this.gbOwnDegMin.Name = "gbOwnDegMin";
            this.gbOwnDegMin.Size = new System.Drawing.Size(160, 63);
            this.gbOwnDegMin.TabIndex = 29;
            this.gbOwnDegMin.TabStop = false;
            // 
            // tbLMin1
            // 
            this.tbLMin1.Location = new System.Drawing.Point(75, 36);
            this.tbLMin1.MaxLength = 8;
            this.tbLMin1.Name = "tbLMin1";
            this.tbLMin1.Size = new System.Drawing.Size(50, 20);
            this.tbLMin1.TabIndex = 15;
            // 
            // tbBMin1
            // 
            this.tbBMin1.Location = new System.Drawing.Point(75, 13);
            this.tbBMin1.MaxLength = 8;
            this.tbBMin1.Name = "tbBMin1";
            this.tbBMin1.Size = new System.Drawing.Size(50, 20);
            this.tbBMin1.TabIndex = 13;
            // 
            // lLDegMin
            // 
            this.lLDegMin.AutoSize = true;
            this.lLDegMin.Location = new System.Drawing.Point(12, 39);
            this.lLDegMin.Name = "lLDegMin";
            this.lLDegMin.Size = new System.Drawing.Size(64, 13);
            this.lLDegMin.TabIndex = 12;
            this.lLDegMin.Text = "L.................";
            // 
            // lBDegMin
            // 
            this.lBDegMin.AutoSize = true;
            this.lBDegMin.Location = new System.Drawing.Point(12, 16);
            this.lBDegMin.Name = "lBDegMin";
            this.lBDegMin.Size = new System.Drawing.Size(59, 13);
            this.lBDegMin.TabIndex = 10;
            this.lBDegMin.Text = "B...............";
            // 
            // tbOwnHeight
            // 
            this.tbOwnHeight.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbOwnHeight.Location = new System.Drawing.Point(118, 101);
            this.tbOwnHeight.Name = "tbOwnHeight";
            this.tbOwnHeight.Size = new System.Drawing.Size(48, 20);
            this.tbOwnHeight.TabIndex = 64;
            // 
            // lHeightOwnObject
            // 
            this.lHeightOwnObject.AutoSize = true;
            this.lHeightOwnObject.Location = new System.Drawing.Point(180, 35);
            this.lHeightOwnObject.Name = "lHeightOwnObject";
            this.lHeightOwnObject.Size = new System.Drawing.Size(152, 13);
            this.lHeightOwnObject.TabIndex = 21;
            this.lHeightOwnObject.Text = "Total height , m........................";
            // 
            // lOwnHeight
            // 
            this.lOwnHeight.AutoSize = true;
            this.lOwnHeight.Location = new System.Drawing.Point(14, 103);
            this.lOwnHeight.Name = "lOwnHeight";
            this.lOwnHeight.Size = new System.Drawing.Size(92, 13);
            this.lOwnHeight.TabIndex = 63;
            this.lOwnHeight.Text = "H, m.....................";
            // 
            // tbCoeffOwn
            // 
            this.tbCoeffOwn.Location = new System.Drawing.Point(347, 93);
            this.tbCoeffOwn.Name = "tbCoeffOwn";
            this.tbCoeffOwn.Size = new System.Drawing.Size(60, 20);
            this.tbCoeffOwn.TabIndex = 88;
            this.tbCoeffOwn.Text = "4";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(1, 12);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(76, 13);
            this.label38.TabIndex = 139;
            this.label38.Text = "Center of zone";
            // 
            // cbCenterLSR
            // 
            this.cbCenterLSR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCenterLSR.FormattingEnabled = true;
            this.cbCenterLSR.Items.AddRange(new object[] {
            "X,Y",
            "JS",
            "Mated JS",
            "Control post"});
            this.cbCenterLSR.Location = new System.Drawing.Point(77, 7);
            this.cbCenterLSR.Name = "cbCenterLSR";
            this.cbCenterLSR.Size = new System.Drawing.Size(97, 21);
            this.cbCenterLSR.TabIndex = 136;
            this.cbCenterLSR.SelectedIndexChanged += new System.EventHandler(this.cbCenterLSR_SelectedIndexChanged);
            // 
            // tbPowerOwn
            // 
            this.tbPowerOwn.Location = new System.Drawing.Point(347, 62);
            this.tbPowerOwn.Name = "tbPowerOwn";
            this.tbPowerOwn.Size = new System.Drawing.Size(60, 20);
            this.tbPowerOwn.TabIndex = 86;
            this.tbPowerOwn.Text = "125";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(178, 12);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(156, 13);
            this.label17.TabIndex = 111;
            this.label17.Text = "Antenna height, m ....................";
            // 
            // lPowerOwn
            // 
            this.lPowerOwn.AutoSize = true;
            this.lPowerOwn.Location = new System.Drawing.Point(178, 65);
            this.lPowerOwn.Name = "lPowerOwn";
            this.lPowerOwn.Size = new System.Drawing.Size(150, 13);
            this.lPowerOwn.TabIndex = 89;
            this.lPowerOwn.Text = "Power, W (10-300) .................";
            // 
            // lCoeffOwn
            // 
            this.lCoeffOwn.AutoSize = true;
            this.lCoeffOwn.Location = new System.Drawing.Point(180, 94);
            this.lCoeffOwn.Name = "lCoeffOwn";
            this.lCoeffOwn.Size = new System.Drawing.Size(149, 13);
            this.lCoeffOwn.TabIndex = 87;
            this.lCoeffOwn.Text = "Gain (4-8) ...............................";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.tbPt1Height);
            this.tabPage1.Controls.Add(this.gbPt1Rect);
            this.tabPage1.Controls.Add(this.gbPt1DegMinSec);
            this.tabPage1.Controls.Add(this.gbPt1DegMin);
            this.tabPage1.Controls.Add(this.gbPt1Rect42);
            this.tabPage1.Controls.Add(this.tbCoeffSupOwn);
            this.tabPage1.Controls.Add(this.gbPt1Rad);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.lPt1Height);
            this.tabPage1.Controls.Add(this.label27);
            this.tabPage1.Controls.Add(this.tbOpponentAntenna1);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.tbOpponentAntenna);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.textBox3);
            this.tabPage1.Controls.Add(this.textBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(414, 126);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Object of jam";
            // 
            // tbPt1Height
            // 
            this.tbPt1Height.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbPt1Height.Location = new System.Drawing.Point(116, 100);
            this.tbPt1Height.Name = "tbPt1Height";
            this.tbPt1Height.Size = new System.Drawing.Size(48, 20);
            this.tbPt1Height.TabIndex = 37;
            // 
            // gbPt1Rect
            // 
            this.gbPt1Rect.Controls.Add(this.tbPt1YRect);
            this.gbPt1Rect.Controls.Add(this.lPt1YRect);
            this.gbPt1Rect.Controls.Add(this.tbPt1XRect);
            this.gbPt1Rect.Controls.Add(this.lPt1XRect);
            this.gbPt1Rect.Location = new System.Drawing.Point(10, 35);
            this.gbPt1Rect.Name = "gbPt1Rect";
            this.gbPt1Rect.Size = new System.Drawing.Size(160, 63);
            this.gbPt1Rect.TabIndex = 29;
            this.gbPt1Rect.TabStop = false;
            // 
            // tbPt1YRect
            // 
            this.tbPt1YRect.Location = new System.Drawing.Point(78, 36);
            this.tbPt1YRect.MaxLength = 7;
            this.tbPt1YRect.Name = "tbPt1YRect";
            this.tbPt1YRect.Size = new System.Drawing.Size(75, 20);
            this.tbPt1YRect.TabIndex = 5;
            // 
            // lPt1YRect
            // 
            this.lPt1YRect.AutoSize = true;
            this.lPt1YRect.Location = new System.Drawing.Point(4, 42);
            this.lPt1YRect.Name = "lPt1YRect";
            this.lPt1YRect.Size = new System.Drawing.Size(70, 13);
            this.lPt1YRect.TabIndex = 6;
            this.lPt1YRect.Text = "L...................";
            // 
            // tbPt1XRect
            // 
            this.tbPt1XRect.Location = new System.Drawing.Point(78, 13);
            this.tbPt1XRect.MaxLength = 7;
            this.tbPt1XRect.Name = "tbPt1XRect";
            this.tbPt1XRect.Size = new System.Drawing.Size(75, 20);
            this.tbPt1XRect.TabIndex = 4;
            // 
            // lPt1XRect
            // 
            this.lPt1XRect.AutoSize = true;
            this.lPt1XRect.Location = new System.Drawing.Point(4, 20);
            this.lPt1XRect.Name = "lPt1XRect";
            this.lPt1XRect.Size = new System.Drawing.Size(71, 13);
            this.lPt1XRect.TabIndex = 3;
            this.lPt1XRect.Text = "B...................";
            // 
            // gbPt1DegMin
            // 
            this.gbPt1DegMin.Controls.Add(this.tbPt1LMin1);
            this.gbPt1DegMin.Controls.Add(this.tbPt1BMin1);
            this.gbPt1DegMin.Controls.Add(this.lPt1LDegMin);
            this.gbPt1DegMin.Controls.Add(this.lPt1BDegMin);
            this.gbPt1DegMin.Location = new System.Drawing.Point(10, 35);
            this.gbPt1DegMin.Name = "gbPt1DegMin";
            this.gbPt1DegMin.Size = new System.Drawing.Size(149, 63);
            this.gbPt1DegMin.TabIndex = 34;
            this.gbPt1DegMin.TabStop = false;
            // 
            // tbPt1LMin1
            // 
            this.tbPt1LMin1.Location = new System.Drawing.Point(78, 34);
            this.tbPt1LMin1.MaxLength = 8;
            this.tbPt1LMin1.Name = "tbPt1LMin1";
            this.tbPt1LMin1.Size = new System.Drawing.Size(75, 20);
            this.tbPt1LMin1.TabIndex = 15;
            // 
            // tbPt1BMin1
            // 
            this.tbPt1BMin1.Location = new System.Drawing.Point(78, 11);
            this.tbPt1BMin1.MaxLength = 8;
            this.tbPt1BMin1.Name = "tbPt1BMin1";
            this.tbPt1BMin1.Size = new System.Drawing.Size(75, 20);
            this.tbPt1BMin1.TabIndex = 13;
            // 
            // lPt1LDegMin
            // 
            this.lPt1LDegMin.AutoSize = true;
            this.lPt1LDegMin.Location = new System.Drawing.Point(4, 41);
            this.lPt1LDegMin.Name = "lPt1LDegMin";
            this.lPt1LDegMin.Size = new System.Drawing.Size(94, 13);
            this.lPt1LDegMin.TabIndex = 12;
            this.lPt1LDegMin.Text = "L...........................";
            // 
            // lPt1BDegMin
            // 
            this.lPt1BDegMin.AutoSize = true;
            this.lPt1BDegMin.Location = new System.Drawing.Point(4, 18);
            this.lPt1BDegMin.Name = "lPt1BDegMin";
            this.lPt1BDegMin.Size = new System.Drawing.Size(95, 13);
            this.lPt1BDegMin.TabIndex = 10;
            this.lPt1BDegMin.Text = "B...........................";
            // 
            // gbPt1Rect42
            // 
            this.gbPt1Rect42.Controls.Add(this.tbPt1YRect42);
            this.gbPt1Rect42.Controls.Add(this.lPt1YRect42);
            this.gbPt1Rect42.Controls.Add(this.tbPt1XRect42);
            this.gbPt1Rect42.Controls.Add(this.lPt1XRect42);
            this.gbPt1Rect42.Location = new System.Drawing.Point(10, 35);
            this.gbPt1Rect42.Name = "gbPt1Rect42";
            this.gbPt1Rect42.Size = new System.Drawing.Size(160, 63);
            this.gbPt1Rect42.TabIndex = 30;
            this.gbPt1Rect42.TabStop = false;
            // 
            // tbPt1YRect42
            // 
            this.tbPt1YRect42.Location = new System.Drawing.Point(78, 36);
            this.tbPt1YRect42.MaxLength = 7;
            this.tbPt1YRect42.Name = "tbPt1YRect42";
            this.tbPt1YRect42.Size = new System.Drawing.Size(75, 20);
            this.tbPt1YRect42.TabIndex = 5;
            // 
            // lPt1YRect42
            // 
            this.lPt1YRect42.AutoSize = true;
            this.lPt1YRect42.Location = new System.Drawing.Point(13, 43);
            this.lPt1YRect42.Name = "lPt1YRect42";
            this.lPt1YRect42.Size = new System.Drawing.Size(85, 13);
            this.lPt1YRect42.TabIndex = 6;
            this.lPt1YRect42.Text = "Y, m...................";
            // 
            // tbPt1XRect42
            // 
            this.tbPt1XRect42.Location = new System.Drawing.Point(78, 13);
            this.tbPt1XRect42.MaxLength = 7;
            this.tbPt1XRect42.Name = "tbPt1XRect42";
            this.tbPt1XRect42.Size = new System.Drawing.Size(75, 20);
            this.tbPt1XRect42.TabIndex = 4;
            // 
            // lPt1XRect42
            // 
            this.lPt1XRect42.AutoSize = true;
            this.lPt1XRect42.Location = new System.Drawing.Point(13, 16);
            this.lPt1XRect42.Name = "lPt1XRect42";
            this.lPt1XRect42.Size = new System.Drawing.Size(85, 13);
            this.lPt1XRect42.TabIndex = 3;
            this.lPt1XRect42.Text = "X, m...................";
            // 
            // tbCoeffSupOwn
            // 
            this.tbCoeffSupOwn.Location = new System.Drawing.Point(346, 4);
            this.tbCoeffSupOwn.Name = "tbCoeffSupOwn";
            this.tbCoeffSupOwn.Size = new System.Drawing.Size(60, 20);
            this.tbCoeffSupOwn.TabIndex = 109;
            this.tbCoeffSupOwn.Text = "1";
            // 
            // gbPt1Rad
            // 
            this.gbPt1Rad.Controls.Add(this.tbPt1LRad);
            this.gbPt1Rad.Controls.Add(this.lPt1LRad);
            this.gbPt1Rad.Controls.Add(this.tbPt1BRad);
            this.gbPt1Rad.Controls.Add(this.lPt1BRad);
            this.gbPt1Rad.Location = new System.Drawing.Point(10, 35);
            this.gbPt1Rad.Name = "gbPt1Rad";
            this.gbPt1Rad.Size = new System.Drawing.Size(160, 63);
            this.gbPt1Rad.TabIndex = 31;
            this.gbPt1Rad.TabStop = false;
            // 
            // tbPt1LRad
            // 
            this.tbPt1LRad.Location = new System.Drawing.Point(78, 36);
            this.tbPt1LRad.MaxLength = 10;
            this.tbPt1LRad.Name = "tbPt1LRad";
            this.tbPt1LRad.Size = new System.Drawing.Size(75, 20);
            this.tbPt1LRad.TabIndex = 5;
            // 
            // lPt1LRad
            // 
            this.lPt1LRad.AutoSize = true;
            this.lPt1LRad.Location = new System.Drawing.Point(4, 41);
            this.lPt1LRad.Name = "lPt1LRad";
            this.lPt1LRad.Size = new System.Drawing.Size(91, 13);
            this.lPt1LRad.TabIndex = 6;
            this.lPt1LRad.Text = "L, rad...................";
            // 
            // tbPt1BRad
            // 
            this.tbPt1BRad.Location = new System.Drawing.Point(78, 13);
            this.tbPt1BRad.MaxLength = 10;
            this.tbPt1BRad.Name = "tbPt1BRad";
            this.tbPt1BRad.Size = new System.Drawing.Size(75, 20);
            this.tbPt1BRad.TabIndex = 4;
            // 
            // lPt1BRad
            // 
            this.lPt1BRad.AutoSize = true;
            this.lPt1BRad.Location = new System.Drawing.Point(4, 18);
            this.lPt1BRad.Name = "lPt1BRad";
            this.lPt1BRad.Size = new System.Drawing.Size(92, 13);
            this.lPt1BRad.TabIndex = 3;
            this.lPt1BRad.Text = "B, rad...................";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(184, 7);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(155, 13);
            this.label21.TabIndex = 21;
            this.label21.Text = "J/S ratio (1-100) .......................";
            // 
            // lPt1Height
            // 
            this.lPt1Height.AutoSize = true;
            this.lPt1Height.Location = new System.Drawing.Point(10, 105);
            this.lPt1Height.Name = "lPt1Height";
            this.lPt1Height.Size = new System.Drawing.Size(89, 13);
            this.lPt1Height.TabIndex = 36;
            this.lPt1Height.Text = "H, m....................";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(183, 100);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(143, 13);
            this.label27.TabIndex = 137;
            this.label27.Text = "Antenna height (receiver), m ";
            // 
            // tbOpponentAntenna1
            // 
            this.tbOpponentAntenna1.Location = new System.Drawing.Point(347, 98);
            this.tbOpponentAntenna1.Name = "tbOpponentAntenna1";
            this.tbOpponentAntenna1.Size = new System.Drawing.Size(60, 20);
            this.tbOpponentAntenna1.TabIndex = 136;
            this.tbOpponentAntenna1.Text = "2";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(183, 77);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(150, 13);
            this.label11.TabIndex = 135;
            this.label11.Text = "Antenna height, m ..................";
            // 
            // tbOpponentAntenna
            // 
            this.tbOpponentAntenna.Location = new System.Drawing.Point(347, 74);
            this.tbOpponentAntenna.Name = "tbOpponentAntenna";
            this.tbOpponentAntenna.Size = new System.Drawing.Size(60, 20);
            this.tbOpponentAntenna.TabIndex = 134;
            this.tbOpponentAntenna.Text = "2";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(182, 57);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(152, 13);
            this.label14.TabIndex = 121;
            this.label14.Text = "Gain (1-10) ..............................";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(183, 32);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(153, 13);
            this.label12.TabIndex = 120;
            this.label12.Text = "Power, W (1-100) ....................";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(347, 51);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(60, 20);
            this.textBox3.TabIndex = 118;
            this.textBox3.Text = "1";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(347, 28);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(60, 20);
            this.textBox2.TabIndex = 117;
            this.textBox2.Text = "25";
            // 
            // cbChooseSC
            // 
            this.cbChooseSC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChooseSC.DropDownWidth = 150;
            this.cbChooseSC.FormattingEnabled = true;
            this.cbChooseSC.Items.AddRange(new object[] {
            "Degrees (WGS84)",
            "Meters (Gauss-Krueger)",
            "Radians (CS42)",
            "Degrees (CS42)",
            "Deg,min,sec(CS42)"});
            this.cbChooseSC.Location = new System.Drawing.Point(37, 170);
            this.cbChooseSC.Name = "cbChooseSC";
            this.cbChooseSC.Size = new System.Drawing.Size(148, 21);
            this.cbChooseSC.TabIndex = 113;
            this.cbChooseSC.SelectedIndexChanged += new System.EventHandler(this.cbChooseSC_SelectedIndexChanged);
            // 
            // cbHeightOwnObject
            // 
            this.cbHeightOwnObject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbHeightOwnObject.FormattingEnabled = true;
            this.cbHeightOwnObject.Items.AddRange(new object[] {
            "Antenna height+relief",
            "Antenna height",
            "Enter by yourself"});
            this.cbHeightOwnObject.Location = new System.Drawing.Point(464, 525);
            this.cbHeightOwnObject.Name = "cbHeightOwnObject";
            this.cbHeightOwnObject.Size = new System.Drawing.Size(152, 21);
            this.cbHeightOwnObject.TabIndex = 27;
            this.cbHeightOwnObject.Visible = false;
            // 
            // cbPt1HeightOwnObject
            // 
            this.cbPt1HeightOwnObject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPt1HeightOwnObject.FormattingEnabled = true;
            this.cbPt1HeightOwnObject.Items.AddRange(new object[] {
            "Antenna height+relief",
            "Antenna height",
            "Enter by yourself"});
            this.cbPt1HeightOwnObject.Location = new System.Drawing.Point(473, 559);
            this.cbPt1HeightOwnObject.Name = "cbPt1HeightOwnObject";
            this.cbPt1HeightOwnObject.Size = new System.Drawing.Size(143, 21);
            this.cbPt1HeightOwnObject.TabIndex = 27;
            this.cbPt1HeightOwnObject.Visible = false;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(8, 173);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(21, 13);
            this.label36.TabIndex = 114;
            this.label36.Text = "CS";
            // 
            // tbMidH
            // 
            this.tbMidH.Location = new System.Drawing.Point(365, 168);
            this.tbMidH.Name = "tbMidH";
            this.tbMidH.Size = new System.Drawing.Size(60, 20);
            this.tbMidH.TabIndex = 145;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(245, 171);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(93, 13);
            this.label37.TabIndex = 146;
            this.label37.Text = "Average height, m";
            // 
            // FormSuppression
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 251);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.tbMidH);
            this.Controls.Add(this.cbHeightOwnObject);
            this.Controls.Add(this.cbPt1HeightOwnObject);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.cbChooseSC);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.tbRadiusZone1);
            this.Controls.Add(this.lRadiusZone);
            this.Controls.Add(this.tbRadiusZone);
            this.Controls.Add(this.bAccept);
            this.Controls.Add(this.tcParam);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(800, 100);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSuppression";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Calculation of energy availability by zones";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.FormSuppression_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSuppression_FormClosing);
            this.Load += new System.EventHandler(this.FormSuppression_Load);
            this.gbPt1DegMinSec.ResumeLayout(false);
            this.gbPt1DegMinSec.PerformLayout();
            this.tcParam.ResumeLayout(false);
            this.tpOwnObject.ResumeLayout(false);
            this.tpOwnObject.PerformLayout();
            this.gbOwnRect.ResumeLayout(false);
            this.gbOwnRect.PerformLayout();
            this.gbOwnRad.ResumeLayout(false);
            this.gbOwnRad.PerformLayout();
            this.gbOwnRect42.ResumeLayout(false);
            this.gbOwnRect42.PerformLayout();
            this.gbOwnDegMinSec.ResumeLayout(false);
            this.gbOwnDegMinSec.PerformLayout();
            this.gbOwnDegMin.ResumeLayout(false);
            this.gbOwnDegMin.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.gbPt1Rect.ResumeLayout(false);
            this.gbPt1Rect.PerformLayout();
            this.gbPt1DegMin.ResumeLayout(false);
            this.gbPt1DegMin.PerformLayout();
            this.gbPt1Rect42.ResumeLayout(false);
            this.gbPt1Rect42.PerformLayout();
            this.gbPt1Rad.ResumeLayout(false);
            this.gbPt1Rad.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox tbPt1LSec;
        public System.Windows.Forms.TextBox tbPt1BSec;
        private System.Windows.Forms.Label lPt1Min4;
        private System.Windows.Forms.Label lPt1Min3;
        public System.Windows.Forms.TextBox tbPt1LMin2;
        public System.Windows.Forms.TextBox tbPt1BMin2;
        private System.Windows.Forms.Label lPt1Sec2;
        private System.Windows.Forms.Label lPt1Sec1;
        private System.Windows.Forms.Label lPt1Deg4;
        private System.Windows.Forms.Label lPt1Deg3;
        public System.Windows.Forms.TextBox tbPt1LDeg2;
        private System.Windows.Forms.Label lPt1LDegMinSec;
        public System.Windows.Forms.TextBox tbPt1BDeg2;
        private System.Windows.Forms.Label lPt1BDegMinSec;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lRadiusZone;
        private System.Windows.Forms.Button bAccept;
        public System.Windows.Forms.TabControl tcParam;
        private System.Windows.Forms.TabPage tpOwnObject;
        private System.Windows.Forms.Label lPowerOwn;
        private System.Windows.Forms.TextBox tbCoeffOwn;
        private System.Windows.Forms.Label lCoeffOwn;
        private System.Windows.Forms.TextBox tbPowerOwn;
        private System.Windows.Forms.Label lHeightOwnObject;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox tbCoeffSupOwn;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.TextBox tbRadiusZone1;
        public System.Windows.Forms.TextBox tbRadiusZone;
        public System.Windows.Forms.ComboBox cbHeightOwnObject;
        public System.Windows.Forms.TextBox tbHeightOwnObject;
        public System.Windows.Forms.ComboBox cbPt1HeightOwnObject;
        public System.Windows.Forms.GroupBox gbPt1DegMinSec;
        public System.Windows.Forms.TextBox tbHAnt;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox tbOpponentAntenna;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label27;
        public System.Windows.Forms.TextBox tbOpponentAntenna1;
        public System.Windows.Forms.TextBox tbYRect;
        private System.Windows.Forms.Label lYRect;
        public System.Windows.Forms.TextBox tbXRect;
        private System.Windows.Forms.Label lXRect;
        public System.Windows.Forms.TextBox tbOwnHeight;
        private System.Windows.Forms.GroupBox gbOwnDegMin;
        public System.Windows.Forms.TextBox tbLMin1;
        public System.Windows.Forms.TextBox tbBMin1;
        private System.Windows.Forms.Label lLDegMin;
        private System.Windows.Forms.Label lBDegMin;
        private System.Windows.Forms.GroupBox gbOwnDegMinSec;
        public System.Windows.Forms.TextBox tbLSec;
        public System.Windows.Forms.TextBox tbBSec;
        private System.Windows.Forms.Label lMin4;
        private System.Windows.Forms.Label lMin3;
        public System.Windows.Forms.TextBox tbLMin2;
        public System.Windows.Forms.TextBox tbBMin2;
        private System.Windows.Forms.Label lSec2;
        private System.Windows.Forms.Label lSec1;
        private System.Windows.Forms.Label lDeg4;
        private System.Windows.Forms.Label lDeg3;
        public System.Windows.Forms.TextBox tbLDeg2;
        private System.Windows.Forms.Label lLDegMinSec;
        public System.Windows.Forms.TextBox tbBDeg2;
        private System.Windows.Forms.Label lBDegMinSec;
        private System.Windows.Forms.GroupBox gbOwnRect42;
        public System.Windows.Forms.TextBox tbYRect42;
        private System.Windows.Forms.Label lYRect42;
        public System.Windows.Forms.TextBox tbXRect42;
        private System.Windows.Forms.Label lXRect42;
        private System.Windows.Forms.Label lOwnHeight;
        private System.Windows.Forms.GroupBox gbOwnRad;
        public System.Windows.Forms.TextBox tbLRad;
        private System.Windows.Forms.Label lLRad;
        public System.Windows.Forms.TextBox tbBRad;
        private System.Windows.Forms.Label lBRad;
        public System.Windows.Forms.TextBox tbPt1Height;
        private System.Windows.Forms.Label lPt1Height;
        private System.Windows.Forms.GroupBox gbPt1Rad;
        public System.Windows.Forms.TextBox tbPt1LRad;
        private System.Windows.Forms.Label lPt1LRad;
        public System.Windows.Forms.TextBox tbPt1BRad;
        private System.Windows.Forms.Label lPt1BRad;
        private System.Windows.Forms.GroupBox gbPt1Rect;
        public System.Windows.Forms.TextBox tbPt1YRect;
        private System.Windows.Forms.Label lPt1YRect;
        public System.Windows.Forms.TextBox tbPt1XRect;
        private System.Windows.Forms.Label lPt1XRect;
        private System.Windows.Forms.GroupBox gbPt1DegMin;
        public System.Windows.Forms.TextBox tbPt1LMin1;
        public System.Windows.Forms.TextBox tbPt1BMin1;
        private System.Windows.Forms.Label lPt1LDegMin;
        private System.Windows.Forms.Label lPt1BDegMin;
        private System.Windows.Forms.GroupBox gbPt1Rect42;
        public System.Windows.Forms.TextBox tbPt1YRect42;
        private System.Windows.Forms.Label lPt1YRect42;
        public System.Windows.Forms.TextBox tbPt1XRect42;
        private System.Windows.Forms.Label lPt1XRect42;
        public System.Windows.Forms.ComboBox cbChooseSC;
        private System.Windows.Forms.Label label36;
        public System.Windows.Forms.ComboBox cbCenterLSR;
        private System.Windows.Forms.Label label38;
        public System.Windows.Forms.TextBox tbMidH;
        private System.Windows.Forms.Label label37;
        public System.Windows.Forms.GroupBox gbOwnRect;

    }
}