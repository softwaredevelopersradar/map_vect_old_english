﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;

using System.Drawing.Imaging;
using System.Windows.Input;

using Berezina.Maps;

// 3
using ModelsTablesDBLib;

namespace GrozaMap
{

    public class ClassMap
    {


        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        //[DllImport("gisacces.dll")]
        //static extern int mapGeoWGS84ToPlane(int hmap, ref double Bx, ref double Ly);
        //[DllImport("gisacces.dll")]
        //static extern int mapWGS84GeoToPlane(int hmap, ref double Bx, ref double Ly);

        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        [DllImport("gisacces.dll")]
        static extern int mapGetScreenPrecision();

        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        // VARS *******************************************************************************

        // STATIC -----------------------------------------------------------------------------
/*
        public static double MapX1 = 0;
        public static double MapY1 = 0;
        public static int hmapl = 0;

        // Самолеты
        public static int fl_AirPlane = 0;
        public static double Lat_air;
        public static double Long_air;
        public static double Angle_air;
*/
        // ----------------------------------------------------------------------------STATIC

        //public static double LAMBDA=300000;

        // Координаты -------------------------------------------------------------------------
        //private double Pi;

        // Число угловых секунд в радиане
        private double ro;

        // Эллипсоид Красовского
        private double aP;  // Большая полуось
        private double a1P; // Сжатие
        private double e2P; // Квадрат эксцентриситета

        // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
        private double aW;  // Большая полуось
        private double a1W; // Сжатие
        private double e2W; // Квадрат эксцентриситета

        // Вспомогательные значения для преобразования эллипсоидов
        private double a;
        private double da;
        private double e2;
        private double de2;

        // Угловые элементы трансформирования, в секундах
        private double wx;
        private double wy;
        private double wz;

        // Дифференциальное различие масштабов
        private double ms;

        // Флаг НУ
        //private int fl_first1_Coord;
        private int fl_first2_Coord;
        //private int fl_first3_Coord;
        //private int fl_first5_Coord;
        //private int fl_first6_Coord;
        //private int fl_first7_Coord;
        //private int fl_first8_Coord;
        // ------------------------------------------------------------------------Координаты

        // Пеленг PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP

        // Флаг НУ
        private int fl_first_Pel;

        // Вспомогательные флаги
        //private int fl1_Pel;

        // Индекс фиктивной точки (1,2, ...)
        private uint IndFP_Pel;
        // Индекс i-й фиктивной точки в массивах(0,...)
        private uint IndMas_Pel;

        // Расстояние по поверхности земли от точки стояния пеленгатора до текущей 
        // фиктивной точки 
        //private double Mi_Pel;
        // Расстояние по прямой от точки стояния пеленгатора до пересечения прямой
        // (проходящей через текущую фиктивную точку и начало координат)и плоскости,
        // касательной в точке стояния пеленгатора
        //private double Di_Pel;

        // Радиус Земли (шар)
        private double REarth_Pel;
        // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
        private double REarthP_Pel;
        // Радиус Земли  в (i-1)-й фиктивной точке с учетом текущей широты 
        //double REarthF_Pel;

        // Координаты фиктивной точки в СК пеленгатора (СКП)
        //private double Xi_Pel;
        //private double Yi_Pel;
        //private double Zi_Pel;

        // Координаты фиктивной точки в СК1 (Поворот СКП на угол=широте пеленгатора)
        //private double Xi0_Pel;
        //private double Yi0_Pel;
        //private double Zi0_Pel;

        // Координаты фиктивной точки в опорной геоцентрической СК
        // (Поворот СК1 на угол=долготе пеленгатора)
        private double XiG_Pel;
        private double YiG_Pel;
        private double ZiG_Pel;

        // Угловые координаты фиктивной точки в опорной геоцентрической СК
        private double Ri_Pel;
        private double LATi_Pel;
        private double LONGi_Pel;

        // Координаты точки пересечения прямой (проходящей через текущую фиктивную
        // точку и начало координат) и плоскости касательной в точке стояния 
        // пеленгатора в опорной геоцентрической СК
        //private double XiG1_Pel;
        //private double YiG1_Pel;
        //private double ZiG1_Pel;
        //private double Ri1_Pel;

        // Центральный угол дуги Mi
        //private double Alpha;

        // Широта и долгота стояния пеленгатора
        private double LatP_Pel_tmp;
        private double LongP_Pel_tmp;

        // Количество фиктивных точек в плоскости пеленгования пеленгатора
        private uint NumbFikt_Pel_tmp;

        // Max дальность отображения пеленга
        private double Mmax_Pel_tmp;

        // Пеленг
        private double Theta_Pel_tmp;

        // Координаты пеленгатора в опорной геоцентрической СК
        private double XP_Pel_tmp;
        private double YP_Pel_tmp;
        private double ZP_Pel_tmp;

        // Счетчик
        //private int sch_Pel;

        // ----------------------------------------------------------------
        // !!! VAR2

        private double Lat_Pel_N;  //Lat(n)
        private double Lat_Pel_N1;  //Lat(n-1)
        private double Long_Pel_N;  //Long(n)
        private double Long_Pel_N1;  //Long(n-1)
        private double V_Pel;

        private int fl_var2;
        private int fl1_var2;
        private int fl2_var2;

        // ----------------------------------------------------------------

        // PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP Пеленг


        // Триангуляция TriangTriangTriangTriangTriangTriangTriangTriangTriangTrian
        // Флаг НУ
        private int fl_first_Tr;

        // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
        private double REarthP1_Tr;
        private double REarthP2_Tr;

        // Расчетные коэффициенты
        private double K1_Tr;
        private double K2_Tr;
        private double K3_Tr;

        // Коэффициенты канонического уравнения для плоскости пеленгования ИРИ
        // i-ым пеленгатором 
        private double A1_Tr; // Для пеленгатора1
        private double T1_Tr;
        private double C1_Tr;
        private double A2_Tr; // Для пеленгатора2
        private double T2_Tr;
        private double C2_Tr;

        // Координаты искомого ИРИ в опорной геоцентрической СК
        private double XIRI1_Tr;
        private double XIRI2_Tr;
        private double YIRI1_Tr;
        private double YIRI2_Tr;
        private double ZIRI1_Tr;
        private double ZIRI2_Tr;

        // Координаты середины отрезка, соединяющего N-ые фиктивные точки
        private double XM_Tr;
        private double YM_Tr;
        private double ZM_Tr;

        // Расстояние до ПП
        private double L1_Tr;
        private double L2_Tr;

        // Координаты пеленгатора в опорной геоцентрической СК
        private double XP1_Tr_tmp;
        private double YP1_Tr_tmp;
        private double ZP1_Tr_tmp;
        private double XP2_Tr_tmp;
        private double YP2_Tr_tmp;
        private double ZP2_Tr_tmp;

        // Координаты N-й фиктивной точки в опорной геоцентрической СК
        private double XFN1_Tr_tmp;
        private double YFN1_Tr_tmp;
        private double ZFN1_Tr_tmp;
        private double XFN2_Tr_tmp;
        private double YFN2_Tr_tmp;
        private double ZFN2_Tr_tmp;

        // TriangTriangTriangTriangTriangTriangTriangTriangTriangTrian Триангуляция

        // ******************************************************************************* VARS


        // Конструктор *************************************************************************

        public ClassMap()
        {

            // Координаты ---------------------------------------------------------------------
            //Pi = 3.14159265358979;

            // Число угловых секунд в радиане
            ro = 206264.8062;

            // Эллипсоид Красовского
            aP = 6378245;  // Большая полуось
            a1P = 0; // Сжатие
            e2P = 0; // Квадрат эксцентриситета

            // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
            aW = 6378137;  // Большая полуось
            a1W = 0; // Сжатие
            e2W = 0; // Квадрат эксцентриситета

            // Вспомогательные значения для преобразования эллипсоидов
            a = 0;
            da = 0;
            e2 = 0;
            de2 = 0;

            // Линейные элементы трансформирования, в метрах
            // ГОСТ 51794_2001
            //dx = 23.92;
            //dy = -141.27;
            //dz = -80.9;
            //wx = 0;
            //wy = 0.35;
            //wz = 0.82;
            //ms = -0.000012;

            // ГОСТ 51794_2008
            //dx = 25;
            //dy = -141;
            //dz = -80;
            //wx=0; 
            //wy = -0.35;
            //wz = -0.66;
            //ms = 0;
            //dx = 28;
            //dy = -130;
            //dz = -95;
            ms = 0;
            wx = 0;
            wy = -0.35;
            wz = -0.66;

            //dx = 28;
            //dy = -130;
            //dz = -95;

            // Флаг НУ
            //fl_first1_Coord = 0;
            fl_first2_Coord = 0;
            //fl_first3_Coord = 0;
            //fl_first5_Coord = 0;
            //fl_first6_Coord = 0;
            //fl_first7_Coord = 0;
            //fl_first8_Coord = 0;
            // ---------------------------------------------------------------------Координаты


            // Пеленг PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP
            // Флаг НУ
            fl_first_Pel = 0;

            // Вспомогательные флаги
            //fl1_Pel = 0;

            // Индекс фиктивной точки (1,2, ...)
            IndFP_Pel = 1;
            // Индекс i-й фиктивной точки в массивах(0,...)
            IndMas_Pel = 0;

            // Расстояние по поверхности земли от точки стояния пеленгатора до текущей 
            // фиктивной точки 
            //Mi_Pel = 0;
            // Расстояние по прямой от точки стояния пеленгатора до пересечения прямой
            // (проходящей через текущую фиктивную точку и начало координат)и плоскости,
            // касательной в точке стояния пеленгатора
            //Di_Pel = 0;

            // Радиус Земли (шар)
            REarth_Pel = 6378245;
            // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
            REarthP_Pel = 0;
            // Радиус Земли  в (i-1)-й фиктивной точке с учетом текущей широты 
            //REarthF_Pel=0;

            // Координаты фиктивной точки в СК пеленгатора
            //Xi_Pel = 0;
            //Yi_Pel = 0;
            //Zi_Pel = 0;

            // Координаты фиктивной точки в СК1 (Поворот СКП на угол=широте пеленгатора)
            //Xi0_Pel = 0;
            //Yi0_Pel = 0;
            //Zi0_Pel = 0;

            // Координаты фиктивной точки в опорной геоцентрической СК
            // (Поворот СК1 на угол=долготе пеленгатора)
            XiG_Pel = 0;
            YiG_Pel = 0;
            ZiG_Pel = 0;

            // Угловые координаты фиктивной точки в опорной геоцентрической СК
            Ri_Pel = 0;
            LATi_Pel = 0;
            LONGi_Pel = 0;

            // Координаты точки пересечения прямой (проходящей через текущую фиктивную
            // точку и начало координат) и плоскости касательной в точке стояния 
            // пеленгатора в опорной геоцентрической СК
            //XiG1_Pel = 0;
            //YiG1_Pel = 0;
            //ZiG1_Pel = 0;
            //Ri1_Pel = 0;

            // Центральный угол дуги Mi
            //Alpha = 0;

            // Широта и долгота стояния пеленгатора
            LatP_Pel_tmp = 0;
            LongP_Pel_tmp = 0;

            // Количество фиктивных точек в плоскости пеленгования пеленгатора
            NumbFikt_Pel_tmp = 0;

            // Max дальность отображения пеленга
            Mmax_Pel_tmp = 0;

            // Пеленг
            Theta_Pel_tmp = 0;

            // Счетчик
            //sch_Pel = 0;

            // Координаты пеленгатора в опорной геоцентрической СК
            XP_Pel_tmp = 0;
            YP_Pel_tmp = 0;
            ZP_Pel_tmp = 0;

            // ----------------------------------------------------------------
            // !!! VAR2

            Lat_Pel_N = 0;  //Lat(n)
            Lat_Pel_N1 = 0;  //Lat(n-1)
            Long_Pel_N = 0;  //Long(n)
            Long_Pel_N1 = 0;  //Long(n-1)
            V_Pel = 0;

            fl_var2 = 0;
            fl1_var2 = 0;
            fl2_var2 = 0;

            // ----------------------------------------------------------------

            // PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP Пеленг

            // Триангуляция TriangTriangTriangTriangTriangTriangTriangTriangTriangTrian
            // Флаг НУ
            fl_first_Tr = 0;

            // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
            REarthP1_Tr = 0;
            REarthP2_Tr = 0;

            // Расчетные коэффициенты
            K1_Tr = 0;
            K2_Tr = 0;
            K3_Tr = 0;

            // Коэффициенты канонического уравнения для плоскости пеленгования ИРИ
            // i-ым пеленгатором 
            A1_Tr = 0; // Для пеленгатора1
            T1_Tr = 0;
            C1_Tr = 0;
            A2_Tr = 0; // Для пеленгатора2
            T2_Tr = 0;
            C2_Tr = 0;

            // Координаты искомого ИРИ в опорной геоцентрической СК
            XIRI1_Tr = 0;
            XIRI2_Tr = 0;
            YIRI1_Tr = 0;
            YIRI2_Tr = 0;
            ZIRI1_Tr = 0;
            ZIRI2_Tr = 0;

            // Координаты середины отрезка, соединяющего N-ые фиктивные точки
            XM_Tr = 0;
            YM_Tr = 0;
            ZM_Tr = 0;

            L1_Tr = 0;
            L2_Tr = 0;

            // Координаты пеленгатора в опорной геоцентрической СК
            XP1_Tr_tmp = 0;
            YP1_Tr_tmp = 0;
            ZP1_Tr_tmp = 0;
            XP2_Tr_tmp = 0;
            YP2_Tr_tmp = 0;
            ZP2_Tr_tmp = 0;

            // Координаты N-й фиктивной точки в опорной геоцентрической СК
            XFN1_Tr_tmp = 0;
            YFN1_Tr_tmp = 0;
            ZFN1_Tr_tmp = 0;
            XFN2_Tr_tmp = 0;
            YFN2_Tr_tmp = 0;
            ZFN2_Tr_tmp = 0;

            // TriangTriangTriangTriangTriangTriangTriangTriangTriangTrian Триангуляция


        } // Конструктор
        // ************************************************************************* Конструктор

        // *************************************************************************************
        // Module
        // *************************************************************************************
        public double module(double a)
        {
            if (a >= 0) return (a);
            else return (-a);

        } // P/P module
        // *************************************************************************************

        // *************************************************************************************
        //  Считывание координат карты по клику мыши на карте
        // *************************************************************************************

        public static void f_XYMap(double x, double y)
        {
            GlobalVarLn.MapX1 = x;
            GlobalVarLn.MapY1 = y;

        } // f_XYMap
        // *************************************************************************************


        // ***********************************************************************
        // Преобразование dd.ddddd (grad) -> DD MM SS 
        // (дробное значение градусов -> в градусы(целое значение), минуты(целое значение),
        // секунды(дробное значение))

        // Входные параметры:
        // Grad_Dbl_Coord - градусы в дробном виде

        // Выходные параметры:
        // Grad_I_Coord - градусы (целое)
        // Min_Coord - минуты (целое)
        // Sec_Coord - секунды (дробное)
        // ***********************************************************************
        public void f_Grad_GMS
            (

                // Входные параметры (grad)
                double Grad_Dbl_Coord,

                // Выходные параметры 
                ref int Grad_I_Coord,
                ref int Min_Coord,
                ref double Sec_Coord

            )
        {

            Grad_I_Coord = (int)(Grad_Dbl_Coord);

            Min_Coord = (int)((module(Grad_Dbl_Coord) - module(Grad_I_Coord)) * 60);

            Sec_Coord = ((module(Grad_Dbl_Coord) - module(Grad_I_Coord)) * 60 - Min_Coord) * 60;

        } // Функция f_Grad_GMS
        //************************************************************************

        // ***********************************************************************
        // Расчет приращения по долготе при преобразованиях координат WGS84 (эллипсоид)
        // <-> эллипсоид Красовского(Пулково-42)   (преобразования Молоденского) в
        //  угл.сек

        // Входные параметры:
        // Lat_Coord_8442 - широта (град)
        // Long_Coord_8442 - долгота (град)
        // H_Coord_8442 - высота (км)
        // dX_Coord,dY_Coord,dZ_Coord - !!! это д.б.глобальные переменные, в которых
        // хранится DATUM (м) 

        // Выходные параметры:
        // dLong_Coord - приращение по долготе, !!! угловые секунды
        // ***********************************************************************
        public void f_dLong
            (

                // Входные параметры (внутри функции переводятся в рад и м)
                double Lat_Coord_8442,   // широта   (grad)
                double Long_Coord_8442,  // долгота  (grad)
                double H_Coord_8442,     // высота   (km)

                // DATUM
                double dX_Coord,
                double dY_Coord,
                double dZ_Coord,

                // Выходные параметры 
                ref double dLong_Coord   // приращение по долготе, угл.сек

            )
        {

            double N;
            double Lat_Coord_8442_tmp;
            double Long_Coord_8442_tmp;
            double H_Coord_8442_tmp;


            // Initial conditions ****************************************************
            // Initial conditions

            if (fl_first2_Coord == 0)
            {

                // Эллипсоид Красовского
                a1P = 1 / 298.3;         // Сжатие
                e2P = 2 * a1P - a1P * a1P; // Квадрат эксцентриситета

                // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
                a1W = 1 / 298.257223563; // Сжатие
                e2W = 2 * a1W - a1W * a1W;  // Квадрат эксцентриситета

                // Вспомогательные значения для преобразования эллипсоидов
                a = (aP + aW) / 2;
                e2 = (e2P + e2W) / 2;
                da = aW - aP;
                de2 = e2W - e2P;
                // ...............................................................

                fl_first2_Coord = 1;
                // ...............................................................

            }
            // **************************************************** Initial conditions

            // ...................................................................
            // Перевод в радианы и м

            H_Coord_8442_tmp = H_Coord_8442 * 1000; // km->m

            // grad->rad
            Lat_Coord_8442_tmp = (Lat_Coord_8442 * Math.PI) / 180;
            Long_Coord_8442_tmp = (Long_Coord_8442 * Math.PI) / 180;
            // ...................................................................

            // Радиус кривизны первого вертикала
            N = a / Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp));

            dLong_Coord = ro / ((N + H_Coord_8442_tmp) * Math.Cos(Lat_Coord_8442_tmp)) *
                         (-dX_Coord * Math.Sin(Long_Coord_8442_tmp) + dY_Coord * Math.Cos(Long_Coord_8442_tmp)) +
                        Math.Tan(Lat_Coord_8442_tmp) * (1 - e2) * (wx * Math.Cos(Long_Coord_8442_tmp) + wy * Math.Sin(Long_Coord_8442_tmp)) - wz;


        } // Функция f_dLong
        //************************************************************************

        // ***********************************************************************
        // Расчет приращения по широте при преобразованиях координат WGS84 (эллипсоид)
        // <-> эллипсоид Красовского(Пулково-42)   (преобразования Молоденского) в
        //  угл.сек

        // Входные параметры:
        // Lat_Coord_8442 - широта (град)
        // Long_Coord_8442 - долгота (град)
        // H_Coord_8442 - высота (км)
        // dX_Coord,dY_Coord,dZ_Coord - !!! это д.б.глобальные переменные, в которых
        // хранится DATUM (м) 

        // Выходные параметры:
        // dLat_Coord - приращение по широте, !!! угловые секунды
        // ***********************************************************************
        public void f_dLat
            (

                // Входные параметры (внутри функции переводятся в рад и м)
                double Lat_Coord_8442,   // широта (grad)
                double Long_Coord_8442,  // долгота (grad)
                double H_Coord_8442,     // высота (km)

                // DATUM
                double dX_Coord,
                double dY_Coord,
                double dZ_Coord,

                // Выходные параметры 
                ref double dLat_Coord    // приращение по широте, угл.сек

            )
        {

            double N, M;
            double Lat_Coord_8442_tmp;
            double Long_Coord_8442_tmp;
            double H_Coord_8442_tmp;

            // Initial conditions ****************************************************
            // Initial conditions

            if (fl_first2_Coord == 0)
            {
                // ...................................................................

                // Эллипсоид Красовского
                a1P = 1 / 298.3;                 // Сжатие
                //e2P = 2 * a1P - a1P*a1P;       // Квадрат эксцентриситета
                e2P = 2 * a1P - Math.Pow(a1P, 2); // Квадрат эксцентриситета


                // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
                a1W = 1 / 298.257223563; // Сжатие
                e2W = 2 * a1W - a1W * a1W;  // Квадрат эксцентриситета

                // Вспомогательные значения для преобразования эллипсоидов
                a = (aP + aW) / 2;
                e2 = (e2P + e2W) / 2;
                da = aW - aP;
                de2 = e2W - e2P;
                // ...............................................................

                fl_first2_Coord = 1;
                // ...............................................................

            }
            // **************************************************** Initial conditions

            // ...................................................................
            // Перевод в радианы и м

            H_Coord_8442_tmp = H_Coord_8442 * 1000; // km->m

            // grad->rad
            Lat_Coord_8442_tmp = (Lat_Coord_8442 * Math.PI) / 180;
            Long_Coord_8442_tmp = (Long_Coord_8442 * Math.PI) / 180;
            // ...................................................................

            // Радиус кривизны первого вертикала
            N = a / Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp));

            // Радиус кривизны меридиана
            M = a * (1 - e2) *
                (1 / (Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp)) *
                    Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp)) *
                    Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp))));

            dLat_Coord = (ro / (M + H_Coord_8442_tmp)) *
                          ((N / a) * e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Cos(Lat_Coord_8442_tmp) * da +
                          ((N * N) / (a * a) + 1) * N * Math.Sin(Lat_Coord_8442_tmp) * Math.Cos(Lat_Coord_8442_tmp) * (de2 / 2) -
                          dX_Coord * Math.Cos(Long_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp) -
                          dY_Coord * Math.Sin(Long_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp) +
                          dZ_Coord * Math.Cos(Lat_Coord_8442_tmp)) -
                          wx * Math.Sin(Long_Coord_8442_tmp) * (1 + e2 * Math.Cos(2 * Lat_Coord_8442_tmp)) +
                          wy * Math.Cos(Long_Coord_8442_tmp) * (1 + e2 * Math.Cos(2 * Lat_Coord_8442_tmp)) -
                          ro * ms * e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Cos(Lat_Coord_8442_tmp);


        } // Функция f_dLat
        //************************************************************************

        // ***********************************************************************
        // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)

        // Входные параметры:
        // Lat_Coord_8442 - широта (град)
        // Long_Coord_8442 - долгота (град)
        // H_Coord_8442 - высота (км)
        // Lat_Coord - приращение по широте, !!! угловые секунды
        // Long_Coord - приращение по долготе, !!! угловые секунды

        // Выходные параметры:
        // Lat_Coord_Vyx_8442 - широта (град)
        // Long_Coord_Vyx_8442 - долгота (град)

        // ***********************************************************************
        public void f_WGS84_SK42_Lat_Long
               (

                   // Входные параметры (grad,km)
                   double Lat_Coord_8442,   // широта
                   double Long_Coord_8442,  // долгота
                   double H_Coord_8442,     // высота
                   double dLat_Coord,       // приращение по долготе,угл.сек
                   double dLong_Coord,      // приращение по долготе,угл.сек

                   // Выходные параметры (grad)
                   ref double Lat_Coord_Vyx_8442,   // широта
                   ref double Long_Coord_Vyx_8442   // долгота

               )
        {

            // grad
            //Lat_Coord_Vyx_8442 = (Lat_Coord_8442 * 180) / Math.PI - dLat_Coord / 3600; 
            //Long_Coord_Vyx_8442 = (Long_Coord_8442 * 180) / Math.PI - dLong_Coord / 3600;
            Lat_Coord_Vyx_8442 = Lat_Coord_8442 - dLat_Coord / 3600;
            Long_Coord_Vyx_8442 = Long_Coord_8442 - dLong_Coord / 3600;

        } // Функция f_WGS84_SK42_Lat_Long
        //************************************************************************

        // ***********************************************************************
        // Преобразование геодезических координат (широта, долгота, высота) 
        // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
        // проекции Гаусса-Крюгера

        // Входные параметры:
        // Lat_Coord_Kr - широта (град)
        // Long_Coord_Kr - долгота (град)

        // Выходные параметры:
        // X_Coord_Kr, Y_Coord_Kr - плоские прямоугольные координаты (км)
        // ***********************************************************************
        public void f_SK42_Krug
            (

                // Входные параметры (!!! град)
                double Lat_Coord_Kr,   // широта
                double Long_Coord_Kr,  // долгота
            //double H_Coord_Kr,   // высота (m)

                // Выходные параметры (км)
                ref double X_Coord_Kr,
                ref double Y_Coord_Kr

            )
        {

            int No_tmp;
            double Lo_tmp, Bo_tmp;

            // Вспомогательные величины
            double Xa_tmp, Xb_tmp, Xc_tmp, Xd_tmp;
            double Ya_tmp, Yb_tmp, Yc_tmp;
            double sin2_B, sin4_B, sin6_B;

            // No ********************************************************************
            // Номер шестиградусной зоны в проекци Гаусса-Крюгера (1,2,...)
            // !!! Long - в град

            No_tmp = (int)(6 + Long_Coord_Kr) / 6;

            // ******************************************************************** No

            // Lo ********************************************************************
            // Расстояние от определяемой точки до осевого меридиана зоны, рад

            Lo_tmp = (Long_Coord_Kr - (3 + 6 * (No_tmp - 1))) / 57.29577951;

            // ******************************************************************** Lo

            // Bo ********************************************************************
            // Переводим широту в рад

            Bo_tmp = (Lat_Coord_Kr * Math.PI) / 180;


            sin2_B = Math.Pow(Math.Sin(Bo_tmp), 2);
            sin4_B = Math.Pow(Math.Sin(Bo_tmp), 4);
            sin6_B = Math.Pow(Math.Sin(Bo_tmp), 6);

            // ******************************************************************** Bo

            // X *********************************************************************

            Xa_tmp = Math.Pow(Lo_tmp, 2) *
                     (109500 - 574700 * sin2_B + 863700 * sin4_B -
                      398600 * sin6_B);

            Xb_tmp = Math.Pow(Lo_tmp, 2) *
                     (278194 - 830174 * sin2_B + 572434 * sin4_B -
                      16010 * sin6_B + Xa_tmp);

            Xc_tmp = Math.Pow(Lo_tmp, 2) *
                    (672483.4 - 811219.9 * sin2_B + 5420 * sin4_B -
                     10.6 * sin6_B + Xb_tmp);

            Xd_tmp = Math.Pow(Lo_tmp, 2) *
                    (1594561.25 + 5336.535 * sin2_B + 26.79 * sin4_B +
                    0.149 * sin6_B + Xc_tmp);

            X_Coord_Kr = 6367558.4968 * Bo_tmp -
                         Math.Sin(Bo_tmp * 2) *
                         (16002.89 + 66.9607 * sin2_B + 0.3515 * sin4_B - Xd_tmp);

            // ********************************************************************* X

            // Y *********************************************************************

            Ya_tmp = Math.Pow(Lo_tmp, 2) *
                    (79690 - 866190 * sin2_B + 1730360 * sin4_B -
                     945460 * sin6_B);

            Yb_tmp = Math.Pow(Lo_tmp, 2) *
                    (270806 - 1523417 * sin2_B + 1327645 * sin4_B -
                     21701 * sin6_B + Ya_tmp);

            Yc_tmp = Math.Pow(Lo_tmp, 2) *
                     (1070204.16 - 2136826.66 * sin2_B + 17.98 * sin4_B -
                      11.99 * sin6_B + Yb_tmp);

            Y_Coord_Kr = (5 + 10 * No_tmp) * 100000 +
                         Lo_tmp * Math.Cos(Bo_tmp) *
                         (6378245 + 21346.1415 * sin2_B + 107.159 * sin4_B +
                          0.5977 * sin6_B + Yc_tmp);

            // ********************************************************************* Y

            // .......................................................................
            // перевод в км

            X_Coord_Kr = X_Coord_Kr / 1000;
            Y_Coord_Kr = Y_Coord_Kr / 1000;
            // .......................................................................


        } // Функция f_SK42_Krug
        //************************************************************************

        // ***********************************************************************
        // Преобразование плоских прямоугольных координат в проекции 
        // Гаусса-Крюгера в геодезические координаты (широта, долгота, высота) 
        // эллипсоида Красовского (СК42) 

        // Входные параметры:
        // X_Coord_Kr, Y_Coord_Kr - плоские прямоугольные координаты (км)

        // Выходные параметры:
        // Lat_Coord_Kr - широта (град)
        // Long_Coord_Kr - долгота (град)

        // ***********************************************************************
        public void f_Krug_SK42
            (

                // Входные параметры (км)
                double X_Coord_Kr,
                double Y_Coord_Kr,

                // Выходные параметры (град)
                ref double Lat_Coord_Kr,   // широта
                ref double Long_Coord_Kr   // долгота

            )
        {

            int No_tmp;
            double Bo_tmp;

            // Вспомогательные величины
            double Bi_tmp, Zo_tmp;
            double Ba_tmp, Bb_tmp, Bc_tmp;
            double dB_tmp;
            double La_tmp, Lb_tmp, Lc_tmp, Ld_tmp;
            double dL_tmp;
            double sin2_B, sin4_B, sin6_B;

            double X_Coord_Kr_tmp;
            double Y_Coord_Kr_tmp;

            // .......................................................................
            // Перевод в м

            X_Coord_Kr_tmp = X_Coord_Kr * 1000;
            Y_Coord_Kr_tmp = Y_Coord_Kr * 1000;
            // .......................................................................


            // No ********************************************************************
            // Номер шестиградусной зоны в проекци Гаусса-Крюгера (1,2,...)

            No_tmp = (int)(Y_Coord_Kr_tmp * Math.Pow(10, -6));

            // ******************************************************************** No

            // Bi,Bo *****************************************************************
            // Bo->Геодезическая широта точки, абсцисса которой равнв абсциссе заданной
            // точки, а ордината равна нулю (рад)

            Bi_tmp = X_Coord_Kr_tmp / 6367558.4968;

            Bo_tmp = Bi_tmp + Math.Sin(Bi_tmp * 2) *
                     (0.00252588685 - 0.0000149186 * Math.Pow(Math.Sin(Bi_tmp), 2) +
                      0.00000011904 * Math.Pow(Math.Sin(Bi_tmp), 4));

            sin2_B = Math.Pow(Math.Sin(Bo_tmp), 2);
            sin4_B = Math.Pow(Math.Sin(Bo_tmp), 4);
            sin6_B = Math.Pow(Math.Sin(Bo_tmp), 6);

            // ***************************************************************** Bi,Bo

            // Bi,Zo *****************************************************************
            // Вспомогательная величина

            if (module(6378245 * Math.Cos(Bo_tmp)) > 1E-15)
                Zo_tmp = (Y_Coord_Kr_tmp - (10 * No_tmp + 5) * 100000) / (6378245 * Math.Cos(Bo_tmp));
            else
                Zo_tmp = (Y_Coord_Kr_tmp - (10 * No_tmp + 5) * 100000) / 1E-15;

            // ***************************************************************** Bi,Zo

            // Широта ****************************************************************

            Ba_tmp = Zo_tmp * Zo_tmp *
                     (0.01672 - 0.0063 * sin2_B + 0.01188 * sin4_B - 0.00328 * sin6_B);

            Bb_tmp = Zo_tmp * Zo_tmp *
                     (0.042858 - 0.025318 * sin2_B + 0.014346 * sin4_B - 0.001264 * sin6_B - Ba_tmp);

            Bc_tmp = Zo_tmp * Zo_tmp *
                    (0.10500614 - 0.04559916 * sin2_B + 0.00228901 * sin4_B -
                     0.00002987 * sin6_B - Bb_tmp);

            dB_tmp = -Zo_tmp * Zo_tmp * Math.Sin(Bo_tmp * 2) *
                     (0.251684631 - 0.003369263 * sin2_B + 0.000011276 * sin4_B - Bc_tmp);


            Lat_Coord_Kr = Bo_tmp + dB_tmp;  // rad
            // **************************************************************** Широта

            // Долгота ***************************************************************

            La_tmp = Zo_tmp * Zo_tmp *
                     (0.0038 + 0.0524 * sin2_B + 0.0482 * sin4_B + 0.0032 * sin6_B);

            Lb_tmp = Zo_tmp * Zo_tmp *
                    (0.01225 + 0.09477 * sin2_B + 0.03282 * sin4_B - 0.00034 * sin6_B - La_tmp);

            Lc_tmp = Zo_tmp * Zo_tmp *
                    (0.0420025 + 0.1487407 * sin2_B + 0.005942 * sin4_B - 0.000015 * sin6_B - Lb_tmp);

            Ld_tmp = Zo_tmp * Zo_tmp *
                     (0.16778975 + 0.16273586 * sin2_B - 0.0005249 * sin4_B -
                      0.00000846 * sin6_B - Lc_tmp);

            dL_tmp = Zo_tmp *
                     (1 - 0.0033467108 * sin2_B - 0.0000056002 * sin4_B -
                      0.0000000187 * sin6_B - Ld_tmp);

            Long_Coord_Kr = 6 * (No_tmp - 0.5) / 57.29577951 + dL_tmp; // rad
            // *************************************************************** Долгота

            // .......................................................................
            // Перевод в градусы

            Lat_Coord_Kr = (Lat_Coord_Kr * 180) / Math.PI;
            Long_Coord_Kr = (Long_Coord_Kr * 180) / Math.PI;
            // .......................................................................


        } // Функция f_Krug_SK42
        //************************************************************************


        // ***********************************************************************
        // Отрисовка траектории N фиктивных точек по пеленгу
        //
        // Входные параметры: 
        // Theta_Pel(град) - пеленг,
        // Mmax_Pel(км) - максимальная дальность отображения пеленга,
        // NumberFikt_Pel - количество фиктивных точек
        // LatP_Pel,LongP_Pel (град) - широта и долгота стояния пеленгатора 

        //
        // Выходные параметры: 
        // массив mas_Pel(R(км),Latitude(град),Longitude(град)) ->
        // дальность, широта, долгота
        // массив mas_Pel_XYZ (км) -> координаты в опорной геоцентрической СК
        // XP_Pel,YP_Pel,ZP_Pel (км) -координаты пеленгатора в опорной геоцентрической СК
        // XFN_Pel,YFN_Pel,ZFN_Pel (км) -координаты N-й фиктивной точки в опорной 
        // геоцентрической СК

        // Широта -90(юг)...+90(север)
        // Долгота -180(на запад от Гринвича)...+180(на восток от Гринвича)
        // Пеленг 0...360(по часовой стрелке)
        // ***********************************************************************
        public void f_Peleng
            (

                 // Пеленг
                 double Theta_Pel,
            // Max дальность отображения пеленга
                 double Mmax_Pel,
            // Количество фиктивных точек в плоскости пеленгования пеленгатора
                 uint NumbFikt_Pel,

                 // Широта и долгота стояния пеленгатора
                 double LatP_Pel,
                 double LongP_Pel,

                 // Координаты пеленгатора в опорной геоцентрической СК
                 ref double XP_Pel,
                 ref double YP_Pel,
                 ref double ZP_Pel,

                 // Координаты N-й фиктивной точки в опорной геоцентрической СК
                 ref double XFN_Pel,
                 ref double YFN_Pel,
                 ref double ZFN_Pel,

                 // Координаты фиктивных точек
                 ref double[] mas_Pel,
                 ref double[] mas_Pel_XYZ

                 // ForOtl
            // Time of model
            //double *pT,
            // Discrete of time
            //double *pdt,
            //double *pmas_Otl_Pel        


            )
        {

            // .........................................................................
            //double yyy;
            //int jjj;
            //int lngp_i, lngi_i, lngpi_i;
            //double lngp_d, lngi_d, lngpi_d;


            //jjj = 0;
            //lngp_i = 0;
            //lngp_d = 0;
            //lngi_i = 0;
            //lngi_d = 0;
            //lngpi_i = 0;
            //lngpi_d = 0;
            // .........................................................................


            // Initial conditions ****************************************************
            // Initial conditions

            //if (fl_first_Pel == 0)
            //{

                // ...............................................................
                // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 

                // m
                //REarthP_Pel = REarth_Pel*(1.-0.003352*sin(*pLatP_Pel)*sin(*pLatP_Pel));
                REarthP_Pel = REarth_Pel; // Шар
                // ...............................................................

                fl_first_Pel = 1;
                // ...............................................................
                // Широта и долгота стояния пеленгатора

                LatP_Pel_tmp = (LatP_Pel * Math.PI) / 180;   // grad->rad
                LongP_Pel_tmp = (LongP_Pel * Math.PI) / 180;
                // ...............................................................
                // Количество фиктивных точек в плоскости пеленгования пеленгатора

                NumbFikt_Pel_tmp = NumbFikt_Pel;
                // ...............................................................
                // Max дальность отображения пеленга

                Mmax_Pel_tmp = Mmax_Pel * 1000; // km-> m
                // ...............................................................
                // Координаты пеленгатора в опорной геоцентрической СК

                // m
                XP_Pel_tmp = REarthP_Pel * Math.Cos(LatP_Pel_tmp) * Math.Cos(LongP_Pel_tmp);
                YP_Pel_tmp = REarthP_Pel * Math.Cos(LatP_Pel_tmp) * Math.Sin(LongP_Pel_tmp);
                ZP_Pel_tmp = REarthP_Pel * Math.Sin(LatP_Pel_tmp);

                // km
                XP_Pel = XP_Pel_tmp / 1000;
                YP_Pel = YP_Pel_tmp / 1000;
                ZP_Pel = ZP_Pel_tmp / 1000;
                // ...............................................................
                // Пеленг

                // grad -> rad
                Theta_Pel_tmp = (Theta_Pel * Math.PI) / 180;
                // ...............................................................

                // ...............................................................
                // VAR2

                V_Pel = Mmax_Pel_tmp / NumbFikt_Pel;
                Lat_Pel_N1 = LatP_Pel_tmp;
                Long_Pel_N1 = LongP_Pel_tmp;
                fl_var2 = 0;
                fl1_var2 = 0;
                fl2_var2 = 0;
                // ...............................................................

            //}
            // **************************************************** Initial conditions


            while (IndMas_Pel < (NumbFikt_Pel))
            {

/*
                
                        // VAR1 *******************************************************************
                        //yyy = Math.Acos((Math.Sin(LATi_Pel) -Math.Sin(LatP_Pel_tmp)*Math.Cos(Alpha))/
                        //(Math.Cos(LatP_Pel_tmp)*Math.Sin(Alpha)));
                        //yyy = Math.Acos(Math.Sin(LatP_Pel)/Math.Cos(LATi_Pel)); 

                        // Mi ********************************************************************
                        // Расстояние по поверхности земли от точки стояния пеленгатора до текущей 
                        // фиктивной точки (Mi-дуга болшьшого круга с центральным углом Mi/Rз радиан)

                        Mi_Pel = (((double)IndFP_Pel) * Mmax_Pel_tmp) / ((double)(NumbFikt_Pel_tmp));

                        // Центральный угол дуги Mi
                        Alpha = Mi_Pel / REarthP_Pel;
                        // ******************************************************************** Mi

                        // ***********************************************************************
                        if (Alpha >= Math.PI / 2)
                        {

                            jjj += 1;

                            // A2
                            yyy = Math.Acos(Math.Sin(LatP_Pel_tmp) / Math.Cos(LATi_Pel)); // Pel=45grad
                            //yyy = Math.Acos(Math.Sin(LATi_Pel) / Math.Cos(LatP_Pel)); // Pel=45grad
                            //yyy = Math.Asin((1 / Math.Tan(Math.PI / 2 - LatP_Pel)) /
                            //(1 / Math.Tan(Theta_Pel_tmp))); // Pel=45grad

                            NumbFikt_Pel_tmp = NumbFikt_Pel_tmp - IndFP_Pel;
                            IndFP_Pel = 1;
                            Mmax_Pel_tmp = Mmax_Pel_tmp - Mi_Pel;
                            LatP_Pel_tmp = LATi_Pel;
                            LongP_Pel_tmp = LONGi_Pel;
                            //Theta_Pel_tmp = Math.PI - Theta_Pel_tmp;
                            Mi_Pel = (((double)IndFP_Pel) * Mmax_Pel_tmp) / ((double)(NumbFikt_Pel_tmp));
                            Alpha = Mi_Pel / REarthP_Pel;

                            //Theta_Pel_tmp = yyy;

                            // Pel<90 and !=0
                            if ((Theta_Pel < Math.PI / 2) && (Theta_Pel != 0))
                                Theta_Pel_tmp = Math.PI - yyy;  // Pel=45grad

                            // Pel=0
                            else if (Theta_Pel == 0)
                            {
                                //if (jjj<2)
                                //    Theta_Pel_tmp = 0;
                                //else
                                //    Theta_Pel_tmp = Math.PI;

                                lngp_i = (int)(LongP_Pel * 1000);
                                lngp_d = ((double)lngp_i)/1000;
                                lngi_i = (int)(LONGi_Pel * 1000);
                                lngi_d = ((double)lngi_i) / 1000;
                                lngpi_i = (int)(Math.PI * 1000);
                                lngpi_d = ((double)lngpi_i) / 1000;


                               if(lngi_d == lngp_d )
                                  Theta_Pel_tmp = 0;
                               else if (lngi_d == (lngp_d+lngpi_d))
                                   Theta_Pel_tmp = Math.PI;
                               else
                                   Theta_Pel_tmp = 0;
                            }

                            // Pel=90
                            else if (Theta_Pel == Math.PI/2)
                            {
                                Theta_Pel_tmp = Math.PI / 2;
                            }

                            //Theta_Pel_tmp = Math.PI + yyy;   // Pel=315grad
                            //else
                            //Theta_Pel_tmp = yyy;    // Pel=135grad
                            //Theta_Pel_tmp = Math.PI / 2 + Theta_Pel_tmp; 


                        } // Alpha >= Math.PI / 2
                        // ***********************************************************************

                        // Di ********************************************************************
                        // Расстояние по прямой от точки стояния пеленгатора до пересечения прямой
                        // (проходящей через текущую фиктивную точку и начало координат)и плоскости,
                        // касательной в точке стояния пеленгатора

                        //Di_Pel = REarthP_Pel * Math.Tan(Alpha);
                        //Di_Pel = REarthP_Pel * module(Math.Tan(Mi_Pel / REarthP_Pel));
                        Di_Pel = REarthP_Pel * module(Math.Tan(Alpha));

                        // ******************************************************************** Di

                        // XiYiZi ****************************************************************
                        // Координаты точки пересечения прямой (проходящей через текущую фиктивную
                        // точку и начало координат) и плоскости касательной в точке стояния 
                        // пеленгатора в СК пеленгатора

                        Xi_Pel = REarthP_Pel;
                        Yi_Pel = Di_Pel * Math.Sin(Theta_Pel_tmp);
                        Zi_Pel = Di_Pel * Math.Cos(Theta_Pel_tmp);

                        // **************************************************************** XiYiZi

                        // Xi0Yi0Zi0 *************************************************************
                        // Координаты точки пересечения прямой (проходящей через текущую фиктивную
                        // точку и начало координат) и плоскости касательной в точке стояния 
                        // пеленгатора в СК1 (Поворот СКП на угол=широте пеленгатора)

                        Xi0_Pel = Xi_Pel * Math.Cos(LatP_Pel_tmp) - Zi_Pel * Math.Sin(LatP_Pel_tmp);
                        Yi0_Pel = Yi_Pel;
                        Zi0_Pel = Xi_Pel * Math.Sin(LatP_Pel_tmp) + Zi_Pel * Math.Cos(LatP_Pel_tmp);

                        // ************************************************************* Xi0Yi0Zi0

                        // XiG1YiG1ZiG1 **********************************************************
                        // Координаты точки пересечения прямой (проходящей через текущую фиктивную
                        // точку и начало координат) и плоскости касательной в точке стояния 
                        // пеленгатора в опорной геоцентрической СК (Поворот СК1 на угол=долготе 
                        // пеленгатора)

                        // !!!рабочий вариант
                        XiG1_Pel = Xi0_Pel * Math.Cos(LongP_Pel_tmp) - Yi0_Pel * Math.Sin(LongP_Pel_tmp);
                        YiG1_Pel = Xi0_Pel * Math.Sin(LongP_Pel_tmp) + Yi0_Pel * Math.Cos(LongP_Pel_tmp);

                        //XiG1_Pel = Xi0_Pel * Math.Cos(LongP_Pel_tmp) + Yi0_Pel * Math.Sin(LongP_Pel_tmp);
                        //YiG1_Pel = -Xi0_Pel * Math.Sin(LongP_Pel_tmp) + Yi0_Pel * Math.Cos(LongP_Pel_tmp);
                        ZiG1_Pel = Zi0_Pel;

                        // ********************************************************** XiG1YiG1ZiG1

                        // RiLATiLONGi ***********************************************************
                        // Угловые координаты фиктивной точки в опорной геоцентрической СК

                        Ri1_Pel = Math.Sqrt(XiG1_Pel * XiG1_Pel + YiG1_Pel * YiG1_Pel + ZiG1_Pel * ZiG1_Pel);

                        LATi_Pel = Math.Asin(ZiG1_Pel / Ri1_Pel);

                        Def_Longitude_180(
                        //Def_Longitude(

                                      XiG1_Pel,
                                      YiG1_Pel,
                                      Ri1_Pel,
                                      LATi_Pel,
                                      ref LONGi_Pel
                                     );


                        XiG_Pel = REarthP_Pel * Math.Cos(LATi_Pel) * Math.Cos(LONGi_Pel);
                        YiG_Pel = REarthP_Pel * Math.Cos(LATi_Pel) * Math.Sin(LONGi_Pel);
                        ZiG_Pel = REarthP_Pel * Math.Sin(LATi_Pel);
                        Ri_Pel = Math.Sqrt(XiG_Pel * XiG_Pel + YiG_Pel * YiG_Pel + ZiG_Pel * ZiG_Pel);

                        // *********************************************************** RiLATiLONGi

                        // ******************************************************************* VAR1
                
*/





                // VAR2 ******************************************************************
                // !!!VAR2

                // ......................................................................
                // LAT

                if ((fl_var2 == 0) && (fl2_var2 == 0))
                    Lat_Pel_N = Lat_Pel_N1 + (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;

                // Перешли через северный полюс
                else if ((fl_var2 == 1) && (fl2_var2 == 0) && (Math.Cos(Theta_Pel_tmp) >= 0))
                    Lat_Pel_N = Lat_Pel_N1 - (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;
                else if ((fl_var2 == 1) && (fl2_var2 == 0) && (Math.Cos(Theta_Pel_tmp) < 0))
                    Lat_Pel_N = Lat_Pel_N1 + (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;

                // Перешли через южный полюс 
                else if ((fl2_var2 == 1) && (fl_var2 == 0) && (Math.Cos(Theta_Pel_tmp) >= 0))
                    Lat_Pel_N = Lat_Pel_N1 + (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;
                else if ((fl2_var2 == 1) && (fl_var2 == 0) && (Math.Cos(Theta_Pel_tmp) < 0))
                    Lat_Pel_N = Lat_Pel_N1 - (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;


                // Перешли через северный полюс
                if (
                    (fl_var2 == 0) &&
                    (Lat_Pel_N > 0) &&
                    (Lat_Pel_N >= Math.PI / 2)
                   )
                {
                    Lat_Pel_N = Math.PI / 2;

                    fl_var2 = 1;
                    fl2_var2 = 0;
                    fl1_var2 = 0;

                }

                // Перешли через южный полюс
                if (
                    (fl2_var2 == 0) &&
                    (Lat_Pel_N < 0) &&
                    (Lat_Pel_N <= -Math.PI / 2)
                   )
                {
                    Lat_Pel_N = -Math.PI / 2;
                    //Theta_Pel = 0;

                    fl2_var2 = 1;
                    fl_var2 = 0;
                    fl1_var2 = 0;

                }

                // .......................................................................
                // LONG

                // Перешли через полюс
                if (
                    (fl1_var2 == 0) &&
                    ((fl_var2 == 1) || (fl2_var2 == 1))
                   )
                {
                    if (Long_Pel_N1 >= 0)
                        Long_Pel_N1 = Long_Pel_N1 + Math.PI;
                    else if (Long_Pel_N1 < 0)
                        Long_Pel_N1 = Long_Pel_N1 - Math.PI;

                    if (Long_Pel_N1 > Math.PI)
                        Long_Pel_N1 = -(2 * Math.PI - Long_Pel_N1);
                    else if (Long_Pel_N1 < -Math.PI)
                        Long_Pel_N1 = 2 * Math.PI + Long_Pel_N1;


                    fl1_var2 = 1;
                }


                // Обычный расчет
                Long_Pel_N = Long_Pel_N1 +
                            (V_Pel * Math.Sin(Theta_Pel_tmp)) / (REarthP_Pel * Math.Cos(Lat_Pel_N1));

                if (Long_Pel_N > Math.PI)
                    Long_Pel_N = -(2 * Math.PI - Long_Pel_N);
                else if (Long_Pel_N < -Math.PI)
                    Long_Pel_N = 2 * Math.PI + Long_Pel_N;
                // .......................................................................

                // ........................................................................
                XiG_Pel = REarthP_Pel * Math.Cos(Lat_Pel_N) * Math.Cos(Long_Pel_N);
                YiG_Pel = REarthP_Pel * Math.Cos(Lat_Pel_N) * Math.Sin(Long_Pel_N);
                ZiG_Pel = REarthP_Pel * Math.Sin(Lat_Pel_N);
                Ri_Pel = Math.Sqrt(XiG_Pel * XiG_Pel + YiG_Pel * YiG_Pel + ZiG_Pel * ZiG_Pel);
                // ........................................................................

                Lat_Pel_N1 = Lat_Pel_N;
                Long_Pel_N1 = Long_Pel_N;
                // ****************************************************************** VAR2


                // Массивы ***************************************************************
                // Занесение в массивы

                // Ri,LATi,LONGi (км, град)
                mas_Pel[IndMas_Pel * 3] = Ri_Pel / 1000;


                // VAR2
                mas_Pel[IndMas_Pel * 3 + 1] = (Lat_Pel_N * 180) / Math.PI;
                mas_Pel[IndMas_Pel * 3 + 2] = (Long_Pel_N * 180) / Math.PI;


                // XYZ в опорной геоцентрической СК (км)
                mas_Pel_XYZ[IndMas_Pel * 3] = XiG_Pel / 1000;
                mas_Pel_XYZ[IndMas_Pel * 3 + 1] = YiG_Pel / 1000;
                mas_Pel_XYZ[IndMas_Pel * 3 + 2] = ZiG_Pel / 1000;

                // *************************************************************** Массивы

                // Вывод для отладки ****************************************************

                //*(pmas_Otl_Pel+IndMas_Pel) = Di_Pel;

                // **************************************************** Вывод для отладки`

                // ***********************************************************************
                // Переход к следующей фиктивной точке

                IndFP_Pel += 1;
                IndMas_Pel += 1;
                // ***********************************************************************

            }; // WHILE

            // km
            XFN_Pel = XiG_Pel / 1000;
            YFN_Pel = YiG_Pel / 1000;
            ZFN_Pel = ZiG_Pel / 1000;


            // Пеленг PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP
            IndFP_Pel = 1;
            IndMas_Pel = 0;
            REarth_Pel = 6378245;
            REarthP_Pel = 0;
            XiG_Pel = 0;
            YiG_Pel = 0;
            ZiG_Pel = 0;
            Ri_Pel = 0;
            LATi_Pel = 0;
            LONGi_Pel = 0;
            LatP_Pel_tmp = 0;
            LongP_Pel_tmp = 0;
            NumbFikt_Pel_tmp = 0;
            Mmax_Pel_tmp = 0;
            Theta_Pel_tmp = 0;
            XP_Pel_tmp = 0;
            YP_Pel_tmp = 0;
            ZP_Pel_tmp = 0;
            Lat_Pel_N = 0;  //Lat(n)
            Lat_Pel_N1 = 0;  //Lat(n-1)
            Long_Pel_N = 0;  //Long(n)
            Long_Pel_N1 = 0;  //Long(n-1)
            V_Pel = 0;
            fl_var2 = 0;
            fl1_var2 = 0;
            fl2_var2 = 0;
            // PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP Пеленг


        } // Функция f_Peleng
        //************************************************************************

        // *************************************************************************
        // Расчет азимута ИРИ относительно СП
        // dY,dX - разность координат между ИРИ и СП на плоскости XOY (dX=Xири-Xсп...)
        // Условно считаем Y - направление на север

        // Возврат: азимут в градусах (0...360 по часовой стрелке)
        // *************************************************************************

        public double f_Def_Azimuth(
                                   double dY,
                                   double dX
                                      )
        {
            double Beta, Beta_tmp;

            Beta = 0;
            Beta_tmp = 0;
            // ------------------------------------------------------------------------
            if (dY != 0)
                Beta_tmp = Math.Atan(module(dX) / module(dY));
            // -------------------------------------------------------------------------
            if ((dX == 0) && (dY >= 0))
                Beta = 0;
            // -------------------------------------------------------------------------
            else if ((dX == 0) && (dY < 0))
                Beta = Math.PI;
            // ------------------------------------------------------------------------
            else if ((dY == 0) && (dX > 0))
                Beta = Math.PI / 2;
            // -------------------------------------------------------------------------
            else if ((dY == 0) && (dX < 0))
                Beta = 3 * Math.PI / 2;
            // -------------------------------------------------------------------------
            else if ((dY > 0) && (dX > 0))
                Beta = Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY > 0) && (dX < 0))
                Beta = 2 * Math.PI - Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY < 0) && (dX > 0))
                Beta = Math.PI - Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY < 0) && (dX < 0))
                Beta = Math.PI + Beta_tmp;
            // -------------------------------------------------------------------------
            // Перевод в градусы

            Beta = (Beta * 180) / Math.PI;
            // -------------------------------------------------------------------------

            return Beta;

        } // P/P f_Def_Azimuth
        // *************************************************************************

        // ***********************************************************************
        // Расчет долготы
        // [0...180] к востоку от Гринвича
        // ]0...-180[ к западу от Гринвича
        // Широта -90(юг)...+90(север)
        // ***********************************************************************

        public void Def_Longitude_180(
                            double X,
                            double Y,
                            double R,
                            double Lat, // rad
                            ref double Long

                                  )
        {
            double Long1;

            // -------------------------------------------------------------------
            if (module(R * Math.Cos(Lat)) > 1E-10)
                Long1 = Math.Asin(module(Y) / (R * Math.Cos(Lat)));
            else
                Long1 = Math.Asin(module(Y) / 1E-10);

            // -------------------------------------------------------------------
            if ((Y == 0) && (X > 0))
                Long = 0;
            // -------------------------------------------------------------------
            else if ((Y == 0) && (X < 0))
                Long = Math.PI;
            // -------------------------------------------------------------------
            else if ((Y > 0) && (X > 0))
                Long = Long1;
            // -------------------------------------------------------------------
            else if ((Y < 0) && (X > 0))
                Long = -Long1;
            // -------------------------------------------------------------------
            else if ((Y > 0) && (X < 0))
                Long = Math.PI - Long1;
            // ------------------------------------------------------------------
            else // X<0 Y<0
                Long = -(Math.PI - Long1);
            // -------------------------------------------------------------------

        } // Функция Def_Longitude_180
        // ***********************************************************************

        // ***********************************************************************
        // Расчет долготы
        // 0...360 к востоку от Гринвича
        // ***********************************************************************

        public void Def_Longitude(
                            double X,
                            double Y,
                            double R,
                            double Lat,
                            ref double Long

                                  )
        {
            double Long1;

            // -------------------------------------------------------------------
            if (module(R * Math.Cos(Lat)) > 1E-10)
                Long1 = Math.Asin(module(Y) / (R * Math.Cos(Lat)));
            else
                Long1 = Math.Asin(module(Y) / 1E-10);

            // -------------------------------------------------------------------
            if ((Y == 0) && (X > 0))
                Long = 0;
            // -------------------------------------------------------------------
            else if ((Y == 0) && (X < 0))
                Long = Math.PI;
            // -------------------------------------------------------------------
            else if ((Y > 0) && (X > 0))
                Long = Long1;
            // -------------------------------------------------------------------
            else if ((Y < 0) && (X > 0))
                Long = 2 * Math.PI - Long1;
            // -------------------------------------------------------------------
            else if ((Y > 0) && (X < 0))
                Long = Math.PI - Long1;
            // ------------------------------------------------------------------
            else // X<0 Y<0
                Long = Math.PI + Long1;
            // -------------------------------------------------------------------

        } // Функция Def_Longitude
        // ***********************************************************************

        // ***********************************************************************
        // Расчет сектора
        // Beta1 - азимут начальной точки (grad)
        // 0...360 по часовой стрелке
        // ***********************************************************************

        public double f_Sect(
                            double Beta1,
                            double Beta2
                                  )
        {
            double Sect;
            Sect = 0;
            // -------------------------------------------------------------------
            if (Beta1 == Beta2)
                Sect = 0;
            // -------------------------------------------------------------------
            else if (Beta1==0)
                Sect=Beta2;
            // -------------------------------------------------------------------
            else if (Beta2<Beta1)
                Sect=360-(Beta1-Beta2);
            // -------------------------------------------------------------------
            else // Beta2>Beta1
                Sect=Beta2-Beta1;
            // -------------------------------------------------------------------

            return Sect;

        } // Функция Def_Longitude
        // ***********************************************************************

        // ОТРИСОВКА DRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWD

        // ***********************************************************************
        // Самолет по долготе и широте (Geo,grad)
        // ***********************************************************************
        public static void f_DrawAirPlane(
                                  double Lat_air,
                                  double Long_air,
                                  double Angle_air
                                         )
        {
            double LatDX, LongDY;

            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed = new Pen(Color.Red, 2);
            Brush brushRed = new SolidBrush(Color.Red);

            // -------------------------------------------------------------------------------------
            // grad->rad
            LatDX = (Lat_air * Math.PI) / 180;
            LongDY = (Long_air * Math.PI) / 180;

            // Подаем rad, получаем там же расстояние на карте в м
            mapGeoToPlane(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
            // -------------------------------------------------------------------------------------
            // Прямоугольник

/*
            if (graph != null)
            {
                
                graph.FillRectangle(brushRed,
                                    (int)LatDX - GlobalVar.axMapScreenGlobal.MapLeft,
                                    (int)LongDY - GlobalVar.axMapScreenGlobal.MapTop,
                                    7,
                                    7
                                   );

                // Надпись
                //TitleObject(typeStation, lat, lon);
            }
*/
            // -------------------------------------------------------------------------------------
            // Самолет

            if (graph != null)
            {

                // Куда поместим рисунок (!!! Изображение д.б. не меньше прямоугольника)
                Rectangle rec = new Rectangle((int)LatDX - GlobalVarLn.axMapScreenGlobal.MapLeft - GlobalVarLn.width_air/2,
                                              (int)LongDY - GlobalVarLn.axMapScreenGlobal.MapTop - GlobalVarLn.height_air/2,
                                              GlobalVarLn.width_air,
                                              GlobalVarLn.height_air
                                             );

                Icon icon1 = new Icon("airplaneBlue24x24.ico");
                Bitmap bmp = icon1.ToBitmap();

                graph.DrawImage(f_rotateImage(bmp, Angle_air), rec);

            }
            // -------------------------------------------------------------------------------------

        } // Функция f_DrawAirPlane
        // ***********************************************************************

        // ***********************************************************************
        // Самолет по XY (pix), Angle(grad), Num
        // ***********************************************************************
        public static void f_DrawAirPlaneXY(
                                  double Xp_air,
                                  double Yp_air,
                                  double Angle_air,
                                  int Num_air
                                         )
        {

            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed = new Pen(Color.Red, 2);
            Brush brushRed = new SolidBrush(Color.Red);
            Font font = new Font("Arial", 10);

            // -------------------------------------------------------------------------------------
            // Самолет

            if (graph != null)
            {

                // Куда поместим рисунок (!!! Изображение д.б. не меньше прямоугольника)
                Rectangle rec = new Rectangle((int)Xp_air - GlobalVarLn.axMapScreenGlobal.MapLeft - GlobalVarLn.width_air / 2,
                                              (int)Yp_air - GlobalVarLn.axMapScreenGlobal.MapTop - GlobalVarLn.height_air / 2,
                                              GlobalVarLn.width_air,
                                              GlobalVarLn.height_air
                                             );

                if(Num_air==1)
                {
                 //Icon icon1 = new Icon("airplane106blue.ico");
                 Icon icon1 = new Icon("airplanePink.ico");
                 Bitmap bmp1 = icon1.ToBitmap();
                 graph.DrawImage(f_rotateImage(bmp1, Angle_air), rec);
                }
                else if(Num_air==2)
                {
                    Icon icon2 = new Icon("airplanePurple.ico");
                 Bitmap bmp2 = icon2.ToBitmap();
                 graph.DrawImage(f_rotateImage(bmp2, Angle_air), rec);
                }
                else if (Num_air == 3)
                {
                    //Icon icon3 = new Icon("airplane106.ico");
                    Icon icon3 = new Icon("airplaneYellow.ico");
                    Bitmap bmp3 = icon3.ToBitmap();
                    graph.DrawImage(f_rotateImage(bmp3, Angle_air), rec);
                }
                else if (Num_air == 4)
                {
                    Icon icon4 = new Icon("airplaneBlue.ico");
                    Bitmap bmp4 = icon4.ToBitmap();
                    graph.DrawImage(f_rotateImage(bmp4, Angle_air), rec);
                }
                else if (Num_air == 5)
                {
                    Icon icon5 = new Icon("airplaneRed.ico");
                    Bitmap bmp5 = icon5.ToBitmap();
                    graph.DrawImage(f_rotateImage(bmp5, Angle_air), rec);
                }

                else 
                {
                    Icon icon6 = new Icon("airplaneGreen.ico");
                    Bitmap bmp6 = icon6.ToBitmap();
                    graph.DrawImage(f_rotateImage(bmp6, Angle_air), rec);
                }


            }
            // -------------------------------------------------------------------------------------
            // Подпись

            graph.DrawString(
                             Convert.ToString(Num_air),
                             font,
                             brushRed,
                             (int)Xp_air - GlobalVarLn.axMapScreenGlobal.MapLeft + 3,
                             (int)Yp_air - GlobalVarLn.axMapScreenGlobal.MapTop +3
                             );
  
            // -------------------------------------------------------------------------------------


        } // Функция f_DrawAirPlaneXY
        // ***********************************************************************

        // ***********************************************************************
        // Самолет по XY (pix), Angle(grad), Num
        // ***********************************************************************
        public static void  f_DrawAirPlaneXY1(
                                  double Xp_air,
                                  double Yp_air,
                                  double Angle_air,
                                  String sNum_air
                                         )
        {

            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            //Pen penRed = new Pen(Color.Red, 2);
            //Brush brushRed = new SolidBrush(Color.Red);
            //Font font = new Font("Arial", 10);
            //Font font1 = new Font("Arial", 9);

            // -------------------------------------------------------------------------------------
            // Самолет

            if (graph != null)
            {

                // Куда поместим рисунок (!!! Изображение д.б. не меньше прямоугольника)
                Rectangle rec = new Rectangle((int)Xp_air - GlobalVarLn.axMapScreenGlobal.MapLeft - GlobalVarLn.width_air / 2,
                                              (int)Yp_air - GlobalVarLn.axMapScreenGlobal.MapTop - GlobalVarLn.height_air / 2,
                                              GlobalVarLn.width_air,
                                              GlobalVarLn.height_air
                                             );

                //Icon icon1 = new Icon("airplanePink.ico");
                //Icon icon1 = new Icon("airplaneBlue.ico");
                Icon icon1 = Properties.Resources.airplaneBlue;

                Bitmap bmp1 = icon1.ToBitmap();
                graph.DrawImage(f_rotateImage(bmp1, Angle_air), rec);

                // -------------------------------------------------------------------------------------
                // Подпись

                Font font2 = new Font(FontFamily.GenericSansSerif, 10.0F, FontStyle.Regular, GraphicsUnit.Pixel);
                Brush brushi = new SolidBrush(Color.White);
                Brush brushj = new SolidBrush(Color.Blue);
                Rectangle rec1 = new Rectangle((int)Xp_air - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                                  (int)Yp_air - GlobalVarLn.axMapScreenGlobal.MapTop + 10,
                                                  40,
                                                  13
                                                 );



                graph.FillRectangle(brushi, rec1);

                graph.DrawString(
                                 sNum_air,
                                 font2,
                                 brushj,
                                 (int)Xp_air - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                 (int)Yp_air - GlobalVarLn.axMapScreenGlobal.MapTop + 11
                                 );


                // -------------------------------------------------------------------------------------

            }
            // -------------------------------------------------------------------------------------

        } // Функция f_DrawAirPlaneXY1
        // ***********************************************************************

        // ***********************************************************************
        // Поворот изображения
        // ***********************************************************************

        public static Bitmap f_rotateImage(Bitmap input, double angle)
        {
            Bitmap result = new Bitmap(input.Width, input.Height);
            Graphics g = Graphics.FromImage(result);
            g.TranslateTransform((float)input.Width / 2, (float)input.Height / 2);
            g.RotateTransform((float)angle);
            g.TranslateTransform(-(float)input.Width / 2, -(float)input.Height / 2);
            g.DrawImage(input, new Point(0, 0));
            return result;

        } // f_rotateImage
        // ***********************************************************************

        // ***********************************************************************
        // Добавить самолет
        // ff_first=0 -> 1-й раз в лист
        // ***********************************************************************
        public static void f_AddAirPlane1(
                                  double Lat_air,   // grad
                                  double Long_air,
                                  String sNum_air,
                                  int ff_first
                                         )
        {
            double LatDX = 0;
            double LongDY = 0;
            double LatDX_m = 0;
            double LongDY_m = 0;
            int f = 0;
            double X1 = 0;
            double X2 = 0;
            double Y1 = 0;
            double Y2 = 0;
            double dX = 0;
            double dY = 0;
            double Az = 0;

            int iij = 0;

            double dchislo1 = 0;
            long ichislo1 = 0;
            double dchislo2 = 0;
            long ichislo2 = 0;
            double dchislo3 = 0;
            long ichislo3 = 0;
            double dchislo4 = 0;
            long ichislo4 = 0;
            // -------------------------------------------------------------------------------------
            AirPlane objAirPlane = new AirPlane();
            // -------------------------------------------------------------------------------------
            // Широта,долгота в градусах

            objAirPlane.Lat = Lat_air;
            objAirPlane.Long = Long_air;
            // -------------------------------------------------------------------------------------
            // grad->rad
            LatDX = (Lat_air * Math.PI) / 180;
            LongDY = (Long_air * Math.PI) / 180;

            // Подаем rad, получаем там же расстояние на карте в м
            mapGeoToPlane(MapForm.hmapl1, ref LatDX, ref LongDY);

            LatDX_m = LatDX;
            LongDY_m = LongDY;

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
            // -------------------------------------------------------------------------------------
            // расстояние на карте в м

            objAirPlane.X_m = LatDX_m;
            objAirPlane.Y_m = LongDY_m;
            X2 = LatDX_m;
            Y2 = LongDY_m;
            // -------------------------------------------------------------------------------------
            objAirPlane.sNum = String.Copy(sNum_air);
            // -------------------------------------------------------------------------------------
            // 1-ое занесение в лист

            // IF1
            if (ff_first == 0)
            {
                objAirPlane.sh = 1;
                objAirPlane.fl_sh = 1;
                objAirPlane.Angle = 0;

                GlobalVarLn.list_air.Add(objAirPlane);
                GlobalVarLn.Number_air += 1;
                return;

            } // IF1:ff_first == 0
            // -------------------------------------------------------------------------------------
            // НЕ 1-ое занесение в лист

            // IF1
            else
            {
                // ...................................................................................
                // Есть ли самолет с таким же номером

                f = 0;
                for (int i = (GlobalVarLn.Number_air - 1); i >= 0; i--)
                {
                    // ...................................................................................
                    // Есть самолет с таким же номером

                    // IF2
                    if (
                        String.Compare(GlobalVarLn.list_air[i].sNum, objAirPlane.sNum) == 0
                       )
                    {
                        f = 1;

                        ichislo1 = (long)(GlobalVarLn.list_air[i].Lat * 100000);
                        dchislo1 = ((double)ichislo1) / 100000;
                        ichislo2 = (long)(objAirPlane.Lat * 100000);
                        dchislo2 = ((double)ichislo2) / 100000;

                        ichislo3 = (long)(GlobalVarLn.list_air[i].Long * 100000);
                        dchislo3 = ((double)ichislo3) / 100000;
                        ichislo4 = (long)(objAirPlane.Long * 100000);
                        dchislo4 = ((double)ichislo4) / 100000;

                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // Координаты повторились

                        // IF3
                        if (
                            (dchislo1 == dchislo2) &&
                            (dchislo3 == dchislo4)
                           )
                            return;
                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // координаты не повторились

                        // IF3
                        else
                        {
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // счетчик вышел

                            // IF4
                            if (GlobalVarLn.list_air[i].sh >= GlobalVarLn.ndraw_air)
                            {

                                X1 = GlobalVarLn.list_air[i].X_m;
                                Y1 = GlobalVarLn.list_air[i].Y_m;
                                // На карте X - вверх
                                dY = X2 - X1;
                                dX = Y2 - Y1;
                                // grad
                                Az = f_Def_Azimuth1(dY, dX);
                                objAirPlane.Angle = Az;

                                objAirPlane.sh = 1;
                                objAirPlane.fl_sh = 2;

                                // FOR2: Убрать все предыдущие
                                for (iij = (GlobalVarLn.Number_air - 1); iij >= 0; iij--)
                                {

                                    if (String.Compare(GlobalVarLn.list_air[iij].sNum, objAirPlane.sNum) == 0)
                                    {
                                        GlobalVarLn.list_air.Remove(GlobalVarLn.list_air[iij]);
                                        GlobalVarLn.Number_air -= 1;
                                    }
                                } // FOR2

                                // Добавили новый
                                GlobalVarLn.list_air.Add(objAirPlane);
                                GlobalVarLn.Number_air += 1;

                                // Убрать с карты

                                //if (GlobalVarLn.axMapScreenGlobal.InvokeRequired)
                                //    GlobalVarLn.axMapScreenGlobal.Invoke((Action)(() => { GlobalVarLn.axMapScreenGlobal.Repaint(); }));
                                //else
                                //    GlobalVarLn.axMapScreenGlobal.Repaint();

                                return;

                            } // IF4: счетчик вышел
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // счетчик не вышел-> Добавить и вычислить азимут

                          // IF4
                            else
                            {
                                objAirPlane.sh = GlobalVarLn.list_air[i].sh + 1;
                                objAirPlane.fl_sh = 2;

                                X1 = GlobalVarLn.list_air[i].X_m;
                                Y1 = GlobalVarLn.list_air[i].Y_m;

                                // На карте X - вверх
                                dY = X2 - X1;
                                dX = Y2 - Y1;

                                // grad
                                Az = f_Def_Azimuth1(dY, dX);

                                objAirPlane.Angle = Az;

                                GlobalVarLn.list_air.Add(objAirPlane);

                                GlobalVarLn.Number_air += 1;
                                return;

                            } // IF4: счетчик не вышел
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,


                        } // IF3 координаты не повторились
                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                    } // IF2 (есть такой же самолет)
                    // ...................................................................................

                } // FOR
                // ...................................................................................

                // ...................................................................................
                // Такого самолета не нашлось при НЕ первом сеансе получения данных

                if (f == 0)
                {
                    objAirPlane.sh = 1;
                    objAirPlane.fl_sh = 1;
                    objAirPlane.Angle = 0;
                    GlobalVarLn.list_air.Add(objAirPlane);
                    GlobalVarLn.Number_air += 1;
                }
                // ...................................................................................

            } // IF1:НЕ 1-ое занесение в лист
            // -------------------------------------------------------------------------------------

        } // Функция f_AddAirPlane1
        // ***********************************************************************

        // ***********************************************************************
        // Удалить все самолеты с этим номером
        // ***********************************************************************
        public static void f_DelAirPlane(
                                  String sNum_air
                                       )
        {
            int iij = 0;

            // FOR1: Убрать все с этим номером
            for (iij = (GlobalVarLn.Number_air - 1); iij >= 0; iij--)
            {

                if (String.Compare(GlobalVarLn.list_air[iij].sNum, sNum_air) == 0)
                {
                    GlobalVarLn.list_air.Remove(GlobalVarLn.list_air[iij]);
                    GlobalVarLn.Number_air -= 1;
                }
            } // FOR1

            if (GlobalVarLn.Number_air==0) // Самолетов не осталось
            {
                GlobalVarLn.fl_AirPlane = 0;
            }

            // Перерисовать карту
            GlobalVarLn.axMapScreenGlobal.Repaint();


        } // Функция f_DelAirPlane
        // ***********************************************************************

        // ***********************************************************************
        // Удалить все самолеты с этим номером
        // ***********************************************************************
        public static void f_DelAirPlaneAll()
        {
            int iij = 0;

            if (GlobalVarLn.list_air.Count != 0)
            {
                // FOR1: Убрать все 
                for (iij = (GlobalVarLn.Number_air - 1); iij >= 0; iij--)
                {

                        GlobalVarLn.list_air.Remove(GlobalVarLn.list_air[iij]);
                        GlobalVarLn.Number_air -= 1;

                } // FOR1

                GlobalVarLn.Number_air = 0;
                GlobalVarLn.fl_AirPlane = 0;
                // Перерисовать карту
                GlobalVarLn.axMapScreenGlobal.Repaint();

            } // IF GlobalVarLn.list_air.Count != 0


        } // Функция f_DelAirPlaneAll
        // ***********************************************************************

        // ***********************************************************************
        // Добавить самолет
        // ***********************************************************************
        public static void f_AddAirPlane(
                                  double Lat_air,   // grad
                                  double Long_air,
                                  int Num_air
                                         )
        {
            double LatDX = 0;
            double LongDY = 0;
            double LatDX_m = 0;
            double LongDY_m = 0;
            int f = 0;
            double X1 = 0;
            double X2 = 0;
            double Y1 = 0;
            double Y2 = 0;
            double dX = 0;
            double dY = 0;
            double Az = 0;
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_AirPlane = 1;
            // -------------------------------------------------------------------------------------
            GlobalVarLn.Number_air += 1;
            // ------------------------------------------------------------------------------------
            GlobalVarLn.mass_stAirPlane[GlobalVarLn.Number_air - 1].Lat = Lat_air;
            GlobalVarLn.mass_stAirPlane[GlobalVarLn.Number_air - 1].Long = Long_air;
            // ------------------------------------------------------------------------------------

            // grad->rad
            LatDX = (Lat_air * Math.PI) / 180;
            LongDY = (Long_air * Math.PI) / 180;

            // Подаем rad, получаем там же расстояние на карте в м
            mapGeoToPlane(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
            LatDX_m = LatDX;
            LongDY_m = LongDY;

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
            // -------------------------------------------------------------------------------------
            //GlobalVar.mass_stAirPlane[GlobalVar.Number_air - 1].X_p = LatDX;
            //GlobalVar.mass_stAirPlane[GlobalVar.Number_air - 1].Y_p = LongDY;
            GlobalVarLn.mass_stAirPlane[GlobalVarLn.Number_air - 1].X_m = LatDX_m;
            GlobalVarLn.mass_stAirPlane[GlobalVarLn.Number_air - 1].Y_m = LongDY_m;
            X2 = LatDX_m;
            Y2 = LongDY_m;
            GlobalVarLn.mass_stAirPlane[GlobalVarLn.Number_air - 1].Num = Num_air;
            // ------------------------------------------------------------------------------------
            f = 0;
            for (int i = (GlobalVarLn.Number_air - 2); i >= 0; i--)
            {
                if (GlobalVarLn.mass_stAirPlane[i].Num == GlobalVarLn.mass_stAirPlane[GlobalVarLn.Number_air - 1].Num)
                {
                    f = 1;
                    X1 = GlobalVarLn.mass_stAirPlane[i].X_m;
                    Y1 = GlobalVarLn.mass_stAirPlane[i].Y_m;
                    // На карте X - вверх
                    dY = X2 - X1;
                    dX = Y2 - Y1;

                    // grad
                    Az = f_Def_Azimuth1(dY, dX);
                    GlobalVarLn.mass_stAirPlane[GlobalVarLn.Number_air - 1].Angle = Az;
                    break;

                } // IF


            } // FOR

            if (f == 0)
                GlobalVarLn.mass_stAirPlane[GlobalVarLn.Number_air - 1].Angle = 0;


        } // Функция f_AddAirPlane
        // ***********************************************************************

        // ***********************************************************************
        // Перерисовка самолетов
        // ***********************************************************************
        public static void f_ReDrawAirPlane()
        {
            double LatDX = 0;
            double LongDY = 0;


            for (int i = 0; i < GlobalVarLn.Number_air; i++)
            {

                // grad->rad
                LatDX = (GlobalVarLn.mass_stAirPlane[i].Lat * Math.PI) / 180;
                LongDY = (GlobalVarLn.mass_stAirPlane[i].Long * Math.PI) / 180;

                // Подаем rad, получаем там же расстояние на карте в м
                mapGeoToPlane(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

                // Расстояние в м на карте -> пикселы на изображении
                mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

                f_DrawAirPlaneXY(
                                 //GlobalVar.mass_stAirPlane[i].X_p,
                                 //GlobalVar.mass_stAirPlane[i].Y_p,
                                 LatDX,
                                 LongDY,
                                 GlobalVarLn.mass_stAirPlane[i].Angle,
                                 GlobalVarLn.mass_stAirPlane[i].Num
                                 );

            } // FOR


        } // Функция f_ReDrawAirPlane
        // ***********************************************************************

        // ***********************************************************************
        // Перерисовка самолетов
        // ***********************************************************************
        public static void f_ReDrawAirPlane1()
        {
            double LatDX = 0;
            double LongDY = 0;

            for (int i = 0; i < GlobalVarLn.list_air.Count; i++)
            {

                // grad->rad
                LatDX = (GlobalVarLn.list_air[i].Lat * Math.PI) / 180;
                LongDY = (GlobalVarLn.list_air[i].Long * Math.PI) / 180;

                // Подаем rad, получаем там же расстояние на карте в м
                mapGeoToPlane(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

                // Расстояние в м на карте -> пикселы на изображении
                mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

                f_DrawAirPlaneXY1(
                                 LatDX,
                                 LongDY,
                                 GlobalVarLn.list_air[i].Angle,
                                 GlobalVarLn.list_air[i].sNum
                                 );

            } // FOR

        } // Функция f_ReDrawAirPlane1
        // ***********************************************************************

        // *************************************************************************
        // Расчет азимута ИРИ относительно СП
        // dY,dX - разность координат между ИРИ и СП на плоскости XOY (dX=Xири-Xсп...)
        // Условно считаем Y - направление на север

        // Возврат: азимут в градусах (0...360 по часовой стрелке)
        // *************************************************************************

        public static double f_Def_Azimuth1(
                                   double dY,
                                   double dX
                                      )
        {
            double Beta, Beta_tmp;

            Beta = 0;
            Beta_tmp = 0;
            // ------------------------------------------------------------------------
            if (dY != 0) 
                Beta_tmp = Math.Atan(module1(dX) / module1(dY));

            // -------------------------------------------------------------------------
            if ((dX == 0) && (dY >= 0))
                Beta = 0;
            // -------------------------------------------------------------------------
            else if ((dX == 0) && (dY < 0))
                Beta = Math.PI;
            // ------------------------------------------------------------------------
            else if ((dY == 0) && (dX > 0))
                Beta = Math.PI / 2;
            // -------------------------------------------------------------------------
            else if ((dY == 0) && (dX < 0))
                Beta = 3 * Math.PI / 2;
            // -------------------------------------------------------------------------
            else if ((dY > 0) && (dX > 0))
                Beta = Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY > 0) && (dX < 0))
                Beta = 2 * Math.PI - Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY < 0) && (dX > 0))
                Beta = Math.PI - Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY < 0) && (dX < 0))
                Beta = Math.PI + Beta_tmp;
            // -------------------------------------------------------------------------
            // Перевод в градусы

            Beta = (Beta * 180) / Math.PI;
            // -------------------------------------------------------------------------

            return Beta;

        } // P/P f_Def_Azimuth
        // *************************************************************************

        // *************************************************************************************
        // Module
        // *************************************************************************************
        public static double module1(double a)
        {
            if (a >= 0) return (a);
            else return (-a);

        } // P/P module
        // *************************************************************************************

        // ******************************************************************************************
        // Зона энергодоступности(м на местности)
        // ******************************************************************************************
        public static void f_Map_El_XY_ed(
                                         Point tpCenterPoint,
                                         long iRadiusZone
                                        )
        {

            double dLeftX = 0;
            double dLeftY = 0;
            double dRightX = 0;
            double dRightY = 0;

            double XC = 0;
            double YC = 0;
            XC = tpCenterPoint.X;
            YC = tpCenterPoint.Y;

            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed1 = new Pen(Color.Red, 2);
            Brush brushRed1 = new SolidBrush(Color.Red);

            Pen pen1 = new Pen(Color.HotPink, 5.0f);
            HatchBrush brush1 = new HatchBrush(HatchStyle.Cross, Color.Linen, Color.Transparent);

            // -------------------------------------------------------------------------------------

            //dLeftX = tpCenterPoint.X + iRadiusZone;
            //dLeftY = tpCenterPoint.Y + iRadiusZone;
            //dRightX = tpCenterPoint.X - iRadiusZone;
            //dRightY = tpCenterPoint.Y - iRadiusZone;
            // -------------------------------------------------------------------------------------
            /*
                 long j = 0;
                 j=mapPlaneToPicture(ClassMap.hmapl, ref dLeftX, ref dLeftY);

                if (j > 0)
                {
                    dLeftX = dLeftX - axaxcMapScreen.MapLeft;
                    dLeftY = dLeftY - axaxcMapScreen.MapTop;
                }

                 j = 0;
                 j=mapPlaneToPicture(ClassMap.hmapl, ref dRightX, ref dRightY);

                if (j > 0)
                {
                    dRightX = dRightX - axaxcMapScreen.MapLeft;
                    dRightY = dRightY - axaxcMapScreen.MapTop;
                }

            */
            // -------------------------------------------------------------------------------------
            //int iWidthRect = 0;
            //iWidthRect = (int)(dLeftX - dRightX);

            //int iHeightRect = 0;
            //iHeightRect = (int)(dLeftY - dRightY);

            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                dLeftX = XC - iRadiusZone / 2;
                dLeftY = YC - iRadiusZone / 2;
                dRightX = XC + iRadiusZone / 2;
                dRightY = YC + iRadiusZone / 2;
                mapPlaneToPicture(GlobalVarLn.hmapl, ref XC, ref YC);
                mapPlaneToPicture(GlobalVarLn.hmapl, ref dLeftX, ref dLeftY);
                mapPlaneToPicture(GlobalVarLn.hmapl, ref dRightX, ref dRightY);

                //graph.FillEllipse(brushRed1, (int)dLeftX - axaxcMapScreen.MapLeft, (int)dLeftY - axaxcMapScreen.MapTop, (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));

                graph.DrawEllipse(pen1, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                                        (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));
                graph.FillEllipse(brush1, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                                        (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));


            }
            // -------------------------------------------------------------------------------------

        } // P/P f_Map_El_XY_ed
        // *************************************************************************************

        // ******************************************************************************************
        // Нарисовать метку по координатам (треугольник)
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // Color: 1-Red, 2-Blue
        // ******************************************************************************************
        public static void f_Map_Pol_XY_stat(
                                         double X,
                                         double Y,
                                         int Col,
                                         String s
                                        )
        {

            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed1 = new Pen(Color.Red, 2);
            Brush brushRed1 = new SolidBrush(Color.Red);
            Brush brushBlue = new SolidBrush(Color.Blue);

            Font font = new Font("Arial", 7);
            Brush brushSnow = new SolidBrush(Color.Snow);

            // -------------------------------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении

            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                Point[] toDraw = new Point[3];
                toDraw[0].X = (int)X - (GlobalVarLn.axMapScreenGlobal.MapLeft + 7);
                toDraw[0].Y = (int)Y - (GlobalVarLn.axMapScreenGlobal.MapTop - 7);
                toDraw[1].X = (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft;
                toDraw[1].Y = (int)Y - (GlobalVarLn.axMapScreenGlobal.MapTop + 7);
                toDraw[2].X = (int)X - (GlobalVarLn.axMapScreenGlobal.MapLeft - 7);
                toDraw[2].Y = (int)Y - (GlobalVarLn.axMapScreenGlobal.MapTop - 7);

                if(Col==1)
                    graph.FillPolygon(brushRed1, toDraw);
                else
                    graph.FillPolygon(brushBlue, toDraw);

            }

            // -------------------------------------------------------------------------------------
            // С подписью

            if (s != "")
            {
                graph.FillRectangle(brushSnow,
                                   (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                   (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 10,
                                   20,
                                   10
                                   );

                // Подпись
                graph.DrawString(
                                 s,
                                 font,
                                 brushRed1,
                                 (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                 (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 10
                                 );

                if (Col == 1)
                {
                    // Подпись
                    graph.DrawString(
                                     s,
                                     font,
                                     brushRed1,
                                     (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                     (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 10
                                     );
                }
                else
                {
                    // Подпись
                    graph.DrawString(
                                     s,
                                     font,
                                     brushBlue,
                                     (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                     (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 10
                                     );
                }

            } // S
            // -------------------------------------------------------------------------------------

        } // P/P f_Map_Pol_XY_stat
        // *************************************************************************************

        // **********************************************************************
        // Перерисовка "Энергодоступность по УС"
        // **********************************************************************
        public static void f_Map_Redraw_CommPowerAvail()
        {

            if (GlobalVarLn.flCoordSP_comm == 1) // СП выбрана
            {
                ClassMap.f_Map_Pol_XY_stat(
                              GlobalVarLn.XCenter_comm,  // m
                              GlobalVarLn.YCenter_comm,
                              1,
                              ""
                                           );
            }

            if (GlobalVarLn.flCoordYS1_comm == 1) // YS1 выбран
            {
                //Azb
                if (GlobalVarLn.fl_Azb == 0)
                {
                    ClassMap.f_Map_Pol_XY_stat(
                                  GlobalVarLn.XPoint1_comm,  // m
                                  GlobalVarLn.YPoint1_comm,
                                  2,
                                  "УС1"
                                 );
                }
                else if(GlobalVarLn.fl_Azb == 1)
                {
                    ClassMap.f_Map_Pol_XY_stat(
                                  GlobalVarLn.XPoint1_comm,  // m
                                  GlobalVarLn.YPoint1_comm,
                                  2,
                                  "RQ1"
                                  );
                }
                else 
                {
                    ClassMap.f_Map_Pol_XY_stat(
                                  GlobalVarLn.XPoint1_comm,  // m
                                  GlobalVarLn.YPoint1_comm,
                                  2,
                                  "CC1"
                                  );
                }

            } // YS1 выбран

            if (GlobalVarLn.flCoordYS2_comm == 1) // YS2 выбран
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    ClassMap.f_Map_Pol_XY_stat(
                                  GlobalVarLn.XPoint2_comm,  // m
                                  GlobalVarLn.YPoint2_comm,
                                  2,
                                  "УС2"
                                 );
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    ClassMap.f_Map_Pol_XY_stat(
                                  GlobalVarLn.XPoint2_comm,  // m
                                  GlobalVarLn.YPoint2_comm,
                                  2,
                                  //Azb
                                  "RQ2"
                                 );

                }
                else 
                {
                    ClassMap.f_Map_Pol_XY_stat(
                                  GlobalVarLn.XPoint2_comm,  // m
                                  GlobalVarLn.YPoint2_comm,
                                  2,
                                  //Azb
                                  "CC2"
                                 );

                }

            } // YS2 выбран


        } // P/P f_Map_Redraw_CommPowerAvail
        // **********************************************************************

        // ******************************************************************************************
        // Нарисовать text по координатам 
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // Col: 1-Black 2-Red 
        // ******************************************************************************************
        public static void f_Sup_S(
                                         double X,
                                         double Y,
                                         int Col,
                                         String s
                                        )
        {

            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            //Pen pen1 = new Pen(Color.Black, 2);
            Brush brush1 = new SolidBrush(Color.Black);
            Brush brush2 = new SolidBrush(Color.Red);
            Font font = new Font("Arial", 7);
            Brush brushSnow = new SolidBrush(Color.Snow);

            // -------------------------------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении

            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                if (s != "")
                {
                    graph.FillRectangle(
                                       brushSnow,
                        //(int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                        //(int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 10,
                                       (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 50,
                                       (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop - 17,
                                       //100,
                                       90,
                                       10
                                       );

                    if (Col == 1)
                    {
                        // Подпись
                        graph.DrawString(
                                         s,
                                         font,
                                         brush1,
                                         (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 50,
                                         (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop - 17
                                         );
                    }
                    else
                    {
                        // Подпись
                        graph.DrawString(
                                         s,
                                         font,
                                         brush2,
                                         (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 50,
                                         (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop - 17
                                         );
                    }

                } // S
                // -------------------------------------------------------------------------------------

            }
        } // P/P f_Sup_S
        // *************************************************************************************

        // ******************************************************************************************
        // Нарисовать Circle по координатам 
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // Col: 1-Black 2-Red 
        // ******************************************************************************************
        public static void f_Sup_Circle(
                                         double X,
                                         double Y,
                                         double R
                                         //int Col,
                                         //String s
                                        )
        {

            double dLeftX = 0;
            double dLeftY = 0;
            double dRightX = 0;
            double dRightY = 0;

            double XC = 0;
            double YC = 0;
            XC = X;
            YC = Y;
            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
            Pen pen1 = new Pen(Color.Black, 2);
            // -------------------------------------------------------------------------------------
            if (graph != null)
            {
             dLeftX = XC - R;
             dLeftY = YC - R;
             dRightX = XC + R;
             dRightY = YC + R;
             mapPlaneToPicture(GlobalVarLn.hmapl, ref XC, ref YC);
             mapPlaneToPicture(GlobalVarLn.hmapl, ref dLeftX, ref dLeftY);
              mapPlaneToPicture(GlobalVarLn.hmapl, ref dRightX, ref dRightY);

            graph.DrawEllipse(
                              pen1, 
                              (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft,
                              (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                              (int)(dRightX - dLeftX), 
                              (int)(dRightY - dLeftY)
                              );
            //graph.FillEllipse(brush1, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft,
            //    (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
            //    (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));

           }

        } // P/P f_Sup_Circle
        // *************************************************************************************


        // ******************************************************************************************
        // Нарисовать Line по координатам 
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // ******************************************************************************************
        public static void f_Sup_Line(
                                         double X1,
                                         double Y1,
                                         double X2,
                                         double Y2
                                        )
        {

            // -----------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
            Pen pen2 = new Pen(Color.Black, 2);
            // -----------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X1, ref Y1);
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X2, ref Y2);
            // -----------------------------------------------------------------
            if (graph != null)
            {
                Point point1 = new Point((int)X1 - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)Y1 - 
                                          GlobalVarLn.axMapScreenGlobal.MapTop);
                Point point2 = new Point((int)X2 - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)Y2 - 
                                          GlobalVarLn.axMapScreenGlobal.MapTop);
                graph.DrawLine(pen2, point1, point2);
            }
            // -----------------------------------------------------------------

        } // P/P f_Sup_Line
        // *************************************************************************************

        // **********************************************************************
        // Перерисовка "Энергодоступность по УС"
        // **********************************************************************
        public static void f_Map_Redraw_CommPowerAvail1()
        {

          // -------------------------------------------------------------------
            if (GlobalVarLn.flCoordSP_comm == 1) // СП выбрана
            {
                // SP
                ClassMap.f_DrawSPXY(
                              GlobalVarLn.XCenter_comm,  // m на местности
                              GlobalVarLn.YCenter_comm,
                                  ""
                             );
            }

            if (GlobalVarLn.flCoordYS1_comm == 1) // YS1 выбран
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    ClassMap.f_Map_Pol_XY_stat(
                                  GlobalVarLn.XPoint1_comm,  // m
                                  GlobalVarLn.YPoint1_comm,
                                  2,
                                  "УС1"
                                  //Azb
                                  //"RQ1"
                                 );
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    ClassMap.f_Map_Pol_XY_stat(
                                  GlobalVarLn.XPoint1_comm,  // m
                                  GlobalVarLn.YPoint1_comm,
                                  2,
                                  //Azb
                                  "RQ1"
                                 );
                }
                else 
                {
                    ClassMap.f_Map_Pol_XY_stat(
                                  GlobalVarLn.XPoint1_comm,  // m
                                  GlobalVarLn.YPoint1_comm,
                                  2,
                                  "CC1"
                                 );
                }

            } // YS1 выбран

            if (GlobalVarLn.flCoordYS2_comm == 1) // YS2 выбран
            {
                if (GlobalVarLn.fl_Azb == 0)
                {
                    ClassMap.f_Map_Pol_XY_stat(
                                  GlobalVarLn.XPoint2_comm,  // m
                                  GlobalVarLn.YPoint2_comm,
                                  2,
                                  "УС2"
                                  //Azb
                                  //"RQ2"
                                 );
                }
                else if (GlobalVarLn.fl_Azb == 1)
                {
                    ClassMap.f_Map_Pol_XY_stat(
                                  GlobalVarLn.XPoint2_comm,  // m
                                  GlobalVarLn.YPoint2_comm,
                                  2,
                                  //Azb
                                  "RQ2"
                                 );
                }
                else 
                {
                    ClassMap.f_Map_Pol_XY_stat(
                                  GlobalVarLn.XPoint2_comm,  // m
                                  GlobalVarLn.YPoint2_comm,
                                  2,
                                  "CC2"
                                 );
                }

            } // YS2 выбран
            // -----------------------------------------------------------------

            if (GlobalVarLn.fl_ZSupYS==1)
            {
            // .................................................................
            // OneYS

            if (GlobalVarLn.Numb_CommPowerAvail == 1)
            {
                ClassMap.f_Sup_Circle(
                              GlobalVarLn.XPoint1_comm,  // m
                              GlobalVarLn.YPoint1_comm,
                              GlobalVarLn.R_CommPA
                                );

               if(GlobalVarLn.fl_NoSup1==1)
               {
                   if (GlobalVarLn.fl_Azb == 0)
                   {
                       ClassMap.f_Sup_S(
                                     GlobalVarLn.XPoint1_comm,  // m
                                     GlobalVarLn.YPoint1_comm,
                                     2,
                                     "УС1 не подавляется"
                                      //Azb
                                      //"УС1 не подавляется"

                                       );
                   }
                   else if (GlobalVarLn.fl_Azb == 1)
                        {
                       ClassMap.f_Sup_S(
                                     GlobalVarLn.XPoint1_comm,  // m
                                     GlobalVarLn.YPoint1_comm,
                                     2,
                                     //Azb
                                      "RQ1 susdurulmur"
                                       );

                        }
                   else 
                        {
                            ClassMap.f_Sup_S(
                                          GlobalVarLn.XPoint1_comm,  // m
                                          GlobalVarLn.YPoint1_comm,
                                          2,
                                           "CC1 is not jammed"
                                            );

                        }


                    }
                    else
                   {
                   if (GlobalVarLn.fl_Azb == 0)
                   {
                       ClassMap.f_Sup_S(
                                     GlobalVarLn.XPoint1_comm,  // m
                                     GlobalVarLn.YPoint1_comm,
                                     1,
                                     "УС1 подавляется"
                                     //Azb???
                                      //"УС1 подавляется"
                                       );
                   }
                   else if (GlobalVarLn.fl_Azb == 1)
                    {
                       ClassMap.f_Sup_S(
                                     GlobalVarLn.XPoint1_comm,  // m
                                     GlobalVarLn.YPoint1_comm,
                                     1,
                                      //Azb???
                                      "RQ1 susdurulur"

                                       );
                    }
                   else 
                    {
                            ClassMap.f_Sup_S(
                                          GlobalVarLn.XPoint1_comm,  // m
                                          GlobalVarLn.YPoint1_comm,
                                          1,
                                           //Azb???
                                           "СС1 is jammed"
                                            );
                    }

                    }


                } // GlobalVarLn.Numb_CommPowerAvail == 1
            // .................................................................
            // Two YS

            else
            {
                ClassMap.f_Sup_Line(
                              GlobalVarLn.XPoint1_comm,  // m
                              GlobalVarLn.YPoint1_comm,
                              GlobalVarLn.XPoint2_comm,  // m
                              GlobalVarLn.YPoint2_comm
                                );

               if(GlobalVarLn.fl_NoSup1==1)
               {
                   if (GlobalVarLn.fl_Azb == 0)
                   {
                       ClassMap.f_Sup_S(
                                     GlobalVarLn.XPoint1_comm,  // m
                                     GlobalVarLn.YPoint1_comm,
                                     2,
                                     "УС1 не подавляется"
                                     //Azb???
                                      //"УС1 не подавляется"
                                       );
                   }
                   else if (GlobalVarLn.fl_Azb == 1)
                        {
                       ClassMap.f_Sup_S(
                                     GlobalVarLn.XPoint1_comm,  // m
                                     GlobalVarLn.YPoint1_comm,
                                     2,
                                     //Azb???
                                     "RQ1 susdurulmur"
                                       );
                   }
                   else 
                   {
                            ClassMap.f_Sup_S(
                                          GlobalVarLn.XPoint1_comm,  // m
                                          GlobalVarLn.YPoint1_comm,
                                          2,
                                          //Azb???
                                          "СС1 is not jammed"
                                            );
                   }

                   }
                    else
                  {
                   if (GlobalVarLn.fl_Azb == 0)
                   {
                       ClassMap.f_Sup_S(
                                     GlobalVarLn.XPoint1_comm,  // m
                                     GlobalVarLn.YPoint1_comm,
                                     1,
                                     "УС1 подавляется"
                                     //Azb???
                                     //"УС1 подавляется"
                                       );
                   }
                   else if (GlobalVarLn.fl_Azb == 1)
                   {
                       ClassMap.f_Sup_S(
                                     GlobalVarLn.XPoint1_comm,  // m
                                     GlobalVarLn.YPoint1_comm,
                                     1,
                                     //Azb???
                                      "RQ1 susdurulur"
                                       );
                   }
                   else 
                   {
                            ClassMap.f_Sup_S(
                                          GlobalVarLn.XPoint1_comm,  // m
                                          GlobalVarLn.YPoint1_comm,
                                          1,
                                           //Azb???
                                           "СС1 is jammed"
                                            );
                   }

                   }


                   if (GlobalVarLn.fl_NoSup2==1)
                   {
                   if (GlobalVarLn.fl_Azb == 0)
                   {
                       ClassMap.f_Sup_S(
                                     GlobalVarLn.XPoint2_comm,  // m
                                     GlobalVarLn.YPoint2_comm,
                                     2,
                                     "УС2 не подавляется"
                                     //Azb???
                                      //"УС2 не подавляется"
                                       );
                   }
                   else if (GlobalVarLn.fl_Azb == 1)
                   {
                       ClassMap.f_Sup_S(
                                     GlobalVarLn.XPoint2_comm,  // m
                                     GlobalVarLn.YPoint2_comm,
                                     2,
                                     //Azb???
                                     "RQ2 susdurulmur"
                                       );
                   }
                   else 
                   {
                            ClassMap.f_Sup_S(
                                          GlobalVarLn.XPoint2_comm,  // m
                                          GlobalVarLn.YPoint2_comm,
                                          2,
                                          //Azb???
                                          "CC2 is not jammed"
                                            );

                    }

                   }

                    else
                   {
                   if (GlobalVarLn.fl_Azb == 0)
                   {
                       ClassMap.f_Sup_S(
                                     GlobalVarLn.XPoint2_comm,  // m
                                     GlobalVarLn.YPoint2_comm,
                                     1,
                                     "УС2 подавляется"
                                     //Azb???
                                      //"УС2 подавляется"
                                       );
                   }
                   else if (GlobalVarLn.fl_Azb == 1)
                   {
                       ClassMap.f_Sup_S(
                                     GlobalVarLn.XPoint2_comm,  // m
                                     GlobalVarLn.YPoint2_comm,
                                     1,
                                     //Azb???
                                     "RQ2 susdurulur"
                                       );
                   }
                   else 
                    {
                            ClassMap.f_Sup_S(
                                          GlobalVarLn.XPoint2_comm,  // m
                                          GlobalVarLn.YPoint2_comm,
                                          1,
                                          //Azb???
                                          "CC2 is jammed"
                                            );
                    }

                    }

                } // Two YS
            // .................................................................

             } // GlobalVarLn.fl_ZSupYS==1
            // -------------------------------------------------------------------

 
        }  // P/P f_Map_Redraw_CommPowerAvail1
        // ******************************************************************************************

        // ******************************************************************************************
        // Нарисовать метку по координатам (красный прямоугольник)
        //
        // Входные параметры:
        // Lat - широта, град (GEO)
        // Long - долгота, град (GEO)
        // ******************************************************************************************
        public static void f_Map_Rect_LatLong_stat(
                                         double Lat,
                                         double Long
                                        )
        {

            double LatDX, LongDY;

            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed = new Pen(Color.Red, 2);
            Brush brushRed = new SolidBrush(Color.Red);

            // -------------------------------------------------------------------------------------

            // grad->rad
            LatDX = (Lat * Math.PI) / 180;
            LongDY = (Long * Math.PI) / 180;

            // Подаем rad, получаем там же расстояние на карте в м
            mapGeoToPlane(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {
                graph.FillRectangle(brushRed, (int)LatDX - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)LongDY -
                                    GlobalVarLn.axMapScreenGlobal.MapTop, 7, 7);

                // Надпись
                //TitleObject(typeStation, lat, lon);
            }

            // -------------------------------------------------------------------------------------

            //objMapForm1.graph.Dispose();


        } // P/P f_Map_Rect_LatLong_stat
        // *************************************************************************************

        // *************************************************************************************
        // Нарисовать метку по координатам (красный треугольник)
        //
        // Входные параметры:
        // Lat - широта, град (GEO)
        // Long - долгота, град (GEO)
        // ************************************************************************************
        public static void f_Map_Pol_LatLong_stat(
                                         double Lat,
                                         double Long
                                        )
        {
            double LatDX, LongDY;

            // --------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed1 = new Pen(Color.Red, 2);
            Brush brushRed1 = new SolidBrush(Color.Red);

            // --------------------------------------------------------------------------------
            // grad->rad
            LatDX = (Lat * Math.PI) / 180;
            LongDY = (Long * Math.PI) / 180;

            // Подаем rad, получаем там же расстояние на карте в м
            mapGeoToPlane(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
            // ---------------------------------------------------------------------------------

            if (graph != null)
            {
                Point[] toDraw = new Point[3];
                toDraw[0].X = (int)LatDX - (GlobalVarLn.axMapScreenGlobal.MapLeft + 7);
                toDraw[0].Y = (int)LongDY - (GlobalVarLn.axMapScreenGlobal.MapTop - 7);
                toDraw[1].X = (int)LatDX - GlobalVarLn.axMapScreenGlobal.MapLeft;
                toDraw[1].Y = (int)LongDY - (GlobalVarLn.axMapScreenGlobal.MapTop + 7);
                toDraw[2].X = (int)LatDX - (GlobalVarLn.axMapScreenGlobal.MapLeft - 7);
                toDraw[2].Y = (int)LongDY - (GlobalVarLn.axMapScreenGlobal.MapTop - 7);
                graph.FillPolygon(brushRed1, toDraw);

            }

            // -------------------------------------------------------------------
        } // P/P f_Map_Pol_LatLong_stat

        // ***********************************************************************

        // ***********************************************************************
        // Нарисовать метку по координатам (красный кружок для линии)
        //
        // Входные параметры:
        // Lat - широта, град (GEO)
        // Long - долгота, град (GEO)
        // ***********************************************************************
        public static void f_Map_El_LatLong_stat(
                                              double Lat,
                                              double Long
                                                )
        {

            double LatDX, LongDY;
            // -------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed1 = new Pen(Color.Red, 2);
            Brush brushRed1 = new SolidBrush(Color.Red);
            // -------------------------------------------------------------------

            // grad->rad
            LatDX = (Lat * Math.PI) / 180;
            LongDY = (Long * Math.PI) / 180;

            // Подаем градусы, получаем там же расстояние на карте в км
            mapGeoToPlane(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

            // Расстояние в км на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
            // -----------------------------------------------------------------
            if (graph != null)
            {
                graph.FillEllipse(brushRed1, (int)LatDX - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)LongDY -
                                  GlobalVarLn.axMapScreenGlobal.MapTop, 5, 5);
            }
            // -------------------------------------------------------------------

        } // P/P f_Map_El_LatLong_stat
        // ***********************************************************************

        // ***********************************************************************
        // Перерисовка пеленга FRCH
        // ***********************************************************************
        public static void f_ReDrawPeleng()
        {

           // 26_09_2018 *********************************************************
            GlobalVarLn.X1_PelMain = (int)GlobalVarLn.list1_Sost[0].X_m;  // SP1
            GlobalVarLn.Y1_PelMain = (int)GlobalVarLn.list1_Sost[0].Y_m;
            GlobalVarLn.X1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].X_m; // SP2
            GlobalVarLn.Y1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].Y_m;

            double a1 = 0;
            double b1 = 0;
            double c1 = 0;
            double d1 = 0;
            double f1 = 0;
            double g1 = 0;
            double a2 = 0;
            double b2 = 0;
            double c2 = 0;
            double d2 = 0;
            double f2 = 0;
            double g2 = 0;
            double x184 = 0;
            double y184 = 0;
            double x284 = 0;
            double y284 = 0;

            double[] arr_Pel_XYZ1 = new double[GlobalVarLn.numberofdots * 3];
            double[] arr_Pel_XYZ2 = new double[GlobalVarLn.numberofdots * 3];
            ClassMap classMap = new ClassMap();
            ClassMap classMap1 = new ClassMap();

            x184 = GlobalVarLn.X1_PelMain;
            y184 = GlobalVarLn.Y1_PelMain;
            x284 = GlobalVarLn.X1_1_PelMain;
            y284 = GlobalVarLn.Y1_1_PelMain;

            mapPlaneToGeo(GlobalVarLn.hmapl, ref x184, ref y184);
            mapPlaneToGeo(GlobalVarLn.hmapl, ref x284, ref y284);

            // rad->grad
            x184 = (x184 * 180) / Math.PI;
            y184 = (y184 * 180) / Math.PI;
            x284 = (x284 * 180) / Math.PI;
            y284 = (y284 * 180) / Math.PI;

            // ...............................................................................
            // Чтобы избежать неправильной отрисовки пеленга при изменении положения СП ->
            // еще раз считаем пеленг от нового положения СП

            // 28_09_2018
            // Peleng1
            //if ((GlobalVarLn.fl_PelIRI_1 == 1) && (GlobalVarLn.PrevP1!=-1))
            if (GlobalVarLn.fl_PelIRI_1 == 1)
            {
                classMap.f_Peleng(GlobalVarLn.PELENGG_1, GlobalVarLn.distance, GlobalVarLn.numberofdots, x184, y184,
                                  ref a1, ref b1, ref c1, ref d1, ref f1, ref g1,
                                  ref GlobalVarLn.arr_Pel1, ref arr_Pel_XYZ1);
            }

            //if ((GlobalVarLn.flPelMain2 == 1) && (GlobalVarLn.PrevP2!=-1))
            if (GlobalVarLn.flPelMain2 == 1)
            {
                classMap1.f_Peleng(GlobalVarLn.PELENGG_2, GlobalVarLn.distance, GlobalVarLn.numberofdots, x284, y284,
                                   ref a2, ref b2, ref c2, ref d2, ref f2, ref g2,
                                   ref GlobalVarLn.arr_Pel2, ref arr_Pel_XYZ2);
            } // Peleng2
            // ...............................................................................

            // ********************************************************************* 26_09_2018

            // ...............................................................................
            double x1s=0;
            double x2s = 0;
            double y1s = 0;
            double y2s = 0;

            x1s = GlobalVarLn.X1_PelMain;
            x2s = GlobalVarLn.X1_1_PelMain;
            y1s = GlobalVarLn.Y1_PelMain;
            y2s = GlobalVarLn.Y1_1_PelMain;

            mapPlaneToPicture(GlobalVarLn.hmapl, ref x1s, ref y1s);
            mapPlaneToPicture(GlobalVarLn.hmapl, ref x2s, ref y2s);

            double[] arr_Pel1_1 = new double[GlobalVarLn.numberofdots*3+1];     // R,Широта,долгота
            double[] arr_Pel2_1 = new double[GlobalVarLn.numberofdots*3+1];     // R,Широта,долгота
            // ...............................................................................

            if (GlobalVarLn.fl_PelIRI_1 == 1)
            {
                Array.Copy(GlobalVarLn.arr_Pel1, arr_Pel1_1, GlobalVarLn.numberofdots * 3);
            }

            if (GlobalVarLn.flPelMain2 == 1)
            {
                Array.Copy(GlobalVarLn.arr_Pel2, arr_Pel2_1, GlobalVarLn.numberofdots * 3);
            }
            // ...............................................................................

            // FOR1
            int ind2 = 0;
            for (int i2 = 0; i2 < GlobalVarLn.numberofdots; i2++)
            {
                // 14_09_2018
                if (GlobalVarLn.fl_PelIRI_1 == 1)
                {
                    // grad->rad
                    arr_Pel1_1[ind2 + 1] = (arr_Pel1_1[ind2 + 1] * Math.PI) / 180;
                    arr_Pel1_1[ind2 + 2] = (arr_Pel1_1[ind2 + 2] * Math.PI) / 180;
                }

                if (GlobalVarLn.flPelMain2 == 1)
                {
                    arr_Pel2_1[ind2 + 1] = (arr_Pel2_1[ind2 + 1] * Math.PI) / 180;
                    arr_Pel2_1[ind2 + 2] = (arr_Pel2_1[ind2 + 2] * Math.PI) / 180;
                }

                ind2 += 3;

            } // FOR1
            // ...............................................................................

            // FOR2
            int ind = 0;
            for (int i = 0; i < GlobalVarLn.numberofdots; i++)
            {
                if (GlobalVarLn.fl_PelIRI_1 == 1)
                {
                    mapGeoToPlane(GlobalVarLn.hmapl, ref arr_Pel1_1[ind + 1], ref arr_Pel1_1[ind + 2]);
                    mapPlaneToPicture(GlobalVarLn.hmapl, ref arr_Pel1_1[ind + 1], ref arr_Pel1_1[ind + 2]);
                }

                if (GlobalVarLn.flPelMain2 == 1)
                {
                    mapGeoToPlane(GlobalVarLn.hmapl, ref arr_Pel2_1[ind + 1], ref arr_Pel2_1[ind + 2]);
                    mapPlaneToPicture(GlobalVarLn.hmapl, ref arr_Pel2_1[ind + 1], ref arr_Pel2_1[ind + 2]);
                }

                ind += 3;
            } // FOR2
            // ...............................................................................

            Point pnt1_1 = new Point();
            Point pnt2_1 = new Point();
            Point pnt1_2 = new Point();
            Point pnt2_2 = new Point();
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
            Pen pen1 = new Pen(Color.Red, 2.0f);

            // FOR3
            int ind3 = 0;
            for (int i3 = 0; i3 < GlobalVarLn.numberofdots; i3++)
            {
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // 1-я точка

                if (i3 == 0)
                {
                    if (GlobalVarLn.fl_PelIRI_1 == 1)
                    {

                        pnt1_1.X = (int)x1s - GlobalVarLn.axMapScreenGlobal.MapLeft;
                        pnt1_1.Y = (int)y1s - GlobalVarLn.axMapScreenGlobal.MapTop;
                        pnt2_1.X = (int)arr_Pel1_1[ind3 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft;
                        pnt2_1.Y = (int)arr_Pel1_1[ind3 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop;
                        graph.DrawLine(pen1, pnt1_1, pnt2_1);

                        pnt1_1.X = (int)arr_Pel1_1[ind3 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft;
                        pnt1_1.Y = (int)arr_Pel1_1[ind3 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop;
                    }

                    if (GlobalVarLn.flPelMain2 == 1)
                    {
                        pnt1_2.X = (int)x2s - GlobalVarLn.axMapScreenGlobal.MapLeft;
                        pnt1_2.Y = (int)y2s - GlobalVarLn.axMapScreenGlobal.MapTop;
                        pnt2_2.X = (int)arr_Pel2_1[ind3 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft;
                        pnt2_2.Y = (int)arr_Pel2_1[ind3 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop;
                        graph.DrawLine(pen1, pnt1_2, pnt2_2);

                        pnt1_2.X = (int)arr_Pel2_1[ind3 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft;
                        pnt1_2.Y = (int)arr_Pel2_1[ind3 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop;
                    }

                }
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                else
                {
                    // 14_09_2018
                    if (GlobalVarLn.fl_PelIRI_1 == 1)
                    {

                        pnt2_1.X = (int)arr_Pel1_1[ind3 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft;
                        pnt2_1.Y = (int)arr_Pel1_1[ind3 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop;
                    }

                    if (GlobalVarLn.flPelMain2 == 1)
                    {
                        pnt2_2.X = (int)arr_Pel2_1[ind3 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft;
                        pnt2_2.Y = (int)arr_Pel2_1[ind3 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop;
                    }

                    // 14_09_2018
                    if (GlobalVarLn.fl_PelIRI_1 == 1)
                    {
                        graph.DrawLine(pen1, pnt1_1, pnt2_1);
                    }

                    if (GlobalVarLn.flPelMain2 == 1)
                    {
                        graph.DrawLine(pen1, pnt1_2, pnt2_2);
                    }

                    // 14_09_2018
                    if (GlobalVarLn.fl_PelIRI_1 == 1)
                    {
                        pnt1_1.X = pnt2_1.X;
                        pnt1_1.Y = pnt2_1.Y;
                    }

                    if (GlobalVarLn.flPelMain2 == 1)
                    {
                        pnt1_2.X = pnt2_2.X;
                        pnt1_2.Y = pnt2_2.Y;
                    }

                } // ELSE
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                ind3 += 3;

            } // FOR3
            // ...............................................................................

        } // Функция f_ReDrawPeleng
        // ***********************************************************************************


        // ***********************************************************************
        // Перерисовка пеленга PPRCH
        // ***********************************************************************
        public static void f_ReDrawPeleng_2()
        {

          // 26_09_2018 **********************************************************

            double a1 = 0;
            double b1 = 0;
            double c1 = 0;
            double d1 = 0;
            double f1 = 0;
            double g1 = 0;
            double a2 = 0;
            double b2 = 0;
            double c2 = 0;
            double d2 = 0;
            double f2 = 0;
            double g2 = 0;
            double x184 = 0;
            double y184 = 0;
            double x284 = 0;
            double y284 = 0;

            double[] arr_Pel_XYZ1 = new double[GlobalVarLn.numberofdots * 3];
            double[] arr_Pel_XYZ2 = new double[GlobalVarLn.numberofdots * 3];


            GlobalVarLn.X1_PelMain = (int)GlobalVarLn.list1_Sost[0].X_m;  // SP1
            GlobalVarLn.Y1_PelMain = (int)GlobalVarLn.list1_Sost[0].Y_m;
            GlobalVarLn.X1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].X_m; // SP2
            GlobalVarLn.Y1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].Y_m;

            x184 = GlobalVarLn.X1_PelMain;
            y184 = GlobalVarLn.Y1_PelMain;
            x284 = GlobalVarLn.X1_1_PelMain;
            y284 = GlobalVarLn.Y1_1_PelMain;

            mapPlaneToGeo(GlobalVarLn.hmapl, ref x184, ref y184);
            mapPlaneToGeo(GlobalVarLn.hmapl, ref x284, ref y284);

            // rad->grad
            x184 = (x184 * 180) / Math.PI;
            y184 = (y184 * 180) / Math.PI;
            x284 = (x284 * 180) / Math.PI;
            y284 = (y284 * 180) / Math.PI;

            ClassMap classMap = new ClassMap();
            ClassMap classMap1 = new ClassMap();

            // ********************************************************** 26_09_2018
            
            // .....................................................................
            int countPel=0;
            double x1s = 0;
            double x2s = 0;
            double y1s = 0;
            double y2s = 0;

            x1s = GlobalVarLn.X1_PelMain;
            x2s = GlobalVarLn.X1_1_PelMain;
            y1s = GlobalVarLn.Y1_PelMain;
            y2s = GlobalVarLn.Y1_1_PelMain;

            // 14_09_2018
            int ff1 = 0;
            int ff2 = 0;
            if ((x1s != 0) && (y1s != 0))
                ff1 = 1;
            if ((x2s != 0) && (y2s != 0))
                ff2 = 1;


            mapPlaneToPicture(GlobalVarLn.hmapl, ref x1s, ref y1s);
            mapPlaneToPicture(GlobalVarLn.hmapl, ref x2s, ref y2s);
            // .....................................................................

           // List -----------------------------------------------------------------
           //FOR***

            for (countPel = 0; countPel < GlobalVarLn.list_PelIRI.Count; countPel++)
            {
                double[] arr_Pel1_1 = new double[GlobalVarLn.numberofdots * 3 + 1];     // R,Широта,долгота
                double[] arr_Pel2_1 = new double[GlobalVarLn.numberofdots * 3 + 1];     // R,Широта,долгота

             // 14_09_2018
             if (
                 (GlobalVarLn.list_PelIRI[countPel].Pel1 != -1)||
                 (GlobalVarLn.list_PelIRI[countPel].Pel2 != -1)
                 )
             {

              // 26_09_2018 *********************************************************

                    // !!!БЫЛО
                    // 14_09_2018
                    //if(ff1==1)
                    //  Array.Copy(GlobalVarLn.list_PelIRI[countPel].arr_Pel1, arr_Pel1_1, GlobalVarLn.numberofdots * 3);
                    // 14_09_2018
                    //if ((GlobalVarLn.list_PelIRI[countPel].flPelMain2 == 1)&&(ff2==1))
                    //{
                    //    Array.Copy(GlobalVarLn.list_PelIRI[countPel].arr_Pel2, arr_Pel2_1, GlobalVarLn.numberofdots * 3);
                    //}

                    if ((ff1 == 1) && (GlobalVarLn.list_PelIRI[countPel].Pel1 != -1))
                    {

                        PelIRI[] mass1 = GlobalVarLn.list_PelIRI.ToArray();
                        // Peleng1
                        classMap.f_Peleng(GlobalVarLn.list_PelIRI[countPel].Pel1, GlobalVarLn.distance, GlobalVarLn.numberofdots, x184, y184,
                                          ref a1, ref b1, ref c1, ref d1, ref f1, ref g1,
                                          ref mass1[countPel].arr_Pel1, ref arr_Pel_XYZ1);
                        GlobalVarLn.list_PelIRI = mass1.ToList();

                        Array.Copy(GlobalVarLn.list_PelIRI[countPel].arr_Pel1, arr_Pel1_1, GlobalVarLn.numberofdots * 3);
                    }

                    if ((GlobalVarLn.list_PelIRI[countPel].flPelMain2 == 1)&&(ff2==1))
                    {
                        PelIRI[] mass2 = GlobalVarLn.list_PelIRI.ToArray();
                        classMap1.f_Peleng(GlobalVarLn.list_PelIRI[countPel].Pel2, GlobalVarLn.distance, GlobalVarLn.numberofdots, x284, y284,
                                           ref a2, ref b2, ref c2, ref d2, ref f2, ref g2,
                                           ref mass2[countPel].arr_Pel2, ref arr_Pel_XYZ2);
                        GlobalVarLn.list_PelIRI = mass2.ToList();

                        Array.Copy(GlobalVarLn.list_PelIRI[countPel].arr_Pel2, arr_Pel2_1, GlobalVarLn.numberofdots * 3);
                    }

                    // ********************************************************* 26_09_2018

                    // .....................................................................
                    // FOR1
                    int ind2 = 0;
                    for (int i2 = 0; i2 < GlobalVarLn.numberofdots; i2++)
                    {
                        // 28_09_2018
                        //if (ff1 == 1)
                        if ((ff1 == 1) && (GlobalVarLn.list_PelIRI[countPel].Pel1 != -1))
                        {
                            // grad->rad
                            arr_Pel1_1[ind2 + 1] = (arr_Pel1_1[ind2 + 1] * Math.PI) / 180;
                            arr_Pel1_1[ind2 + 2] = (arr_Pel1_1[ind2 + 2] * Math.PI) / 180;
                        }

                        if ((GlobalVarLn.list_PelIRI[countPel].flPelMain2 == 1)&&(ff2==1))
                        {
                            arr_Pel2_1[ind2 + 1] = (arr_Pel2_1[ind2 + 1] * Math.PI) / 180;
                            arr_Pel2_1[ind2 + 2] = (arr_Pel2_1[ind2 + 2] * Math.PI) / 180;
                        }

                        ind2 += 3;
                    } // FOR1
                    // .....................................................................
                    // FOR2
                    int ind = 0;
                    for (int i = 0; i < GlobalVarLn.numberofdots; i++)
                    {
                        // 28_09_2018
                        //if (ff1 == 1)
                        if ((ff1 == 1) && (GlobalVarLn.list_PelIRI[countPel].Pel1 != -1))
                        {
                            mapGeoToPlane(GlobalVarLn.hmapl, ref arr_Pel1_1[ind + 1], ref arr_Pel1_1[ind + 2]);
                            mapPlaneToPicture(GlobalVarLn.hmapl, ref arr_Pel1_1[ind + 1], ref arr_Pel1_1[ind + 2]);
                        }

                        if ((GlobalVarLn.list_PelIRI[countPel].flPelMain2 == 1)&&(ff2==1))
                        {
                            mapGeoToPlane(GlobalVarLn.hmapl, ref arr_Pel2_1[ind + 1], ref arr_Pel2_1[ind + 2]);
                            mapPlaneToPicture(GlobalVarLn.hmapl, ref arr_Pel2_1[ind + 1], ref arr_Pel2_1[ind + 2]);
                        }

                        ind += 3;
                    } // FOR2
                    // .....................................................................

                    Point pnt1_1 = new Point();
                    Point pnt2_1 = new Point();
                    Point pnt1_2 = new Point();
                    Point pnt2_2 = new Point();
                    Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
                    Pen pen1 = new Pen(Color.Blue, 2.0f);

                    // FOR3
                    int ind3 = 0;
                    for (int i3 = 0; i3 < GlobalVarLn.numberofdots; i3++)
                    {
                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // 1-я точка

                        if (i3 == 0)
                        {
                            // 28_09_2018
                            //if (ff1 == 1)
                            if ((ff1 == 1) && (GlobalVarLn.list_PelIRI[countPel].Pel1 != -1))
                            {
                                pnt1_1.X = (int)x1s - GlobalVarLn.axMapScreenGlobal.MapLeft;
                                pnt1_1.Y = (int)y1s - GlobalVarLn.axMapScreenGlobal.MapTop;
                                pnt2_1.X = (int)arr_Pel1_1[ind3 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft;
                                pnt2_1.Y = (int)arr_Pel1_1[ind3 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop;
                                graph.DrawLine(pen1, pnt1_1, pnt2_1);

                                pnt1_1.X = (int)arr_Pel1_1[ind3 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft;
                                pnt1_1.Y = (int)arr_Pel1_1[ind3 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop;
                            }

                            if ((GlobalVarLn.list_PelIRI[countPel].flPelMain2 == 1)&&(ff2==1))
                            {
                                pnt1_2.X = (int)x2s - GlobalVarLn.axMapScreenGlobal.MapLeft;
                                pnt1_2.Y = (int)y2s - GlobalVarLn.axMapScreenGlobal.MapTop;
                                pnt2_2.X = (int)arr_Pel2_1[ind3 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft;
                                pnt2_2.Y = (int)arr_Pel2_1[ind3 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop;
                                graph.DrawLine(pen1, pnt1_2, pnt2_2);

                                pnt1_2.X = (int)arr_Pel2_1[ind3 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft;
                                pnt1_2.Y = (int)arr_Pel2_1[ind3 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop;
                            }

                        } // i3=0
                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        else
                        {
                            // 28_09_2018
                            //if (ff1 == 1)
                            if ((ff1 == 1) && (GlobalVarLn.list_PelIRI[countPel].Pel1 != -1))
                            {
                                pnt2_1.X = (int)arr_Pel1_1[ind3 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft;
                                pnt2_1.Y = (int)arr_Pel1_1[ind3 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop;
                            }

                            if ((GlobalVarLn.list_PelIRI[countPel].flPelMain2 == 1)&&(ff2==1))
                            {
                                pnt2_2.X = (int)arr_Pel2_1[ind3 + 1] - GlobalVarLn.axMapScreenGlobal.MapLeft;
                                pnt2_2.Y = (int)arr_Pel2_1[ind3 + 2] - GlobalVarLn.axMapScreenGlobal.MapTop;
                            }

                            // 28_09_2018
                            //if (ff1 == 1)
                            if ((ff1 == 1) && (GlobalVarLn.list_PelIRI[countPel].Pel1 != -1))
                            {
                                graph.DrawLine(pen1, pnt1_1, pnt2_1);
                            }

                            if ((GlobalVarLn.list_PelIRI[countPel].flPelMain2 == 1)&&(ff2==1))
                            {
                                graph.DrawLine(pen1, pnt1_2, pnt2_2);
                            }

                            // 28_09_2018
                            //if (ff1 == 1)
                            if ((ff1 == 1) && (GlobalVarLn.list_PelIRI[countPel].Pel1 != -1))
                            {
                                pnt1_1.X = pnt2_1.X;
                                pnt1_1.Y = pnt2_1.Y;
                            }

                            if ((GlobalVarLn.list_PelIRI[countPel].flPelMain2 == 1)&&(ff2==1))
                            {
                                pnt1_2.X = pnt2_2.X;
                                pnt1_2.Y = pnt2_2.Y;
                            }

                        } // ELSE
                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        ind3 += 3;

                    } // FOR3
                 // .....................................................................


         }// IF(Pel1/Pel2!=-1)

         }// FOR***
            // ----------------------------------------------------------------- List

        } // Функция f_ReDrawPeleng_2
        // ***********************************************************************


        // **********************************************************************
        // Нарисовать метку по координатам (красный прямоугольник)
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // *********************************************************************
        public static void f_Map_Rect_XY_stat(

                                  double X,
                                  double Y
                                  )
        {
            // -----------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed2 = new Pen(Color.Red, 2);
            Brush brushRed2 = new SolidBrush(Color.Red);
            // -----------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -----------------------------------------------------------------

            if (graph != null)
            {
                graph.FillRectangle(brushRed2, (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)Y - 
                                    GlobalVarLn.axMapScreenGlobal.MapTop, 7, 7);
            }
            // -----------------------------------------------------------------

        } // P/P f_Map_Rect_XY_stat
        // *********************************************************************

        // *********************************************************************
        // Нарисовать линию по координатам
        //
        // Входные параметры:
        // X1,X2 - X, m на местности
        // Y1,Y2 - Y, m
        // *********************************************************************
        public static void f_Map_Line_XY_stat(
                                  double X1,
                                  double Y1,
                                  double X2,
                                  double Y2
                                             )
        {

            // -----------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
            Pen penRed2 = new Pen(Color.Red, 2);
            Brush brushRed2 = new SolidBrush(Color.Red);
            // -----------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X1, ref Y1);
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X2, ref Y2);
            // -----------------------------------------------------------------
            if (graph != null)
            {
                Point point1 = new Point((int)X1 - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)Y1 - 
                                          GlobalVarLn.axMapScreenGlobal.MapTop);
                Point point2 = new Point((int)X2 - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)Y2 - 
                                          GlobalVarLn.axMapScreenGlobal.MapTop);
                graph.DrawLine(penRed2, point1, point2);
            }
            // -----------------------------------------------------------------

        } // P/P f_Map_Line_XY_stat
        // *********************************************************************

        // ***********************************************************************
        // Перерисовка S
        // ***********************************************************************
        public static void f_ReDrawS_stat()
        {
            // ......................................................................
            if (GlobalVarLn.fl_S1_stat==1)
            {
            f_Map_Rect_XY_stat(
                          GlobalVarLn.X_Coordl1_stat,
                          GlobalVarLn.Y_Coordl1_stat
                         );
            }
            // ......................................................................


            // ......................................................................
            if (GlobalVarLn.fl_S2_stat == 1)
            {
                f_Map_Rect_XY_stat(
                              GlobalVarLn.X_Coordl2_stat,
                              GlobalVarLn.Y_Coordl2_stat
                             );
                f_Map_Line_XY_stat(
                              GlobalVarLn.X_Coordl1_stat,
                              GlobalVarLn.Y_Coordl1_stat,
                              GlobalVarLn.X_Coordl2_stat,
                              GlobalVarLn.Y_Coordl2_stat
                             );
            }
            // ......................................................................

        } // Функция f_ReDrawS_stat
        // ***********************************************************************

        // **********************************************************************
        // Нарисовать метку по координатам (красный прямоугольник) с надписью
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // *********************************************************************
        public static void f_Map_Rect_XYS_stat(

                                  double X,
                                  double Y,
                                  String s1
                                  )
        {
            // -----------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
            Brush brushRed2 = new SolidBrush(Color.Red);
            Font font2 = new Font(FontFamily.GenericSansSerif, 10.0F, FontStyle.Regular, GraphicsUnit.Pixel);
            Brush brushj = new SolidBrush(Color.Blue);
            Brush brushi = new SolidBrush(Color.White);

            // -----------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -----------------------------------------------------------------

            if (graph != null)
            {
                graph.FillRectangle(brushRed2, (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)Y -
                                    GlobalVarLn.axMapScreenGlobal.MapTop, 5, 5);

               Rectangle rec1 = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 5,
                                               (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 7,
                                                  15,
                                                  13
                                                 );

               if (s1 != "")
               {

                   graph.FillRectangle(brushi, rec1);

                   graph.DrawString(
                                    s1,
                                    font2,
                                    brushj,
                                    (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 5,
                                    (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 8
                                    );
               } // s!=0
            }
            // -----------------------------------------------------------------

        } // P/P f_Map_Rect_XYS_stat
        // *********************************************************************

        // ************************************************************************
        // функция рисования ЗПВ
        // ************************************************************************

        public static void DrawLSR1()
        {
            int l = 360 / GlobalVarLn.iStepAngleInput_ZPV + 1;

            double AddPointX = 0;
            double AddPointY = 0;

            GlobalVarLn.listPointPictDSR = new Point[l];

            // в цикле пересчитать координаты на местности
            // в координаты на экране
            for (int i = 0; i < l; i++)
            {
                AddPointX = 0;
                AddPointY = 0;

                AddPointX = GlobalVarLn.listPointDSR[i].X;
                AddPointY = GlobalVarLn.listPointDSR[i].Y;

                // Расстояние в м на карте -> пикселы на изображении
                long j = 0;
                j = mapPlaneToPicture(GlobalVarLn.hmapl, ref AddPointX, ref AddPointY);

                if (j >= 0)
                {
                    GlobalVarLn.listPointPictDSR[i].X = (int)(AddPointX - GlobalVarLn.axMapScreenGlobal.MapLeft);
                    GlobalVarLn.listPointPictDSR[i].Y = (int)(AddPointY - GlobalVarLn.axMapScreenGlobal.MapTop);
                }
            }
            // ---------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
            //Pen pen1 = new Pen(Color.HotPink, 5.0f);
            Pen pen1 = new Pen(Color.Crimson, 2.0f);
            //HatchBrush brush1 = new HatchBrush(HatchStyle.Cross, Color.Linen, Color.Transparent);
            HatchBrush brush1 = new HatchBrush(HatchStyle.Cross, Color.Cornsilk, Color.Transparent);

            if (graph != null)
            {

                graph.DrawPolygon(pen1, GlobalVarLn.listPointPictDSR);
                graph.FillClosedCurve(brush1, GlobalVarLn.listPointPictDSR);
            }

        }
        // ************************************************************************

        // ************************************************************************
        // функция рисования ЗПВ
        // ************************************************************************

        public static void DrawLSR()
        {
            DrawPolygon(GlobalVarLn.listPointDSR, Color.Yellow);
        }
       // --------------------------------------------------------------------------
        public static void DrawLSR_Z1()
        {
            DrawPolygon(GlobalVarLn.listPointDSR1, Color.Yellow);
        }
        // --------------------------------------------------------------------------
        public static void DrawLSR_Z2()
        {
            DrawPolygon(GlobalVarLn.listPointDSR2, Color.LightBlue);
        }
        // --------------------------------------------------------------------------



        public static void DrawPolygon(IReadOnlyList<Point> points, Color color)
        {
            if (points.Count == 0)
            {
                return;
            }
            var listPointPictDSR = new Point[points.Count];

            // в цикле пересчитать координаты на местности
            // в координаты на экране
            for (var i = 0; i < points.Count; i++)
            {
                double addPointX = points[i].X;
                double addPointY = points[i].Y;

                // Расстояние в м на карте -> пикселы на изображении
                var j = mapPlaneToPicture(GlobalVarLn.hmapl, ref addPointX, ref addPointY);

                if (j >= 0)
                {
                    listPointPictDSR[i].X = (int)(addPointX - GlobalVarLn.axMapScreenGlobal.MapLeft);
                    listPointPictDSR[i].Y = (int)(addPointY - GlobalVarLn.axMapScreenGlobal.MapTop);
                }
            }
            // ---------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
            Pen pen1 = new Pen(color, 2.0f);
            HatchBrush brush1 = new HatchBrush(HatchStyle.Cross, Color.Cornsilk, Color.Transparent);

            graph.DrawPolygon(pen1, listPointPictDSR);
            graph.FillClosedCurve(brush1, listPointPictDSR);
        }

        // ************************************************************************

        // ***********************************************************************
        // SP по XY m
        // ***********************************************************************
        public static void f_DrawSPXY11(
                                         double X,
                                         double Y,
                                         Bitmap pbmp
                                         )
        {

            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            // -------------------------------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении

            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                // Куда поместим рисунок (!!! Изображение д.б. не меньше прямоугольника)
                Rectangle rec = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - GlobalVarLn.width_Sost / 2,
                                               (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop - GlobalVarLn.height_Sost,
                                               GlobalVarLn.width_Sost,
                                               GlobalVarLn.height_Sost
                                              );



                graph.DrawImage(pbmp, rec);

                // -------------------------------------------------------------------------------------

            } // graph!=0


        } // Функция f_DrawSPXY
        // ***********************************************************************

        // **********************************************************************
        // Перерисовка "Состав комплекса"
        // **********************************************************************
        // GPSSPPU

        public static void f_Map_Redraw_Sost()
        {

            if (GlobalVarLn.flCoordSP_Sost == 1) // SP1
            {
                // SP1
                ClassMap.f_DrawSPXY11(
                              GlobalVarLn.XCenter_Sost,  // m на местности
                              GlobalVarLn.YCenter_Sost,
                       (Bitmap)GlobalVarLn.objFormSostG.imageList1.Images[GlobalVarLn.list1_Sost[0].indzn]
                             );
            }


            if (GlobalVarLn.flCoordYS1_Sost == 1) // SP2
            {
                // SP2
                ClassMap.f_DrawSPXY11(
                              GlobalVarLn.XPoint1_Sost,  // m на местности
                              GlobalVarLn.YPoint1_Sost,
                       (Bitmap)GlobalVarLn.objFormSostG.imageList2.Images[GlobalVarLn.list1_Sost[1].indzn]
                             );
            }

            if (GlobalVarLn.flCoordYS2_Sost == 1) // PU
            {
                // PU
                ClassMap.f_DrawSPXY11(
                              GlobalVarLn.XPoint2_Sost,  // m на местности
                              GlobalVarLn.YPoint2_Sost,
                       (Bitmap)GlobalVarLn.objFormSostG.imageList3.Images[GlobalVarLn.list1_Sost[2].indzn]
                             );
            }

        } // P/P f_Map_Redraw_Sost
        // **********************************************************************

        // *********************************************************************
        // Draw ZoneBearing
        // *********************************************************************
        public static void f_Map_Bearing(
                                  double X1,
                                  double Y1,
                                  double width,
                                  double height,
                                  double R,
                                  double Rmin,
                                  int intens,
                                  double gamma
                                             )
        {
            // -----------------------------------------------------------------
            int prozr1 = 0;
            int clr1 = 0;
            int fl1 = 0;
            int fl2 = 0;
            int fl3 = 0;

            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;
            //double wd1=0;
            //double ht1=0;

            int mmm=0;

            x1 = X1;
            y1 = Y1;
            // -----------------------------------------------------------------
            x2 = x1 + width/2;
            y2 = y1 - height/2;

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref x1, ref y1);
            mapPlaneToPicture(GlobalVarLn.hmapl, ref x2, ref y2);

            //wd1=2*Math.Abs(x1-x2);
            //ht1=2*Math.Abs(y1-y2);
            // -----------------------------------------------------------------

            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            // -----------------------------------------------------------------
            // Razmer rectangle

             if (GlobalVarLn.axMapScreenGlobal.ViewScale >= 500000)
                 mmm = 7;
             else if ((GlobalVarLn.axMapScreenGlobal.ViewScale < 500000) && (GlobalVarLn.axMapScreenGlobal.ViewScale >=250000))
                 mmm = 10;
             else if ((GlobalVarLn.axMapScreenGlobal.ViewScale < 250000) && (GlobalVarLn.axMapScreenGlobal.ViewScale >= 125000))
                 mmm=13;
             else if ((GlobalVarLn.axMapScreenGlobal.ViewScale < 125000) && (GlobalVarLn.axMapScreenGlobal.ViewScale >= 60000))
                 mmm=25;
             else if ((GlobalVarLn.axMapScreenGlobal.ViewScale < 60000) && (GlobalVarLn.axMapScreenGlobal.ViewScale >= 30000))
                 mmm=30;
             else if ((GlobalVarLn.axMapScreenGlobal.ViewScale < 30000) && (GlobalVarLn.axMapScreenGlobal.ViewScale >= 15000))
                 mmm=60;
             else
                 mmm = 100;
            // -----------------------------------------------------------------
             prozr1 = GlobalVarLn.prozr;
            // -----------------------------------------------------------------
             // Rmin...2Rmin
             if ((R >= Rmin) && (R < (2 * Rmin)))
             {
                 clr1 = GlobalVarLn.grn1;
                 fl1 = 1; // green

             } 

             // 2Rmin...3Rmin
             else if ((R >= (2 * Rmin)) && (R < (3 * Rmin)))
             {
                 clr1 = GlobalVarLn.grn2;
                 fl1 = 1; // green

             } 

             // 3Rmin...4Rmin
             else if ((R >= (3 * Rmin)) && (R < (4 * Rmin)))
             {
                 clr1 = GlobalVarLn.grn3;
                 fl1 = 1; // green

             } 

             // 4Rmin...5Rmin
             else if ((R >= (4 * Rmin)) && (R < (5 * Rmin)))
             {
                 clr1 = GlobalVarLn.grn4;
                 fl1 = 1; // green

             } 

             // 5Rmin...6Rmin
             else if ((R >= (5 * Rmin)) && (R < (6 * Rmin)))
             {
                 fl2 = 1; // red
                 clr1 = GlobalVarLn.rd1;

             } 

             // 6Rmin...7Rmin
             else if ((R >= (6 * Rmin)) && (R < (7 * Rmin)))
             {
                 fl2 = 1; // red
                 clr1 = GlobalVarLn.rd2;

             } 

             // 7Rmin...8Rmin
             else if ((R >= (7 * Rmin)) && (R < (8 * Rmin)))
             {
                 fl2 = 1; // red
                 clr1 = GlobalVarLn.rd3;

             } 

             // 8Rmin...9Rmin
             else if ((R >= (8 * Rmin)) && (R < (9 * Rmin)))
             {
                 fl3 = 1; // blue
                 //clr1 = GlobalVarLn.bl1;
             } 

             // 9Rmin...10Rmin
             else if ((R >= (9 * Rmin)) && (R < (10 * Rmin)))
             {
                 fl3 = 1; // blue
                 //clr1 = GlobalVarLn.bl2;

             }

             // 10Rmin...11Rmin
             else if ((R >= (10 * Rmin)) && (R < (11 * Rmin)))
             {
                 fl3 = 1; // blue
                 //clr1 = GlobalVarLn.bl2;

             } 
             // -----------------------------------------------------------------
             Brush brush1;

             if (fl1==1)  // green
             {
                 brush1 = new SolidBrush(Color.FromArgb(prozr1, 0,clr1, 0)); // ближняя

             }

             else if (fl2 == 1) // red
             {
                 brush1 = new SolidBrush(Color.FromArgb(prozr1, clr1, 0, 0)); // ближняя

             }

             //else if (fl3 == 1) // blue
             else
             {
                 brush1 = new SolidBrush(Color.FromArgb(prozr1, 0, 0, clr1)); // ближняя

             } 
             // -----------------------------------------------------------------

             if ((gamma >= 0.1) && (gamma <= 3.1)&&((fl1==1)||(fl2==1)||(fl3==1)))
             {
                 graph.FillRectangle(
                                     brush1,
                     // Верхний левый угол
                                     (int)x2 - GlobalVarLn.axMapScreenGlobal.MapLeft,
                                     (int)y2 - GlobalVarLn.axMapScreenGlobal.MapTop,
                                     mmm,
                                     mmm
                                     );
             }
             // -----------------------------------------------------------------



            //Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
            // -----------------------------------------------------------------


/*
            // -----------------------------------------------------------------
            // Rmin...2Rmin

            if ((R >= Rmin) && (R < (2 * Rmin)))
            {

                Brush brush1 = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr, 0, GlobalVarLn.grn1, 0)); // ближняя

            if ((gamma >= 0.1) && (gamma <= 3.1))
            {
                graph.FillRectangle(
                                    brush1,
                                    // Верхний левый угол
                                    (int)x2 - GlobalVarLn.axMapScreenGlobal.MapLeft,
                                    (int)y2 - GlobalVarLn.axMapScreenGlobal.MapTop,
                                    mmm,
                                    mmm
                                   //7,
                                   //7
                                    //(int)wd1,
                                    //(int)ht1
                                    );
            }

            } // Rmin...2Rmin
            // -----------------------------------------------------------------
            // 2Rmin...3Rmin

            else if ((R >= (2*Rmin)) && (R < (3 * Rmin)))
            {
                Brush brush2 = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr, 0, GlobalVarLn.grn2, 0));

                if ((gamma >= 0.1) && (gamma <= 3.1))
                {
                    graph.FillRectangle(
                                        brush2,
                                        (int)x2 - GlobalVarLn.axMapScreenGlobal.MapLeft,
                                        (int)y2 - GlobalVarLn.axMapScreenGlobal.MapTop,
                                         mmm,
                                         mmm
                                        //7,
                                        //7
                                        //(int)wd1,
                                        //(int)ht1
                                        );
                }

            } // 2Rmin...3Rmin
            // -----------------------------------------------------------------
            // 3Rmin...4Rmin

            else if ((R >= (3 * Rmin)) && (R < (4 * Rmin)))
            {
                Brush brush3 = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr, 0, GlobalVarLn.grn3, 0));

                if ((gamma >= 0.1) && (gamma <= 3.1))
                {
                    graph.FillRectangle(
                                        brush3,
                                        (int)x2 - GlobalVarLn.axMapScreenGlobal.MapLeft,
                                        (int)y2 - GlobalVarLn.axMapScreenGlobal.MapTop,
                                        mmm,
                                        mmm
                                        //7,
                                        //7
                                        //(int)wd1,
                                        //(int)ht1
                                        );
                }

            } // 3Rmin...4Rmin
            // -----------------------------------------------------------------
            // 4Rmin...5Rmin

            else if ((R >= (4 * Rmin)) && (R < (5 * Rmin)))
            {
                Brush brush4 = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr, 0, GlobalVarLn.grn4, 0));

                if ((gamma >= 0.1) && (gamma <= 3.1))
                {
                    graph.FillRectangle(
                                        brush4,
                                        (int)x2 - GlobalVarLn.axMapScreenGlobal.MapLeft,
                                        (int)y2 - GlobalVarLn.axMapScreenGlobal.MapTop,
                                        mmm,
                                        mmm
                                        //7,
                                        //7
                                        //(int)wd1,
                                        //(int)ht1
                                        );
                }

            } // 4Rmin...5Rmin
            // -----------------------------------------------------------------
            // 5Rmin...6Rmin

            else if ((R >= (5 * Rmin)) && (R < (6 * Rmin)))
            {
                Brush brush5 = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr, GlobalVarLn.rd1, 0, 0));

                if ((gamma >= 0.1) && (gamma <= 3.1))
                {
                    graph.FillRectangle(
                                        brush5,
                                        (int)x2 - GlobalVarLn.axMapScreenGlobal.MapLeft,
                                        (int)y2 - GlobalVarLn.axMapScreenGlobal.MapTop,
                                        mmm,
                                        mmm
                                        //7,
                                        //7
                                        //(int)wd1,
                                        //(int)ht1
                                        );
                }

            } // 5Rmin...6Rmin
            // -----------------------------------------------------------------
            // 6Rmin...7Rmin

            else if ((R >= (6 * Rmin)) && (R < (7 * Rmin)))
            {
                Brush brush6 = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr, GlobalVarLn.rd2, 0, 0));

                if ((gamma >= 0.1) && (gamma <= 3.1))
                {
                    graph.FillRectangle(
                                        brush6,
                                        (int)x2 - GlobalVarLn.axMapScreenGlobal.MapLeft,
                                        (int)y2 - GlobalVarLn.axMapScreenGlobal.MapTop,
                                        mmm,
                                        mmm
                                        //7,
                                        //7
                                        //(int)wd1,
                                        //(int)ht1
                                        );
                }

            } // 6Rmin...7Rmin
            // -----------------------------------------------------------------
            // 7Rmin...8Rmin

            else if ((R >= (7 * Rmin)) && (R < (8 * Rmin)))
            {
                Brush brush7 = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr, GlobalVarLn.rd3, 0, 0));

                if ((gamma >= 0.1) && (gamma <= 3.1))
                {
                    graph.FillRectangle(
                                        brush7,
                                        (int)x2 - GlobalVarLn.axMapScreenGlobal.MapLeft,
                                        (int)y2 - GlobalVarLn.axMapScreenGlobal.MapTop,
                                        mmm,
                                        mmm
                                        //7,
                                        //7
                                        //(int)wd1,
                                        //(int)ht1
                                        );
                }

            } // 7Rmin...8Rmin
            // -----------------------------------------------------------------
            // 8Rmin...9Rmin

            else if ((R >= (8 * Rmin)) && (R < (9 * Rmin)))
            {
                Brush brush8 = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr, 0, 0, GlobalVarLn.bl1));

                if ((gamma >= 0.1) && (gamma <= 3.1))
                {
                    graph.FillRectangle(
                                        brush8,
                                        (int)x2 - GlobalVarLn.axMapScreenGlobal.MapLeft,
                                        (int)y2 - GlobalVarLn.axMapScreenGlobal.MapTop,
                                        mmm,
                                        mmm
                                        //7,
                                        //7
                                        //(int)wd1,
                                        //(int)ht1
                                        );
                }

            } // 8Rmin...9Rmin
            // -----------------------------------------------------------------
            // 9Rmin...10Rmin

            else if ((R >= (9 * Rmin)) && (R < (10 * Rmin)))
            {
                Brush brush9 = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr, 0, 0, GlobalVarLn.bl2));

                if ((gamma >= 0.1) && (gamma <= 3.1))
                {
                    graph.FillRectangle(
                                        brush9,
                                        (int)x2 - GlobalVarLn.axMapScreenGlobal.MapLeft,
                                        (int)y2 - GlobalVarLn.axMapScreenGlobal.MapTop,
                                        mmm,
                                        mmm
                                        //7,
                                        //7
                                        //(int)wd1,
                                        //(int)ht1
                                        );
                }

            } // 9Rmin...10Rmin
            // -----------------------------------------------------------------
            // 10Rmin...11Rmin

            //else if (R >= (10 * Rmin))
            else if ((R >= (10 * Rmin)) && (R < (11 * Rmin)))

            {
                Brush brush10 = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr, 0, 0, GlobalVarLn.bl3));

                if ((gamma >= 0.1) && (gamma <= 3.1))
                {
                    graph.FillRectangle(
                                        brush10,
                                        (int)x2 - GlobalVarLn.axMapScreenGlobal.MapLeft,
                                        (int)y2 - GlobalVarLn.axMapScreenGlobal.MapTop,
                                        mmm,
                                        mmm
                                        //7,
                                        //7
                                        //(int)wd1,
                                        //(int)ht1
                                        );
                 }

            } // 10Rmin...11Rmin
            // -----------------------------------------------------------------
*/


        } //f_Map_Bearing
        // *********************************************************************

        // *********************************************************************
        // ReDraw ZoneBearing
        // *********************************************************************

        public static void f_ReDraw_Map_Bearing()
        {
            int ii = 0;

            for (ii = 0; ii < GlobalVarLn.list2_PelMain.Count; ii++)
            {
                f_Map_Bearing(
                              GlobalVarLn.list2_PelMain[ii].X_m,
                              GlobalVarLn.list2_PelMain[ii].Y_m,
                              GlobalVarLn.list2_PelMain[ii].ds,
                              GlobalVarLn.list2_PelMain[ii].dh,
                              GlobalVarLn.list2_PelMain[ii].R,
                              GlobalVarLn.list2_PelMain[ii].Rmin,
                              GlobalVarLn.list2_PelMain[ii].Intensity,
                              GlobalVarLn.list2_PelMain[ii].Gamma
                              );             

            }


            if (GlobalVarLn.flCoordSP_PelMain == 1) // Pel1
            {
                // SP(Pel1)
                ClassMap.f_DrawSPXY(
                              GlobalVarLn.XCenter_PelMain,  // m на местности
                              GlobalVarLn.YCenter_PelMain,
                                  ""
                             );

            } // Pel1

            if (GlobalVarLn.flCoordYS1_PelMain == 1) // Pel2
            {

                // SP(Pel2)
                ClassMap.f_DrawSPXY(
                              GlobalVarLn.XPoint1_PelMain,  // m на местности
                              GlobalVarLn.YPoint1_PelMain,
                                  ""
                             );

            } // Pel2



        } // ReDraw ZoneBearing
        // *********************************************************************

        // *********************************************************************
        // ReDraw ZoneBearing1
        // *********************************************************************

        public static void f_ReDraw_Map_Bearing1(

                                                )
        {    //2106
            GlobalVarLn.fl_PelMain = 0;
            // .....................................................................
            // Убрать с карты
            GlobalVarLn.axMapScreenGlobal.Repaint();
            // .....................................................................
            GlobalVarLn.fl_PelMain = 1;

            int ii = 0;


            // -----------------------------------------------------------------
            //int prozr1 = 0;
            //int clr1 = 0;
            //int fl1 = 0;
            //int fl2 = 0;
            //int fl3 = 0;
            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;
            double x3 = 0;
            double y3 = 0;
            double dx = 0;
            double dy = 0;

            //int mmm = 0;
            // -----------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
            //Brush brush1;
            // -----------------------------------------------------------------


            // ZONE *************************************************************

            if (GlobalVarLn.axMapScreenGlobal.ViewScale == 500000)
            {
                GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
            }
            else if ((GlobalVarLn.axMapScreenGlobal.ViewScale > 500000) && (GlobalVarLn.axMapScreenGlobal.ViewScale <= 1000000))
            {
                if (GlobalVarLn.dD_PelMain1 == 100)
                    //GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 130;
                    GlobalVarLn.prozr1_bear =(int)((double)(GlobalVarLn.prozr)*0.3);
                else
                    GlobalVarLn.prozr1_bear = (int)((double)(GlobalVarLn.prozr) * 0.8);

/*
                else if (GlobalVarLn.dD_PelMain1 == 200)
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 30;
                else if (GlobalVarLn.dD_PelMain1 == 300)
                    //GlobalVarLn.prozr1_bear = 200;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 30;
                else if (GlobalVarLn.dD_PelMain1 == 400)
                    //GlobalVarLn.prozr1_bear = 200;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 30;
                else if (GlobalVarLn.dD_PelMain1 == 500)
                    //GlobalVarLn.prozr1_bear = 200;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 30;
*/

                if (GlobalVarLn.prozr1_bear <= 30)
                    GlobalVarLn.prozr1_bear = 30;
                if (GlobalVarLn.prozr1_bear >= 255)
                    GlobalVarLn.prozr1_bear = 255;

            }
            else if ((GlobalVarLn.axMapScreenGlobal.ViewScale > 1000000) && (GlobalVarLn.axMapScreenGlobal.ViewScale <= 2000000))
            //prozr1 = 100;
            {

                if (GlobalVarLn.dD_PelMain1 == 100)
                    //GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 130;
                    GlobalVarLn.prozr1_bear =(int)((double)(GlobalVarLn.prozr)*0.4);
                //else
                    //GlobalVarLn.prozr1_bear = (int)((double)(GlobalVarLn.prozr) * 0.85);
                else if (GlobalVarLn.dD_PelMain1 == 200)
                    GlobalVarLn.prozr1_bear = (int)((double)(GlobalVarLn.prozr) * 0.9);
                else if (GlobalVarLn.dD_PelMain1 == 300)
                    GlobalVarLn.prozr1_bear = (int)((double)(GlobalVarLn.prozr) * 0.6);
                else if (GlobalVarLn.dD_PelMain1 == 400)
                    GlobalVarLn.prozr1_bear = (int)((double)(GlobalVarLn.prozr) * 0.9);
                else if (GlobalVarLn.dD_PelMain1 == 500)
                    GlobalVarLn.prozr1_bear = 253;

                if (GlobalVarLn.prozr1_bear <= 30)
                    GlobalVarLn.prozr1_bear = 30;
                if (GlobalVarLn.prozr1_bear >= 255)
                    GlobalVarLn.prozr1_bear = 255;

            }
            else if ((GlobalVarLn.axMapScreenGlobal.ViewScale > 2000000) && (GlobalVarLn.axMapScreenGlobal.ViewScale <= 4000000))
            //prozr1 = 100;
            {

                if (GlobalVarLn.dD_PelMain1 == 100)
                    //GlobalVarLn.prozr1_bear = GlobalVarLn.prozr - 130;
                    //GlobalVarLn.prozr1_bear = (int)((double)(GlobalVarLn.prozr) * 0.1);
                    GlobalVarLn.prozr1_bear = 10;
                //else
                //GlobalVarLn.prozr1_bear = (int)((double)(GlobalVarLn.prozr) * 0.85);
                else if (GlobalVarLn.dD_PelMain1 == 200)
                    GlobalVarLn.prozr1_bear = (int)((double)(GlobalVarLn.prozr) * 0.5);
                else if (GlobalVarLn.dD_PelMain1 == 300)
                    GlobalVarLn.prozr1_bear = (int)((double)(GlobalVarLn.prozr) * 253);
                else if (GlobalVarLn.dD_PelMain1 == 400)
                    GlobalVarLn.prozr1_bear = (int)((double)(GlobalVarLn.prozr) * 0.7);
                else if (GlobalVarLn.dD_PelMain1 == 500)
                    GlobalVarLn.prozr1_bear = (int)((double)(GlobalVarLn.prozr) * 0.5);

                if (GlobalVarLn.prozr1_bear <= 30)
                    GlobalVarLn.prozr1_bear = 30;
                if (GlobalVarLn.prozr1_bear >= 255)
                    GlobalVarLn.prozr1_bear = 255;


            }

            // .................................................................
/*
            else if ((GlobalVarLn.axMapScreenGlobal.ViewScale < 500000) && (GlobalVarLn.axMapScreenGlobal.ViewScale >= 250000))
            //prozr1 = 240;
            {

                if (GlobalVarLn.dD_PelMain1 == 100)
                    //GlobalVarLn.prozr1_bear = 240;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr + 10;
                else if (GlobalVarLn.dD_PelMain1 == 200)
                    //GlobalVarLn.prozr1_bear = 240;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr + 10;
                else if (GlobalVarLn.dD_PelMain1 == 300)
                    //GlobalVarLn.prozr1_bear = 240;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr + 10;
                else if (GlobalVarLn.dD_PelMain1 == 400)
                    //GlobalVarLn.prozr1_bear = 240;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr + 10;
                else if (GlobalVarLn.dD_PelMain1 == 500)
                    //GlobalVarLn.prozr1_bear = 240;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr + 10;
                if (GlobalVarLn.prozr1_bear >= 255)
                    GlobalVarLn.prozr1_bear = 255;

            }
            else if ((GlobalVarLn.axMapScreenGlobal.ViewScale < 250000) && (GlobalVarLn.axMapScreenGlobal.ViewScale >= 125000))
            //prozr1 = 235;
            {

                if (GlobalVarLn.dD_PelMain1 == 100)
                    //GlobalVarLn.prozr1_bear = 235;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr + 5;
                else if (GlobalVarLn.dD_PelMain1 == 200)
                    //GlobalVarLn.prozr1_bear = 235;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr + 5;
                else if (GlobalVarLn.dD_PelMain1 == 300)
                    //GlobalVarLn.prozr1_bear = 235;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr + 5;
                else if (GlobalVarLn.dD_PelMain1 == 400)
                    //GlobalVarLn.prozr1_bear = 235;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr + 5;
                else if (GlobalVarLn.dD_PelMain1 == 500)
                    //GlobalVarLn.prozr1_bear = 235;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr + 5;
                if (GlobalVarLn.prozr1_bear >= 255)
                    GlobalVarLn.prozr1_bear = 255;

            }
            else if ((GlobalVarLn.axMapScreenGlobal.ViewScale < 125000) && (GlobalVarLn.axMapScreenGlobal.ViewScale >= 62500))
            //prozr1 = 230;
            {

                if (GlobalVarLn.dD_PelMain1 == 100)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 200)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 300)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 400)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 500)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                if (GlobalVarLn.prozr1_bear >= 255)
                    GlobalVarLn.prozr1_bear = 255;

            }
            else if ((GlobalVarLn.axMapScreenGlobal.ViewScale < 60000) && (GlobalVarLn.axMapScreenGlobal.ViewScale >= 31250))
            //prozr1 = 230;
            {
                if (GlobalVarLn.dD_PelMain1 == 100)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 200)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 300)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 400)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 500)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                if (GlobalVarLn.prozr1_bear >= 255)
                    GlobalVarLn.prozr1_bear = 255;
            }
            else if ((GlobalVarLn.axMapScreenGlobal.ViewScale < 30000) && (GlobalVarLn.axMapScreenGlobal.ViewScale >= 15625))
            //prozr1 = 230;
            {
                if (GlobalVarLn.dD_PelMain1 == 100)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 200)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 300)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 400)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 500)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                if (GlobalVarLn.prozr1_bear >= 255)
                    GlobalVarLn.prozr1_bear = 255;
            }
            else if ((GlobalVarLn.axMapScreenGlobal.ViewScale < 15625) && (GlobalVarLn.axMapScreenGlobal.ViewScale >= 10000))
            //prozr1 = 230;
            {
                if (GlobalVarLn.dD_PelMain1 == 100)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 200)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 300)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 400)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                else if (GlobalVarLn.dD_PelMain1 == 500)
                    //GlobalVarLn.prozr1_bear = 230;
                    GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
                if (GlobalVarLn.prozr1_bear >= 255)
                    GlobalVarLn.prozr1_bear = 255;
            }
 */
            else
                GlobalVarLn.prozr1_bear = GlobalVarLn.prozr;
            // -----------------------------------------------------------------
            // Rmin...2Rmin
            GlobalVarLn.brush1_1bear = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, 0, GlobalVarLn.grn1, 0)); // ближняя

            // 2Rmin...3Rmin
            GlobalVarLn.brush1_2bear = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, 0, GlobalVarLn.grn2, 0));

            // 3Rmin...4Rmin
            GlobalVarLn.brush1_3bear = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, 0, GlobalVarLn.grn3, 0));

            // 4Rmin...5Rmin
            GlobalVarLn.brush1_4bear = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, 0, GlobalVarLn.grn4, 0));

            // 5Rmin...6Rmin
            GlobalVarLn.brush1_5bear = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, 0, GlobalVarLn.grn5, 0));

            // 6Rmin...7Rmin
            GlobalVarLn.brush1_6bear = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, GlobalVarLn.rd1, 0, 0));

            // 7Rmin...8Rmin
            GlobalVarLn.brush1_7bear = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, GlobalVarLn.rd2, 0, 0));

            // 8Rmin...9Rmin
            GlobalVarLn.brush1_8bear = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, GlobalVarLn.rd3, 0, 0));

            // 9Rmin...10Rmin
            GlobalVarLn.brush1_9bear = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, GlobalVarLn.rd4, 0, 0));

            // 10Rmin...11Rmin
            GlobalVarLn.brush1_10bear = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, GlobalVarLn.rd5, 0, 0));

            // -----------------------------------------------------------------


            // ************************************************************* ZONE




            // FOR_ZONE ************************************************************

            //2310_1

            Rectangle rect_tmp = new Rectangle();

            //2310_1
            
                        GlobalVarLn.list_rects1.Clear();
                        GlobalVarLn.list_rects2.Clear();
                        GlobalVarLn.list_rects3.Clear();
                        GlobalVarLn.list_rects4.Clear();
                        GlobalVarLn.list_rects5.Clear();
                        GlobalVarLn.list_rects6.Clear();
                        GlobalVarLn.list_rects7.Clear();
                        GlobalVarLn.list_rects8.Clear();
                        GlobalVarLn.list_rects9.Clear();
                        GlobalVarLn.list_rects10.Clear();
              

                        for (ii = 0; ii < GlobalVarLn.list2_PelMain.Count; ii++)
                        {

                            // -----------------------------------------------------------------
                            x1 = GlobalVarLn.list2_PelMain[ii].X_m;
                            y1 = GlobalVarLn.list2_PelMain[ii].Y_m;

                            // LeftUp
                            x2 = x1 + GlobalVarLn.list2_PelMain[ii].ds / 2;
                            y2 = y1 - GlobalVarLn.list2_PelMain[ii].dh / 2;
                            //x2 = x1 + mmm/2; 
                            //y2 = y1 - mmm/2;
                            // RightDown
                            x3 = x1 - GlobalVarLn.list2_PelMain[ii].ds / 2;
                            y3 = y1 + GlobalVarLn.list2_PelMain[ii].dh / 2;

                            // Расстояние в м на карте -> пикселы на изображении
                            mapPlaneToPicture(GlobalVarLn.hmapl, ref x1, ref y1);
                            mapPlaneToPicture(GlobalVarLn.hmapl, ref x2, ref y2);
                            mapPlaneToPicture(GlobalVarLn.hmapl, ref x3, ref y3);

                            dx = Math.Abs(x2 - x3);
                            dy = Math.Abs(y2 - y3);
                            if (dx <= 1) dx = 1;
                            if (dy <= 1) dy = 1;

                            // -----------------------------------------------------------------
                            //2310

                            rect_tmp.X = (int)x2 - GlobalVarLn.axMapScreenGlobal.MapLeft;
                            rect_tmp.Y = (int)y2 - GlobalVarLn.axMapScreenGlobal.MapTop;
                            rect_tmp.Width = (int)(dx + 0.9);
                            rect_tmp.Height = (int)(dy + 0.9);
                            // -----------------------------------------------------------------


                            // Rmin...2Rmin
                            if ((GlobalVarLn.list2_PelMain[ii].R >= GlobalVarLn.list2_PelMain[ii].Rmin) &&
                                (GlobalVarLn.list2_PelMain[ii].R < (2 * GlobalVarLn.list2_PelMain[ii].Rmin)))
                            {
                                //2310
                                GlobalVarLn.list_rects1.Add(rect_tmp);
                            }

                            // 2Rmin...3Rmin
                            else if ((GlobalVarLn.list2_PelMain[ii].R >= (2 * GlobalVarLn.list2_PelMain[ii].Rmin)) &&
                                (GlobalVarLn.list2_PelMain[ii].R < (3 * GlobalVarLn.list2_PelMain[ii].Rmin)))
                            {
                                //2310
                                GlobalVarLn.list_rects2.Add(rect_tmp);
                            }

                            // 3Rmin...4Rmin
                            else if ((GlobalVarLn.list2_PelMain[ii].R >= (3 * GlobalVarLn.list2_PelMain[ii].Rmin)) &&
                                (GlobalVarLn.list2_PelMain[ii].R < (4 * GlobalVarLn.list2_PelMain[ii].Rmin)))
                            {
                                //2310
                                GlobalVarLn.list_rects3.Add(rect_tmp);
                            }

                            // 4Rmin...5Rmin
                            else if ((GlobalVarLn.list2_PelMain[ii].R >= (4 * GlobalVarLn.list2_PelMain[ii].Rmin)) &&
                                (GlobalVarLn.list2_PelMain[ii].R < (5 * GlobalVarLn.list2_PelMain[ii].Rmin)))
                            {
                                //2310
                                GlobalVarLn.list_rects4.Add(rect_tmp);
                            }

                            // 5Rmin...6Rmin
                            else if ((GlobalVarLn.list2_PelMain[ii].R >= (5 * GlobalVarLn.list2_PelMain[ii].Rmin)) &&
                                (GlobalVarLn.list2_PelMain[ii].R < (6 * GlobalVarLn.list2_PelMain[ii].Rmin)))
                            {
                                //2310
                                GlobalVarLn.list_rects5.Add(rect_tmp);
                            }

                            // 6Rmin...7Rmin
                            else if ((GlobalVarLn.list2_PelMain[ii].R >= (6 * GlobalVarLn.list2_PelMain[ii].Rmin)) &&
                                (GlobalVarLn.list2_PelMain[ii].R < (7 * GlobalVarLn.list2_PelMain[ii].Rmin)))
                            {
                                //2310
                                GlobalVarLn.list_rects6.Add(rect_tmp);
                            }

                            // 7Rmin...8Rmin
                            else if ((GlobalVarLn.list2_PelMain[ii].R >= (7 * GlobalVarLn.list2_PelMain[ii].Rmin)) &&
                                (GlobalVarLn.list2_PelMain[ii].R < (8 * GlobalVarLn.list2_PelMain[ii].Rmin)))
                            {
                                //2310
                                GlobalVarLn.list_rects7.Add(rect_tmp);
                            }

                            // 8Rmin...9Rmin
                            else if ((GlobalVarLn.list2_PelMain[ii].R >= (8 * GlobalVarLn.list2_PelMain[ii].Rmin)) &&
                                (GlobalVarLn.list2_PelMain[ii].R < (9 * GlobalVarLn.list2_PelMain[ii].Rmin)))
                            {
                                //2310
                                GlobalVarLn.list_rects8.Add(rect_tmp);
                            }

                            // 9Rmin...10Rmin
                            else if ((GlobalVarLn.list2_PelMain[ii].R >= (9 * GlobalVarLn.list2_PelMain[ii].Rmin)) &&
                                (GlobalVarLn.list2_PelMain[ii].R < (10 * GlobalVarLn.list2_PelMain[ii].Rmin)))
                            {
                                //2310
                                GlobalVarLn.list_rects9.Add(rect_tmp);
                            }

                            // 10Rmin...11Rmin
                            else if ((GlobalVarLn.list2_PelMain[ii].R >= (10 * GlobalVarLn.list2_PelMain[ii].Rmin)) &&
                                (GlobalVarLn.list2_PelMain[ii].R < (11 * GlobalVarLn.list2_PelMain[ii].Rmin)))
                            {
                                //2310
                                GlobalVarLn.list_rects10.Add(rect_tmp);

                            }
                            // -----------------------------------------------------------------


                        } // FOR_ZONE
             
            // ************************************************************ FOR_ZONE


            // ZONE *************************************************************

            //2310 
            //if ((GlobalVarLn.list_rects1.Count!=0 ) && (GlobalVarLn.list2_PelMain.Count != 0))
            if (GlobalVarLn.list2_PelMain.Count != 0)
            {

              //2310
              if(GlobalVarLn.list_rects1.Count!=0)
              {
              Rectangle[] rects1=GlobalVarLn.list_rects1.ToArray();
              graph.FillRectangles(
                                    GlobalVarLn.brush1_1bear,
                                    rects1
                                    );

              Array.Clear(rects1, 0, GlobalVarLn.list_rects1.Count);
              }

              //2310
              if (GlobalVarLn.list_rects2.Count != 0)
              {
                  Rectangle[] rects2 = GlobalVarLn.list_rects2.ToArray();
                  graph.FillRectangles(
                                      GlobalVarLn.brush1_2bear,
                                      rects2
                                      );
                  Array.Clear(rects2, 0, GlobalVarLn.list_rects2.Count);
              }

              //2310
              if (GlobalVarLn.list_rects3.Count != 0)
              {
                Rectangle[] rects3 = GlobalVarLn.list_rects3.ToArray();
                graph.FillRectangles(
                                    GlobalVarLn.brush1_3bear,
                                    rects3
                                    );
                Array.Clear(rects3, 0, GlobalVarLn.list_rects3.Count);
              }

              //2310
              if (GlobalVarLn.list_rects4.Count != 0)
              {
                Rectangle[] rects4 = GlobalVarLn.list_rects4.ToArray();
                graph.FillRectangles(
                                    GlobalVarLn.brush1_4bear,
                                    rects4
                                    );
                Array.Clear(rects4, 0, GlobalVarLn.list_rects4.Count);
              }

              //2310
              if (GlobalVarLn.list_rects5.Count != 0)
              {
                Rectangle[] rects5 = GlobalVarLn.list_rects5.ToArray();
                graph.FillRectangles(
                                    GlobalVarLn.brush1_5bear,
                                    rects5
                                    );
                Array.Clear(rects5, 0, GlobalVarLn.list_rects5.Count);
              }

              //2310
              if (GlobalVarLn.list_rects6.Count != 0)
              {
                Rectangle[] rects6 = GlobalVarLn.list_rects6.ToArray();
                graph.FillRectangles(
                                    GlobalVarLn.brush1_6bear,
                                    rects6
                                    );
                Array.Clear(rects6, 0, GlobalVarLn.list_rects6.Count);
              }

              //2310
              if (GlobalVarLn.list_rects7.Count != 0)
              {
                Rectangle[] rects7 = GlobalVarLn.list_rects7.ToArray();
                graph.FillRectangles(
                                    GlobalVarLn.brush1_7bear,
                                    rects7
                                    );
                Array.Clear(rects7, 0, GlobalVarLn.list_rects7.Count);
              }

              //2310
              if (GlobalVarLn.list_rects8.Count != 0)
              {
                Rectangle[] rects8 = GlobalVarLn.list_rects8.ToArray();
                graph.FillRectangles(
                                    GlobalVarLn.brush1_8bear,
                                    rects8
                                    );
                Array.Clear(rects8, 0, GlobalVarLn.list_rects8.Count);
              }

              //2310
              if (GlobalVarLn.list_rects9.Count != 0)
              {
                Rectangle[] rects9 = GlobalVarLn.list_rects9.ToArray();
                graph.FillRectangles(
                                    GlobalVarLn.brush1_9bear,
                                    rects9
                                    );
                Array.Clear(rects9, 0, GlobalVarLn.list_rects9.Count);
              }

              //2310
              if (GlobalVarLn.list_rects10.Count != 0)
              {
                Rectangle[] rects10 = GlobalVarLn.list_rects10.ToArray();
                graph.FillRectangles(
                                    GlobalVarLn.brush1_10bear,
                                    rects10
                                    );
                Array.Clear(rects10, 0, GlobalVarLn.list_rects10.Count);
              }

            }

/*
            if (GlobalVarLn.fl1_bear == 1)  // green
            {
                brush1 = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, 0, GlobalVarLn.clr1_bear, 0)); // ближняя

            }

            else if (GlobalVarLn.fl2_bear == 1) // red
            {
                brush1 = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, GlobalVarLn.clr1_bear, 0, 0)); // ближняя

            }

            //else if (fl3 == 1) // blue
            else
            {
                brush1 = new SolidBrush(Color.FromArgb(GlobalVarLn.prozr1_bear, 0, 0, GlobalVarLn.clr1_bear)); // ближняя

            }
*/




/*

            for (ii = 0; ii < GlobalVarLn.list2_PelMain.Count; ii++)
            {

                // -----------------------------------------------------------------
                x1 = GlobalVarLn.list2_PelMain[ii].X_m;
                y1 = GlobalVarLn.list2_PelMain[ii].Y_m;

                // LeftUp
                x2 = x1 + GlobalVarLn.list2_PelMain[ii].ds / 2;
                y2 = y1 - GlobalVarLn.list2_PelMain[ii].dh / 2;
                //x2 = x1 + mmm/2; 
                //y2 = y1 - mmm/2;
                // RightDown
                x3 = x1 - GlobalVarLn.list2_PelMain[ii].ds / 2;
                y3 = y1 + GlobalVarLn.list2_PelMain[ii].dh / 2;

                // Расстояние в м на карте -> пикселы на изображении
                mapPlaneToPicture(GlobalVarLn.hmapl, ref x1, ref y1);
                mapPlaneToPicture(GlobalVarLn.hmapl, ref x2, ref y2);
                mapPlaneToPicture(GlobalVarLn.hmapl, ref x3, ref y3);

                dx = Math.Abs(x2-x3);
                dy = Math.Abs(y2 - y3);
                //if (dx <= 0) dx = 1;
                //if (dy <= 0) dy = 1;
                if (dx <= 1) dx = 1;
                if (dy <= 1) dy = 1;

                // -----------------------------------------------------------------

                fl1 = 0;
                fl2 = 0;
                fl3 = 0;

                // Rmin...2Rmin
                if ((GlobalVarLn.list2_PelMain[ii].R >= GlobalVarLn.list2_PelMain[ii].Rmin) &&
                    (GlobalVarLn.list2_PelMain[ii].R < (2 * GlobalVarLn.list2_PelMain[ii].Rmin)))
                {
                    clr1 = GlobalVarLn.grn1;
                    fl1 = 1; // green
                }

                // 2Rmin...3Rmin
                else if ((GlobalVarLn.list2_PelMain[ii].R >= (2 * GlobalVarLn.list2_PelMain[ii].Rmin)) &&
                    (GlobalVarLn.list2_PelMain[ii].R < (3 * GlobalVarLn.list2_PelMain[ii].Rmin)))
                {
                    clr1 = GlobalVarLn.grn2;
                    fl1 = 1; // green
                }

                // 3Rmin...4Rmin
                else if ((GlobalVarLn.list2_PelMain[ii].R >= (3 * GlobalVarLn.list2_PelMain[ii].Rmin)) &&
                    (GlobalVarLn.list2_PelMain[ii].R < (4 * GlobalVarLn.list2_PelMain[ii].Rmin)))
                {
                    clr1 = GlobalVarLn.grn3;
                    fl1 = 1; // green
                }

                // 4Rmin...5Rmin
                else if ((GlobalVarLn.list2_PelMain[ii].R >= (4 * GlobalVarLn.list2_PelMain[ii].Rmin)) &&
                    (GlobalVarLn.list2_PelMain[ii].R < (5 * GlobalVarLn.list2_PelMain[ii].Rmin)))
                {
                    clr1 = GlobalVarLn.grn4;
                    fl1 = 1; // green
                }

                // 5Rmin...6Rmin
                else if ((GlobalVarLn.list2_PelMain[ii].R >= (5 * GlobalVarLn.list2_PelMain[ii].Rmin)) &&
                    (GlobalVarLn.list2_PelMain[ii].R < (6 * GlobalVarLn.list2_PelMain[ii].Rmin)))
                {
                    fl1 = 1; // red
                    clr1 = GlobalVarLn.grn5;
                }

                // 6Rmin...7Rmin
                else if ((GlobalVarLn.list2_PelMain[ii].R >= (6 * GlobalVarLn.list2_PelMain[ii].Rmin)) &&
                    (GlobalVarLn.list2_PelMain[ii].R < (7 * GlobalVarLn.list2_PelMain[ii].Rmin)))
                {
                    fl2 = 1; // red
                    clr1 = GlobalVarLn.rd1;
                }

                // 7Rmin...8Rmin
                else if ((GlobalVarLn.list2_PelMain[ii].R >= (7 * GlobalVarLn.list2_PelMain[ii].Rmin)) &&
                    (GlobalVarLn.list2_PelMain[ii].R < (8 * GlobalVarLn.list2_PelMain[ii].Rmin)))
                {
                    fl2 = 1; // red
                    clr1 = GlobalVarLn.rd2;
                }

                // 8Rmin...9Rmin
                else if ((GlobalVarLn.list2_PelMain[ii].R >= (8 * GlobalVarLn.list2_PelMain[ii].Rmin)) &&
                    (GlobalVarLn.list2_PelMain[ii].R < (9 * GlobalVarLn.list2_PelMain[ii].Rmin)))
                {
                    fl2 = 1; // red
                    clr1 = GlobalVarLn.rd3;
                }

                // 9Rmin...10Rmin
                else if ((GlobalVarLn.list2_PelMain[ii].R >= (9 * GlobalVarLn.list2_PelMain[ii].Rmin)) &&
                    (GlobalVarLn.list2_PelMain[ii].R < (10 * GlobalVarLn.list2_PelMain[ii].Rmin)))
                {
                    fl2 = 1; // red
                    clr1 = GlobalVarLn.rd4;

                }

                // 10Rmin...11Rmin
                else if ((GlobalVarLn.list2_PelMain[ii].R >= (10 * GlobalVarLn.list2_PelMain[ii].Rmin)) &&
                    (GlobalVarLn.list2_PelMain[ii].R < (11 * GlobalVarLn.list2_PelMain[ii].Rmin)))
                {
                    fl2 = 1; // red
                    clr1 = GlobalVarLn.rd5;

                }
                // -----------------------------------------------------------------
                if (fl1 == 1)  // green
                {
                    brush1 = new SolidBrush(Color.FromArgb(prozr1, 0, clr1, 0)); // ближняя

                }

                else if (fl2 == 1) // red
                {
                    brush1 = new SolidBrush(Color.FromArgb(prozr1, clr1, 0, 0)); // ближняя

                }

                //else if (fl3 == 1) // blue
                else
                {
                    brush1 = new SolidBrush(Color.FromArgb(prozr1, 0, 0, clr1)); // ближняя

                }
 
                // -----------------------------------------------------------------
                {
                    graph.FillRectangle(
                                        brush1,
                                        // Верхний левый угол
                                        (int)x2 - GlobalVarLn.axMapScreenGlobal.MapLeft,
                                        (int)y2 - GlobalVarLn.axMapScreenGlobal.MapTop,
                                        //mmm,
                                        //mmm
                                        //(int)dx,
                                        //(int)dy
                                        //(int)(dx+0.5),
                                        //(int)(dy+0.5)
                                        (int)(dx+0.9),
                                        (int)(dy+0.9)

                                        //7,
                                         //7
                                        );



                }
                // -----------------------------------------------------------------

            } // FOR

 */
            // ************************************************************* ZONE

          // ------------------------------------------------------------------
          // Pel1,Pel2

            if (GlobalVarLn.flCoordSP_PelMain == 1) // Pel1
            {
                // SP(Pel1)
                ClassMap.f_DrawSPXY(
                              GlobalVarLn.XCenter_PelMain,  // m на местности
                              GlobalVarLn.YCenter_PelMain,
                                  ""
                             );

            } // Pel1

            if (GlobalVarLn.flCoordYS1_PelMain == 1) // Pel2
            {

                // SP(Pel2)
                ClassMap.f_DrawSPXY(
                              GlobalVarLn.XPoint1_PelMain,  // m на местности
                              GlobalVarLn.YPoint1_PelMain,
                                  ""
                             );

            } // Pel2
            // ------------------------------------------------------------------



        } // ReDraw ZoneBearing1
        // *********************************************************************



        // DRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWD ОТРИСОВКА


// NEW ****************************************************************************

// ********************************************************************************
// Закрыть окно при открытии другого
// ********************************************************************************

public static void f_RemoveFrm(int fl)
{

  // Sostav
  if ((fl != 1) && (GlobalVarLn.f_Open_objFormSost == 1))
   {
    GlobalVarLn.objFormSostG.Hide();
    GlobalVarLn.f_Open_objFormSost = 0;
   }

  // Way
  if ((fl != 2) && (GlobalVarLn.f_Open_objFormWay == 1))
  {
      GlobalVarLn.objFormWayG.Hide();
      GlobalVarLn.f_Open_objFormWay = 0;
  }

  // Az
  if ((fl != 3) && (GlobalVarLn.f_Open_objFormAz == 1))
  {
      GlobalVarLn.objFormAzG.Hide();
      GlobalVarLn.f_Open_objFormAz = 0;
  }

  // ZPV
  if ((fl != 4) && (GlobalVarLn.f_Open_ObjFormLineSightRange == 1))
  {
      GlobalVarLn.ObjFormLineSightRangeG.Hide();
      GlobalVarLn.f_Open_ObjFormLineSightRange = 0;
  }

  // W on Zones
  if ((fl != 5) && (GlobalVarLn.f_Open_objZonePowerAvail == 1))
  {
      GlobalVarLn.objZonePowerAvailG.Hide();
      GlobalVarLn.f_Open_objZonePowerAvail = 0;
  }

  // W on YS
  if ((fl != 6) && (GlobalVarLn.f_Open_ObjCommPowerAvail == 1))
  {
      GlobalVarLn.ObjCommPowerAvailG.Hide();
      GlobalVarLn.f_Open_ObjCommPowerAvail = 0;
  }

  // Bearing
  if ((fl != 7) && (GlobalVarLn.f_Open_objFormBearing == 1))
  {
      GlobalVarLn.objFormBearingG.Hide();
      GlobalVarLn.f_Open_objFormBearing = 0;
  }

  // Az1
  if ((fl != 8) && (GlobalVarLn.f_Open_objFormAz1 == 1))
  {
      GlobalVarLn.objFormAz1G.Hide();
      GlobalVarLn.f_Open_objFormAz1 = 0;
  }

  // Энергодоступность по зонам (подавление)
  if ((fl != 9) && (GlobalVarLn.f_Open_objFormSuppression == 1))
  {
      GlobalVarLn.objFormSuppressionG.Hide();
      GlobalVarLn.f_Open_objFormSuppression = 0;
  }

} // Закрыть окно при открытии другого
// ********************************************************************************

// ********************************************************************************
// Загрузка состава комплекса с INI-файла
// ********************************************************************************
public static void f_LoadSostIni()
{
    // ......................................................................
    double xtmp_ed, ytmp_ed,htmp_ed;
    double xtmp1_ed, ytmp1_ed;

    xtmp_ed = 0;
    ytmp_ed = 0;
    htmp_ed = 0;
    xtmp1_ed = 0;
    ytmp1_ed = 0;

    // ......................................................................
    ClassMap objClassMap1_ed = new ClassMap();
    ClassMap objClassMap2_ed = new ClassMap();
    ClassMap objClassMap3_ed = new ClassMap();
    ClassMap objClassMap4_ed = new ClassMap();
    ClassMap objClassMap5_ed = new ClassMap();
    ClassMap objClassMap6_ed = new ClassMap();
    ClassMap objClassMap7_ed = new ClassMap();
    ClassMap objClassMap8_ed = new ClassMap();
    ClassMap objClassMap9_ed = new ClassMap();

    LF objLF = new LF();
    // ......................................................................




/*




    // SP1 ***********************************************************************

    // ......................................................................
    // !!! реальные координаты на местности карты в м (Plane)

    if (System.IO.File.Exists(Application.StartupPath + "\\Settings.ini"))
    {
        GlobalVarLn.XCenter_Sost = (double)iniRW.get_X_ASP();
        GlobalVarLn.YCenter_Sost = (double)iniRW.get_Y_ASP();
        GlobalVarLn.indz1_Sost = (int)iniRW.get_Sign_ASP();
    }
    else
    {
        MessageBox.Show("Невозможно открыть INI файл");
        return;   
    }

    if ((GlobalVarLn.XCenter_Sost == 0) || (GlobalVarLn.YCenter_Sost==0))
    {
        MessageBox.Show("Нет координат АСП");
        goto SP2;
    }

    xtmp_ed = GlobalVarLn.XCenter_Sost;
    ytmp_ed = GlobalVarLn.YCenter_Sost;
    objLF.X_m = GlobalVarLn.XCenter_Sost;
    objLF.Y_m = GlobalVarLn.YCenter_Sost;
    // ......................................................................
    // H

    GlobalVarLn.axMapPointGlobalAdd.SetPoint(xtmp_ed, ytmp_ed);
    GlobalVarLn.HCenter_Sost = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
    htmp_ed = GlobalVarLn.HCenter_Sost;
    GlobalVarLn.objFormSostG.tbOwnHeight.Text = Convert.ToString(htmp_ed);
    objLF.H_m = htmp_ed;
    // ......................................................................
    // sign

    objLF.indzn = GlobalVarLn.indz1_Sost;
    // ......................................................................
    // List

    //GlobalVarLn.list1_Sost.Add(objLF);
    GlobalVarLn.list1_Sost[0]=objLF;
    // ......................................................................
    // Draw SP1

    ClassMap.f_DrawSPXY11(
                  xtmp_ed,  // m на местности
                  ytmp_ed,
           (Bitmap)GlobalVarLn.objFormSostG.imageList1.Images[GlobalVarLn.list1_Sost[0].indzn]
                 );
    // ......................................................................
    GlobalVarLn.fl_Sost = 1;
    GlobalVarLn.flCoordSP_Sost = 1; // СП1 выбрана
    // ......................................................................
    // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
    // !!! Выход функции(rad) идет на место входных переменных

    mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

    // rad(WGS84)->grad(WGS84)
    xtmp1_ed = (xtmp_ed * 180) / Math.PI;
    ytmp1_ed = (ytmp_ed * 180) / Math.PI;

    //333
    GlobalVarLn.BWGS84_1_sost = xtmp1_ed;
    GlobalVarLn.LWGS84_1_sost = ytmp1_ed;

    // .......................................................................

    // WGS84(эллипсоид)->элл.Красовского ----------------------------------------------
    // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
    // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
    // Входные параметры -> град,km
    // Перевод в рад - внутри функции

    // dLong ..................................................................
    // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
    // (преобразования Молоденского), угл.сек

    objClassMap1_ed.f_dLong
        (
            // Входные параметры (град,км)
            xtmp1_ed,   // широта
            ytmp1_ed,  // долгота
            0,     // высота

            // DATUM,m
            GlobalVarLn.dXdat_comm,
            GlobalVarLn.dYdat_comm,
            GlobalVarLn.dZdat_comm,

            ref GlobalVarLn.dLong_comm   // приращение по долготе, угл.сек
        );
    // ................................................................ dLong

    // dLat .................................................................
    // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
    // (преобразования Молоденского), угл.сек

    objClassMap1_ed.f_dLat
        (
            // Входные параметры (град,км)
            xtmp1_ed,   // широта
            ytmp1_ed,  // долгота
            0,     // высота

            // DATUM,m
            GlobalVarLn.dXdat_comm,
            GlobalVarLn.dYdat_comm,
            GlobalVarLn.dZdat_comm,

            ref GlobalVarLn.dLat_comm        // приращение по долготе, угл.сек
        );
    // ................................................................. dLat

    // Lat,Long .............................................................
    // Преобразования широты и долготы при пересчете WGS84->SK42

    objClassMap1_ed.f_WGS84_SK42_Lat_Long
           (
               // Входные параметры (град,км)
               xtmp1_ed,   // широта
               ytmp1_ed,  // долгота
               0,     // высота
               GlobalVarLn.dLat_comm,       // приращение по долготе, угл.сек
               GlobalVarLn.dLong_comm,      // приращение по долготе, угл.сек

               // Выходные параметры (grad)
               ref GlobalVarLn.LatKrG_comm,   // широта
               ref GlobalVarLn.LongKrG_comm   // долгота
           );
    // ............................................................ Lat,Long

    // ----------------------------------- WGS84(эллипсоид)->элл.Красовского

    // .......................................................................
    // Эллипсоид Красовского, радианы

    GlobalVarLn.LatKrR_comm = (GlobalVarLn.LatKrG_comm * Math.PI) / 180;
    GlobalVarLn.LongKrR_comm = (GlobalVarLn.LongKrG_comm * Math.PI) / 180;
    // .......................................................................
    // Эллипсоид Красовского, grad,min,sec
    // dd.ddddd -> DD MM SS

    // Широта
    objClassMap3_ed.f_Grad_GMS
      (
        // Входные параметры (grad)
        GlobalVarLn.LatKrG_comm,

        // Выходные параметры 
        ref GlobalVarLn.Lat_Grad_comm,
        ref GlobalVarLn.Lat_Min_comm,
        ref GlobalVarLn.Lat_Sec_comm
      );

    // Долгота
    objClassMap3_ed.f_Grad_GMS
      (
        // Входные параметры (grad)
        GlobalVarLn.LongKrG_comm,

        // Выходные параметры 
        ref GlobalVarLn.Long_Grad_comm,
        ref GlobalVarLn.Long_Min_comm,
        ref GlobalVarLn.Long_Sec_comm
      );
    // .......................................................................

    // SK42(элл.)->Крюгер --------------------------------------------------
    // Преобразование геодезических координат (широта, долгота, высота) 
    // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
    // проекции Гаусса-Крюгера
    // Входные параметры -> !!!grad

    objClassMap3_ed.f_SK42_Krug
           (
        // Входные параметры (!!! grad)
        // !!! эллипсоид Красовского
               GlobalVarLn.LatKrG_comm,   // широта
               GlobalVarLn.LongKrG_comm,  // долгота

               // Выходные параметры (km)
               ref GlobalVarLn.XSP42_comm,
               ref GlobalVarLn.YSP42_comm
           );

    // km->m
    GlobalVarLn.XSP42_comm = GlobalVarLn.XSP42_comm * 1000;
    GlobalVarLn.YSP42_comm = GlobalVarLn.YSP42_comm * 1000;
    // ---------------------------------------------------- SK42(элл.)->Крюгер

    // .......................................................................
    // Отобразить координаты в окошках

    OtobrSP1();
    // .......................................................................

    // *********************************************************************** SP1
   
    // SP2 ***********************************************************************

    // ......................................................................
    // !!! реальные координаты на местности карты в м (Plane)

SP2:
    GlobalVarLn.XPoint1_Sost = (double)iniRW.get_X_ASPS();
    GlobalVarLn.YPoint1_Sost = (double)iniRW.get_Y_ASPS();
    GlobalVarLn.indz2_Sost = (int)iniRW.get_Sign_ASPS();

    if ((GlobalVarLn.XPoint1_Sost == 0) || (GlobalVarLn.YPoint1_Sost == 0))
    {
        MessageBox.Show("Нет координат сопряженной АСП");
        goto PU;
    }

    xtmp_ed = GlobalVarLn.XPoint1_Sost;
    ytmp_ed = GlobalVarLn.YPoint1_Sost;
    objLF.X_m = xtmp_ed;
    objLF.Y_m = ytmp_ed;
    // ......................................................................
    // H

    GlobalVarLn.axMapPointGlobalAdd.SetPoint(xtmp_ed, ytmp_ed);
    GlobalVarLn.HPoint1_Sost = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
    htmp_ed = GlobalVarLn.HPoint1_Sost;
    GlobalVarLn.objFormSostG.tbPt1Height.Text = Convert.ToString(htmp_ed);
    objLF.H_m = htmp_ed;
    // ......................................................................
    // sign

    objLF.indzn = GlobalVarLn.indz2_Sost;
    // ......................................................................
    // List

    //GlobalVarLn.list1_Sost.Add(objLF);
    GlobalVarLn.list1_Sost[1]=objLF;
    // ......................................................................
    // Draw SP2

    ClassMap.f_DrawSPXY11(
                  xtmp_ed,  // m на местности
                  ytmp_ed,
           (Bitmap)GlobalVarLn.objFormSostG.imageList2.Images[GlobalVarLn.list1_Sost[1].indzn]
                 );
    // ......................................................................
    GlobalVarLn.flCoordYS1_Sost = 1; // SP2 выбрана
    // ......................................................................
    // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
    // !!! Выход функции(rad) идет на место входных переменных

    mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

    // rad(WGS84)->grad(WGS84)
    xtmp1_ed = (xtmp_ed * 180) / Math.PI;
    ytmp1_ed = (ytmp_ed * 180) / Math.PI;

    //333
    GlobalVarLn.BWGS84_2_sost = xtmp1_ed;
    GlobalVarLn.LWGS84_2_sost = ytmp1_ed;

    // .......................................................................

    // WGS84(эллипсоид)->элл.Красовского ----------------------------------------------
    // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
    // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
    // Входные параметры -> град,km
    // Перевод в рад - внутри функции

    // dLong ..................................................................
    // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
    // (преобразования Молоденского), угл.сек

    objClassMap4_ed.f_dLong
        (
            // Входные параметры (град,км)
            xtmp1_ed,   // широта
            ytmp1_ed,  // долгота
            0,     // высота

            // DATUM,m
            GlobalVarLn.dXdat_comm,
            GlobalVarLn.dYdat_comm,
            GlobalVarLn.dZdat_comm,

            ref GlobalVarLn.dLong_comm   // приращение по долготе, угл.сек
        );
    // ................................................................ dLong

    // dLat .................................................................
    // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
    // (преобразования Молоденского), угл.сек

    objClassMap4_ed.f_dLat
        (
            // Входные параметры (град,км)
            xtmp1_ed,   // широта
            ytmp1_ed,  // долгота
            0,     // высота

            // DATUM,m
            GlobalVarLn.dXdat_comm,
            GlobalVarLn.dYdat_comm,
            GlobalVarLn.dZdat_comm,

            ref GlobalVarLn.dLat_comm        // приращение по долготе, угл.сек
        );
    // ................................................................. dLat

    // Lat,Long .............................................................
    // Преобразования широты и долготы при пересчете WGS84->SK42

    objClassMap4_ed.f_WGS84_SK42_Lat_Long
           (
               // Входные параметры (град,км)
               xtmp1_ed,   // широта
               ytmp1_ed,  // долгота
               0,     // высота
               GlobalVarLn.dLat_comm,       // приращение по долготе, угл.сек
               GlobalVarLn.dLong_comm,      // приращение по долготе, угл.сек

               // Выходные параметры (grad)
               ref GlobalVarLn.LatKrG_YS1_comm,   // широта
               ref GlobalVarLn.LongKrG_YS1_comm   // долгота
           );
    // ............................................................ Lat,Long

    // ----------------------------------- WGS84(эллипсоид)->элл.Красовского

    // .......................................................................
    // Эллипсоид Красовского, радианы

    GlobalVarLn.LatKrR_YS1_comm = (GlobalVarLn.LatKrG_YS1_comm * Math.PI) / 180;
    GlobalVarLn.LongKrR_YS1_comm = (GlobalVarLn.LongKrG_YS1_comm * Math.PI) / 180;
    // .......................................................................
    // Эллипсоид Красовского, grad,min,sec
    // dd.ddddd -> DD MM SS

    // Широта
    objClassMap5_ed.f_Grad_GMS
      (
        // Входные параметры (grad)
        GlobalVarLn.LatKrG_YS1_comm,

        // Выходные параметры 
        ref GlobalVarLn.Lat_Grad_YS1_comm,
        ref GlobalVarLn.Lat_Min_YS1_comm,
        ref GlobalVarLn.Lat_Sec_YS1_comm
      );

    // Долгота
    objClassMap5_ed.f_Grad_GMS
      (
        // Входные параметры (grad)
        GlobalVarLn.LongKrG_YS1_comm,

        // Выходные параметры 
        ref GlobalVarLn.Long_Grad_YS1_comm,
        ref GlobalVarLn.Long_Min_YS1_comm,
        ref GlobalVarLn.Long_Sec_YS1_comm
      );
    // .......................................................................

    // SK42(элл.)->Крюгер --------------------------------------------------
    // Преобразование геодезических координат (широта, долгота, высота) 
    // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
    // проекции Гаусса-Крюгера
    // Входные параметры -> !!!grad

    objClassMap6_ed.f_SK42_Krug
           (
        // Входные параметры (!!! grad)
        // !!! эллипсоид Красовского
               GlobalVarLn.LatKrG_YS1_comm,   // широта
               GlobalVarLn.LongKrG_YS1_comm,  // долгота

               // Выходные параметры (km)
               ref GlobalVarLn.XYS142_comm,
               ref GlobalVarLn.YYS142_comm
           );

    // km->m
    GlobalVarLn.XYS142_comm = GlobalVarLn.XYS142_comm * 1000;
    GlobalVarLn.YYS142_comm = GlobalVarLn.YYS142_comm * 1000;
    // ---------------------------------------------------- SK42(элл.)->Крюгер

    // .......................................................................
    // Отобразить координаты в окошках

    OtobrSP2();
    // .......................................................................

    // *********************************************************************** SP2

 */





    // 19_11_2018
    // PU ************************************************************************

    // ......................................................................
    // !!! реальные координаты на местности карты в м (Plane)
//PU:
    GlobalVarLn.XPoint2_Sost = (double)iniRW.get_X_PU();
    GlobalVarLn.YPoint2_Sost = (double)iniRW.get_Y_PU();
    GlobalVarLn.indz3_Sost = (int)iniRW.get_Sign_PU();

    if ((GlobalVarLn.XPoint2_Sost == 0) || (GlobalVarLn.YPoint2_Sost == 0))
    {
        MessageBox.Show("Нет координат ПУ");
        return;
    }

    xtmp_ed = GlobalVarLn.XPoint2_Sost;
    ytmp_ed = GlobalVarLn.YPoint2_Sost;
    objLF.X_m = xtmp_ed;
    objLF.Y_m = ytmp_ed;
    // ......................................................................
    // H

    GlobalVarLn.axMapPointGlobalAdd.SetPoint(xtmp_ed, ytmp_ed);
    GlobalVarLn.HPoint2_Sost = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
    htmp_ed = GlobalVarLn.HPoint2_Sost;
    GlobalVarLn.objFormSostG.tbPt2Height.Text = Convert.ToString(htmp_ed);
    objLF.H_m = htmp_ed;
    // ......................................................................
    // sign

    objLF.indzn = GlobalVarLn.indz3_Sost;
    // ......................................................................
    // List

    //GlobalVarLn.list1_Sost.Add(objLF);
    GlobalVarLn.list1_Sost[2] = objLF;
    // ......................................................................
    // Draw PU

    ClassMap.f_DrawSPXY11(
                  xtmp_ed,  // m на местности
                  ytmp_ed,
           (Bitmap)GlobalVarLn.objFormSostG.imageList3.Images[GlobalVarLn.list1_Sost[2].indzn]
                 );
    // ......................................................................
    GlobalVarLn.flCoordYS2_Sost = 1; // PU выбран
    // ......................................................................
    // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
    // !!! Выход функции(rad) идет на место входных переменных

    mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

    // rad(WGS84)->grad(WGS84)
    xtmp1_ed = (xtmp_ed * 180) / Math.PI;
    ytmp1_ed = (ytmp_ed * 180) / Math.PI;

    //333
    GlobalVarLn.BWGS84_3_sost = xtmp1_ed;
    GlobalVarLn.LWGS84_3_sost = ytmp1_ed;

    // .......................................................................

    // WGS84(эллипсоид)->элл.Красовского ----------------------------------------------
    // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
    // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
    // Входные параметры -> град,km
    // Перевод в рад - внутри функции

    // dLong ..................................................................
    // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
    // (преобразования Молоденского), угл.сек

    objClassMap7_ed.f_dLong
        (
            // Входные параметры (град,км)
            xtmp1_ed,   // широта
            ytmp1_ed,  // долгота
            0,     // высота

            // DATUM,m
            GlobalVarLn.dXdat_comm,
            GlobalVarLn.dYdat_comm,
            GlobalVarLn.dZdat_comm,

            ref GlobalVarLn.dLong_comm   // приращение по долготе, угл.сек
        );
    // ................................................................ dLong

    // dLat .................................................................
    // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
    // (преобразования Молоденского), угл.сек

    objClassMap7_ed.f_dLat
        (
            // Входные параметры (град,км)
            xtmp1_ed,   // широта
            ytmp1_ed,  // долгота
            0,     // высота

            // DATUM,m
            GlobalVarLn.dXdat_comm,
            GlobalVarLn.dYdat_comm,
            GlobalVarLn.dZdat_comm,

            ref GlobalVarLn.dLat_comm        // приращение по долготе, угл.сек
        );
    // ................................................................. dLat

    // Lat,Long .............................................................
    // Преобразования широты и долготы при пересчете WGS84->SK42

    objClassMap7_ed.f_WGS84_SK42_Lat_Long
           (
               // Входные параметры (град,км)
               xtmp1_ed,   // широта
               ytmp1_ed,  // долгота
               0,     // высота
               GlobalVarLn.dLat_comm,       // приращение по долготе, угл.сек
               GlobalVarLn.dLong_comm,      // приращение по долготе, угл.сек

               // Выходные параметры (grad)
               ref GlobalVarLn.LatKrG_YS2_comm,   // широта
               ref GlobalVarLn.LongKrG_YS2_comm   // долгота
           );
    // ............................................................ Lat,Long

    // ----------------------------------- WGS84(эллипсоид)->элл.Красовского

    // .......................................................................
    // Эллипсоид Красовского, радианы

    GlobalVarLn.LatKrR_YS2_comm = (GlobalVarLn.LatKrG_YS2_comm * Math.PI) / 180;
    GlobalVarLn.LongKrR_YS2_comm = (GlobalVarLn.LongKrG_YS2_comm * Math.PI) / 180;
    // .......................................................................
    // Эллипсоид Красовского, grad,min,sec
    // dd.ddddd -> DD MM SS

    // Широта
    objClassMap8_ed.f_Grad_GMS
      (
        // Входные параметры (grad)
        GlobalVarLn.LatKrG_YS2_comm,

        // Выходные параметры 
        ref GlobalVarLn.Lat_Grad_YS2_comm,
        ref GlobalVarLn.Lat_Min_YS2_comm,
        ref GlobalVarLn.Lat_Sec_YS2_comm
      );

    // Долгота
    objClassMap8_ed.f_Grad_GMS
      (
        // Входные параметры (grad)
        GlobalVarLn.LongKrG_YS2_comm,

        // Выходные параметры 
        ref GlobalVarLn.Long_Grad_YS2_comm,
        ref GlobalVarLn.Long_Min_YS2_comm,
        ref GlobalVarLn.Long_Sec_YS2_comm
      );
    // .......................................................................

    // SK42(элл.)->Крюгер --------------------------------------------------
    // Преобразование геодезических координат (широта, долгота, высота) 
    // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
    // проекции Гаусса-Крюгера
    // Входные параметры -> !!!grad

    objClassMap9_ed.f_SK42_Krug
           (
        // Входные параметры (!!! grad)
        // !!! эллипсоид Красовского
               GlobalVarLn.LatKrG_YS2_comm,   // широта
               GlobalVarLn.LongKrG_YS2_comm,  // долгота

               // Выходные параметры (km)
               ref GlobalVarLn.XYS242_comm,
               ref GlobalVarLn.YYS242_comm
           );

    // km->m
    GlobalVarLn.XYS242_comm = GlobalVarLn.XYS242_comm * 1000;
    GlobalVarLn.YYS242_comm = GlobalVarLn.YYS242_comm * 1000;
    // ---------------------------------------------------- SK42(элл.)->Крюгер

    // .......................................................................
    // Отобразить координаты в окошках

    OtobrPU();
    // .......................................................................

    // ************************************************************************ PU


} // f_LoadSostIni
// ********************************************************************************

// ************************************************************************
// функция отображения координат SP1
// ************************************************************************
public static void OtobrSP1()
{
        long ichislo;
        double dchislo;
        ichislo = 0;
        dchislo = 0;

        //333
        // Метры на местности
        //ichislo = (long)(GlobalVarLn.XCenter_Sost);
        //GlobalVarLn.objFormSostG.tbXRect.Text = Convert.ToString(ichislo);
        //ichislo = (long)(GlobalVarLn.YCenter_Sost);
        //GlobalVarLn.objFormSostG.tbYRect.Text = Convert.ToString(ichislo);

        ichislo = (long)(GlobalVarLn.BWGS84_1_sost * 1000000);
        dchislo = ((double)ichislo) / 1000000;
        GlobalVarLn.objFormSostG.tbXRect.Text = Convert.ToString(dchislo);
        ichislo = (long)(GlobalVarLn.LWGS84_1_sost * 1000000);
        dchislo = ((double)ichislo) / 1000000;
        GlobalVarLn.objFormSostG.tbYRect.Text = Convert.ToString(dchislo);


        // Метры 1942 года
        ichislo = (long)(GlobalVarLn.XSP42_comm);
        GlobalVarLn.objFormSostG.tbXRect42.Text = Convert.ToString(ichislo);
        ichislo = (long)(GlobalVarLn.YSP42_comm);
        GlobalVarLn.objFormSostG.tbYRect42.Text = Convert.ToString(ichislo);

        // Радианы (Красовский)
        ichislo = (long)(GlobalVarLn.LatKrR_comm * 1000000);
        dchislo = ((double)ichislo) / 1000000;
        GlobalVarLn.objFormSostG.tbBRad.Text = Convert.ToString(dchislo);   // X, карта
        ichislo = (long)(GlobalVarLn.LongKrR_comm * 1000000);
        dchislo = ((double)ichislo) / 1000000;
        GlobalVarLn.objFormSostG.tbLRad.Text = Convert.ToString(dchislo);   // X, карта

        // Градусы (Красовский)
        ichislo = (long)(GlobalVarLn.LatKrG_comm * 100000000);
        dchislo = ((double)ichislo) / 100000000;
        GlobalVarLn.objFormSostG.tbBMin1.Text = Convert.ToString(dchislo);
        ichislo = (long)(GlobalVarLn.LongKrG_comm * 100000000);
        dchislo = ((double)ichislo) / 100000000;
        GlobalVarLn.objFormSostG.tbLMin1.Text = Convert.ToString(dchislo);

        // Градусы,мин,сек (Красовский)
        GlobalVarLn.objFormSostG.tbBDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_comm);
        GlobalVarLn.objFormSostG.tbBMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_comm);
        ichislo = (long)(GlobalVarLn.Lat_Sec_comm);
        GlobalVarLn.objFormSostG.tbBSec.Text = Convert.ToString(ichislo);
        GlobalVarLn.objFormSostG.tbLDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_comm);
        GlobalVarLn.objFormSostG.tbLMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_comm);
        ichislo = (long)(GlobalVarLn.Long_Sec_comm);
        GlobalVarLn.objFormSostG.tbLSec.Text = Convert.ToString(ichislo);


} // OtobrSP1
// ************************************************************************

// ************************************************************************
// функция отображения координат SP2
// ************************************************************************
public static void OtobrSP2()
{
    long ichislo;
    double dchislo;
    ichislo = 0;
    dchislo = 0;

    //333
  //ichislo = (long)(GlobalVarLn.XPoint1_Sost);
   // GlobalVarLn.objFormSostG.tbPt1XRect.Text = Convert.ToString(ichislo);
   // ichislo = (long)(GlobalVarLn.YPoint1_Sost);
   // GlobalVarLn.objFormSostG.tbPt1YRect.Text = Convert.ToString(ichislo);

    ichislo = (long)(GlobalVarLn.BWGS84_2_sost * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormSostG.tbPt1XRect.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LWGS84_2_sost * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormSostG.tbPt1YRect.Text = Convert.ToString(dchislo);


    // Метры 1942 года
    ichislo = (long)(GlobalVarLn.XYS142_comm);
    GlobalVarLn.objFormSostG.tbPt1XRect42.Text = Convert.ToString(ichislo);
    ichislo = (long)(GlobalVarLn.YYS142_comm);
    GlobalVarLn.objFormSostG.tbPt1YRect42.Text = Convert.ToString(ichislo);

    // Радианы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrR_YS1_comm * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormSostG.tbPt1BRad.Text = Convert.ToString(dchislo);   // X, карта
    ichislo = (long)(GlobalVarLn.LongKrR_YS1_comm * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormSostG.tbPt1LRad.Text = Convert.ToString(dchislo);   // X, карта

    // Градусы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrG_YS1_comm * 100000000);
    dchislo = ((double)ichislo) / 100000000;
    GlobalVarLn.objFormSostG.tbPt1BMin1.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LongKrG_YS1_comm * 100000000);
    dchislo = ((double)ichislo) / 100000000;
    GlobalVarLn.objFormSostG.tbPt1LMin1.Text = Convert.ToString(dchislo);

    // Градусы,мин,сек (Красовский)
    GlobalVarLn.objFormSostG.tbPt1BDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_YS1_comm);
    GlobalVarLn.objFormSostG.tbPt1BMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_YS1_comm);
    ichislo = (long)(GlobalVarLn.Lat_Sec_YS1_comm);
    GlobalVarLn.objFormSostG.tbPt1BSec.Text = Convert.ToString(ichislo);
    GlobalVarLn.objFormSostG.tbPt1LDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_YS1_comm);
    GlobalVarLn.objFormSostG.tbPt1LMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_YS1_comm);
    ichislo = (long)(GlobalVarLn.Long_Sec_YS1_comm);
    GlobalVarLn.objFormSostG.tbPt1LSec.Text = Convert.ToString(ichislo);

} // OtobrSP2
// ************************************************************************

// ************************************************************************
// функция отображения координат PU
// ************************************************************************
public static void OtobrPU()
{
    long ichislo;
    double dchislo;
    ichislo = 0;
    dchislo = 0;

    //333
    // Метры на местности
    //ichislo = (long)(GlobalVarLn.XPoint2_Sost);
    //GlobalVarLn.objFormSostG.tbPt2XRect.Text = Convert.ToString(ichislo);
    //ichislo = (long)(GlobalVarLn.YPoint2_Sost);
    //GlobalVarLn.objFormSostG.tbPt2YRect.Text = Convert.ToString(ichislo);

    ichislo = (long)(GlobalVarLn.BWGS84_3_sost * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormSostG.tbPt2XRect.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LWGS84_3_sost * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormSostG.tbPt2YRect.Text = Convert.ToString(dchislo);

    // Метры 1942 года
    ichislo = (long)(GlobalVarLn.XYS242_comm);
    GlobalVarLn.objFormSostG.tbPt2XRect42.Text = Convert.ToString(ichislo);
    ichislo = (long)(GlobalVarLn.YYS242_comm);
    GlobalVarLn.objFormSostG.tbPt2YRect42.Text = Convert.ToString(ichislo);

    // Радианы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrR_YS2_comm * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormSostG.tbPt2BRad.Text = Convert.ToString(dchislo);   // X, карта
    ichislo = (long)(GlobalVarLn.LongKrR_YS2_comm * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormSostG.tbPt2LRad.Text = Convert.ToString(dchislo);   // X, карта

    // Градусы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrG_YS2_comm * 100000000);
    dchislo = ((double)ichislo) / 100000000;
    GlobalVarLn.objFormSostG.tbPt2BMin1.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LongKrG_YS2_comm * 100000000);
    dchislo = ((double)ichislo) / 100000000;
    GlobalVarLn.objFormSostG.tbPt2LMin1.Text = Convert.ToString(dchislo);

    // Градусы,мин,сек (Красовский)
    GlobalVarLn.objFormSostG.tbPt1BDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_YS1_comm);
    GlobalVarLn.objFormSostG.tbPt1BMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_YS1_comm);
    ichislo = (long)(GlobalVarLn.Lat_Sec_YS1_comm);
    GlobalVarLn.objFormSostG.tbPt1BSec.Text = Convert.ToString(ichislo);
    GlobalVarLn.objFormSostG.tbPt1LDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_YS1_comm);
    GlobalVarLn.objFormSostG.tbPt1LMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_YS1_comm);
    ichislo = (long)(GlobalVarLn.Long_Sec_YS1_comm);
    GlobalVarLn.objFormSostG.tbPt1LSec.Text = Convert.ToString(ichislo);

    GlobalVarLn.objFormSostG.tbPt2BDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_YS2_comm);
    GlobalVarLn.objFormSostG.tbPt2BMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_YS2_comm);
    ichislo = (long)(GlobalVarLn.Lat_Sec_YS2_comm);
    GlobalVarLn.objFormSostG.tbPt2BSec.Text = Convert.ToString(ichislo);
    GlobalVarLn.objFormSostG.tbPt2LDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_YS2_comm);
    GlobalVarLn.objFormSostG.tbPt2LMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_YS2_comm);
    ichislo = (long)(GlobalVarLn.Long_Sec_YS2_comm);
    GlobalVarLn.objFormSostG.tbPt2LSec.Text = Convert.ToString(ichislo);

} // OtobrPU
// ************************************************************************

// ************************************************************************
// функция отображения координат SP (Suppression)
// ************************************************************************
public static void OtobrSP_Supression()
{
    long ichislo;
    double dchislo;
    ichislo = 0;
    dchislo = 0;

    //333
    // Метры на местности
    //ichislo = (long)(GlobalVarLn.XCenter_sup);
    //GlobalVarLn.objFormSuppressionG.tbXRect.Text = Convert.ToString(ichislo);
    //ichislo = (long)(GlobalVarLn.YCenter_sup);
    //GlobalVarLn.objFormSuppressionG.tbYRect.Text = Convert.ToString(ichislo);

    ichislo = (long)(GlobalVarLn.BWGS84_1_sup * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormSuppressionG.tbXRect.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LWGS84_1_sup * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormSuppressionG.tbYRect.Text = Convert.ToString(dchislo);

    // Метры 1942 года
    ichislo = (long)(GlobalVarLn.XSP42_sup);
    GlobalVarLn.objFormSuppressionG.tbXRect42.Text = Convert.ToString(ichislo);
    ichislo = (long)(GlobalVarLn.YSP42_sup);
    GlobalVarLn.objFormSuppressionG.tbYRect42.Text = Convert.ToString(ichislo);

    // Радианы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrR_sup * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormSuppressionG.tbBRad.Text = Convert.ToString(dchislo);   // X, карта
    ichislo = (long)(GlobalVarLn.LongKrR_sup * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormSuppressionG.tbLRad.Text = Convert.ToString(dchislo);   // X, карта

    // Градусы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrG_sup * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormSuppressionG.tbBMin1.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LongKrG_sup * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormSuppressionG.tbLMin1.Text = Convert.ToString(dchislo);

    // Градусы,мин,сек (Красовский)
    GlobalVarLn.objFormSuppressionG.tbBDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_sup);
    GlobalVarLn.objFormSuppressionG.tbBMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_sup);
    ichislo = (long)(GlobalVarLn.Lat_Sec_sup);
    GlobalVarLn.objFormSuppressionG.tbBSec.Text = Convert.ToString(ichislo);
    GlobalVarLn.objFormSuppressionG.tbLDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_sup);
    GlobalVarLn.objFormSuppressionG.tbLMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_sup);
    ichislo = (long)(GlobalVarLn.Long_Sec_sup);
    GlobalVarLn.objFormSuppressionG.tbLSec.Text = Convert.ToString(ichislo);

} // OtobrSP_Supression
// ************************************************************************

// ************************************************************************
// функция отображения координат OP (Suppression)
// ************************************************************************
public static void OtobrOP_Suppression()
{
    long ichislo;
    double dchislo;
    ichislo = 0;
    dchislo = 0;

    //333
    // Метры на местности
    //ichislo = (long)(GlobalVarLn.XPoint1_sup);
    //GlobalVarLn.objFormSuppressionG.tbPt1XRect.Text = Convert.ToString(ichislo);
    //ichislo = (long)(GlobalVarLn.YPoint1_sup);
    //GlobalVarLn.objFormSuppressionG.tbPt1YRect.Text = Convert.ToString(ichislo);

    ichislo = (long)(GlobalVarLn.BWGS84_2_sup * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormSuppressionG.tbPt1XRect.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LWGS84_2_sup * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormSuppressionG.tbPt1YRect.Text = Convert.ToString(dchislo);

    // Метры 1942 года
    ichislo = (long)(GlobalVarLn.XYS142_sup);
    GlobalVarLn.objFormSuppressionG.tbPt1XRect42.Text = Convert.ToString(ichislo);
    ichislo = (long)(GlobalVarLn.YYS142_sup);
    GlobalVarLn.objFormSuppressionG.tbPt1YRect42.Text = Convert.ToString(ichislo);

    // Радианы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrR_YS1_sup * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormSuppressionG.tbPt1BRad.Text = Convert.ToString(dchislo);   // X, карта
    ichislo = (long)(GlobalVarLn.LongKrR_YS1_sup * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormSuppressionG.tbPt1LRad.Text = Convert.ToString(dchislo);   // X, карта

    // Градусы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrG_YS1_sup * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormSuppressionG.tbPt1BMin1.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LongKrG_YS1_sup * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormSuppressionG.tbPt1LMin1.Text = Convert.ToString(dchislo);

    // Градусы,мин,сек (Красовский)
    GlobalVarLn.objFormSuppressionG.tbPt1BDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_YS1_sup);
    GlobalVarLn.objFormSuppressionG.tbPt1BMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_YS1_sup);
    ichislo = (long)(GlobalVarLn.Lat_Sec_YS1_sup);
    GlobalVarLn.objFormSuppressionG.tbPt1BSec.Text = Convert.ToString(ichislo);
    GlobalVarLn.objFormSuppressionG.tbPt1LDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_YS1_sup);
    GlobalVarLn.objFormSuppressionG.tbPt1LMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_YS1_sup);
    ichislo = (long)(GlobalVarLn.Long_Sec_YS1_sup);
    GlobalVarLn.objFormSuppressionG.tbPt1LSec.Text = Convert.ToString(ichislo);

} // OtobrOP_Suppression
// ************************************************************************

// ************************************************************************
// функция отображения координат Pel1 (Bearing)
// ************************************************************************
public static void OtobrPel1_Bearing()
{
    long ichislo;
    double dchislo;
    ichislo = 0;
    dchislo = 0;

    //333
    // Метры на местности
    //ichislo = (long)(GlobalVarLn.XCenter_PelMain);
    //GlobalVarLn.objFormBearingG.tbXRect.Text = Convert.ToString(ichislo);
    //ichislo = (long)(GlobalVarLn.YCenter_PelMain);
    //GlobalVarLn.objFormBearingG.tbYRect.Text = Convert.ToString(ichislo);

    ichislo = (long)(GlobalVarLn.BWGS84_1_bear * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormBearingG.tbXRect.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LWGS84_1_bear * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormBearingG.tbYRect.Text = Convert.ToString(dchislo);


    // Метры 1942 года
    ichislo = (long)(GlobalVarLn.XSP42_br);
    GlobalVarLn.objFormBearingG.tbXRect42.Text = Convert.ToString(ichislo);
    ichislo = (long)(GlobalVarLn.YSP42_br);
    GlobalVarLn.objFormBearingG.tbYRect42.Text = Convert.ToString(ichislo);

    // Радианы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrR_br * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormBearingG.tbBRad.Text = Convert.ToString(dchislo);   // X, карта
    ichislo = (long)(GlobalVarLn.LongKrR_br * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormBearingG.tbLRad.Text = Convert.ToString(dchislo);   // X, карта

    // Градусы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrG_br * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormBearingG.tbBMin1.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LongKrG_br * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormBearingG.tbLMin1.Text = Convert.ToString(dchislo);

    // Градусы,мин,сек (Красовский)
    GlobalVarLn.objFormBearingG.tbBDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_br);
    GlobalVarLn.objFormBearingG.tbBMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_br);
    ichislo = (long)(GlobalVarLn.Lat_Sec_br);
    GlobalVarLn.objFormBearingG.tbBSec.Text = Convert.ToString(ichislo);
    GlobalVarLn.objFormBearingG.tbLDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_br);
    GlobalVarLn.objFormBearingG.tbLMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_br);
    ichislo = (long)(GlobalVarLn.Long_Sec_br);
    GlobalVarLn.objFormBearingG.tbLSec.Text = Convert.ToString(ichislo);

} // OtobrPel1_Bearing
// ************************************************************************

// ************************************************************************
// функция отображения координат Pel2 (Bearing)
// ************************************************************************
public static void OtobrPel2_Bearing()
{
    long ichislo;
    double dchislo;
    ichislo = 0;
    dchislo = 0;

    //333
    // Метры на местности
    //ichislo = (long)(GlobalVarLn.XPoint1_PelMain);
    //GlobalVarLn.objFormBearingG.tbPt1XRect.Text = Convert.ToString(ichislo);
    //ichislo = (long)(GlobalVarLn.YPoint1_PelMain);
    //GlobalVarLn.objFormBearingG.tbPt1YRect.Text = Convert.ToString(ichislo);

    ichislo = (long)(GlobalVarLn.BWGS84_2_bear * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormBearingG.tbPt1XRect.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LWGS84_2_bear * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormBearingG.tbPt1YRect.Text = Convert.ToString(dchislo);


    // Метры 1942 года
    ichislo = (long)(GlobalVarLn.XYS142_br);
    GlobalVarLn.objFormBearingG.tbPt1XRect42.Text = Convert.ToString(ichislo);
    ichislo = (long)(GlobalVarLn.YYS142_br);
    GlobalVarLn.objFormBearingG.tbPt1YRect42.Text = Convert.ToString(ichislo);

    // Радианы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrR_YS1_br * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormBearingG.tbPt1BRad.Text = Convert.ToString(dchislo);   // X, карта
    ichislo = (long)(GlobalVarLn.LongKrR_YS1_br * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormBearingG.tbPt1LRad.Text = Convert.ToString(dchislo);   // X, карта

    // Градусы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrG_YS1_br * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormBearingG.tbPt1BMin1.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LongKrG_YS1_br * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormBearingG.tbPt1LMin1.Text = Convert.ToString(dchislo);

    // Градусы,мин,сек (Красовский)
    GlobalVarLn.objFormBearingG.tbPt1BDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_YS1_br);
    GlobalVarLn.objFormBearingG.tbPt1BMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_YS1_br);
    ichislo = (long)(GlobalVarLn.Lat_Sec_YS1_br);
    GlobalVarLn.objFormBearingG.tbPt1BSec.Text = Convert.ToString(ichislo);
    GlobalVarLn.objFormBearingG.tbPt1LDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_YS1_br);
    GlobalVarLn.objFormBearingG.tbPt1LMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_YS1_br);
    ichislo = (long)(GlobalVarLn.Long_Sec_YS1_br);
    GlobalVarLn.objFormBearingG.tbPt1LSec.Text = Convert.ToString(ichislo);

} // OtobrPel2_Bearing
// ************************************************************************

// ************************************************************************
// функция отображения координат SP (Form2)
// ************************************************************************
public static void OtobrSP_Form2()
{
    long ichislo;
    double dchislo;
    ichislo = 0;
    dchislo = 0;

    //333
    // Метры на местности
    //ichislo = (long)(GlobalVarLn.XCenter_comm);
    //GlobalVarLn.ObjCommPowerAvailG.tbXRect.Text = Convert.ToString(ichislo);
    //ichislo = (long)(GlobalVarLn.YCenter_comm);
    //GlobalVarLn.ObjCommPowerAvailG.tbYRect.Text = Convert.ToString(ichislo);

    ichislo = (long)(GlobalVarLn.BWGS84_1_comm2 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjCommPowerAvailG.tbXRect.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LWGS84_1_comm2 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjCommPowerAvailG.tbYRect.Text = Convert.ToString(dchislo);


    // Метры 1942 года
    ichislo = (long)(GlobalVarLn.XSP42_comm1);
    GlobalVarLn.ObjCommPowerAvailG.tbXRect42.Text = Convert.ToString(ichislo);
    ichislo = (long)(GlobalVarLn.YSP42_comm1);
    GlobalVarLn.ObjCommPowerAvailG.tbYRect42.Text = Convert.ToString(ichislo);

    // Радианы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrR_comm1 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjCommPowerAvailG.tbBRad.Text = Convert.ToString(dchislo);   // X, карта
    ichislo = (long)(GlobalVarLn.LongKrR_comm1 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjCommPowerAvailG.tbLRad.Text = Convert.ToString(dchislo);   // X, карта

    // Градусы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrG_comm1 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjCommPowerAvailG.tbBMin1.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LongKrG_comm1 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjCommPowerAvailG.tbLMin1.Text = Convert.ToString(dchislo);

    // Градусы,мин,сек (Красовский)
    GlobalVarLn.ObjCommPowerAvailG.tbBDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_comm1);
    GlobalVarLn.ObjCommPowerAvailG.tbBMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_comm1);
    ichislo = (long)(GlobalVarLn.Lat_Sec_comm1);
    GlobalVarLn.ObjCommPowerAvailG.tbBSec.Text = Convert.ToString(ichislo);
    GlobalVarLn.ObjCommPowerAvailG.tbLDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_comm1);
    GlobalVarLn.ObjCommPowerAvailG.tbLMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_comm1);
    ichislo = (long)(GlobalVarLn.Long_Sec_comm1);
    GlobalVarLn.ObjCommPowerAvailG.tbLSec.Text = Convert.ToString(ichislo);

} // OtobrSP_Form2
// ************************************************************************

// ************************************************************************
// функция отображения координат YS1 (Form2)
// ************************************************************************
public static void OtobrYS1_Form2()
{
    long ichislo;
    double dchislo;
    ichislo = 0;
    dchislo = 0;

    //333
    // Метры на местности
    //ichislo = (long)(GlobalVarLn.XPoint1_comm);
    //GlobalVarLn.ObjCommPowerAvailG.tbPt1XRect.Text = Convert.ToString(ichislo);
    //ichislo = (long)(GlobalVarLn.YPoint1_comm);
    //GlobalVarLn.ObjCommPowerAvailG.tbPt1YRect.Text = Convert.ToString(ichislo);

    ichislo = (long)(GlobalVarLn.BWGS84_2_comm2 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjCommPowerAvailG.tbPt1XRect.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LWGS84_2_comm2 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjCommPowerAvailG.tbPt1YRect.Text = Convert.ToString(dchislo);


    // Метры 1942 года
    ichislo = (long)(GlobalVarLn.XYS142_comm1);
    GlobalVarLn.ObjCommPowerAvailG.tbPt1XRect42.Text = Convert.ToString(ichislo);
    ichislo = (long)(GlobalVarLn.YYS142_comm1);
    GlobalVarLn.ObjCommPowerAvailG.tbPt1YRect42.Text = Convert.ToString(ichislo);

    // Радианы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrR_YS1_comm1 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjCommPowerAvailG.tbPt1BRad.Text = Convert.ToString(dchislo);   // X, карта
    ichislo = (long)(GlobalVarLn.LongKrR_YS1_comm1 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjCommPowerAvailG.tbPt1LRad.Text = Convert.ToString(dchislo);   // X, карта

    // Градусы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrG_YS1_comm1 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjCommPowerAvailG.tbPt1BMin1.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LongKrG_YS1_comm1 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjCommPowerAvailG.tbPt1LMin1.Text = Convert.ToString(dchislo);

    // Градусы,мин,сек (Красовский)
    GlobalVarLn.ObjCommPowerAvailG.tbPt1BDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_YS1_comm1);
    GlobalVarLn.ObjCommPowerAvailG.tbPt1BMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_YS1_comm1);
    ichislo = (long)(GlobalVarLn.Lat_Sec_YS1_comm1);
    GlobalVarLn.ObjCommPowerAvailG.tbPt1BSec.Text = Convert.ToString(ichislo);
    GlobalVarLn.ObjCommPowerAvailG.tbPt1LDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_YS1_comm1);
    GlobalVarLn.ObjCommPowerAvailG.tbPt1LMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_YS1_comm1);
    ichislo = (long)(GlobalVarLn.Long_Sec_YS1_comm1);
    GlobalVarLn.ObjCommPowerAvailG.tbPt1LSec.Text = Convert.ToString(ichislo);

} // OtobrYS1_Form2
// ************************************************************************

// ************************************************************************
// функция отображения координат YS2 (Form2)
// ************************************************************************
public static void OtobrYS2_Form2()
{
    long ichislo;
    double dchislo;
    ichislo = 0;
    dchislo = 0;

    //333
    // Метры на местности
    //ichislo = (long)(GlobalVarLn.XPoint2_comm);
    //GlobalVarLn.ObjCommPowerAvailG.tbPt2XRect.Text = Convert.ToString(ichislo);
    //ichislo = (long)(GlobalVarLn.YPoint2_comm);
    //GlobalVarLn.ObjCommPowerAvailG.tbPt2YRect.Text = Convert.ToString(ichislo);

    ichislo = (long)(GlobalVarLn.BWGS84_3_comm2 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjCommPowerAvailG.tbPt2XRect.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LWGS84_3_comm2 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjCommPowerAvailG.tbPt2YRect.Text = Convert.ToString(dchislo);


    // Метры 1942 года
    ichislo = (long)(GlobalVarLn.XYS242_comm1);
    GlobalVarLn.ObjCommPowerAvailG.tbPt2XRect42.Text = Convert.ToString(ichislo);
    ichislo = (long)(GlobalVarLn.YYS242_comm1);
    GlobalVarLn.ObjCommPowerAvailG.tbPt2YRect42.Text = Convert.ToString(ichislo);

    // Радианы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrR_YS2_comm1 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjCommPowerAvailG.tbPt2BRad.Text = Convert.ToString(dchislo);   // X, карта
    ichislo = (long)(GlobalVarLn.LongKrR_YS2_comm1 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjCommPowerAvailG.tbPt2LRad.Text = Convert.ToString(dchislo);   // X, карта

    // Градусы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrG_YS2_comm1 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjCommPowerAvailG.tbPt2BMin1.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LongKrG_YS2_comm1 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjCommPowerAvailG.tbPt2LMin1.Text = Convert.ToString(dchislo);

    // Градусы,мин,сек (Красовский)
    GlobalVarLn.ObjCommPowerAvailG.tbPt2BDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_YS2_comm1);
    GlobalVarLn.ObjCommPowerAvailG.tbPt2BMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_YS2_comm1);
    ichislo = (long)(GlobalVarLn.Lat_Sec_YS2_comm1);
    GlobalVarLn.ObjCommPowerAvailG.tbPt2BSec.Text = Convert.ToString(ichislo);
    GlobalVarLn.ObjCommPowerAvailG.tbPt2LDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_YS2_comm1);
    GlobalVarLn.ObjCommPowerAvailG.tbPt2LMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_YS2_comm1);
    ichislo = (long)(GlobalVarLn.Long_Sec_YS2_comm1);
    GlobalVarLn.ObjCommPowerAvailG.tbPt2LSec.Text = Convert.ToString(ichislo);

} // OtobrYS2_Form2
// ************************************************************************

// ************************************************************************
// Очистка состава комплекса
// ************************************************************************
// GPSSPPU
// 13_09_2018

public static void ClearSost()
{

    // SP1
    GlobalVarLn.objFormSostG.tbXRect.Text = "";
    GlobalVarLn.objFormSostG.tbYRect.Text = "";
    GlobalVarLn.objFormSostG.tbXRect42.Text = "";
    GlobalVarLn.objFormSostG.tbYRect42.Text = "";
    GlobalVarLn.objFormSostG.tbBRad.Text = "";
    GlobalVarLn.objFormSostG.tbLRad.Text = "";
    GlobalVarLn.objFormSostG.tbBMin1.Text = "";
    GlobalVarLn.objFormSostG.tbLMin1.Text = "";
    GlobalVarLn.objFormSostG.tbBDeg2.Text = "";
    GlobalVarLn.objFormSostG.tbBMin2.Text = "";
    GlobalVarLn.objFormSostG.tbBSec.Text = "";
    GlobalVarLn.objFormSostG.tbLDeg2.Text = "";
    GlobalVarLn.objFormSostG.tbLMin2.Text = "";
    GlobalVarLn.objFormSostG.tbLSec.Text = "";

    // SP2
    GlobalVarLn.objFormSostG.tbPt1XRect.Text = "";
    GlobalVarLn.objFormSostG.tbPt1YRect.Text = "";
    GlobalVarLn.objFormSostG.tbPt1XRect42.Text = "";
    GlobalVarLn.objFormSostG.tbPt1YRect42.Text = "";
    GlobalVarLn.objFormSostG.tbPt1BRad.Text = "";
    GlobalVarLn.objFormSostG.tbPt1LRad.Text = "";
    GlobalVarLn.objFormSostG.tbPt1BMin1.Text = "";
    GlobalVarLn.objFormSostG.tbPt1LMin1.Text = "";
    GlobalVarLn.objFormSostG.tbPt1BDeg2.Text = "";
    GlobalVarLn.objFormSostG.tbPt1BMin2.Text = "";
    GlobalVarLn.objFormSostG.tbPt1BSec.Text = "";
    GlobalVarLn.objFormSostG.tbPt1LDeg2.Text = "";
    GlobalVarLn.objFormSostG.tbPt1LMin2.Text = "";
    GlobalVarLn.objFormSostG.tbPt1LSec.Text = "";

    // PU
    GlobalVarLn.objFormSostG.tbPt2XRect.Text = "";
    GlobalVarLn.objFormSostG.tbPt2YRect.Text = "";
    GlobalVarLn.objFormSostG.tbPt2XRect42.Text = "";
    GlobalVarLn.objFormSostG.tbPt2YRect42.Text = "";
    GlobalVarLn.objFormSostG.tbPt2BRad.Text = "";
    GlobalVarLn.objFormSostG.tbPt2LRad.Text = "";
    GlobalVarLn.objFormSostG.tbPt2BMin1.Text = "";
    GlobalVarLn.objFormSostG.tbPt2LMin1.Text = "";
    GlobalVarLn.objFormSostG.tbPt2BDeg2.Text = "";
    GlobalVarLn.objFormSostG.tbPt2BMin2.Text = "";
    GlobalVarLn.objFormSostG.tbPt2BSec.Text = "";
    GlobalVarLn.objFormSostG.tbPt2LDeg2.Text = "";
    GlobalVarLn.objFormSostG.tbPt2LMin2.Text = "";
    GlobalVarLn.objFormSostG.tbPt2LSec.Text = "";
    GlobalVarLn.objFormSostG.tbOwnHeight.Text = "";
    GlobalVarLn.objFormSostG.tbPt1Height.Text = "";
    GlobalVarLn.objFormSostG.tbPt2Height.Text = "";

    // ...................................................................
    // переменные

    GlobalVarLn.flCoordSP_Sost = 0; // =1-> Выбрали СП
    GlobalVarLn.flCoordYS1_Sost = 0;
    GlobalVarLn.flCoordYS2_Sost = 0;
    GlobalVarLn.fl_Sost = 0; // Отрисовка зоны
    GlobalVarLn.list1_Sost.Clear();

        GlobalVarLn.XCenterGRAD_Sost = -1;
        GlobalVarLn.YCenterGRAD_Sost = -1;
        GlobalVarLn.XPoint1GRAD_Sost = -1;
        GlobalVarLn.YPoint1GRAD_Sost = -1;
        GlobalVarLn.XPoint2GRAD_Sost = -1;
        GlobalVarLn.YPoint2GRAD_Sost = -1;
        GlobalVarLn.XCenterGRAD_Dubl_Sost = -1;
        GlobalVarLn.YCenterGRAD_Dubl_Sost = -1;
        GlobalVarLn.XPoint1GRAD_Dubl_Sost = -1;
        GlobalVarLn.YPoint1GRAD_Dubl_Sost = -1;
        GlobalVarLn.XPoint2GRAD_Dubl_Sost = -1;
        GlobalVarLn.YPoint2GRAD_Dubl_Sost = -1;
        GlobalVarLn.flTmpGPS_VKL = false;
        GlobalVarLn.flTmrGPS = 0;

        GlobalVarLn.XCenter_Sost = 0;
        GlobalVarLn.YCenter_Sost = 0;
        GlobalVarLn.HCenter_Sost = 0;
        GlobalVarLn.XPoint1_Sost = 0;
        GlobalVarLn.YPoint1_Sost = 0;
        GlobalVarLn.HPoint1_Sost = 0;
        GlobalVarLn.XPoint2_Sost = 0;
        GlobalVarLn.YPoint2_Sost = 0;
        GlobalVarLn.HPoint2_Sost = 0;


        GlobalVarLn.BWGS84_1_sost = 0;
        GlobalVarLn.LWGS84_1_sost = 0;
        GlobalVarLn.BWGS84_2_sost = 0;
        GlobalVarLn.LWGS84_2_sost = 0;
        GlobalVarLn.BWGS84_3_sost = 0;
        GlobalVarLn.LWGS84_3_sost = 0;
        // ...................................................................
        GlobalVarLn.objFormSostG.chbXY1.Checked = false;

    //GlobalVarLn.indz1_Sost = 0;
    //GlobalVarLn.indz2_Sost = 0;
    //GlobalVarLn.indz3_Sost = 0;
    //GlobalVarLn.objFormSostG.pbSP.Image = GlobalVarLn.objFormSostG.imageList1.Images[GlobalVarLn.indz1_Sost];
    //GlobalVarLn.objFormSostG.pictureBox1.Image = GlobalVarLn.objFormSostG.imageList2.Images[GlobalVarLn.indz2_Sost];
    //GlobalVarLn.objFormSostG.pictureBox2.Image = GlobalVarLn.objFormSostG.imageList3.Images[GlobalVarLn.indz3_Sost];
    // ----------------------------------------------------------------------
    LF objLF = new LF();

    for (int i = 0; i < GlobalVarLn.Number_Sost; i++)
    {
        objLF.X_m = 0;
        objLF.Y_m = 0;
        objLF.H_m = 0;
        objLF.indzn = 0;
        GlobalVarLn.list1_Sost.Add(objLF);
    }
    // ----------------------------------------------------------------------

    //3101
    // SP1
    GlobalVarLn.objFormSostG.gbOwnRect.Enabled = false;
    GlobalVarLn.objFormSostG.gbOwnRect42.Enabled = false;
    GlobalVarLn.objFormSostG.gbOwnRad.Enabled = false;
    GlobalVarLn.objFormSostG.gbOwnDegMin.Enabled = false;
    GlobalVarLn.objFormSostG.gbOwnDegMinSec.Enabled = false;
    // SP2
    GlobalVarLn.objFormSostG.gbPt1Rect.Enabled = false;
    GlobalVarLn.objFormSostG.gbPt1Rect42.Enabled = false;
    GlobalVarLn.objFormSostG.gbPt1Rad.Enabled = false;
    GlobalVarLn.objFormSostG.gbPt1DegMin.Enabled = false;
    GlobalVarLn.objFormSostG.gbPt1DegMinSec.Enabled = false;
    // PU
    GlobalVarLn.objFormSostG.gbPt2Rect.Enabled = false;
    GlobalVarLn.objFormSostG.gbPt2Rect42.Enabled = false;
    GlobalVarLn.objFormSostG.gbPt2Rad.Enabled = false;
    GlobalVarLn.objFormSostG.gbPt2DegMin.Enabled = false;
    GlobalVarLn.objFormSostG.gbPt2DegMinSec.Enabled = false;

    // Убрать с карты
    GlobalVarLn.axMapScreenGlobal.Repaint();
    // ---------------------------------------------------------------------

} // ClearSost
// ************************************************************************

// ************************************************************************
// Очистка состава комплекса (SP1)
// ************************************************************************
// GPSSPPU
// 13_09_2018

public static void ClearSostSP1()
{

    // SP1
    GlobalVarLn.objFormSostG.tbXRect.Text = "";
    GlobalVarLn.objFormSostG.tbYRect.Text = "";
    GlobalVarLn.objFormSostG.tbXRect42.Text = "";
    GlobalVarLn.objFormSostG.tbYRect42.Text = "";
    GlobalVarLn.objFormSostG.tbBRad.Text = "";
    GlobalVarLn.objFormSostG.tbLRad.Text = "";
    GlobalVarLn.objFormSostG.tbBMin1.Text = "";
    GlobalVarLn.objFormSostG.tbLMin1.Text = "";
    GlobalVarLn.objFormSostG.tbBDeg2.Text = "";
    GlobalVarLn.objFormSostG.tbBMin2.Text = "";
    GlobalVarLn.objFormSostG.tbBSec.Text = "";
    GlobalVarLn.objFormSostG.tbLDeg2.Text = "";
    GlobalVarLn.objFormSostG.tbLMin2.Text = "";
    GlobalVarLn.objFormSostG.tbLSec.Text = "";

/*
    // SP2
    GlobalVarLn.objFormSostG.tbPt1XRect.Text = "";
    GlobalVarLn.objFormSostG.tbPt1YRect.Text = "";
    GlobalVarLn.objFormSostG.tbPt1XRect42.Text = "";
    GlobalVarLn.objFormSostG.tbPt1YRect42.Text = "";
    GlobalVarLn.objFormSostG.tbPt1BRad.Text = "";
    GlobalVarLn.objFormSostG.tbPt1LRad.Text = "";
    GlobalVarLn.objFormSostG.tbPt1BMin1.Text = "";
    GlobalVarLn.objFormSostG.tbPt1LMin1.Text = "";
    GlobalVarLn.objFormSostG.tbPt1BDeg2.Text = "";
    GlobalVarLn.objFormSostG.tbPt1BMin2.Text = "";
    GlobalVarLn.objFormSostG.tbPt1BSec.Text = "";
    GlobalVarLn.objFormSostG.tbPt1LDeg2.Text = "";
    GlobalVarLn.objFormSostG.tbPt1LMin2.Text = "";
    GlobalVarLn.objFormSostG.tbPt1LSec.Text = "";

    // PU
    GlobalVarLn.objFormSostG.tbPt2XRect.Text = "";
    GlobalVarLn.objFormSostG.tbPt2YRect.Text = "";
    GlobalVarLn.objFormSostG.tbPt2XRect42.Text = "";
    GlobalVarLn.objFormSostG.tbPt2YRect42.Text = "";
    GlobalVarLn.objFormSostG.tbPt2BRad.Text = "";
    GlobalVarLn.objFormSostG.tbPt2LRad.Text = "";
    GlobalVarLn.objFormSostG.tbPt2BMin1.Text = "";
    GlobalVarLn.objFormSostG.tbPt2LMin1.Text = "";
    GlobalVarLn.objFormSostG.tbPt2BDeg2.Text = "";
    GlobalVarLn.objFormSostG.tbPt2BMin2.Text = "";
    GlobalVarLn.objFormSostG.tbPt2BSec.Text = "";
    GlobalVarLn.objFormSostG.tbPt2LDeg2.Text = "";
    GlobalVarLn.objFormSostG.tbPt2LMin2.Text = "";
    GlobalVarLn.objFormSostG.tbPt2LSec.Text = "";
 */
    GlobalVarLn.objFormSostG.tbOwnHeight.Text = "";
    //GlobalVarLn.objFormSostG.tbPt1Height.Text = "";
    //GlobalVarLn.objFormSostG.tbPt2Height.Text = "";

    // ...................................................................
    // переменные

    GlobalVarLn.flCoordSP_Sost = 0; // =1-> Выбрали СП
    //GlobalVarLn.flCoordYS1_Sost = 0;
    //GlobalVarLn.flCoordYS2_Sost = 0;
    //GlobalVarLn.fl_Sost = 0; // Отрисовка зоны
    //GlobalVarLn.list1_Sost.Clear();

    GlobalVarLn.XCenterGRAD_Sost = -1;
    GlobalVarLn.YCenterGRAD_Sost = -1;
    //GlobalVarLn.XPoint1GRAD_Sost = -1;
    //GlobalVarLn.YPoint1GRAD_Sost = -1;
    //GlobalVarLn.XPoint2GRAD_Sost = -1;
    //GlobalVarLn.YPoint2GRAD_Sost = -1;
    GlobalVarLn.XCenterGRAD_Dubl_Sost = -1;
    GlobalVarLn.YCenterGRAD_Dubl_Sost = -1;

    //GlobalVarLn.XPoint1GRAD_Dubl_Sost = -1;
    //GlobalVarLn.YPoint1GRAD_Dubl_Sost = -1;
    //GlobalVarLn.XPoint2GRAD_Dubl_Sost = -1;
    //GlobalVarLn.YPoint2GRAD_Dubl_Sost = -1;
    //GlobalVarLn.flTmpGPS_VKL = false;
    //GlobalVarLn.flTmrGPS = 0;

    GlobalVarLn.XCenter_Sost = 0;
    GlobalVarLn.YCenter_Sost = 0;
    GlobalVarLn.HCenter_Sost = 0;
            //GlobalVarLn.XPoint1_Sost = 0;
            //GlobalVarLn.YPoint1_Sost = 0;
            //GlobalVarLn.HPoint1_Sost = 0;
            //GlobalVarLn.XPoint2_Sost = 0;
            //GlobalVarLn.YPoint2_Sost = 0;
            //GlobalVarLn.HPoint2_Sost = 0;

            GlobalVarLn.BWGS84_1_sost = 0;
            GlobalVarLn.LWGS84_1_sost = 0;

            // ...................................................................
            //GlobalVarLn.objFormSostG.chbXY1.Checked = false;

            //GlobalVarLn.indz1_Sost = 0;
            //GlobalVarLn.indz2_Sost = 0;
            //GlobalVarLn.indz3_Sost = 0;
            //GlobalVarLn.objFormSostG.pbSP.Image = GlobalVarLn.objFormSostG.imageList1.Images[GlobalVarLn.indz1_Sost];
            //GlobalVarLn.objFormSostG.pictureBox1.Image = GlobalVarLn.objFormSostG.imageList2.Images[GlobalVarLn.indz2_Sost];
            //GlobalVarLn.objFormSostG.pictureBox2.Image = GlobalVarLn.objFormSostG.imageList3.Images[GlobalVarLn.indz3_Sost];
            // ----------------------------------------------------------------------
            LF objLF = new LF();
    LF objLF2 = new LF();
    LF objLF3 = new LF();


    // SP1
    objLF.X_m = 0;
    objLF.Y_m = 0;
    objLF.H_m = 0;
    // SP2
    objLF2.X_m = GlobalVarLn.list1_Sost[1].X_m;
    objLF2.Y_m = GlobalVarLn.list1_Sost[1].Y_m;
    objLF2.H_m = GlobalVarLn.list1_Sost[1].H_m;
    objLF2.indzn = GlobalVarLn.list1_Sost[1].indzn;
    // PU
    objLF3.X_m = GlobalVarLn.list1_Sost[2].X_m;
    objLF3.Y_m = GlobalVarLn.list1_Sost[2].Y_m;
    objLF3.H_m = GlobalVarLn.list1_Sost[2].H_m;
    objLF3.indzn = GlobalVarLn.list1_Sost[2].indzn;

    GlobalVarLn.list1_Sost.Clear();
    GlobalVarLn.list1_Sost.Add(objLF);
    GlobalVarLn.list1_Sost.Add(objLF2);
    GlobalVarLn.list1_Sost.Add(objLF3);

    // ----------------------------------------------------------------------
    // SP1
    GlobalVarLn.objFormSostG.gbOwnRect.Enabled = false;
    GlobalVarLn.objFormSostG.gbOwnRect42.Enabled = false;
    GlobalVarLn.objFormSostG.gbOwnRad.Enabled = false;
    GlobalVarLn.objFormSostG.gbOwnDegMin.Enabled = false;
    GlobalVarLn.objFormSostG.gbOwnDegMinSec.Enabled = false;

/*
    // SP2
    GlobalVarLn.objFormSostG.gbPt1Rect.Enabled = false;
    GlobalVarLn.objFormSostG.gbPt1Rect42.Enabled = false;
    GlobalVarLn.objFormSostG.gbPt1Rad.Enabled = false;
    GlobalVarLn.objFormSostG.gbPt1DegMin.Enabled = false;
    GlobalVarLn.objFormSostG.gbPt1DegMinSec.Enabled = false;
    // PU
    GlobalVarLn.objFormSostG.gbPt2Rect.Enabled = false;
    GlobalVarLn.objFormSostG.gbPt2Rect42.Enabled = false;
    GlobalVarLn.objFormSostG.gbPt2Rad.Enabled = false;
    GlobalVarLn.objFormSostG.gbPt2DegMin.Enabled = false;
    GlobalVarLn.objFormSostG.gbPt2DegMinSec.Enabled = false;
*/

    // Убрать с карты
    GlobalVarLn.axMapScreenGlobal.Repaint();
    // ---------------------------------------------------------------------

} // ClearSostSP1
// ************************************************************************

// ************************************************************************
// Очистка состава комплекса (SP2)
// ************************************************************************
// GPSSPPU
// 13_09_2018

public static void ClearSostSP2()
{
/*
    // SP1
    GlobalVarLn.objFormSostG.tbXRect.Text = "";
    GlobalVarLn.objFormSostG.tbYRect.Text = "";
    GlobalVarLn.objFormSostG.tbXRect42.Text = "";
    GlobalVarLn.objFormSostG.tbYRect42.Text = "";
    GlobalVarLn.objFormSostG.tbBRad.Text = "";
    GlobalVarLn.objFormSostG.tbLRad.Text = "";
    GlobalVarLn.objFormSostG.tbBMin1.Text = "";
    GlobalVarLn.objFormSostG.tbLMin1.Text = "";
    GlobalVarLn.objFormSostG.tbBDeg2.Text = "";
    GlobalVarLn.objFormSostG.tbBMin2.Text = "";
    GlobalVarLn.objFormSostG.tbBSec.Text = "";
    GlobalVarLn.objFormSostG.tbLDeg2.Text = "";
    GlobalVarLn.objFormSostG.tbLMin2.Text = "";
    GlobalVarLn.objFormSostG.tbLSec.Text = "";
*/
    
        // SP2
        GlobalVarLn.objFormSostG.tbPt1XRect.Text = "";
        GlobalVarLn.objFormSostG.tbPt1YRect.Text = "";
        GlobalVarLn.objFormSostG.tbPt1XRect42.Text = "";
        GlobalVarLn.objFormSostG.tbPt1YRect42.Text = "";
        GlobalVarLn.objFormSostG.tbPt1BRad.Text = "";
        GlobalVarLn.objFormSostG.tbPt1LRad.Text = "";
        GlobalVarLn.objFormSostG.tbPt1BMin1.Text = "";
        GlobalVarLn.objFormSostG.tbPt1LMin1.Text = "";
        GlobalVarLn.objFormSostG.tbPt1BDeg2.Text = "";
        GlobalVarLn.objFormSostG.tbPt1BMin2.Text = "";
        GlobalVarLn.objFormSostG.tbPt1BSec.Text = "";
        GlobalVarLn.objFormSostG.tbPt1LDeg2.Text = "";
        GlobalVarLn.objFormSostG.tbPt1LMin2.Text = "";
        GlobalVarLn.objFormSostG.tbPt1LSec.Text = "";

/*
        // PU
        GlobalVarLn.objFormSostG.tbPt2XRect.Text = "";
        GlobalVarLn.objFormSostG.tbPt2YRect.Text = "";
        GlobalVarLn.objFormSostG.tbPt2XRect42.Text = "";
        GlobalVarLn.objFormSostG.tbPt2YRect42.Text = "";
        GlobalVarLn.objFormSostG.tbPt2BRad.Text = "";
        GlobalVarLn.objFormSostG.tbPt2LRad.Text = "";
        GlobalVarLn.objFormSostG.tbPt2BMin1.Text = "";
        GlobalVarLn.objFormSostG.tbPt2LMin1.Text = "";
        GlobalVarLn.objFormSostG.tbPt2BDeg2.Text = "";
        GlobalVarLn.objFormSostG.tbPt2BMin2.Text = "";
        GlobalVarLn.objFormSostG.tbPt2BSec.Text = "";
        GlobalVarLn.objFormSostG.tbPt2LDeg2.Text = "";
        GlobalVarLn.objFormSostG.tbPt2LMin2.Text = "";
        GlobalVarLn.objFormSostG.tbPt2LSec.Text = "";
 */
        //GlobalVarLn.objFormSostG.tbOwnHeight.Text = "";
        GlobalVarLn.objFormSostG.tbPt1Height.Text = "";
        //GlobalVarLn.objFormSostG.tbPt2Height.Text = "";
    
    // ...................................................................
    // переменные

    //GlobalVarLn.flCoordSP_Sost = 0; // =1-> Выбрали СП
    GlobalVarLn.flCoordYS1_Sost = 0;
    //GlobalVarLn.flCoordYS2_Sost = 0;
    //GlobalVarLn.fl_Sost = 0; // Отрисовка зоны
    //GlobalVarLn.list1_Sost.Clear();

    //GlobalVarLn.XCenterGRAD_Sost = -1;
    //GlobalVarLn.YCenterGRAD_Sost = -1;
    GlobalVarLn.XPoint1GRAD_Sost = -1;
    GlobalVarLn.YPoint1GRAD_Sost = -1;
    //GlobalVarLn.XPoint2GRAD_Sost = -1;
    //GlobalVarLn.YPoint2GRAD_Sost = -1;
    //GlobalVarLn.XCenterGRAD_Dubl_Sost = -1;
    //GlobalVarLn.YCenterGRAD_Dubl_Sost = -1;
    GlobalVarLn.XPoint1GRAD_Dubl_Sost = -1;
    GlobalVarLn.YPoint1GRAD_Dubl_Sost = -1;
    //GlobalVarLn.XPoint2GRAD_Dubl_Sost = -1;
    //GlobalVarLn.YPoint2GRAD_Dubl_Sost = -1;
    //GlobalVarLn.flTmpGPS_VKL = false;
    //GlobalVarLn.flTmrGPS = 0;

    //GlobalVarLn.XCenter_Sost = 0;
    //GlobalVarLn.YCenter_Sost = 0;
    //GlobalVarLn.HCenter_Sost = 0;
    GlobalVarLn.XPoint1_Sost = 0;
    GlobalVarLn.YPoint1_Sost = 0;
    GlobalVarLn.HPoint1_Sost = 0;
            //GlobalVarLn.XPoint2_Sost = 0;
            //GlobalVarLn.YPoint2_Sost = 0;
            //GlobalVarLn.HPoint2_Sost = 0;

            GlobalVarLn.BWGS84_2_sost = 0;
            GlobalVarLn.LWGS84_2_sost = 0;

            // ...................................................................
            //GlobalVarLn.objFormSostG.chbXY1.Checked = false;

            //GlobalVarLn.indz1_Sost = 0;
            //GlobalVarLn.indz2_Sost = 0;
            //GlobalVarLn.indz3_Sost = 0;
            //GlobalVarLn.objFormSostG.pbSP.Image = GlobalVarLn.objFormSostG.imageList1.Images[GlobalVarLn.indz1_Sost];
            //GlobalVarLn.objFormSostG.pictureBox1.Image = GlobalVarLn.objFormSostG.imageList2.Images[GlobalVarLn.indz2_Sost];
            //GlobalVarLn.objFormSostG.pictureBox2.Image = GlobalVarLn.objFormSostG.imageList3.Images[GlobalVarLn.indz3_Sost];
            // ----------------------------------------------------------------------
            LF objLF = new LF();
    LF objLF2 = new LF();
    LF objLF3 = new LF();

    // SP1
    objLF.X_m = GlobalVarLn.list1_Sost[0].X_m;
    objLF.Y_m = GlobalVarLn.list1_Sost[0].Y_m;
    objLF.H_m = GlobalVarLn.list1_Sost[0].H_m;
    objLF.indzn = GlobalVarLn.list1_Sost[0].indzn;
    // SP2
    objLF2.X_m = 0;
    objLF2.Y_m = 0;
    objLF2.H_m = 0;
    // PU
    objLF3.X_m = GlobalVarLn.list1_Sost[2].X_m;
    objLF3.Y_m = GlobalVarLn.list1_Sost[2].Y_m;
    objLF3.H_m = GlobalVarLn.list1_Sost[2].H_m;
    objLF3.indzn = GlobalVarLn.list1_Sost[2].indzn;

    GlobalVarLn.list1_Sost.Clear();
    GlobalVarLn.list1_Sost.Add(objLF);
    GlobalVarLn.list1_Sost.Add(objLF2);
    GlobalVarLn.list1_Sost.Add(objLF3);
    // ----------------------------------------------------------------------

/*
    //3101
    // SP1
    GlobalVarLn.objFormSostG.gbOwnRect.Enabled = false;
    GlobalVarLn.objFormSostG.gbOwnRect42.Enabled = false;
    GlobalVarLn.objFormSostG.gbOwnRad.Enabled = false;
    GlobalVarLn.objFormSostG.gbOwnDegMin.Enabled = false;
    GlobalVarLn.objFormSostG.gbOwnDegMinSec.Enabled = false;
*/
    
        // SP2
        GlobalVarLn.objFormSostG.gbPt1Rect.Enabled = false;
        GlobalVarLn.objFormSostG.gbPt1Rect42.Enabled = false;
        GlobalVarLn.objFormSostG.gbPt1Rad.Enabled = false;
        GlobalVarLn.objFormSostG.gbPt1DegMin.Enabled = false;
        GlobalVarLn.objFormSostG.gbPt1DegMinSec.Enabled = false;

/*
        // PU
        GlobalVarLn.objFormSostG.gbPt2Rect.Enabled = false;
        GlobalVarLn.objFormSostG.gbPt2Rect42.Enabled = false;
        GlobalVarLn.objFormSostG.gbPt2Rad.Enabled = false;
        GlobalVarLn.objFormSostG.gbPt2DegMin.Enabled = false;
        GlobalVarLn.objFormSostG.gbPt2DegMinSec.Enabled = false;
    */


    // Убрать с карты
    GlobalVarLn.axMapScreenGlobal.Repaint();
    // ---------------------------------------------------------------------

} // ClearSostSP2
// ************************************************************************

// ************************************************************************
// Очистка состава комплекса (PU)
// ************************************************************************
// GPSSPPU
// 13_09_2018

public static void ClearSostPU()
{
    /*
        // SP1
        GlobalVarLn.objFormSostG.tbXRect.Text = "";
        GlobalVarLn.objFormSostG.tbYRect.Text = "";
        GlobalVarLn.objFormSostG.tbXRect42.Text = "";
        GlobalVarLn.objFormSostG.tbYRect42.Text = "";
        GlobalVarLn.objFormSostG.tbBRad.Text = "";
        GlobalVarLn.objFormSostG.tbLRad.Text = "";
        GlobalVarLn.objFormSostG.tbBMin1.Text = "";
        GlobalVarLn.objFormSostG.tbLMin1.Text = "";
        GlobalVarLn.objFormSostG.tbBDeg2.Text = "";
        GlobalVarLn.objFormSostG.tbBMin2.Text = "";
        GlobalVarLn.objFormSostG.tbBSec.Text = "";
        GlobalVarLn.objFormSostG.tbLDeg2.Text = "";
        GlobalVarLn.objFormSostG.tbLMin2.Text = "";
        GlobalVarLn.objFormSostG.tbLSec.Text = "";

    // SP2
    GlobalVarLn.objFormSostG.tbPt1XRect.Text = "";
    GlobalVarLn.objFormSostG.tbPt1YRect.Text = "";
    GlobalVarLn.objFormSostG.tbPt1XRect42.Text = "";
    GlobalVarLn.objFormSostG.tbPt1YRect42.Text = "";
    GlobalVarLn.objFormSostG.tbPt1BRad.Text = "";
    GlobalVarLn.objFormSostG.tbPt1LRad.Text = "";
    GlobalVarLn.objFormSostG.tbPt1BMin1.Text = "";
    GlobalVarLn.objFormSostG.tbPt1LMin1.Text = "";
    GlobalVarLn.objFormSostG.tbPt1BDeg2.Text = "";
    GlobalVarLn.objFormSostG.tbPt1BMin2.Text = "";
    GlobalVarLn.objFormSostG.tbPt1BSec.Text = "";
    GlobalVarLn.objFormSostG.tbPt1LDeg2.Text = "";
    GlobalVarLn.objFormSostG.tbPt1LMin2.Text = "";
    GlobalVarLn.objFormSostG.tbPt1LSec.Text = "";
*/
    
            // PU
            GlobalVarLn.objFormSostG.tbPt2XRect.Text = "";
            GlobalVarLn.objFormSostG.tbPt2YRect.Text = "";
            GlobalVarLn.objFormSostG.tbPt2XRect42.Text = "";
            GlobalVarLn.objFormSostG.tbPt2YRect42.Text = "";
            GlobalVarLn.objFormSostG.tbPt2BRad.Text = "";
            GlobalVarLn.objFormSostG.tbPt2LRad.Text = "";
            GlobalVarLn.objFormSostG.tbPt2BMin1.Text = "";
            GlobalVarLn.objFormSostG.tbPt2LMin1.Text = "";
            GlobalVarLn.objFormSostG.tbPt2BDeg2.Text = "";
            GlobalVarLn.objFormSostG.tbPt2BMin2.Text = "";
            GlobalVarLn.objFormSostG.tbPt2BSec.Text = "";
            GlobalVarLn.objFormSostG.tbPt2LDeg2.Text = "";
            GlobalVarLn.objFormSostG.tbPt2LMin2.Text = "";
            GlobalVarLn.objFormSostG.tbPt2LSec.Text = "";

            //GlobalVarLn.objFormSostG.tbOwnHeight.Text = "";
            //GlobalVarLn.objFormSostG.tbPt1Height.Text = "";
            GlobalVarLn.objFormSostG.tbPt2Height.Text = "";
        
    // ...................................................................
    // переменные

    //GlobalVarLn.flCoordSP_Sost = 0; // =1-> Выбрали СП
    //GlobalVarLn.flCoordYS1_Sost = 0;
    GlobalVarLn.flCoordYS2_Sost = 0;
    //GlobalVarLn.fl_Sost = 0; // Отрисовка зоны
    //GlobalVarLn.list1_Sost.Clear();

    //GlobalVarLn.XCenterGRAD_Sost = -1;
    //GlobalVarLn.YCenterGRAD_Sost = -1;
    //GlobalVarLn.XPoint1GRAD_Sost = -1;
    //GlobalVarLn.YPoint1GRAD_Sost = -1;
    GlobalVarLn.XPoint2GRAD_Sost = -1;
    GlobalVarLn.YPoint2GRAD_Sost = -1;
    //GlobalVarLn.XCenterGRAD_Dubl_Sost = -1;
    //GlobalVarLn.YCenterGRAD_Dubl_Sost = -1;
    //GlobalVarLn.XPoint1GRAD_Dubl_Sost = -1;
    //GlobalVarLn.YPoint1GRAD_Dubl_Sost = -1;
    GlobalVarLn.XPoint2GRAD_Dubl_Sost = -1;
    GlobalVarLn.YPoint2GRAD_Dubl_Sost = -1;
    //GlobalVarLn.flTmpGPS_VKL = false;
    //GlobalVarLn.flTmrGPS = 0;

    //GlobalVarLn.XCenter_Sost = 0;
    //GlobalVarLn.YCenter_Sost = 0;
    //GlobalVarLn.HCenter_Sost = 0;
    //GlobalVarLn.XPoint1_Sost = 0;
    //GlobalVarLn.YPoint1_Sost = 0;
    //GlobalVarLn.HPoint1_Sost = 0;
    GlobalVarLn.XPoint2_Sost = 0;
    GlobalVarLn.YPoint2_Sost = 0;
    GlobalVarLn.HPoint2_Sost = 0;

            GlobalVarLn.BWGS84_3_sost = 0;
            GlobalVarLn.LWGS84_3_sost = 0;

            // ...................................................................
            //GlobalVarLn.objFormSostG.chbXY1.Checked = false;

            //GlobalVarLn.indz1_Sost = 0;
            //GlobalVarLn.indz2_Sost = 0;
            //GlobalVarLn.indz3_Sost = 0;
            //GlobalVarLn.objFormSostG.pbSP.Image = GlobalVarLn.objFormSostG.imageList1.Images[GlobalVarLn.indz1_Sost];
            //GlobalVarLn.objFormSostG.pictureBox1.Image = GlobalVarLn.objFormSostG.imageList2.Images[GlobalVarLn.indz2_Sost];
            //GlobalVarLn.objFormSostG.pictureBox2.Image = GlobalVarLn.objFormSostG.imageList3.Images[GlobalVarLn.indz3_Sost];
            // ----------------------------------------------------------------------
            LF objLF = new LF();
    LF objLF2 = new LF();
    LF objLF3 = new LF();

    // SP1
    objLF.X_m = GlobalVarLn.list1_Sost[0].X_m;
    objLF.Y_m = GlobalVarLn.list1_Sost[0].Y_m;
    objLF.H_m = GlobalVarLn.list1_Sost[0].H_m;
    objLF.indzn = GlobalVarLn.list1_Sost[0].indzn;
    // SP2
    objLF2.X_m = GlobalVarLn.list1_Sost[1].X_m;
    objLF2.Y_m = GlobalVarLn.list1_Sost[1].Y_m;
    objLF2.H_m = GlobalVarLn.list1_Sost[1].H_m;
    objLF2.indzn = GlobalVarLn.list1_Sost[1].indzn;
    // PU
    objLF3.X_m = 0;
    objLF3.Y_m = 0;
    objLF3.H_m = 0;

    GlobalVarLn.list1_Sost.Clear();
    GlobalVarLn.list1_Sost.Add(objLF);
    GlobalVarLn.list1_Sost.Add(objLF2);
    GlobalVarLn.list1_Sost.Add(objLF3);
    // ----------------------------------------------------------------------

    /*
        //3101
        // SP1
        GlobalVarLn.objFormSostG.gbOwnRect.Enabled = false;
        GlobalVarLn.objFormSostG.gbOwnRect42.Enabled = false;
        GlobalVarLn.objFormSostG.gbOwnRad.Enabled = false;
        GlobalVarLn.objFormSostG.gbOwnDegMin.Enabled = false;
        GlobalVarLn.objFormSostG.gbOwnDegMinSec.Enabled = false;

    // SP2
    GlobalVarLn.objFormSostG.gbPt1Rect.Enabled = false;
    GlobalVarLn.objFormSostG.gbPt1Rect42.Enabled = false;
    GlobalVarLn.objFormSostG.gbPt1Rad.Enabled = false;
    GlobalVarLn.objFormSostG.gbPt1DegMin.Enabled = false;
    GlobalVarLn.objFormSostG.gbPt1DegMinSec.Enabled = false;
*/
    
            // PU
            GlobalVarLn.objFormSostG.gbPt2Rect.Enabled = false;
            GlobalVarLn.objFormSostG.gbPt2Rect42.Enabled = false;
            GlobalVarLn.objFormSostG.gbPt2Rad.Enabled = false;
            GlobalVarLn.objFormSostG.gbPt2DegMin.Enabled = false;
            GlobalVarLn.objFormSostG.gbPt2DegMinSec.Enabled = false;
        

    // Убрать с карты
    GlobalVarLn.axMapScreenGlobal.Repaint();
    // ---------------------------------------------------------------------

} // ClearSostPU
// ************************************************************************


// ************************************************************************
// Очистка Az1
// ************************************************************************
public static void ClearAz1()
{

    GlobalVarLn.objFormAz1G.tbXRect.Text = "";
    GlobalVarLn.objFormAz1G.tbYRect.Text = "";
    GlobalVarLn.objFormAz1G.tbXRect42.Text = "";
    GlobalVarLn.objFormAz1G.tbYRect42.Text = "";
    GlobalVarLn.objFormAz1G.tbBRad.Text = "";
    GlobalVarLn.objFormAz1G.tbLRad.Text = "";
    GlobalVarLn.objFormAz1G.tbBMin1.Text = "";
    GlobalVarLn.objFormAz1G.tbLMin1.Text = "";
    GlobalVarLn.objFormAz1G.tbBDeg2.Text = "";
    GlobalVarLn.objFormAz1G.tbBMin2.Text = "";
    GlobalVarLn.objFormAz1G.tbBSec.Text = "";
    GlobalVarLn.objFormAz1G.tbLDeg2.Text = "";
    GlobalVarLn.objFormAz1G.tbLMin2.Text = "";
    GlobalVarLn.objFormAz1G.tbLSec.Text = "";


    GlobalVarLn.objFormAz1G.tbOwnHeight.Text = "";
    // ...................................................................
    // переменные

    GlobalVarLn.flCoord_Az1 = 0; // =1-> Выбрали
    // ...................................................................

    // ----------------------------------------------------------------------
    // Очистка dataGridView

    GlobalVarLn.objFormAz1G.dataGridView1.ClearSelection();
    while (GlobalVarLn.objFormAz1G.dataGridView1.Rows.Count != 0)
        GlobalVarLn.objFormAz1G.dataGridView1.Rows.Remove(GlobalVarLn.objFormAz1G.dataGridView1.Rows[GlobalVarLn.objFormAz1G.dataGridView1.Rows.Count - 1]);

    if (GlobalVarLn.fl_Azb == 0)
    {
        GlobalVarLn.objFormAz1G.dataGridView1.Rows.Add("АСП", "", "");
        GlobalVarLn.objFormAz1G.dataGridView1.Rows.Add("АСПсопр", "", "");
        GlobalVarLn.objFormAz1G.dataGridView1.Rows.Add("ПУ", "", "");
    }
    else if (GlobalVarLn.fl_Azb == 1)
    {
        //Azb???
        GlobalVarLn.objFormAz1G.dataGridView1.Rows.Add("MS", "", "");
        GlobalVarLn.objFormAz1G.dataGridView1.Rows.Add("MSbi", "", "");
        GlobalVarLn.objFormAz1G.dataGridView1.Rows.Add("İP", "", "");
    }
    else 
    {
        GlobalVarLn.objFormAz1G.dataGridView1.Rows.Add("JS", "", "");
        GlobalVarLn.objFormAz1G.dataGridView1.Rows.Add("JSmated" +
            "", "", "");
        GlobalVarLn.objFormAz1G.dataGridView1.Rows.Add("CP", "", "");
    }
    // ----------------------------------------------------------------------

    // ----------------------------------------------------------------------
    // Убрать с карты

            GlobalVarLn.axMapScreenGlobal.Repaint();
    // ---------------------------------------------------------------------

} // ClearAz1
// ************************************************************************

// ************************************************************************
// Очистка ZPV
// ************************************************************************
public static void ClearZPV()
{

    GlobalVarLn.ObjFormLineSightRangeG.tbXRect.Text = "";
    GlobalVarLn.ObjFormLineSightRangeG.tbYRect.Text = "";
    GlobalVarLn.ObjFormLineSightRangeG.tbXRect42.Text = "";
    GlobalVarLn.ObjFormLineSightRangeG.tbYRect42.Text = "";
    GlobalVarLn.ObjFormLineSightRangeG.tbBRad.Text = "";
    GlobalVarLn.ObjFormLineSightRangeG.tbLRad.Text = "";
    GlobalVarLn.ObjFormLineSightRangeG.tbBMin1.Text = "";
    GlobalVarLn.ObjFormLineSightRangeG.tbLMin1.Text = "";
    GlobalVarLn.ObjFormLineSightRangeG.tbBDeg2.Text = "";
    GlobalVarLn.ObjFormLineSightRangeG.tbBMin2.Text = "";
    GlobalVarLn.ObjFormLineSightRangeG.tbBSec.Text = "";
    GlobalVarLn.ObjFormLineSightRangeG.tbLDeg2.Text = "";
    GlobalVarLn.ObjFormLineSightRangeG.tbLMin2.Text = "";
    GlobalVarLn.ObjFormLineSightRangeG.tbLSec.Text = "";


    GlobalVarLn.ObjFormLineSightRangeG.tbOwnHeight.Text = "";
    GlobalVarLn.ObjFormLineSightRangeG.tbHeightOwnObject.Text = "";
    GlobalVarLn.ObjFormLineSightRangeG.tbHeightOpponObject.Text = "";

    GlobalVarLn.ObjFormLineSightRangeG.tbMiddleHeight.Text = "";
    GlobalVarLn.ObjFormLineSightRangeG.tbDistSightRange.Text = "";
    // ...................................................................
    // переменные

    GlobalVarLn.fl_LineSightRange = 0; // =1-> Выбрали центр ЗПВ
    GlobalVarLn.flCoordZPV = 0; // Отрисовка 
    GlobalVarLn.XCenter_ZPV = 0;
    GlobalVarLn.YCenter_ZPV = 0;
    //777*
    GlobalVarLn.flCoordParaZPV = 0; // Отрисовка 
    GlobalVarLn.XCenter1_ZPV = 0;
    GlobalVarLn.YCenter1_ZPV = 0;
    GlobalVarLn.XCenter2_ZPV = 0;
    GlobalVarLn.YCenter2_ZPV = 0;

    // ----------------------------------------------------------------------
    // !!! После очистки координат

    GlobalVarLn.ObjFormLineSightRangeG.cbChooseSC.SelectedIndex = 0;
    GlobalVarLn.ObjFormLineSightRangeG.cbCenterLSR.SelectedIndex = 0;
    // ----------------------------------------------------------------------

    if (GlobalVarLn.listPointDSR.Count != 0)
        GlobalVarLn.listPointDSR.Clear();
    //777*
    if (GlobalVarLn.listPointDSR1.Count != 0)
        GlobalVarLn.listPointDSR1.Clear();
    if (GlobalVarLn.listPointDSR2.Count != 0)
        GlobalVarLn.listPointDSR2.Clear();

    // ----------------------------------------------------------------------
    // Убрать с карты

    GlobalVarLn.axMapScreenGlobal.Repaint();
    // ---------------------------------------------------------------------

} // ClearZPV
// ************************************************************************

// ************************************************************************
// Очистка ZonePowerAvail
// ************************************************************************
public static void ClearZonePowerAvail()
{

    GlobalVarLn.objZonePowerAvailG.tbXRect.Text = "";
    GlobalVarLn.objZonePowerAvailG.tbYRect.Text = "";
    GlobalVarLn.objZonePowerAvailG.tbXRect42.Text = "";
    GlobalVarLn.objZonePowerAvailG.tbYRect42.Text = "";
    GlobalVarLn.objZonePowerAvailG.tbBRad.Text = "";
    GlobalVarLn.objZonePowerAvailG.tbLRad.Text = "";
    GlobalVarLn.objZonePowerAvailG.tbBMin1.Text = "";
    GlobalVarLn.objZonePowerAvailG.tbLMin1.Text = "";
    GlobalVarLn.objZonePowerAvailG.tbBDeg2.Text = "";
    GlobalVarLn.objZonePowerAvailG.tbBMin2.Text = "";
    GlobalVarLn.objZonePowerAvailG.tbBSec.Text = "";
    GlobalVarLn.objZonePowerAvailG.tbLDeg2.Text = "";
    GlobalVarLn.objZonePowerAvailG.tbLMin2.Text = "";
    GlobalVarLn.objZonePowerAvailG.tbLSec.Text = "";

    GlobalVarLn.objZonePowerAvailG.tbOwnHeight.Text = "";
    GlobalVarLn.objZonePowerAvailG.tbHeightOwnObject.Text = "";

    GlobalVarLn.objZonePowerAvailG.tbRadiusZone.Text = "";
    GlobalVarLn.objZonePowerAvailG.tbMaxDist.Text = "";
    GlobalVarLn.objZonePowerAvailG.tbMiddleHeight.Text = "";
    // ...................................................................
    // переменные

    GlobalVarLn.fl_ZonePowerAvail = 0; // =1-> Выбрали центр ЗПВ
    GlobalVarLn.flCoord_ed = 0; // Отрисовка 
    GlobalVarLn.XCenter_ed = 0;
    GlobalVarLn.YCenter_ed = 0;
    // ----------------------------------------------------------------------
    // !!! После очистки координат

    GlobalVarLn.objZonePowerAvailG.cbChooseSC.SelectedIndex = 0;
    GlobalVarLn.objZonePowerAvailG.cbCenterLSR.SelectedIndex = 0;
    // ----------------------------------------------------------------------
    // Убрать с карты

    GlobalVarLn.axMapScreenGlobal.Repaint();
    // ---------------------------------------------------------------------

} // ClearZonePowerAvail
// ************************************************************************

// ************************************************************************
// Очистка ZonePowerAvail (!!! Suppression)
// ************************************************************************
public static void ClearSuppression()
{

    // SP
    GlobalVarLn.objFormSuppressionG.tbXRect.Text = "";
    GlobalVarLn.objFormSuppressionG.tbYRect.Text = "";
    GlobalVarLn.objFormSuppressionG.tbXRect42.Text = "";
    GlobalVarLn.objFormSuppressionG.tbYRect42.Text = "";
    GlobalVarLn.objFormSuppressionG.tbBRad.Text = "";
    GlobalVarLn.objFormSuppressionG.tbLRad.Text = "";
    GlobalVarLn.objFormSuppressionG.tbBMin1.Text = "";
    GlobalVarLn.objFormSuppressionG.tbLMin1.Text = "";
    GlobalVarLn.objFormSuppressionG.tbBDeg2.Text = "";
    GlobalVarLn.objFormSuppressionG.tbBMin2.Text = "";
    GlobalVarLn.objFormSuppressionG.tbBSec.Text = "";
    GlobalVarLn.objFormSuppressionG.tbLDeg2.Text = "";
    GlobalVarLn.objFormSuppressionG.tbLMin2.Text = "";
    GlobalVarLn.objFormSuppressionG.tbLSec.Text = "";


    // OP
    GlobalVarLn.objFormSuppressionG.tbPt1XRect.Text = "";
    GlobalVarLn.objFormSuppressionG.tbPt1YRect.Text = "";
    GlobalVarLn.objFormSuppressionG.tbPt1XRect42.Text = "";
    GlobalVarLn.objFormSuppressionG.tbPt1YRect42.Text = "";
    GlobalVarLn.objFormSuppressionG.tbPt1BRad.Text = "";
    GlobalVarLn.objFormSuppressionG.tbPt1LRad.Text = "";
    GlobalVarLn.objFormSuppressionG.tbPt1BMin1.Text = "";
    GlobalVarLn.objFormSuppressionG.tbPt1LMin1.Text = "";
    GlobalVarLn.objFormSuppressionG.tbPt1BDeg2.Text = "";
    GlobalVarLn.objFormSuppressionG.tbPt1BMin2.Text = "";
    GlobalVarLn.objFormSuppressionG.tbPt1BSec.Text = "";
    GlobalVarLn.objFormSuppressionG.tbPt1LDeg2.Text = "";
    GlobalVarLn.objFormSuppressionG.tbPt1LMin2.Text = "";
    GlobalVarLn.objFormSuppressionG.tbPt1LSec.Text = "";

    GlobalVarLn.objFormSuppressionG.tbOwnHeight.Text = "";
    GlobalVarLn.objFormSuppressionG.tbHeightOwnObject.Text = "";
    GlobalVarLn.objFormSuppressionG.tbPt1Height.Text = "";

    GlobalVarLn.objFormSuppressionG.tbRadiusZone.Text = "";
    GlobalVarLn.objFormSuppressionG.tbRadiusZone1.Text = "";
    GlobalVarLn.objFormSuppressionG.tbMidH.Text = "";
    // ...................................................................
    // переменные

    GlobalVarLn.fl_Suppression = 0; // =1-> Выбрали центр Zone
    GlobalVarLn.fl_ZoneSuppression = 0; // =1-> Выбрали центр Zone
    GlobalVarLn.flCoordSP_sup = 0; // Отрисовка 
    GlobalVarLn.flCoordOP = 0; // Отрисовка 

    GlobalVarLn.XCenter_sup = 0;
    GlobalVarLn.YCenter_sup = 0;
    GlobalVarLn.XPoint1_sup = 0;
    GlobalVarLn.YPoint1_sup = 0;
    GlobalVarLn.flA_sup = 0;


    if (GlobalVarLn.flCoordSP_sup == 1)
    {
        // SP
        ClassMap.f_DrawSPXY(
                      GlobalVarLn.XCenter_sup,  // m на местности
                      GlobalVarLn.YCenter_sup,
                          ""
                     );
    }


    // ----------------------------------------------------------------------
    // !!! После очистки координат

    GlobalVarLn.objFormSuppressionG.cbChooseSC.SelectedIndex = 0;
    GlobalVarLn.objFormSuppressionG.cbCenterLSR.SelectedIndex = 0;
    // ----------------------------------------------------------------------
    // Убрать с карты

    GlobalVarLn.axMapScreenGlobal.Repaint();
    // ---------------------------------------------------------------------

} // ClearSuppression
// ************************************************************************

// ************************************************************************
// Очистка ZonePowerAvail (!!! Suppression)
// ************************************************************************
public static void ClearForm2()
{

    // SP
    GlobalVarLn.ObjCommPowerAvailG.tbXRect.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbYRect.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbXRect42.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbYRect42.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbBRad.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbLRad.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbBMin1.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbLMin1.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbBDeg2.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbBMin2.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbBSec.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbLDeg2.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbLMin2.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbLSec.Text = "";

    // YS1
    GlobalVarLn.ObjCommPowerAvailG.tbPt1XRect.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt1YRect.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt1XRect42.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt1YRect42.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt1BRad.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt1LRad.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt1BMin1.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt1LMin1.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt1BDeg2.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt1BMin2.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt1BSec.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt1LDeg2.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt1LMin2.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt1LSec.Text = "";

    // YS2
    GlobalVarLn.ObjCommPowerAvailG.tbPt2XRect.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt2YRect.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt2XRect42.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt2YRect42.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt2BRad.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt2LRad.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt2BMin1.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt2LMin1.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt2BDeg2.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt2BMin2.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt2BSec.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt2LDeg2.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt2LMin2.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt2LSec.Text = "";

    GlobalVarLn.ObjCommPowerAvailG.tbOwnHeight.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbHeightOwnObject.Text = "";

    GlobalVarLn.ObjCommPowerAvailG.tbPt1Height.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbPt2Height.Text = "";

    GlobalVarLn.ObjCommPowerAvailG.tbRadiusZone.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbMaxDist.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbHindSignal.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbHindSignal2.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbResult.Text = "";
    GlobalVarLn.ObjCommPowerAvailG.tbMidH.Text = "";
    // ...................................................................
    // переменные

    GlobalVarLn.flCoordSP_comm = 0; // =1-> Выбрали СП
    GlobalVarLn.flCoordYS1_comm = 0;
    GlobalVarLn.flCoordYS2_comm = 0;
    GlobalVarLn.fl_CommPowerAvail = 0; // Отрисовка зоны
    GlobalVarLn.fl_ZSupYS=0;
    GlobalVarLn.fl_NoSup1 = 0;
    GlobalVarLn.fl_NoSup2 = 0;

    GlobalVarLn.XCenter_comm = 0;
    GlobalVarLn.YCenter_comm = 0;
    GlobalVarLn.XPoint1_comm = 0;
    GlobalVarLn.YPoint1_comm = 0;
    GlobalVarLn.XPoint2_comm = 0;
    GlobalVarLn.YPoint2_comm = 0;
    // ...................................................................

    // ----------------------------------------------------------------------
    // !!! После очистки координат

    GlobalVarLn.objFormSuppressionG.cbChooseSC.SelectedIndex = 0;
    GlobalVarLn.objFormSuppressionG.cbCenterLSR.SelectedIndex = 0;
    // ----------------------------------------------------------------------
    // Убрать с карты

    GlobalVarLn.axMapScreenGlobal.Repaint();
    // ---------------------------------------------------------------------

} // ClearForm2
// ************************************************************************

// ************************************************************************
// Очистка ZonePowerAvail (!!! Suppression)
// ************************************************************************
public static void ClearBearing()
{

    // Peleng1
    GlobalVarLn.objFormBearingG.tbXRect.Text = "";
    GlobalVarLn.objFormBearingG.tbYRect.Text = "";
    GlobalVarLn.objFormBearingG.tbXRect42.Text = "";
    GlobalVarLn.objFormBearingG.tbYRect42.Text = "";
    GlobalVarLn.objFormBearingG.tbBRad.Text = "";
    GlobalVarLn.objFormBearingG.tbLRad.Text = "";
    GlobalVarLn.objFormBearingG.tbBMin1.Text = "";
    GlobalVarLn.objFormBearingG.tbLMin1.Text = "";
    GlobalVarLn.objFormBearingG.tbBDeg2.Text = "";
    GlobalVarLn.objFormBearingG.tbBMin2.Text = "";
    GlobalVarLn.objFormBearingG.tbBSec.Text = "";
    GlobalVarLn.objFormBearingG.tbLDeg2.Text = "";
    GlobalVarLn.objFormBearingG.tbLMin2.Text = "";
    GlobalVarLn.objFormBearingG.tbLSec.Text = "";

    // Peleng2
    GlobalVarLn.objFormBearingG.tbPt1XRect.Text = "";
    GlobalVarLn.objFormBearingG.tbPt1YRect.Text = "";
    GlobalVarLn.objFormBearingG.tbPt1XRect42.Text = "";
    GlobalVarLn.objFormBearingG.tbPt1YRect42.Text = "";
    GlobalVarLn.objFormBearingG.tbPt1BRad.Text = "";
    GlobalVarLn.objFormBearingG.tbPt1LRad.Text = "";
    GlobalVarLn.objFormBearingG.tbPt1BMin1.Text = "";
    GlobalVarLn.objFormBearingG.tbPt1LMin1.Text = "";
    GlobalVarLn.objFormBearingG.tbPt1BDeg2.Text = "";
    GlobalVarLn.objFormBearingG.tbPt1BMin2.Text = "";
    GlobalVarLn.objFormBearingG.tbPt1BSec.Text = "";
    GlobalVarLn.objFormBearingG.tbPt1LDeg2.Text = "";
    GlobalVarLn.objFormBearingG.tbPt1LMin2.Text = "";
    GlobalVarLn.objFormBearingG.tbPt1LSec.Text = "";

    GlobalVarLn.objFormBearingG.tbOwnHeight.Text = "";
    GlobalVarLn.objFormBearingG.tbPt1Height.Text = "";
    GlobalVarLn.objFormBearingG.tbProzr.Text = "";

    if (GlobalVarLn.fl_Azb == 0)
    {
        GlobalVarLn.objFormBearingG.tbR1.Text = "Rмин";
        GlobalVarLn.objFormBearingG.tbR2.Text = "2Rмин";
        GlobalVarLn.objFormBearingG.tbR3.Text = "2Rмин";
        GlobalVarLn.objFormBearingG.tbR4.Text = "3Rмин";
        GlobalVarLn.objFormBearingG.tbR5.Text = "3Rмин";
        GlobalVarLn.objFormBearingG.tbR6.Text = "4Rмин";
        GlobalVarLn.objFormBearingG.tbR7.Text = "4Rмин";
        GlobalVarLn.objFormBearingG.tbR8.Text = "5Rмин";
        GlobalVarLn.objFormBearingG.tbR9.Text = "5Rмин";
        GlobalVarLn.objFormBearingG.tbR10.Text = "6Rмин";
        GlobalVarLn.objFormBearingG.tbR11.Text = "6Rмин";
        GlobalVarLn.objFormBearingG.tbR12.Text = "7Rмин";
        GlobalVarLn.objFormBearingG.tbR13.Text = "7Rмин";
        GlobalVarLn.objFormBearingG.tbR14.Text = "8Rмин";
        GlobalVarLn.objFormBearingG.tbR15.Text = "8Rмин";
        GlobalVarLn.objFormBearingG.tbR16.Text = "9Rмин";
        GlobalVarLn.objFormBearingG.tbR17.Text = "9Rмин";
        GlobalVarLn.objFormBearingG.tbR18.Text = "10Rмин";
        GlobalVarLn.objFormBearingG.tbR19.Text = "10Rмин";
        GlobalVarLn.objFormBearingG.tbR20.Text = "11Rмин";
    }
    else
    {
        //Azb
        GlobalVarLn.objFormBearingG.tbR1.Text = "Rmin";
        GlobalVarLn.objFormBearingG.tbR2.Text = "2Rmin";
        GlobalVarLn.objFormBearingG.tbR3.Text = "2Rmin";
        GlobalVarLn.objFormBearingG.tbR4.Text = "3Rmin";
        GlobalVarLn.objFormBearingG.tbR5.Text = "3Rmin";
        GlobalVarLn.objFormBearingG.tbR6.Text = "4Rmin";
        GlobalVarLn.objFormBearingG.tbR7.Text = "4Rmin";
        GlobalVarLn.objFormBearingG.tbR8.Text = "5Rmin";
        GlobalVarLn.objFormBearingG.tbR9.Text = "5Rmin";
        GlobalVarLn.objFormBearingG.tbR10.Text = "6Rmin";
        GlobalVarLn.objFormBearingG.tbR11.Text = "6Rmin";
        GlobalVarLn.objFormBearingG.tbR12.Text = "7Rmin";
        GlobalVarLn.objFormBearingG.tbR13.Text = "7Rmin";
        GlobalVarLn.objFormBearingG.tbR14.Text = "8Rmin";
        GlobalVarLn.objFormBearingG.tbR15.Text = "8Rmin";
        GlobalVarLn.objFormBearingG.tbR16.Text = "9Rmin";
        GlobalVarLn.objFormBearingG.tbR17.Text = "9Rmin";
        GlobalVarLn.objFormBearingG.tbR18.Text = "10Rmin";
        GlobalVarLn.objFormBearingG.tbR19.Text = "10Rmin";
        GlobalVarLn.objFormBearingG.tbR20.Text = "11Rmin";
    }
    // ...................................................................
    // переменные

    GlobalVarLn.fl_PelMain = 0; // Отрисовка зоны
    GlobalVarLn.flCoordSP_PelMain = 0;    // =1-> Выбрали Pel1
    GlobalVarLn.flCoordYS1_PelMain = 0;   // =1-> Выбрали Pel2
    GlobalVarLn.list1_PelMain.Clear();
    GlobalVarLn.list2_PelMain.Clear();

    //2310
    GlobalVarLn.list_rects1.Clear();
    GlobalVarLn.list_rects2.Clear();
    GlobalVarLn.list_rects3.Clear();
    GlobalVarLn.list_rects4.Clear();
    GlobalVarLn.list_rects5.Clear();
    GlobalVarLn.list_rects6.Clear();
    GlobalVarLn.list_rects7.Clear();
    GlobalVarLn.list_rects8.Clear();
    GlobalVarLn.list_rects9.Clear();
    GlobalVarLn.list_rects10.Clear();

    GlobalVarLn.XCenter_PelMain = 0;
    GlobalVarLn.YCenter_PelMain = 0;
    GlobalVarLn.XPoint1_PelMain = 0;
    GlobalVarLn.YPoint1_PelMain = 0;
    // ----------------------------------------------------------------------
    // !!! После очистки координат

    GlobalVarLn.objFormBearingG.cbChooseSC.SelectedIndex = 0;
    // ----------------------------------------------------------------------
    // Убрать с карты

    GlobalVarLn.axMapScreenGlobal.Repaint();
    // ---------------------------------------------------------------------

} // ClearBearing
// ************************************************************************

// ************************************************************************
// Нарисовать метку по координатам (красный прямоугольник)
// Входные параметры(на карте):
// X - X, m
// Y - Y, m
// ************************************************************************
public static void f_Rect_Az1(
                             double X,
                             double Y
                             )
{

    // -----------------------------------------------------------------
    Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
    Brush brushRed2 = new SolidBrush(Color.Red);
    // -----------------------------------------------------------------
    // Расстояние в м на карте -> пикселы на изображении
    mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
    // -----------------------------------------------------------------
    if (graph != null)
    {
        graph.FillRectangle(
                           brushRed2, 
                           (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft, 
                           (int)Y -GlobalVarLn.axMapScreenGlobal.MapTop, 
                           7,
                           7
                           );

    }
    // -----------------------------------------------------------------

} // P/P f_Rect_Az1
// ************************************************************************

// ************************************************************************
// функция отображения координат Object
// ************************************************************************
public static void Otobr_Az1()
{
    long ichislo;
    double dchislo;
    ichislo = 0;
    dchislo = 0;

    //333
    // Метры на местности
    //ichislo = (long)(GlobalVarLn.XCenter_Az1);
    //GlobalVarLn.objFormAz1G.tbXRect.Text = Convert.ToString(ichislo);
    //ichislo = (long)(GlobalVarLn.YCenter_Az1);
    //GlobalVarLn.objFormAz1G.tbYRect.Text = Convert.ToString(ichislo);

    ichislo = (long)(GlobalVarLn.BWGS84_Az1 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormAz1G.tbXRect.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LWGS84_Az1 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormAz1G.tbYRect.Text = Convert.ToString(dchislo);



    // Метры 1942 года
    ichislo = (long)(GlobalVarLn.XSP42_comm_Az1);
    GlobalVarLn.objFormAz1G.tbXRect42.Text = Convert.ToString(ichislo);
    ichislo = (long)(GlobalVarLn.YSP42_comm_Az1);
    GlobalVarLn.objFormAz1G.tbYRect42.Text = Convert.ToString(ichislo);

    // Радианы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrR_comm_Az1 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormAz1G.tbBRad.Text = Convert.ToString(dchislo);   // X, карта
    ichislo = (long)(GlobalVarLn.LongKrR_comm_Az1 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormAz1G.tbLRad.Text = Convert.ToString(dchislo);   // X, карта

    // Градусы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrG_comm_Az1 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormAz1G.tbBMin1.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LongKrG_comm_Az1 * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objFormAz1G.tbLMin1.Text = Convert.ToString(dchislo);

    // Градусы,мин,сек (Красовский)
    GlobalVarLn.objFormAz1G.tbBDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_comm_Az1);
    GlobalVarLn.objFormAz1G.tbBMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_comm_Az1);
    ichislo = (long)(GlobalVarLn.Lat_Sec_comm_Az1);
    GlobalVarLn.objFormAz1G.tbBSec.Text = Convert.ToString(ichislo);
    GlobalVarLn.objFormAz1G.tbLDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_comm_Az1);
    GlobalVarLn.objFormAz1G.tbLMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_comm_Az1);
    ichislo = (long)(GlobalVarLn.Long_Sec_comm_Az1);
    GlobalVarLn.objFormAz1G.tbLSec.Text = Convert.ToString(ichislo);


} // Otobr_Az1
// ************************************************************************

// ************************************************************************
// функция отображения координат CenterZPV
// ************************************************************************
public static void Otobr_ZPV()
{
    long ichislo;
    double dchislo;
    ichislo = 0;
    dchislo = 0;

    //333
    // Метры на местности
    //ichislo = (long)(GlobalVarLn.XCenter_ZPV);
    //GlobalVarLn.ObjFormLineSightRangeG.tbXRect.Text = Convert.ToString(ichislo);
    //ichislo = (long)(GlobalVarLn.YCenter_ZPV);
    //GlobalVarLn.ObjFormLineSightRangeG.tbYRect.Text = Convert.ToString(ichislo);

    ichislo = (long)(GlobalVarLn.BWGS84_ZPV * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjFormLineSightRangeG.tbXRect.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LWGS84_ZPV * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjFormLineSightRangeG.tbYRect.Text = Convert.ToString(dchislo);


    // Метры 1942 года
    ichislo = (long)(GlobalVarLn.XSP42_comm_ZPV);
    GlobalVarLn.ObjFormLineSightRangeG.tbXRect42.Text = Convert.ToString(ichislo);
    ichislo = (long)(GlobalVarLn.YSP42_comm_ZPV);
    GlobalVarLn.ObjFormLineSightRangeG.tbYRect42.Text = Convert.ToString(ichislo);

    // Радианы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrR_comm_ZPV * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjFormLineSightRangeG.tbBRad.Text = Convert.ToString(dchislo);   // X, карта
    ichislo = (long)(GlobalVarLn.LongKrR_comm_ZPV * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjFormLineSightRangeG.tbLRad.Text = Convert.ToString(dchislo);   // X, карта

    // Градусы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrG_comm_ZPV * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjFormLineSightRangeG.tbBMin1.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LongKrG_comm_ZPV * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.ObjFormLineSightRangeG.tbLMin1.Text = Convert.ToString(dchislo);

    // Градусы,мин,сек (Красовский)
    GlobalVarLn.ObjFormLineSightRangeG.tbBDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_comm_ZPV);
    GlobalVarLn.ObjFormLineSightRangeG.tbBMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_comm_ZPV);
    ichislo = (long)(GlobalVarLn.Lat_Sec_comm_ZPV);
    GlobalVarLn.ObjFormLineSightRangeG.tbBSec.Text = Convert.ToString(ichislo);
    GlobalVarLn.ObjFormLineSightRangeG.tbLDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_comm_ZPV);
    GlobalVarLn.ObjFormLineSightRangeG.tbLMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_comm_ZPV);
    ichislo = (long)(GlobalVarLn.Long_Sec_comm_ZPV);
    GlobalVarLn.ObjFormLineSightRangeG.tbLSec.Text = Convert.ToString(ichislo);

} // Otobr_ZPV
// ************************************************************************

// ************************************************************************
// функция отображения координат CenterZonePowerAvail
// ************************************************************************
public static void Otobr_ZonePowerAvail()
{
    long ichislo;
    double dchislo;
    ichislo = 0;
    dchislo = 0;

    // Метры на местности
    ichislo = (long)(GlobalVarLn.XCenter_ed);
    GlobalVarLn.objZonePowerAvailG.tbXRect.Text = Convert.ToString(ichislo);
    ichislo = (long)(GlobalVarLn.YCenter_ed);
    GlobalVarLn.objZonePowerAvailG.tbYRect.Text = Convert.ToString(ichislo);

    // Метры 1942 года
    ichislo = (long)(GlobalVarLn.XSP42_comm_ed);
    GlobalVarLn.objZonePowerAvailG.tbXRect42.Text = Convert.ToString(ichislo);
    ichislo = (long)(GlobalVarLn.YSP42_comm_ed);
    GlobalVarLn.objZonePowerAvailG.tbYRect42.Text = Convert.ToString(ichislo);

    // Радианы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrR_comm_ed * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objZonePowerAvailG.tbBRad.Text = Convert.ToString(dchislo);   // X, карта
    ichislo = (long)(GlobalVarLn.LongKrR_comm_ed * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objZonePowerAvailG.tbLRad.Text = Convert.ToString(dchislo);   // X, карта

    // Градусы (Красовский)
    ichislo = (long)(GlobalVarLn.LatKrG_comm_ed * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objZonePowerAvailG.tbBMin1.Text = Convert.ToString(dchislo);
    ichislo = (long)(GlobalVarLn.LongKrG_comm_ed * 1000000);
    dchislo = ((double)ichislo) / 1000000;
    GlobalVarLn.objZonePowerAvailG.tbLMin1.Text = Convert.ToString(dchislo);

    // Градусы,мин,сек (Красовский)
    GlobalVarLn.objZonePowerAvailG.tbBDeg2.Text = Convert.ToString(GlobalVarLn.Lat_Grad_comm_ed);
    GlobalVarLn.objZonePowerAvailG.tbBMin2.Text = Convert.ToString(GlobalVarLn.Lat_Min_comm_ed);
    ichislo = (long)(GlobalVarLn.Lat_Sec_comm_ed);
    GlobalVarLn.objZonePowerAvailG.tbBSec.Text = Convert.ToString(ichislo);
    GlobalVarLn.objZonePowerAvailG.tbLDeg2.Text = Convert.ToString(GlobalVarLn.Long_Grad_comm_ed);
    GlobalVarLn.objZonePowerAvailG.tbLMin2.Text = Convert.ToString(GlobalVarLn.Long_Min_comm_ed);
    ichislo = (long)(GlobalVarLn.Long_Sec_comm_ed);
    GlobalVarLn.objZonePowerAvailG.tbLSec.Text = Convert.ToString(ichislo);

} // Otobr_ZonePowerAvail
// ************************************************************************


// ***********************************************************************
// SP по XY m: ZPV
// !!! 1.bmp
// ***********************************************************************
public static void f_DrawSPXY(
                                 double X,
                                 double Y,
                                 String s
                                 )
{

    // -------------------------------------------------------------------------------------
    Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

    // -------------------------------------------------------------------------------------
    // Расстояние в м на карте -> пикселы на изображении

    mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
    // -------------------------------------------------------------------------------------

    if (graph != null)
    {
        // Куда поместим рисунок (!!! Изображение д.б. не меньше прямоугольника)
        Rectangle rec = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - GlobalVarLn.width_SP / 2,
                                       (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop - GlobalVarLn.height_SP,
                                       GlobalVarLn.width_SP,
                                       GlobalVarLn.height_SP
                                      );

        Bitmap bmp = new Bitmap("1.bmp");

        graph.DrawImage(bmp, rec);
        // -------------------------------------------------------------------------------------
        // Подпись

        if (s != "")
        {
            Font font2 = new Font(FontFamily.GenericSansSerif, 10.0F, FontStyle.Regular, GraphicsUnit.Pixel);
            Brush brushi = new SolidBrush(Color.White);
            Brush brushj = new SolidBrush(Color.Blue);
            Rectangle rec1 = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                              (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 1,
                                              //40,
                                              30,
                                              13
                                             );

            graph.FillRectangle(brushi, rec1);

            graph.DrawString(
                             s,
                             font2,
                             brushj,
                             (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                             (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 2
                             );
        }
        // -------------------------------------------------------------------------------------

    } // graph!=0


} // Функция f_DrawSPXY
// ***********************************************************************

// ***********************************************************************
// Зона подавления(м на местности)
// ***********************************************************************
public static void f_Map_Zon_Suppression(
                                 Point tpCenterPoint,
                                 int clr,
                                 long iRadiusZone
                                )
{
    double dLeftX = 0;
    double dLeftY = 0;
    double dRightX = 0;
    double dRightY = 0;

    double XC = 0;
    double YC = 0;
    XC = tpCenterPoint.X;
    YC = tpCenterPoint.Y;
    // ----------------------------------------------------------------------
    Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

    Pen penRed1 = new Pen(Color.Red, 2);
    Pen penRed2 = new Pen(Color.White, 2);

    Brush brushRed1 = new SolidBrush(Color.Red);
    Brush brushRed2 = new SolidBrush(Color.Blue);

    Pen pen1 = new Pen(Color.HotPink, 5.0f);
    HatchBrush brush1 = new HatchBrush(HatchStyle.Cross, Color.Linen, Color.Transparent);

    HatchBrush brush2 = new HatchBrush(HatchStyle.DiagonalCross, Color.Linen, Color.Transparent);


    Brush brushi = new SolidBrush(Color.White);

    // -------------------------------------------------------------------------------------

    if (graph != null)
    {

        dLeftX = XC - iRadiusZone;
        dLeftY = YC - iRadiusZone;
        dRightX = XC + iRadiusZone;
        dRightY = YC + iRadiusZone;
        mapPlaneToPicture(GlobalVarLn.hmapl, ref XC, ref YC);
        mapPlaneToPicture(GlobalVarLn.hmapl, ref dLeftX, ref dLeftY);
        mapPlaneToPicture(GlobalVarLn.hmapl, ref dRightX, ref dRightY);

        if (clr == 1)
        {

            graph.DrawEllipse(pen1, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft,
                (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));
            graph.FillEllipse(brush1, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft,
                (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));

        }

        else
        {
            graph.DrawEllipse(penRed2, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft,
                (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));

            // !!! Если повернули зону неподавления на СП -> НЕ заполнять
            if (GlobalVarLn.flA_sup != 3)
            {
                graph.FillEllipse(brush2, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft,
                    (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                    (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));
            }
        }


    }
// ---------------------------------------------------------------------------------

} // P/P f_Zon_Suppression
// *********************************************************************************

// ******************************************************************************************
// Перерисовка зоны подавления(м на местности)
// ******************************************************************************************
public static void f_Map_Redraw_Zon_Suppression()
{
    if (GlobalVarLn.flCoordSP_sup == 1)
    {
        // SP
        ClassMap.f_DrawSPXY(
                      GlobalVarLn.XCenter_sup,  // m на местности
                      GlobalVarLn.YCenter_sup,
                          ""
                     );
    }


    if (GlobalVarLn.flCoordOP == 1)
    {
        // OP
        ClassMap.f_Map_Pol_XY_stat(
                      GlobalVarLn.XPoint1_sup,  // m
                     GlobalVarLn.YPoint1_sup,
                      2,
                     //1,
                      ""
                     );

    }

    //if (GlobalVarLn.fl_Suppression == 1)
     if (GlobalVarLn.fl_ZoneSuppression == 1)

    {
        // ZoneSuppression
        f_Map_Zon_Suppression(
                              GlobalVarLn.tpOwnCoordRect_sup,
                              1,
                              (long)GlobalVarLn.RZ_sup
                              );
        //DrawPolygon(GlobalVarLn.listControlJammingZone, Color.Red);


        // ZoneNOSuppression (around OP)
        if (GlobalVarLn.flA_sup == 2)
        {
            f_Map_Zon_Suppression(
                                  GlobalVarLn.tpPointCentre1_sup,
                                  2,
                                  (long)GlobalVarLn.RZ1_sup
                                          );

        }
        // ZoneNOSuppression (around SP)
        else if (GlobalVarLn.flA_sup == 3) // SP
        {
            ClassMap.f_Map_Zon_Suppression(
                              //GlobalVarLn.tpOwnCoordRect_sup,
                              GlobalVarLn.tpPointCentre1_sup,
                                           2,
                              (long)GlobalVarLn.RZ1_sup
                                          );

        }
    }

} // P/P f_Map_Redraw_Zon_Suppression
// *************************************************************************************

// ***********************************************************************
// Прием состава комплекса от GPS
// ***********************************************************************
// GPSSPPU

public static void f_GetGPS()
{
    double xtmp_ed, ytmp_ed, htmp_ed;
    double xtmp1_ed, ytmp1_ed;

    xtmp_ed = 0;
    ytmp_ed = 0;
    htmp_ed = 0;
    xtmp1_ed = 0;
    ytmp1_ed = 0;

    LF objLF = new LF();
    // -----------------------------------------------------------------------------

    if((GlobalVarLn.XCenterGRAD_Sost!=-1)&&(GlobalVarLn.YCenterGRAD_Sost)!=-1)
        GlobalVarLn.flASP_GPS=1;
    if((GlobalVarLn.XPoint1GRAD_Sost!=-1)&&(GlobalVarLn.YPoint1GRAD_Sost)!=-1)
        GlobalVarLn.flASPS_GPS=1;
    if((GlobalVarLn.XPoint2GRAD_Sost!=-1)&&(GlobalVarLn.YPoint2GRAD_Sost)!=-1)
        GlobalVarLn.flPU_GPS=1;

    //333
    GlobalVarLn.BWGS84_1_sost = GlobalVarLn.XCenterGRAD_Sost;
    GlobalVarLn.LWGS84_1_sost = GlobalVarLn.YCenterGRAD_Sost;
    GlobalVarLn.BWGS84_2_sost = GlobalVarLn.XPoint1GRAD_Sost;
    GlobalVarLn.LWGS84_2_sost = GlobalVarLn.YPoint1GRAD_Sost;
    GlobalVarLn.BWGS84_3_sost = GlobalVarLn.XPoint2GRAD_Sost;
    GlobalVarLn.LWGS84_3_sost = GlobalVarLn.YPoint2GRAD_Sost;
    // -----------------------------------------------------------------------------

    // SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1
    // ASP

    if (GlobalVarLn.flASP_GPS == 1)
    {
        // ......................................................................
        ClassMap objClassMap1_ed = new ClassMap();
        ClassMap objClassMap2_ed = new ClassMap();
        ClassMap objClassMap3_ed = new ClassMap();
        // ......................................................................
        // grad(WGS84)

        xtmp1_ed = GlobalVarLn.XCenterGRAD_Sost;
        ytmp1_ed = GlobalVarLn.YCenterGRAD_Sost;
        // ......................................................................

        // WGS84(эллипсоид)->элл.Красовского *************************************
        // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
        // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
        // Входные параметры -> град,km
        // Перевод в рад - внутри функции

        // dLong ..................................................................
        // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
        // (преобразования Молоденского), угл.сек

        objClassMap1_ed.f_dLong
            (
            // Входные параметры (град,км)
                xtmp1_ed,   // широта
                ytmp1_ed,  // долгота
                0,     // высота

                // DATUM,m
                GlobalVarLn.dXdat_comm,
                GlobalVarLn.dYdat_comm,
                GlobalVarLn.dZdat_comm,

                ref GlobalVarLn.dLong_comm   // приращение по долготе, угл.сек
            );
        // ................................................................ dLong

        // dLat .................................................................
        // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
        // (преобразования Молоденского), угл.сек

        objClassMap1_ed.f_dLat
            (
            // Входные параметры (град,км)
                xtmp1_ed,   // широта
                ytmp1_ed,  // долгота
                0,     // высота

                // DATUM,m
                GlobalVarLn.dXdat_comm,
                GlobalVarLn.dYdat_comm,
                GlobalVarLn.dZdat_comm,

                ref GlobalVarLn.dLat_comm        // приращение по долготе, угл.сек
            );
        // ................................................................. dLat

        // Lat,Long .............................................................
        // Преобразования широты и долготы при пересчете WGS84->SK42

        objClassMap1_ed.f_WGS84_SK42_Lat_Long
               (
            // Входные параметры (град,км)
                   xtmp1_ed,   // широта
                   ytmp1_ed,  // долгота
                   0,     // высота
                   GlobalVarLn.dLat_comm,       // приращение по долготе, угл.сек
                   GlobalVarLn.dLong_comm,      // приращение по долготе, угл.сек

                   // Выходные параметры (grad)
                   ref GlobalVarLn.LatKrG_comm,   // широта
                   ref GlobalVarLn.LongKrG_comm   // долгота
               );
        // ............................................................ Lat,Long


        // *********************************** WGS84(эллипсоид)->элл.Красовского

        // ......................................................................
        // ellipsKras -> M real

        xtmp_ed = GlobalVarLn.LatKrG_comm;
        ytmp_ed = GlobalVarLn.LongKrG_comm;

        // Grad->rad
        xtmp_ed = (xtmp_ed * Math.PI) / 180;
        ytmp_ed = (ytmp_ed * Math.PI) / 180;

        mapGeoToPlane(GlobalVarLn.hmapl,ref xtmp_ed,ref ytmp_ed);

        GlobalVarLn.XCenter_Sost = xtmp_ed;
        GlobalVarLn.YCenter_Sost = ytmp_ed;
        // ......................................................................
        objLF.X_m = xtmp_ed;
        objLF.Y_m = ytmp_ed;
        // ......................................................................
        // H

        GlobalVarLn.axMapPointGlobalAdd.SetPoint(xtmp_ed, ytmp_ed);
        GlobalVarLn.HCenter_Sost = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
        htmp_ed = GlobalVarLn.HCenter_Sost;

        // 13_09_2018
        if (htmp_ed!=-111111)
         GlobalVarLn.objFormSostG.tbOwnHeight.Text = Convert.ToString(htmp_ed);

        objLF.H_m = htmp_ed;
        // ......................................................................
        // Sign

        objLF.indzn = GlobalVarLn.indz1_Sost;
        // ......................................................................
        GlobalVarLn.fl_Sost = 1;
        GlobalVarLn.flCoordSP_Sost = 1; // СП1 выбрана

        // 14_09_2018
        // Peleng, FRCH
        if (GlobalVarLn.flPelMain == 1) // нажата птичка отрисовки пеленга
            GlobalVarLn.flag_eq_draw1 = 1;

        // ......................................................................
        // List

        GlobalVarLn.list1_Sost[0] = objLF;
        // ......................................................................

        // .......................................................................
        // Эллипсоид Красовского, радианы

        GlobalVarLn.LatKrR_comm = (GlobalVarLn.LatKrG_comm * Math.PI) / 180;
        GlobalVarLn.LongKrR_comm = (GlobalVarLn.LongKrG_comm * Math.PI) / 180;
        // .......................................................................
        // Эллипсоид Красовского, grad,min,sec
        // dd.ddddd -> DD MM SS

        // Широта
        objClassMap3_ed.f_Grad_GMS
          (
            // Входные параметры (grad)
            GlobalVarLn.LatKrG_comm,

            // Выходные параметры 
            ref GlobalVarLn.Lat_Grad_comm,
            ref GlobalVarLn.Lat_Min_comm,
            ref GlobalVarLn.Lat_Sec_comm
          );

        // Долгота
        objClassMap3_ed.f_Grad_GMS
          (
            // Входные параметры (grad)
            GlobalVarLn.LongKrG_comm,

            // Выходные параметры 
            ref GlobalVarLn.Long_Grad_comm,
            ref GlobalVarLn.Long_Min_comm,
            ref GlobalVarLn.Long_Sec_comm
          );
        // .......................................................................

        // SK42(элл.)->Крюгер ****************************************************
        // Преобразование геодезических координат (широта, долгота, высота) 
        // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
        // проекции Гаусса-Крюгера
        // Входные параметры -> !!!grad

        objClassMap3_ed.f_SK42_Krug
               (
            // Входные параметры (!!! grad)
            // !!! эллипсоид Красовского
                   GlobalVarLn.LatKrG_comm,   // широта
                   GlobalVarLn.LongKrG_comm,  // долгота

                   // Выходные параметры (km)
                   ref GlobalVarLn.XSP42_comm,
                   ref GlobalVarLn.YSP42_comm
               );

        // km->m
        GlobalVarLn.XSP42_comm = GlobalVarLn.XSP42_comm * 1000;
        GlobalVarLn.YSP42_comm = GlobalVarLn.YSP42_comm * 1000;
        // **************************************************** SK42(элл.)->Крюгер

        // .......................................................................
        // Отобразить координаты в окошках

        // Убрать с карты
        //GlobalVarLn.axMapScreenGlobal.Repaint();
        ClassMap.OtobrSP1();

        GlobalVarLn.flASP_GPS = 0;
        // .......................................................................
    } // GlobalVarLn.flASP_GPS==1

    // SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1

    // SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2
    // SPsopr

    if (GlobalVarLn.flASPS_GPS == 1)
    {
        // ......................................................................
        ClassMap objClassMap4_ed = new ClassMap();
        ClassMap objClassMap5_ed = new ClassMap();
        ClassMap objClassMap6_ed = new ClassMap();
        // ......................................................................
        // grad(WGS84)

        xtmp1_ed = GlobalVarLn.XPoint1GRAD_Sost;
        ytmp1_ed = GlobalVarLn.YPoint1GRAD_Sost;
        // ......................................................................

        // WGS84(эллипсоид)->элл.Красовского *************************************
        // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
        // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
        // Входные параметры -> град,km
        // Перевод в рад - внутри функции

        // dLong ..................................................................
        // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
        // (преобразования Молоденского), угл.сек

        objClassMap4_ed.f_dLong
            (
            // Входные параметры (град,км)
                xtmp1_ed,   // широта
                ytmp1_ed,  // долгота
                0,     // высота

                // DATUM,m
                GlobalVarLn.dXdat_comm,
                GlobalVarLn.dYdat_comm,
                GlobalVarLn.dZdat_comm,

                ref GlobalVarLn.dLong_comm   // приращение по долготе, угл.сек
            );
        // ................................................................ dLong

        // dLat .................................................................
        // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
        // (преобразования Молоденского), угл.сек

        objClassMap4_ed.f_dLat
            (
            // Входные параметры (град,км)
                xtmp1_ed,   // широта
                ytmp1_ed,  // долгота
                0,     // высота

                // DATUM,m
                GlobalVarLn.dXdat_comm,
                GlobalVarLn.dYdat_comm,
                GlobalVarLn.dZdat_comm,

                ref GlobalVarLn.dLat_comm        // приращение по долготе, угл.сек
            );
        // ................................................................. dLat

        // Lat,Long .............................................................
        // Преобразования широты и долготы при пересчете WGS84->SK42

        objClassMap4_ed.f_WGS84_SK42_Lat_Long
               (
            // Входные параметры (град,км)
                   xtmp1_ed,   // широта
                   ytmp1_ed,  // долгота
                   0,     // высота
                   GlobalVarLn.dLat_comm,       // приращение по долготе, угл.сек
                   GlobalVarLn.dLong_comm,      // приращение по долготе, угл.сек

                   // Выходные параметры (grad)
                   ref GlobalVarLn.LatKrG_YS1_comm,   // широта
                   ref GlobalVarLn.LongKrG_YS1_comm   // долгота
               );
        // ............................................................ Lat,Long


        // *********************************** WGS84(эллипсоид)->элл.Красовского

        // ......................................................................
        // ellipsKras -> M real

        xtmp_ed = GlobalVarLn.LatKrG_YS1_comm;
        ytmp_ed = GlobalVarLn.LongKrG_YS1_comm;

        // Grad->rad
        xtmp_ed = (xtmp_ed * Math.PI) / 180;
        ytmp_ed = (ytmp_ed * Math.PI) / 180;

        mapGeoToPlane(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

        GlobalVarLn.XPoint1_Sost = xtmp_ed;
        GlobalVarLn.YPoint1_Sost = ytmp_ed;
        // ......................................................................
        objLF.X_m = xtmp_ed;
        objLF.Y_m = ytmp_ed;
        // ......................................................................
        // H

        GlobalVarLn.axMapPointGlobalAdd.SetPoint(xtmp_ed, ytmp_ed);
        GlobalVarLn.HPoint1_Sost = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
        htmp_ed = GlobalVarLn.HPoint1_Sost;

        // 13_09_2018
        if (htmp_ed != -111111)
            GlobalVarLn.objFormSostG.tbPt1Height.Text = Convert.ToString(htmp_ed);


        objLF.H_m = htmp_ed;
        // ......................................................................
        // Sign

        objLF.indzn = GlobalVarLn.indz2_Sost;
        // ......................................................................
        // List

        GlobalVarLn.list1_Sost[1] = objLF;
        // ......................................................................
        GlobalVarLn.flCoordYS1_Sost = 1; // YS1 выбран

        // 14_09_2018
        // Peleng, FRCH
        if (GlobalVarLn.flPelMain == 1) // нажата птичка отрисовки пеленга
            GlobalVarLn.flag_eq_draw2 = 1;

        // ......................................................................
        // Draw SP2

        // Убрать с карты
        //GlobalVarLn.axMapScreenGlobal.Repaint();
        // Redraw
        //ClassMap.f_Map_Redraw_Sost();
        // ......................................................................
        // Эллипсоид Красовского, радианы

        GlobalVarLn.LatKrR_YS1_comm = (GlobalVarLn.LatKrG_YS1_comm * Math.PI) / 180;
        GlobalVarLn.LongKrR_YS1_comm = (GlobalVarLn.LongKrG_YS1_comm * Math.PI) / 180;
        // .......................................................................
        // Эллипсоид Красовского, grad,min,sec
        // dd.ddddd -> DD MM SS

        // Широта
        objClassMap5_ed.f_Grad_GMS
          (
            // Входные параметры (grad)
            GlobalVarLn.LatKrG_YS1_comm,

            // Выходные параметры 
            ref GlobalVarLn.Lat_Grad_YS1_comm,
            ref GlobalVarLn.Lat_Min_YS1_comm,
            ref GlobalVarLn.Lat_Sec_YS1_comm
          );

        // Долгота
        objClassMap5_ed.f_Grad_GMS
          (
            // Входные параметры (grad)
            GlobalVarLn.LongKrG_YS1_comm,

            // Выходные параметры 
            ref GlobalVarLn.Long_Grad_YS1_comm,
            ref GlobalVarLn.Long_Min_YS1_comm,
            ref GlobalVarLn.Long_Sec_YS1_comm
          );
        // .......................................................................

        // SK42(элл.)->Крюгер ****************************************************
        // Преобразование геодезических координат (широта, долгота, высота) 
        // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
        // проекции Гаусса-Крюгера
        // Входные параметры -> !!!grad

        objClassMap6_ed.f_SK42_Krug
               (
            // Входные параметры (!!! grad)
            // !!! эллипсоид Красовского
                   GlobalVarLn.LatKrG_YS1_comm,   // широта
                   GlobalVarLn.LongKrG_YS1_comm,  // долгота

                   // Выходные параметры (km)
                   ref GlobalVarLn.XYS142_comm,
                   ref GlobalVarLn.YYS142_comm
               );

        // km->m
        GlobalVarLn.XYS142_comm = GlobalVarLn.XYS142_comm * 1000;
        GlobalVarLn.YYS142_comm = GlobalVarLn.YYS142_comm * 1000;
        // **************************************************** SK42(элл.)->Крюгер

        // .......................................................................
        // Отобразить координаты в окошках

        ClassMap.OtobrSP2();

        GlobalVarLn.flASPS_GPS = 0;
        // .......................................................................

    } // GlobalVarLn.flASPS_GPS==1
    // SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2

    // PUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPU
    // PU

    if (GlobalVarLn.flPU_GPS == 1)
    {
        // ......................................................................
        ClassMap objClassMap7_ed = new ClassMap();
        ClassMap objClassMap8_ed = new ClassMap();
        ClassMap objClassMap9_ed = new ClassMap();
        // ......................................................................
        // grad(WGS84)

        xtmp1_ed = GlobalVarLn.XPoint2GRAD_Sost;
        ytmp1_ed = GlobalVarLn.YPoint2GRAD_Sost;
        // ......................................................................

        // WGS84(эллипсоид)->элл.Красовского *************************************
        // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
        // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
        // Входные параметры -> град,km
        // Перевод в рад - внутри функции

        // dLong ..................................................................
        // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
        // (преобразования Молоденского), угл.сек

        objClassMap7_ed.f_dLong
            (
            // Входные параметры (град,км)
                xtmp1_ed,   // широта
                ytmp1_ed,  // долгота
                0,     // высота

                // DATUM,m
                GlobalVarLn.dXdat_comm,
                GlobalVarLn.dYdat_comm,
                GlobalVarLn.dZdat_comm,

                ref GlobalVarLn.dLong_comm   // приращение по долготе, угл.сек
            );
        // ................................................................ dLong

        // dLat .................................................................
        // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
        // (преобразования Молоденского), угл.сек

        objClassMap7_ed.f_dLat
            (
            // Входные параметры (град,км)
                xtmp1_ed,   // широта
                ytmp1_ed,  // долгота
                0,     // высота

                // DATUM,m
                GlobalVarLn.dXdat_comm,
                GlobalVarLn.dYdat_comm,
                GlobalVarLn.dZdat_comm,

                ref GlobalVarLn.dLat_comm        // приращение по долготе, угл.сек
            );
        // ................................................................. dLat

        // Lat,Long .............................................................
        // Преобразования широты и долготы при пересчете WGS84->SK42


        objClassMap7_ed.f_WGS84_SK42_Lat_Long
               (
            // Входные параметры (град,км)
                   xtmp1_ed,   // широта
                   ytmp1_ed,  // долгота
                   0,     // высота
                   GlobalVarLn.dLat_comm,       // приращение по долготе, угл.сек
                   GlobalVarLn.dLong_comm,      // приращение по долготе, угл.сек

                   // Выходные параметры (grad)
                   ref GlobalVarLn.LatKrG_YS2_comm,   // широта
                   ref GlobalVarLn.LongKrG_YS2_comm   // долгота
               );
        // ............................................................ Lat,Long


        // *********************************** WGS84(эллипсоид)->элл.Красовского

        // ......................................................................
        // ellipsKras -> M real

        xtmp_ed = GlobalVarLn.LatKrG_YS2_comm;
        ytmp_ed = GlobalVarLn.LongKrG_YS2_comm;

        // Grad->rad
        xtmp_ed = (xtmp_ed * Math.PI) / 180;
        ytmp_ed = (ytmp_ed * Math.PI) / 180;

        mapGeoToPlane(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

        GlobalVarLn.XPoint2_Sost = xtmp_ed;
        GlobalVarLn.YPoint2_Sost = ytmp_ed;
        // ......................................................................
        objLF.X_m = xtmp_ed;
        objLF.Y_m = ytmp_ed;
        // ......................................................................
        // H

        GlobalVarLn.axMapPointGlobalAdd.SetPoint(xtmp_ed, ytmp_ed);
        GlobalVarLn.HPoint2_Sost = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
        htmp_ed = GlobalVarLn.HPoint2_Sost;

        // 13_09_2018
        if (htmp_ed != -111111)
            GlobalVarLn.objFormSostG.tbPt2Height.Text = Convert.ToString(htmp_ed);

        objLF.H_m = htmp_ed;
        // ......................................................................
        // Sign

        objLF.indzn = GlobalVarLn.indz3_Sost;
        // ......................................................................
        // List

        //GlobalVarLn.list1_Sost.Add(objLF);
        GlobalVarLn.list1_Sost[2] = objLF;
        // ......................................................................
        GlobalVarLn.flCoordYS2_Sost = 1; // PU выбран
        // ......................................................................
        // Draw PU

        // Убрать с карты
        //GlobalVarLn.axMapScreenGlobal.Repaint();
        // Redraw
        //ClassMap.f_Map_Redraw_Sost();
        // ......................................................................

        // .......................................................................
        // Эллипсоид Красовского, радианы

        GlobalVarLn.LatKrR_YS2_comm = (GlobalVarLn.LatKrG_YS2_comm * Math.PI) / 180;
        GlobalVarLn.LongKrR_YS2_comm = (GlobalVarLn.LongKrG_YS2_comm * Math.PI) / 180;
        // .......................................................................
        // Эллипсоид Красовского, grad,min,sec
        // dd.ddddd -> DD MM SS

        // Широта
        objClassMap8_ed.f_Grad_GMS
          (
            // Входные параметры (grad)
            GlobalVarLn.LatKrG_YS2_comm,

            // Выходные параметры 
            ref GlobalVarLn.Lat_Grad_YS2_comm,
            ref GlobalVarLn.Lat_Min_YS2_comm,
            ref GlobalVarLn.Lat_Sec_YS2_comm
          );

        // Долгота
        objClassMap8_ed.f_Grad_GMS
          (
            // Входные параметры (grad)
            GlobalVarLn.LongKrG_YS2_comm,

            // Выходные параметры 
            ref GlobalVarLn.Long_Grad_YS2_comm,
            ref GlobalVarLn.Long_Min_YS2_comm,
            ref GlobalVarLn.Long_Sec_YS2_comm
          );
        // .......................................................................

        // SK42(элл.)->Крюгер ****************************************************
        // Преобразование геодезических координат (широта, долгота, высота) 
        // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
        // проекции Гаусса-Крюгера
        // Входные параметры -> !!!grad


        objClassMap9_ed.f_SK42_Krug
               (
            // Входные параметры (!!! grad)
            // !!! эллипсоид Красовского
                   GlobalVarLn.LatKrG_YS2_comm,   // широта
                   GlobalVarLn.LongKrG_YS2_comm,  // долгота

                   // Выходные параметры (km)
                   ref GlobalVarLn.XYS242_comm,
                   ref GlobalVarLn.YYS242_comm
               );

        // km->m
        GlobalVarLn.XYS242_comm = GlobalVarLn.XYS242_comm * 1000;
        GlobalVarLn.YYS242_comm = GlobalVarLn.YYS242_comm * 1000;
        // **************************************************** SK42(элл.)->Крюгер

        // .......................................................................
        // Отобразить координаты в окошках

        ClassMap.OtobrPU();

        GlobalVarLn.flPU_GPS = 0;
        // .......................................................................

    } // GlobalVarLn.flPU_GPS==1
    // PUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPUPU


    // Убрать с карты
    GlobalVarLn.axMapScreenGlobal.Repaint();
    // Redraw
    ClassMap.f_Map_Redraw_Sost();

} // Прием состава комплекса от GPS
// ***********************************************************************


// ***********************************************************************
// Прием IRI от GPS
// ***********************************************************************
//GPSIRI
// 2504
public static void f_GetGPS_IRI(int id)
{
    double xtmp_ed, ytmp_ed, htmp_ed;
    double xtmp1_ed, ytmp1_ed;

    xtmp_ed = 0;
    ytmp_ed = 0;
    htmp_ed = 0;
    xtmp1_ed = 0;
    ytmp1_ed = 0;

    AirPlane objLF = new AirPlane();
    // -----------------------------------------------------------------------------
    // !!! Get to (WGS84)

    // -----------------------------------------------------------------------------

    if (GlobalVarLn.flGPS_IRI == 1)
    {
        // ......................................................................
        ClassMap objClassMap1_ed = new ClassMap();
        ClassMap objClassMap2_ed = new ClassMap();
        ClassMap objClassMap3_ed = new ClassMap();
        // ......................................................................
        // grad(WGS84)

        xtmp1_ed = GlobalVarLn.LatGPS_IRI;
        ytmp1_ed = GlobalVarLn.LongGPS_IRI;
        // ......................................................................

        // WGS84(эллипсоид)->элл.Красовского *************************************
        // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
        // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
        // Входные параметры -> град,km
        // Перевод в рад - внутри функции

        // dLong ..................................................................
        // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
        // (преобразования Молоденского), угл.сек

        objClassMap1_ed.f_dLong
            (
            // Входные параметры (град,км)
                xtmp1_ed,   // широта
                ytmp1_ed,  // долгота
                0,     // высота

                // DATUM,m
                GlobalVarLn.dXdat_comm,
                GlobalVarLn.dYdat_comm,
                GlobalVarLn.dZdat_comm,

                ref GlobalVarLn.dLong_GPS   // приращение по долготе, угл.сек
            );
        // ................................................................ dLong

        // dLat .................................................................
        // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
        // (преобразования Молоденского), угл.сек

        objClassMap1_ed.f_dLat
            (
            // Входные параметры (град,км)
                xtmp1_ed,   // широта
                ytmp1_ed,  // долгота
                0,     // высота

                // DATUM,m
                GlobalVarLn.dXdat_comm,
                GlobalVarLn.dYdat_comm,
                GlobalVarLn.dZdat_comm,

                ref GlobalVarLn.dLat_GPS        // приращение по долготе, угл.сек
            );
        // ................................................................. dLat

        // Lat,Long .............................................................
        // Преобразования широты и долготы при пересчете WGS84->SK42

        objClassMap1_ed.f_WGS84_SK42_Lat_Long
               (
            // Входные параметры (град,км)
                   xtmp1_ed,   // широта
                   ytmp1_ed,  // долгота
                   0,     // высота
                   GlobalVarLn.dLat_GPS,       // приращение по долготе, угл.сек
                   GlobalVarLn.dLong_GPS,      // приращение по долготе, угл.сек

                   // Выходные параметры (grad)
                   ref GlobalVarLn.LatKrG_GPS,   // широта
                   ref GlobalVarLn.LongKrG_GPS   // долгота
               );
        // ............................................................ Lat,Long


        // *********************************** WGS84(эллипсоид)->элл.Красовского

        // ......................................................................
        // ellipsKras -> M real

        xtmp_ed = GlobalVarLn.LatKrG_GPS;
        ytmp_ed = GlobalVarLn.LongKrG_GPS;

        // Grad->rad
        xtmp_ed = (xtmp_ed * Math.PI) / 180;
        ytmp_ed = (ytmp_ed * Math.PI) / 180;

        mapGeoToPlane(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

        GlobalVarLn.XGPS_IRI = xtmp_ed;
        GlobalVarLn.YGPS_IRI = ytmp_ed;
        // ......................................................................
        // !!! реальные координаты на местности карты в м (Plane)

        objLF.X_m = xtmp_ed;
        objLF.Y_m = ytmp_ed;
        objLF.sNum = GlobalVarLn.sGPS_IRI;
        objLF.Num = GlobalVarLn.IDGPS_IRI;
        objLF.sh = GlobalVarLn.AdDlGPS_IRI;
        // ......................................................................
        objLF.cl = GlobalVarLn.ColGPS_IRI;
        // ......................................................................
        objLF.Num = id;
        for (int i = 0; i < GlobalVarLn.listGPS_IRI.Count; i++)
        {
            if (GlobalVarLn.listGPS_IRI[i].Num == id)
                GlobalVarLn.listGPS_IRI.Remove(GlobalVarLn.listGPS_IRI[i]);
        }
        GlobalVarLn.listGPS_IRI.Add(objLF);
        // ......................................................................
        // Draw SP1

        if ((GlobalVarLn.flDublGPS_IRI == -1) || (GlobalVarLn.flDublGPS_IRI == 1))
        {
            GlobalVarLn.flDublGPS_IRI = 0;
            // Убрать с карты
            GlobalVarLn.axMapScreenGlobal.Repaint();
            // Redraw
            ClassMap.f_Map_Redraw_GPS_IRI();

            GlobalVarLn.DublLatGPS_IRI = GlobalVarLn.LatGPS_IRI;
            GlobalVarLn.DublLongGPS_IRI = GlobalVarLn.LongGPS_IRI;

        }

        // 2504
        else
        {
            double dds = 250; // m
            double sss=0;
            sss = ClassMap.f_D_2Points(GlobalVarLn.DublLatGPS_IRI, GlobalVarLn.DublLongGPS_IRI, GlobalVarLn.LatGPS_IRI, GlobalVarLn.LongGPS_IRI, 1);

            if (
                ((GlobalVarLn.LatGPS_IRI != GlobalVarLn.DublLatGPS_IRI) || 
                (GlobalVarLn.LongGPS_IRI != GlobalVarLn.DublLongGPS_IRI))&&
                (sss>dds)
                )
            {
            // Убрать с карты
            GlobalVarLn.axMapScreenGlobal.Repaint();
            // Redraw
            ClassMap.f_Map_Redraw_GPS_IRI();

            //GlobalVarLn.DublLatGPS_IRI = GlobalVarLn.LatGPS_IRI;
            //GlobalVarLn.DublLongGPS_IRI = GlobalVarLn.LongGPS_IRI;
            }
            GlobalVarLn.DublLatGPS_IRI = GlobalVarLn.LatGPS_IRI;
            GlobalVarLn.DublLongGPS_IRI = GlobalVarLn.LongGPS_IRI;

        }

        // ......................................................................
    } // GlobalVarLn.flGPS_IRI==1


} // Прием IRI от GPS
// ***********************************************************************

// ***********************************************************************
// Прием IRI от GPS
// ***********************************************************************
//GPSIRI
// 19_10_2018

public static void f_GetGPS_IRI_2(int ind)
{
    double xtmp_ed, ytmp_ed, htmp_ed;
    double xtmp1_ed, ytmp1_ed;

    xtmp_ed = 0;
    ytmp_ed = 0;
    htmp_ed = 0;
    xtmp1_ed = 0;
    ytmp1_ed = 0;

    double dLong = 0;
    double dLat = 0;
    double LatKrG_GPS = 0;
    double LongKrG_GPS = 0;

    AirPlane objLF = new AirPlane();
    // -----------------------------------------------------------------------------
    // !!! Get to (WGS84)

    // -----------------------------------------------------------------------------

        // ......................................................................
        ClassMap objClassMap1_ed = new ClassMap();
        ClassMap objClassMap2_ed = new ClassMap();
        ClassMap objClassMap3_ed = new ClassMap();
        // ......................................................................
        // grad(WGS84)

        xtmp1_ed = GlobalVarLn.list_PelIRI2[ind].Lat;
        ytmp1_ed = GlobalVarLn.list_PelIRI2[ind].Long;
        // ......................................................................

        // WGS84(эллипсоид)->элл.Красовского *************************************
        // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
        // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
        // Входные параметры -> град,km
        // Перевод в рад - внутри функции

        // dLong ..................................................................
        // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
        // (преобразования Молоденского), угл.сек

        objClassMap1_ed.f_dLong
            (
            // Входные параметры (град,км)
                xtmp1_ed,   // широта
                ytmp1_ed,  // долгота
                0,     // высота

                // DATUM,m
                GlobalVarLn.dXdat_comm,
                GlobalVarLn.dYdat_comm,
                GlobalVarLn.dZdat_comm,

                ref dLong   // приращение по долготе, угл.сек
            );
        // ................................................................ dLong

        // dLat .................................................................
        // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
        // (преобразования Молоденского), угл.сек

        objClassMap1_ed.f_dLat
            (
            // Входные параметры (град,км)
                xtmp1_ed,   // широта
                ytmp1_ed,  // долгота
                0,     // высота

                // DATUM,m
                GlobalVarLn.dXdat_comm,
                GlobalVarLn.dYdat_comm,
                GlobalVarLn.dZdat_comm,

                ref dLat        // приращение по долготе, угл.сек
            );
        // ................................................................. dLat

        // Lat,Long .............................................................
        // Преобразования широты и долготы при пересчете WGS84->SK42

        objClassMap1_ed.f_WGS84_SK42_Lat_Long
               (
            // Входные параметры (град,км)
                   xtmp1_ed,   // широта
                   ytmp1_ed,  // долгота
                   0,     // высота
                   dLat,       // приращение по долготе, угл.сек
                   dLong,      // приращение по долготе, угл.сек

                   // Выходные параметры (grad)
                   ref LatKrG_GPS,   // широта
                   ref LongKrG_GPS   // долгота
               );
        // ............................................................ Lat,Long


        // *********************************** WGS84(эллипсоид)->элл.Красовского

        // ......................................................................
        // ellipsKras -> M real

        xtmp_ed = LatKrG_GPS;
        ytmp_ed = LongKrG_GPS;

        // Grad->rad
        xtmp_ed = (xtmp_ed * Math.PI) / 180;
        ytmp_ed = (ytmp_ed * Math.PI) / 180;

        mapGeoToPlane(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

        // !!! реальные координаты на местности карты в м (Plane)
        PelIRI[] mass = GlobalVarLn.list_PelIRI2.ToArray();
        mass[ind].XGPS_IRI = xtmp_ed;
        mass[ind].YGPS_IRI = ytmp_ed;
        GlobalVarLn.list_PelIRI2 = mass.ToList();


} // Прием IRI от GPS_2
// ***********************************************************************

// ***********************************************************************
// Draw IRI от GPS
// ***********************************************************************

public static void f_Map_Redraw_GPS_IRI()
{

    for (int i = 0; i < GlobalVarLn.listGPS_IRI.Count; i++)
    {
        ClassMap.f_Pol_GPS_IRI(
                                   GlobalVarLn.listGPS_IRI[i].X_m,
                                   GlobalVarLn.listGPS_IRI[i].Y_m,
                                   //2,
                                   GlobalVarLn.listGPS_IRI[i].cl,
                                   GlobalVarLn.listGPS_IRI[i].sNum
                                  );

    }

} // f_Map_Redraw_GPS_IRI

// ***********************************************************************

// ***********************************************************************
// Draw IRI от GPS
// ***********************************************************************
// 19_10_2018

public static void f_RedrawIRI_2()
{

    for (int i = 0; i < GlobalVarLn.list_PelIRI2.Count; i++)
    {
        if (
            (GlobalVarLn.list_PelIRI2[i].XGPS_IRI != -1) &&
            (GlobalVarLn.list_PelIRI2[i].YGPS_IRI != -1)
          )
        {
            ClassMap.f_Pol_GPS_IRI(
                                       GlobalVarLn.list_PelIRI2[i].XGPS_IRI,
                                       GlobalVarLn.list_PelIRI2[i].YGPS_IRI,
                                       GlobalVarLn.list_PelIRI2[i].color,
                                       ""
                                     );
        }

    }//FOR

} // f_Map_RedrawIRI_2

// ***********************************************************************


// ******************************************************************************************
// Нарисовать метку по координатам (треугольник)
//
// Входные параметры:
// X - X, m на местности
// Y - Y, m
// Color: 1-Red, 2-Blue
// ******************************************************************************************
public static void f_Pol_GPS_IRI(
                                 double X,
                                 double Y,
                                 //int Col,
                                 Color cl,
                                 String s
                                )
{

    // -------------------------------------------------------------------------------------
    Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

    Pen penRed1 = new Pen(Color.Red, 2);
    Brush brushRed1 = new SolidBrush(Color.Red);

    //Brush brushBlue = new SolidBrush(Color.Blue);
    Brush brushBlue = new SolidBrush(cl);
    Brush brushBlue1 = new SolidBrush(Color.Blue);

    Font font = new Font("Arial", 7);
    Brush brushSnow = new SolidBrush(Color.Snow);
    // -------------------------------------------------------------------------------------
    // Расстояние в м на карте -> пикселы на изображении

    mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
    // -------------------------------------------------------------------------------------

    if (graph != null)
    {

        Point[] toDraw = new Point[3];
        toDraw[0].X = (int)X - (GlobalVarLn.axMapScreenGlobal.MapLeft + 7);
        toDraw[0].Y = (int)Y - (GlobalVarLn.axMapScreenGlobal.MapTop - 7);
        toDraw[1].X = (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft;
        toDraw[1].Y = (int)Y - (GlobalVarLn.axMapScreenGlobal.MapTop + 7);
        toDraw[2].X = (int)X - (GlobalVarLn.axMapScreenGlobal.MapLeft - 7);
        toDraw[2].Y = (int)Y - (GlobalVarLn.axMapScreenGlobal.MapTop - 7);

        graph.FillPolygon(brushBlue, toDraw);

    }

    // -------------------------------------------------------------------------------------
    // С подписью

    if (s != "")
    {
        graph.FillRectangle(brushSnow,
                           //(int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 30,
                           (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft-15,

                           (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 10,

                           //60,
                           40,

                           10
                           );
            // Подпись
            graph.DrawString(
                             s,
                             font,

                             //brushBlue,
                             brushBlue1,

                             //(int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 30,
                             (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft-15,

                             (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 10
                             );
        //}

    } // S
    // -------------------------------------------------------------------------------------

} // P/P f_Pol_GPS_IRI
// *************************************************************************************

// ***********************************************************************
// Удалить IRI от GPS
// ***********************************************************************
//GPSIRI

public static void f_DelGPS_IRI(int ID, byte bdel)
{
    int iij=0;

    if (bdel == 1)
    {
        for (iij = (GlobalVarLn.listGPS_IRI.Count - 1); iij >= 0; iij--)
        {

            if (GlobalVarLn.listGPS_IRI[iij].Num == ID)
            {
                GlobalVarLn.listGPS_IRI.Remove(GlobalVarLn.listGPS_IRI[iij]);

                // Убрать с карты
                GlobalVarLn.axMapScreenGlobal.Repaint();
                // Redraw
                ClassMap.f_Map_Redraw_GPS_IRI();
            }


        } // FOR
    }

    else
    {

        for (iij = (GlobalVarLn.listGPS_IRI.Count - 1); iij >= 0; iij--)
        {

                GlobalVarLn.listGPS_IRI.Remove(GlobalVarLn.listGPS_IRI[iij]);
        }
                // Убрать с карты
                GlobalVarLn.axMapScreenGlobal.Repaint();
                
                // 01_10_2018
                // Redraw
                //ClassMap.f_Map_Redraw_GPS_IRI();


    }


    GlobalVarLn.flDublGPS_IRI = 1;

}
// ***********************************************************************

// ***********************************************************************
// Удалить IRI от GPS
// ***********************************************************************
//GPSIRI
//2501

public static void f_DelGPS_IRI_2(int ID)
{
    int iij = 0;

    for (iij = (GlobalVarLn.list_PelIRI.Count - 1); iij >= 0; iij--)
    {
        // IF1
        if (GlobalVarLn.list_PelIRI[iij].ID == ID)
        {
            PelIRI[] mass = GlobalVarLn.list_PelIRI.ToArray();
            mass[iij].Lat = -1;
            mass[iij].Long = -1;
            mass[iij].XGPS_IRI = -1;
            mass[iij].YGPS_IRI = -1;
            GlobalVarLn.list_PelIRI = mass.ToList();

            if ((GlobalVarLn.list_PelIRI[iij].Pel1 == -1) && (GlobalVarLn.list_PelIRI[iij].Pel2 == -1))
            {
              GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iij]);
            }

        }//IF1


    } // FOR


} //f_DelGPS_IRI_2
// ***********************************************************************


// ***********************************************************************
// Расчет координат точки пересечения двух пеленгов

// Входные параметры: 
// X1,Y1 - Координаты пеленгатора1 в СК-42
//         (картографическая проекция Гаусса-Крюгера), км
// X2,Y2 - Координаты пеленгатора2 в СК-42
//         (картографическая проекция Гаусса-Крюгера), км
// Theta1- Пеленг1, град
// Theta2- Пеленг2, град
// Mmax  - Максимальная дальность отображения пеленга в км

// Выходные параметры: 
// X_IRI_42, Y_IRI_42 - Координаты искомого ИРИ в СК-42
//         (картографическая проекция Гаусса-Крюгера), км

// Функция возвращает 0 при удачном поиске или -1 в случае НЕ обнаружения ИРИ

// Пеленг 0...360(по часовой стрелке)
// ***********************************************************************

public int f_Triang2

    (

        double X1,
        double Y1,
        double X2,
        double Y2,

        double Theta1,
        double Theta2,
        double Mmax,

        // Координаты искомого ИРИ (км)
        ref double X_IRI_42,
        ref double Y_IRI_42

    )
{
    double[] mas1 = new double[10000]; // R,Широта,долгота    
    double[] mas2 = new double[10000]; // R,Широта,долгота    
    double[] mas1_X = new double[10000]; // XYZ в опорной геоцентрической СК    
    double[] mas2_X = new double[10000]; // XYZ в опорной геоцентрической СК    

    //double[] mas1 = new double[1000000]; // R,Широта,долгота    
    //double[] mas2 = new double[1000000]; // R,Широта,долгота    
    //double[] mas1_X = new double[1000000]; // XYZ в опорной геоцентрической СК    
    //double[] mas2_X = new double[1000000]; // XYZ в опорной геоцентрической СК    

    string str_tmp1 = "";
    uint N;

    // Координаты пеленгатора в опорной геоцентрической СК(км)
    double XP1_Tr;
    double YP1_Tr;
    double ZP1_Tr;
    double XP2_Tr;
    double YP2_Tr;
    double ZP2_Tr;

    // Координаты N-й фиктивной точки в опорной геоцентрической СК(км)
    double XFN1_Tr;
    double YFN1_Tr;
    double ZFN1_Tr;
    double XFN2_Tr;
    double YFN2_Tr;
    double ZFN2_Tr;

    double Lat1_42;
    double Long1_42;
    double Lat2_42;
    double Long2_42;
    double Lat1;
    double Long1;
    double Lat2;
    double Long2;

    double XIRI_Tr;
    double YIRI_Tr;
    double ZIRI_Tr;

    double RIRI_Tr;
    double LatIRI_Tr;
    double LongIRI_Tr;

    double D1;
    double D2;

    // Initial conditions ****************************************************
    // Initial conditions

    if (fl_first_Tr == 0)
    {
        // ...............................................................

        fl_first_Pel = 1;
        // ...............................................................

    }
    // **************************************************** Initial conditions

    ClassMap objPeleng1 = new ClassMap();
    ClassMap objPeleng2 = new ClassMap();

    N = 1000;
    //N = 200000;


    XP1_Tr = 0;
    YP1_Tr = 0;
    ZP1_Tr = 0;
    XP2_Tr = 0;
    YP2_Tr = 0;
    ZP2_Tr = 0;
    XFN1_Tr = 0;
    YFN1_Tr = 0;
    ZFN1_Tr = 0;
    XFN2_Tr = 0;
    YFN2_Tr = 0;
    ZFN2_Tr = 0;

    Lat1 = 0;
    Long1 = 0;
    Lat2 = 0;
    Long2 = 0;

    XIRI_Tr = 0;
    YIRI_Tr = 0;
    ZIRI_Tr = 0;

    RIRI_Tr = 0;
    LatIRI_Tr = 0;
    LongIRI_Tr = 0;

    Lat1_42 = 0;
    Long1_42 = 0;
    Lat2_42 = 0;
    Long2_42 = 0;

    D1 = 0;
    D2 = 0;
    // -----------------------------------------------------------------------

    // -----------------------------------------------------------------------
    // Krug->Kras
    f_Krug_SK42
           (
        // Входные параметры (km)
               X1,
               Y1,
        // Выходные параметры (grad)
               ref Lat1_42,   // широта
               ref Long1_42   // долгота

           );

    f_Krug_SK42
           (
        // Входные параметры (km)
               X2,
               Y2,
        // Выходные параметры (grad)
               ref Lat2_42,   // широта
               ref Long2_42   // долгота

           );
    // -----------------------------------------------------------------------


    Lat1 = Lat1_42;
    Long1 = Long1_42;
    Lat2 = Lat2_42;
    Long2 = Long2_42;

    // -----------------------------------------------------------------------

    // -----------------------------------------------------------------------
    // Вычисление координат пеленгатора1 и N-й фиктивной точки для пеленгатора1
    // в опорной геоцентрической СК

    objPeleng1.f_Peleng(

                   // Пеленг(град)
                   Theta1,
        // Max дальность отображения пеленга(км)
                   Mmax,
        // Количество фиктивных точек в плоскости пеленгования пеленгатора
                   N,
        // Широта и долгота стояния пеленгатора(град)
                   Lat1,
                   Long1,

                   // Координаты пеленгатора в опорной геоцентрической СК(км)
                   ref XP1_Tr,
                   ref YP1_Tr,
                   ref ZP1_Tr,

                   // Координаты N-й фиктивной точки в опорной геоцентрической СК(км)
                   ref XFN1_Tr,
                   ref YFN1_Tr,
                   ref ZFN1_Tr,

                   // Координаты фиктивных точек
                   ref mas1,    // R(км),Lat(град),Long(град)
                   ref mas1_X // км

                            );

    // -----------------------------------------------------------------------

    // -----------------------------------------------------------------------
    // Вычисление координат пеленгатора2 и N-й фиктивной точки для пеленгатора1
    // в опорной геоцентрической СК

    objPeleng2.f_Peleng(

                   // Пеленг(град)
                   Theta2,
        // Max дальность отображения пеленга(км)
                   Mmax,
        // Количество фиктивных точек в плоскости пеленгования пеленгатора
                   N,
        // Широта и долгота стояния пеленгатора(град)
                   Lat2,
                   Long2,

                   // Координаты пеленгатора в опорной геоцентрической СК(км)
                   ref XP2_Tr,
                   ref YP2_Tr,
                   ref ZP2_Tr,

                   // Координаты N-й фиктивной точки в опорной геоцентрической СК(км)
                   ref XFN2_Tr,
                   ref YFN2_Tr,
                   ref ZFN2_Tr,

                   // Координаты фиктивных точек
                   ref mas2,    // R(км),Lat(град),Long(град)
                   ref mas2_X // км

                            );

    // -----------------------------------------------------------------------

    // ...............................................................
    // Перевод в м 


    XP1_Tr_tmp = XP1_Tr * 1000;
    YP1_Tr_tmp = YP1_Tr * 1000;
    ZP1_Tr_tmp = ZP1_Tr * 1000;
    XP2_Tr_tmp = XP2_Tr * 1000;
    YP2_Tr_tmp = YP2_Tr * 1000;
    ZP2_Tr_tmp = ZP2_Tr * 1000;

    XFN1_Tr_tmp = XFN1_Tr * 1000;
    YFN1_Tr_tmp = YFN1_Tr * 1000;
    ZFN1_Tr_tmp = ZFN1_Tr * 1000;
    XFN2_Tr_tmp = XFN2_Tr * 1000;
    YFN2_Tr_tmp = YFN2_Tr * 1000;
    ZFN2_Tr_tmp = ZFN2_Tr * 1000;

    // ...............................................................

    // -----------------------------------------------------------------------

    // A,T,C *****************************************************************
    // Расчет коэффициентов пллоскости пеленгования

    A1_Tr = YP1_Tr_tmp * (ZFN1_Tr_tmp) - ZP1_Tr_tmp * (YFN1_Tr_tmp);
    T1_Tr = ZP1_Tr_tmp * (XFN1_Tr_tmp) - XP1_Tr_tmp * (ZFN1_Tr_tmp);
    C1_Tr = XP1_Tr_tmp * (YFN1_Tr_tmp) - YP1_Tr_tmp * (XFN1_Tr_tmp);

    A2_Tr = YP2_Tr_tmp * (ZFN2_Tr_tmp) - ZP2_Tr_tmp * (YFN2_Tr_tmp);
    T2_Tr = ZP2_Tr_tmp * (XFN2_Tr_tmp) - XP2_Tr_tmp * (ZFN2_Tr_tmp);
    C2_Tr = XP2_Tr_tmp * (YFN2_Tr_tmp) - YP2_Tr_tmp * (XFN2_Tr_tmp);

    // ***************************************************************** A,T,C

    // C1!=0 11111111111111111111111111111111111111111111111111111111111111111
    if (C1_Tr != 0)
    {
        // .......................................................................

        if (module(C1_Tr) < 1E-10)
        {
            K1_Tr = -(A1_Tr / 1E-10);
            K2_Tr = -(T1_Tr / 1E-10);
        }

        else
        {
            K1_Tr = -(A1_Tr / C1_Tr);
            K2_Tr = -(T1_Tr / C1_Tr);
        }

        if (module(T1_Tr * C2_Tr - C1_Tr * T2_Tr) < 1E-10)
            K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / 1E-10;
        else
            K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / (T1_Tr * C2_Tr - C1_Tr * T2_Tr);
        // .......................................................................
        // Координаты искомого ИРИ в опорной геоцентрической СК

        if (module(Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr))) < 1E-10)
        {
            XIRI1_Tr = REarth_Pel / 1E-10;
            XIRI2_Tr = -REarth_Pel / 1E-10;
        }
        else
        {
            XIRI1_Tr = REarth_Pel / Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr));
            XIRI2_Tr = -REarth_Pel / Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr));
        }

        YIRI1_Tr = K3_Tr * XIRI1_Tr;
        YIRI2_Tr = K3_Tr * XIRI2_Tr;

        ZIRI1_Tr = K1_Tr * XIRI1_Tr + K2_Tr * YIRI1_Tr;
        ZIRI2_Tr = K1_Tr * XIRI2_Tr + K2_Tr * YIRI2_Tr;
        // .......................................................................

    } // C1!=0
    // 11111111111111111111111111111111111111111111111111111111111111111 C1!=0

    // C1=0 222222222222222222222222222222222222222222222222222222222222222222
    else
    {
        // .......................................................................
        if (module(T1_Tr) < 1E-10)
            K3_Tr = -A1_Tr / 1E-10;
        else
            K3_Tr = -A1_Tr / T1_Tr;

        /*
            if (module(C2_Tr) < 1E-10)
                K2_Tr = (A1_Tr + K3_Tr * T1_Tr) / 1E-10; 
            else
                K2_Tr = (A1_Tr+K3_Tr*T1_Tr)/C2_Tr; 
         */

        if (module(C2_Tr) < 1E-10)
            K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / 1E-10;
        else
            K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / C2_Tr;

        // .......................................................................
        // Координаты искомого ИРИ в опорной геоцентрической СК

        if (module(Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr)) < 1E-10)
        {
            XIRI1_Tr = REarth_Pel / 1E-10;
            XIRI2_Tr = -REarth_Pel / 1E-10;
        }

        else
        {
            XIRI1_Tr = REarth_Pel / Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr);
            XIRI2_Tr = -REarth_Pel / Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr);
        }

        YIRI1_Tr = K3_Tr * XIRI1_Tr;
        YIRI2_Tr = K3_Tr * XIRI2_Tr;

        ZIRI1_Tr = K2_Tr * XIRI1_Tr;
        ZIRI2_Tr = K2_Tr * XIRI2_Tr;
        // .......................................................................

    } // C1=0
    // 222222222222222222222222222222222222222222222222222222222222222222 C1=0

    // XIRI_Tr,YIRI_Tr,ZIRI_Tr ***********************************************
    // Критерий выбора координат искомого ИРИ в опорной геоцентрической СК
    // (минимальное расстояние до ПП)

    /*
     L1_Tr =((*pXFN1_Tr-*pXFN2_Tr)/2.-XIRI1_Tr)*((*pXFN1_Tr-*pXFN2_Tr)/2.-XIRI1_Tr)+
            ((*pYFN1_Tr-*pYFN2_Tr)/2.-YIRI1_Tr)*((*pYFN1_Tr-*pYFN2_Tr)/2.-YIRI1_Tr)+
            ((*pZFN1_Tr-*pZFN2_Tr)/2.-ZIRI1_Tr)*((*pZFN1_Tr-*pZFN2_Tr)/2.-ZIRI1_Tr);

     L2_Tr =((*pXFN1_Tr-*pXFN2_Tr)/2.-XIRI2_Tr)*((*pXFN1_Tr-*pXFN2_Tr)/2.-XIRI2_Tr)+
            ((*pYFN1_Tr-*pYFN2_Tr)/2.-YIRI2_Tr)*((*pYFN1_Tr-*pYFN2_Tr)/2.-YIRI2_Tr)+
            ((*pZFN1_Tr-*pZFN2_Tr)/2.-ZIRI2_Tr)*((*pZFN1_Tr-*pZFN2_Tr)/2.-ZIRI2_Tr);

    */

    // Координаты середины отрезка, соединяющего N-ые фиктивные точки
    DefMiddle_Pel(XFN1_Tr_tmp, XFN2_Tr_tmp, ref XM_Tr);
    DefMiddle_Pel(YFN1_Tr_tmp, YFN2_Tr_tmp, ref YM_Tr);
    DefMiddle_Pel(ZFN1_Tr_tmp, ZFN2_Tr_tmp, ref ZM_Tr);

    L1_Tr = Math.Sqrt((XM_Tr - XIRI1_Tr) * (XM_Tr - XIRI1_Tr) +
                      (YM_Tr - YIRI1_Tr) * (YM_Tr - YIRI1_Tr) +
                      (ZM_Tr - ZIRI1_Tr) * (ZM_Tr - ZIRI1_Tr));

    L2_Tr = Math.Sqrt((XM_Tr - XIRI2_Tr) * (XM_Tr - XIRI2_Tr) +
                      (YM_Tr - YIRI2_Tr) * (YM_Tr - YIRI2_Tr) +
                      (ZM_Tr - ZIRI2_Tr) * (ZM_Tr - ZIRI2_Tr));

    if (L1_Tr < L2_Tr)
    {
        XIRI_Tr = XIRI1_Tr;
        YIRI_Tr = YIRI1_Tr;
        ZIRI_Tr = ZIRI1_Tr;
    }
    else
    {
        XIRI_Tr = XIRI2_Tr;
        YIRI_Tr = YIRI2_Tr;
        ZIRI_Tr = ZIRI2_Tr;
    }
    // *********************************************** XIRI_Tr,YIRI_Tr,ZIRI_Tr

    // RIRI_Tr,LatIRI_Tr,LongIRI_Tr ******************************************
    // Угловые координаты искомого ИРИ в опорной геоцентрической СК

    // !!! Д.б. *pRIRI_Tr==REarth_Pel, т.е. r=Rз
    RIRI_Tr = Math.Sqrt(XIRI_Tr * XIRI_Tr + YIRI_Tr * YIRI_Tr + ZIRI_Tr * ZIRI_Tr);

    LatIRI_Tr = Math.Asin(ZIRI_Tr / RIRI_Tr);

    Def_Longitude_180(
                  XIRI_Tr,
                  YIRI_Tr,
                  RIRI_Tr,
                  LatIRI_Tr,
                  ref LongIRI_Tr
                 );

    // ****************************************** RIRI_Tr,LatIRI_Tr,LongIRI_Tr

    // *********************************************************************
    // Перевод в км и град

    // Координаты искомого ИРИ в опорной геоцентрической СК
    XIRI_Tr = XIRI_Tr / 1000;
    YIRI_Tr = YIRI_Tr / 1000;
    ZIRI_Tr = ZIRI_Tr / 1000;

    RIRI_Tr = RIRI_Tr / 1000;

    // Угловые координаты искомого ИРИ в опорной геоцентрической СК
    LatIRI_Tr = (LatIRI_Tr * 180) / Math.PI;
    LongIRI_Tr = (LongIRI_Tr * 180) / Math.PI;
    // *********************************************************************

    // Kras->Krug
    f_SK42_Krug
           (

               // Входные параметры (!!! grad)
        // !!! эллипсоид Красовского
               LatIRI_Tr,   // широта
               LongIRI_Tr,  // долгота

               // Выходные параметры (km)
               ref X_IRI_42,
               ref Y_IRI_42

           );
    // *********************************************************************

    D1 = Math.Sqrt((X_IRI_42 - X1) * (X_IRI_42 - X1) + (Y_IRI_42 - Y1) * (Y_IRI_42 - Y1));
    D2 = Math.Sqrt((X_IRI_42 - X2) * (X_IRI_42 - X2) + (Y_IRI_42 - Y2) * (Y_IRI_42 - Y2));

    if (
        (D1 > Mmax) ||
        (D2 > Mmax)
      )
        return -1;
    else
        return 0;

} // Функция f_Triang2
        //**************************************************************************

// ***********************************************************************
// Определение координаты середины отрезка
// ***********************************************************************

public void DefMiddle_Pel(
                    double X1,
                    double X2,
                    ref double XM

                          )
{

    // -------------------------------------------------------------------
    if (X1 == 0)
        XM = X2 / 2;
    // -------------------------------------------------------------------
    else if (X2 == 0)
        XM = X1 / 2;
    // -------------------------------------------------------------------
    else if ((X1 > 0) && (X2 > 0))
    {
        if (X2 > (X1))
            XM = X1 + ((X2 - (X1)) / 2);
        else if (X1 > (X2))
            XM = X2 + ((X1 - (X2)) / 2);
        else
            XM = X1 / 2;
    }
    // -------------------------------------------------------------------
    else if ((X1 < 0) && (X2 < 0))
    {
        if (X2 < (X1))
            XM = X1 - ((module(X2) - module(X1)) / 2);
        else if (X1 < (X2))
            XM = X2 - ((module(X1) - module(X2)) / 2);
        else
            XM = X1 / 2;
    }
    // -------------------------------------------------------------------
    else if ((X1 > 0) && (X2 < 0))
    {
        if (module(X1) > module(X2))
            XM = (module(X2) + module(X1)) / 2;
        else if (module(X1) < module(X2))
            XM = -(module(X2) + module(X1)) / 2;
        else
            XM = 0;
    }
    // -------------------------------------------------------------------
    else
    {
        if (module(X2) > module(X1))
            XM = (module(X2) + module(X1)) / 2;
        else if (module(X2) < module(X1))
            XM = -(module(X2) + module(X1)) / 2;
        else
            XM = 0;
    }
    // -------------------------------------------------------------------

} // Функция DefMiddle_Pel
        // ***********************************************************************

// ***********************************************************************
// Перевод координат эллипсоид Красовского(Пулково-42)->WGS84 (эллипсоид)

// Входные параметры:
// Lat_Coord_8442 - широта (град)
// Long_Coord_8442 - долгота (град)
// H_Coord_8442 - высота (км)
// Lat_Coord - приращение по широте, !!! угловые секунды
// Long_Coord - приращение по долготе, !!! угловые секунды

// Выходные параметры:
// Lat_Coord_Vyx_4284 - широта (град)
// Long_Coord_Vyx_4284 - долгота (град)

// ***********************************************************************
public void f_SK42_WGS84_Lat_Long
       (

           // Входные параметры (grad,km)
           double Lat_Coord_8442,   // широта
           double Long_Coord_8442,  // долгота
           double H_Coord_8442,     // высота
           double dLat_Coord,    // приращение по долготе
           double dLong_Coord,   // приращение по долготе

           // Выходные параметры (grad)
           ref double Lat_Coord_Vyx_4284,   // широта
           ref double Long_Coord_Vyx_4284  // долгота

       )
{

    // grad
    //Lat_Coord_Vyx_4284 = (Lat_Coord_8442 * 180) / Math.PI + dLat_Coord / 3600;
    //Long_Coord_Vyx_4284 = (Long_Coord_8442 * 180) / Math.PI + dLong_Coord / 3600;
    Lat_Coord_Vyx_4284 = Lat_Coord_8442 + dLat_Coord / 3600;
    Long_Coord_Vyx_4284 = Long_Coord_8442 + dLong_Coord / 3600;


} // Функция f_SK42_WGS84_Lat_Long
        //************************************************************************



// **************************************************************************** NEW

// SECTOR *****************************************************************

// ***********************************************************************
// Sector
// ***********************************************************************
//Sect

public static void Sector
 (
    // SP
     double XXX,
     double YYY

 )
{
    //Sect
    //GlobalVarLn.flsect = 0;
    // .....................................................................
    // Убрать с карты
    //GlobalVarLn.axMapScreenGlobal.Repaint();
    // .....................................................................

    if ((XXX == 0) || (YYY == 0))
        return;
    if ((XXX == -1) || (YYY == -1))
        return;

    if (
        (GlobalVarLn.luch1 == -1) &&
        (GlobalVarLn.luch2 == -1) &&
        (GlobalVarLn.luch3 == -1) &&
        (GlobalVarLn.luch4 == -1) &&
        (GlobalVarLn.luch5 == -1)
       )
        return;

    if (
        (GlobalVarLn.luch1 == 0) &&
        (GlobalVarLn.luch2 == 0) &&
        (GlobalVarLn.luch3 == 0) &&
        (GlobalVarLn.luch4 == 0) &&
        (GlobalVarLn.luch5 == 0)
       )
        return;
    // ..................................................................

/*
    if (GlobalVarLn.f_luch == 0)
    {
        GlobalVarLn.f_luch = 1;
        GlobalVarLn.luch1_dubl = GlobalVarLn.luch1;
        GlobalVarLn.luch2_dubl = GlobalVarLn.luch2;
        GlobalVarLn.luch3_dubl = GlobalVarLn.luch3;
        GlobalVarLn.luch4_dubl = GlobalVarLn.luch4;
        GlobalVarLn.luch5_dubl = GlobalVarLn.luch5;
    }

    else
    {
        if (
            (GlobalVarLn.luch1 == GlobalVarLn.luch1_dubl) &&
            (GlobalVarLn.luch2 == GlobalVarLn.luch2_dubl) &&
            (GlobalVarLn.luch3 == GlobalVarLn.luch3_dubl) &&
            (GlobalVarLn.luch4 == GlobalVarLn.luch4_dubl) &&
            (GlobalVarLn.luch5 == GlobalVarLn.luch5_dubl)
          )
        {
            return;
        }
        else
        {
            GlobalVarLn.luch1_dubl = GlobalVarLn.luch1;
            GlobalVarLn.luch2_dubl = GlobalVarLn.luch2;
            GlobalVarLn.luch3_dubl = GlobalVarLn.luch3;
            GlobalVarLn.luch4_dubl = GlobalVarLn.luch4;
            GlobalVarLn.luch5_dubl = GlobalVarLn.luch5;

        }

    }
 */
    // ..................................................................
    double xx = 0;
    double yy = 0;
    double xx1 = 0;
    double yy1 = 0;
    double xx2 = 0;
    double yy2 = 0;
    // ..................................................................

    GlobalVarLn.flsect = 1;
    // ..................................................................
    //GlobalVarLn.RSect = iniRW.get_RSect(); // m
    // ..................................................................
    Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
    // ..................................................................
    // !!! Отладка Это будем получать в прерывании
   // GlobalVarLn.luch1 = 0; // grad
   // GlobalVarLn.luch2 = 10;
   // GlobalVarLn.luch3 = 90;
   // GlobalVarLn.luch4 = 180;
   // GlobalVarLn.luch5 = 300;
    // ..................................................................

    GlobalVarLn.dAlpha1 = (double)GlobalVarLn.ValSect1 / 2;
    GlobalVarLn.dAlpha2 = (double)GlobalVarLn.ValSect2 / 2;
    GlobalVarLn.dAlpha3 = (double)GlobalVarLn.ValSect3 / 2;
    GlobalVarLn.dAlpha4 = (double)GlobalVarLn.ValSect4 / 2;
    GlobalVarLn.dAlpha5 = (double)GlobalVarLn.ValSect5 / 2;

    // ..................................................................
    // Create pen.
    //Pen blackPen = new Pen(Color.Black, 3);
    //Pen RedPen = new Pen(Color.FromArgb(GlobalVarLn.prozr1_bear, 0, GlobalVarLn.grn1, 0), 3);

/*
    int prozr = 100;
    Brush brush1;
    brush1 = new SolidBrush(Color.FromArgb(prozr, 255, 0, 0)); // red
    Brush brush2;
    brush2 = new SolidBrush(Color.FromArgb(prozr, 0, 255, 0));// green
    Brush brush3;
    brush3 = new SolidBrush(Color.FromArgb(prozr, 0, 0, 255));//blue
    Brush brush4;
    brush4 = new SolidBrush(Color.FromArgb(prozr, 100, 0, 0));
    Brush brush5;
    brush5 = new SolidBrush(Color.FromArgb(prozr, 0, 100, 0));
*/


    int prozr = 100;
    Brush brush1;
    brush1 = new SolidBrush(Color.FromArgb(prozr, GlobalVarLn.Col1_1, GlobalVarLn.Col1_2, GlobalVarLn.Col1_3)); // red
    Brush brush2;
    brush2 = new SolidBrush(Color.FromArgb(prozr, GlobalVarLn.Col2_1, GlobalVarLn.Col2_2, GlobalVarLn.Col2_3));// green
    Brush brush3;
    brush3 = new SolidBrush(Color.FromArgb(prozr, GlobalVarLn.Col3_1, GlobalVarLn.Col3_2, GlobalVarLn.Col3_3));//blue
    Brush brush4;
    brush4 = new SolidBrush(Color.FromArgb(prozr, GlobalVarLn.Col4_1, GlobalVarLn.Col4_2, GlobalVarLn.Col4_3));
    Brush brush5;
    brush5 = new SolidBrush(Color.FromArgb(prozr, GlobalVarLn.Col5_1, GlobalVarLn.Col5_2, GlobalVarLn.Col5_3));

    // ..................................................................

    //Rectangle rect = new Rectangle(0,0, 200, 200);
    xx = XXX;
    yy = YYY;
    xx1 = xx + (double)GlobalVarLn.RSect;
    yy1 = yy + (double)GlobalVarLn.RSect;

    mapPlaneToPicture(GlobalVarLn.hmapl, ref xx, ref yy);
    mapPlaneToPicture(GlobalVarLn.hmapl, ref xx1, ref yy1);

    xx2 = xx1 - xx;
    yy2 = yy - yy1;
    // ..................................................................
    // Левый верхний угол сектора, ширина, высота
    // !!! Это для функции DrawPie

    //Rectangle rect = new Rectangle((int)xx - GlobalVarLn.axMapScreenGlobal.MapLeft-(int)(xx2/2), (int)yy - GlobalVarLn.axMapScreenGlobal.MapTop-(int)(yy2/2), 
    //    (int)xx2, (int)yy2);
    // Draw arc to screen.
    //graph.DrawPie(blackPen, rect, startAngle, sweepAngle);

    // ..................................................................

    // Create start and sweep angles on ellipse.
    //float startAngle = 45.0F;
    //float sweepAngle = 270.0F;

    // От оси X(гориз) по часовой стрелке
    //GlobalVarLn.float startAngle = 315.0F;
    // От startAngle по час. стрелке
    //float sweepAngle = 90.0F;

    // Получается окружность
    //float startAngle = 0.0F;
    //float sweepAngle = 360.0F;

    // От оси X(гориз) по часовой стрелке
    //float startAngle = 305.0F;
    // От startAngle по час. стрелке
    //float sweepAngle = 20.0F;
    // ..................................................................

    /*
        graph.FillPie(
                      brush1, 
                      (int)xx - GlobalVarLn.axMapScreenGlobal.MapLeft - (int)xx2/2,
                      (int)yy - GlobalVarLn.axMapScreenGlobal.MapTop - (int)yy2/2,
                      (int)xx2,
                      (int)yy2,
                      startAngle,
                      sweepAngle
                      );
     */
    // ..................................................................

    // Otl
    //GlobalVarLn.luch1 = 270;
    //GlobalVarLn.dAlpha1 = 30;
    //GlobalVarLn.ValSect1=60; // dAlpha1*2

    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    // 1

    if (
        (GlobalVarLn.fllSect1 == 1)&&
        (GlobalVarLn.luch1>0)&&
        (GlobalVarLn.luch1<=360)
        )
    {
        Sector_DefA1A2(
                     GlobalVarLn.luch1,
                     GlobalVarLn.dAlpha1,
                     ref GlobalVarLn.Alpha1,
                     ref GlobalVarLn.Alpha2
                     );
        Sector_DefStartASweepA(
                             GlobalVarLn.Alpha1,
                             GlobalVarLn.ValSect1,
                             ref GlobalVarLn.startA,
                             ref GlobalVarLn.sweepA
                            );
        graph.FillPie(
                    brush1,
                    (int)xx - GlobalVarLn.axMapScreenGlobal.MapLeft - (int)xx2 / 2,
                    (int)yy - GlobalVarLn.axMapScreenGlobal.MapTop - (int)yy2 / 2,
                    (int)xx2,
                    (int)yy2,
                    GlobalVarLn.startA,
                    GlobalVarLn.sweepA
                    );
    }
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    // 2
    if (
        (GlobalVarLn.fllSect2 == 1) &&
        (GlobalVarLn.luch2 > 0) &&
        (GlobalVarLn.luch2 <= 360)
        )
    {
        Sector_DefA1A2(
                     GlobalVarLn.luch2,
                     GlobalVarLn.dAlpha2,
                     ref GlobalVarLn.Alpha1,
                     ref GlobalVarLn.Alpha2
                     );
        Sector_DefStartASweepA(
                             GlobalVarLn.Alpha1,
                             GlobalVarLn.ValSect2,
                             ref GlobalVarLn.startA,
                             ref GlobalVarLn.sweepA
                            );
        graph.FillPie(
                    brush2,
                    (int)xx - GlobalVarLn.axMapScreenGlobal.MapLeft - (int)xx2 / 2,
                    (int)yy - GlobalVarLn.axMapScreenGlobal.MapTop - (int)yy2 / 2,
                    (int)xx2,
                    (int)yy2,
                    GlobalVarLn.startA,
                    GlobalVarLn.sweepA
                    );
    }
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    // 3
    if (
        (GlobalVarLn.fllSect3 == 1) &&
        (GlobalVarLn.luch3 > 0) &&
        (GlobalVarLn.luch3 <= 360)
        )
    {
        Sector_DefA1A2(
                     GlobalVarLn.luch3,
                     GlobalVarLn.dAlpha3,
                     ref GlobalVarLn.Alpha1,
                     ref GlobalVarLn.Alpha2
                     );
        Sector_DefStartASweepA(
                             GlobalVarLn.Alpha1,
                             GlobalVarLn.ValSect3,
                             ref GlobalVarLn.startA,
                             ref GlobalVarLn.sweepA
                            );
        graph.FillPie(
                    brush3,
                    (int)xx - GlobalVarLn.axMapScreenGlobal.MapLeft - (int)xx2 / 2,
                    (int)yy - GlobalVarLn.axMapScreenGlobal.MapTop - (int)yy2 / 2,
                    (int)xx2,
                    (int)yy2,
                    GlobalVarLn.startA,
                    GlobalVarLn.sweepA
                    );
    }
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    // 4
    if (
        (GlobalVarLn.fllSect4 == 1) &&
        (GlobalVarLn.luch4 > 0) &&
        (GlobalVarLn.luch4 <= 360)
        )
    {
        Sector_DefA1A2(
                     GlobalVarLn.luch4,
                     GlobalVarLn.dAlpha4,
                     ref GlobalVarLn.Alpha1,
                     ref GlobalVarLn.Alpha2
                     );
        Sector_DefStartASweepA(
                             GlobalVarLn.Alpha1,
                             GlobalVarLn.ValSect4,
                             ref GlobalVarLn.startA,
                             ref GlobalVarLn.sweepA
                            );
        graph.FillPie(
                    brush4,
                    (int)xx - GlobalVarLn.axMapScreenGlobal.MapLeft - (int)xx2 / 2,
                    (int)yy - GlobalVarLn.axMapScreenGlobal.MapTop - (int)yy2 / 2,
                    (int)xx2,
                    (int)yy2,
                    GlobalVarLn.startA,
                    GlobalVarLn.sweepA
                    );
    }
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    // 5
    if (
        (GlobalVarLn.fllSect5 == 1) &&
        (GlobalVarLn.luch5 > 0) &&
        (GlobalVarLn.luch5 <= 360)
        )
    {
        Sector_DefA1A2(
                     GlobalVarLn.luch5,
                     GlobalVarLn.dAlpha5,
                     ref GlobalVarLn.Alpha1,
                     ref GlobalVarLn.Alpha2
                     );
        Sector_DefStartASweepA(
                             GlobalVarLn.Alpha1,
                             GlobalVarLn.ValSect5,
                             ref GlobalVarLn.startA,
                             ref GlobalVarLn.sweepA
                            );
        graph.FillPie(
                    brush5,
                    (int)xx - GlobalVarLn.axMapScreenGlobal.MapLeft - (int)xx2 / 2,
                    (int)yy - GlobalVarLn.axMapScreenGlobal.MapTop - (int)yy2 / 2,
                    (int)xx2,
                    (int)yy2,
                    GlobalVarLn.startA,
                    GlobalVarLn.sweepA
                    );
    }
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


    // ..................................................................


} // Sector
//************************************************************************
// Определение верхнего и нижнего углов сектора (от вертик. оси Y по час. 
// стрелке 0...360)
//************************************************************************
public static void Sector_DefA1A2(
                                  double Lch,
                                  double dA,
                                  ref double A1,
                                  ref double A2
                                 )
{

    A1 = Lch - dA;
    if (A1 < 0)
        A1 = 360 + A1;
    A2 = Lch + dA;
    if (A2 > 360)
        A2 = A2 - 360;

} // Sector_DefA1A2

//************************************************************************
// Пересчет от верхнего и нижнего углов сектора (от вертик. оси Y по час. 
// стрелке 0...360) в:
// - start -> от гориз. оси X по час. стрелке 0...360
// - sweep -> от угла START по час. стрелке 0...360
//************************************************************************
public static void Sector_DefStartASweepA(
                                  double A1,
    //double A2,
                                  double dA,
                                  ref float stA,
                                  ref float swA
                                 )
{
    // ......................................................................
    // START

    if ((A1 > 0) && (A1 <= 90))
    {
        stA = 270 + (float)A1;
    }
    else
    {
        stA = (float)A1 - 90;
    }
    // ......................................................................
    // SWEEP

    swA = (float)dA;
    // ......................................................................

} // Sector_DefStartASweepA
//************************************************************************

//************************************************************************
// Accept of Sectors
//************************************************************************

public static void Sector_Accept()
{
/*
    // .....................................................................
    if (GlobalVarLn.objFormSectG.chbAnt1.Checked == true)
        GlobalVarLn.fllSect1 = 1;
    else
        GlobalVarLn.fllSect1 = 0;

    if (GlobalVarLn.objFormSectG.chbAnt2.Checked == true)
        GlobalVarLn.fllSect2 = 1;
    else
        GlobalVarLn.fllSect2 = 0;

    if (GlobalVarLn.objFormSectG.chbAnt3.Checked == true)
        GlobalVarLn.fllSect3 = 1;
    else
        GlobalVarLn.fllSect3 = 0;

    if (GlobalVarLn.objFormSectG.chbAnt4.Checked == true)
        GlobalVarLn.fllSect4 = 1;
    else
        GlobalVarLn.fllSect4 = 0;

    if (GlobalVarLn.objFormSectG.chbAnt5.Checked == true)
        GlobalVarLn.fllSect5 = 1;
    else
        GlobalVarLn.fllSect5 = 0;

    // .....................................................................
    if (System.IO.File.Exists(Application.StartupPath + "\\Settings.ini"))
    {
        iniRW.write_flSect(GlobalVarLn.fllSect1, GlobalVarLn.fllSect2, GlobalVarLn.fllSect3, GlobalVarLn.fllSect4, GlobalVarLn.fllSect5);

    }
    else
    {
        //Azb
        if (GlobalVarLn.fl_Azb == 0)
        {
            MessageBox.Show("Невозможно открыть INI файл");
        }
        else
        {
            MessageBox.Show("fayl açıq deyil");
        }
        return;
    }
    // .....................................................................
*/

} // Sector_Accept
//************************************************************************
// INIT of Sectors
//************************************************************************

public static void Sector_INI()
{
    // -------------------------------------------------------------------------
    if (System.IO.File.Exists(Application.StartupPath + "\\Settings.ini"))
    {

        // .....................................................................
        // Угловая величина сектора (град)

        GlobalVarLn.ValSect1 = (double)iniRW.get_Sect1();
        if (
            (GlobalVarLn.ValSect1 <= 0) ||
            (GlobalVarLn.ValSect1 > 360)
           )
            GlobalVarLn.ValSect1 = 120;

        GlobalVarLn.ValSect2 = (double)iniRW.get_Sect2();
        if (
            (GlobalVarLn.ValSect2 <= 0) ||
            (GlobalVarLn.ValSect2 > 360)
           )
            GlobalVarLn.ValSect2 = 120;

        GlobalVarLn.ValSect3 = (double)iniRW.get_Sect3();
        if (
            (GlobalVarLn.ValSect3 <= 0) ||
            (GlobalVarLn.ValSect3 > 360)
           )
            GlobalVarLn.ValSect3 = 120;

        GlobalVarLn.ValSect4 = (double)iniRW.get_Sect4();
        if (
            (GlobalVarLn.ValSect4 <= 0) ||
            (GlobalVarLn.ValSect4 > 360)
           )
            GlobalVarLn.ValSect4 = 7;

        GlobalVarLn.ValSect5 = (double)iniRW.get_Sect5();
        if (
            (GlobalVarLn.ValSect5 <= 0) ||
            (GlobalVarLn.ValSect5 > 360)
           )
            GlobalVarLn.ValSect5 = 7;

        // .....................................................................
        // Радиус сектора

        GlobalVarLn.RSect = iniRW.get_RSect(); // m
        if (
            (GlobalVarLn.RSect <= 0) ||
            (GlobalVarLn.RSect > 1000000)
           )
            GlobalVarLn.RSect = 200000;
        // .....................................................................
        // Цвета сектора

        GlobalVarLn.Col1_1=iniRW.get_Col1_1();
        GlobalVarLn.Col1_2 = iniRW.get_Col1_2();
        GlobalVarLn.Col1_3 = iniRW.get_Col1_3();
        GlobalVarLn.Col2_1 = iniRW.get_Col2_1();
        GlobalVarLn.Col2_2 = iniRW.get_Col2_2();
        GlobalVarLn.Col2_3 = iniRW.get_Col2_3();
        GlobalVarLn.Col3_1 = iniRW.get_Col3_1();
        GlobalVarLn.Col3_2 = iniRW.get_Col3_2();
        GlobalVarLn.Col3_3 = iniRW.get_Col3_3();
        GlobalVarLn.Col4_1 = iniRW.get_Col4_1();
        GlobalVarLn.Col4_2 = iniRW.get_Col4_2();
        GlobalVarLn.Col4_3 = iniRW.get_Col4_3();
        GlobalVarLn.Col5_1 = iniRW.get_Col5_1();
        GlobalVarLn.Col5_2 = iniRW.get_Col5_2();
        GlobalVarLn.Col5_3 = iniRW.get_Col5_3();
        // .....................................................................

    }
    // -------------------------------------------------------------------------

    else
    {
        //Azb
        if (GlobalVarLn.fl_Azb == 0)
        {
            MessageBox.Show("Невозможно открыть INI файл");
        }
        else if (GlobalVarLn.fl_Azb == 1)
                {
            MessageBox.Show("fayl açıq deyil");
        }
        else 
        {
            MessageBox.Show("Can’t open file INI");
        }

        return;
    }
    // -------------------------------------------------------------------------

} // Sector_INI
//************************************************************************
// Clear of Sectors
//************************************************************************

public static void  Sector_Clear()
{

/*
    // .....................................................................
    if (System.IO.File.Exists(Application.StartupPath + "\\Settings.ini"))
    {
        ;
    }
    else
    {
        //Azb
        if (GlobalVarLn.fl_Azb == 0)
        {
            MessageBox.Show("Невозможно открыть INI файл");
        }
        else
        {
            MessageBox.Show("fayl açıq deyil");
        }
        return;
    }
    // .....................................................................
    //GlobalVarLn.flsect = 0;
    // .....................................................................
    GlobalVarLn.objFormSectG.chbAnt1.Checked = false;
    GlobalVarLn.objFormSectG.chbAnt2.Checked = false;
    GlobalVarLn.objFormSectG.chbAnt3.Checked = false;
    GlobalVarLn.objFormSectG.chbAnt4.Checked = false;
    GlobalVarLn.objFormSectG.chbAnt5.Checked = false;
    // .....................................................................
    GlobalVarLn.fllSect1 = 0;
    GlobalVarLn.fllSect2 = 0;
    GlobalVarLn.fllSect3 = 0;
    GlobalVarLn.fllSect4 = 0;
    GlobalVarLn.fllSect5 = 0;
    // .....................................................................
    iniRW.write_flSect(GlobalVarLn.fllSect1, GlobalVarLn.fllSect2, GlobalVarLn.fllSect3, GlobalVarLn.fllSect4, GlobalVarLn.fllSect5);
    // .....................................................................
    // Убрать с карты
    GlobalVarLn.axMapScreenGlobal.Repaint();
    // .....................................................................
*/


    // .....................................................................
    GlobalVarLn.flsect = 0;
    GlobalVarLn.f_luch = 0;

    //GlobalVarLn.fllSect1 = 0;
    //GlobalVarLn.fllSect2 = 0;
    //GlobalVarLn.fllSect3 = 0;
    //GlobalVarLn.fllSect4 = 0;
    //GlobalVarLn.fllSect5 = 0;

    // .....................................................................
    // Убрать с карты
    GlobalVarLn.axMapScreenGlobal.Repaint();
    // .....................................................................


} // Sector_Clear
//************************************************************************
// Clear of Sectors(i)
//************************************************************************

public static void Sector_Clear_I(int ind)
{
/*
    // .....................................................................
    if (System.IO.File.Exists(Application.StartupPath + "\\Settings.ini"))
    {
        ;
    }
    else
    {
        //Azb
        if (GlobalVarLn.fl_Azb == 0)
        {
            MessageBox.Show("Невозможно открыть INI файл");
        }
        else
        {
            MessageBox.Show("fayl açıq deyil");
        }
        return;
    }
    // .....................................................................
    switch (ind)
    {
        case 1:
            GlobalVarLn.fllSect1 = 0;
            GlobalVarLn.objFormSectG.chbAnt1.Checked = false;
            break;
        case 2:
            GlobalVarLn.fllSect2 = 0;
            GlobalVarLn.objFormSectG.chbAnt2.Checked = false;
            break;
        case 3:
            GlobalVarLn.fllSect3 = 0;
            GlobalVarLn.objFormSectG.chbAnt3.Checked = false;
            break;
        case 4:
            GlobalVarLn.fllSect4 = 0;
            GlobalVarLn.objFormSectG.chbAnt4.Checked = false;
            break;
        case 5:
            GlobalVarLn.fllSect5 = 0;
            GlobalVarLn.objFormSectG.chbAnt5.Checked = false;
            break;
    };
    // .....................................................................
    iniRW.write_flSect(GlobalVarLn.fllSect1, GlobalVarLn.fllSect2, GlobalVarLn.fllSect3, GlobalVarLn.fllSect4, GlobalVarLn.fllSect5);
    // .....................................................................
    // Убрать с карты
    GlobalVarLn.axMapScreenGlobal.Repaint();
    // .....................................................................
*/

} // Sector_Clear_I
//************************************************************************
// Add of Sectors(i)
//************************************************************************

public static void Sector_Add_I(int ind)
{
/*
    // .....................................................................
    if (System.IO.File.Exists(Application.StartupPath + "\\Settings.ini"))
    {
        ;
    }
    else
    {
        //Azb
        if (GlobalVarLn.fl_Azb == 0)
        {
            MessageBox.Show("Невозможно открыть INI файл");
        }
        else
        {
            MessageBox.Show("fayl açıq deyil");
        }
        return;
    }
    // .....................................................................
    switch (ind)
    {
        case 1:
            GlobalVarLn.fllSect1 = 1;
            GlobalVarLn.objFormSectG.chbAnt1.Checked = true;
            break;
        case 2:
            GlobalVarLn.fllSect2 = 1;
            GlobalVarLn.objFormSectG.chbAnt2.Checked = true;
            break;
        case 3:
            GlobalVarLn.fllSect3 = 1;
            GlobalVarLn.objFormSectG.chbAnt3.Checked = true;
            break;
        case 4:
            GlobalVarLn.fllSect4 = 1;
            GlobalVarLn.objFormSectG.chbAnt4.Checked = true;
            break;
        case 5:
            GlobalVarLn.fllSect5 = 1;
            GlobalVarLn.objFormSectG.chbAnt5.Checked = true;
            break;
    };
    // .....................................................................
    iniRW.write_flSect(GlobalVarLn.fllSect1, GlobalVarLn.fllSect2, GlobalVarLn.fllSect3, GlobalVarLn.fllSect4, GlobalVarLn.fllSect5);
    // .....................................................................
    // Убрать с карты
    GlobalVarLn.axMapScreenGlobal.Repaint();
    // .....................................................................
*/

} // Sector_Add_I
        //************************************************************************



// ***************************************************************** SECTOR



// ***********************************************************************
// Расстояние между двумя точками с учетом кривизны Земли

// Входные параметры: 

// Lat1,Long1,Lat2,Long2 (град) - широта и долгота точек
// fl_84_42=1-> долгота и широта в WGS84, =2 -> SK42

// Выходные параметры: 
// возвращает расстояние в м

// Широта -90(юг)...+90(север)
// Долгота -180(на запад от Гринвича)...+180(на восток от Гринвича)
// ***********************************************************************

public static double f_D_2Points

    (
    // Широта и долгота стояния точек (град)
         double Lat1,
         double Long1,
         double Lat2,
         double Long2,

         int fl_84_42

    )
{

    // -----------------------------------------------------------------------
    double REarth = 0;
    double REarth_Lat = 0;
    double LatSr = 0;
    double LatRad1 = 0;
    double LatRad2 = 0;
    double LongRad1 = 0;
    double LongRad2 = 0;
    double dLat = 0;
    double dLong = 0;
    double dq = 0;
    double s = 0;
    // -----------------------------------------------------------------------
    // Средний радиус Земли (шар)

    if (fl_84_42 == 2) // Красовский
        REarth = 6371220;
    else
        REarth = 6378136.3; // WGS84
    // -----------------------------------------------------------------------
    // Широта и долгота (rad)

    LatRad1 = (Lat1 * Math.PI) / 180;   // grad->rad
    LatRad2 = (Lat2 * Math.PI) / 180;
    LongRad1 = (Long1 * Math.PI) / 180;
    LongRad2 = (Long2 * Math.PI) / 180;
    dLat = LatRad2 - LatRad1;
    dLong = LongRad2 - LongRad1;
    LatSr = (LatRad1 + LatRad2) / 2;
    // -----------------------------------------------------------------------
    // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 

    // m  Old
    //REarthP_Pel = REarth_Pel; // Шар

    // Сжатие=(a-b)/a
    // 0.003352 -Красовский
    if (fl_84_42 == 2) // Красовский
        REarth_Lat = REarth * (1 - 0.003352 * Math.Sin(LatSr) * Math.Sin(LatSr));
    else
        REarth_Lat = REarth * (1 - 0.00669438 * Math.Sin(LatSr) * Math.Sin(LatSr)); // WGS84
    // -----------------------------------------------------------------------
    // 1-й вариант

    dq = 2 * Math.Asin(Math.Sqrt(Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                                 Math.Cos(LatRad1) * Math.Cos(LatRad2) * Math.Sin(dLong / 2) * Math.Sin(dLong / 2)));
    //s = REarth_Lat* dq;
    // -----------------------------------------------------------------------
    // !!! 2-й вариант

    dq = Math.Atan(Math.Sqrt((Math.Cos(LatRad2) * Math.Sin(dLong)) * (Math.Cos(LatRad2) * Math.Sin(dLong)) +
                             (Math.Cos(LatRad1) * Math.Sin(LatRad2) - Math.Sin(LatRad1) * Math.Cos(LatRad2) * Math.Cos(dLong)) *
                             (Math.Cos(LatRad1) * Math.Sin(LatRad2) - Math.Sin(LatRad1) * Math.Cos(LatRad2) * Math.Cos(dLong))) /
                   (Math.Sin(LatRad1) * Math.Sin(LatRad2) + Math.Cos(LatRad1) * Math.Cos(LatRad2) * Math.Cos(dLong)));
    // -----------------------------------------------------------------------
    s = REarth_Lat * dq;
    // -----------------------------------------------------------------------

    return s;

} // Функция f_D_2Points
  //**************************************************************************

        // ForGetFromDB 33333333333333333333333333333333333333333333333333333333333333
        // 3

        // GetASP_DB *****************************************************************

        public static void GetASP_DB(List<TableASP> lstTableASP )
        {

            // --------------------------------------------------------------------------         
            bool flsp1 = false;
            bool flsp2 = false;
            bool fldraw = false;
            double dds = 250; // m
            int i = 0;
            int j = 0;
            // -------------------------------------------------------------------------- 

            // Find SP1 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            // FOR1

            for(i = 0; i < lstTableASP.Count; i++)
            {
                // ......................................................................
                // Find SP1

                if(
                   (lstTableASP[i].ISOwn==true)&&
                   (lstTableASP[i].Coordinates.Latitude!=-1)&&
                   (lstTableASP[i].Coordinates.Longitude!=-1)
                  )
                {
                    if (lstTableASP[i].Coordinates.Latitude < 0)
                        lstTableASP[i].Coordinates.Latitude = -lstTableASP[i].Coordinates.Latitude;
                    if (lstTableASP[i].Coordinates.Longitude < 0)
                        lstTableASP[i].Coordinates.Longitude = -lstTableASP[i].Coordinates.Longitude;

                    flsp1 = true;

                    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    // SP1 was on the map

                    if ((GlobalVarLn.BWGS84_1_sost != 0) && (GlobalVarLn.LWGS84_1_sost != 0))
                    {
                        // Aren't the same coordinates
                        if((lstTableASP[i].Coordinates.Latitude!= GlobalVarLn.BWGS84_1_sost) ||
                           (lstTableASP[i].Coordinates.Longitude != GlobalVarLn.LWGS84_1_sost))
                        {
                            double sss = 0;
                            sss = ClassMap.f_D_2Points(lstTableASP[i].Coordinates.Latitude, lstTableASP[i].Coordinates.Longitude,
                                                       GlobalVarLn.BWGS84_1_sost, GlobalVarLn.LWGS84_1_sost, 1);
                            // New pozition
                            if(sss>dds)
                            {
                                GlobalVarLn.BWGS84_1_sost = lstTableASP[i].Coordinates.Latitude;
                                GlobalVarLn.LWGS84_1_sost = lstTableASP[i].Coordinates.Longitude;

                                ClassMap.AddSP1();

                                fldraw = true;

                            } // s>250

                        } // new coordinates

                    } // // SP1 was on the map
                    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    // SP1 wasn't on the map

                    else
                    {
                        GlobalVarLn.BWGS84_1_sost = lstTableASP[i].Coordinates.Latitude;
                        GlobalVarLn.LWGS84_1_sost = lstTableASP[i].Coordinates.Longitude;

                        ClassMap.AddSP1();

                        fldraw = true;

                    } // SP1 wasn't on the map
                    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

                    // out from cicle
                    i = lstTableASP.Count + 1;

                } // find SP1
                // ......................................................................

            } // FOR1
            // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            // SP1 DON't find -> del it<if it was on the map (+pelengs, if they were on the map)

            if(flsp1==false)
            {
                // SP1 was on the map
                if ((GlobalVarLn.BWGS84_1_sost != 0) && (GlobalVarLn.LWGS84_1_sost != 0))
                {
                    ClassMap.DelSP1();
                    fldraw = true;

                } // // SP1 was on the map

            } // if(flsp1==false)
            // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Find SP1

            // Find SP2 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            // FOR2

            for (j = 0; j < lstTableASP.Count; j++)
            {
                // ......................................................................
                // Find SP2

                if (
                   (lstTableASP[j].ISOwn == false) &&
                   (lstTableASP[j].Coordinates.Latitude != -1) &&
                   (lstTableASP[j].Coordinates.Longitude != -1)
                  )
                {
                    if (lstTableASP[j].Coordinates.Latitude < 0)
                        lstTableASP[j].Coordinates.Latitude = -lstTableASP[j].Coordinates.Latitude;
                    if (lstTableASP[j].Coordinates.Longitude < 0)
                        lstTableASP[j].Coordinates.Longitude = -lstTableASP[j].Coordinates.Longitude;

                    flsp2 = true;

                    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    // SP2 was on the map

                    if ((GlobalVarLn.BWGS84_2_sost != 0) && (GlobalVarLn.LWGS84_2_sost != 0))
                    {
                        // Aren't the same coordinates
                        if ((lstTableASP[j].Coordinates.Latitude != GlobalVarLn.BWGS84_2_sost) ||
                           (lstTableASP[j].Coordinates.Longitude != GlobalVarLn.LWGS84_2_sost))
                        {
                            double sss1 = 0;
                            sss1 = ClassMap.f_D_2Points(lstTableASP[j].Coordinates.Latitude, lstTableASP[j].Coordinates.Longitude,
                                                       GlobalVarLn.BWGS84_2_sost, GlobalVarLn.LWGS84_2_sost, 1);
                            // New pozition
                            if (sss1 > dds)
                            {
                                GlobalVarLn.BWGS84_2_sost = lstTableASP[j].Coordinates.Latitude;
                                GlobalVarLn.LWGS84_2_sost = lstTableASP[j].Coordinates.Longitude;

                                ClassMap.AddSP2();

                                fldraw = true;

                            } // s>250

                        } // new coordinates

                    } // // SP2 was on the map
                    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    // SP2 wasn't on the map

                    else
                    {
                        GlobalVarLn.BWGS84_2_sost = lstTableASP[j].Coordinates.Latitude;
                        GlobalVarLn.LWGS84_2_sost = lstTableASP[j].Coordinates.Longitude;

                        ClassMap.AddSP2();

                        fldraw = true;

                    } // SP2 wasn't on the map
                    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

                    // out from cicle
                    j = lstTableASP.Count + 1;

                } // find SP2
                // ......................................................................

            } // FOR2
            // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            // SP2 DON't find -> del it<if it was on the map (+pelengs, if they were on the map)

            if (flsp2 == false)
            {
                // SP2 was on the map
                if ((GlobalVarLn.BWGS84_2_sost != 0) && (GlobalVarLn.LWGS84_2_sost != 0))
                {
                    ClassMap.DelSP2();
                    fldraw = true;

                } // // SP2 was on the map

            } // if(flsp2==false)
            // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Find SP2

            // --------------------------------------------------------------------------         
            if (fldraw==true)
                 GlobalVarLn.axMapScreenGlobal.Repaint();
            // --------------------------------------------------------------------------         

        }
        // ***************************************************************** GetASP_DB

        // AddSP1 ********************************************************************

        public static void AddSP1()
        {
            // ......................................................................
            double xtmp_ed = 0;
            double ytmp_ed = 0;
            double htmp_ed = 0;
            double xtmp1_ed = 0;
            double ytmp1_ed = 0;

            String s1 = "";
            // ......................................................................
            ClassMap objClassMap1_ed = new ClassMap();
            ClassMap objClassMap2_ed = new ClassMap();
            ClassMap objClassMap3_ed = new ClassMap();

            LF objLF = new LF();
            // ......................................................................
            GlobalVarLn.f42SP_sost = 0;
            GlobalVarLn.f42SP_G_sost = 0;
            GlobalVarLn.f84SP_G_sost = 0;
            // ......................................................................

            GlobalVarLn.objFormSostG.tbXRect.Text = Convert.ToString(GlobalVarLn.BWGS84_1_sost);
            GlobalVarLn.objFormSostG.tbYRect.Text = Convert.ToString(GlobalVarLn.LWGS84_1_sost);

            xtmp1_ed = GlobalVarLn.BWGS84_1_sost;
            ytmp1_ed = GlobalVarLn.LWGS84_1_sost;
            // ......................................................................
            // !!! Здесь как бы ручной ввод в WGS84

            GlobalVarLn.f84SP_G_sost = 1;
            // ......................................................................

            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLong
                (
                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLong_comm   // приращение по долготе, угл.сек
                );
            // ................................................................ dLong

            // dLat .................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLat
                (
                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLat_comm        // приращение по долготе, угл.сек
                );
            // ................................................................. dLat

            // Lat,Long .............................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objClassMap1_ed.f_WGS84_SK42_Lat_Long
                   (
                       // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       GlobalVarLn.dLat_comm,       // приращение по долготе, угл.сек
                       GlobalVarLn.dLong_comm,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref GlobalVarLn.LatKrG_comm,   // широта
                       ref GlobalVarLn.LongKrG_comm   // долгота
                   );
            // ............................................................ Lat,Long

            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // ......................................................................
            double lt1 = 0;
            double lng1 = 0;

            // grad->rad
            lt1 = (GlobalVarLn.LatKrG_comm * Math.PI) / 180;
            lng1 = (GlobalVarLn.LongKrG_comm * Math.PI) / 180;

            // Подаем rad, получаем там же расстояние на карте в м
            mapGeoToPlane(GlobalVarLn.hmapl, ref lt1, ref lng1);

            GlobalVarLn.XCenter_Sost = lt1;
            GlobalVarLn.YCenter_Sost = lng1;
            // ......................................................................

            // ......................................................................
            xtmp_ed = GlobalVarLn.XCenter_Sost;
            ytmp_ed = GlobalVarLn.YCenter_Sost;

            objLF.X_m = xtmp_ed;
            objLF.Y_m = ytmp_ed;
            // ......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(xtmp_ed, ytmp_ed);
            GlobalVarLn.HCenter_Sost = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            htmp_ed = GlobalVarLn.HCenter_Sost;
            GlobalVarLn.objFormSostG.tbOwnHeight.Text = Convert.ToString(htmp_ed);
            objLF.H_m = htmp_ed;
            // ......................................................................
            // Sign

            objLF.indzn = GlobalVarLn.indz1_Sost;
            // ......................................................................
            GlobalVarLn.fl_Sost = 1;
            GlobalVarLn.flCoordSP_Sost = 1; // СП1 выбрана

            // Peleng, FRCH
            if (GlobalVarLn.flPelMain == 1) // нажата птичка отрисовки пеленга
                GlobalVarLn.flag_eq_draw1 = 1;
            // ......................................................................
            // List

            GlobalVarLn.list1_Sost[0] = objLF;
            // ......................................................................
            // Draw SP1

            // ??????????????????
            // Убрать с карты
            //GlobalVarLn.axMapScreenGlobal.Repaint();
            // Redraw
            //ClassMap.f_Map_Redraw_Sost();

            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            if (GlobalVarLn.f84SP_G_sost == 0) // не ручной ввод
            {
                mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

                // rad(WGS84)->grad(WGS84)
                xtmp1_ed = (xtmp_ed * 180) / Math.PI;
                ytmp1_ed = (ytmp_ed * 180) / Math.PI;

                //333
                GlobalVarLn.BWGS84_1_sost = xtmp1_ed;
                GlobalVarLn.LWGS84_1_sost = ytmp1_ed;

                GlobalVarLn.XCenterGRAD_Dubl_Sost = xtmp1_ed;
                GlobalVarLn.YCenterGRAD_Dubl_Sost = ytmp1_ed;

            }
            // .......................................................................

            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            if ((GlobalVarLn.f42SP_sost == 0) && (GlobalVarLn.f42SP_G_sost == 0) && (GlobalVarLn.f84SP_G_sost == 0))
            {
                // dLong ..................................................................
                // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
                // (преобразования Молоденского), угл.сек

                objClassMap1_ed.f_dLong
                    (
                        // Входные параметры (град,км)
                        xtmp1_ed,   // широта
                        ytmp1_ed,  // долгота
                        0,     // высота

                        // DATUM,m
                        GlobalVarLn.dXdat_comm,
                        GlobalVarLn.dYdat_comm,
                        GlobalVarLn.dZdat_comm,

                        ref GlobalVarLn.dLong_comm   // приращение по долготе, угл.сек
                    );
                // ................................................................ dLong

                // dLat .................................................................
                // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
                // (преобразования Молоденского), угл.сек

                objClassMap1_ed.f_dLat
                    (
                        // Входные параметры (град,км)
                        xtmp1_ed,   // широта
                        ytmp1_ed,  // долгота
                        0,     // высота

                        // DATUM,m
                        GlobalVarLn.dXdat_comm,
                        GlobalVarLn.dYdat_comm,
                        GlobalVarLn.dZdat_comm,

                        ref GlobalVarLn.dLat_comm        // приращение по долготе, угл.сек
                    );
                // ................................................................. dLat

                // Lat,Long .............................................................
                // Преобразования широты и долготы при пересчете WGS84->SK42

                objClassMap1_ed.f_WGS84_SK42_Lat_Long
                       (
                           // Входные параметры (град,км)
                           xtmp1_ed,   // широта
                           ytmp1_ed,  // долгота
                           0,     // высота
                           GlobalVarLn.dLat_comm,       // приращение по долготе, угл.сек
                           GlobalVarLn.dLong_comm,      // приращение по долготе, угл.сек

                           // Выходные параметры (grad)
                           ref GlobalVarLn.LatKrG_comm,   // широта
                           ref GlobalVarLn.LongKrG_comm   // долгота
                       );
                // ............................................................ Lat,Long

            } // (GlobalVarLn.f42SP_sost == 0) && (GlobalVarLn.f42SP_G_sost == 0)77(GlobalVarLn.f84SP_G_sost==0)
            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            GlobalVarLn.LatKrR_comm = (GlobalVarLn.LatKrG_comm * Math.PI) / 180;
            GlobalVarLn.LongKrR_comm = (GlobalVarLn.LongKrG_comm * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LatKrG_comm,

                // Выходные параметры 
                ref GlobalVarLn.Lat_Grad_comm,
                ref GlobalVarLn.Lat_Min_comm,
                ref GlobalVarLn.Lat_Sec_comm
              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LongKrG_comm,

                // Выходные параметры 
                ref GlobalVarLn.Long_Grad_comm,
                ref GlobalVarLn.Long_Min_comm,
                ref GlobalVarLn.Long_Sec_comm
              );
            // .......................................................................

            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            if (GlobalVarLn.f42SP_sost == 0)
            {
                objClassMap3_ed.f_SK42_Krug
                       (
                           // Входные параметры (!!! grad)
                           // !!! эллипсоид Красовского
                           GlobalVarLn.LatKrG_comm,   // широта
                           GlobalVarLn.LongKrG_comm,  // долгота

                           // Выходные параметры (km)
                           ref GlobalVarLn.XSP42_comm,
                           ref GlobalVarLn.YSP42_comm
                       );

                // km->m
                GlobalVarLn.XSP42_comm = GlobalVarLn.XSP42_comm * 1000;
                GlobalVarLn.YSP42_comm = GlobalVarLn.YSP42_comm * 1000;

            } // GlobalVarLn.f42SP_sost == 0
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отобразить координаты в окошках

            ClassMap.OtobrSP1();
            // .......................................................................

            //if (ClickGetGNSS != null)
            //{
            //    if (GlobalVarLn.HCenter_Sost == -111111)
            //        GlobalVarLn.HCenter_Sost = -11111;
            //    ClickGetGNSS(GlobalVarLn.BWGS84_1_sost, GlobalVarLn.LWGS84_1_sost, GlobalVarLn.HCenter_Sost);
            //}
            // .......................................................................

        }
        // ******************************************************************** AddSP1

        // DelSP1 ********************************************************************

        public static void DelSP1()
        {
            // -----------------------------------------------------------------------
            // Для пеленгов

            // Нажата птичка для отрисовки пеленгов
            if (GlobalVarLn.flPelMain == 1)
            {
                // FRCH
                GlobalVarLn.PrevP1 = -1;
                GlobalVarLn.fl_PelIRI_1 = 0; // 1-й пеленг
                GlobalVarLn.flag_eq_draw1 = 0;

                GlobalVarLn.PELENGG_1 = -1;
                // --------------------------------------------------------------------
                //PPRCH

                for (int iii = (GlobalVarLn.list_PelIRI.Count - 1); iii >= 0; iii--)
                {

                    PelIRI[] mass = GlobalVarLn.list_PelIRI.ToArray();
                    mass[iii].Pel1 = -1;
                    GlobalVarLn.list_PelIRI = mass.ToList();

                    if (
                        (GlobalVarLn.list_PelIRI[iii].Pel1 == -1) &&
                        (GlobalVarLn.list_PelIRI[iii].Pel2 == -1) &&
                        (GlobalVarLn.list_PelIRI[iii].Lat == -1) &&
                        (GlobalVarLn.list_PelIRI[iii].Long == -1)
                       )
                    {
                        GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii]);
                    }
                }
                // -------------------------------------------------------------------

            } // Нажата птичка для отрисовки пеленгов
            // -----------------------------------------------------------------------

            ClassMap.ClearSP1();

            //ClickGetGNSS(-1, -1, -1);

        }
        // ******************************************************************** DelSP1

        // ClearSP1 ******************************************************************
        public static void ClearSP1()
        {

            // SP1
            GlobalVarLn.objFormSostG.tbXRect.Text = "";
            GlobalVarLn.objFormSostG.tbYRect.Text = "";
            GlobalVarLn.objFormSostG.tbXRect42.Text = "";
            GlobalVarLn.objFormSostG.tbYRect42.Text = "";
            GlobalVarLn.objFormSostG.tbBRad.Text = "";
            GlobalVarLn.objFormSostG.tbLRad.Text = "";
            GlobalVarLn.objFormSostG.tbBMin1.Text = "";
            GlobalVarLn.objFormSostG.tbLMin1.Text = "";
            GlobalVarLn.objFormSostG.tbBDeg2.Text = "";
            GlobalVarLn.objFormSostG.tbBMin2.Text = "";
            GlobalVarLn.objFormSostG.tbBSec.Text = "";
            GlobalVarLn.objFormSostG.tbLDeg2.Text = "";
            GlobalVarLn.objFormSostG.tbLMin2.Text = "";
            GlobalVarLn.objFormSostG.tbLSec.Text = "";

            GlobalVarLn.objFormSostG.tbOwnHeight.Text = "";
            // ...................................................................
            // переменные

            GlobalVarLn.flCoordSP_Sost = 0; // =1-> Выбрали СП

            GlobalVarLn.XCenterGRAD_Sost = -1;
            GlobalVarLn.YCenterGRAD_Sost = -1;
            GlobalVarLn.XCenterGRAD_Dubl_Sost = -1;
            GlobalVarLn.YCenterGRAD_Dubl_Sost = -1;

            GlobalVarLn.XCenter_Sost = 0;
            GlobalVarLn.YCenter_Sost = 0;
            GlobalVarLn.HCenter_Sost = 0;

            // 3 !!!
            GlobalVarLn.BWGS84_1_sost = 0;
            GlobalVarLn.LWGS84_1_sost = 0;
        // ...................................................................

        // ----------------------------------------------------------------------
        LF objLF = new LF();
            LF objLF2 = new LF();
            LF objLF3 = new LF();

            // SP1
            objLF.X_m = 0;
            objLF.Y_m = 0;
            objLF.H_m = 0;
            // SP2
            objLF2.X_m = GlobalVarLn.list1_Sost[1].X_m;
            objLF2.Y_m = GlobalVarLn.list1_Sost[1].Y_m;
            objLF2.H_m = GlobalVarLn.list1_Sost[1].H_m;
            objLF2.indzn = GlobalVarLn.list1_Sost[1].indzn;
            // PU
            objLF3.X_m = GlobalVarLn.list1_Sost[2].X_m;
            objLF3.Y_m = GlobalVarLn.list1_Sost[2].Y_m;
            objLF3.H_m = GlobalVarLn.list1_Sost[2].H_m;
            objLF3.indzn = GlobalVarLn.list1_Sost[2].indzn;

            GlobalVarLn.list1_Sost.Clear();
            GlobalVarLn.list1_Sost.Add(objLF);
            GlobalVarLn.list1_Sost.Add(objLF2);
            GlobalVarLn.list1_Sost.Add(objLF3);
            // ----------------------------------------------------------------------
            // SP1
            GlobalVarLn.objFormSostG.gbOwnRect.Enabled = false;
            GlobalVarLn.objFormSostG.gbOwnRect42.Enabled = false;
            GlobalVarLn.objFormSostG.gbOwnRad.Enabled = false;
            GlobalVarLn.objFormSostG.gbOwnDegMin.Enabled = false;
            GlobalVarLn.objFormSostG.gbOwnDegMinSec.Enabled = false;

            // Убрать с карты
            //GlobalVarLn.axMapScreenGlobal.Repaint();
            // ---------------------------------------------------------------------

        }
        // ****************************************************************** ClearSP1

        // AddSP2 ********************************************************************

        public static void AddSP2()
        {
            // ......................................................................
            double xtmp_ed = 0;
            double ytmp_ed = 0;
            double htmp_ed = 0;
            double xtmp1_ed = 0;
            double ytmp1_ed = 0;

            String s1 = "";
            // ......................................................................
            ClassMap objClassMap4_ed = new ClassMap();
            ClassMap objClassMap5_ed = new ClassMap();
            ClassMap objClassMap6_ed = new ClassMap();

            LF objLF = new LF();
            // ......................................................................
            GlobalVarLn.f42SP1_sost = 0;
            GlobalVarLn.f42SP1_G_sost = 0;
            GlobalVarLn.f84SP1_G_sost = 0;
            // ......................................................................
            GlobalVarLn.objFormSostG.tbPt1XRect.Text = Convert.ToString(GlobalVarLn.BWGS84_2_sost);
            GlobalVarLn.objFormSostG.tbPt1YRect.Text = Convert.ToString(GlobalVarLn.LWGS84_2_sost);

            xtmp1_ed = GlobalVarLn.BWGS84_2_sost;
            ytmp1_ed = GlobalVarLn.LWGS84_2_sost;
            // ......................................................................
            // !!! Здесь как бы ручной ввод в WGS84

            GlobalVarLn.f84SP1_G_sost = 1;
            // ......................................................................

            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap4_ed.f_dLong
                (
                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLong_comm   // приращение по долготе, угл.сек
                );
            // ................................................................ dLong

            // dLat .................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap4_ed.f_dLat
                (
                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    GlobalVarLn.dXdat_comm,
                    GlobalVarLn.dYdat_comm,
                    GlobalVarLn.dZdat_comm,

                    ref GlobalVarLn.dLat_comm        // приращение по долготе, угл.сек
                );
            // ................................................................. dLat

            // Lat,Long .............................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objClassMap4_ed.f_WGS84_SK42_Lat_Long
                   (
                       // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       GlobalVarLn.dLat_comm,       // приращение по долготе, угл.сек
                       GlobalVarLn.dLong_comm,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref GlobalVarLn.LatKrG_YS1_comm,   // широта
                       ref GlobalVarLn.LongKrG_YS1_comm   // долгота
                   );
            // ............................................................ Lat,Long

            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // ......................................................................
            double lt1 = 0;
            double lng1 = 0;

            // grad->rad
            lt1 = (GlobalVarLn.LatKrG_YS1_comm * Math.PI) / 180;
            lng1 = (GlobalVarLn.LongKrG_YS1_comm * Math.PI) / 180;

            // Подаем rad, получаем там же расстояние на карте в м
            mapGeoToPlane(GlobalVarLn.hmapl, ref lt1, ref lng1);

            GlobalVarLn.XPoint1_Sost = lt1;
            GlobalVarLn.YPoint1_Sost = lng1;
            // ......................................................................

            // ......................................................................
            xtmp_ed = GlobalVarLn.XPoint1_Sost;
            ytmp_ed = GlobalVarLn.YPoint1_Sost;

            objLF.X_m = xtmp_ed;
            objLF.Y_m = ytmp_ed;
            // ......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(xtmp_ed, ytmp_ed);
            GlobalVarLn.HPoint1_Sost = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            htmp_ed = GlobalVarLn.HPoint1_Sost;
            GlobalVarLn.objFormSostG.tbPt1Height.Text = Convert.ToString(htmp_ed);
            objLF.H_m = htmp_ed;
            // ......................................................................
            // Sign

            objLF.indzn = GlobalVarLn.indz2_Sost;
            // ......................................................................
            // List

            GlobalVarLn.list1_Sost[1] = objLF;
            // ......................................................................
            GlobalVarLn.flCoordYS1_Sost = 1; // SP2 выбрана
            GlobalVarLn.fl_Sost = 1;

            // Peleng, FRCH
            if (GlobalVarLn.flPelMain == 1) // нажата птичка отрисовки пеленга
                GlobalVarLn.flag_eq_draw2 = 1;
            // ......................................................................

            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            if (GlobalVarLn.f84SP1_G_sost == 0) // не ручной ввод
            {
                mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

                // rad(WGS84)->grad(WGS84)
                xtmp1_ed = (xtmp_ed * 180) / Math.PI;
                ytmp1_ed = (ytmp_ed * 180) / Math.PI;

                GlobalVarLn.BWGS84_2_sost = xtmp1_ed;
                GlobalVarLn.LWGS84_2_sost = ytmp1_ed;

                GlobalVarLn.XPoint1GRAD_Dubl_Sost = xtmp1_ed;
                GlobalVarLn.YPoint1GRAD_Dubl_Sost = ytmp1_ed;
            }
            // .......................................................................

            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            if ((GlobalVarLn.f42SP1_sost == 0) && (GlobalVarLn.f42SP1_G_sost == 0) && (GlobalVarLn.f84SP1_G_sost == 0))
            {
                // dLong ..................................................................
                // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
                // (преобразования Молоденского), угл.сек

                objClassMap4_ed.f_dLong
                    (
                        // Входные параметры (град,км)
                        xtmp1_ed,   // широта
                        ytmp1_ed,  // долгота
                        0,     // высота

                        // DATUM,m
                        GlobalVarLn.dXdat_comm,
                        GlobalVarLn.dYdat_comm,
                        GlobalVarLn.dZdat_comm,

                        ref GlobalVarLn.dLong_comm   // приращение по долготе, угл.сек
                    );
                // ................................................................ dLong

                // dLat .................................................................
                // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
                // (преобразования Молоденского), угл.сек

                objClassMap4_ed.f_dLat
                    (
                        // Входные параметры (град,км)
                        xtmp1_ed,   // широта
                        ytmp1_ed,  // долгота
                        0,     // высота

                        // DATUM,m
                        GlobalVarLn.dXdat_comm,
                        GlobalVarLn.dYdat_comm,
                        GlobalVarLn.dZdat_comm,

                        ref GlobalVarLn.dLat_comm        // приращение по долготе, угл.сек
                    );
                // ................................................................. dLat

                // Lat,Long .............................................................
                // Преобразования широты и долготы при пересчете WGS84->SK42

                objClassMap4_ed.f_WGS84_SK42_Lat_Long
                       (
                           // Входные параметры (град,км)
                           xtmp1_ed,   // широта
                           ytmp1_ed,  // долгота
                           0,     // высота
                           GlobalVarLn.dLat_comm,       // приращение по долготе, угл.сек
                           GlobalVarLn.dLong_comm,      // приращение по долготе, угл.сек

                           // Выходные параметры (grad)
                           ref GlobalVarLn.LatKrG_YS1_comm,   // широта
                           ref GlobalVarLn.LongKrG_YS1_comm   // долгота
                       );
                // ............................................................ Lat,Long

            } // (GlobalVarLn.f42SP1_sost == 0) && (GlobalVarLn.f42SP1_G_sost == 0)
            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            GlobalVarLn.LatKrR_YS1_comm = (GlobalVarLn.LatKrG_YS1_comm * Math.PI) / 180;
            GlobalVarLn.LongKrR_YS1_comm = (GlobalVarLn.LongKrG_YS1_comm * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap5_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LatKrG_YS1_comm,

                // Выходные параметры 
                ref GlobalVarLn.Lat_Grad_YS1_comm,
                ref GlobalVarLn.Lat_Min_YS1_comm,
                ref GlobalVarLn.Lat_Sec_YS1_comm
              );

            // Долгота
            objClassMap5_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                GlobalVarLn.LongKrG_YS1_comm,

                // Выходные параметры 
                ref GlobalVarLn.Long_Grad_YS1_comm,
                ref GlobalVarLn.Long_Min_YS1_comm,
                ref GlobalVarLn.Long_Sec_YS1_comm
              );
            // .......................................................................

            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            if (GlobalVarLn.f42SP1_sost == 0)
            {

                objClassMap6_ed.f_SK42_Krug
                       (
                           // Входные параметры (!!! grad)
                           // !!! эллипсоид Красовского
                           GlobalVarLn.LatKrG_YS1_comm,   // широта
                           GlobalVarLn.LongKrG_YS1_comm,  // долгота

                           // Выходные параметры (km)
                           ref GlobalVarLn.XYS142_comm,
                           ref GlobalVarLn.YYS142_comm
                       );

                // km->m
                GlobalVarLn.XYS142_comm = GlobalVarLn.XYS142_comm * 1000;
                GlobalVarLn.YYS142_comm = GlobalVarLn.YYS142_comm * 1000;

            } // GlobalVarLn.f42SP1_sost == 0
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отобразить координаты в окошках

            ClassMap.OtobrSP2();
            // .......................................................................

            //if (ClickGetGNSS2 != null)
            //{
            //    if (GlobalVarLn.HPoint1_Sost == -111111)
            //        GlobalVarLn.HPoint1_Sost = -11111;
            //
            //    ClickGetGNSS2(GlobalVarLn.BWGS84_2_sost, GlobalVarLn.LWGS84_2_sost, GlobalVarLn.HPoint1_Sost);
            //}
            // .......................................................................

        }
        // ******************************************************************** AddSP2

        // DelSP2 ********************************************************************

        public static void DelSP2()
        {
            // -----------------------------------------------------------------------
            // Для пеленгов

            try {
                // Нажата птичка для отрисовки пеленгов
                if (GlobalVarLn.flPelMain == 1)
                {
                    // FRCH
                    GlobalVarLn.PrevP2 = -1;
                    GlobalVarLn.flPelMain2 = 0;   // 2-й пеленг
                    GlobalVarLn.flag_eq_draw2 = 0;

                    GlobalVarLn.PELENGG_2 = -1;
                    // --------------------------------------------------------------------
                    //PPRCH

                    for (int iii = (GlobalVarLn.list_PelIRI.Count - 1); iii >= 0; iii--)
                    {

                        PelIRI[] mass = GlobalVarLn.list_PelIRI.ToArray();
                        mass[iii].Pel2 = -1;
                        GlobalVarLn.list_PelIRI = mass.ToList();

                        if (
                            (GlobalVarLn.list_PelIRI[iii].Pel1 == -1) &&
                            (GlobalVarLn.list_PelIRI[iii].Pel2 == -1) &&
                            (GlobalVarLn.list_PelIRI[iii].Lat == -1) &&
                            (GlobalVarLn.list_PelIRI[iii].Long == -1)
                           )
                        {
                            GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii]);
                        }
                    }
                    // -------------------------------------------------------------------

                } // Нажата птичка для отрисовки пеленгов
                  // -----------------------------------------------------------------------

                ClassMap.ClearSP2();

                // ??????????
                //ClickGetGNSS2(-1, -1, -1);

            }
            catch { }
        }

        // ******************************************************************** DelSP2

        // ClearSP2 ******************************************************************
        public static void ClearSP2()
        {

            // SP2
            GlobalVarLn.objFormSostG.tbPt1XRect.Text = "";
            GlobalVarLn.objFormSostG.tbPt1YRect.Text = "";
            GlobalVarLn.objFormSostG.tbPt1XRect42.Text = "";
            GlobalVarLn.objFormSostG.tbPt1YRect42.Text = "";
            GlobalVarLn.objFormSostG.tbPt1BRad.Text = "";
            GlobalVarLn.objFormSostG.tbPt1LRad.Text = "";
            GlobalVarLn.objFormSostG.tbPt1BMin1.Text = "";
            GlobalVarLn.objFormSostG.tbPt1LMin1.Text = "";
            GlobalVarLn.objFormSostG.tbPt1BDeg2.Text = "";
            GlobalVarLn.objFormSostG.tbPt1BMin2.Text = "";
            GlobalVarLn.objFormSostG.tbPt1BSec.Text = "";
            GlobalVarLn.objFormSostG.tbPt1LDeg2.Text = "";
            GlobalVarLn.objFormSostG.tbPt1LMin2.Text = "";
            GlobalVarLn.objFormSostG.tbPt1LSec.Text = "";

            GlobalVarLn.objFormSostG.tbPt1Height.Text = "";
            // ...................................................................
            // переменные

            GlobalVarLn.flCoordYS1_Sost = 0;

            GlobalVarLn.XPoint1GRAD_Sost = -1;
            GlobalVarLn.YPoint1GRAD_Sost = -1;
            GlobalVarLn.XPoint1GRAD_Dubl_Sost = -1;
            GlobalVarLn.YPoint1GRAD_Dubl_Sost = -1;

            // 3 !!!
            GlobalVarLn.BWGS84_2_sost = 0;
            GlobalVarLn.LWGS84_2_sost = 0;

            GlobalVarLn.XPoint1_Sost = 0;
            GlobalVarLn.YPoint1_Sost = 0;
            GlobalVarLn.HPoint1_Sost = 0;
            // ...................................................................

            // -------------------------------------------------------------------
            LF objLF = new LF();
            LF objLF2 = new LF();
            LF objLF3 = new LF();

            // SP1
            objLF.X_m = GlobalVarLn.list1_Sost[0].X_m;
            objLF.Y_m = GlobalVarLn.list1_Sost[0].Y_m;
            objLF.H_m = GlobalVarLn.list1_Sost[0].H_m;
            objLF.indzn = GlobalVarLn.list1_Sost[0].indzn;
            // SP2
            objLF2.X_m = 0;
            objLF2.Y_m = 0;
            objLF2.H_m = 0;
            // PU
            objLF3.X_m = GlobalVarLn.list1_Sost[2].X_m;
            objLF3.Y_m = GlobalVarLn.list1_Sost[2].Y_m;
            objLF3.H_m = GlobalVarLn.list1_Sost[2].H_m;
            objLF3.indzn = GlobalVarLn.list1_Sost[2].indzn;

            GlobalVarLn.list1_Sost.Clear();
            GlobalVarLn.list1_Sost.Add(objLF);
            GlobalVarLn.list1_Sost.Add(objLF2);
            GlobalVarLn.list1_Sost.Add(objLF3);

            // ----------------------------------------------------------------------
            // SP2
            GlobalVarLn.objFormSostG.gbPt1Rect.Enabled = false;
            GlobalVarLn.objFormSostG.gbPt1Rect42.Enabled = false;
            GlobalVarLn.objFormSostG.gbPt1Rad.Enabled = false;
            GlobalVarLn.objFormSostG.gbPt1DegMin.Enabled = false;
            GlobalVarLn.objFormSostG.gbPt1DegMinSec.Enabled = false;

            // Убрать с карты
            //GlobalVarLn.axMapScreenGlobal.Repaint();
            // ---------------------------------------------------------------------

        }
        // ****************************************************************** ClearSP2

        // GetADSB_DB ****************************************************************

        public static void GetADSB_DB(List<TempADSB> lstTableADSB)
        {
            // ---------------------------------------------------------------------
            int ii = 0;
            int ii1 = 0;
            int fi = 0;
            String s1 = "";

            bool fldraw = false;
            // ---------------------------------------------------------------------
            try
            {
                // Airplanes are existing >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                if (lstTableADSB.Count != 0)
                {

                    // ................................................................
                    // 1-й сеанс получения данных

                    if (GlobalVarLn.fl_AirPlane == 0)
                    {
                        GlobalVarLn.fl_AirPlane = 1;

                        //if (MapForm.chbair.Checked == true)
                        //    GlobalVarLn.fl_AirPlaneVisible = 1;

                        for (ii = 0; ii < lstTableADSB.Count; ii++)
                        {
                            // Добавляем 1-й цикл структур в лист
                            GlobalVarLn.Lat_air = lstTableADSB[ii].Coordinates.Latitude;
                            GlobalVarLn.Long_air = lstTableADSB[ii].Coordinates.Longitude;
                            GlobalVarLn.sNum_air = lstTableADSB[ii].ICAO;

                            fldraw = ClassMap.AddAirPlaneBD(
                                                   GlobalVarLn.Lat_air,
                                                   GlobalVarLn.Long_air,
                                                   GlobalVarLn.sNum_air,
                                                   0  // 1-й раз
                                                  );

                        } // FOR

                        fldraw = true;

                        //if (chbair.Checked == true)
                        //{
                        //    // Перерисовка самолетов
                        //    ClassMap.f_ReDrawAirPlane1();
                        //}

                    } // fl_AirPlane == 0
                      // ................................................................
                      // НЕ 1-й сеанс получения данных

                    else
                    {

                        // FOR1 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // Проверить на исчезновение самолетов (Если в текущей партии какого-то самолета
                        // нет, значит он исчез)

                        // FOR1 (Свой List с конца)
                        for (ii = (GlobalVarLn.Number_air - 1); ii >= 0; ii--)
                        {

                            fi = 0;
                            // FOR2
                            for (ii1 = 0; ii1 < lstTableADSB.Count; ii1++)
                            {
                                s1 = String.Copy(lstTableADSB[ii1].ICAO);
                                if (String.Compare(GlobalVarLn.list_air[ii].sNum, s1) == 0)
                                    fi = 1;
                            } // FOR2

                            if (fi == 0)  // Этого самолета уже нет
                            {
                                ClassMap.DelAirPlaneBD(GlobalVarLn.list_air[ii].sNum);
                                fldraw = true;
                            }

                        } // FOR1
                          // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FOR1

                        // FOR2 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        // FOR2
                        for (ii = 0; ii < lstTableADSB.Count; ii++)
                        {
                            // Добавляем цикл структур в лист
                            GlobalVarLn.Lat_air = lstTableADSB[ii].Coordinates.Latitude;
                            GlobalVarLn.Long_air = lstTableADSB[ii].Coordinates.Longitude;
                            GlobalVarLn.sNum_air = lstTableADSB[ii].ICAO;

                            fldraw = ClassMap.AddAirPlaneBD(
                                                   GlobalVarLn.Lat_air,
                                                   GlobalVarLn.Long_air,
                                                   GlobalVarLn.sNum_air,
                                                   1
                                                  );


                        } // FOR2
                          // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FOR2

                    } // fl_AirPlane == 1 -> НЕ 1-й сеанс получения данных
                      // ................................................................

                } // if(lstTableADSB.Count!=0)
                  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Airplanes are existing

                // NO Airplanes  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                else
                {
                    if (GlobalVarLn.list_air.Count != 0)
                    {
                        ClassMap.DelAirPlanes();
                        fldraw = true;
                    }

                } // if(lstTableADSB.Count==0)
                  //  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> NO Airplanes

                // --------------------------------------------------------------------------         
                if (
                    (fldraw == true) &&
                    (GlobalVarLn.fl_AirPlaneVisible == 1)
                   )
                {
                    GlobalVarLn.axMapScreenGlobal.Repaint();
                }
                // --------------------------------------------------------------------------         

            }
            catch { }
        }
            // **************************************************************** GetADSB_DB

            // GetADSB_DB1 ****************************************************************
            // Air

            public static void GetADSB_DB1(List<TempADSB> lstTableADSB)
        {
            // ---------------------------------------------------------------------
            int ii = 0;
            int ii1 = 0;
            int fi = 0;
            String s1 = "";

            bool fldraw = false;
            // ---------------------------------------------------------------------

            // Airplanes are existing >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            if (lstTableADSB.Count != 0)
            {

                // ................................................................
                // 1-й сеанс получения данных

                if (GlobalVarLn.fl_AirPlane == 0)
                {
                    GlobalVarLn.fl_AirPlane = 1;

                    //if (MapForm.chbair.Checked == true)
                    //    GlobalVarLn.fl_AirPlaneVisible = 1;

                    for (ii = 0; ii < lstTableADSB.Count; ii++)
                    {
                        // Добавляем 1-й цикл структур в лист
                        GlobalVarLn.Lat_air = lstTableADSB[ii].Coordinates.Latitude;
                        GlobalVarLn.Long_air = lstTableADSB[ii].Coordinates.Longitude;
                        GlobalVarLn.sNum_air = lstTableADSB[ii].ICAO;

                        fldraw = ClassMap.AddAirPlaneBD1(
                                               GlobalVarLn.Lat_air,
                                               GlobalVarLn.Long_air,
                                               GlobalVarLn.sNum_air,
                                               0  // 1-й раз
                                              );

                    } // FOR

                    fldraw = true;

                    //if (chbair.Checked == true)
                    //{
                    //    // Перерисовка самолетов
                    //    ClassMap.f_ReDrawAirPlane1();
                    //}

                } // fl_AirPlane == 0
                // ................................................................
                // НЕ 1-й сеанс получения данных

                else
                {

                    // FOR1 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // Проверить на исчезновение самолетов (Если в текущей партии какого-то самолета
                    // нет, значит он исчез)

                    // FOR1 (Свой List с конца)
                    for (ii = (GlobalVarLn.Number_air - 1); ii >= 0; ii--)
                    {

                        fi = 0;
                        // FOR2
                        for (ii1 = 0; ii1 < lstTableADSB.Count; ii1++)
                        {
                            s1 = String.Copy(lstTableADSB[ii1].ICAO);
                            if (String.Compare(GlobalVarLn.list_ClassAirPlane[ii].sNum, s1) == 0)
                                fi = 1;
                        } // FOR2

                        if (fi == 0)  // Этого самолета уже нет
                        {
                            ClassMap.DelAirPlaneBD1(GlobalVarLn.list_ClassAirPlane[ii].sNum);
                            fldraw = true;
                        }

                    } // FOR1
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FOR1

                    // FOR2 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                    // FOR2
                    for (ii = 0; ii < lstTableADSB.Count; ii++)
                    {
                        // Добавляем цикл структур в лист
                        GlobalVarLn.Lat_air = lstTableADSB[ii].Coordinates.Latitude;
                        GlobalVarLn.Long_air = lstTableADSB[ii].Coordinates.Longitude;
                        GlobalVarLn.sNum_air = lstTableADSB[ii].ICAO;

                        fldraw = ClassMap.AddAirPlaneBD1(
                                               GlobalVarLn.Lat_air,
                                               GlobalVarLn.Long_air,
                                               GlobalVarLn.sNum_air,
                                               1
                                              );


                    } // FOR2
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FOR2

                } // fl_AirPlane == 1 -> НЕ 1-й сеанс получения данных
                // ................................................................

            } // if(lstTableADSB.Count!=0)
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Airplanes are existing

            // NO Airplanes  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            else
            {
                if (GlobalVarLn.list_ClassAirPlane.Count != 0)
                {
                    ClassMap.DelAirPlanes1();
                    fldraw = true;
                }

            } // if(lstTableADSB.Count==0)
            //  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> NO Airplanes

            // --------------------------------------------------------------------------         
            if (
                (fldraw == true) &&
                (GlobalVarLn.fl_AirPlaneVisible == 1)
               )
            {
                GlobalVarLn.axMapScreenGlobal.Repaint();
            }
            // --------------------------------------------------------------------------         

        }
        // **************************************************************** GetADSB_DB1

        // GetADSB_DB2 ****************************************************************
        // Air

        public static void GetADSB_DB2(List<TempADSB> lstTableADSB)
        {
            // ---------------------------------------------------------------------
            int ii = 0;
            int ii1 = 0;
            int fi = 0;
            String s1 = "";
            String sICAO = "";
            double lat = 0;
            double lon = 0;

            bool fldraw = false;
            bool f = false;
            // ---------------------------------------------------------------------
            try
            {
                // Airplanes are existing >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                if (lstTableADSB.Count != 0)
                {

                    // 1-й сеанс ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    // 1-й сеанс получения данных

                    // IF1
                    if (GlobalVarLn.fl_AirPlane == 0)
                    {

                        GlobalVarLn.fl_AirPlane = 1;

                        // 1-й раз идут для одного ICAO несколько записей, напримкер 
                        // ICAO1,coord1 ICAO1,coord2... ICAO2,coord1,...  

                        // WHILE1: Формируем исходный List самолетов
                        while (lstTableADSB.Count != 0)
                        {
                            GlobalVarLn.sNum_air = lstTableADSB[0].ICAO; // ICAO
                            sICAO = lstTableADSB[0].ICAO; // ICAO
                            GlobalVarLn.Lat_air = lstTableADSB[0].Coordinates.Latitude;
                            GlobalVarLn.Long_air = lstTableADSB[0].Coordinates.Longitude;
                            fldraw = ClassMap.AddAirPlaneBD2(
                                                            GlobalVarLn.Lat_air,
                                                            GlobalVarLn.Long_air,
                                                            GlobalVarLn.sNum_air,
                                                            0 // 1-й раз
                                                            );
                            lstTableADSB.Remove(lstTableADSB[0]);

                            // FOR1 входной List: Добавляем все с выбранным ICAO в новый лист
                            for (ii = 0; ii < lstTableADSB.Count; ii++)
                            {
                                // Ищем все выбранные ICAO
                                // IF2
                                if (
                                  String.Compare(lstTableADSB[ii].ICAO, sICAO) == 0
                                   )
                                {
                                    GlobalVarLn.sNum_air = lstTableADSB[ii].ICAO; // ICAO
                                    GlobalVarLn.Lat_air = lstTableADSB[ii].Coordinates.Latitude;
                                    GlobalVarLn.Long_air = lstTableADSB[ii].Coordinates.Longitude;
                                    fldraw = ClassMap.AddAirPlaneBD2(
                                                                    GlobalVarLn.Lat_air,
                                                                    GlobalVarLn.Long_air,
                                                                    GlobalVarLn.sNum_air,
                                                                    1 // НЕ 1-й раз
                                                                    );
                                } // IF2 

                            } // FOR1 входной List

                            // FOR2 входной List: Убираем все с выбранным ICAO во входном листе
                            for (ii1 = lstTableADSB.Count - 1; ii1 >= 0; ii1--)
                            {
                                // Ищем все выбранные ICAO
                                // IF
                                if (
                                  String.Compare(lstTableADSB[ii1].ICAO, sICAO) == 0
                                   )
                                {
                                    lstTableADSB.Remove(lstTableADSB[ii1]);
                                } // IF 

                            } // FOR2 входной List

                        }; // WHILE1: (Count входного листа !=0)


                    } // IF1: fl_AirPlane == 0
                      // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 1-й сеанс

                    // НЕ 1-й сеанс +++++++++++++++++++++++++++++++++++++++++++++++++++++
                    // ................................................................
                    // НЕ 1-й сеанс получения данных

                    // ELSE on IF1
                    else
                    {
                        // WHILE2: Добавляем новые координаты для ICAOi-ых самолетов, если они есть
                        while (lstTableADSB.Count != 0)
                        {
                            GlobalVarLn.sNum_air = lstTableADSB[0].ICAO; // ICAO
                            sICAO = lstTableADSB[0].ICAO; // ICAO
                            GlobalVarLn.Lat_air = lstTableADSB[0].Coordinates.Latitude;
                            GlobalVarLn.Long_air = lstTableADSB[0].Coordinates.Longitude;
                            fldraw = ClassMap.AddAirPlaneBD2(
                                                            GlobalVarLn.Lat_air,
                                                            GlobalVarLn.Long_air,
                                                            GlobalVarLn.sNum_air,
                                                            1 // НЕ 1-й раз
                                                            );
                            lstTableADSB.Remove(lstTableADSB[0]);

                            // FOR1_1 входной List: Добавляем все с выбранным ICAO в новый лист
                            for (ii = 0; ii < lstTableADSB.Count; ii++)
                            {
                                // Ищем все выбранные ICAO
                                // IF2
                                if (
                                  String.Compare(lstTableADSB[ii].ICAO, sICAO) == 0
                                   )
                                {
                                    GlobalVarLn.sNum_air = lstTableADSB[ii].ICAO; // ICAO
                                    GlobalVarLn.Lat_air = lstTableADSB[ii].Coordinates.Latitude;
                                    GlobalVarLn.Long_air = lstTableADSB[ii].Coordinates.Longitude;
                                    fldraw = ClassMap.AddAirPlaneBD2(
                                                                    GlobalVarLn.Lat_air,
                                                                    GlobalVarLn.Long_air,
                                                                    GlobalVarLn.sNum_air,
                                                                    1 // НЕ 1-й раз
                                                                    );
                                } // IF2 

                            } // FOR1_1 входной List

                            // FOR2_1 входной List: Убираем все с выбранным ICAO во входном листе
                            for (ii1 = lstTableADSB.Count - 1; ii1 >= 0; ii1--)
                            {
                                // Ищем все выбранные ICAO
                                // IF
                                if (
                                  String.Compare(lstTableADSB[ii1].ICAO, sICAO) == 0
                                   )
                                {
                                    lstTableADSB.Remove(lstTableADSB[ii1]);
                                } // IF 

                            } // FOR2_1 входной List

                        }; // WHILE2: (Count входного листа !=0)

                    } // ELSE on IF1: fl_AirPlane == 1 -> НЕ 1-й сеанс получения данных
                      // ................................................................

                    // +++++++++++++++++++++++++++++++++++++++++++++++++++++ НЕ 1-й сеанс

                } // if(lstTableADSB.Count!=0)
                  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Airplanes are existing

                // NO Airplanes  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                else
                {
                    if (GlobalVarLn.list_ClassAirPlane.Count != 0)
                    {
                        ClassMap.DelAirPlanes2();
                        fldraw = true;
                    }

                } // if(lstTableADSB.Count==0)
                  //  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> NO Airplanes

                // --------------------------------------------------------------------------   
                // !!! Перерисовка по Tick Timer1


                /*
                            if (
                                (fldraw == true) &&
                                (GlobalVarLn.fl_AirPlaneVisible == 1)
                               )
                            {
                                GlobalVarLn.axMapScreenGlobal.Repaint();
                            }
                            // --------------------------------------------------------------------------         
                */

            }
            catch { }
        }
            // **************************************************************** GetADSB_DB2

            // DelAllAirPlanes *******************************************************

            public static void DelAirPlanes()
        {
            try
            {
                int iij = 0;

                // FOR1: Убрать все 
                for (iij = (GlobalVarLn.Number_air - 1); iij >= 0; iij--)
                {
                    GlobalVarLn.list_air.Remove(GlobalVarLn.list_air[iij]);
                    //GlobalVarLn.Number_air -= 1;
                } // FOR1

                GlobalVarLn.Number_air = 0;
                GlobalVarLn.fl_AirPlane = 0;
                // Перерисовать карту
                //GlobalVarLn.axMapScreenGlobal.Repaint();
            }
            catch { }
        }
        // ******************************************************* DelAllAirPlanes

        // DelAllAirPlanes1 *******************************************************

        public static void DelAirPlanes1()
        {
            try
            {
                int iij = 0;

                // FOR1: Убрать все 
                //for (iij = (GlobalVarLn.Number_air - 1); iij >= 0; iij--)
                for (iij = (GlobalVarLn.list_ClassAirPlane.Count - 1); iij >= 0; iij--)
                {
                    GlobalVarLn.list_ClassAirPlane.Remove(GlobalVarLn.list_ClassAirPlane[iij]);

                } // FOR1

                GlobalVarLn.Number_air = 0;
                GlobalVarLn.fl_AirPlane = 0;
            }
            catch { }
            // Перерисовать карту
            //GlobalVarLn.axMapScreenGlobal.Repaint();
        }
        // ******************************************************* DelAllAirPlanes1

        // DelAllAirPlanes2 *******************************************************

        public static void DelAirPlanes2()
        {
            try
            {
                int iij = 0;

                // FOR1: Убрать все 
                for (iij = (GlobalVarLn.list_ClassAirPlane.Count - 1); iij >= 0; iij--)
                {
                    GlobalVarLn.list_ClassAirPlane.Remove(GlobalVarLn.list_ClassAirPlane[iij]);

                } // FOR1

                GlobalVarLn.Number_air = 0;
                GlobalVarLn.fl_AirPlane = 0;
            }
            catch { }

        }
        // ******************************************************* DelAllAirPlanes2

        // AddAirplane ***********************************************************
        // Добавить самолет
        // ff_first=0 -> 1-й раз в лист
        // Возврат: true -> нужна перерисовка

        public static bool AddAirPlaneBD(
                                  double Lat_air,   // grad
                                  double Long_air,
                                  String sNum_air,
                                  int ff_first
                                         )
        {
            try
            {
                double LatDX = 0;
                double LongDY = 0;
                double LatDX_m = 0;
                double LongDY_m = 0;
                int f = 0;
                double X1 = 0;
                double X2 = 0;
                double Y1 = 0;
                double Y2 = 0;
                double dX = 0;
                double dY = 0;
                double Az = 0;

                int iij = 0;

                double dchislo1 = 0;
                long ichislo1 = 0;
                double dchislo2 = 0;
                long ichislo2 = 0;
                double dchislo3 = 0;
                long ichislo3 = 0;
                double dchislo4 = 0;
                long ichislo4 = 0;
                // -------------------------------------------------------------------------------------
                AirPlane objAirPlane = new AirPlane();
                // -------------------------------------------------------------------------------------
                // Широта,долгота в градусах

                objAirPlane.Lat = Lat_air;
                objAirPlane.Long = Long_air;
                // -------------------------------------------------------------------------------------
                // grad->rad
                LatDX = (Lat_air * Math.PI) / 180;
                LongDY = (Long_air * Math.PI) / 180;

                // Подаем rad, получаем там же расстояние на карте в м
                mapGeoToPlane(MapForm.hmapl1, ref LatDX, ref LongDY);

                LatDX_m = LatDX;
                LongDY_m = LongDY;

                // Расстояние в м на карте -> пикселы на изображении
                mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
                // -------------------------------------------------------------------------------------
                // расстояние на карте в м

                objAirPlane.X_m = LatDX_m;
                objAirPlane.Y_m = LongDY_m;
                X2 = LatDX_m;
                Y2 = LongDY_m;
                // -------------------------------------------------------------------------------------
                objAirPlane.sNum = String.Copy(sNum_air);
                // -------------------------------------------------------------------------------------
                // 1-ое занесение в лист

                // IF1
                if (ff_first == 0)
                {
                    objAirPlane.sh = 1;
                    objAirPlane.fl_sh = 1;
                    objAirPlane.Angle = 0;

                    GlobalVarLn.list_air.Add(objAirPlane);
                    GlobalVarLn.Number_air += 1;
                    // 3
                    return true;

                } // IF1:ff_first == 0
                  // -------------------------------------------------------------------------------------
                  // НЕ 1-ое занесение в лист

                // IF1
                else
                {
                    // ...................................................................................
                    // Есть ли самолет с таким же номером

                    f = 0;
                    for (int i = (GlobalVarLn.Number_air - 1); i >= 0; i--)
                    {
                        // ...................................................................................
                        // Есть самолет с таким же номером

                        // IF2
                        if (
                            String.Compare(GlobalVarLn.list_air[i].sNum, objAirPlane.sNum) == 0
                           )
                        {
                            f = 1;

                            ichislo1 = (long)(GlobalVarLn.list_air[i].Lat * 100000);
                            dchislo1 = ((double)ichislo1) / 100000;
                            ichislo2 = (long)(objAirPlane.Lat * 100000);
                            dchislo2 = ((double)ichislo2) / 100000;

                            ichislo3 = (long)(GlobalVarLn.list_air[i].Long * 100000);
                            dchislo3 = ((double)ichislo3) / 100000;
                            ichislo4 = (long)(objAirPlane.Long * 100000);
                            dchislo4 = ((double)ichislo4) / 100000;

                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Координаты повторились

                            // IF3
                            if (
                                (dchislo1 == dchislo2) &&
                                (dchislo3 == dchislo4)
                               )
                                return false;
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // координаты не повторились

                            // IF3
                            else
                            {
                                // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                                // счетчик вышел

                                // IF4
                                if (GlobalVarLn.list_air[i].sh >= GlobalVarLn.ndraw_air)
                                {

                                    X1 = GlobalVarLn.list_air[i].X_m;
                                    Y1 = GlobalVarLn.list_air[i].Y_m;
                                    // На карте X - вверх
                                    dY = X2 - X1;
                                    dX = Y2 - Y1;
                                    // grad
                                    Az = f_Def_Azimuth1(dY, dX);
                                    objAirPlane.Angle = Az;

                                    objAirPlane.sh = 1;
                                    objAirPlane.fl_sh = 2;

                                    // FOR2: Убрать все предыдущие
                                    for (iij = (GlobalVarLn.Number_air - 1); iij >= 0; iij--)
                                    {

                                        if (String.Compare(GlobalVarLn.list_air[iij].sNum, objAirPlane.sNum) == 0)
                                        {
                                            GlobalVarLn.list_air.Remove(GlobalVarLn.list_air[iij]);
                                            GlobalVarLn.Number_air -= 1;
                                        }
                                    } // FOR2

                                    // Добавили новый
                                    GlobalVarLn.list_air.Add(objAirPlane);
                                    GlobalVarLn.Number_air += 1;

                                    // 3
                                    // Убрать с карты
                                    //GlobalVarLn.axMapScreenGlobal.Repaint();
                                    return true;

                                } // IF4: счетчик вышел
                                  // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                                  // счетчик не вышел-> Добавить и вычислить азимут
                                  // !!! Сюда сейчас не должен заходить тк ndraw_air=1

                                // IF4
                                else
                                {
                                    objAirPlane.sh = GlobalVarLn.list_air[i].sh + 1;
                                    objAirPlane.fl_sh = 2;

                                    X1 = GlobalVarLn.list_air[i].X_m;
                                    Y1 = GlobalVarLn.list_air[i].Y_m;

                                    // На карте X - вверх
                                    dY = X2 - X1;
                                    dX = Y2 - Y1;

                                    // grad
                                    Az = f_Def_Azimuth1(dY, dX);

                                    objAirPlane.Angle = Az;

                                    GlobalVarLn.list_air.Add(objAirPlane);

                                    GlobalVarLn.Number_air += 1;
                                    // 3
                                    return false;

                                } // IF4: счетчик не вышел
                                  // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,


                            } // IF3 координаты не повторились
                              // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        } // IF2 (есть такой же самолет)
                          // ...................................................................................

                    } // FOR
                      // ...................................................................................

                    // ...................................................................................
                    // Такого самолета не нашлось при НЕ первом сеансе получения данных

                    if (f == 0)
                    {
                        objAirPlane.sh = 1;
                        objAirPlane.fl_sh = 1;
                        objAirPlane.Angle = 0;
                        GlobalVarLn.list_air.Add(objAirPlane);
                        GlobalVarLn.Number_air += 1;

                        // 3
                        return true;
                    }

                    // ?????
                    // 3
                    // Сюда не должен выйти
                    else
                        return false;
                    // ...................................................................................

                } // IF1:НЕ 1-ое занесение в лист
                  // -------------------------------------------------------------------------------------

            }
            catch { return false; }
        }// Функция AddAirPlaneBD
                     // *********************************************************** AddAirplane

            // AddAirplane1 ***********************************************************
            // Добавить самолет
            // ff_first=0 -> 1-й раз в лист
            // Возврат: true -> нужна перерисовка

            // Air
            public static bool AddAirPlaneBD1(
                                  double Lat_air,   // grad
                                  double Long_air,
                                  String sNum_air,
                                  int ff_first
                                         )
        {
            try {
                double LatDX = 0;
                double LongDY = 0;
                double LatDX_m = 0;
                double LongDY_m = 0;
                int f = 0;
                double X1 = 0;
                double X2 = 0;
                double Y1 = 0;
                double Y2 = 0;
                double dX = 0;
                double dY = 0;
                double Az = 0;

                int iij = 0;

                double dchislo1 = 0;
                long ichislo1 = 0;
                double dchislo2 = 0;
                long ichislo2 = 0;
                double dchislo3 = 0;
                long ichislo3 = 0;
                double dchislo4 = 0;
                long ichislo4 = 0;
                // -------------------------------------------------------------------------------------
                //AirPlane objAirPlane = new AirPlane();
                ClassAirPlane objAirPlane = new ClassAirPlane();
                objAirPlane.list_air1 = new List<AirPlane1>();

                AirPlane1 objAirPlane1 = new AirPlane1(); // Only Xm,Ym
                                                          // -------------------------------------------------------------------------------------
                                                          // Широта,долгота в градусах

                objAirPlane.Lat = Lat_air;
                objAirPlane.Long = Long_air;
                // -------------------------------------------------------------------------------------
                // grad->rad
                LatDX = (Lat_air * Math.PI) / 180;
                LongDY = (Long_air * Math.PI) / 180;

                // Подаем rad, получаем там же расстояние на карте в м
                mapGeoToPlane(MapForm.hmapl1, ref LatDX, ref LongDY);

                LatDX_m = LatDX;
                LongDY_m = LongDY;

                // Расстояние в м на карте -> пикселы на изображении
                mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
                // -------------------------------------------------------------------------------------
                // текущие координаты (пикселы)

                objAirPlane.X_m = LatDX_m;
                objAirPlane.Y_m = LongDY_m;
                objAirPlane1.X_m = LatDX_m;
                objAirPlane1.Y_m = LongDY_m;

                X2 = LatDX_m;
                Y2 = LongDY_m;
                // -------------------------------------------------------------------------------------
                objAirPlane.sNum = String.Copy(sNum_air); // ICAO
                                                          // -------------------------------------------------------------------------------------
                                                          // 1-ое занесение в лист

                // IF1
                if (ff_first == 0)
                {
                    objAirPlane.sh = 1;
                    objAirPlane.fl_sh = 1;
                    objAirPlane.Angle = 0;

                    // *
                    //GlobalVarLn.list_air.Add(objAirPlane);
                    // Добавили самолет
                    GlobalVarLn.list_ClassAirPlane.Add(objAirPlane);
                    // Текущие координаты в пикселах самолета
                    GlobalVarLn.list_ClassAirPlane[GlobalVarLn.list_ClassAirPlane.Count - 1].list_air1.Add(objAirPlane1);

                    GlobalVarLn.Number_air += 1;
                    // 3
                    return true;

                } // IF1:ff_first == 0
                  // -------------------------------------------------------------------------------------
                  // НЕ 1-ое занесение в лист

                // IF1
                else
                {
                    // ...................................................................................
                    // Есть ли самолет с таким же номером

                    f = 0;
                    for (int i = (GlobalVarLn.Number_air - 1); i >= 0; i--)
                    {
                        // ...................................................................................
                        // Есть самолет с таким же номером

                        // IF2
                        if (
                            //String.Compare(GlobalVarLn.list_air[i].sNum, objAirPlane.sNum) == 0
                            String.Compare(GlobalVarLn.list_ClassAirPlane[i].sNum, objAirPlane.sNum) == 0
                           )
                        {
                            f = 1;

                            //ichislo1 = (long)(GlobalVarLn.list_air[i].Lat * 100000);
                            ichislo1 = (long)(GlobalVarLn.list_ClassAirPlane[i].Lat * 100000);
                            dchislo1 = ((double)ichislo1) / 100000;
                            ichislo2 = (long)(objAirPlane.Lat * 100000);
                            dchislo2 = ((double)ichislo2) / 100000;

                            //ichislo3 = (long)(GlobalVarLn.list_air[i].Long * 100000);
                            ichislo3 = (long)(GlobalVarLn.list_ClassAirPlane[i].Long * 100000);
                            dchislo3 = ((double)ichislo3) / 100000;
                            ichislo4 = (long)(objAirPlane.Long * 100000);
                            dchislo4 = ((double)ichislo4) / 100000;

                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Координаты повторились

                            // IF3
                            if (
                                (dchislo1 == dchislo2) &&
                                (dchislo3 == dchislo4)
                               )
                                return false;
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // координаты не повторились

                            // IF3
                            else
                            {
                                // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                                // счетчик вышел -> обновляем лист координат

                                // IF4
                                //if (GlobalVarLn.list_air[i].sh >= GlobalVarLn.ndraw_air)
                                if (GlobalVarLn.list_ClassAirPlane[i].sh >= GlobalVarLn.ndraw_air)
                                {

                                    // Предыдущие кллрдинаты (пикселы)
                                    X1 = GlobalVarLn.list_ClassAirPlane[i].X_m;
                                    Y1 = GlobalVarLn.list_ClassAirPlane[i].Y_m;

                                    // На карте X - вверх
                                    dY = X2 - X1;
                                    dX = Y2 - Y1;
                                    // grad
                                    Az = f_Def_Azimuth1(dY, dX);
                                    objAirPlane.Angle = Az;

                                    objAirPlane.sh = 1;
                                    objAirPlane.fl_sh = 2;

                                    // FOR2: Убрать все предыдущие
                                    //for (iij = (GlobalVarLn.Number_air - 1); iij >= 0; iij--)
                                    //{
                                    //    if (String.Compare(GlobalVarLn.list_air[iij].sNum, objAirPlane.sNum) == 0)
                                    //    {
                                    //        GlobalVarLn.list_air.Remove(GlobalVarLn.list_air[iij]);
                                    //        GlobalVarLn.Number_air -= 1;
                                    //    }
                                    //} // FOR2
                                    // Добавили новый
                                    //GlobalVarLn.list_air.Add(objAirPlane);
                                    //GlobalVarLn.Number_air += 1;

                                    // Удаляем List старых координат
                                    int jjj1 = 0;
                                    for (jjj1 = 0; jjj1 < GlobalVarLn.list_ClassAirPlane[i].list_air1.Count; jjj1++)
                                    {
                                        GlobalVarLn.list_ClassAirPlane[i].list_air1.Remove(GlobalVarLn.list_ClassAirPlane[i].list_air1[jjj1]);
                                    }

                                    // Добавляем новые координаты
                                    AirPlane1 objtmp1 = new AirPlane1();
                                    objtmp1.X_m = X2;
                                    objtmp1.Y_m = Y2;
                                    objAirPlane.list_air1.Add(objtmp1);
                                    // Убрать старый самолет и добавить новый 
                                    // Т.е. заменили старый новым
                                    GlobalVarLn.list_ClassAirPlane.Remove(GlobalVarLn.list_ClassAirPlane[i]);
                                    GlobalVarLn.list_ClassAirPlane.Add(objAirPlane);

                                    return true;

                                } // IF4: счетчик вышел
                                  // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                                  // !!! Для отрисовки траектории: счетчик не вышел-> вычислить азимут, добавить новые 
                                  //     координаты

                                // IF4
                                else
                                {
                                    objAirPlane.sh = GlobalVarLn.list_air[i].sh + 1;
                                    objAirPlane.fl_sh = 2;

                                    // Предыдущие к-ты (пикселы)
                                    //X1 = GlobalVarLn.list_air[i].X_m;
                                    //Y1 = GlobalVarLn.list_air[i].Y_m;
                                    X1 = GlobalVarLn.list_ClassAirPlane[i].X_m;
                                    Y1 = GlobalVarLn.list_ClassAirPlane[i].Y_m;

                                    // На карте X - вверх
                                    dY = X2 - X1;
                                    dX = Y2 - Y1;

                                    // grad
                                    Az = f_Def_Azimuth1(dY, dX);

                                    objAirPlane.Angle = Az;

                                    // Переписываем List старых координат
                                    int jjj = 0;
                                    for (jjj = 0; jjj < GlobalVarLn.list_ClassAirPlane[i].list_air1.Count; jjj++)
                                    {
                                        AirPlane1 objtmp = new AirPlane1();
                                        objtmp.X_m = GlobalVarLn.list_ClassAirPlane[i].list_air1[jjj].X_m;
                                        objtmp.Y_m = GlobalVarLn.list_ClassAirPlane[i].list_air1[jjj].Y_m;
                                        objAirPlane.list_air1.Add(objtmp);
                                    }
                                    // Добавляем новые координаты
                                    AirPlane1 objtmp1 = new AirPlane1();
                                    objtmp1.X_m = X2;
                                    objtmp1.Y_m = Y2;
                                    objAirPlane.list_air1.Add(objtmp1);
                                    // Убрать старый самолет и добавить новый 
                                    // Т.е. заменили старый новым
                                    GlobalVarLn.list_ClassAirPlane.Remove(GlobalVarLn.list_ClassAirPlane[i]);
                                    GlobalVarLn.list_ClassAirPlane.Add(objAirPlane);

                                    //GlobalVarLn.list_air.Add(objAirPlane);

                                    //GlobalVarLn.Number_air += 1;

                                    //return false;
                                    return true;

                                } // IF4: счетчик не вышел
                                  // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,


                            } // IF3 координаты не повторились
                              // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        } // IF2 (есть такой же самолет)
                          // ...................................................................................

                    } // FOR
                      // ...................................................................................

                    // ...................................................................................
                    // Такого самолета не нашлось при НЕ первом сеансе получения данных

                    if (f == 0)
                    {
                        objAirPlane.sh = 1;
                        objAirPlane.fl_sh = 1;
                        objAirPlane.Angle = 0;

                        //GlobalVarLn.list_air.Add(objAirPlane);
                        // Добавили самолет
                        GlobalVarLn.list_ClassAirPlane.Add(objAirPlane);
                        // Текущие координаты в пикселах самолета
                        GlobalVarLn.list_ClassAirPlane[GlobalVarLn.list_ClassAirPlane.Count - 1].list_air1.Add(objAirPlane1);

                        GlobalVarLn.Number_air += 1;

                        // 3
                        return true;
                    }

                    // ?????
                    // 3
                    // Сюда не должен выйти
                    else
                        return false;
                    // ...................................................................................

                } // IF1:НЕ 1-ое занесение в лист
                  // -------------------------------------------------------------------------------------

            }
            catch { return false; }
        } // Функция AddAirPlaneBD1
        // *********************************************************** AddAirplane1

        // AddAirplane2 ***********************************************************
        // Добавить самолет
        // ff_first=0 -> 1-й раз в лист
        // Возврат: true -> нужна перерисовка

        // Air
        public static bool AddAirPlaneBD2(
                                  double Lat_air,   // grad
                                  double Long_air,
                                  String sNum_air, // ICAO
                                                   //List<AirPlane2> lst_air2,
                                  int ff_first
                                         )
        {
            try {
                // -------------------------------------------------------------------------------------
                double LatDX = 0;
                double LongDY = 0;
                double LatDX_m = 0;
                double LongDY_m = 0;
                int f = 0;
                double X1 = 0;
                double X2 = 0;
                double Y1 = 0;
                double Y2 = 0;
                double dX = 0;
                double dY = 0;
                double Az = 0;

                int iij = 0;

                double dchislo1 = 0;
                long ichislo1 = 0;
                double dchislo2 = 0;
                long ichislo2 = 0;
                double dchislo3 = 0;
                long ichislo3 = 0;
                double dchislo4 = 0;
                long ichislo4 = 0;
                // -------------------------------------------------------------------------------------

                // AirPlane (objAirPlane) >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // Заполняем общую структуру (объект класса Самолет)

                // -------------------------------------------------------------------------------------
                ClassAirPlane objAirPlane = new ClassAirPlane();
                objAirPlane.list_air2 = new List<AirPlane2>();  // Lat,Long,X_m,Y_m
                AirPlane2 objAirPlane2 = new AirPlane2();
                // -------------------------------------------------------------------------------------
                // Широта,долгота в градусах (текущие)

                objAirPlane.Lat = Lat_air;
                objAirPlane.Long = Long_air;
                // -------------------------------------------------------------------------------------
                // grad->rad
                LatDX = (Lat_air * Math.PI) / 180;
                LongDY = (Long_air * Math.PI) / 180;

                // Подаем rad, получаем там же расстояние на карте в м
                mapGeoToPlane(MapForm.hmapl1, ref LatDX, ref LongDY);

                LatDX_m = LatDX;
                LongDY_m = LongDY;

                // Расстояние в м на карте -> пикселы на изображении
                //mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
                // -------------------------------------------------------------------------------------
                // текущие координаты (m на местности)

                objAirPlane.X_m = LatDX_m;
                objAirPlane.Y_m = LongDY_m;

                // Для азимута
                X2 = LatDX_m;
                Y2 = LongDY_m;
                // -------------------------------------------------------------------------------------
                objAirPlane.sNum = String.Copy(sNum_air); // ICAO
                                                          // -------------------------------------------------------------------------------------
                                                          // Список координат (траектория): последний элемент - текущие
                                                          // !!! На вход идет самолет ICAO c новой текущей записью (НЕ вся траектория) 

                //for(iij=0; iij< lst_air2.Count; iij++ )
                //{
                //    AirPlane2 objtmp1 = new AirPlane2();
                //    objtmp1.Lat = lst_air2[iij].Lat;
                //    objtmp1.Long = lst_air2[iij].Long;
                //    // grad->rad
                //    LatDX = (lst_air2[iij].Lat * Math.PI) / 180;
                //    LongDY = (lst_air2[iij].Long * Math.PI) / 180;
                //    // Подаем rad, получаем там же расстояние на карте в м
                //    mapGeoToPlane(MapForm.hmapl1, ref LatDX, ref LongDY);
                //    objtmp1.X_m = LatDX;
                //    objtmp1.Y_m = LongDY;
                //    objAirPlane.list_air2.Add(objtmp1);
                //} // FOR

                objAirPlane2.Lat = Lat_air;
                objAirPlane2.Long = Long_air;
                // grad->rad
                LatDX = (Lat_air * Math.PI) / 180;
                LongDY = (Long_air * Math.PI) / 180;
                // Подаем rad, получаем там же расстояние на карте в м
                mapGeoToPlane(MapForm.hmapl1, ref LatDX, ref LongDY);
                objAirPlane2.X_m = LatDX;
                objAirPlane2.Y_m = LongDY;
                //objAirPlane.list_air2.Add(objAirPlane2);

                // -------------------------------------------------------------------------------------

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> AirPlane (objAirPlane)

                // list_ClassAirPlane(1-е занесение) >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // 1-ое занесение в лист

                // IF1
                if (ff_first == 0)
                {
                    objAirPlane.sh = 1;
                    objAirPlane.fl_sh = 1;
                    objAirPlane.Angle = 0;

                    // *
                    // Добавили самолет
                    objAirPlane.list_air2.Add(objAirPlane2);
                    GlobalVarLn.list_ClassAirPlane.Add(objAirPlane);
                    GlobalVarLn.Number_air += 1;
                    return true;

                } // IF1:ff_first == 0
                  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> list_ClassAirPlane(1-е занесение)

                // list_ClassAirPlane(НЕ 1-е занесение) >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // НЕ 1-ое занесение в лист

                // ELSE on IF1
                else
                {
                    // FOR (old list) ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    // Есть ли самолет с таким же номером

                    f = 0;
                    bool fb = false;

                    //for (int i = (GlobalVarLn.Number_air - 1); i >= 0; i--)
                    for (int i = (GlobalVarLn.list_ClassAirPlane.Count - 1); i >= 0; i--)
                    {
                        // ...............................................................................
                        // Есть самолет с таким же номером (ICAO)

                        // IF2
                        if (
                          String.Compare(GlobalVarLn.list_ClassAirPlane[i].sNum, objAirPlane.sNum) == 0
                           )
                        {
                            f = 1;

                            // Последние координаты в старом списке (до 5-го знака после запятой)
                            ichislo1 = (long)(GlobalVarLn.list_ClassAirPlane[i].Lat * 100000);
                            dchislo1 = ((double)ichislo1) / 100000;
                            // Новые координаты
                            ichislo2 = (long)(objAirPlane.Lat * 100000);
                            dchislo2 = ((double)ichislo2) / 100000;

                            ichislo3 = (long)(GlobalVarLn.list_ClassAirPlane[i].Long * 100000);
                            dchislo3 = ((double)ichislo3) / 100000;
                            ichislo4 = (long)(objAirPlane.Long * 100000);
                            dchislo4 = ((double)ichislo4) / 100000;

                            double sss = ClassMap.f_D_2Points(dchislo1, dchislo3, dchislo2, dchislo4, 1);
                            objAirPlane2.ds = sss; // m

                            // ==============================================================
                            // Координаты повторились

                            // IF3
                            if (
                                (
                                (dchislo1 == dchislo2) &&
                                (dchislo3 == dchislo4)
                                ) ||
                                //(objAirPlane.Lat>GlobalVarLn.lat_max) ||
                                //(objAirPlane.Lat < GlobalVarLn.lat_min) ||
                                //(objAirPlane.Long > GlobalVarLn.lon_max) ||
                                //(objAirPlane.Long < GlobalVarLn.lon_min)||
                                (objAirPlane.Lat == 0) ||
                                (objAirPlane.Long == 0)
                               )
                            {
                                fb = false;
                                return false;

                            } // IF3
                              // ==============================================================
                              // координаты не повторились

                            // ELSE on IF3
                            else
                            {
                                // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                                // счетчик вышел -> обновляем лист координат

                                // IF4
                                //if (GlobalVarLn.list_ClassAirPlane[i].sh >= GlobalVarLn.ndraw_air)
                                //{
                                // Предыдущие координаты (m на местности)
                                X1 = GlobalVarLn.list_ClassAirPlane[i].X_m;
                                Y1 = GlobalVarLn.list_ClassAirPlane[i].Y_m;

                                // На карте X - вверх
                                dY = X2 - X1;
                                dX = Y2 - Y1;
                                // grad
                                Az = f_Def_Azimuth1(dY, dX);
                                objAirPlane.Angle = Az;

                                objAirPlane.sh = 1;
                                objAirPlane.fl_sh = 2;

                                // Удаляем List старых координат
                                //int jjj1 = 0;
                                //for (jjj1 = 0; jjj1 < GlobalVarLn.list_ClassAirPlane[i].list_air1.Count; jjj1++)
                                //{
                                //    GlobalVarLn.list_ClassAirPlane[i].list_air1.Remove(GlobalVarLn.list_ClassAirPlane[i].list_air1[jjj1]);
                                //}

                                // Добавляем новые координаты
                                if (GlobalVarLn.list_ClassAirPlane[i].list_air2.Count >= GlobalVarLn.numbtr)
                                {
                                    // Если список переполнился -> Удаляем 1-й
                                    GlobalVarLn.list_ClassAirPlane[i].list_air2.Remove(GlobalVarLn.list_ClassAirPlane[i].list_air2[0]);
                                }
                                // В списке траектории добавляем новые координаты
                                GlobalVarLn.list_ClassAirPlane[i].list_air2.Add(objAirPlane2);
                                // Переписываем траекторию во вновь созданный объект
                                for (iij = 0; iij < GlobalVarLn.list_ClassAirPlane[i].list_air2.Count; iij++)
                                {
                                    AirPlane2 objtmp2 = new AirPlane2();
                                    objtmp2.Lat = GlobalVarLn.list_ClassAirPlane[i].list_air2[iij].Lat;
                                    objtmp2.Long = GlobalVarLn.list_ClassAirPlane[i].list_air2[iij].Long;
                                    objtmp2.X_m = GlobalVarLn.list_ClassAirPlane[i].list_air2[iij].X_m;
                                    objtmp2.Y_m = GlobalVarLn.list_ClassAirPlane[i].list_air2[iij].Y_m;
                                    objtmp2.ds = GlobalVarLn.list_ClassAirPlane[i].list_air2[iij].ds;
                                    // Добавляем в List траектории во вновь созданный объект класса-самолета
                                    objAirPlane.list_air2.Add(objtmp2);
                                }

                                // Убрать старый самолет и добавить новый 
                                // Т.е. заменили старый новым
                                GlobalVarLn.list_ClassAirPlane.Remove(GlobalVarLn.list_ClassAirPlane[i]);
                                GlobalVarLn.list_ClassAirPlane.Add(objAirPlane);

                                fb = true;
                                return true;

                                //} // IF4: счетчик вышел
                                // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                                // !!! Для отрисовки траектории: счетчик не вышел-> вычислить азимут, добавить новые 
                                //     координаты

                                // IF4
                                //else
                                //{
                                //objAirPlane.sh = GlobalVarLn.list_air[i].sh + 1;
                                //objAirPlane.fl_sh = 2;

                                //// Предыдущие к-ты (пикселы)
                                //X1 = GlobalVarLn.list_ClassAirPlane[i].X_m;
                                //Y1 = GlobalVarLn.list_ClassAirPlane[i].Y_m;
                                //// На карте X - вверх
                                //dY = X2 - X1;
                                //dX = Y2 - Y1;
                                //// grad
                                //Az = f_Def_Azimuth1(dY, dX);
                                //objAirPlane.Angle = Az;

                                //// Переписываем List старых координат
                                //int jjj = 0;
                                //for (jjj = 0; jjj < GlobalVarLn.list_ClassAirPlane[i].list_air1.Count; jjj++)
                                //{
                                //    AirPlane1 objtmp = new AirPlane1();
                                //    objtmp.X_m = GlobalVarLn.list_ClassAirPlane[i].list_air1[jjj].X_m;
                                //    objtmp.Y_m = GlobalVarLn.list_ClassAirPlane[i].list_air1[jjj].Y_m;
                                //    objAirPlane.list_air1.Add(objtmp);
                                //}
                                //// Добавляем новые координаты
                                //AirPlane1 objtmp1 = new AirPlane1();
                                //objtmp1.X_m = X2;
                                //objtmp1.Y_m = Y2;
                                //objAirPlane.list_air1.Add(objtmp1);
                                //// Убрать старый самолет и добавить новый 
                                //// Т.е. заменили старый новым
                                //GlobalVarLn.list_ClassAirPlane.Remove(GlobalVarLn.list_ClassAirPlane[i]);
                                //GlobalVarLn.list_ClassAirPlane.Add(objAirPlane);

                                //fb = true;
                                //return true;

                                //} // IF4: счетчик не вышел
                                // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

                            } // ELSE on IF3 координаты не повторились
                              // ==============================================================

                        } // IF2 (есть такой же самолет)
                          // ..............................................................................

                    } // FOR
                      // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ FOR (old list)

                    // New AirPlane ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    // Такого самолета не нашлось при НЕ первом сеансе получения данных

                    if (f == 0)
                    {
                        objAirPlane.sh = 1;
                        objAirPlane.fl_sh = 1;
                        objAirPlane.Angle = 0;

                        // Добавили самолет
                        GlobalVarLn.list_ClassAirPlane.Add(objAirPlane);
                        GlobalVarLn.Number_air += 1;
                        return true;
                    }
                    // ??? Вообще сюда недолжны выходить
                    else
                        return fb;
                    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ New AirPlane

                } // ELSE on IF1:НЕ 1-ое занесение в лист
                  // -------------------------------------------------------------------------------------

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> list_ClassAirPlane(НЕ 1-е занесение)

            }
            catch { return false; }
        }
        
        // *********************************************************** AddAirplane2

        // DelAirplane ***********************************************************
        // Удалить все самолеты с этим номером

        public static void DelAirPlaneBD(
                                  String sNum_air
                                       )
        {
            try
            {
                int iij = 0;

                // FOR1: Убрать все с этим номером
                for (iij = (GlobalVarLn.Number_air - 1); iij >= 0; iij--)
                {

                    if (String.Compare(GlobalVarLn.list_air[iij].sNum, sNum_air) == 0)
                    {
                        GlobalVarLn.list_air.Remove(GlobalVarLn.list_air[iij]);
                        GlobalVarLn.Number_air -= 1;
                    }
                } // FOR1

                if (GlobalVarLn.Number_air == 0) // Самолетов не осталось
                {
                    GlobalVarLn.fl_AirPlane = 0;
                }
            }
            catch { }
            // Перерисовать карту
            //GlobalVarLn.axMapScreenGlobal.Repaint();

        }
        // *********************************************************** DelAirplane

        // DelAirplane1 ***********************************************************
        // Удалить все самолеты с этим номером

        public static void DelAirPlaneBD1(
                                  String sNum_air
                                       )
        {
            try
            {
                int iij = 0;

                // FOR1: Убрать все с этим номером
                //for (iij = (GlobalVarLn.Number_air - 1); iij >= 0; iij--)
                for (iij = (GlobalVarLn.list_ClassAirPlane.Count - 1); iij >= 0; iij--)
                {
                    if (String.Compare(GlobalVarLn.list_ClassAirPlane[iij].sNum, sNum_air) == 0)
                    {
                        GlobalVarLn.list_ClassAirPlane.Remove(GlobalVarLn.list_ClassAirPlane[iij]);
                        GlobalVarLn.Number_air -= 1;
                    }
                } // FOR1

                if (GlobalVarLn.Number_air == 0) // Самолетов не осталось
                {
                    GlobalVarLn.fl_AirPlane = 0;
                }
            }
            catch { }
            // Перерисовать карту
            //GlobalVarLn.axMapScreenGlobal.Repaint();

        }
        // *********************************************************** DelAirplane1

        // DelAirplane2 ***********************************************************
        // Удалить все самолеты с этим номером

        public static void DelAirPlaneBD2(
                                  String sNum_air
                                       )
        {
            try
            {
                int iij = 0;

                // FOR1: Убрать все с этим номером
                for (iij = (GlobalVarLn.list_ClassAirPlane.Count - 1); iij >= 0; iij--)
                {
                    if (String.Compare(GlobalVarLn.list_ClassAirPlane[iij].sNum, sNum_air) == 0)
                    {
                        GlobalVarLn.list_ClassAirPlane.Remove(GlobalVarLn.list_ClassAirPlane[iij]);
                        GlobalVarLn.Number_air -= 1;
                    }
                } // FOR1

                if (GlobalVarLn.Number_air == 0) // Самолетов не осталось
                {
                    GlobalVarLn.fl_AirPlane = 0;
                }
            }
            catch
            {

            }

        }
        // *********************************************************** DelAirplane2

        // Redraw_Airplane *******************************************************

        public static void f_ReDrawAirPlaneBD()
        {
            double LatDX = 0;
            double LongDY = 0;

            try
            {
                // FOR1
                for (int i = 0; i < GlobalVarLn.list_ClassAirPlane.Count; i++)
                {
                    // grad->rad
                    LatDX = (GlobalVarLn.list_ClassAirPlane[i].Lat * Math.PI) / 180;
                    LongDY = (GlobalVarLn.list_ClassAirPlane[i].Long * Math.PI) / 180;

                    // Подаем rad, получаем там же расстояние на карте в м
                    mapGeoToPlane(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

                    // Расстояние в м на карте -> пикселы на изображении
                    mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

                    f_DrawAirPlaneXY1(
                                     LatDX,
                                     LongDY,
                                     GlobalVarLn.list_ClassAirPlane[i].Angle,
                                     GlobalVarLn.list_ClassAirPlane[i].sNum
                                     );

                    // ................................................................
                    // Траектория

                    int i1 = 0;
                    double x1 = 0;
                    double y1 = 0;
                    double x2 = 0;
                    double y2 = 0;



                    if (GlobalVarLn.flair_traj == true)
                    {
                        // FOR2
                        for (i1 = 0; i1 < GlobalVarLn.list_ClassAirPlane[i].list_air2.Count; i1++)
                        {
                            // ............................................................
                            // 1-й элемент

                            if (i1 == 0)
                            {   // m на местности
                                x1 = GlobalVarLn.list_ClassAirPlane[i].list_air2[i1].X_m;
                                y1 = GlobalVarLn.list_ClassAirPlane[i].list_air2[i1].Y_m;
                            }
                            // ............................................................
                            //  НЕ 1-й элемент

                            else
                            {
                                x2 = GlobalVarLn.list_ClassAirPlane[i].list_air2[i1].X_m;
                                y2 = GlobalVarLn.list_ClassAirPlane[i].list_air2[i1].Y_m;

                                ClassMap.f_Map_Line_XY_stat(
                                                         x1,
                                                         y1,
                                                         x2,
                                                         y2
                                                            );
                                x1 = GlobalVarLn.list_ClassAirPlane[i].list_air2[i1].X_m;
                                y1 = GlobalVarLn.list_ClassAirPlane[i].list_air2[i1].Y_m;

                            } // else
                              // ............................................................


                        } // FOR2
                          // ................................................................
                    }



                } // FOR1
            }
            catch {  }
        }
        // ******************************************************* Redraw_Airplane

        // Get_ИРИ_ФРЧ ***************************************************************
        // Get and draw ИРИ ФРЧ

        public static void GetTempFWS_DB(
                                  List<TempFWS> lstTableTempFWS
                                       )
        {
            // ------------------------------------------------------------------------
            bool fldraw = false;
            int i = 0;
            int j = 0;
            bool f = false;
            double freqd = 0;

            double dds = 250; // m
            double sss = 0;

            AirPlane objIRI = new AirPlane();

            // -------------------------------------------------------------------------
            // Delete all

            if((GlobalVarLn.listGPS_IRI.Count!=0)&&(lstTableTempFWS.Count==0))
            {
                ClassMap.DelTempFWS_DB();
                // Del and redraw
                GlobalVarLn.axMapScreenGlobal.Repaint();
                return;
            }
            // -------------------------------------------------------------------------
            // Нужна ли перерисовка: есть новые или пришли старые с измененном положением
            // (при этом расстояние > 250m)

            // ..........................................................................
            // Есть ли новые ИРИ

            // FOR1: new list 
            for (i = 0; i < lstTableTempFWS.Count; i++)
            {
                f = false;
                // FOR2: old list
                //var temp = GlobalVarLn.listGPS_IRI.FirstOrDefault(rec => rec.Num == lstTableTempFWS[i].Id);
                //if (temp.f != 0)
                //{
                //    f = true;
                //}

                for (j = 0; j < GlobalVarLn.listGPS_IRI.Count; j++)
                {
                    if (lstTableTempFWS[i].Id== GlobalVarLn.listGPS_IRI[j].Num)
                    {
                        f = true;
                    }

                } // FOR2: Old list

                // в старом не было такого ИРИ
                if(f==false)
                {
                    fldraw = true;

                    // 555
                    //i = lstTableTempFWS.Count + 1; // exit from FOR1

                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    // 555
                    // Добавляем этот новый

                    GlobalVarLn.LatGPS_IRI = lstTableTempFWS[i].Coordinates.Latitude;
                    GlobalVarLn.LongGPS_IRI = lstTableTempFWS[i].Coordinates.Longitude;
                    GlobalVarLn.IDGPS_IRI = lstTableTempFWS[i].Id;
                    freqd = lstTableTempFWS[i].FreqKHz;
                    GlobalVarLn.sGPS_IRI = Convert.ToString((int)freqd);
                    GlobalVarLn.fGPS_IRI = freqd;

                    if ((freqd >= GlobalVarLn.fCl1) && (freqd < GlobalVarLn.fCl1_2))
                        GlobalVarLn.ColGPS_IRI = Color.LightGray;
                    else if ((freqd >= GlobalVarLn.fCl2) && (freqd < GlobalVarLn.fCl2_2))
                        GlobalVarLn.ColGPS_IRI = Color.Gray;
                    else if ((freqd >= GlobalVarLn.fCl3) && (freqd < GlobalVarLn.fCl3_2))
                        GlobalVarLn.ColGPS_IRI = Color.DarkGray;

                    else if ((freqd >= GlobalVarLn.fCl4) && (freqd < GlobalVarLn.fCl4_2))
                        GlobalVarLn.ColGPS_IRI = Color.LightSkyBlue;
                    else if ((freqd >= GlobalVarLn.fCl5) && (freqd < GlobalVarLn.fCl5_2))
                        GlobalVarLn.ColGPS_IRI = Color.Turquoise;
                    else if ((freqd >= GlobalVarLn.fCl6) && (freqd < GlobalVarLn.fCl6_2))
                        GlobalVarLn.ColGPS_IRI = Color.DarkTurquoise;

                    else if ((freqd >= GlobalVarLn.fCl7) && (freqd < GlobalVarLn.fCl7_2))
                        GlobalVarLn.ColGPS_IRI = Color.LightBlue;
                    else if ((freqd >= GlobalVarLn.fCl8) && (freqd < GlobalVarLn.fCl8_2))
                        GlobalVarLn.ColGPS_IRI = Color.Blue;
                    else if ((freqd >= GlobalVarLn.fCl9) && (freqd < GlobalVarLn.fCl9_2))
                        GlobalVarLn.ColGPS_IRI = Color.DarkBlue;
                    else
                        GlobalVarLn.ColGPS_IRI = Color.Black;

                    GlobalVarLn.flGPS_IRI = 1;

                    ClassMap.Get_IRI_TempFWS(GlobalVarLn.IDGPS_IRI);

                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                }



            } // FOR1 new list
            // ..........................................................................
            // Новых нет, НО исчезли ли какие-либо старые

            if (fldraw == false)
            {
                // FOR1: !!!OLD list 
                for (i = 0; i < GlobalVarLn.listGPS_IRI.Count; i++)
                {
                    f = false;
                    // FOR2: !!!NEW list
                    for (j = 0; j < lstTableTempFWS.Count; j++)
                    {
                        if (lstTableTempFWS[j].Id == GlobalVarLn.listGPS_IRI[i].Num)
                        {
                            f = true;
                        }

                    } // FOR2: !!!NEW list

                    // в новом уже нет такого ИРИ
                    if (f == false)
                    {
                        fldraw = true;

                        // 555
                        //i = GlobalVarLn.listGPS_IRI.Count + 1; // exit from FOR1

                        // 555
                        // Удаляем этот старый
                        GlobalVarLn.listGPS_IRI.Remove(GlobalVarLn.listGPS_IRI[i]);

                    }

                } // FOR1 !!!OLD list

            } // IF (новых не было)
            // ..........................................................................
            // Изменилось ли положение старых и расстояние > 250m

            // 555
            // !!! Новых не было, но положение старых могло измениться

            //if (fldraw==false) // Новых не было
            //{

                // FOR1: new list 
                for (i = 0; i < lstTableTempFWS.Count; i++)
                {
                    // FOR2: old list
                    for (j = 0; j < GlobalVarLn.listGPS_IRI.Count; j++)
                    {
                        if (lstTableTempFWS[i].Id == GlobalVarLn.listGPS_IRI[j].Num)
                        {
                            sss = ClassMap.f_D_2Points(GlobalVarLn.listGPS_IRI[j].Lat, GlobalVarLn.listGPS_IRI[j].Long,
                                                       lstTableTempFWS[i].Coordinates.Latitude, lstTableTempFWS[i].Coordinates.Longitude, 1);
                            if (sss > dds)
                            {
                                fldraw = true;

                                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                // 555
                                // Удаляем старый и добавляем новый

                                GlobalVarLn.listGPS_IRI.Remove(GlobalVarLn.listGPS_IRI[j]);

                                GlobalVarLn.LatGPS_IRI = lstTableTempFWS[i].Coordinates.Latitude;
                                GlobalVarLn.LongGPS_IRI = lstTableTempFWS[i].Coordinates.Longitude;
                                GlobalVarLn.IDGPS_IRI = lstTableTempFWS[i].Id;
                                freqd = lstTableTempFWS[i].FreqKHz;
                                GlobalVarLn.sGPS_IRI = Convert.ToString((int)freqd);
                                GlobalVarLn.fGPS_IRI = freqd;

                                if ((freqd >= GlobalVarLn.fCl1) && (freqd < GlobalVarLn.fCl1_2))
                                    GlobalVarLn.ColGPS_IRI = Color.LightGray;
                                else if ((freqd >= GlobalVarLn.fCl2) && (freqd < GlobalVarLn.fCl2_2))
                                    GlobalVarLn.ColGPS_IRI = Color.Gray;
                                else if ((freqd >= GlobalVarLn.fCl3) && (freqd < GlobalVarLn.fCl3_2))
                                    GlobalVarLn.ColGPS_IRI = Color.DarkGray;

                                else if ((freqd >= GlobalVarLn.fCl4) && (freqd < GlobalVarLn.fCl4_2))
                                    GlobalVarLn.ColGPS_IRI = Color.LightSkyBlue;
                                else if ((freqd >= GlobalVarLn.fCl5) && (freqd < GlobalVarLn.fCl5_2))
                                    GlobalVarLn.ColGPS_IRI = Color.Turquoise;
                                else if ((freqd >= GlobalVarLn.fCl6) && (freqd < GlobalVarLn.fCl6_2))
                                    GlobalVarLn.ColGPS_IRI = Color.DarkTurquoise;

                                else if ((freqd >= GlobalVarLn.fCl7) && (freqd < GlobalVarLn.fCl7_2))
                                    GlobalVarLn.ColGPS_IRI = Color.LightBlue;
                                else if ((freqd >= GlobalVarLn.fCl8) && (freqd < GlobalVarLn.fCl8_2))
                                    GlobalVarLn.ColGPS_IRI = Color.Blue;
                                else if ((freqd >= GlobalVarLn.fCl9) && (freqd < GlobalVarLn.fCl9_2))
                                    GlobalVarLn.ColGPS_IRI = Color.DarkBlue;
                                else
                                    GlobalVarLn.ColGPS_IRI = Color.Black;

                                GlobalVarLn.flGPS_IRI = 1;
                                ClassMap.Get_IRI_TempFWS(GlobalVarLn.IDGPS_IRI);

                                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                            }
                        }

                    } // FOR2: Old list

                    // в старом нашелся такой же ИРИ, но новое положение отличается > 250m
                    if (fldraw == true)
                    {
                        // 555
                        //i = lstTableTempFWS.Count + 1; // exit from FOR1
                    }

                } // FOR1 new list

            //} // IF(Новых не было)
            // ..........................................................................

            // ------------------------------------------------------------------------
            // Выяснилм, что нужна перерисовка

            if (fldraw == true)
            {

                // 555
/*
                // ..........................................................................
                // Del all IRI

                ClassMap.DelTempFWS_DB();
                // ..........................................................................
                // Add new IRI

                if (lstTableTempFWS.Count != 0)
                {
                    GlobalVarLn.flGPS_IRI = 1;

                    for (i = 0; i < lstTableTempFWS.Count; i++)
                    {
                        GlobalVarLn.LatGPS_IRI = lstTableTempFWS[i].Coordinates.Latitude;
                        GlobalVarLn.LongGPS_IRI = lstTableTempFWS[i].Coordinates.Longitude;
                        GlobalVarLn.IDGPS_IRI = lstTableTempFWS[i].Id;
                        freqd = lstTableTempFWS[i].FreqKHz;
                        GlobalVarLn.sGPS_IRI = Convert.ToString((int)freqd);
                        GlobalVarLn.fGPS_IRI = freqd;

                        if ((freqd >= GlobalVarLn.fCl1) && (freqd < GlobalVarLn.fCl1_2))
                            GlobalVarLn.ColGPS_IRI = Color.LightGray;
                        else if ((freqd >= GlobalVarLn.fCl2) && (freqd < GlobalVarLn.fCl2_2))
                            GlobalVarLn.ColGPS_IRI = Color.Gray;
                        else if ((freqd >= GlobalVarLn.fCl3) && (freqd < GlobalVarLn.fCl3_2))
                            GlobalVarLn.ColGPS_IRI = Color.DarkGray;

                        else if ((freqd >= GlobalVarLn.fCl4) && (freqd < GlobalVarLn.fCl4_2))
                            GlobalVarLn.ColGPS_IRI = Color.LightSkyBlue;
                        else if ((freqd >= GlobalVarLn.fCl5) && (freqd < GlobalVarLn.fCl5_2))
                            GlobalVarLn.ColGPS_IRI = Color.Turquoise;
                        else if ((freqd >= GlobalVarLn.fCl6) && (freqd < GlobalVarLn.fCl6_2))
                            GlobalVarLn.ColGPS_IRI = Color.DarkTurquoise;

                        else if ((freqd >= GlobalVarLn.fCl7) && (freqd < GlobalVarLn.fCl7_2))
                            GlobalVarLn.ColGPS_IRI = Color.LightBlue;
                        else if ((freqd >= GlobalVarLn.fCl8) && (freqd < GlobalVarLn.fCl8_2))
                            GlobalVarLn.ColGPS_IRI = Color.Blue;
                        else if ((freqd >= GlobalVarLn.fCl9) && (freqd < GlobalVarLn.fCl9_2))
                            GlobalVarLn.ColGPS_IRI = Color.DarkBlue;
                        else
                            GlobalVarLn.ColGPS_IRI = Color.Black;


                        //GlobalVarLn.ColGPS_IRI = Color.Blue;

                        ClassMap.Get_IRI_TempFWS(GlobalVarLn.IDGPS_IRI);

                    } // FOR

                } // lstTableTempFWS.Count!=0
*/
                // ..........................................................................
                // Del and redraw
                // !!! По Tick Timer2

                GlobalVarLn.axMapScreenGlobal.Repaint();
                // ..........................................................................

            } // IF(fldraw==true)
            // ------------------------------------------------------------------------

        }
        // *************************************************************** Get_ИРИ_ФРЧ

        // Del_ИРИ_ФРЧ ***************************************************************
        // Del all

        public static void DelTempFWS_DB()
        {
            int iij = 0;
            
            for (iij = (GlobalVarLn.listGPS_IRI.Count - 1); iij >= 0; iij--)
            {
                GlobalVarLn.listGPS_IRI.Remove(GlobalVarLn.listGPS_IRI[iij]);
            }

            GlobalVarLn.flGPS_IRI = 0;
        }
        // *************************************************************** Del_ИРИ_ФРЧ

        // Add_One_ИРИФРЧ ************************************************************

        public static void Get_IRI_TempFWS(int id)
        {
            double xtmp_ed, ytmp_ed, htmp_ed;
            double xtmp1_ed, ytmp1_ed;

            xtmp_ed = 0;
            ytmp_ed = 0;
            htmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;

            AirPlane objLF = new AirPlane();
            // -----------------------------------------------------------------------------
            // !!! Get to (WGS84)

            // -----------------------------------------------------------------------------

            if (GlobalVarLn.flGPS_IRI == 1)
            {
                // ......................................................................
                ClassMap objClassMap1_ed = new ClassMap();
                ClassMap objClassMap2_ed = new ClassMap();
                ClassMap objClassMap3_ed = new ClassMap();
                // ......................................................................
                // grad(WGS84)

                xtmp1_ed = GlobalVarLn.LatGPS_IRI;
                ytmp1_ed = GlobalVarLn.LongGPS_IRI;
                // ......................................................................

                // WGS84(эллипсоид)->элл.Красовского *************************************
                // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
                // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
                // Входные параметры -> град,km
                // Перевод в рад - внутри функции

                // dLong ..................................................................
                // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
                // (преобразования Молоденского), угл.сек

                objClassMap1_ed.f_dLong
                    (
                        // Входные параметры (град,км)
                        xtmp1_ed,   // широта
                        ytmp1_ed,  // долгота
                        0,     // высота

                        // DATUM,m
                        GlobalVarLn.dXdat_comm,
                        GlobalVarLn.dYdat_comm,
                        GlobalVarLn.dZdat_comm,

                        ref GlobalVarLn.dLong_GPS   // приращение по долготе, угл.сек
                    );
                // ................................................................ dLong

                // dLat .................................................................
                // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
                // (преобразования Молоденского), угл.сек

                objClassMap1_ed.f_dLat
                    (
                        // Входные параметры (град,км)
                        xtmp1_ed,   // широта
                        ytmp1_ed,  // долгота
                        0,     // высота

                        // DATUM,m
                        GlobalVarLn.dXdat_comm,
                        GlobalVarLn.dYdat_comm,
                        GlobalVarLn.dZdat_comm,

                        ref GlobalVarLn.dLat_GPS        // приращение по долготе, угл.сек
                    );
                // ................................................................. dLat

                // Lat,Long .............................................................
                // Преобразования широты и долготы при пересчете WGS84->SK42

                objClassMap1_ed.f_WGS84_SK42_Lat_Long
                       (
                           // Входные параметры (град,км)
                           xtmp1_ed,   // широта
                           ytmp1_ed,  // долгота
                           0,     // высота
                           GlobalVarLn.dLat_GPS,       // приращение по долготе, угл.сек
                           GlobalVarLn.dLong_GPS,      // приращение по долготе, угл.сек

                           // Выходные параметры (grad)
                           ref GlobalVarLn.LatKrG_GPS,   // широта
                           ref GlobalVarLn.LongKrG_GPS   // долгота
                       );
                // ............................................................ Lat,Long


                // *********************************** WGS84(эллипсоид)->элл.Красовского

                // ......................................................................
                // ellipsKras -> M real

                xtmp_ed = GlobalVarLn.LatKrG_GPS;
                ytmp_ed = GlobalVarLn.LongKrG_GPS;

                // Grad->rad
                xtmp_ed = (xtmp_ed * Math.PI) / 180;
                ytmp_ed = (ytmp_ed * Math.PI) / 180;

                mapGeoToPlane(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

                GlobalVarLn.XGPS_IRI = xtmp_ed;
                GlobalVarLn.YGPS_IRI = ytmp_ed;
                // ......................................................................
                // !!! реальные координаты на местности карты в м (Plane)

                objLF.f = GlobalVarLn.fGPS_IRI;
                objLF.Lat = GlobalVarLn.LatGPS_IRI;
                objLF.Long = GlobalVarLn.LongGPS_IRI;
                objLF.X_m = xtmp_ed;
                objLF.Y_m = ytmp_ed;
                objLF.sNum = GlobalVarLn.sGPS_IRI;
                objLF.Num = GlobalVarLn.IDGPS_IRI;
                //objLF.sh = GlobalVarLn.AdDlGPS_IRI;
                // ......................................................................
                objLF.cl = GlobalVarLn.ColGPS_IRI;
                // ......................................................................
                objLF.Num = id;

                //for (int i = 0; i < GlobalVarLn.listGPS_IRI.Count; i++)
                //{
                //    if (GlobalVarLn.listGPS_IRI[i].Num == id)
                //        GlobalVarLn.listGPS_IRI.Remove(GlobalVarLn.listGPS_IRI[i]);
                //}
                GlobalVarLn.listGPS_IRI.Add(objLF);
                // ......................................................................
                // Draw 

/*
                if ((GlobalVarLn.flDublGPS_IRI == -1) || (GlobalVarLn.flDublGPS_IRI == 1))
                {
                    GlobalVarLn.flDublGPS_IRI = 0;
                    // Убрать с карты
                    GlobalVarLn.axMapScreenGlobal.Repaint();
                    // Redraw
                    ClassMap.f_Map_Redraw_GPS_IRI();

                    GlobalVarLn.DublLatGPS_IRI = GlobalVarLn.LatGPS_IRI;
                    GlobalVarLn.DublLongGPS_IRI = GlobalVarLn.LongGPS_IRI;
                }
                else
                {
                    double dds = 250; // m
                    double sss = 0;
                    sss = ClassMap.f_D_2Points(GlobalVarLn.DublLatGPS_IRI, GlobalVarLn.DublLongGPS_IRI, GlobalVarLn.LatGPS_IRI, GlobalVarLn.LongGPS_IRI, 1);
                    if (
                        ((GlobalVarLn.LatGPS_IRI != GlobalVarLn.DublLatGPS_IRI) ||
                        (GlobalVarLn.LongGPS_IRI != GlobalVarLn.DublLongGPS_IRI)) &&
                        (sss > dds)
                        )
                    {
                        // Убрать с карты
                        GlobalVarLn.axMapScreenGlobal.Repaint();
                        // Redraw
                        ClassMap.f_Map_Redraw_GPS_IRI();
                    }
                    GlobalVarLn.DublLatGPS_IRI = GlobalVarLn.LatGPS_IRI;
                    GlobalVarLn.DublLongGPS_IRI = GlobalVarLn.LongGPS_IRI;
                }
*/
                // ......................................................................

            } // GlobalVarLn.flGPS_IRI==1


        }
        // ************************************************************ Add_One_ИРИФРЧ

        // Get_Pel_ФРЧ ***************************************************************
        // Get and draw Peleng ФРЧ

        public static void GetTempFWS_Pel(
                                  List<TempFWS> lstTableTempFWS
                                       )
        {
            try
            {
                // ------------------------------------------------------------------------
                bool fldraw = false;
                int i = 0;
                int j = 0;

                bool fl = false;

                double bearing1 = -1; // for sp1
                double bearing2 = -1; // for sp2
                                      // -------------------------------------------------------------------------

                // -------------------------------------------------------------------------
                // Delete all

                if ((GlobalVarLn.listGPS_IRI.Count != 0) && (lstTableTempFWS.Count == 0))
                {
                    ClassMap.DelTempFWS_Pel();
                    // Del and redraw
                    GlobalVarLn.axMapScreenGlobal.Repaint();
                    return;
                }
                // -------------------------------------------------------------------------
                // Add Peleng1, Peleng2

                // IF1
                if (GlobalVarLn.list1_Sost.Count != 0)
                {
                    // 18.07
                    int index1 = lstTableTempFWS.FindIndex(x => ((x.IsSelected.HasValue) && (x.IsSelected.Value == true)));
                    if (index1 >= 0)
                    {
                        // .............................................................
                        // FOR2
                        for (j = 0; j < lstTableTempFWS[i].ListQ.Count; j++)
                        {

                            if (
                               (lstTableTempFWS[index1].ListQ[j].IsOwn == true) &&
                               (bearing1 == -1) &&
                               (lstTableTempFWS[index1].ListQ[j].Bearing != -1) &&
                               (GlobalVarLn.flCoordSP_Sost == 1)
                              )
                            {
                                bearing1 = (double)lstTableTempFWS[index1].ListQ[j].Bearing;
                                //if (bearing1 > 270)
                                //    bearing1 = bearing1;


                            }

                            if (
                                 (lstTableTempFWS[index1].ListQ[j].IsOwn == false) &&
                                 (bearing2 == -1) &&
                                 (lstTableTempFWS[index1].ListQ[j].Bearing != -1) &&
                                 (GlobalVarLn.flCoordYS1_Sost == 1)
                               )
                            {
                                bearing2 = (double)lstTableTempFWS[index1].ListQ[j].Bearing;
                            }

                        } // FOR2
                          // .............................................................

                    }  // index1>=0


                    fldraw = AddPelengBD(bearing1, bearing2);



                    /*

                                    // FOR1
                                    for (i = 0; i < lstTableTempFWS.Count; i++)
                                    {
                                        // IF2
                                        if (lstTableTempFWS[i].IsSelected.HasValue)
                                        {
                                            fl = lstTableTempFWS[i].IsSelected.Value;
                                            // IF3
                                            if(fl==true)
                                            {

                                            // .............................................................
                                            // FOR2
                                             for(j=0; j< lstTableTempFWS[i].ListQ.Count; j++)
                                              {

                                               if(
                                                  (lstTableTempFWS[i].ListQ[j].IsOwn==true)&&
                                                  (bearing1==-1)&&
                                                  (lstTableTempFWS[i].ListQ[j].Bearing!=-1)&&
                                                  (GlobalVarLn.flCoordSP_Sost==1)
                                                 )
                                                {
                                                 bearing1 = (double)lstTableTempFWS[i].ListQ[j].Bearing;
                                                        //if (bearing1 > 270)
                                                        //    bearing1 = bearing1;


                                                }

                                             if (
                                                  (lstTableTempFWS[i].ListQ[j].IsOwn == false) &&
                                                  (bearing2 == -1) &&
                                                  (lstTableTempFWS[i].ListQ[j].Bearing != -1) &&
                                                  (GlobalVarLn.flCoordYS1_Sost == 1)
                                                )
                                             {
                                                bearing2 = (double)lstTableTempFWS[i].ListQ[j].Bearing;
                                             }

                                            } // FOR2
                                              // .............................................................

                                                fldraw=AddPelengBD(bearing1,bearing2);

                                            } // IF3: fl==true

                                        } // IF2: lstTableTempFWS[i].IsSelected.HasValue

                                    } // FOR1
                    */







                } // IF1: GlobalVarLn.list1_Sost.Count
                  // ..........................................................................
                  // Del and redraw
                  // По Tick Timer2 (1s)

                // 18.07
                if (fldraw == true)
                    GlobalVarLn.axMapScreenGlobal.Repaint();
                // 1-й раз при загрузке таблиц
                //if (GlobalVarLn.flfrh_first == false)
                //{
                //    GlobalVarLn.flfrh_first = true;
                //    GlobalVarLn.axMapScreenGlobal.Repaint();
                //}
                // ..........................................................................
            }
            catch
            {

            }

        }
        // *************************************************************** Get_Pel_ФРЧ

        // Del_Pel_ФРЧ ***************************************************************
        // Del all

        public static void DelTempFWS_Pel()
        {
            if (GlobalVarLn.chbpfrh == true)
            {

                GlobalVarLn.PrevP1 = -1;
                GlobalVarLn.PrevP2 = -1;
                GlobalVarLn.flPelMain2 = 0;   // 2-й пеленг
                GlobalVarLn.fl_PelIRI_1 = 0;  // 1-й пеленг
                GlobalVarLn.flag_eq_draw1 = 0;
                GlobalVarLn.flag_eq_draw2 = 0;
                GlobalVarLn.PELENGG_1 = -1;
                GlobalVarLn.PELENGG_2 = -1;
            }
        }
        // *************************************************************** Del_Pel_ФРЧ

        // Del_Peleng1 ***************************************************************

        public static void DelPeleng1()
        {
            if (GlobalVarLn.chbpfrh == true)
            {
                GlobalVarLn.PrevP1 = -1;
                //GlobalVarLn.PrevP2 = -1;
                //GlobalVarLn.flPelMain2 = 0;   // 2-й пеленг
                GlobalVarLn.fl_PelIRI_1 = 0;  // 1-й пеленг
                GlobalVarLn.flag_eq_draw1 = 0;
                //GlobalVarLn.flag_eq_draw2 = 0;
                GlobalVarLn.PELENGG_1 = -1;
                //GlobalVarLn.PELENGG_2 = -1;
            }
        }
        // *************************************************************** Del_Peleng1

        // Del_Peleng2 ***************************************************************

        public static void DelPeleng2()
        {
            if (GlobalVarLn.chbpfrh == true)
            {
                //GlobalVarLn.PrevP1 = -1;
                GlobalVarLn.PrevP2 = -1;
                GlobalVarLn.flPelMain2 = 0;   // 2-й пеленг
                //GlobalVarLn.fl_PelIRI_1 = 0;  // 1-й пеленг
                //GlobalVarLn.flag_eq_draw1 = 0;
                GlobalVarLn.flag_eq_draw2 = 0;
                //GlobalVarLn.PELENGG_1 = -1;
                GlobalVarLn.PELENGG_2 = -1;
            }
        }
        // *************************************************************** Del_Peleng2

        // Add_Pel_ФРЧ ***************************************************************
        // Add Peleng ФРЧ

        public static bool AddPelengBD(
                                  double bearing1,double bearing2
                                   )
        {

            //if (chbPeleng.Checked == true)
            if (GlobalVarLn.chbpfrh == true)
            {
                // -------------------------------------------------------------------------
                double azgr1 = -1;
                double azgr2 = -1;
                bool fldraw = false;
                // -------------------------------------------------------------------------
                // Peleng1

                if (bearing1 != -1)
                {
                    //azgr1 = (double)(bearing1 / 10d); // Peleng1, grad
                    azgr1 = bearing1; // Peleng1, grad
                    GlobalVarLn.fl_PelIRI_1 = 1;
                }
                else
                {
                    GlobalVarLn.fl_PelIRI_1 = 0;
                }
                // -------------------------------------------------------------------------
                // Peleng2

                if (bearing2 != -1)
                {
                    azgr2 = bearing2; // Peleng2, grad
                    GlobalVarLn.flPelMain2 = 1;
                }
                else
                {
                    GlobalVarLn.flPelMain2 = 0;
                }
                // -------------------------------------------------------------------------
                GlobalVarLn.PELENGG_1 = azgr1;
                GlobalVarLn.PELENGG_2 = azgr2;
                // -------------------------------------------------------------------------

                double a1 = 0;
                double b1 = 0;
                double c1 = 0;
                double d1 = 0;
                double f1 = 0;
                double g1 = 0;
                double a2 = 0;
                double b2 = 0;
                double c2 = 0;
                double d2 = 0;
                double f2 = 0;
                double g2 = 0;
                double x184 = 0;
                double y184 = 0;
                double x284 = 0;
                double y284 = 0;

                double[] arr_Pel_XYZ1 = new double[GlobalVarLn.numberofdots * 3];
                double[] arr_Pel_XYZ2 = new double[GlobalVarLn.numberofdots * 3];
                ClassMap classMap = new ClassMap();
                ClassMap classMap1 = new ClassMap();
                // -------------------------------------------------------------------------
                GlobalVarLn.X1_PelMain = (int)GlobalVarLn.list1_Sost[0].X_m;  // SP1
                GlobalVarLn.Y1_PelMain = (int)GlobalVarLn.list1_Sost[0].Y_m;
                GlobalVarLn.X1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].X_m; // SP2
                GlobalVarLn.Y1_1_PelMain = (int)GlobalVarLn.list1_Sost[1].Y_m;

                x184 = GlobalVarLn.X1_PelMain;
                y184 = GlobalVarLn.Y1_PelMain;
                x284 = GlobalVarLn.X1_1_PelMain;
                y284 = GlobalVarLn.Y1_1_PelMain;

                mapPlaneToGeo(GlobalVarLn.hmapl, ref x184, ref y184);
                mapPlaneToGeo(GlobalVarLn.hmapl, ref x284, ref y284);

                // rad->grad
                x184 = (x184 * 180) / Math.PI;
                y184 = (y184 * 180) / Math.PI;
                x284 = (x284 * 180) / Math.PI;
                y284 = (y284 * 180) / Math.PI;
                // -------------------------------------------------------------------------

                // PELENG1 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                // .........................................................................
                // Пеленг исчез

                // IF1
                if ((azgr1 == -1) && (GlobalVarLn.PrevP1 != -1))
                {
                    fldraw = true;
                    ClassMap.DelPeleng1();

                } // IF1
                  // .........................................................................
                  // Новый пеленг или старый, но изменилось положение станции 

                // ELSE on IF1
                else if (
                       ((azgr1 != -1) && (GlobalVarLn.PrevP1 == -1)) ||
                       ((azgr1 != -1) && (GlobalVarLn.PrevP1 != -1) && ((Math.Abs(azgr1 - GlobalVarLn.PrevP1)) > 1)) ||
                       (GlobalVarLn.flag_eq_draw1 == 1)
                       )
                {
                    fldraw = true;

                    GlobalVarLn.flag_eq_draw1 = 0;

                    // Peleng1
                    if (GlobalVarLn.fl_PelIRI_1 == 1)
                    {
                        classMap.f_Peleng(azgr1, GlobalVarLn.distance, GlobalVarLn.numberofdots, x184, y184,
                                          ref a1, ref b1, ref c1, ref d1, ref f1, ref g1,
                                          ref GlobalVarLn.arr_Pel1, ref arr_Pel_XYZ1);
                    }

                    GlobalVarLn.PrevP1 = azgr1;

                } // ELSE on IF1
                  // .........................................................................

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PELENG1

                // PELENG2 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                // .........................................................................
                // Пеленг исчез

                // IF2
                if ((azgr2 == -1) && (GlobalVarLn.PrevP2 != -1))
                {
                    fldraw = true;
                    ClassMap.DelPeleng2();

                } // IF2
                  // .........................................................................
                  // Новый пеленг или старый, но изменилось положение станции 

                // ELSE on IF2
                else if (
                         ((azgr2 != -1) && (GlobalVarLn.PrevP2 == -1)) ||
                         ((azgr2 != -1) && (GlobalVarLn.PrevP2 != -1) && ((Math.Abs(azgr2 - GlobalVarLn.PrevP2)) > 1)) ||
                         (GlobalVarLn.flag_eq_draw2 == 1)
                        )
                {
                    fldraw = true;

                    GlobalVarLn.flag_eq_draw2 = 0;

                    // Peleng2
                    if (GlobalVarLn.flPelMain2 == 1)
                    {
                        classMap1.f_Peleng(azgr2, GlobalVarLn.distance, GlobalVarLn.numberofdots, x284, y284,
                                           ref a2, ref b2, ref c2, ref d2, ref f2, ref g2,
                                           ref GlobalVarLn.arr_Pel2, ref arr_Pel_XYZ2);
                    }

                    GlobalVarLn.PrevP2 = azgr2;

                } // ELSE on IF2
                  // .........................................................................

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PELENG2

                return fldraw;

            } // Chacked==true

            else return false;
        }
        // *************************************************************** Add_Pel_ФРЧ

        // Get_ИРИ_ППРЧ **************************************************************
        // Get and draw ИРИ ППРЧ
        // !!! ППРЧ: с одним ID может прийти List IRI (т.е. несколько ИРИ в одном  ID)

        public static void GetTableReconFHSS_DB(
                                        //List<TableReconFHSS> lstTableTableReconFHSS
                                        List<ClassTableReconFHSS_Dop> lstTableTableReconFHSS

                                               )
        {

            /*
                        // ------------------------------------------------------------------------
                        bool fldraw = false;
                        int i = 0;
                        int j = 0;
                        int k = 0;
                        int l = 0;
                        bool f = false;
                        double freqd = 0;

                        double dds = 250; // m
                        double sss = 0;

                        // -------------------------------------------------------------------------
                        // Нужна ли перерисовка: есть новые или пришли старые с измененном положением
                        // (при этом расстояние > 250m)

                        // ..........................................................................
                        // Есть ли новые пачки ИРИ (с другим ОБЩИМ ID)

                        // FOR1: new list 
                        for (i = 0; i < lstTableTableReconFHSS.Count; i++)
                        {
                            f = false;
                            // FOR2: old list
                            for (j = 0; j < GlobalVarLn.list_ClassTableReconFHSS_Dop.Count; j++)
                            {
                                if (lstTableTableReconFHSS[i].Id == GlobalVarLn.list_ClassTableReconFHSS_Dop[j].Id)
                                {
                                    f = true;
                                }

                            } // FOR2: Old list

                            // в старом не было такого ИРИ
                            if (f == false)
                            {
                                fldraw = true;
                                i = lstTableTableReconFHSS.Count + 1; // exit from FOR1
                            }

                        } // FOR1 new list
                        // ..........................................................................
                        // Новых пачек нет, но не исчезла ли какая-либо старая пачка

                        if(fldraw==false)
                        {
                            // FOR1: !!!OLD list 
                            for (i = 0; i < GlobalVarLn.list_ClassTableReconFHSS_Dop.Count; i++)
                            {
                                f = false;
                                // FOR2: !!!NEW list
                                for (j = 0; j < lstTableTableReconFHSS.Count; j++)
                                {
                                    if (lstTableTableReconFHSS[j].Id == GlobalVarLn.list_ClassTableReconFHSS_Dop[i].Id)
                                    {
                                        f = true;
                                    }

                                } // FOR2: !!!NEW list

                                // в новом уже нет такого ИРИ
                                if (f == false)
                                {
                                    fldraw = true;
                                    i = GlobalVarLn.list_ClassTableReconFHSS_Dop.Count + 1; // exit from FOR1
                                }

                            } // FOR1 !!!OLD list

                        } // IF (новых пачек не было)
                        // ..........................................................................
                        // Есть ли в имеющихся пачках ИРИ новые ИРИ

                        // Новых пачек не было и старые пачки не исчезли
                        if (fldraw==false)
                        {
                            // FOR1: new list 
                            for (i = 0; i < lstTableTableReconFHSS.Count; i++)
                            {
                                // FOR2: old list
                                for (j = 0; j < GlobalVarLn.list_ClassTableReconFHSS_Dop.Count; j++)
                                {
                                    // IF (ID пачек==)
                                    if (lstTableTableReconFHSS[i].Id == GlobalVarLn.list_ClassTableReconFHSS_Dop[j].Id)
                                    {
                                        // FOR3 (внутри i-й пачки)
                                        // new
                                        for (k = 0; k < lstTableTableReconFHSS[i].ListSource.Count; k++)
                                            {
                                                f = false;
                                            // FOR4 (внутри j-й пачки)
                                            // old
                                            for (l = 0; l < GlobalVarLn.list_ClassTableReconFHSS_Dop[j].lst_PelIRI2.Count; l++)
                                            { 
                                                if (lstTableTableReconFHSS[i].ListSource[k].Id == GlobalVarLn.list_ClassTableReconFHSS_Dop[j].lst_PelIRI2[l].Num)
                                                {
                                                    f = true;
                                                }

                                            } // FOR4 (внутри j-й пачки) old

                                            // в старой пачке не было такого ИРИ
                                            if (f == false)
                                            {
                                                fldraw = true;
                                                k = lstTableTableReconFHSS[i].ListSource.Count+1; // exit from FOR3
                                                //j = GlobalVarLn.list_ClassTableReconFHSS_Dop.Count + 1; // exit from FOR2
                                                //i = lstTableTableReconFHSS.Count + 1; // exit from FOR1

                                            } // IF (в старой пачке не было такого ИРИ)

                                        } // FOR3 new (внутри i-й пачки)

                                    } // IF (ID пачек==)

                                    if(fldraw==true)
                                    {
                                        j = GlobalVarLn.list_ClassTableReconFHSS_Dop.Count + 1; // exit from FOR2
                                        i = lstTableTableReconFHSS.Count + 1; // exit from FOR1
                                    }

                                } // FOR2: Old list

                            } // FOR1 new list

                        } // Новых пачек не было и старые пачки не исчезли
                        // ..........................................................................
                        // Не исчезли ли в имеющихся пачках некоторые ИРИ

                        // Новых пачек не появилось,старые пачки не исчезли, внутри имеющихся пачек не появились новые ИРИ
                        if(fldraw==false)
                        {
                            // FOR1: !!!OLD list 
                            for (i = 0; i < GlobalVarLn.list_ClassTableReconFHSS_Dop.Count; i++)
                            {
                                // FOR2: !!!NEW list
                                for (j = 0; j < lstTableTableReconFHSS.Count; j++)
                                {
                                    // IF (ID пачек==)
                                    if (lstTableTableReconFHSS[j].Id == GlobalVarLn.list_ClassTableReconFHSS_Dop[i].Id)
                                    {
                                        // FOR3 (внутри i-й пачки)
                                        // old
                                        for (k = 0; k < GlobalVarLn.list_ClassTableReconFHSS_Dop[i].lst_PelIRI2.Count; k++)
                                        {
                                            f = false;
                                            // FOR4 (внутри j-й пачки)
                                            // new


                                            for (l = 0; l < lstTableTableReconFHSS[j].ListSource.Count; l++)
                                            {
                                                if (lstTableTableReconFHSS[j].ListSource[l].Id == GlobalVarLn.list_ClassTableReconFHSS_Dop[i].lst_PelIRI2[k].Num)
                                                {
                                                    f = true;
                                                }

                                            } // FOR4 (внутри j-й пачки) new

                                            // в новой пачке уже нет такого ИРИ
                                            if (f == false)
                                            {
                                                fldraw = true;
                                                k = GlobalVarLn.list_ClassTableReconFHSS_Dop[i].lst_PelIRI2.Count+1; // exit from FOR3
                                                //j = lstTableTableReconFHSS.Count + 1; // exit from FOR2
                                                //i = GlobalVarLn.list_ClassTableReconFHSS_Dop.Count + 1; // exit from FOR1

                                            } // IF (в новой пачке уже нет такого ИРИ)

                                        } // FOR3 old (внутри i-й пачки)

                                    } // IF (ID пачек==)

                                    if(fldraw==true)
                                    {
                                        j = lstTableTableReconFHSS.Count + 1; // exit from FOR2
                                        i = GlobalVarLn.list_ClassTableReconFHSS_Dop.Count + 1; // exit from FOR1
                                    }

                                } // FOR2: !!!NEW list

                            } // FOR1 !!!OLD list

                        } // IF(// Новых пачек не появилось,старые пачки не исчезли, внутри имеющихся пачек не появились новые ИРИ)
                          // ..........................................................................
                          // Изменилось ли внутри имеющихся пачек положение ИРИ с dS>250m

                        // Новых пачек не появилось,старые пачки не исчезли, 
                        // внутри имеющихся пачек не появились новые ИРИ,
                        //  в имеющихся пачках ИРИ не исчезли 
                        if (fldraw == false) // Новых ID не было
                        {
                            // FOR1: new list 
                            for (i = 0; i < lstTableTableReconFHSS.Count; i++)
                            {
                                // FOR2: old list
                                for (j = 0; j < GlobalVarLn.list_ClassTableReconFHSS_Dop.Count; j++)
                                {
                                    // IF (ID пачек==)
                                    if (lstTableTableReconFHSS[i].Id == GlobalVarLn.list_ClassTableReconFHSS_Dop[j].Id)
                                    {
                                        // FOR3 (внутри i-й пачки)
                                        // new
                                        for (k = 0; k < lstTableTableReconFHSS[i].ListSource.Count; k++)
                                        {
                                            f = false;
                                            // FOR4 (внутри j-й пачки)
                                            // old
                                            for (l = 0; l < GlobalVarLn.list_ClassTableReconFHSS_Dop[j].lst_PelIRI2.Count; l++)
                                            {
                                                if (lstTableTableReconFHSS[i].ListSource[k].Id == GlobalVarLn.list_ClassTableReconFHSS_Dop[j].lst_PelIRI2[l].Num)
                                                {
                                                    sss = ClassMap.f_D_2Points(GlobalVarLn.list_ClassTableReconFHSS_Dop[j].lst_PelIRI2[l].Lat,
                                                                               GlobalVarLn.list_ClassTableReconFHSS_Dop[j].lst_PelIRI2[l].Long,
                                                                               lstTableTableReconFHSS[i].ListSource[k].Coordinates.Latitude,
                                                                               lstTableTableReconFHSS[i].ListSource[k].Coordinates.Longitude, 1);
                                                    if (sss > dds)
                                                    {
                                                        fldraw = true;
                                                    }
                                                } // IF(Id==)

                                            } // FOR4 (внутри j-й пачки) old

                                            // новое положение отличается > 250m
                                            if (fldraw == true)
                                            {
                                                    k = lstTableTableReconFHSS[i].ListSource.Count + 1; // exit from FOR3
                                                    //j = GlobalVarLn.list_ClassTableReconFHSS_Dop.Count + 1; // exit from FOR2
                                                    //i = lstTableTableReconFHSS.Count + 1; // exit from FOR1
                                            }

                                        } // FOR3 new (внутри i-й пачки)

                                    } // IF (ID пачек==)

                                    if(fldraw==true)
                                    {
                                        j = GlobalVarLn.list_ClassTableReconFHSS_Dop.Count + 1; // exit from FOR2
                                        i = lstTableTableReconFHSS.Count + 1; // exit from FOR1
                                    }

                                } // FOR2: Old list

                            } // FOR1 new list

                            // IF
                            // Новых пачек не появилось,старые пачки не исчезли, 
                            // внутри имеющихся пачек не появились новые ИРИ,
                            //  в имеющихся пачках ИРИ не исчезли 
                        }
                        // ..........................................................................

                        // ------------------------------------------------------------------------
                        // Выяснилм, что нужна перерисовка

                        if (fldraw == true)
                        {

                            // ..........................................................................
                            // Del all IRI

                            ClassMap.DelTableReconFHSS_DB();
                            // ..........................................................................
                            // Add new IRI

                            if (lstTableTableReconFHSS.Count != 0)
                            {

                                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                // FOR1 new
                                for (i = 0; i < lstTableTableReconFHSS.Count; i++)
                                {
                                    // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                                    //PelIRI objIRI = new PelIRI();

                                    ClassTableReconFHSS_Dop objClassIRI = new ClassTableReconFHSS_Dop();
                                    objClassIRI.lst_PelIRI2 = new List<PelIRI>();
                                    // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                                    // ID пачки ИРИ

                                    objClassIRI.Id = lstTableTableReconFHSS[i].Id;
                                    // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                                    // Добавить все ИРИ внутри пачки

                                    // FOR2
                                    for (j = 0; j < lstTableTableReconFHSS[i].ListSource.Count; j++)
                                    {
                                        GlobalVarLn.LatGPS_IRI = lstTableTableReconFHSS[i].ListSource[j].Coordinates.Latitude;
                                        GlobalVarLn.LongGPS_IRI = lstTableTableReconFHSS[i].ListSource[j].Coordinates.Longitude;
                                        GlobalVarLn.IDGPS_IRI = lstTableTableReconFHSS[i].ListSource[j].Id;
                                        //freqd = lstTableTempFWS[i].FreqKHz;
                                        //GlobalVarLn.sGPS_IRI = Convert.ToString(freqd);
                                        //GlobalVarLn.fGPS_IRI = freqd;
                                        GlobalVarLn.ColGPS_IRI = Color.Red;

                                        ClassMap.Get_IRI_TableReconFHSS(objClassIRI.lst_PelIRI2);

                                    } // FOR2
                                    // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                                    // Добавить пачку ИРИ в лист

                                    GlobalVarLn.list_ClassTableReconFHSS_Dop.Add(objClassIRI);
                                    // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

                                } // FOR1 new
                                  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                  // Del and redraw

                                  //if (GlobalVarLn.axMapScreenGlobal.InvokeRequired)
                                  //    GlobalVarLn.axMapScreenGlobal.Invoke((Action)(() => { GlobalVarLn.axMapScreenGlobal.Repaint(); }));
                                  //else
                                      GlobalVarLn.axMapScreenGlobal.Repaint();

                                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                            } // IF (lstTableTableReconFHSS.Count!=0)
                            // ..........................................................................


                        } // IF(fldraw==true)
                        // ------------------------------------------------------------------------
            */

            // NEW ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // ------------------------------------------------------------------------
            bool fldraw = false;
            int i = 0;
            int j = 0;
            int k = 0;
            int l = 0;
            bool f = false;
            double freqd = 0;

            double dds = 250; // m
            double sss = 0;

            // -------------------------------------------------------------------------
            // Нужна ли перерисовка: есть новые или пришли старые с измененном положением
            // (при этом расстояние > 250m)

            // ..........................................................................
            // Есть ли новые пачки ИРИ (с другим ОБЩИМ ID)

            // FOR1: new list 
            for (i = 0; i < lstTableTableReconFHSS.Count; i++)
            {
                f = false;
                // FOR2: old list
                for (j = 0; j < GlobalVarLn.list_ClassTableReconFHSS_Dop.Count; j++)
                {
                    if (lstTableTableReconFHSS[i].Id == GlobalVarLn.list_ClassTableReconFHSS_Dop[j].Id)
                    {
                        f = true;
                    }

                } // FOR2: Old list

                // в старом не было такого ИРИ
                if (f == false)
                {
                    fldraw = true;
                    i = lstTableTableReconFHSS.Count + 1; // exit from FOR1
                }

            } // FOR1 new list
              // ..........................................................................
              // Новых пачек нет, но не исчезла ли какая-либо старая пачка

            if (fldraw == false)
            {
                // FOR1: !!!OLD list 
                for (i = 0; i < GlobalVarLn.list_ClassTableReconFHSS_Dop.Count; i++)
                {
                    f = false;
                    // FOR2: !!!NEW list
                    for (j = 0; j < lstTableTableReconFHSS.Count; j++)
                    {
                        if (lstTableTableReconFHSS[j].Id == GlobalVarLn.list_ClassTableReconFHSS_Dop[i].Id)
                        {
                            f = true;
                        }

                    } // FOR2: !!!NEW list

                    // в новом уже нет такого ИРИ
                    if (f == false)
                    {
                        fldraw = true;
                        i = GlobalVarLn.list_ClassTableReconFHSS_Dop.Count + 1; // exit from FOR1
                    }

                } // FOR1 !!!OLD list

            } // IF (новых пачек не было)
              // ..........................................................................
              // Есть ли в имеющихся пачках ИРИ новые ИРИ

            // Новых пачек не было и старые пачки не исчезли
            if (fldraw == false)
            {
                // FOR1: new list 
                for (i = 0; i < lstTableTableReconFHSS.Count; i++)
                {
                    // FOR2: old list
                    for (j = 0; j < GlobalVarLn.list_ClassTableReconFHSS_Dop.Count; j++)
                    {
                        // IF (ID пачек==)
                        if (lstTableTableReconFHSS[i].Id == GlobalVarLn.list_ClassTableReconFHSS_Dop[j].Id)
                        {
                            // FOR3 (внутри i-й пачки)
                            // new
                            for (k = 0; k < lstTableTableReconFHSS[i].lst_PelIRI2.Count; k++)
                            {
                                f = false;
                                // FOR4 (внутри j-й пачки)
                                // old
                                for (l = 0; l < GlobalVarLn.list_ClassTableReconFHSS_Dop[j].lst_PelIRI2.Count; l++)
                                {
                                    if (lstTableTableReconFHSS[i].lst_PelIRI2[k].Num == GlobalVarLn.list_ClassTableReconFHSS_Dop[j].lst_PelIRI2[l].Num)
                                    {
                                        f = true;
                                    }

                                } // FOR4 (внутри j-й пачки) old

                                // в старой пачке не было такого ИРИ
                                if (f == false)
                                {
                                    fldraw = true;
                                    k = lstTableTableReconFHSS[i].lst_PelIRI2.Count + 1; // exit from FOR3
                                                                                        //j = GlobalVarLn.list_ClassTableReconFHSS_Dop.Count + 1; // exit from FOR2
                                                                                        //i = lstTableTableReconFHSS.Count + 1; // exit from FOR1

                                } // IF (в старой пачке не было такого ИРИ)

                            } // FOR3 new (внутри i-й пачки)

                        } // IF (ID пачек==)

                        if (fldraw == true)
                        {
                            j = GlobalVarLn.list_ClassTableReconFHSS_Dop.Count + 1; // exit from FOR2
                            i = lstTableTableReconFHSS.Count + 1; // exit from FOR1
                        }

                    } // FOR2: Old list

                } // FOR1 new list

            } // Новых пачек не было и старые пачки не исчезли
              // ..........................................................................
              // Не исчезли ли в имеющихся пачках некоторые ИРИ

            // Новых пачек не появилось,старые пачки не исчезли, внутри имеющихся пачек не появились новые ИРИ
            if (fldraw == false)
            {
                // FOR1: !!!OLD list 
                for (i = 0; i < GlobalVarLn.list_ClassTableReconFHSS_Dop.Count; i++)
                {
                    // FOR2: !!!NEW list
                    for (j = 0; j < lstTableTableReconFHSS.Count; j++)
                    {
                        // IF (ID пачек==)
                        if (lstTableTableReconFHSS[j].Id == GlobalVarLn.list_ClassTableReconFHSS_Dop[i].Id)
                        {
                            // FOR3 (внутри i-й пачки)
                            // old
                            for (k = 0; k < GlobalVarLn.list_ClassTableReconFHSS_Dop[i].lst_PelIRI2.Count; k++)
                            {
                                f = false;
                                // FOR4 (внутри j-й пачки)
                                // new

                                for (l = 0; l < lstTableTableReconFHSS[j].lst_PelIRI2.Count; l++)
                                {
                                    if (lstTableTableReconFHSS[j].lst_PelIRI2[l].Num == GlobalVarLn.list_ClassTableReconFHSS_Dop[i].lst_PelIRI2[k].Num)
                                    {
                                        f = true;
                                    }

                                } // FOR4 (внутри j-й пачки) new

                                // в новой пачке уже нет такого ИРИ
                                if (f == false)
                                {
                                    fldraw = true;
                                    k = GlobalVarLn.list_ClassTableReconFHSS_Dop[i].lst_PelIRI2.Count + 1; // exit from FOR3
                                    //j = lstTableTableReconFHSS.Count + 1; // exit from FOR2
                                    //i = GlobalVarLn.list_ClassTableReconFHSS_Dop.Count + 1; // exit from FOR1

                                } // IF (в новой пачке уже нет такого ИРИ)

                            } // FOR3 old (внутри i-й пачки)

                        } // IF (ID пачек==)

                        if (fldraw == true)
                        {
                            j = lstTableTableReconFHSS.Count + 1; // exit from FOR2
                            i = GlobalVarLn.list_ClassTableReconFHSS_Dop.Count + 1; // exit from FOR1
                        }

                    } // FOR2: !!!NEW list

                } // FOR1 !!!OLD list

            } // IF(// Новых пачек не появилось,старые пачки не исчезли, внутри имеющихся пачек не появились новые ИРИ)
              // ..........................................................................
              // Изменилось ли внутри имеющихся пачек положение ИРИ с dS>250m

            // Новых пачек не появилось,старые пачки не исчезли, 
            // внутри имеющихся пачек не появились новые ИРИ,
            //  в имеющихся пачках ИРИ не исчезли 
            if (fldraw == false) // Новых ID не было
            {
                // FOR1: new list 
                for (i = 0; i < lstTableTableReconFHSS.Count; i++)
                {
                    // FOR2: old list
                    for (j = 0; j < GlobalVarLn.list_ClassTableReconFHSS_Dop.Count; j++)
                    {
                        // IF (ID пачек==)
                        if (lstTableTableReconFHSS[i].Id == GlobalVarLn.list_ClassTableReconFHSS_Dop[j].Id)
                        {
                            // FOR3 (внутри i-й пачки)
                            // new
                            for (k = 0; k < lstTableTableReconFHSS[i].lst_PelIRI2.Count; k++)
                            {
                                f = false;
                                // FOR4 (внутри j-й пачки)
                                // old
                                for (l = 0; l < GlobalVarLn.list_ClassTableReconFHSS_Dop[j].lst_PelIRI2.Count; l++)
                                {
                                    if (lstTableTableReconFHSS[i].lst_PelIRI2[k].Num == GlobalVarLn.list_ClassTableReconFHSS_Dop[j].lst_PelIRI2[l].Num)
                                    {
                                        sss = ClassMap.f_D_2Points(GlobalVarLn.list_ClassTableReconFHSS_Dop[j].lst_PelIRI2[l].Lat,
                                                                   GlobalVarLn.list_ClassTableReconFHSS_Dop[j].lst_PelIRI2[l].Long,
                                                                   lstTableTableReconFHSS[i].lst_PelIRI2[k].Lat,
                                                                   lstTableTableReconFHSS[i].lst_PelIRI2[k].Long, 1);
                                        if (sss > dds)
                                        {
                                            fldraw = true;
                                        }
                                    } // IF(Id==)

                                } // FOR4 (внутри j-й пачки) old

                                // новое положение отличается > 250m
                                if (fldraw == true)
                                {
                                    k = lstTableTableReconFHSS[i].lst_PelIRI2.Count + 1; // exit from FOR3
                                                                                        //j = GlobalVarLn.list_ClassTableReconFHSS_Dop.Count + 1; // exit from FOR2
                                                                                        //i = lstTableTableReconFHSS.Count + 1; // exit from FOR1
                                }

                            } // FOR3 new (внутри i-й пачки)

                        } // IF (ID пачек==)

                        if (fldraw == true)
                        {
                            j = GlobalVarLn.list_ClassTableReconFHSS_Dop.Count + 1; // exit from FOR2
                            i = lstTableTableReconFHSS.Count + 1; // exit from FOR1
                        }

                    } // FOR2: Old list

                } // FOR1 new list

                // IF
                // Новых пачек не появилось,старые пачки не исчезли, 
                // внутри имеющихся пачек не появились новые ИРИ,
                //  в имеющихся пачках ИРИ не исчезли 
            }
            // ..........................................................................

            // ------------------------------------------------------------------------
            // Выяснилм, что нужна перерисовка

            if (fldraw == true)
            {

                // ..........................................................................
                // Del all IRI

                ClassMap.DelTableReconFHSS_DB();
                // ..........................................................................
                // Add new IRI

                if (lstTableTableReconFHSS.Count != 0)
                {

                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // FOR1 new
                    for (i = 0; i < lstTableTableReconFHSS.Count; i++)
                    {
                        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                        //PelIRI objIRI = new PelIRI();

                        ClassTableReconFHSS_Dop objClassIRI = new ClassTableReconFHSS_Dop();
                        objClassIRI.lst_PelIRI2 = new List<PelIRI>();
                        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                        // ID пачки ИРИ

                        objClassIRI.Id = lstTableTableReconFHSS[i].Id;
                        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                        // Добавить все ИРИ внутри пачки

                        // FOR2
                        for (j = 0; j < lstTableTableReconFHSS[i].lst_PelIRI2.Count; j++)
                        {
                            GlobalVarLn.LatGPS_IRI = lstTableTableReconFHSS[i].lst_PelIRI2[j].Lat;
                            GlobalVarLn.LongGPS_IRI = lstTableTableReconFHSS[i].lst_PelIRI2[j].Long;
                            GlobalVarLn.IDGPS_IRI = lstTableTableReconFHSS[i].lst_PelIRI2[j].Num;
                            //freqd = lstTableTempFWS[i].FreqKHz;
                            //GlobalVarLn.sGPS_IRI = Convert.ToString(freqd);
                            //GlobalVarLn.fGPS_IRI = freqd;
                            GlobalVarLn.ColGPS_IRI = Color.Red;

                            ClassMap.Get_IRI_TableReconFHSS(objClassIRI.lst_PelIRI2);

                        } // FOR2
                          // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                          // Добавить пачку ИРИ в лист

                        GlobalVarLn.list_ClassTableReconFHSS_Dop.Add(objClassIRI);
                        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

                    } // FOR1 new
                      // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                      // Del and redraw

                    //if (GlobalVarLn.axMapScreenGlobal.InvokeRequired)
                    //    GlobalVarLn.axMapScreenGlobal.Invoke((Action)(() => { GlobalVarLn.axMapScreenGlobal.Repaint(); }));
                    //else
                    GlobalVarLn.axMapScreenGlobal.Repaint();

                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                } // IF (lstTableTableReconFHSS.Count!=0)
                  // ..........................................................................


            } // IF(fldraw==true)
              // ------------------------------------------------------------------------


            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ NEW

        }
        // *************************************************************** Get_ИРИ_ППРЧ

        // Del_ИРИ_ППРЧ ***************************************************************
        // Del all

        public static void DelTableReconFHSS_DB()
        {
            int iij = 0;

            for (iij = (GlobalVarLn.list_ClassTableReconFHSS_Dop.Count - 1); iij >= 0; iij--)
            {
                GlobalVarLn.list_ClassTableReconFHSS_Dop.Remove(GlobalVarLn.list_ClassTableReconFHSS_Dop[iij]);
            }

            GlobalVarLn.flGPS_IRI = 0;
        }
        // *************************************************************** Del_ИРИ_ППРЧ

        // Add_One_ИРИППРЧ ************************************************************

        public static void Get_IRI_TableReconFHSS(List<PelIRI> lst_IRIPPRH)
        {
            double xtmp_ed, ytmp_ed, htmp_ed;
            double xtmp1_ed, ytmp1_ed;

            xtmp_ed = 0;
            ytmp_ed = 0;
            htmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;

            //AirPlane objLF = new AirPlane();
            PelIRI objLF = new PelIRI();
            // -----------------------------------------------------------------------------
            // !!! Get to (WGS84)

            // -----------------------------------------------------------------------------

            //if (GlobalVarLn.flGPS_IRI == 1)
            //{
                // ......................................................................
                ClassMap objClassMap1_ed = new ClassMap();
                ClassMap objClassMap2_ed = new ClassMap();
                ClassMap objClassMap3_ed = new ClassMap();
                // ......................................................................
                // grad(WGS84)

                xtmp1_ed = GlobalVarLn.LatGPS_IRI;
                ytmp1_ed = GlobalVarLn.LongGPS_IRI;
                // ......................................................................

                // WGS84(эллипсоид)->элл.Красовского *************************************
                // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
                // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
                // Входные параметры -> град,km
                // Перевод в рад - внутри функции

                // dLong ..................................................................
                // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
                // (преобразования Молоденского), угл.сек

                objClassMap1_ed.f_dLong
                    (
                        // Входные параметры (град,км)
                        xtmp1_ed,   // широта
                        ytmp1_ed,  // долгота
                        0,     // высота

                        // DATUM,m
                        GlobalVarLn.dXdat_comm,
                        GlobalVarLn.dYdat_comm,
                        GlobalVarLn.dZdat_comm,

                        ref GlobalVarLn.dLong_GPS   // приращение по долготе, угл.сек
                    );
                // ................................................................ dLong

                // dLat .................................................................
                // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
                // (преобразования Молоденского), угл.сек

                objClassMap1_ed.f_dLat
                    (
                        // Входные параметры (град,км)
                        xtmp1_ed,   // широта
                        ytmp1_ed,  // долгота
                        0,     // высота

                        // DATUM,m
                        GlobalVarLn.dXdat_comm,
                        GlobalVarLn.dYdat_comm,
                        GlobalVarLn.dZdat_comm,

                        ref GlobalVarLn.dLat_GPS        // приращение по долготе, угл.сек
                    );
                // ................................................................. dLat

                // Lat,Long .............................................................
                // Преобразования широты и долготы при пересчете WGS84->SK42

                objClassMap1_ed.f_WGS84_SK42_Lat_Long
                       (
                           // Входные параметры (град,км)
                           xtmp1_ed,   // широта
                           ytmp1_ed,  // долгота
                           0,     // высота
                           GlobalVarLn.dLat_GPS,       // приращение по долготе, угл.сек
                           GlobalVarLn.dLong_GPS,      // приращение по долготе, угл.сек

                           // Выходные параметры (grad)
                           ref GlobalVarLn.LatKrG_GPS,   // широта
                           ref GlobalVarLn.LongKrG_GPS   // долгота
                       );
                // ............................................................ Lat,Long


                // *********************************** WGS84(эллипсоид)->элл.Красовского

                // ......................................................................
                // ellipsKras -> M real

                xtmp_ed = GlobalVarLn.LatKrG_GPS;
                ytmp_ed = GlobalVarLn.LongKrG_GPS;

                // Grad->rad
                xtmp_ed = (xtmp_ed * Math.PI) / 180;
                ytmp_ed = (ytmp_ed * Math.PI) / 180;

                mapGeoToPlane(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

                GlobalVarLn.XGPS_IRI = xtmp_ed;
                GlobalVarLn.YGPS_IRI = ytmp_ed;
                // ......................................................................
                // !!! реальные координаты на местности карты в м (Plane)

                //objLF.f = GlobalVarLn.fGPS_IRI;
                objLF.Lat = GlobalVarLn.LatGPS_IRI;
                objLF.Long = GlobalVarLn.LongGPS_IRI;
                objLF.X_m = xtmp_ed;
                objLF.Y_m = ytmp_ed;
                //objLF.sNum = GlobalVarLn.sGPS_IRI;
                objLF.Num = GlobalVarLn.IDGPS_IRI;
                // ......................................................................
                objLF.cl = GlobalVarLn.ColGPS_IRI;
                // ......................................................................

                lst_IRIPPRH.Add(objLF);
                // ......................................................................

            //} // GlobalVarLn.flGPS_IRI==1


        }
        // ************************************************************ Add_One_ИРИППРЧ

        // ReDraw_IRI_PPRH ************************************************************

        public static void f_RedrawIRI_PPRH()
        {
            // FOR1 (пачки ИРИ ППРЧ)
            for (int i = 0; i < GlobalVarLn.list_ClassTableReconFHSS_Dop.Count; i++)
            {
                for (int j = 0; j < GlobalVarLn.list_ClassTableReconFHSS_Dop[i].lst_PelIRI2.Count; j++)
                {
                    ClassMap.f_Pol_GPS_IRI(
                                               GlobalVarLn.list_ClassTableReconFHSS_Dop[i].lst_PelIRI2[j].X_m,
                                               GlobalVarLn.list_ClassTableReconFHSS_Dop[i].lst_PelIRI2[j].Y_m,
                                               GlobalVarLn.list_ClassTableReconFHSS_Dop[i].lst_PelIRI2[j].cl,
                                               ""
                                             );
                }

            } // FOR1 (пачки ИРИ ППРЧ)

        }
        // ************************************************************ ReDraw_IRI_PPRH





        /*
                void service_CoordsIRI_PPRChUpdated(object sender, List<TCoordsIRI_PPRCh> listCoordsIRI_PPRCh)
                {
                    // 2504
                    double dds = 250; // m

                    int AdDl = 0;
                    int countPel = 0;
                    int countPel1 = 0;

                    lock (threadLock_5)
                    {
                        // !!! Идут только пеленги (флпаг 3)/только ИРИ(флаг 0/1)/ удалить все (флаг 2)
                        AdDl = (int)listCoordsIRI_PPRCh[0].bDelIRI;

                        PelIRI objPelIRI2 = new PelIRI();  // For IRI

                        // Delete All ************************************************************
                        // Delete Peleng+IRI

                        if (AdDl == 2)
                        {

                            // Del Peleng
                            for (int iii20 = (GlobalVarLn.list_PelIRI.Count - 1); iii20 >= 0; iii20--)
                            {
                                GlobalVarLn.list_PelIRI.Remove(GlobalVarLn.list_PelIRI[iii20]);
                            }
                            // DelIRI
                            for (int iii21 = (GlobalVarLn.list_PelIRI2.Count - 1); iii21 >= 0; iii21--)
                            {
                                GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii21]);
                            }

                            axaxcMapScreen1.Repaint();
                            return;

                        }  // AdDl=2
                        // ************************************************************ Delete All

                        // Delete ID *************************************************************
                        // Удалить конкретный ID

                        if (AdDl == 1)
                        {
                            // Del_Struct_ID ..........................................................
                            // удаляем все структуры с таким IDi 
                            // Придет в 

                            // FOR111
                            for (countPel = 0; countPel < listCoordsIRI_PPRCh.Count; countPel++)
                            {
                                int ffg1 = (int)listCoordsIRI_PPRCh[countPel].bDelIRI;

                                if (ffg1 == 1) // Del
                                {
                                    // For22
                                    for (int iii33 = (GlobalVarLn.list_PelIRI2.Count - 1); iii33 >= 0; iii33--)
                                    {
                                        if (GlobalVarLn.list_PelIRI2[iii33].ID == listCoordsIRI_PPRCh[countPel].iID)
                                            GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii33]);

                                    } //for22

                                } // IF

                            } // FOR111

                            axaxcMapScreen1.Repaint();
                            return;

                        } // AdDl==1 Del
                        // ************************************************************* Delete ID

                        // Add IRI ****************************************************************

                        if (AdDl == 0)
                        {
                            // ------------------------------------------------------------------------
                            // 2504

                            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            bool SignRepaint = false;
                            int i1 = 0;
                            int i2 = 0;
                            int i3 = 0;
                            int i4 = 0;
                            bool flexist = false;
                            int ffg = 0;
                            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            // Убрать в старом те, которых нет в новых

                            for (i1 = (GlobalVarLn.list_PelIRI2.Count - 1); i1 >= 0; i1--) // old
                            {
                                flexist = false;
                                for (i2 = 0; i2 < listCoordsIRI_PPRCh.Count; i2++) // new
                                {
                                    ffg = (int)listCoordsIRI_PPRCh[i2].bDelIRI;

                                    if (
                                        (GlobalVarLn.list_PelIRI2[i1].ID == listCoordsIRI_PPRCh[i2].iID) &&
                                        (listCoordsIRI_PPRCh[i2].dLatitude != -1) &&
                                        (listCoordsIRI_PPRCh[i2].dLongitude != -1) &&
                                        (ffg == 0)
                                       )
                                    {
                                        flexist = true;
                                    }

                                } // i2

                                if (flexist == false)
                                {
                                    GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[i1]);
                                    SignRepaint = true;
                                    //i1 = -1;
                                }

                            } // i1 old

                            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            // 

                            // FOR1_5 вперед по новому
                            for (countPel = 0; countPel < listCoordsIRI_PPRCh.Count; countPel++) // new
                            {

                                ffg = (int)listCoordsIRI_PPRCh[countPel].bDelIRI;

                                if (
                                     (listCoordsIRI_PPRCh[countPel].dLatitude != -1) &&
                                     (listCoordsIRI_PPRCh[countPel].dLongitude != -1) &&
                                     (ffg == 0)
                                   )
                                {

                                    flexist = false;
                                    for (int iii99 = (GlobalVarLn.list_PelIRI2.Count - 1); iii99 >= 0; iii99--)
                                    {

                                        if (GlobalVarLn.list_PelIRI2[iii99].ID == listCoordsIRI_PPRCh[countPel].iID)
                                        {
                                            // есть с таким ID
                                            flexist = true;
                                            double sss = 0;
                                            sss = ClassMap.f_D_2Points(GlobalVarLn.list_PelIRI2[iii99].Lat, GlobalVarLn.list_PelIRI2[iii99].Long, listCoordsIRI_PPRCh[countPel].dLatitude, listCoordsIRI_PPRCh[countPel].dLongitude, 1);
                                            if (sss > dds)
                                            {
                                                SignRepaint = true;
                                                // Убираем старый
                                                GlobalVarLn.list_PelIRI2.Remove(GlobalVarLn.list_PelIRI2[iii99]);

                                                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                                // добавляем новый

                                                objPelIRI2.ID = listCoordsIRI_PPRCh[countPel].iID;
                                                objPelIRI2.XGPS_IRI = -1;
                                                objPelIRI2.YGPS_IRI = -1;
                                                objPelIRI2.flPelMain2 = 0;
                                                objPelIRI2.arr_Pel1 = new double[10000];     // R,Широта,долгота
                                                objPelIRI2.arr_Pel2 = new double[10000];     // R,Широта,долгота
                                                objPelIRI2.Pel1 = -1;
                                                objPelIRI2.Pel2 = -1;

                                                if ((listCoordsIRI_PPRCh[countPel].dLatitude != -1) && (listCoordsIRI_PPRCh[countPel].dLongitude != -1))
                                                {
                                                    objPelIRI2.Lat = listCoordsIRI_PPRCh[countPel].dLatitude;
                                                    objPelIRI2.Long = listCoordsIRI_PPRCh[countPel].dLongitude;
                                                    objPelIRI2.color = ColorFromSharpString(listCoordsIRI_PPRCh[countPel].sColorIRI);
                                                    objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;
                                                }
                                                else
                                                {
                                                    objPelIRI2.Lat = -1;
                                                    objPelIRI2.Long = -1;
                                                    objPelIRI2.XGPS_IRI = -1;
                                                    objPelIRI2.YGPS_IRI = -1;
                                                    objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;

                                                }

                                                GlobalVarLn.list_PelIRI2.Add(objPelIRI2);

                                                ClassMap.f_GetGPS_IRI_2(GlobalVarLn.list_PelIRI2.Count - 1);

                                                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                                            } // S>ds

                                        }  // ID==

                                    }  // for old назад

                                    if (flexist == false) // это новый ID -> добавляем его
                                    {
                                        SignRepaint = true;

                                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                        // добавляем новый

                                        objPelIRI2.ID = listCoordsIRI_PPRCh[countPel].iID;
                                        objPelIRI2.XGPS_IRI = -1;
                                        objPelIRI2.YGPS_IRI = -1;
                                        objPelIRI2.flPelMain2 = 0;
                                        objPelIRI2.arr_Pel1 = new double[10000];     // R,Широта,долгота
                                        objPelIRI2.arr_Pel2 = new double[10000];     // R,Широта,долгота
                                        objPelIRI2.Pel1 = -1;
                                        objPelIRI2.Pel2 = -1;

                                        if ((listCoordsIRI_PPRCh[countPel].dLatitude != -1) && (listCoordsIRI_PPRCh[countPel].dLongitude != -1))
                                        {
                                            objPelIRI2.Lat = listCoordsIRI_PPRCh[countPel].dLatitude;
                                            objPelIRI2.Long = listCoordsIRI_PPRCh[countPel].dLongitude;
                                            objPelIRI2.color = ColorFromSharpString(listCoordsIRI_PPRCh[countPel].sColorIRI);
                                            objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;
                                        }
                                        else
                                        {
                                            objPelIRI2.Lat = -1;
                                            objPelIRI2.Long = -1;
                                            objPelIRI2.XGPS_IRI = -1;
                                            objPelIRI2.YGPS_IRI = -1;
                                            objPelIRI2.AddDel = (int)listCoordsIRI_PPRCh[countPel].bDelIRI; // 0 - add, 1 - delete;

                                        }

                                        GlobalVarLn.list_PelIRI2.Add(objPelIRI2);

                                        ClassMap.f_GetGPS_IRI_2(GlobalVarLn.list_PelIRI2.Count - 1);

                                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                                    } // // это новый ID

                                } //IF(есть координаты) 

                            } //FOR1_5 new
                            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                            if (SignRepaint == true)
                                axaxcMapScreen1.Repaint();


                        } // AdDl==0
                        // **************************************************************** Add IRI

                    } // threadLock_5

                } // INTERRUPT
                  // *********************************************************************** IRI_PPRCH
        */




        // 33333333333333333333333333333333333333333333333333333333333333 ForGetFromDB




    } // class ClassMap

} // namespace MapApplication
