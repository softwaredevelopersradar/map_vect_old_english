﻿namespace GrozaMap
{
    partial class FormSost
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSost));
            this.tcParam = new System.Windows.Forms.TabControl();
            this.tpOwnObject = new System.Windows.Forms.TabPage();
            this.tbOwnHeight = new System.Windows.Forms.TextBox();
            this.gbOwnDegMin = new System.Windows.Forms.GroupBox();
            this.tbLMin1 = new System.Windows.Forms.TextBox();
            this.tbBMin1 = new System.Windows.Forms.TextBox();
            this.lLDegMin = new System.Windows.Forms.Label();
            this.lBDegMin = new System.Windows.Forms.Label();
            this.gbOwnDegMinSec = new System.Windows.Forms.GroupBox();
            this.tbLSec = new System.Windows.Forms.TextBox();
            this.tbBSec = new System.Windows.Forms.TextBox();
            this.lMin4 = new System.Windows.Forms.Label();
            this.lMin3 = new System.Windows.Forms.Label();
            this.tbLMin2 = new System.Windows.Forms.TextBox();
            this.tbBMin2 = new System.Windows.Forms.TextBox();
            this.lSec2 = new System.Windows.Forms.Label();
            this.lSec1 = new System.Windows.Forms.Label();
            this.lDeg4 = new System.Windows.Forms.Label();
            this.lDeg3 = new System.Windows.Forms.Label();
            this.tbLDeg2 = new System.Windows.Forms.TextBox();
            this.lLDegMinSec = new System.Windows.Forms.Label();
            this.tbBDeg2 = new System.Windows.Forms.TextBox();
            this.lBDegMinSec = new System.Windows.Forms.Label();
            this.gbOwnRect42 = new System.Windows.Forms.GroupBox();
            this.tbYRect42 = new System.Windows.Forms.TextBox();
            this.lYRect42 = new System.Windows.Forms.Label();
            this.tbXRect42 = new System.Windows.Forms.TextBox();
            this.lXRect42 = new System.Windows.Forms.Label();
            this.gbOwnRad = new System.Windows.Forms.GroupBox();
            this.tbLRad = new System.Windows.Forms.TextBox();
            this.lLRad = new System.Windows.Forms.Label();
            this.tbBRad = new System.Windows.Forms.TextBox();
            this.lBRad = new System.Windows.Forms.Label();
            this.gbOwnRect = new System.Windows.Forms.GroupBox();
            this.tbYRect = new System.Windows.Forms.TextBox();
            this.lYRect = new System.Windows.Forms.Label();
            this.tbXRect = new System.Windows.Forms.TextBox();
            this.lXRect = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.buttonZSP = new System.Windows.Forms.Button();
            this.pbSP = new System.Windows.Forms.PictureBox();
            this.lOwnHeight = new System.Windows.Forms.Label();
            this.tpOpponentObject = new System.Windows.Forms.TabPage();
            this.tbPt1Height = new System.Windows.Forms.TextBox();
            this.gbPt1DegMinSec = new System.Windows.Forms.GroupBox();
            this.tbPt1LSec = new System.Windows.Forms.TextBox();
            this.tbPt1BSec = new System.Windows.Forms.TextBox();
            this.lPt1Min4 = new System.Windows.Forms.Label();
            this.lPt1Min3 = new System.Windows.Forms.Label();
            this.tbPt1LMin2 = new System.Windows.Forms.TextBox();
            this.tbPt1BMin2 = new System.Windows.Forms.TextBox();
            this.lPt1Sec2 = new System.Windows.Forms.Label();
            this.lPt1Sec1 = new System.Windows.Forms.Label();
            this.lPt1Deg4 = new System.Windows.Forms.Label();
            this.lPt1Deg3 = new System.Windows.Forms.Label();
            this.tbPt1LDeg2 = new System.Windows.Forms.TextBox();
            this.lPt1LDegMinSec = new System.Windows.Forms.Label();
            this.tbPt1BDeg2 = new System.Windows.Forms.TextBox();
            this.lPt1BDegMinSec = new System.Windows.Forms.Label();
            this.gbPt1Rad = new System.Windows.Forms.GroupBox();
            this.tbPt1LRad = new System.Windows.Forms.TextBox();
            this.lPt1LRad = new System.Windows.Forms.Label();
            this.tbPt1BRad = new System.Windows.Forms.TextBox();
            this.lPt1BRad = new System.Windows.Forms.Label();
            this.gbPt1DegMin = new System.Windows.Forms.GroupBox();
            this.tbPt1LMin1 = new System.Windows.Forms.TextBox();
            this.tbPt1BMin1 = new System.Windows.Forms.TextBox();
            this.lPt1LDegMin = new System.Windows.Forms.Label();
            this.lPt1BDegMin = new System.Windows.Forms.Label();
            this.gbPt1Rect42 = new System.Windows.Forms.GroupBox();
            this.tbPt1YRect42 = new System.Windows.Forms.TextBox();
            this.lPt1YRect42 = new System.Windows.Forms.Label();
            this.tbPt1XRect42 = new System.Windows.Forms.TextBox();
            this.lPt1XRect42 = new System.Windows.Forms.Label();
            this.gbPt1Rect = new System.Windows.Forms.GroupBox();
            this.tbPt1YRect = new System.Windows.Forms.TextBox();
            this.lPt1YRect = new System.Windows.Forms.Label();
            this.tbPt1XRect = new System.Windows.Forms.TextBox();
            this.lPt1XRect = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lPt1Height = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tbPt2Height = new System.Windows.Forms.TextBox();
            this.gbPt2DegMinSec = new System.Windows.Forms.GroupBox();
            this.tbPt2LSec = new System.Windows.Forms.TextBox();
            this.tbPt2BSec = new System.Windows.Forms.TextBox();
            this.lPt2Min4 = new System.Windows.Forms.Label();
            this.lPt2Min3 = new System.Windows.Forms.Label();
            this.tbPt2LMin2 = new System.Windows.Forms.TextBox();
            this.tbPt2BMin2 = new System.Windows.Forms.TextBox();
            this.lPt2Sec2 = new System.Windows.Forms.Label();
            this.lPt2Sec1 = new System.Windows.Forms.Label();
            this.lPt2Deg4 = new System.Windows.Forms.Label();
            this.lPt2Deg3 = new System.Windows.Forms.Label();
            this.tbPt2LDeg2 = new System.Windows.Forms.TextBox();
            this.lPt2LDegMinSec = new System.Windows.Forms.Label();
            this.tbPt2BDeg2 = new System.Windows.Forms.TextBox();
            this.lPt2BDegMinSec = new System.Windows.Forms.Label();
            this.gbPt2Rad = new System.Windows.Forms.GroupBox();
            this.tbPt2LRad = new System.Windows.Forms.TextBox();
            this.lPt2LRad = new System.Windows.Forms.Label();
            this.tbPt2BRad = new System.Windows.Forms.TextBox();
            this.lPt2BRad = new System.Windows.Forms.Label();
            this.gbPt2DegMin = new System.Windows.Forms.GroupBox();
            this.tbPt2LMin1 = new System.Windows.Forms.TextBox();
            this.tbPt2BMin1 = new System.Windows.Forms.TextBox();
            this.lPt2LDegMin = new System.Windows.Forms.Label();
            this.lPt2BDegMin = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.gbPt2Rect42 = new System.Windows.Forms.GroupBox();
            this.tbPt2YRect42 = new System.Windows.Forms.TextBox();
            this.lPt2YRect42 = new System.Windows.Forms.Label();
            this.tbPt2XRect42 = new System.Windows.Forms.TextBox();
            this.lPt2XRect42 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.gbPt2Rect = new System.Windows.Forms.GroupBox();
            this.tbPt2YRect = new System.Windows.Forms.TextBox();
            this.lPt2YRect = new System.Windows.Forms.Label();
            this.tbPt2XRect = new System.Windows.Forms.TextBox();
            this.lPt2XRect = new System.Windows.Forms.Label();
            this.lPt2Height = new System.Windows.Forms.Label();
            this.chbXY1 = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.lChooseSC = new System.Windows.Forms.Label();
            this.cbChooseSC = new System.Windows.Forms.ComboBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.imageList3 = new System.Windows.Forms.ImageList(this.components);
            this.bAccept = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.chbGPS = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.timerGPS = new System.Windows.Forms.Timer(this.components);
            this.button9 = new System.Windows.Forms.Button();
            this.tcParam.SuspendLayout();
            this.tpOwnObject.SuspendLayout();
            this.gbOwnDegMin.SuspendLayout();
            this.gbOwnDegMinSec.SuspendLayout();
            this.gbOwnRect42.SuspendLayout();
            this.gbOwnRad.SuspendLayout();
            this.gbOwnRect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSP)).BeginInit();
            this.tpOpponentObject.SuspendLayout();
            this.gbPt1DegMinSec.SuspendLayout();
            this.gbPt1Rad.SuspendLayout();
            this.gbPt1DegMin.SuspendLayout();
            this.gbPt1Rect42.SuspendLayout();
            this.gbPt1Rect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.gbPt2DegMinSec.SuspendLayout();
            this.gbPt2Rad.SuspendLayout();
            this.gbPt2DegMin.SuspendLayout();
            this.gbPt2Rect42.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.gbPt2Rect.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcParam
            // 
            resources.ApplyResources(this.tcParam, "tcParam");
            this.tcParam.Controls.Add(this.tpOwnObject);
            this.tcParam.Controls.Add(this.tpOpponentObject);
            this.tcParam.Controls.Add(this.tabPage1);
            this.tcParam.Name = "tcParam";
            this.tcParam.SelectedIndex = 0;
            this.tcParam.SelectedIndexChanged += new System.EventHandler(this.tcParam_SelectedIndexChanged);
            // 
            // tpOwnObject
            // 
            resources.ApplyResources(this.tpOwnObject, "tpOwnObject");
            this.tpOwnObject.BackColor = System.Drawing.SystemColors.Control;
            this.tpOwnObject.Controls.Add(this.tbOwnHeight);
            this.tpOwnObject.Controls.Add(this.gbOwnDegMin);
            this.tpOwnObject.Controls.Add(this.gbOwnDegMinSec);
            this.tpOwnObject.Controls.Add(this.gbOwnRect42);
            this.tpOwnObject.Controls.Add(this.gbOwnRad);
            this.tpOwnObject.Controls.Add(this.gbOwnRect);
            this.tpOwnObject.Controls.Add(this.button5);
            this.tpOwnObject.Controls.Add(this.buttonZSP);
            this.tpOwnObject.Controls.Add(this.pbSP);
            this.tpOwnObject.Controls.Add(this.lOwnHeight);
            this.tpOwnObject.Name = "tpOwnObject";
            // 
            // tbOwnHeight
            // 
            resources.ApplyResources(this.tbOwnHeight, "tbOwnHeight");
            this.tbOwnHeight.BackColor = System.Drawing.SystemColors.Menu;
            this.tbOwnHeight.Name = "tbOwnHeight";
            // 
            // gbOwnDegMin
            // 
            resources.ApplyResources(this.gbOwnDegMin, "gbOwnDegMin");
            this.gbOwnDegMin.Controls.Add(this.tbLMin1);
            this.gbOwnDegMin.Controls.Add(this.tbBMin1);
            this.gbOwnDegMin.Controls.Add(this.lLDegMin);
            this.gbOwnDegMin.Controls.Add(this.lBDegMin);
            this.gbOwnDegMin.Name = "gbOwnDegMin";
            this.gbOwnDegMin.TabStop = false;
            // 
            // tbLMin1
            // 
            resources.ApplyResources(this.tbLMin1, "tbLMin1");
            this.tbLMin1.Name = "tbLMin1";
            // 
            // tbBMin1
            // 
            resources.ApplyResources(this.tbBMin1, "tbBMin1");
            this.tbBMin1.Name = "tbBMin1";
            // 
            // lLDegMin
            // 
            resources.ApplyResources(this.lLDegMin, "lLDegMin");
            this.lLDegMin.Name = "lLDegMin";
            // 
            // lBDegMin
            // 
            resources.ApplyResources(this.lBDegMin, "lBDegMin");
            this.lBDegMin.Name = "lBDegMin";
            // 
            // gbOwnDegMinSec
            // 
            resources.ApplyResources(this.gbOwnDegMinSec, "gbOwnDegMinSec");
            this.gbOwnDegMinSec.Controls.Add(this.tbLSec);
            this.gbOwnDegMinSec.Controls.Add(this.tbBSec);
            this.gbOwnDegMinSec.Controls.Add(this.lMin4);
            this.gbOwnDegMinSec.Controls.Add(this.lMin3);
            this.gbOwnDegMinSec.Controls.Add(this.tbLMin2);
            this.gbOwnDegMinSec.Controls.Add(this.tbBMin2);
            this.gbOwnDegMinSec.Controls.Add(this.lSec2);
            this.gbOwnDegMinSec.Controls.Add(this.lSec1);
            this.gbOwnDegMinSec.Controls.Add(this.lDeg4);
            this.gbOwnDegMinSec.Controls.Add(this.lDeg3);
            this.gbOwnDegMinSec.Controls.Add(this.tbLDeg2);
            this.gbOwnDegMinSec.Controls.Add(this.lLDegMinSec);
            this.gbOwnDegMinSec.Controls.Add(this.tbBDeg2);
            this.gbOwnDegMinSec.Controls.Add(this.lBDegMinSec);
            this.gbOwnDegMinSec.Name = "gbOwnDegMinSec";
            this.gbOwnDegMinSec.TabStop = false;
            // 
            // tbLSec
            // 
            resources.ApplyResources(this.tbLSec, "tbLSec");
            this.tbLSec.Name = "tbLSec";
            // 
            // tbBSec
            // 
            resources.ApplyResources(this.tbBSec, "tbBSec");
            this.tbBSec.Name = "tbBSec";
            // 
            // lMin4
            // 
            resources.ApplyResources(this.lMin4, "lMin4");
            this.lMin4.Name = "lMin4";
            // 
            // lMin3
            // 
            resources.ApplyResources(this.lMin3, "lMin3");
            this.lMin3.Name = "lMin3";
            // 
            // tbLMin2
            // 
            resources.ApplyResources(this.tbLMin2, "tbLMin2");
            this.tbLMin2.Name = "tbLMin2";
            // 
            // tbBMin2
            // 
            resources.ApplyResources(this.tbBMin2, "tbBMin2");
            this.tbBMin2.Name = "tbBMin2";
            // 
            // lSec2
            // 
            resources.ApplyResources(this.lSec2, "lSec2");
            this.lSec2.Name = "lSec2";
            // 
            // lSec1
            // 
            resources.ApplyResources(this.lSec1, "lSec1");
            this.lSec1.Name = "lSec1";
            // 
            // lDeg4
            // 
            resources.ApplyResources(this.lDeg4, "lDeg4");
            this.lDeg4.Name = "lDeg4";
            // 
            // lDeg3
            // 
            resources.ApplyResources(this.lDeg3, "lDeg3");
            this.lDeg3.Name = "lDeg3";
            // 
            // tbLDeg2
            // 
            resources.ApplyResources(this.tbLDeg2, "tbLDeg2");
            this.tbLDeg2.Name = "tbLDeg2";
            // 
            // lLDegMinSec
            // 
            resources.ApplyResources(this.lLDegMinSec, "lLDegMinSec");
            this.lLDegMinSec.Name = "lLDegMinSec";
            // 
            // tbBDeg2
            // 
            resources.ApplyResources(this.tbBDeg2, "tbBDeg2");
            this.tbBDeg2.Name = "tbBDeg2";
            // 
            // lBDegMinSec
            // 
            resources.ApplyResources(this.lBDegMinSec, "lBDegMinSec");
            this.lBDegMinSec.Name = "lBDegMinSec";
            // 
            // gbOwnRect42
            // 
            resources.ApplyResources(this.gbOwnRect42, "gbOwnRect42");
            this.gbOwnRect42.Controls.Add(this.tbYRect42);
            this.gbOwnRect42.Controls.Add(this.lYRect42);
            this.gbOwnRect42.Controls.Add(this.tbXRect42);
            this.gbOwnRect42.Controls.Add(this.lXRect42);
            this.gbOwnRect42.Name = "gbOwnRect42";
            this.gbOwnRect42.TabStop = false;
            // 
            // tbYRect42
            // 
            resources.ApplyResources(this.tbYRect42, "tbYRect42");
            this.tbYRect42.Name = "tbYRect42";
            // 
            // lYRect42
            // 
            resources.ApplyResources(this.lYRect42, "lYRect42");
            this.lYRect42.Name = "lYRect42";
            // 
            // tbXRect42
            // 
            resources.ApplyResources(this.tbXRect42, "tbXRect42");
            this.tbXRect42.Name = "tbXRect42";
            // 
            // lXRect42
            // 
            resources.ApplyResources(this.lXRect42, "lXRect42");
            this.lXRect42.Name = "lXRect42";
            // 
            // gbOwnRad
            // 
            resources.ApplyResources(this.gbOwnRad, "gbOwnRad");
            this.gbOwnRad.Controls.Add(this.tbLRad);
            this.gbOwnRad.Controls.Add(this.lLRad);
            this.gbOwnRad.Controls.Add(this.tbBRad);
            this.gbOwnRad.Controls.Add(this.lBRad);
            this.gbOwnRad.Name = "gbOwnRad";
            this.gbOwnRad.TabStop = false;
            // 
            // tbLRad
            // 
            resources.ApplyResources(this.tbLRad, "tbLRad");
            this.tbLRad.Name = "tbLRad";
            // 
            // lLRad
            // 
            resources.ApplyResources(this.lLRad, "lLRad");
            this.lLRad.Name = "lLRad";
            // 
            // tbBRad
            // 
            resources.ApplyResources(this.tbBRad, "tbBRad");
            this.tbBRad.Name = "tbBRad";
            // 
            // lBRad
            // 
            resources.ApplyResources(this.lBRad, "lBRad");
            this.lBRad.Name = "lBRad";
            // 
            // gbOwnRect
            // 
            resources.ApplyResources(this.gbOwnRect, "gbOwnRect");
            this.gbOwnRect.Controls.Add(this.tbYRect);
            this.gbOwnRect.Controls.Add(this.lYRect);
            this.gbOwnRect.Controls.Add(this.tbXRect);
            this.gbOwnRect.Controls.Add(this.lXRect);
            this.gbOwnRect.Name = "gbOwnRect";
            this.gbOwnRect.TabStop = false;
            // 
            // tbYRect
            // 
            resources.ApplyResources(this.tbYRect, "tbYRect");
            this.tbYRect.Name = "tbYRect";
            // 
            // lYRect
            // 
            resources.ApplyResources(this.lYRect, "lYRect");
            this.lYRect.Name = "lYRect";
            // 
            // tbXRect
            // 
            resources.ApplyResources(this.tbXRect, "tbXRect");
            this.tbXRect.Name = "tbXRect";
            // 
            // lXRect
            // 
            resources.ApplyResources(this.lXRect, "lXRect");
            this.lXRect.Name = "lXRect";
            // 
            // button5
            // 
            resources.ApplyResources(this.button5, "button5");
            this.button5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button5.Name = "button5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // buttonZSP
            // 
            resources.ApplyResources(this.buttonZSP, "buttonZSP");
            this.buttonZSP.Name = "buttonZSP";
            this.buttonZSP.UseVisualStyleBackColor = true;
            this.buttonZSP.Click += new System.EventHandler(this.buttonZSP_Click);
            // 
            // pbSP
            // 
            resources.ApplyResources(this.pbSP, "pbSP");
            this.pbSP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbSP.Name = "pbSP";
            this.pbSP.TabStop = false;
            // 
            // lOwnHeight
            // 
            resources.ApplyResources(this.lOwnHeight, "lOwnHeight");
            this.lOwnHeight.Name = "lOwnHeight";
            // 
            // tpOpponentObject
            // 
            resources.ApplyResources(this.tpOpponentObject, "tpOpponentObject");
            this.tpOpponentObject.BackColor = System.Drawing.SystemColors.Control;
            this.tpOpponentObject.Controls.Add(this.tbPt1Height);
            this.tpOpponentObject.Controls.Add(this.gbPt1DegMinSec);
            this.tpOpponentObject.Controls.Add(this.gbPt1Rad);
            this.tpOpponentObject.Controls.Add(this.gbPt1DegMin);
            this.tpOpponentObject.Controls.Add(this.gbPt1Rect42);
            this.tpOpponentObject.Controls.Add(this.gbPt1Rect);
            this.tpOpponentObject.Controls.Add(this.button6);
            this.tpOpponentObject.Controls.Add(this.button1);
            this.tpOpponentObject.Controls.Add(this.pictureBox1);
            this.tpOpponentObject.Controls.Add(this.lPt1Height);
            this.tpOpponentObject.Name = "tpOpponentObject";
            // 
            // tbPt1Height
            // 
            resources.ApplyResources(this.tbPt1Height, "tbPt1Height");
            this.tbPt1Height.BackColor = System.Drawing.SystemColors.Menu;
            this.tbPt1Height.Name = "tbPt1Height";
            // 
            // gbPt1DegMinSec
            // 
            resources.ApplyResources(this.gbPt1DegMinSec, "gbPt1DegMinSec");
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1LSec);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1BSec);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Min4);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Min3);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1LMin2);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1BMin2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Sec2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Sec1);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Deg4);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Deg3);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1LDeg2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1LDegMinSec);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1BDeg2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1BDegMinSec);
            this.gbPt1DegMinSec.Name = "gbPt1DegMinSec";
            this.gbPt1DegMinSec.TabStop = false;
            // 
            // tbPt1LSec
            // 
            resources.ApplyResources(this.tbPt1LSec, "tbPt1LSec");
            this.tbPt1LSec.Name = "tbPt1LSec";
            // 
            // tbPt1BSec
            // 
            resources.ApplyResources(this.tbPt1BSec, "tbPt1BSec");
            this.tbPt1BSec.Name = "tbPt1BSec";
            // 
            // lPt1Min4
            // 
            resources.ApplyResources(this.lPt1Min4, "lPt1Min4");
            this.lPt1Min4.Name = "lPt1Min4";
            // 
            // lPt1Min3
            // 
            resources.ApplyResources(this.lPt1Min3, "lPt1Min3");
            this.lPt1Min3.Name = "lPt1Min3";
            // 
            // tbPt1LMin2
            // 
            resources.ApplyResources(this.tbPt1LMin2, "tbPt1LMin2");
            this.tbPt1LMin2.Name = "tbPt1LMin2";
            // 
            // tbPt1BMin2
            // 
            resources.ApplyResources(this.tbPt1BMin2, "tbPt1BMin2");
            this.tbPt1BMin2.Name = "tbPt1BMin2";
            // 
            // lPt1Sec2
            // 
            resources.ApplyResources(this.lPt1Sec2, "lPt1Sec2");
            this.lPt1Sec2.Name = "lPt1Sec2";
            // 
            // lPt1Sec1
            // 
            resources.ApplyResources(this.lPt1Sec1, "lPt1Sec1");
            this.lPt1Sec1.Name = "lPt1Sec1";
            // 
            // lPt1Deg4
            // 
            resources.ApplyResources(this.lPt1Deg4, "lPt1Deg4");
            this.lPt1Deg4.Name = "lPt1Deg4";
            // 
            // lPt1Deg3
            // 
            resources.ApplyResources(this.lPt1Deg3, "lPt1Deg3");
            this.lPt1Deg3.Name = "lPt1Deg3";
            // 
            // tbPt1LDeg2
            // 
            resources.ApplyResources(this.tbPt1LDeg2, "tbPt1LDeg2");
            this.tbPt1LDeg2.Name = "tbPt1LDeg2";
            // 
            // lPt1LDegMinSec
            // 
            resources.ApplyResources(this.lPt1LDegMinSec, "lPt1LDegMinSec");
            this.lPt1LDegMinSec.Name = "lPt1LDegMinSec";
            // 
            // tbPt1BDeg2
            // 
            resources.ApplyResources(this.tbPt1BDeg2, "tbPt1BDeg2");
            this.tbPt1BDeg2.Name = "tbPt1BDeg2";
            // 
            // lPt1BDegMinSec
            // 
            resources.ApplyResources(this.lPt1BDegMinSec, "lPt1BDegMinSec");
            this.lPt1BDegMinSec.Name = "lPt1BDegMinSec";
            // 
            // gbPt1Rad
            // 
            resources.ApplyResources(this.gbPt1Rad, "gbPt1Rad");
            this.gbPt1Rad.Controls.Add(this.tbPt1LRad);
            this.gbPt1Rad.Controls.Add(this.lPt1LRad);
            this.gbPt1Rad.Controls.Add(this.tbPt1BRad);
            this.gbPt1Rad.Controls.Add(this.lPt1BRad);
            this.gbPt1Rad.Name = "gbPt1Rad";
            this.gbPt1Rad.TabStop = false;
            // 
            // tbPt1LRad
            // 
            resources.ApplyResources(this.tbPt1LRad, "tbPt1LRad");
            this.tbPt1LRad.Name = "tbPt1LRad";
            // 
            // lPt1LRad
            // 
            resources.ApplyResources(this.lPt1LRad, "lPt1LRad");
            this.lPt1LRad.Name = "lPt1LRad";
            // 
            // tbPt1BRad
            // 
            resources.ApplyResources(this.tbPt1BRad, "tbPt1BRad");
            this.tbPt1BRad.Name = "tbPt1BRad";
            // 
            // lPt1BRad
            // 
            resources.ApplyResources(this.lPt1BRad, "lPt1BRad");
            this.lPt1BRad.Name = "lPt1BRad";
            // 
            // gbPt1DegMin
            // 
            resources.ApplyResources(this.gbPt1DegMin, "gbPt1DegMin");
            this.gbPt1DegMin.Controls.Add(this.tbPt1LMin1);
            this.gbPt1DegMin.Controls.Add(this.tbPt1BMin1);
            this.gbPt1DegMin.Controls.Add(this.lPt1LDegMin);
            this.gbPt1DegMin.Controls.Add(this.lPt1BDegMin);
            this.gbPt1DegMin.Name = "gbPt1DegMin";
            this.gbPt1DegMin.TabStop = false;
            // 
            // tbPt1LMin1
            // 
            resources.ApplyResources(this.tbPt1LMin1, "tbPt1LMin1");
            this.tbPt1LMin1.Name = "tbPt1LMin1";
            // 
            // tbPt1BMin1
            // 
            resources.ApplyResources(this.tbPt1BMin1, "tbPt1BMin1");
            this.tbPt1BMin1.Name = "tbPt1BMin1";
            // 
            // lPt1LDegMin
            // 
            resources.ApplyResources(this.lPt1LDegMin, "lPt1LDegMin");
            this.lPt1LDegMin.Name = "lPt1LDegMin";
            // 
            // lPt1BDegMin
            // 
            resources.ApplyResources(this.lPt1BDegMin, "lPt1BDegMin");
            this.lPt1BDegMin.Name = "lPt1BDegMin";
            // 
            // gbPt1Rect42
            // 
            resources.ApplyResources(this.gbPt1Rect42, "gbPt1Rect42");
            this.gbPt1Rect42.Controls.Add(this.tbPt1YRect42);
            this.gbPt1Rect42.Controls.Add(this.lPt1YRect42);
            this.gbPt1Rect42.Controls.Add(this.tbPt1XRect42);
            this.gbPt1Rect42.Controls.Add(this.lPt1XRect42);
            this.gbPt1Rect42.Name = "gbPt1Rect42";
            this.gbPt1Rect42.TabStop = false;
            // 
            // tbPt1YRect42
            // 
            resources.ApplyResources(this.tbPt1YRect42, "tbPt1YRect42");
            this.tbPt1YRect42.Name = "tbPt1YRect42";
            // 
            // lPt1YRect42
            // 
            resources.ApplyResources(this.lPt1YRect42, "lPt1YRect42");
            this.lPt1YRect42.Name = "lPt1YRect42";
            // 
            // tbPt1XRect42
            // 
            resources.ApplyResources(this.tbPt1XRect42, "tbPt1XRect42");
            this.tbPt1XRect42.Name = "tbPt1XRect42";
            // 
            // lPt1XRect42
            // 
            resources.ApplyResources(this.lPt1XRect42, "lPt1XRect42");
            this.lPt1XRect42.Name = "lPt1XRect42";
            // 
            // gbPt1Rect
            // 
            resources.ApplyResources(this.gbPt1Rect, "gbPt1Rect");
            this.gbPt1Rect.Controls.Add(this.tbPt1YRect);
            this.gbPt1Rect.Controls.Add(this.lPt1YRect);
            this.gbPt1Rect.Controls.Add(this.tbPt1XRect);
            this.gbPt1Rect.Controls.Add(this.lPt1XRect);
            this.gbPt1Rect.Name = "gbPt1Rect";
            this.gbPt1Rect.TabStop = false;
            // 
            // tbPt1YRect
            // 
            resources.ApplyResources(this.tbPt1YRect, "tbPt1YRect");
            this.tbPt1YRect.Name = "tbPt1YRect";
            // 
            // lPt1YRect
            // 
            resources.ApplyResources(this.lPt1YRect, "lPt1YRect");
            this.lPt1YRect.Name = "lPt1YRect";
            // 
            // tbPt1XRect
            // 
            resources.ApplyResources(this.tbPt1XRect, "tbPt1XRect");
            this.tbPt1XRect.Name = "tbPt1XRect";
            // 
            // lPt1XRect
            // 
            resources.ApplyResources(this.lPt1XRect, "lPt1XRect");
            this.lPt1XRect.Name = "lPt1XRect";
            // 
            // button6
            // 
            resources.ApplyResources(this.button6, "button6");
            this.button6.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button6.Name = "button6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // lPt1Height
            // 
            resources.ApplyResources(this.lPt1Height, "lPt1Height");
            this.lPt1Height.Name = "lPt1Height";
            this.lPt1Height.Click += new System.EventHandler(this.lPt1Height_Click);
            // 
            // tabPage1
            // 
            resources.ApplyResources(this.tabPage1, "tabPage1");
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.tbPt2Height);
            this.tabPage1.Controls.Add(this.gbPt2DegMinSec);
            this.tabPage1.Controls.Add(this.gbPt2Rad);
            this.tabPage1.Controls.Add(this.gbPt2DegMin);
            this.tabPage1.Controls.Add(this.button7);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.gbPt2Rect42);
            this.tabPage1.Controls.Add(this.pictureBox2);
            this.tabPage1.Controls.Add(this.gbPt2Rect);
            this.tabPage1.Controls.Add(this.lPt2Height);
            this.tabPage1.Name = "tabPage1";
            // 
            // tbPt2Height
            // 
            resources.ApplyResources(this.tbPt2Height, "tbPt2Height");
            this.tbPt2Height.BackColor = System.Drawing.SystemColors.Menu;
            this.tbPt2Height.Name = "tbPt2Height";
            // 
            // gbPt2DegMinSec
            // 
            resources.ApplyResources(this.gbPt2DegMinSec, "gbPt2DegMinSec");
            this.gbPt2DegMinSec.Controls.Add(this.tbPt2LSec);
            this.gbPt2DegMinSec.Controls.Add(this.tbPt2BSec);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2Min4);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2Min3);
            this.gbPt2DegMinSec.Controls.Add(this.tbPt2LMin2);
            this.gbPt2DegMinSec.Controls.Add(this.tbPt2BMin2);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2Sec2);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2Sec1);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2Deg4);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2Deg3);
            this.gbPt2DegMinSec.Controls.Add(this.tbPt2LDeg2);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2LDegMinSec);
            this.gbPt2DegMinSec.Controls.Add(this.tbPt2BDeg2);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2BDegMinSec);
            this.gbPt2DegMinSec.Name = "gbPt2DegMinSec";
            this.gbPt2DegMinSec.TabStop = false;
            // 
            // tbPt2LSec
            // 
            resources.ApplyResources(this.tbPt2LSec, "tbPt2LSec");
            this.tbPt2LSec.Name = "tbPt2LSec";
            // 
            // tbPt2BSec
            // 
            resources.ApplyResources(this.tbPt2BSec, "tbPt2BSec");
            this.tbPt2BSec.Name = "tbPt2BSec";
            // 
            // lPt2Min4
            // 
            resources.ApplyResources(this.lPt2Min4, "lPt2Min4");
            this.lPt2Min4.Name = "lPt2Min4";
            // 
            // lPt2Min3
            // 
            resources.ApplyResources(this.lPt2Min3, "lPt2Min3");
            this.lPt2Min3.Name = "lPt2Min3";
            // 
            // tbPt2LMin2
            // 
            resources.ApplyResources(this.tbPt2LMin2, "tbPt2LMin2");
            this.tbPt2LMin2.Name = "tbPt2LMin2";
            // 
            // tbPt2BMin2
            // 
            resources.ApplyResources(this.tbPt2BMin2, "tbPt2BMin2");
            this.tbPt2BMin2.Name = "tbPt2BMin2";
            // 
            // lPt2Sec2
            // 
            resources.ApplyResources(this.lPt2Sec2, "lPt2Sec2");
            this.lPt2Sec2.Name = "lPt2Sec2";
            // 
            // lPt2Sec1
            // 
            resources.ApplyResources(this.lPt2Sec1, "lPt2Sec1");
            this.lPt2Sec1.Name = "lPt2Sec1";
            // 
            // lPt2Deg4
            // 
            resources.ApplyResources(this.lPt2Deg4, "lPt2Deg4");
            this.lPt2Deg4.Name = "lPt2Deg4";
            // 
            // lPt2Deg3
            // 
            resources.ApplyResources(this.lPt2Deg3, "lPt2Deg3");
            this.lPt2Deg3.Name = "lPt2Deg3";
            // 
            // tbPt2LDeg2
            // 
            resources.ApplyResources(this.tbPt2LDeg2, "tbPt2LDeg2");
            this.tbPt2LDeg2.Name = "tbPt2LDeg2";
            // 
            // lPt2LDegMinSec
            // 
            resources.ApplyResources(this.lPt2LDegMinSec, "lPt2LDegMinSec");
            this.lPt2LDegMinSec.Name = "lPt2LDegMinSec";
            // 
            // tbPt2BDeg2
            // 
            resources.ApplyResources(this.tbPt2BDeg2, "tbPt2BDeg2");
            this.tbPt2BDeg2.Name = "tbPt2BDeg2";
            // 
            // lPt2BDegMinSec
            // 
            resources.ApplyResources(this.lPt2BDegMinSec, "lPt2BDegMinSec");
            this.lPt2BDegMinSec.Name = "lPt2BDegMinSec";
            // 
            // gbPt2Rad
            // 
            resources.ApplyResources(this.gbPt2Rad, "gbPt2Rad");
            this.gbPt2Rad.Controls.Add(this.tbPt2LRad);
            this.gbPt2Rad.Controls.Add(this.lPt2LRad);
            this.gbPt2Rad.Controls.Add(this.tbPt2BRad);
            this.gbPt2Rad.Controls.Add(this.lPt2BRad);
            this.gbPt2Rad.Name = "gbPt2Rad";
            this.gbPt2Rad.TabStop = false;
            // 
            // tbPt2LRad
            // 
            resources.ApplyResources(this.tbPt2LRad, "tbPt2LRad");
            this.tbPt2LRad.Name = "tbPt2LRad";
            // 
            // lPt2LRad
            // 
            resources.ApplyResources(this.lPt2LRad, "lPt2LRad");
            this.lPt2LRad.Name = "lPt2LRad";
            // 
            // tbPt2BRad
            // 
            resources.ApplyResources(this.tbPt2BRad, "tbPt2BRad");
            this.tbPt2BRad.Name = "tbPt2BRad";
            // 
            // lPt2BRad
            // 
            resources.ApplyResources(this.lPt2BRad, "lPt2BRad");
            this.lPt2BRad.Name = "lPt2BRad";
            // 
            // gbPt2DegMin
            // 
            resources.ApplyResources(this.gbPt2DegMin, "gbPt2DegMin");
            this.gbPt2DegMin.Controls.Add(this.tbPt2LMin1);
            this.gbPt2DegMin.Controls.Add(this.tbPt2BMin1);
            this.gbPt2DegMin.Controls.Add(this.lPt2LDegMin);
            this.gbPt2DegMin.Controls.Add(this.lPt2BDegMin);
            this.gbPt2DegMin.Name = "gbPt2DegMin";
            this.gbPt2DegMin.TabStop = false;
            // 
            // tbPt2LMin1
            // 
            resources.ApplyResources(this.tbPt2LMin1, "tbPt2LMin1");
            this.tbPt2LMin1.Name = "tbPt2LMin1";
            // 
            // tbPt2BMin1
            // 
            resources.ApplyResources(this.tbPt2BMin1, "tbPt2BMin1");
            this.tbPt2BMin1.Name = "tbPt2BMin1";
            // 
            // lPt2LDegMin
            // 
            resources.ApplyResources(this.lPt2LDegMin, "lPt2LDegMin");
            this.lPt2LDegMin.Name = "lPt2LDegMin";
            // 
            // lPt2BDegMin
            // 
            resources.ApplyResources(this.lPt2BDegMin, "lPt2BDegMin");
            this.lPt2BDegMin.Name = "lPt2BDegMin";
            // 
            // button7
            // 
            resources.ApplyResources(this.button7, "button7");
            this.button7.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button7.Name = "button7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button2
            // 
            resources.ApplyResources(this.button2, "button2");
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // gbPt2Rect42
            // 
            resources.ApplyResources(this.gbPt2Rect42, "gbPt2Rect42");
            this.gbPt2Rect42.Controls.Add(this.tbPt2YRect42);
            this.gbPt2Rect42.Controls.Add(this.lPt2YRect42);
            this.gbPt2Rect42.Controls.Add(this.tbPt2XRect42);
            this.gbPt2Rect42.Controls.Add(this.lPt2XRect42);
            this.gbPt2Rect42.Name = "gbPt2Rect42";
            this.gbPt2Rect42.TabStop = false;
            // 
            // tbPt2YRect42
            // 
            resources.ApplyResources(this.tbPt2YRect42, "tbPt2YRect42");
            this.tbPt2YRect42.Name = "tbPt2YRect42";
            // 
            // lPt2YRect42
            // 
            resources.ApplyResources(this.lPt2YRect42, "lPt2YRect42");
            this.lPt2YRect42.Name = "lPt2YRect42";
            // 
            // tbPt2XRect42
            // 
            resources.ApplyResources(this.tbPt2XRect42, "tbPt2XRect42");
            this.tbPt2XRect42.Name = "tbPt2XRect42";
            // 
            // lPt2XRect42
            // 
            resources.ApplyResources(this.lPt2XRect42, "lPt2XRect42");
            this.lPt2XRect42.Name = "lPt2XRect42";
            // 
            // pictureBox2
            // 
            resources.ApplyResources(this.pictureBox2, "pictureBox2");
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.TabStop = false;
            // 
            // gbPt2Rect
            // 
            resources.ApplyResources(this.gbPt2Rect, "gbPt2Rect");
            this.gbPt2Rect.Controls.Add(this.tbPt2YRect);
            this.gbPt2Rect.Controls.Add(this.lPt2YRect);
            this.gbPt2Rect.Controls.Add(this.tbPt2XRect);
            this.gbPt2Rect.Controls.Add(this.lPt2XRect);
            this.gbPt2Rect.Name = "gbPt2Rect";
            this.gbPt2Rect.TabStop = false;
            // 
            // tbPt2YRect
            // 
            resources.ApplyResources(this.tbPt2YRect, "tbPt2YRect");
            this.tbPt2YRect.Name = "tbPt2YRect";
            // 
            // lPt2YRect
            // 
            resources.ApplyResources(this.lPt2YRect, "lPt2YRect");
            this.lPt2YRect.Name = "lPt2YRect";
            // 
            // tbPt2XRect
            // 
            resources.ApplyResources(this.tbPt2XRect, "tbPt2XRect");
            this.tbPt2XRect.Name = "tbPt2XRect";
            // 
            // lPt2XRect
            // 
            resources.ApplyResources(this.lPt2XRect, "lPt2XRect");
            this.lPt2XRect.Name = "lPt2XRect";
            // 
            // lPt2Height
            // 
            resources.ApplyResources(this.lPt2Height, "lPt2Height");
            this.lPt2Height.Name = "lPt2Height";
            // 
            // chbXY1
            // 
            resources.ApplyResources(this.chbXY1, "chbXY1");
            this.chbXY1.Name = "chbXY1";
            this.chbXY1.UseVisualStyleBackColor = true;
            this.chbXY1.CheckedChanged += new System.EventHandler(this.chbXY1_CheckedChanged);
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            // 
            // lChooseSC
            // 
            resources.ApplyResources(this.lChooseSC, "lChooseSC");
            this.lChooseSC.Name = "lChooseSC";
            this.lChooseSC.Click += new System.EventHandler(this.lChooseSC_Click);
            // 
            // cbChooseSC
            // 
            resources.ApplyResources(this.cbChooseSC, "cbChooseSC");
            this.cbChooseSC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChooseSC.DropDownWidth = 150;
            this.cbChooseSC.FormattingEnabled = true;
            this.cbChooseSC.Items.AddRange(new object[] {
            resources.GetString("cbChooseSC.Items"),
            resources.GetString("cbChooseSC.Items1"),
            resources.GetString("cbChooseSC.Items2"),
            resources.GetString("cbChooseSC.Items3"),
            resources.GetString("cbChooseSC.Items4")});
            this.cbChooseSC.Name = "cbChooseSC";
            this.cbChooseSC.SelectedIndexChanged += new System.EventHandler(this.cbChooseSC_SelectedIndexChanged);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            resources.ApplyResources(this.imageList1, "imageList1");
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // imageList2
            // 
            this.imageList2.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            resources.ApplyResources(this.imageList2, "imageList2");
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // imageList3
            // 
            this.imageList3.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            resources.ApplyResources(this.imageList3, "imageList3");
            this.imageList3.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // bAccept
            // 
            resources.ApplyResources(this.bAccept, "bAccept");
            this.bAccept.Name = "bAccept";
            this.bAccept.UseVisualStyleBackColor = true;
            this.bAccept.Click += new System.EventHandler(this.bAccept_Click);
            // 
            // button3
            // 
            resources.ApplyResources(this.button3, "button3");
            this.button3.Name = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            resources.ApplyResources(this.button4, "button4");
            this.button4.Name = "button4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button8
            // 
            resources.ApplyResources(this.button8, "button8");
            this.button8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button8.Name = "button8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // chbGPS
            // 
            resources.ApplyResources(this.chbGPS, "chbGPS");
            this.chbGPS.Name = "chbGPS";
            this.chbGPS.UseVisualStyleBackColor = true;
            this.chbGPS.CheckedChanged += new System.EventHandler(this.chbGPS_CheckedChanged);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Name = "label1";
            // 
            // timerGPS
            // 
            this.timerGPS.Interval = 1000;
            this.timerGPS.Tick += new System.EventHandler(this.timerGPS_Tick);
            // 
            // button9
            // 
            resources.ApplyResources(this.button9, "button9");
            this.button9.Name = "button9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // FormSost
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button9);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chbGPS);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.bAccept);
            this.Controls.Add(this.lChooseSC);
            this.Controls.Add(this.cbChooseSC);
            this.Controls.Add(this.tcParam);
            this.Controls.Add(this.chbXY1);
            this.Controls.Add(this.label16);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSost";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.FormSost_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSost_FormClosing);
            this.Load += new System.EventHandler(this.FormSost_Load);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FormSost_MouseUp);
            this.Move += new System.EventHandler(this.FormSost_Move);
            this.tcParam.ResumeLayout(false);
            this.tpOwnObject.ResumeLayout(false);
            this.tpOwnObject.PerformLayout();
            this.gbOwnDegMin.ResumeLayout(false);
            this.gbOwnDegMin.PerformLayout();
            this.gbOwnDegMinSec.ResumeLayout(false);
            this.gbOwnDegMinSec.PerformLayout();
            this.gbOwnRect42.ResumeLayout(false);
            this.gbOwnRect42.PerformLayout();
            this.gbOwnRad.ResumeLayout(false);
            this.gbOwnRad.PerformLayout();
            this.gbOwnRect.ResumeLayout(false);
            this.gbOwnRect.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSP)).EndInit();
            this.tpOpponentObject.ResumeLayout(false);
            this.tpOpponentObject.PerformLayout();
            this.gbPt1DegMinSec.ResumeLayout(false);
            this.gbPt1DegMinSec.PerformLayout();
            this.gbPt1Rad.ResumeLayout(false);
            this.gbPt1Rad.PerformLayout();
            this.gbPt1DegMin.ResumeLayout(false);
            this.gbPt1DegMin.PerformLayout();
            this.gbPt1Rect42.ResumeLayout(false);
            this.gbPt1Rect42.PerformLayout();
            this.gbPt1Rect.ResumeLayout(false);
            this.gbPt1Rect.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.gbPt2DegMinSec.ResumeLayout(false);
            this.gbPt2DegMinSec.PerformLayout();
            this.gbPt2Rad.ResumeLayout(false);
            this.gbPt2Rad.PerformLayout();
            this.gbPt2DegMin.ResumeLayout(false);
            this.gbPt2DegMin.PerformLayout();
            this.gbPt2Rect42.ResumeLayout(false);
            this.gbPt2Rect42.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.gbPt2Rect.ResumeLayout(false);
            this.gbPt2Rect.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TabControl tcParam;
        private System.Windows.Forms.TabPage tpOwnObject;
        private System.Windows.Forms.Label lChooseSC;
        public System.Windows.Forms.TextBox tbYRect;
        private System.Windows.Forms.Label lYRect;
        public System.Windows.Forms.TextBox tbXRect;
        private System.Windows.Forms.Label lXRect;
        public System.Windows.Forms.TextBox tbLMin1;
        public System.Windows.Forms.TextBox tbBMin1;
        private System.Windows.Forms.Label lLDegMin;
        private System.Windows.Forms.Label lBDegMin;
        public System.Windows.Forms.TextBox tbLSec;
        public System.Windows.Forms.TextBox tbBSec;
        private System.Windows.Forms.Label lMin4;
        private System.Windows.Forms.Label lMin3;
        public System.Windows.Forms.TextBox tbLMin2;
        public System.Windows.Forms.TextBox tbBMin2;
        private System.Windows.Forms.Label lSec2;
        private System.Windows.Forms.Label lSec1;
        private System.Windows.Forms.Label lDeg4;
        private System.Windows.Forms.Label lDeg3;
        public System.Windows.Forms.TextBox tbLDeg2;
        private System.Windows.Forms.Label lLDegMinSec;
        public System.Windows.Forms.TextBox tbBDeg2;
        private System.Windows.Forms.Label lBDegMinSec;
        public System.Windows.Forms.TextBox tbYRect42;
        private System.Windows.Forms.Label lYRect42;
        public System.Windows.Forms.TextBox tbXRect42;
        private System.Windows.Forms.Label lXRect42;
        private System.Windows.Forms.Label lOwnHeight;
        public System.Windows.Forms.TextBox tbLRad;
        private System.Windows.Forms.Label lLRad;
        public System.Windows.Forms.TextBox tbBRad;
        private System.Windows.Forms.Label lBRad;
        private System.Windows.Forms.ComboBox cbChooseSC;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label lPt2Height;
        public System.Windows.Forms.TextBox tbPt2LSec;
        public System.Windows.Forms.TextBox tbPt2BSec;
        private System.Windows.Forms.Label lPt2Min4;
        private System.Windows.Forms.Label lPt2Min3;
        public System.Windows.Forms.TextBox tbPt2LMin2;
        public System.Windows.Forms.TextBox tbPt2BMin2;
        private System.Windows.Forms.Label lPt2Sec2;
        private System.Windows.Forms.Label lPt2Sec1;
        private System.Windows.Forms.Label lPt2Deg4;
        private System.Windows.Forms.Label lPt2Deg3;
        public System.Windows.Forms.TextBox tbPt2LDeg2;
        private System.Windows.Forms.Label lPt2LDegMinSec;
        public System.Windows.Forms.TextBox tbPt2BDeg2;
        private System.Windows.Forms.Label lPt2BDegMinSec;
        public System.Windows.Forms.TextBox tbPt2LRad;
        private System.Windows.Forms.Label lPt2LRad;
        public System.Windows.Forms.TextBox tbPt2BRad;
        private System.Windows.Forms.Label lPt2BRad;
        public System.Windows.Forms.TextBox tbPt2YRect;
        private System.Windows.Forms.Label lPt2YRect;
        public System.Windows.Forms.TextBox tbPt2XRect;
        private System.Windows.Forms.Label lPt2XRect;
        public System.Windows.Forms.TextBox tbPt2LMin1;
        public System.Windows.Forms.TextBox tbPt2BMin1;
        private System.Windows.Forms.Label lPt2LDegMin;
        private System.Windows.Forms.Label lPt2BDegMin;
        public System.Windows.Forms.TextBox tbPt2YRect42;
        private System.Windows.Forms.Label lPt2YRect42;
        public System.Windows.Forms.TextBox tbPt2XRect42;
        private System.Windows.Forms.Label lPt2XRect42;
        private System.Windows.Forms.Button buttonZSP;
        public System.Windows.Forms.PictureBox pbSP;
        private System.Windows.Forms.TabPage tpOpponentObject;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lPt1Height;
        public System.Windows.Forms.TextBox tbPt1LSec;
        public System.Windows.Forms.TextBox tbPt1BSec;
        private System.Windows.Forms.Label lPt1Min4;
        private System.Windows.Forms.Label lPt1Min3;
        public System.Windows.Forms.TextBox tbPt1LMin2;
        public System.Windows.Forms.TextBox tbPt1BMin2;
        private System.Windows.Forms.Label lPt1Sec2;
        private System.Windows.Forms.Label lPt1Sec1;
        private System.Windows.Forms.Label lPt1Deg4;
        private System.Windows.Forms.Label lPt1Deg3;
        public System.Windows.Forms.TextBox tbPt1LDeg2;
        private System.Windows.Forms.Label lPt1LDegMinSec;
        public System.Windows.Forms.TextBox tbPt1BDeg2;
        private System.Windows.Forms.Label lPt1BDegMinSec;
        public System.Windows.Forms.TextBox tbPt1LRad;
        private System.Windows.Forms.Label lPt1LRad;
        public System.Windows.Forms.TextBox tbPt1BRad;
        private System.Windows.Forms.Label lPt1BRad;
        public System.Windows.Forms.TextBox tbPt1YRect;
        private System.Windows.Forms.Label lPt1YRect;
        public System.Windows.Forms.TextBox tbPt1XRect;
        private System.Windows.Forms.Label lPt1XRect;
        public System.Windows.Forms.TextBox tbPt1LMin1;
        public System.Windows.Forms.TextBox tbPt1BMin1;
        private System.Windows.Forms.Label lPt1LDegMin;
        private System.Windows.Forms.Label lPt1BDegMin;
        public System.Windows.Forms.TextBox tbPt1YRect42;
        private System.Windows.Forms.Label lPt1YRect42;
        public System.Windows.Forms.TextBox tbPt1XRect42;
        private System.Windows.Forms.Label lPt1XRect42;
        private System.Windows.Forms.Button button2;
        public System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.ImageList imageList1;
        public System.Windows.Forms.ImageList imageList2;
        public System.Windows.Forms.ImageList imageList3;
        private System.Windows.Forms.Button bAccept;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        public System.Windows.Forms.TextBox tbOwnHeight;
        public System.Windows.Forms.TextBox tbPt1Height;
        public System.Windows.Forms.TextBox tbPt2Height;
        public System.Windows.Forms.CheckBox chbXY1;
        public System.Windows.Forms.CheckBox chbGPS;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Timer timerGPS;
        public System.Windows.Forms.GroupBox gbOwnRect;
        public System.Windows.Forms.GroupBox gbOwnRect42;
        public System.Windows.Forms.GroupBox gbOwnRad;
        public System.Windows.Forms.GroupBox gbOwnDegMin;
        public System.Windows.Forms.GroupBox gbOwnDegMinSec;
        public System.Windows.Forms.GroupBox gbPt2DegMinSec;
        public System.Windows.Forms.GroupBox gbPt2Rad;
        public System.Windows.Forms.GroupBox gbPt2Rect;
        public System.Windows.Forms.GroupBox gbPt2DegMin;
        public System.Windows.Forms.GroupBox gbPt2Rect42;
        public System.Windows.Forms.GroupBox gbPt1DegMinSec;
        public System.Windows.Forms.GroupBox gbPt1Rad;
        public System.Windows.Forms.GroupBox gbPt1Rect;
        public System.Windows.Forms.GroupBox gbPt1DegMin;
        public System.Windows.Forms.GroupBox gbPt1Rect42;
        private System.Windows.Forms.Button button9;
    }
}