﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.Data;
using System.Drawing;
using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Globalization;

namespace GrozaMap
{
    // Air  Class -> it is one airplane
    class ClassAirPlane
    {
        public double Lat=0;
        public double Long=0;
        public double X_m=0;
        public double Y_m=0;
        public double Angle=0;
        public double f=0;
        public int Num=0;
        public String sNum="";
        public int sh=0;
        public int fl_sh=0;
        public Color cl=new Color();

        public List<AirPlane1> list_air1 = new List<AirPlane1>();
        public List<AirPlane2> list_air2 = new List<AirPlane2>();

    } // Class
} // Namespace
